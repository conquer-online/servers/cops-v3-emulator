﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Linq;
using System.Text;


namespace Restart
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Restart Server[v1.0]");
            Console.WriteLine("Créé par Billy the Kid");
            Console.WriteLine("Copyright © COPS v1 2008\n");

            Console.WriteLine("Chargement de l'émulateur!");

            Ini restart_ = new Ini(System.Windows.Forms.Application.StartupPath + @"\restart.ini");

            string restart_chemin = restart_.ReadValue(Convert.ToString("Restart"), "Chemin");
            Console.WriteLine("Le chemin " + restart_chemin + " est chargé.");

            Console.WriteLine("Lancement de l'émulateur!");

            System.Diagnostics.Process.Start(restart_chemin);

            Environment.Exit(0);
        }
    }
}
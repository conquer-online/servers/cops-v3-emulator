﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.IO;
using OpenSSL;

public class BlowfishCrypto
{
    OpenSSL.Blowfish _blowfish;

    public BlowfishCrypto(string key)
    {
        _blowfish = new OpenSSL.Blowfish(OpenSSL.BlowfishAlgorithm.CFB64);
        _blowfish.SetKey(Encoding.ASCII.GetBytes(key));
    }

    void Decrypt(byte[] packet)
    {
        byte[] buffer = _blowfish.Decrypt(packet);
        System.Buffer.BlockCopy(buffer, 0, packet, 0, buffer.Length);
    }

    void Encrypt(byte[] packet)
    {
        byte[] buffer = _blowfish.Encrypt(packet);
        System.Buffer.BlockCopy(buffer, 0, packet, 0, buffer.Length);
    }

    public OpenSSL.Blowfish Blowfish
    {
        get { return _blowfish; }
    }
}
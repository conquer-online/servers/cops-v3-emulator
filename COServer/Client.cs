using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Timers;
using System.Collections;
using System.Threading;

namespace COServer_Project
{
    public class Client
    {
        public Cryptographer Crypto;
        public HybridSocket ListenSock;
        public int MessageId;
        public string Account = "";
        public byte Authentication;
        public System.Timers.Timer ClientTimer;
        public Character MyChar;
        public bool Online = true;
        public uint Status;
        public int CurrentNPC = 0;
        public bool There = false;
        IPEndPoint IPE;
        bool UppAgree = false;

        public int RGemId = 0;
        public int RItemype = 0;
        public int ChColor = 0;
        public byte GemId = 0;
        public byte WHOpen = 0;
        public uint GemHp = 700000;

        HybridSocket MySocket
        {
            get { return ListenSock; }
            set { ListenSock = value; }
        }

        public Client()
        {
            Crypto = new Cryptographer();
        }

        public void GetIPE()
        {
            IPE = (IPEndPoint)ListenSock.WinSock.RemoteEndPoint;
        }

        public unsafe void GetPacket(byte[] Data)
        {
            try
            {
                //byte[] Data = data;

                Crypto.Decrypt(ref Data);

                ushort PacketId = (ushort)((Data[3] << 8) | Data[2]);
                int PacketType;

                if (PacketId == 53101 || PacketId == 53110)
                    Drop();

                switch (PacketId)
                {
                    case 1024:
                        {
                            if (MyChar.StatP > 0)
                            {
                                MyChar.StatP--;
                                MyChar.Str += Data[4];
                                MyChar.Agi += Data[5];
                                MyChar.Vit += Data[6];
                                MyChar.Spi += Data[7];
                            }
                            break;
                        }
                    case 2050:
                        {
                            if (Data[4] == 3 && MyChar.CPs >= 5)
                            {
                                if (MyChar.CanSend == true)
                                {
                                    MyChar.CPs -= 5;
                                    SendPacket(General.MyPackets.Vital(MyChar.UID, 30, MyChar.CPs));
                                    byte Len = Data[13];
                                    string Message = "";
                                    for (int i = 0; i < Len; i++)
                                    {
                                        Message += Convert.ToChar(Data[14 + i]);
                                    }
                                    //World.SendMsgToAll(Message, MyChar.Name, 2010);
                                    World.SendMsgToAll(Message, MyChar.Name, 2500);

                                    MyChar.CanSend = false;
                                    MyChar.Radio.Interval = 60000;
                                    MyChar.Radio.Elapsed += new ElapsedEventHandler(MyChar.Radio_Elapsed);
                                    MyChar.Radio.Start();
                                }
                                else
                                {
                                    SendPacket(General.MyPackets.NPCSay("Vous ne pouvez pas envoyez de message pour le moment."));
                                    SendPacket(General.MyPackets.NPCLink("je vois.", 255));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                            }
                            break;
                        }
                    case 1112://See guild member's status
                        {
                            string Name = "";
                            int Pos = 9;
                            while (Pos < Data.Length)
                            {
                                Name += Convert.ToChar(Data[Pos]);
                                Pos++;
                            }
                            break;
                        }
                    case 1015://Send guild members
                        {
                            if (MyChar.MyGuild == null)
                                return;
                            string Total = "";
                            string OnlineMembers = "";
                            string OfflineMembers = "";
                            byte Count = 1;

                            string[] Splitter = MyChar.MyGuild.Creator.Split(':');
                            if (World.AllChars.Contains(uint.Parse(Splitter[1])))
                                OnlineMembers += Convert.ToChar((Splitter[0] + Convert.ToChar(32) + Splitter[2] + Convert.ToChar(32) + "1").Length) + (Splitter[0] + Convert.ToChar(32) + Splitter[2] + Convert.ToChar(32) + "1");
                            else
                                OfflineMembers += Convert.ToChar((Splitter[0] + Convert.ToChar(32) + Splitter[2] + Convert.ToChar(32) + "0").Length) + (Splitter[0] + Convert.ToChar(32) + Splitter[2] + Convert.ToChar(32) + "0");


                            foreach (DictionaryEntry DE in MyChar.MyGuild.DLs)
                            {
                                string DL = (string)DE.Value;
                                Count++;
                                Splitter = DL.Split(':');
                                if (World.AllChars.Contains(uint.Parse(Splitter[1])))
                                    OnlineMembers += Convert.ToChar((Splitter[0] + Convert.ToChar(32) + Splitter[2] + Convert.ToChar(32) + "1").Length) + (Splitter[0] + Convert.ToChar(32) + Splitter[2] + Convert.ToChar(32) + "1");
                                else
                                    OfflineMembers += Convert.ToChar((Splitter[0] + Convert.ToChar(32) + Splitter[2] + Convert.ToChar(32) + "0").Length) + (Splitter[0] + Convert.ToChar(32) + Splitter[2] + Convert.ToChar(32) + "0");
                            }

                            foreach (DictionaryEntry DE in MyChar.MyGuild.Members)
                            {
                                string NM = (string)DE.Value;
                                Count++;
                                Splitter = NM.Split(':');
                                if (World.AllChars.Contains(uint.Parse(Splitter[1])))
                                    OnlineMembers += Convert.ToChar((Splitter[0] + Convert.ToChar(32) + Splitter[2] + Convert.ToChar(32) + "1").Length) + (Splitter[0] + Convert.ToChar(32) + Splitter[2] + Convert.ToChar(32) + "1");
                                else
                                    OfflineMembers += Convert.ToChar((Splitter[0] + Convert.ToChar(32) + Splitter[2] + Convert.ToChar(32) + "0").Length) + (Splitter[0] + Convert.ToChar(32) + Splitter[2] + Convert.ToChar(32) + "0");
                            }

                            Total = OnlineMembers + OfflineMembers;

                            SendPacket(Data);
                            SendPacket(General.MyPackets.StringGuild(11, 11, Total, Count));

                            break;
                        }
                    case 1107:
                        {
                            byte Type = Data[4];
                            switch (Type)
                            {
                                case 1://Join guild request
                                    {
                                        if (MyChar.MyGuild == null)
                                        {
                                            uint UID = BitConverter.ToUInt32(Data, 8);

                                            Character JoinWho = (Character)World.AllChars[UID];
                                            if (JoinWho.MyGuild != null && JoinWho.GuildPosition == 100 || JoinWho.GuildPosition == 90)
                                                JoinWho.MyClient.SendPacket(General.MyPackets.SendGuild(MyChar.UID, 1));
                                        }

                                        break;
                                    }
                                case 2://Accept join request
                                    {
                                        uint UID = BitConverter.ToUInt32(Data, 8);
                                        if (World.AllChars.Contains(UID))
                                        {
                                            Character WhoJoins = (Character)World.AllChars[UID];
                                            if (WhoJoins.MyGuild == null)
                                            {
                                                WhoJoins.GuildID = MyChar.MyGuild.GuildID;
                                                WhoJoins.MyGuild = MyChar.MyGuild;
                                                WhoJoins.GuildPosition = 50;
                                                MyChar.MyGuild.PlayerJoins(WhoJoins);
                                                World.UpdateSpawn(WhoJoins);

                                                WhoJoins.MyClient.SendPacket(General.MyPackets.GuildName(WhoJoins.GuildID, WhoJoins.MyGuild.GuildName));
                                                WhoJoins.MyClient.SendPacket(General.MyPackets.GuildInfo(WhoJoins.MyGuild, WhoJoins));
                                            }
                                        }
                                        break;
                                    }
                                case 3://Leave the guild
                                    {
                                        if (MyChar.MyGuild != null && MyChar.GuildPosition != 100)
                                        {
                                            SendPacket(General.MyPackets.SendGuild(MyChar.MyGuild.GuildID, 19));
                                            MyChar.MyGuild.PlayerQuits(MyChar);
                                            MyChar.GuildDonation = 0;
                                            MyChar.GuildID = 0;
                                            MyChar.GuildPosition = 0;
                                            MyChar.MyGuild = null;
                                            World.UpdateSpawn(MyChar);
                                            World.SpawnOthersToMe(MyChar, false);
                                        }
                                        break;
                                    }
                                case 12://Guild status
                                    {
                                        if (MyChar.MyGuild != null)
                                        {
                                            SendPacket(General.MyPackets.GuildName(MyChar.GuildID, MyChar.MyGuild.GuildName));
                                            SendPacket(General.MyPackets.GuildInfo(MyChar.MyGuild, MyChar));
                                            SendPacket(General.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, MyChar.MyGuild.Bulletin, 2111));
                                            SendPacket(General.MyPackets.GeneralData(MyChar.UID, 0, 0, 0, 97));
                                        }
                                        break;
                                    }
                                case 11://Donate
                                    {
                                        if (MyChar.MyGuild != null)
                                        {
                                            uint Amount = BitConverter.ToUInt32(Data, 8);

                                            if (MyChar.Silvers >= Amount)
                                            {
                                                MyChar.MyGuild.Fund += Amount;
                                                MyChar.Silvers -= Amount;
                                                MyChar.GuildDonation += Amount;
                                                MyChar.MyGuild.Refresh(MyChar);
                                                SendPacket(General.MyPackets.GuildInfo(MyChar.MyGuild, MyChar));
                                                SendPacket(General.MyPackets.Vital(MyChar.UID, 4, MyChar.Silvers));
                                                World.SendMsgToAll(MyChar.Name + " a donn� " + Amount.ToString() + " argents � " + MyChar.MyGuild.GuildName + ".", "SYSTEM", 2005);
                                            }
                                        }

                                        break;
                                    }
                            }
                            break;
                        }
                    case 1019:
                        {
                            uint UID = BitConverter.ToUInt32(Data, 4);
                            byte Type = Data[8];
                            switch (Type)
                            {
                                case 14:
                                    {
                                        World.SendMsgToAll(MyChar.Name + " Has Broken Friendship With " + (string)MyChar.Friends[UID], "SYSTEM", 2005);
                                        MyChar.Friends.Remove(UID);
                                        SendPacket(General.MyPackets.FriendEnemyPacket(UID, "", 14, 0));

                                        if (World.AllChars.Contains(UID))
                                        {
                                            Character Char = (Character)World.AllChars[UID];
                                            if (Char != null && Char.MyClient != null)
                                            {
                                                Char.MyClient.SendPacket(General.MyPackets.FriendEnemyPacket(MyChar.UID, "", 14, 0));
                                                Char.Friends.Remove(MyChar.UID);
                                            }
                                            else
                                                DataBase.RemoveFromFriend(MyChar.UID, UID);
                                        }
                                        else
                                            DataBase.RemoveFromFriend(MyChar.UID, UID);

                                        break;
                                    }
                                case 10:
                                    {
                                        Character Other = (Character)World.AllChars[UID];
                                        if (Other == null)
                                            return;
                                        if (Other.RequestFriendWith == MyChar.UID)
                                        {
                                            MyChar.AddFriend(Other);
                                            Other.AddFriend(MyChar);
                                            World.SendMsgToAll(MyChar.Name + " et " + Other.Name + " sont devenu des amis", "SYSTEM", 2005);
                                        }
                                        else
                                        {
                                            MyChar.RequestFriendWith = UID;
                                            Other.MyClient.SendPacket(General.MyPackets.SendMsg(Other.MyClient.MessageId, "SYSTEM", Other.Name, MyChar.Name + " a fait une requ�te d'amitier.", 2005));
                                        }
                                        break;
                                    }
                            }

                            break;
                        }
                    case 1056:
                        {
                            uint UID = BitConverter.ToUInt32(Data, 4);

                            byte Type = Data[8];

                            switch (Type)
                            {
                                case 1://Request trade
                                    {
                                        Character Who = (Character)World.AllChars[UID];
                                        if (UID != MyChar.TradingWith)
                                        {
                                            Who.MyClient.SendPacket(General.MyPackets.TradePacket(MyChar.UID, 1));
                                            MyChar.TradingWith = UID;
                                            Who.TradingWith = MyChar.UID;
                                            SendPacket(General.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "[�change]Requ�te d'�change envoy�e.", 2005));
                                        }
                                        else
                                        {
                                            Who.Trading = true;
                                            MyChar.Trading = true;
                                            Who.MyClient.SendPacket(General.MyPackets.TradePacket(MyChar.UID, 3));
                                            SendPacket(General.MyPackets.TradePacket(Who.UID, 3));
                                        }
                                        break;
                                    }
                                case 2://Close trade
                                    {
                                        if (MyChar.Trading)
                                        {
                                            Character Who = (Character)World.AllChars[MyChar.TradingWith];
                                            if (Who != null)
                                            {
                                                Who.MyClient.SendPacket(General.MyPackets.TradePacket(MyChar.TradingWith, 5));
                                                SendPacket(General.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "�change rat�!", 2005));
                                                Who.MyClient.SendPacket(General.MyPackets.SendMsg(Who.MyClient.MessageId, "SYSTEM", Who.Name, "�change rat�!", 2005));
                                                Who.Trading = false;
                                                MyChar.Trading = false;
                                                Who.TradingWith = 0;
                                                MyChar.TradingWith = 0;
                                                foreach (uint iuid in MyChar.MyTradeSide)
                                                {
                                                    string Item = MyChar.FindItem(iuid);
                                                    string[] Splitter = Item.Split('-');
                                                    SendPacket(General.MyPackets.AddItem(iuid, int.Parse(Splitter[0]), byte.Parse(Splitter[1]), byte.Parse(Splitter[2]), byte.Parse(Splitter[3]), byte.Parse(Splitter[4]), byte.Parse(Splitter[5]), 0, 100, 100));
                                                }
                                                foreach (uint iuid in Who.MyTradeSide)
                                                {
                                                    string Item = Who.FindItem(iuid);
                                                    string[] Splitter = Item.Split('-');
                                                    Who.MyClient.SendPacket(General.MyPackets.AddItem(iuid, int.Parse(Splitter[0]), byte.Parse(Splitter[1]), byte.Parse(Splitter[2]), byte.Parse(Splitter[3]), byte.Parse(Splitter[4]), byte.Parse(Splitter[5]), 0, 100, 100));
                                                }
                                                MyChar.MyTradeSide = new ArrayList(20);
                                                Who.MyTradeSide = new ArrayList(20);
                                                MyChar.TradingCPs = 0;
                                                MyChar.TradingSilvers = 0;
                                                MyChar.TradeOK = false;
                                                Who.TradeOK = false;
                                                Who.MyTradeSideCount = 0;
                                                MyChar.MyTradeSideCount = 0;
                                            }
                                        }

                                        break;
                                    }
                                case 6://Add an item
                                    {
                                        Character Who = (Character)World.AllChars[MyChar.TradingWith];
                                        if (Who.ItemsInInventory + MyChar.MyTradeSideCount < 40)
                                        {
                                            string Item = MyChar.FindItem(UID);
                                            Who.MyClient.SendPacket(General.MyPackets.TradeItem(UID, Item));
                                            MyChar.MyTradeSide.Add(UID);
                                            MyChar.MyTradeSideCount++;
                                        }
                                        else
                                        {
                                            SendPacket(General.MyPackets.TradePacket(UID, 11));
                                            SendPacket(General.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "[�change]Votre partenaire ne peut pas avoir plus d'objets.", 2005));
                                        }
                                        break;
                                    }
                                case 7://Specify money
                                    {
                                        MyChar.TradingSilvers = UID;
                                        Character Who = (Character)World.AllChars[MyChar.TradingWith];
                                        Who.MyClient.SendPacket(General.MyPackets.TradePacket(UID, 8));

                                        break;
                                    }
                                case 10://OK
                                    {
                                        Character Who = (Character)World.AllChars[MyChar.TradingWith];

                                        if (Who.TradeOK)
                                        {
                                            Who.MyClient.SendPacket(General.MyPackets.TradePacket(MyChar.TradingWith, 5));
                                            SendPacket(General.MyPackets.TradePacket(MyChar.UID, 5));
                                            foreach (uint itemuid in Who.MyTradeSide)
                                            {
                                                string item = Who.FindItem(itemuid);
                                                MyChar.AddItem(item, 0, itemuid);
                                                Who.RemoveItem(itemuid);
                                            }
                                            foreach (uint itemuid in MyChar.MyTradeSide)
                                            {
                                                string item = MyChar.FindItem(itemuid);
                                                Who.AddItem(item, 0, itemuid);
                                                MyChar.RemoveItem(itemuid);
                                            }

                                            if (Status != 7)
                                            {
                                                MyChar.Silvers += Who.TradingSilvers;
                                                MyChar.CPs += Who.TradingCPs;

                                                MyChar.Silvers -= MyChar.TradingSilvers;
                                                MyChar.CPs -= MyChar.TradingCPs;

                                                Who.Silvers += MyChar.TradingSilvers;
                                                Who.CPs += MyChar.TradingCPs;

                                                Who.Silvers -= Who.TradingSilvers;
                                                Who.CPs -= Who.TradingCPs;
                                            }

                                            SendPacket(General.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "�change r�ussi.", 2005));
                                            Who.MyClient.SendPacket(General.MyPackets.SendMsg(Who.MyClient.MessageId, "SYSTEM", Who.Name, "�change r�ussi.", 2005));

                                            MyChar.Trading = false;
                                            MyChar.TradingWith = 0;
                                            MyChar.MyTradeSideCount = 0;
                                            MyChar.TradeOK = false;
                                            MyChar.MyTradeSide = new ArrayList(20);
                                            MyChar.TradingCPs = 0;
                                            MyChar.TradingSilvers = 0;

                                            Who.MyTradeSide = new ArrayList(20);
                                            Who.TradingCPs = 0;
                                            Who.TradingSilvers = 0;
                                            Who.TradeOK = false;
                                            Who.TradingWith = 0;
                                            Who.Trading = false;
                                            Who.MyTradeSideCount = 0;


                                            SendPacket(General.MyPackets.Vital(MyChar.UID, 4, MyChar.Silvers));
                                            SendPacket(General.MyPackets.Vital(MyChar.UID, 30, MyChar.CPs));
                                            Who.MyClient.SendPacket(General.MyPackets.Vital(Who.UID, 4, Who.Silvers));
                                            Who.MyClient.SendPacket(General.MyPackets.Vital(Who.UID, 30, Who.CPs));
                                        }
                                        else
                                        {
                                            MyChar.TradeOK = true;
                                            Who.MyClient.SendPacket(General.MyPackets.TradePacket(0, 10));
                                        }
                                        break;
                                    }
                                case 13://Specify CPs
                                    {
                                        MyChar.TradingCPs = UID;
                                        Character Who = (Character)World.AllChars[MyChar.TradingWith];
                                        Who.MyClient.SendPacket(General.MyPackets.TradePacket(UID, 12));
                                        break;
                                    }
                            }

                            break;
                        }
                    case 1023:
                        {
                            byte Type = Data[4];
                            switch (Type)
                            {
                                case 0://Create
                                    {
                                        MyChar.TeamLeader = true;
                                        MyChar.MyTeamLeader = MyChar;
                                        SendPacket(General.MyPackets.Vital(MyChar.UID, 26, MyChar.GetStat()));
                                        SendPacket(General.MyPackets.TeamPacket(MyChar.UID, 0));
                                        World.UpdateSpawn(MyChar);
                                        break;
                                    }
                                case 1://Join request
                                    {
                                        uint JoinWho = (uint)(Data[8] + (Data[9] << 8) + (Data[10] << 16) + (Data[11] << 24));
                                        Character Who = (Character)World.AllChars[JoinWho];
                                        if (Who.TeamLeader)
                                        {
                                            if (!Who.JoinForbidden)
                                            {
                                                if (Who.PlayersInTeam < 6)
                                                {
                                                    Who.MyClient.SendPacket(General.MyPackets.TeamPacket(MyChar.UID, 1));
                                                    SendPacket(General.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "[�quipe]La demande a �t� envoy�!", 2005));
                                                }
                                                else
                                                    SendPacket(General.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "[�quipe]L'�quipe est pleine.", 2005));
                                            }
                                            else
                                                SendPacket(General.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "L'�quipe n'accepte pas d'autres membres.", 2005));
                                        }
                                        else
                                            SendPacket(General.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "[�quipe]La cible n'a pas cr�� d'�quipe.", 2005));

                                        break;
                                    }
                                case 2://Exit team
                                    {
                                        MyChar.MyTeamLeader.TeamRemove(MyChar, false);
                                        break;
                                    }
                                case 3://I accept invitation
                                    {
                                        uint WhoInvited = (uint)(Data[8] + (Data[9] << 8) + (Data[10] << 16) + (Data[11] << 24));
                                        Character Who = (Character)World.AllChars[WhoInvited];
                                        if (Who != null)
                                            Who.TeamAdd(MyChar);

                                        break;
                                    }
                                case 4://Invite request
                                    {
                                        uint InviteWho = (uint)(Data[8] + (Data[9] << 8) + (Data[10] << 16) + (Data[11] << 24));
                                        Character Invited = (Character)World.AllChars[InviteWho];
                                        if (!Invited.TeamLeader && Invited.MyTeamLeader == null && MyChar.TeamLeader && MyChar.PlayersInTeam < 6)
                                        {
                                            Invited.MyClient.SendPacket(General.MyPackets.TeamPacket(MyChar.UID, 6));
                                            Invited.MyClient.SendPacket(General.MyPackets.TeamPacket(MyChar.UID, 4));                                            
                                        }

                                        break;
                                    }
                                case 5://I accept your join request
                                    {
                                        uint WhoJoins = (uint)(Data[8] + (Data[9] << 8) + (Data[10] << 16) + (Data[11] << 24));
                                        Character Joiner = (Character)World.AllChars[WhoJoins];

                                        MyChar.TeamAdd(Joiner);

                                        break;
                                    }
                                case 6://Dismiss
                                    {
                                        MyChar.TeamDismiss();
                                        break;
                                    }
                                case 7://Kick
                                    {
                                        uint KickWho = (uint)(Data[8] + (Data[9] << 8) + (Data[10] << 16) + (Data[11] << 24));
                                        Character Kicked = (Character)World.AllChars[KickWho];

                                        MyChar.TeamRemove(Kicked, true);
                                        break;
                                    }
                                case 8://Forbid joining
                                    {
                                        MyChar.JoinForbidden = true;
                                        break;
                                    }
                                case 9://UnForbid joining
                                    {
                                        MyChar.JoinForbidden = false;
                                        break;
                                    }
                            }
                            break;
                        }
                    case 1102:
                        {
                            uint NPCID = (uint)((Data[7] << 24) + (Data[6] << 16) + (Data[5] << 8) + (Data[4]));
                            uint ItemUID = (uint)((Data[15] << 24) + (Data[14] << 16) + (Data[13] << 8) + (Data[12]));
                            byte Type = Data[8];
                            byte WHID = 0;

                            if (NPCID == 8)
                                WHID = 0;
                            else if (NPCID == 81)
                                WHID = 1;
                            else if (NPCID == 82)
                                WHID = 2;
                            else if (NPCID == 83)
                                WHID = 3;
                            else if (NPCID == 84)
                                WHID = 4;
                            else if (NPCID == 85)
                                WHID = 5;


                            if (Type == 0)
                            {
                                SendPacket(General.MyPackets.WhItems(MyChar, WHID, (ushort)NPCID));
                            }
                            else if (Type == 1)//Throw an item into warehouse
                            {
                                if (WHID == 0 && MyChar.TCWHCount < 20 || WHID == 1 && MyChar.PCWHCount < 20 || WHID == 2 && MyChar.ACWHCount < 20 || WHID == 3 && MyChar.DCWHCount < 20 || WHID == 4 && MyChar.BIWHCount < 20 || WHID == 5 && MyChar.MAWHCount < 40)
                                {
                                    string Item = MyChar.FindItem(ItemUID);
                                    MyChar.RemoveItem(ItemUID);
                                    MyChar.AddWHItem(Item, ItemUID, WHID);
                                    SendPacket(General.MyPackets.WhItems(MyChar, WHID, (ushort)NPCID));
                                }
                            }
                            else if (Type == 2)//Take an item from warehouse
                            {
                                if (MyChar.ItemsInInventory < 40)
                                {
                                    string Item = MyChar.FindWHItem(ItemUID, WHID);
                                    MyChar.RemoveWHItem(ItemUID);
                                    SendPacket(General.MyPackets.WhItems(MyChar, WHID, (ushort)NPCID));
                                    MyChar.AddItem(Item, 0, ItemUID);
                                }
                            }

                            break;
                        }
                    /*case 2036:
                        {
                            uint MainUID = (uint)((Data[11] << 24) + (Data[10] << 16) + (Data[9] << 8) + Data[8]);
                            uint Minor1UID = (uint)((Data[15] << 24) + (Data[14] << 16) + (Data[13] << 8) + Data[12]);
                            uint Minor2UID = (uint)((Data[19] << 24) + (Data[18] << 16) + (Data[17] << 8) + Data[16]);
                            uint Gem1 = (uint)((Data[23] << 24) + (Data[22] << 16) + (Data[21] << 8) + Data[20]);
                            uint Gem2 = (uint)((Data[24] << 24) + (Data[23] << 16) + (Data[22] << 8) + Data[21]);

                            string MainItem = MyChar.FindItem(MainUID);
                            string Minor1Item = MyChar.FindItem(Minor1UID);
                            string Minor2Item = MyChar.FindItem(Minor2UID);

                            if (MainItem == null || Minor1Item == null || Minor2Item == null)
                                return;

                            byte MainPlus = 0;
                            byte Minor1Plus = 0;
                            byte Minor2Plus = 0;
                            uint MainItemId = 0;
                            uint Minor1ItemId = 0;
                            uint Minor2ItemId = 0;

                            string[] Splitter;
                            string[] MainItemE;
                            MainItemE = MainItem.Split('-');
                            MainPlus = byte.Parse(MainItemE[1]);
                            MainItemId = uint.Parse(MainItemE[0]);
                            Splitter = Minor1Item.Split('-');
                            Minor1Plus = byte.Parse(Splitter[1]);
                            Minor1ItemId = uint.Parse(Splitter[0]);
                            Splitter = Minor2Item.Split('-');
                            Minor2Plus = byte.Parse(Splitter[1]);
                            Minor2ItemId = uint.Parse(Splitter[0]);

                            if (Minor1Plus == Minor2Plus)
                                if (Minor1Plus == MainPlus || MainPlus == 0 && Minor1Plus == 1)
                                    if (Other.ItemType2(Minor1ItemId) == Other.ItemType2(Minor2ItemId) && Other.ItemType2(Minor2ItemId) == Other.ItemType2(MainItemId) || (Other.ItemType(MainItemId) == 4 && Other.ItemType(Minor1ItemId) == 4 && Other.ItemType(Minor2ItemId) == 4) || Other.ItemType(MainItemId) == 5 && Other.ItemType(Minor1ItemId) == 5 && Other.ItemType(Minor2ItemId) == 5)
                                    {
                                        MainPlus++;
                                        MyChar.RemoveItem(Minor1UID);
                                        MyChar.RemoveItem(Minor2UID);
                                        MyChar.RemoveItem(MainUID);
                                        if (Gem1 != null && Gem1 != 0)
                                        {
                                            MyChar.RemoveItem(Gem1);
                                        }
                                        if (Gem2 != null && Gem2 != 0)
                                        {
                                            MyChar.RemoveItem(Gem1);
                                        }
                                        MyChar.AddItem(MainItemE[0] + "-" + MainPlus + "-" + MainItemE[2] + "-" + MainItemE[3] + "-" + MainItemE[4] + "-" + MainItemE[5], 0, MainUID);
                                        return;
                                    }

                            if (Minor1Plus == Minor2Plus)
                                if (Minor1Plus == MainPlus || MainPlus == 0 && Minor1Plus == 1)
                                    if (Other.ItemType2(Minor1ItemId) == 73 || Other.ItemType2(Minor2ItemId) == 73)
                                        if ((Other.ItemType2(Minor1ItemId) == 73 && Other.ItemType2(Minor2ItemId) == 73) || (Other.ItemType2(Minor1ItemId) == 73 && Other.ItemType2(Minor2ItemId) != 73 && Other.ItemType2(MainItemId) == Other.ItemType2(Minor2ItemId)) || (Other.ItemType2(Minor2ItemId) == 73 && Other.ItemType2(Minor1ItemId) != 73 && Other.ItemType2(MainItemId) == Other.ItemType2(Minor1ItemId)))
                                        {
                                            MainPlus++;
                                            MyChar.RemoveItem(Minor1UID);
                                            MyChar.RemoveItem(Minor2UID);
                                            MyChar.RemoveItem(MainUID);
                                            if (Gem1 != null && Gem1 != 0)
                                            {
                                                MyChar.RemoveItem(Gem1);
                                            }
                                            if (Gem2 != null && Gem2 != 0)
                                            {
                                                MyChar.RemoveItem(Gem2);
                                            }
                                            MyChar.AddItem(MainItemE[0] + "-" + MainPlus + "-" + MainItemE[2] + "-" + MainItemE[3] + "-" + MainItemE[4] + "-" + MainItemE[5], 0, MainUID);
                                        }

                            break;
                        }*/
                    case 2036:
                        {
                            uint MainUID = (uint)((Data[11] << 24) + (Data[10] << 16) + (Data[9] << 8) + Data[8]);
                            uint Minor1UID = (uint)((Data[15] << 24) + (Data[14] << 16) + (Data[13] << 8) + Data[12]);
                            uint Minor2UID = (uint)((Data[19] << 24) + (Data[18] << 16) + (Data[17] << 8) + Data[16]);
                            uint Gem1 = (uint)((Data[23] << 24) + (Data[22] << 16) + (Data[21] << 8) + Data[20]);
                            uint Gem2 = (uint)((Data[24] << 24) + (Data[23] << 16) + (Data[22] << 8) + Data[21]);

                            string MainItem = MyChar.FindItem(MainUID);
                            string Minor1Item = MyChar.FindItem(Minor1UID);
                            string Minor2Item = MyChar.FindItem(Minor2UID);

                            if (MainItem == null || Minor1Item == null || Minor2Item == null)
                                return;

                            byte MainPlus = 0;
                            byte Minor1Plus = 0;
                            byte Minor2Plus = 0;
                            uint MainItemId = 0;
                            uint Minor1ItemId = 0;
                            uint Minor2ItemId = 0;

                            string[] Splitter;
                            string[] MainItemE;
                            MainItemE = MainItem.Split('-');
                            MainPlus = byte.Parse(MainItemE[1]);
                            MainItemId = uint.Parse(MainItemE[0]);
                            Splitter = Minor1Item.Split('-');
                            Minor1Plus = byte.Parse(Splitter[1]);
                            Minor1ItemId = uint.Parse(Splitter[0]);
                            Splitter = Minor2Item.Split('-');
                            Minor2Plus = byte.Parse(Splitter[1]);
                            Minor2ItemId = uint.Parse(Splitter[0]);

                            if (Minor1Plus == Minor2Plus)
                                if (Minor1Plus == MainPlus || MainPlus == 0 && Minor1Plus == 1)
                                    if (Other.ItemType2(Minor1ItemId) == Other.ItemType2(Minor2ItemId) && Other.ItemType2(Minor2ItemId) == Other.ItemType2(MainItemId) || (Other.ItemType(MainItemId) == 4 && Other.ItemType(Minor1ItemId) == 4 && Other.ItemType(Minor2ItemId) == 4) || Other.ItemType(MainItemId) == 5 && Other.ItemType(Minor1ItemId) == 5 && Other.ItemType(Minor2ItemId) == 5)
                                    {
                                        MainPlus++;
                                        MyChar.RemoveItem(Minor1UID);
                                        MyChar.RemoveItem(Minor2UID);
                                        MyChar.RemoveItem(MainUID);
                                        if (Gem1 != null && Gem1 != 0)
                                        {
                                            MyChar.RemoveItem(Gem1);
                                        }
                                        if (Gem2 != null && Gem2 != 0)
                                        {
                                            MyChar.RemoveItem(Gem2);
                                        }
                                        MyChar.AddItem(MainItemE[0] + "-" + MainPlus + "-" + MainItemE[2] + "-" + MainItemE[3] + "-" + MainItemE[4] + "-" + MainItemE[5], 0, MainUID);
                                        return;
                                    }

                            if (Minor1Plus == Minor2Plus)
                                if (Minor1Plus == MainPlus || MainPlus == 0 && Minor1Plus == 1)
                                    if (Other.ItemType2(Minor1ItemId) == 73 || Other.ItemType2(Minor2ItemId) == 73)
                                        if ((Other.ItemType2(Minor1ItemId) == 73 && Other.ItemType2(Minor2ItemId) == 73) || (Other.ItemType2(Minor1ItemId) == 73 && Other.ItemType2(Minor2ItemId) != 73 && Other.ItemType2(MainItemId) == Other.ItemType2(Minor2ItemId)) || (Other.ItemType2(Minor2ItemId) == 73 && Other.ItemType2(Minor1ItemId) != 73 && Other.ItemType2(MainItemId) == Other.ItemType2(Minor1ItemId)))
                                        {
                                            MainPlus++;
                                            MyChar.RemoveItem(Minor1UID);
                                            MyChar.RemoveItem(Minor2UID);
                                            MyChar.RemoveItem(MainUID);
                                            if (Gem1 != null && Gem1 != 0)
                                            {
                                                MyChar.RemoveItem(Gem1);
                                            }
                                            if (Gem2 != null && Gem2 != 0)
                                            {
                                                MyChar.RemoveItem(Gem2);
                                            }
                                            MyChar.AddItem(MainItemE[0] + "-" + MainPlus + "-" + MainItemE[2] + "-" + MainItemE[3] + "-" + MainItemE[4] + "-" + MainItemE[5], 0, MainUID);
                                        }

                            break;
                        }
                    case 1101:
                        {
                            MyChar.Ready = false;
                            if (MyChar.ItemsInInventory > 39)
                                return;
                            uint ItemUID = (uint)((Data[7] << 24) + (Data[6] << 16) + (Data[5] << 8) + Data[4]);
                            DroppedItem TehItem = null;
                            if (DroppedItems.AllDroppedItems.Contains(ItemUID))
                                TehItem = (DroppedItem)DroppedItems.AllDroppedItems[ItemUID];

                            if (TehItem != null)
                            {
                                if (TehItem.Money == 0)
                                    MyChar.AddItem(TehItem.Item, 0, ItemUID);
                                else
                                {
                                    MyChar.Silvers += TehItem.Money;
                                    SendPacket(General.MyPackets.Vital(MyChar.UID, 4, MyChar.Silvers));
                                    SendPacket(General.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "Vous avez rammass� " + TehItem.Money + " argents.", 2005));
                                }

                                DroppedItems.AllDroppedItems.Remove(ItemUID);
                                World.ItemDissappears(TehItem);
                            }
                            MyChar.Ready = true;
                            break;
                        }
                    case 1022:
                        {
                            MyChar.Ready = false;
                            int AttackType = (Data[23] << 24) + (Data[22] << 16) + (Data[21] << 8) + (Data[20]);
                            if (DateTime.Now < MyChar.LastTargetting.AddMilliseconds(255))
                                return;

                            MyChar.LastTargetting = DateTime.Now;

                            if (AttackType == 8)//Marriage Proposal
                            {
                                fixed (byte* Ptr = Data)
                                {
                                    uint TargetUID = *(uint*)(Ptr + 12);
                                    Character Target = (Character)World.AllChars[TargetUID];
                                    *(uint*)(Ptr + 8) = MyChar.UID;
                                    {
                                        if (MyChar.Model == 2001 || MyChar.Model == 2002)
                                        {
                                            if (Target.Model == 1003 || Target.Model == 1004)
                                            {
                                                if (Target.Spouse == "" || Target.Spouse == "Non")
                                                {
                                                    Target.MyClient.SendPacket(Data);
                                                }
                                                else
                                                {
                                                    SendPacket(General.MyPackets.NPCSay(Target.Name + " est d�j� mari�."));
                                                    SendPacket(General.MyPackets.NPCLink("Ok.", 255));
                                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                                    SendPacket(General.MyPackets.NPCFinish());
                                                }
                                            }
                                            else
                                            {
                                                SendPacket(General.MyPackets.NPCSay("Pas de mariage gay ici!"));
                                                SendPacket(General.MyPackets.NPCLink("Je vois.", 255));
                                                SendPacket(General.MyPackets.NPCSetFace(30));
                                                SendPacket(General.MyPackets.NPCFinish());
                                            }
                                        }
                                        if (MyChar.Model == 1003 || MyChar.Model == 1004)
                                        {
                                            if (Target.Model == 2001 || Target.Model == 2002)
                                            {
                                                if (Target.Spouse == "" || Target.Spouse == "Non")
                                                {
                                                    Target.MyClient.SendPacket(Data);
                                                }
                                                else
                                                {
                                                    SendPacket(General.MyPackets.NPCSay(Target.Name + " est d�j� mari�."));
                                                    SendPacket(General.MyPackets.NPCLink("Ok.", 255));
                                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                                    SendPacket(General.MyPackets.NPCFinish());
                                                }
                                            }
                                            else
                                            {
                                                SendPacket(General.MyPackets.NPCSay("Pas de mariage gay ici!"));
                                                SendPacket(General.MyPackets.NPCLink("Je vois.", 255));
                                                SendPacket(General.MyPackets.NPCSetFace(30));
                                                SendPacket(General.MyPackets.NPCFinish());
                                            }
                                        }
                                    }
                                }
                            }
                            if (AttackType == 9)//Marriage Accept
                            {
                                fixed (byte* Ptr = Data)
                                {
                                    uint UID = *(uint*)(Ptr + 12);
                                    Character Char = (Character)World.AllChars[UID];
                                    {
                                        if (Char.Model == 2001 || Char.Model == 2002)
                                        {
                                            if (MyChar.Model == 1003 || MyChar.Model == 1004)
                                            {
                                                Char.Spouse = MyChar.Name;
                                                MyChar.Spouse = Char.Name;
                                                MyChar.SaveSpouse();
                                                Char.SaveSpouse();
                                                SendPacket(General.MyPackets.CharacterInfo(MyChar));
                                                World.SendMsgToAll(Char.Name + " et " + MyChar.Name + " sont tomb�s dans le monde de cupidon! Vous devez vous d�connectez pour afficher!", "LoveStone", 2011);
                                            }
                                            else
                                            {
                                                SendPacket(General.MyPackets.NPCSay("Pas de mariage gay ici!"));
                                                SendPacket(General.MyPackets.NPCLink("Je vois.", 255));
                                                SendPacket(General.MyPackets.NPCSetFace(30));
                                                SendPacket(General.MyPackets.NPCFinish());
                                            }
                                        }
                                        if (Char.Model == 1003 || Char.Model == 1004)
                                        {
                                            if (MyChar.Model == 2001 || MyChar.Model == 2002)
                                            {
                                                Char.Spouse = MyChar.Name;
                                                MyChar.Spouse = Char.Name;
                                                MyChar.SaveSpouse();
                                                Char.SaveSpouse();
                                                SendPacket(General.MyPackets.CharacterInfo(MyChar));
                                                World.SendMsgToAll(Char.Name + " et " + MyChar.Name + " sont tomb�s dans le monde de cupidon! Vous devez vous d�connectez pour afficher!", "LoveStone", 2011);
                                            }
                                            else
                                            {
                                                SendPacket(General.MyPackets.NPCSay("Pas de mariage gay ici!"));
                                                SendPacket(General.MyPackets.NPCLink("Je vois.", 255));
                                                SendPacket(General.MyPackets.NPCSetFace(30));
                                                SendPacket(General.MyPackets.NPCFinish());
                                            }
                                        }
                                    }
                                }
                            }
                            if (AttackType == 21)
                            {
                                MyChar.PTarget = null;
                                MyChar.TGTarget = null;
                                MyChar.MobTarget = null;
                                MyChar.Attacking = false;
                                ushort SkillId = Convert.ToUInt16(((long)Data[24] & 0xFF) | (((long)Data[25] & 0xFF) << 8));
                                SkillId ^= (ushort)0x915d;
                                SkillId ^= (ushort)MyChar.UID;
                                SkillId = (ushort)(SkillId << 0x3 | SkillId >> 0xd);
                                SkillId -= 0xeb42;

                                long x = (Data[16] & 0xFF) | ((Data[17] & 0xFF) << 8);
                                long y = (Data[18] & 0xFF) | ((Data[19] & 0xFF) << 8);

                                x = x ^ (uint)(MyChar.UID & 0xffff) ^ 0x2ed6;
                                x = ((x << 1) | ((x & 0x8000) >> 15)) & 0xffff;
                                x |= 0xffff0000;
                                x -= 0xffff22ee;

                                y = y ^ (uint)(MyChar.UID & 0xffff) ^ 0xb99b;
                                y = ((y << 5) | ((y & 0xF800) >> 11)) & 0xffff;
                                y |= 0xffff0000;
                                y -= 0xffff8922;

                                uint Target = ((uint)Data[12] & 0xFF) | (((uint)Data[13] & 0xFF) << 8) | (((uint)Data[14] & 0xFF) << 16) | (((uint)Data[15] & 0xFF) << 24);
                                Target = ((((Target & 0xffffe000) >> 13) | ((Target & 0x1fff) << 19)) ^ 0x5F2D2463 ^ MyChar.UID) - 0x746F4AE6;

                                if (SkillId != 1110 && SkillId != 1015 && SkillId != 1020 && SkillId != 1025)
                                {
                                    if (MyChar.LocMap == 1039 || SkillId == 1002 || SkillId == 1000 && SkillId != 1110 && SkillId != 1015 && SkillId != 1020 && SkillId != 1025)
                                    {
                                        MyChar.SkillLooping = SkillId;
                                        MyChar.SkillLoopingX = (ushort)x;
                                        MyChar.SkillLoopingY = (ushort)y;
                                        MyChar.SkillLoopingTarget = Target;
                                        MyChar.AtkType = 21;
                                        MyChar.Attack();
                                        MyChar.Attacking = true;
                                    }
                                    else
                                        MyChar.UseSkill(SkillId, (ushort)x, (ushort)y, Target);
                                }
                                else
                                    MyChar.UseSkill(SkillId, (ushort)x, (ushort)y, Target);
                            }
                            if (AttackType == 2 || AttackType == 25)
                            {
                                uint Target = Data[0x0f];
                                Target = (Target << 8) | Data[0x0e];
                                Target = (Target << 8) | Data[0x0d];
                                Target = (Target << 8) | Data[0x0c];
                                MyChar.TargetUID = Target;

                                if (Target < 7000 && Target >= 5000)
                                {
                                    SingleNPC ThisTGO = (SingleNPC)NPCs.AllNPCs[Target];
                                    MyChar.MobTarget = null;

                                    MyChar.AtkType = (byte)AttackType;
                                    MyChar.TGTarget = ThisTGO;

                                }
                                /*else if (Target > 400000 && Target < 500000)
                                {
                                    SingleMob TargetMob = (SingleMob)Mobs.AllMobs[Target];

                                    MyChar.MobTarget = null;

                                    if (TargetMob != null)
                                    {
                                        MyChar.AtkType = (byte)AttackType;
                                        MyChar.MobTarget = TargetMob;
                                    }
                                }*/
                                else if (Target > 400000 && Target < 500000)
                                {
                                    if (MyChar.Guard != null)
                                    {
                                        if (Target != MyChar.Guard.UID)
                                        {
                                            SingleMob TargetMob = (SingleMob)Mobs.AllMobs[Target];

                                            MyChar.MobTarget = null;

                                            if (TargetMob != null)
                                            {
                                                MyChar.AtkType = (byte)AttackType;
                                                MyChar.MobTarget = TargetMob;
                                            }

                                        }
                                    }
                                    else
                                    {
                                        SingleMob TargetMob = (SingleMob)Mobs.AllMobs[Target];

                                        MyChar.MobTarget = null;

                                        if (TargetMob != null)
                                        {
                                            MyChar.AtkType = (byte)AttackType;
                                            MyChar.MobTarget = TargetMob;
                                        }


                                    }
                                }
                                else
                                {
                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character ThisChar = (Character)DE.Value;
                                        if (ThisChar.UID == Target)
                                        {
                                            MyChar.PTarget = ThisChar;
                                            MyChar.AtkType = (byte)AttackType;
                                            break;
                                        }
                                    }
                                }
                                if (MyChar.AtkType == 2 && MyChar.PTarget != null && MyChar.PTarget.Flying)
                                    MyChar.PTarget = null;

                                MyChar.Attacking = true;
                                MyChar.Attack();
                            }
                            MyChar.Ready = true;
                            break;
                        }
                    case 1027:
                        {
                            MyChar.Ready = false;
                            uint MainItemUID = (uint)((Data[11] << 24) + (Data[10] << 16) + (Data[9] << 8) + Data[8]);
                            uint GemUID = (uint)((Data[15] << 24) + (Data[14] << 16) + (Data[13] << 8) + Data[12]);
                            string RealItem = "";
                            string RealGem = "";

                            int Mode = Data[18];
                            int Slot = Data[16];
                            int Counter = 0;

                            if (Mode == 0)
                            {
                                if (GemUID != 0)
                                {
                                    foreach (ulong uid in MyChar.Inventory_UIDs)
                                    {
                                        if (GemUID == uid)
                                            RealGem = MyChar.Inventory[Counter];
                                        Counter++;
                                    }
                                }
                                Counter = 0;

                                if (MainItemUID != 0)
                                {
                                    foreach (ulong uid in MyChar.Inventory_UIDs)
                                    {
                                        if (MainItemUID == uid)
                                            RealItem = MyChar.Inventory[Counter];
                                        Counter++;
                                    }
                                }

                                if (RealItem != "")
                                    if (RealGem != "")
                                    {
                                        string[] ItemParts = RealItem.Split('-');
                                        string[] GemParts = RealGem.Split('-');
                                        if (Slot == 1)
                                        {
                                            ItemParts[4] = Convert.ToString(uint.Parse(GemParts[0]) - 700000);
                                        }
                                        if (Slot == 2)
                                        {
                                            ItemParts[5] = Convert.ToString(uint.Parse(GemParts[0]) - 700000);
                                        }
                                        MyChar.RemoveItem(MainItemUID);
                                        MyChar.RemoveItem(GemUID);
                                        MyChar.AddItem(ItemParts[0] + "-" + ItemParts[1] + "-" + ItemParts[2] + "-" + ItemParts[3] + "-" + ItemParts[4] + "-" + ItemParts[5], 0, MainItemUID);
                                    }
                            }
                            else
                            {
                                if (MainItemUID != 0)
                                {
                                    foreach (ulong uid in MyChar.Inventory_UIDs)
                                    {
                                        if (MainItemUID == uid)
                                            RealItem = MyChar.Inventory[Counter];
                                        Counter++;
                                    }
                                }
                                if (RealItem == null)
                                    return;
                                string[] ItemParts = RealItem.Split('-');
                                if (Slot == 1)
                                    if (ItemParts[4] != "0")
                                    {
                                        if (ItemParts[5] == "0" || ItemParts[5] == "255")
                                            ItemParts[4] = "255";
                                        else if (ItemParts[5] != "0")
                                        {
                                            ItemParts[4] = ItemParts[5];
                                            ItemParts[5] = "255";
                                        }
                                    }
                                if (Slot == 2)
                                    if (ItemParts[5] != "0")
                                    {
                                        ItemParts[5] = "255";
                                    }
                                MyChar.RemoveItem(MainItemUID);
                                MyChar.AddItem(ItemParts[0] + "-" + ItemParts[1] + "-" + ItemParts[2] + "-" + ItemParts[3] + "-" + ItemParts[4] + "-" + ItemParts[5], 0, MainItemUID);
                            }

                            if (Data.Length == 40)
                            {
                                MainItemUID = (uint)((Data[31] << 24) + (Data[30] << 16) + (Data[29] << 8) + Data[28]);
                                GemUID = (uint)((Data[35] << 24) + (Data[34] << 16) + (Data[33] << 8) + Data[32]);
                                RealItem = "";
                                RealGem = "";

                                Mode = Data[38];
                                Slot = Data[36];
                                Counter = 0;

                                if (Mode == 0)
                                {
                                    if (GemUID != 0)
                                    {
                                        foreach (ulong uid in MyChar.Inventory_UIDs)
                                        {
                                            if (GemUID == uid)
                                                RealGem = MyChar.Inventory[Counter];
                                            Counter++;
                                        }
                                    }
                                    Counter = 0;

                                    if (MainItemUID != 0)
                                    {
                                        foreach (ulong uid in MyChar.Inventory_UIDs)
                                        {
                                            if (MainItemUID == uid)
                                                RealItem = MyChar.Inventory[Counter];
                                            Counter++;
                                        }
                                    }

                                    if (RealItem != "")
                                        if (RealGem != "")
                                        {
                                            string[] ItemParts = RealItem.Split('-');
                                            string[] GemParts = RealGem.Split('-');
                                            if (Slot == 1)
                                            {
                                                ItemParts[4] = Convert.ToString(uint.Parse(GemParts[0]) - 700000);
                                            }
                                            if (Slot == 2)
                                            {
                                                ItemParts[5] = Convert.ToString(uint.Parse(GemParts[0]) - 700000);
                                            }
                                            MyChar.RemoveItem(MainItemUID);
                                            MyChar.RemoveItem(GemUID);
                                            MyChar.AddItem(ItemParts[0] + "-" + ItemParts[1] + "-" + ItemParts[2] + "-" + ItemParts[3] + "-" + ItemParts[4] + "-" + ItemParts[5], 0, MainItemUID);
                                        }
                                }
                                else
                                {
                                    string[] ItemParts = RealItem.Split('-');
                                    if (Slot == 1)
                                    {
                                        ItemParts[4] = "0";
                                    }
                                    if (Slot == 2)
                                    {
                                        ItemParts[5] = "0";
                                    }
                                    MyChar.RemoveItem(MainItemUID);
                                    MyChar.AddItem(ItemParts[0] + "-" + ItemParts[1] + "-" + ItemParts[2] + "-" + ItemParts[3] + "-" + ItemParts[4] + "-" + ItemParts[5], 0, MainItemUID);
                                }
                            }
                            MyChar.Ready = true;
                            break;
                        }

                    case 2031:
                        {
                            MyChar.Ready = false;
                            int NPCID = (Data[7] << 24) + (Data[6] << 16) + (Data[5] << 8) + Data[4];
                            SendPacket(General.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "Vous essayez de parler au NPC(ID: " + NPCID + ")", 2005));
                            int Control = (int)Data[10];
                            CurrentNPC = NPCID;
                            if (CurrentNPC == 9543)
                            {
                                if (MyChar.WHPWcheck == 1)
                                {
                                    SendPacket(General.MyPackets.NPCSay("Vous avez r�ussi � cr�er un mot de passe pour votre banque. Voulez-vous le changer?"));
                                    SendPacket(General.MyPackets.NPCLink("Je veux le changer.", 4));
                                    SendPacket(General.MyPackets.NPCLink("Laissez moi r�fl�chir.", 255));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                else
                                {
                                    SendPacket(General.MyPackets.NPCSay("Tout le monde qui mette leurs tr�sors dans leurs banques est � leur risque."));
                                    SendPacket(General.MyPackets.NPCSay(" Je vous offre la chance de mettre un mot de passe � votre banque."));
                                    SendPacket(General.MyPackets.NPCSay(" Que voulez-vous faire?"));
                                    SendPacket(General.MyPackets.NPCLink("Je veux mettre un mot de passe.", 1));
                                    SendPacket(General.MyPackets.NPCLink("Laissez moi r�fl�chir.", 255));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                            }
                            if (CurrentNPC == 127)
                            {
                                if (MyChar.RBCount >= 1)
                                {
                                    SendPacket(General.MyPackets.NPCSay("Vous avez d�j� reborn. Je ne peux plus vous aider."));
                                    SendPacket(General.MyPackets.NPCLink("Je vois.", 255));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                else
                                {
                                    if (MyChar.Level >= 120)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("J'ai pass� toute ma vie � chercher la vie �ternelle, finalement j'ai d�couvert la renaissance."));
                                        SendPacket(General.MyPackets.NPCLink("Je veux en savoir plus.", 1));
                                        SendPacket(General.MyPackets.NPCLink("Je fais que passer.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else if (MyChar.Level >= 110 && MyChar.Job == 135)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("J'ai pass� toute ma vie � chercher la vie �ternelle, finalement j'ai d�couvert la renaissance."));
                                        SendPacket(General.MyPackets.NPCLink("Je veux en savoir plus.", 1));
                                        SendPacket(General.MyPackets.NPCLink("Je fais que passer.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else if (MyChar.Level > 119)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous pouvez rena�tre si vous �tes 120 ou plus haut niveau."));
                                        SendPacket(General.MyPackets.NPCLink("Je vois.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                            }
                            if (CurrentNPC == 7001)
                            {

                            }
                            #region TC Monster Hunter Quest
                            if (CurrentNPC == 280)//TC CloudSaint'sJar Quest
                            {
                                if (MyChar.QuestFrom == "")
                                {
                                    if (MyChar.Level <= 24)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Heureux de voir que vous �tes ici! La Ville Dragon a �t� assi�g� par des monstres r�cemment. Si vous pouvez nous aider, votre r�compense sera d'une valeur d'EXP �quivalente � une ExpBall et un Meteor. Mais n'oubliez pas que vous pouvez essayer 3 fois par jour."));
                                        SendPacket(General.MyPackets.NPCLink("Allez tuer des Tourterelles (Level 7)", 1));
                                        SendPacket(General.MyPackets.NPCLink("Allez tuer des Rouge-Gorges (Level 12)", 2));
                                        SendPacket(General.MyPackets.NPCLink("Allez tuer des Apparitions (Level 17)", 3));
                                        SendPacket(General.MyPackets.NPCLink("Allez tuer des Fant�mes (Level 22)", 4));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else if (MyChar.Level <= 44)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous �tes maintenant puissant. Je vous sugg�re d'aller au capitaine de la Citadelle Ph�nix, parce que j'ai entendu qu'il �tait �nerv� � cause des monstres. Je crois que vous pouvez beaucoup l'aider. Je vais vous t�l�porter l� bas pour gagner du temps."));
                                        SendPacket(General.MyPackets.NPCLink("Je veux y aller.", 10));
                                        SendPacket(General.MyPackets.NPCLink("Je m'en vais.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else if (MyChar.Level <= 64)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous �tes maintenant puissant. Je vous sugg�re d'aller au capitaine de la Ville Tigre, parce que j'ai entendu qu'il �tait �nerv� � cause des monstres. Je crois que vous pouvez beaucoup l'aider. Je vais vous t�l�porter l� bas pour gagner du temps."));
                                        SendPacket(General.MyPackets.NPCLink("Je veux y aller.", 11));
                                        SendPacket(General.MyPackets.NPCLink("Je m'en vais.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else if (MyChar.Level <= 84)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous �tes maintenant puissant. Je vous sugg�re d'aller au capitaine de la Ville D�sert, parce que j'ai entendu qu'il �tait �nerv� � cause des monstres. Je crois que vous pouvez beaucoup l'aider. Je vais vous t�l�porter l� bas pour gagner du temps."));
                                        SendPacket(General.MyPackets.NPCLink("Je veux y aller.", 12));
                                        SendPacket(General.MyPackets.NPCLink("Je m'en vais.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else if (MyChar.Level <= 99)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous �tes maintenant puissant. Je vous sugg�re d'aller au capitaine de l'�le de Grue, parce que j'ai entendu qu'il �tait �nerv� � cause des monstres. Je crois que vous pouvez beaucoup l'aider. Je vais vous t�l�porter l� bas pour gagner du temps."));
                                        SendPacket(General.MyPackets.NPCLink("Je veux y aller.", 13));
                                        SendPacket(General.MyPackets.NPCLink("Je m'en vais.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else if (MyChar.Level <= 119)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous �tes maintenant puissant. Je vous sugg�re d'aller au capitaine de la Citadelle Ancienne, parce que j'ai entendu qu'il �tait �nerv� � cause des monstres. Je crois que vous pouvez l'aider beaucoup. Je vais vous t�l�porter l� pour gagner du temps."));
                                        SendPacket(General.MyPackets.NPCLink("Je veux y aller.", 14));
                                        SendPacket(General.MyPackets.NPCLink("Je m'en vais.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else if (MyChar.Level >= 120)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous avez rendu la paix � cette terre! Il ne reste plus rien � faire pour vous!"));
                                        SendPacket(General.MyPackets.NPCLink("Ok.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                                else if (MyChar.QuestFrom == "TC")
                                {
                                    if (MyChar.QuestKO >= 300 && MyChar.InventoryContains(750000, 1))
                                    {
                                        MyChar.RemoveItem(MyChar.ItemNext(750000));
                                        MyChar.QuestKO = 0;
                                        MyChar.QuestMob = "";
                                        MyChar.QuestFrom = "";
                                        MyChar.AddExp((ulong)(1295000 + MyChar.Level * 100000), false);
                                        MyChar.AddItem("1088001-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                        SendPacket(General.MyPackets.NPCSay("Bon travail! Vous avez tu� 300 Monstres! Voici votre Exp �quivalente � 2 ExpBalls et un Meteor."));
                                        SendPacket(General.MyPackets.NPCLink("Merci.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else if (MyChar.QuestKO >= 0)
                                    {
                                        if (MyChar.InventoryContains(750000, 1))
                                        {
                                            SendPacket(General.MyPackets.NPCSay("Pourquoi avez-vous h�te de revenir? Que s'est-il pass�? Qu'en est-il de votre qu�te pour tuer 300 Monstres? " + MyChar.QuestMob + "?"));
                                            SendPacket(General.MyPackets.NPCLink("Je vais le faire.", 255));
                                            SendPacket(General.MyPackets.NPCLink("Je veux mettre fin � la Qu�te.", 9));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                        else
                                        {
                                            SendPacket(General.MyPackets.NPCSay("H�las! O� est la gourde que je vous ai pr�t�? Si vous ne la trouvez pas, je vous sugg�re d'abandonner la recherche. Mais ne vous inqui�tez pas, vous pouvez recommencer la qu�te."));
                                            SendPacket(General.MyPackets.NPCLink("D�sol�, c'est ma faute.", 9));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                    }
                                }
                                else if (MyChar.QuestFrom == "PC")
                                {
                                    SendPacket(General.MyPackets.NPCSay("La qu�te que vous avez obtenu de PCCaptain n'a pas encore �t� termin�. S�il vous pla�t finissez la ou abandonner pour pouvoir avoir une nouvelle qu�te."));
                                    SendPacket(General.MyPackets.NPCLink("Je vais terminer tout de suite.", 255));
                                    SendPacket(General.MyPackets.NPCLink("Je veux mettre fin � la Qu�te.", 9));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                else if (MyChar.QuestFrom == "AC")
                                {
                                    SendPacket(General.MyPackets.NPCSay("La qu�te que vous avez obtenu de ACCaptain n'a pas encore �t� termin�. S�il vous pla�t finissez la ou abandonner pour pouvoir avoir une nouvelle qu�te."));
                                    SendPacket(General.MyPackets.NPCLink("Je vais terminer tout de suite.", 255));
                                    SendPacket(General.MyPackets.NPCLink("Je veux mettre fin � la Qu�te.", 9));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                else if (MyChar.QuestFrom == "DC")
                                {
                                    SendPacket(General.MyPackets.NPCSay("La qu�te que vous avez obtenu de DCCaptain n'a pas encore �t� termin�. S�il vous pla�t finissez la ou abandonner pour pouvoir avoir une nouvelle qu�te."));
                                    SendPacket(General.MyPackets.NPCLink("Je vais terminer tout de suite.", 255));
                                    SendPacket(General.MyPackets.NPCLink("Je veux mettre fin � la Qu�te.", 9));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                else if (MyChar.QuestFrom == "BI")
                                {
                                    SendPacket(General.MyPackets.NPCSay("La qu�te que vous avez obtenu de BICaptain n'a pas encore �t� termin�. S�il vous pla�t finissez la ou abandonner pour pouvoir avoir une nouvelle qu�te."));
                                    SendPacket(General.MyPackets.NPCLink("Je vais terminer tout de suite.", 255));
                                    SendPacket(General.MyPackets.NPCLink("Je veux mettre fin � la Qu�te.", 9));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                else if (MyChar.QuestFrom == "MC")
                                {
                                    SendPacket(General.MyPackets.NPCSay("La qu�te que vous avez obtenu de MCCaptain n'a pas encore �t� termin�. S�il vous pla�t finissez la ou abandonner pour pouvoir avoir une nouvelle qu�te."));
                                    SendPacket(General.MyPackets.NPCLink("Je vais terminer tout de suite.", 255));
                                    SendPacket(General.MyPackets.NPCLink("Je veux mettre fin � la Qu�te.", 9));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                            }
                            #endregion
                            #region PC Monster Hunter Quest
                            if (CurrentNPC == 281)//PC CloudSaint'sJar Quest
                            {
                                if (MyChar.QuestFrom == "")
                                {
                                    if (MyChar.Level <= 24)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous �tes puissant maintenant. Je vous sugg�re d'aller au capitaine de Dragon, parce que j'ai entendu qu'il �tait �nerv� � cause des monstres. Je crois que vous pouvez beaucoup l'aider. Je vais vous t�l�porter l� bas pour gagner du temps."));
                                        SendPacket(General.MyPackets.NPCLink("Je veux y aller.", 9));
                                        SendPacket(General.MyPackets.NPCLink("Je m'en vais.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else if (MyChar.Level <= 44)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Heureux de voir que vous �tes ici! La Citadelle Ph�nix a �t� assi�g� par des monstres r�cemment. Si vous pouvez nous aider, votre r�compense sera d'une valeur �quivalente � une EXP ExpBall et un Meteor. Mais n'oubliez pas que vous pouvez essayer 3 fois par jour."));
                                        SendPacket(General.MyPackets.NPCLink("Allez tuer SerpentAil�s (Level 27)", 1));
                                        SendPacket(General.MyPackets.NPCLink("Allez tuer Bandits (Level 32)", 2));
                                        SendPacket(General.MyPackets.NPCLink("Allez tuer RatDeFeus (Level 42)", 3));
                                        SendPacket(General.MyPackets.NPCLink("Allez tuer EspritDuFeus (Level 47)", 4));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else if (MyChar.Level <= 64)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous �tes maintenant puissant. Je vous sugg�re d'aller au capitaine de la Ville Tigre, parce que j'ai entendu qu'il �tait �nerv� � cause des monstres. Je crois que vous pouvez beaucoup l'aider. Je vais vous t�l�porter l� bas pour gagner du temps."));
                                        SendPacket(General.MyPackets.NPCLink("Je veux y aller.", 11));
                                        SendPacket(General.MyPackets.NPCLink("Je m'en vais.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else if (MyChar.Level <= 84)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous �tes maintenant puissant. Je vous sugg�re d'aller au capitaine de la Ville D�sert, parce que j'ai entendu qu'il �tait �nerv� � cause des monstres. Je crois que vous pouvez beaucoup l'aider. Je vais vous t�l�porter l� bas pour gagner du temps."));
                                        SendPacket(General.MyPackets.NPCLink("Je veux y aller.", 12));
                                        SendPacket(General.MyPackets.NPCLink("Je m'en vais.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else if (MyChar.Level <= 99)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous �tes maintenant puissant. Je vous sugg�re d'aller au capitaine de l'�le de Grue, parce que j'ai entendu qu'il �tait �nerv� � cause des monstres. Je crois que vous pouvez beaucoup l'aider. Je vais vous t�l�porter l� bas pour gagner du temps."));
                                        SendPacket(General.MyPackets.NPCLink("Je veux y aller.", 13));
                                        SendPacket(General.MyPackets.NPCLink("Je m'en vais.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else if (MyChar.Level <= 119)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous �tes maintenant puissant. Je vous sugg�re d'aller au capitaine de la Citadelle Ancienne, parce que j'ai entendu qu'il �tait �nerv� � cause des monstres. Je crois que vous pouvez l'aider beaucoup. Je vais vous t�l�porter l� pour gagner du temps."));
                                        SendPacket(General.MyPackets.NPCLink("Je veux y aller.", 14));
                                        SendPacket(General.MyPackets.NPCLink("Je m'en vais.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else if (MyChar.Level >= 120)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous avez rendu la paix � cette terre! Il ne reste plus rien � faire pour vous!"));
                                        SendPacket(General.MyPackets.NPCLink("Ok.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                                else if (MyChar.QuestFrom == "PC")
                                {
                                    if (MyChar.QuestKO >= 300 && MyChar.InventoryContains(750000, 1))
                                    {
                                        MyChar.RemoveItem(MyChar.ItemNext(750000));
                                        MyChar.QuestKO = 0;
                                        MyChar.QuestMob = "";
                                        MyChar.QuestFrom = "";
                                        MyChar.AddExp((ulong)(1295000 + MyChar.Level * 50000), false);
                                        MyChar.AddItem("1088001-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                        SendPacket(General.MyPackets.NPCSay("Bon travail! Vous avez tu� 300 Monstres! Voici votre Exp et Meteor."));
                                        SendPacket(General.MyPackets.NPCLink("Merci.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else if (MyChar.QuestKO >= 0)
                                    {
                                        if (MyChar.InventoryContains(750000, 1))
                                        {
                                            SendPacket(General.MyPackets.NPCSay("Pourquoi avez-vous h�te de revenir? Que s'est-il pass�? Qu'en est-il de votre qu�te pour tuer 300 monstres? " + MyChar.QuestMob + "?"));
                                            SendPacket(General.MyPackets.NPCLink("Je vais le faire.", 255));
                                            SendPacket(General.MyPackets.NPCLink("Je veux mettre fin � la Qu�te.", 9));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                        else
                                        {
                                            SendPacket(General.MyPackets.NPCSay("H�las! O� est la gourde que je vous ai pr�t�? Si vous ne le trouvez pas, je vous sugg�re d'abandonner la recherche. Mais ne vous inqui�tez pas, vous pouvez recommencer la qu�te."));
                                            SendPacket(General.MyPackets.NPCLink("D�sol�, c'est ma faute.", 9));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                    }
                                }
                                else if (MyChar.QuestFrom == "TC")
                                {
                                    SendPacket(General.MyPackets.NPCSay("La qu�te que vous avez obtenu de TCCaptain n'a pas encore �t� termin�. S�il vous pla�t finissez la ou abandonner pour pouvoir avoir une nouvelle qu�te."));
                                    SendPacket(General.MyPackets.NPCLink("Je vais terminer tout de suite.", 255));
                                    SendPacket(General.MyPackets.NPCLink("Je veux mettre fin � la Qu�te.", 9));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                else if (MyChar.QuestFrom == "AC")
                                {
                                    SendPacket(General.MyPackets.NPCSay("La qu�te que vous avez obtenu de ACCaptain n'a pas encore �t� termin�. S�il vous pla�t finissez la ou abandonner pour pouvoir avoir une nouvelle qu�te."));
                                    SendPacket(General.MyPackets.NPCLink("Je vais terminer tout de suite.", 255));
                                    SendPacket(General.MyPackets.NPCLink("Je veux mettre fin � la Qu�te.", 9));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                else if (MyChar.QuestFrom == "DC")
                                {
                                    SendPacket(General.MyPackets.NPCSay("La qu�te que vous avez obtenu de DCCaptain n'a pas encore �t� termin�. S�il vous pla�t finissez la ou abandonner pour pouvoir avoir une nouvelle qu�te."));
                                    SendPacket(General.MyPackets.NPCLink("Je vais terminer tout de suite.", 255));
                                    SendPacket(General.MyPackets.NPCLink("Je veux mettre fin � la Qu�te.", 9));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                else if (MyChar.QuestFrom == "BI")
                                {
                                    SendPacket(General.MyPackets.NPCSay("La qu�te que vous avez obtenu de BICaptain n'a pas encore �t� termin�. S�il vous pla�t finissez la ou abandonner pour pouvoir avoir une nouvelle qu�te."));
                                    SendPacket(General.MyPackets.NPCLink("Je vais terminer tout de suite.", 255));
                                    SendPacket(General.MyPackets.NPCLink("Je veux mettre fin � la Qu�te.", 9));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                else if (MyChar.QuestFrom == "MC")
                                {
                                    SendPacket(General.MyPackets.NPCSay("La qu�te que vous avez obtenu de MCCaptain n'a pas encore �t� termin�. S�il vous pla�t finissez la ou abandonner pour pouvoir avoir une nouvelle qu�te."));
                                    SendPacket(General.MyPackets.NPCLink("Je vais terminer tout de suite.", 255));
                                    SendPacket(General.MyPackets.NPCLink("Je veux mettre fin � la Qu�te.", 9));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                            }
                            #endregion
                            #region AC Monster Hunter Quest
                            if (CurrentNPC == 282)//AC CloudSaint'sJar Quest
                            {
                                if (MyChar.QuestFrom == "")
                                {
                                    if (MyChar.Level <= 24)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous �tes puissant maintenant. Je vous sugg�re d'aller au capitaine de Dragon, parce que j'ai entendu qu'il �tait �nerv� � cause des monstres. Je crois que vous pouvez beaucoup l'aider. Je vais vous t�l�porter l� bas pour gagner du temps."));
                                        SendPacket(General.MyPackets.NPCLink("Je veux y aller.", 9));
                                        SendPacket(General.MyPackets.NPCLink("Je m'en vais.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else if (MyChar.Level <= 44)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous �tes puissant maintenant. Je vous sugg�re d'aller au capitaine de la Citadelle Ph�nix, parce que j'ai entendu qu'il �tait �nerv� � cause des monstres. Je crois que vous pouvez beaucoup l'aider. Je vais vous t�l�porter l� bas pour gagner du temps."));
                                        SendPacket(General.MyPackets.NPCLink("Je veux y aller.", 10));
                                        SendPacket(General.MyPackets.NPCLink("Je m'en vais.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else if (MyChar.Level <= 64)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Heureux de voir que vous �tes ici! Ape Mountain a �t� assi�g� par des monstres r�cemment. Si vous pouvez nous aider, votre r�compense sera d'une valeur d'EXP �quivalente � une ExpBall et un Meteor. Mais n'oubliez pas que vous pouvez essayer 3 fois pas jour."));
                                        SendPacket(General.MyPackets.NPCLink("Allez tuer Macaques (Level 47)", 1));
                                        SendPacket(General.MyPackets.NPCLink("Allez tuer Gorilles (Level 52)", 2));
                                        SendPacket(General.MyPackets.NPCLink("Allez tuer GibbonFoudres (Level 57)", 3));
                                        SendPacket(General.MyPackets.NPCLink("Allez tuer HommeSerpents (Level 62)", 4));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else if (MyChar.Level <= 84)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous �tes maintenant puissant. Je vous sugg�re d'aller au capitaine de la Ville D�sert, parce que j'ai entendu qu'il �tait �nerv� � cause des monstres. Je crois que vous pouvez beaucoup l'aider. Je vais vous t�l�porter l� bas pour gagner du temps."));
                                        SendPacket(General.MyPackets.NPCLink("Je veux y aller.", 12));
                                        SendPacket(General.MyPackets.NPCLink("Je m'en vais.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else if (MyChar.Level <= 99)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous �tes maintenant puissant. Je vous sugg�re d'aller au capitaine de l'�le de Grue, parce que j'ai entendu qu'il �tait �nerv� � cause des monstres. Je crois que vous pouvez beaucoup l'aider. Je vais vous t�l�porter l� bas pour gagner du temps."));
                                        SendPacket(General.MyPackets.NPCLink("Je veux y aller.", 13));
                                        SendPacket(General.MyPackets.NPCLink("Je m'en vais.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else if (MyChar.Level <= 119)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous �tes maintenant puissant. Je vous sugg�re d'aller au capitaine de la Citadelle Ancienne, parce que j'ai entendu qu'il �tait �nerv� � cause des monstres. Je crois que vous pouvez l'aider beaucoup. Je vais vous t�l�porter l� pour gagner du temps."));
                                        SendPacket(General.MyPackets.NPCLink("Je veux y aller.", 14));
                                        SendPacket(General.MyPackets.NPCLink("Je m'en vais.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else if (MyChar.Level >= 120)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous avez rendu la paix � cette terre! Il ne reste plus rien � faire pour vous!"));
                                        SendPacket(General.MyPackets.NPCLink("Ok.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                                else if (MyChar.QuestFrom == "AC")
                                {
                                    if (MyChar.QuestKO >= 300 && MyChar.InventoryContains(750000, 1))
                                    {
                                        MyChar.RemoveItem(MyChar.ItemNext(750000));
                                        MyChar.QuestKO = 0;
                                        MyChar.QuestMob = "";
                                        MyChar.QuestFrom = "";
                                        MyChar.AddExp((ulong)(1295000 + MyChar.Level * 50000), false);
                                        MyChar.AddItem("1088001-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                        SendPacket(General.MyPackets.NPCSay("Bon travail! Vous avez tu� 300 Monstres! Voici votre Exp et Meteor."));
                                        SendPacket(General.MyPackets.NPCLink("Merci.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else if (MyChar.QuestKO >= 0)
                                    {
                                        if (MyChar.InventoryContains(750000, 1))
                                        {
                                            SendPacket(General.MyPackets.NPCSay("Pourquoi avez-vous h�te de revenir? Que s'est-il pass�? Qu'en est-il de votre qu�te pour tuer 300 monstres? " + MyChar.QuestMob + "?"));
                                            SendPacket(General.MyPackets.NPCLink("Je vais le faire.", 255));
                                            SendPacket(General.MyPackets.NPCLink("Je veux mettre fin � la Qu�te.", 9));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                        else
                                        {
                                            SendPacket(General.MyPackets.NPCSay("H�las! O� est la gourde que je vous ai pr�t�? Si vous ne le trouvez pas, je vous sugg�re d'abandonner la recherche. Mais ne vous inqui�tez pas, vous pouvez recommencer la qu�te de nouveau."));
                                            SendPacket(General.MyPackets.NPCLink("D�sol�, c'est ma faute.", 9));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                    }
                                }
                                else if (MyChar.QuestFrom == "TC")
                                {
                                    SendPacket(General.MyPackets.NPCSay("La qu�te que vous avez obtenu de TCCaptain n'a pas encore �t� termin�. S�il vous pla�t finissez la ou abandonner pour pouvoir avoir une nouvelle qu�te."));
                                    SendPacket(General.MyPackets.NPCLink("Je vais terminer tout de suite.", 255));
                                    SendPacket(General.MyPackets.NPCLink("Je veux mettre fin � la Qu�te.", 9));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                else if (MyChar.QuestFrom == "PC")
                                {
                                    SendPacket(General.MyPackets.NPCSay("La qu�te que vous avez obtenu de PCCaptain n'a pas encore �t� termin�. S�il vous pla�t finissez la ou abandonner pour pouvoir avoir une nouvelle qu�te."));
                                    SendPacket(General.MyPackets.NPCLink("Je vais terminer tout de suite.", 255));
                                    SendPacket(General.MyPackets.NPCLink("Je veux mettre fin � la Qu�te.", 9));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                else if (MyChar.QuestFrom == "DC")
                                {
                                    SendPacket(General.MyPackets.NPCSay("La qu�te que vous avez obtenu de DCCaptain n'a pas encore �t� termin�. S�il vous pla�t finissez la ou abandonner pour pouvoir avoir une nouvelle qu�te."));
                                    SendPacket(General.MyPackets.NPCLink("Je vais terminer tout de suite.", 255));
                                    SendPacket(General.MyPackets.NPCLink("Je veux mettre fin � la Qu�te.", 9));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                else if (MyChar.QuestFrom == "BI")
                                {
                                    SendPacket(General.MyPackets.NPCSay("La qu�te que vous avez obtenu de BICaptain n'a pas encore �t� termin�. S�il vous pla�t finissez ou abandonner pour pouvoir avoir une nouvelle qu�te."));
                                    SendPacket(General.MyPackets.NPCLink("Je vais terminer tout de suite.", 255));
                                    SendPacket(General.MyPackets.NPCLink("Je veux mettre fin � la Qu�te.", 9));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                else if (MyChar.QuestFrom == "MC")
                                {
                                    SendPacket(General.MyPackets.NPCSay("La qu�te que vous avez obtenu de MCCaptain n'a pas encore �t� termin�. S�il vous pla�t finissez la ou abandonner pour pouvoir avoir une nouvelle qu�te."));
                                    SendPacket(General.MyPackets.NPCLink("Je vais terminer tout de suite.", 255));
                                    SendPacket(General.MyPackets.NPCLink("Je veux mettre fin � la Qu�te.", 9));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                            }
                            #endregion
                            #region DC Monster Hunter Quest
                            if (CurrentNPC == 283)//DC CloudSaint'sJar Quest
                            {
                                if (MyChar.QuestFrom == "")
                                {
                                    if (MyChar.Level <= 24)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous �tes puissant maintenant. Je vous sugg�re d'aller au capitaine de Dragon, parce que j'ai entendu qu'il �tait �nerv� � cause des monstres. Je crois que vous pouvez beaucoup l'aider. Je vais vous t�l�porter l� bas pour gagner du temps."));
                                        SendPacket(General.MyPackets.NPCLink("Je veux y aller.", 9));
                                        SendPacket(General.MyPackets.NPCLink("Je m'en vais.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else if (MyChar.Level <= 44)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous �tes puissant maintenant. Je vous sugg�re d'aller au capitaine de la Citadelle Ph�nix, parce que j'ai entendu qu'il �tait �nerv� � cause des monstres. Je crois que vous pouvez beaucoup l'aider. Je vais vous t�l�porter l� bas pour gagner du temps."));
                                        SendPacket(General.MyPackets.NPCLink("Je veux y aller.", 10));
                                        SendPacket(General.MyPackets.NPCLink("Je m'en vais.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else if (MyChar.Level <= 64)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous �tes maintenant puissant. Je vous sugg�re d'aller au capitaine de la Ville Tigre, parce que j'ai entendu qu'il �tait �nerv� � cause des monstres. Je crois que vous pouvez beaucoup l'aider. Je vais vous t�l�porter l� bas pour gagner du temps."));
                                        SendPacket(General.MyPackets.NPCLink("Je veux y aller.", 11));
                                        SendPacket(General.MyPackets.NPCLink("Je m'en vais.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else if (MyChar.Level <= 84)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Heureux de voir que vous �tes ici! La Ville D�sert a �t� assi�g�e par des monstres r�cemment. Si vous pouvez nous aider, votre r�compense sera d'une valeur d'EXP �quivalente � une ExpBall et un Meteor. Mais n'oubliez pas que vous pouvez essayer 3 fois par jour."));
                                        SendPacket(General.MyPackets.NPCLink("Allez tuer MonstreSables (Level 67)", 1));
                                        SendPacket(General.MyPackets.NPCLink("Allez tuer MonstreHerbes (Level 72)", 2));
                                        SendPacket(General.MyPackets.NPCLink("Allez tuer MonstreRocs (Level 77)", 3));
                                        SendPacket(General.MyPackets.NPCLink("Allez tuer Fant�meSangs (Level 82)", 4));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else if (MyChar.Level <= 99)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous �tes maintenant puissant. Je vous sugg�re d'aller au capitaine de l'�le de Grue, parce que j'ai entendu qu'il �tait �nerv� � cause des monstres. Je crois que vous pouvez beaucoup l'aider. Je vais vous t�l�porter l� bas pour gagner du temps."));
                                        SendPacket(General.MyPackets.NPCLink("Je veux y aller.", 13));
                                        SendPacket(General.MyPackets.NPCLink("Je m'en vais.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else if (MyChar.Level <= 119)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous �tes maintenant puissant. Je vous sugg�re d'aller au capitaine de la Citadelle Ancienne, parce que j'ai entendu qu'il �tait �nerv� � cause des monstres. Je crois que vous pouvez l'aider beaucoup. Je vais vous t�l�porter l� pour gagner du temps."));
                                        SendPacket(General.MyPackets.NPCLink("Je veux y aller.", 14));
                                        SendPacket(General.MyPackets.NPCLink("Je m'en vais.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else if (MyChar.Level >= 120)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous avez rendu la paix � cette terre! Il ne reste plus rien � faire pour vous!"));
                                        SendPacket(General.MyPackets.NPCLink("Ok.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                                else if (MyChar.QuestFrom == "DC")
                                {
                                    if (MyChar.QuestKO >= 300 && MyChar.InventoryContains(750000, 1))
                                    {
                                        MyChar.RemoveItem(MyChar.ItemNext(750000));
                                        MyChar.QuestKO = 0;
                                        MyChar.QuestMob = "";
                                        MyChar.QuestFrom = "";
                                        MyChar.AddExp((ulong)(1295000 + MyChar.Level * 50000), false);
                                        MyChar.AddItem("1088001-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                        SendPacket(General.MyPackets.NPCSay("Bon travail! Vous avez tu� 300 Monstres! Voici votre Exp et Meteor."));
                                        SendPacket(General.MyPackets.NPCLink("Merci.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else if (MyChar.QuestKO >= 0)
                                    {
                                        if (MyChar.InventoryContains(750000, 1))
                                        {
                                            SendPacket(General.MyPackets.NPCSay("Pourquoi avez-vous h�te de revenir? Que s'est-il pass�? Qu'en est-il de votre qu�te pour tuer 300 monstres? " + MyChar.QuestMob + "?"));
                                            SendPacket(General.MyPackets.NPCLink("Je vais le faire.", 255));
                                            SendPacket(General.MyPackets.NPCLink("Je veux mettre fin � la Qu�te.", 9));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                        else
                                        {
                                            SendPacket(General.MyPackets.NPCSay("H�las! O� est la gourde que je vous ai pr�t�? Si vous ne le trouvez pas, je vous sugg�re d'abandonner la recherche. Mais ne vous inqui�tez pas, vous pouvez recommencer la qu�te de nouveau."));
                                            SendPacket(General.MyPackets.NPCLink("D�sol�, c'est ma faute.", 9));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                    }
                                }
                                else if (MyChar.QuestFrom == "TC")
                                {
                                    SendPacket(General.MyPackets.NPCSay("La qu�te que vous avez obtenu de TCCaptain n'a pas encore �t� termin�. S�il vous pla�t finissez la ou abandonner pour pouvoir avoir une nouvelle qu�te."));
                                    SendPacket(General.MyPackets.NPCLink("Je vais terminer tout de suite.", 255));
                                    SendPacket(General.MyPackets.NPCLink("Je veux mettre fin � la Qu�te.", 9));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                else if (MyChar.QuestFrom == "PC")
                                {
                                    SendPacket(General.MyPackets.NPCSay("La qu�te que vous avez obtenu de PCCaptain n'a pas encore �t� termin�. S�il vous pla�t finissez la ou abandonner pour pouvoir avoir une nouvelle qu�te."));
                                    SendPacket(General.MyPackets.NPCLink("Je vais terminer tout de suite.", 255));
                                    SendPacket(General.MyPackets.NPCLink("Je veux mettre fin � la Qu�te.", 9));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                else if (MyChar.QuestFrom == "AC")
                                {
                                    SendPacket(General.MyPackets.NPCSay("La qu�te que vous avez obtenu de ACCaptain n'a pas encore �t� termin�. S�il vous pla�t finissez la ou abandonner pour pouvoir avoir une nouvelle qu�te."));
                                    SendPacket(General.MyPackets.NPCLink("Je vais terminer tout de suite.", 255));
                                    SendPacket(General.MyPackets.NPCLink("Je veux mettre fin � la Qu�te.", 9));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                else if (MyChar.QuestFrom == "BI")
                                {
                                    SendPacket(General.MyPackets.NPCSay("La qu�te que vous avez obtenu de BICaptain n'a pas encore �t� termin�. S�il vous pla�t finissez la ou abandonner pour pouvoir avoir une nouvelle qu�te."));
                                    SendPacket(General.MyPackets.NPCLink("Je vais terminer tout de suite.", 255));
                                    SendPacket(General.MyPackets.NPCLink("Je veux mettre fin � la Qu�te.", 9));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                else if (MyChar.QuestFrom == "MC")
                                {
                                    SendPacket(General.MyPackets.NPCSay("La qu�te que vous avez obtenu de MCCaptain n'a pas encore �t� termin�. S�il vous pla�t finissez la ou abandonner pour pouvoir avoir une nouvelle qu�te."));
                                    SendPacket(General.MyPackets.NPCLink("Je vais terminer tout de suite.", 255));
                                    SendPacket(General.MyPackets.NPCLink("Je veux mettre fin � la Qu�te.", 9));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                            }
                            #endregion
                            #region BI Monster Hunter Quest
                            if (CurrentNPC == 284)//BI CloudSaint'sJar Quest
                            {
                                if (MyChar.QuestFrom == "")
                                {
                                    if (MyChar.Level <= 24)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous �tes puissant maintenant. Je vous sugg�re d'aller au capitaine de Dragon, parce que j'ai entendu qu'il �tait �nerv� � cause des monstres. Je crois que vous pouvez beaucoup l'aider. Je vais vous t�l�porter l� bas pour gagner du temps."));
                                        SendPacket(General.MyPackets.NPCLink("Je veux y aller.", 9));
                                        SendPacket(General.MyPackets.NPCLink("Je m'en vais.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else if (MyChar.Level <= 44)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous �tes puissant maintenant. Je vous sugg�re d'aller au capitaine de la Citadelle Ph�nix, parce que j'ai entendu qu'il �tait �nerv� � cause des monstres. Je crois que vous pouvez beaucoup l'aider. Je vais vous t�l�porter l� bas pour gagner du temps."));
                                        SendPacket(General.MyPackets.NPCLink("Je veux y aller.", 10));
                                        SendPacket(General.MyPackets.NPCLink("Je m'en vais.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else if (MyChar.Level <= 64)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous �tes maintenant puissant. Je vous sugg�re d'aller au capitaine de la Ville Tigre, parce que j'ai entendu qu'il �tait �nerv� � cause des monstres. Je crois que vous pouvez beaucoup l'aider. Je vais vous t�l�porter l� bas pour gagner du temps."));
                                        SendPacket(General.MyPackets.NPCLink("Je veux y aller.", 11));
                                        SendPacket(General.MyPackets.NPCLink("Je m'en vais.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else if (MyChar.Level <= 84)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous �tes puissant maintenant. Je vous sugg�re d'aller au capitaine de la Ville D�sert, parce que j'ai entendu qu'il �tait �nerv� � cause des monstres. Je crois que vous pouvez beaucoup l'aider. Je vais vous t�l�porter l� bas pour gagner du temps."));
                                        SendPacket(General.MyPackets.NPCLink("Je veux y aller.", 12));
                                        SendPacket(General.MyPackets.NPCLink("Je m'en vais.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else if (MyChar.Level <= 99)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Heureux de voir que vous �tes ici! L'�le de Grue a �t� assi�g�e par des monstres r�cemment. Si vous pouvez nous aider, votre r�compense sera d'une valeur d'EXP �quivalente � une ExpBall et un Meteor. Mais n'oubliez pas que vous pouvez essayer 3 fois par jour."));
                                        SendPacket(General.MyPackets.NPCLink("Allez tuer HommeOiseaux (Level 87)", 1));
                                        SendPacket(General.MyPackets.NPCLink("Allez tuer RoiAigles (Level 92)", 2));
                                        SendPacket(General.MyPackets.NPCLink("Allez tuer BanditL97 (Level 97)", 3));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else if (MyChar.Level <= 119)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous �tes maintenant puissant. Je vous sugg�re d'aller au capitaine de la Citadelle Ancienne, parce que j'ai entendu qu'il �tait �nerv� � cause des monstres. Je crois que vous pouvez l'aider beaucoup. Je vais vous t�l�porter l� pour gagner du temps."));
                                        SendPacket(General.MyPackets.NPCLink("Je veux y aller.", 14));
                                        SendPacket(General.MyPackets.NPCLink("Je m'en vais.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else if (MyChar.Level >= 120)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous avez rendu la paix � cette terre! Il ne reste plus rien � faire pour vous!"));
                                        SendPacket(General.MyPackets.NPCLink("Ok.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                                else if (MyChar.QuestFrom == "BI")
                                {
                                    if (MyChar.QuestKO >= 300 && MyChar.InventoryContains(750000, 1))
                                    {
                                        MyChar.RemoveItem(MyChar.ItemNext(750000));
                                        MyChar.QuestKO = 0;
                                        MyChar.QuestMob = "";
                                        MyChar.QuestFrom = "";
                                        MyChar.AddExp((ulong)(1295000 + MyChar.Level * 50000), false);
                                        MyChar.AddItem("1088001-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                        SendPacket(General.MyPackets.NPCSay("Bon travail! Vous avez tu� 300 Monstres! Voici votre Exp et Meteor."));
                                        SendPacket(General.MyPackets.NPCLink("Merci.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else if (MyChar.QuestKO >= 0)
                                    {
                                        if (MyChar.InventoryContains(750000, 1))
                                        {
                                            SendPacket(General.MyPackets.NPCSay("Pourquoi avez-vous h�te de revenir? Que s'est-il pass�? Qu'en est-il de votre qu�te pour tuer 300 monstres? " + MyChar.QuestMob + "?"));
                                            SendPacket(General.MyPackets.NPCLink("Je vais le faire.", 255));
                                            SendPacket(General.MyPackets.NPCLink("Je veux mettre fin � la Qu�te.", 9));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                        else
                                        {
                                            SendPacket(General.MyPackets.NPCSay("H�las! O� est la gourde que je vous ai pr�t�? Si vous ne le trouvez pas, je vous sugg�re d'abandonner la recherche. Mais ne vous inqui�tez pas, vous pouvez recommencer la qu�te de nouveau."));
                                            SendPacket(General.MyPackets.NPCLink("D�sol�, c'est ma faute.", 9));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                    }
                                }
                                else if (MyChar.QuestFrom == "TC")
                                {
                                    SendPacket(General.MyPackets.NPCSay("La qu�te que vous avez obtenu de TCCaptain n'a pas encore �t� termin�. S�il vous pla�t finissez la ou abandonner pour pouvoir avoir une nouvelle qu�te."));
                                    SendPacket(General.MyPackets.NPCLink("Je vais terminer tout de suite.", 255));
                                    SendPacket(General.MyPackets.NPCLink("Je veux mettre fin � la Qu�te.", 9));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                else if (MyChar.QuestFrom == "PC")
                                {
                                    SendPacket(General.MyPackets.NPCSay("La qu�te que vous avez obtenu de PCCaptain n'a pas encore �t� termin�. S�il vous pla�t finissez ou abandonner pour pouvoir avoir une nouvelle qu�te."));
                                    SendPacket(General.MyPackets.NPCLink("Je vais terminer tout de suite.", 255));
                                    SendPacket(General.MyPackets.NPCLink("Je veux mettre fin � la Qu�te.", 9));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                else if (MyChar.QuestFrom == "AC")
                                {
                                    SendPacket(General.MyPackets.NPCSay("La qu�te que vous avez obtenu de ACCaptain n'a pas encore �t� termin�. S�il vous pla�t finissez ou abandonner pour pouvoir avoir une nouvelle qu�te."));
                                    SendPacket(General.MyPackets.NPCLink("Je vais terminer tout de suite.", 255));
                                    SendPacket(General.MyPackets.NPCLink("Je veux mettre fin � la Qu�te.", 9));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                else if (MyChar.QuestFrom == "DC")
                                {
                                    SendPacket(General.MyPackets.NPCSay("La qu�te que vous avez obtenu de DCCaptain n'a pas encore �t� termin�. S�il vous pla�t finissez ou abandonner pour pouvoir avoir une nouvelle qu�te."));
                                    SendPacket(General.MyPackets.NPCLink("Je vais terminer tout de suite.", 255));
                                    SendPacket(General.MyPackets.NPCLink("Je veux mettre fin � la Qu�te.", 9));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                else if (MyChar.QuestFrom == "MC")
                                {
                                    SendPacket(General.MyPackets.NPCSay("La qu�te que vous avez obtenu de MCCaptain n'a pas encore �t� termin�. S�il vous pla�t finissez ou abandonner pour pouvoir avoir une nouvelle qu�te."));
                                    SendPacket(General.MyPackets.NPCLink("Je vais terminer tout de suite.", 255));
                                    SendPacket(General.MyPackets.NPCLink("Je veux mettre fin � la Qu�te.", 9));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                            }
                            #endregion
                            #region MC Monster Hunter Quest
                            if (CurrentNPC == 285)//MC CloudSaint'sJar Quest
                            {
                                if (MyChar.QuestFrom == "")
                                {
                                    if (MyChar.Level <= 24)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous �tes puissant maintenant. Je vous sugg�re d'aller au capitaine de Dragon, parce que j'ai entendu qu'il �tait �nerv� � cause des monstres. Je crois que vous pouvez beaucoup l'aider. Je vais vous t�l�porter l� bas pour gagner du temps."));
                                        SendPacket(General.MyPackets.NPCLink("Je veux y aller.", 9));
                                        SendPacket(General.MyPackets.NPCLink("Je m'en vais.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else if (MyChar.Level <= 44)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous �tes puissant maintenant. Je vous sugg�re d'aller au capitaine de la Citadelle Ph�nix, parce que j'ai entendu qu'il �tait �nerv� � cause des monstres. Je crois que vous pouvez beaucoup l'aider. Je vais vous t�l�porter l� bas pour gagner du temps."));
                                        SendPacket(General.MyPackets.NPCLink("Je veux y aller.", 10));
                                        SendPacket(General.MyPackets.NPCLink("Je m'en vais.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else if (MyChar.Level <= 64)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous �tes maintenant puissant. Je vous sugg�re d'aller au capitaine de la Ville Tigre, parce que j'ai entendu qu'il �tait �nerv� � cause des monstres. Je crois que vous pouvez beaucoup l'aider. Je vais vous t�l�porter l� bas pour gagner du temps."));
                                        SendPacket(General.MyPackets.NPCLink("Je veux y aller.", 11));
                                        SendPacket(General.MyPackets.NPCLink("Je m'en vais.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else if (MyChar.Level <= 84)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous �tes puissant maintenant. Je vous sugg�re d'aller au capitaine de la Ville D�sert, parce que j'ai entendu qu'il �tait �nerv� � cause des monstres. Je crois que vous pouvez beaucoup l'aider. Je vais vous t�l�porter l� bas pour gagner du temps."));
                                        SendPacket(General.MyPackets.NPCLink("Je veux y aller.", 12));
                                        SendPacket(General.MyPackets.NPCLink("Je m'en vais.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else if (MyChar.Level <= 99)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous �tes maintenant puissant. Je vous sugg�re d'aller au capitaine de l'�le de Grue, parce que j'ai entendu qu'il �tait �nerv� � cause des monstres. Je crois que vous pouvez beaucoup l'aider. Je vais vous t�l�porter l� bas pour gagner du temps."));
                                        SendPacket(General.MyPackets.NPCLink("Je veux y aller.", 13));
                                        SendPacket(General.MyPackets.NPCLink("Je m'en vais.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else if (MyChar.Level <= 119)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Heureux de voir que vous �tes ici! Mystic Castle a �t� assi�g� par des monstres r�cemment. Si vous pouvez nous aider, votre r�compense sera d'une valeur d'EXP �quivalente � une ExpBall et un Meteor. Mais n'oubliez pas que vous pouvez essayer 3 fois par jour."));
                                        SendPacket(General.MyPackets.NPCLink("Allez tuer BatTombales (Level 102)", 1));
                                        SendPacket(General.MyPackets.NPCLink("Allez tuer BatSanglants (Level 107)", 2));
                                        SendPacket(General.MyPackets.NPCLink("Allez tuer MonstreTaureaux (Level 112)", 3));
                                        SendPacket(General.MyPackets.NPCLink("Allez tuer D�monRougeL117 (Level 117)", 4));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else if (MyChar.Level >= 120)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous avez rendu la pais � cette terre! Il ne reste plus rien � faire pour vous!"));
                                        SendPacket(General.MyPackets.NPCLink("Ok.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                                else if (MyChar.QuestFrom == "MC")
                                {
                                    if (MyChar.QuestKO >= 300 && MyChar.InventoryContains(750000, 1))
                                    {
                                        MyChar.RemoveItem(MyChar.ItemNext(750000));
                                        MyChar.QuestKO = 0;
                                        MyChar.QuestMob = "";
                                        MyChar.QuestFrom = "";
                                        MyChar.AddExp((ulong)(1295000 + MyChar.Level * 50000), false);
                                        MyChar.AddItem("1088001-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                        SendPacket(General.MyPackets.NPCSay("Bon travail! Vous avez tu� 300 Monstres! Voici votre Exp et Meteor."));
                                        SendPacket(General.MyPackets.NPCLink("Merci.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else if (MyChar.QuestKO >= 0)
                                    {
                                        if (MyChar.InventoryContains(750000, 1))
                                        {
                                            SendPacket(General.MyPackets.NPCSay("Pourquoi avez-vous h�te de revenir? Que s'est-il pass�? Qu'en est-il de votre qu�te pour tuer 300 monstres? " + MyChar.QuestMob + "?"));
                                            SendPacket(General.MyPackets.NPCLink("Je vais le faire.", 255));
                                            SendPacket(General.MyPackets.NPCLink("Je veux mettre fin � la Qu�te.", 9));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                        else
                                        {
                                            SendPacket(General.MyPackets.NPCSay("H�las! O� est la gourde que je vous ai pr�t�? Si vous ne le trouvez pas, je vous sugg�re d'abandonner la recherche. Mais ne vous inqui�tez pas, vous pouvez recommencer la qu�te de nouveau."));
                                            SendPacket(General.MyPackets.NPCLink("D�sol�, c'est ma faute.", 9));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                    }
                                }
                                else if (MyChar.QuestFrom == "TC")
                                {
                                    SendPacket(General.MyPackets.NPCSay("La qu�te vous avez obtenu de TCCaptain n'a pas encore �t� termin�. S�il vous pla�t finissez la ou abandonner pour pouvoir avoir une nouvelle quete."));
                                    SendPacket(General.MyPackets.NPCLink("Je vais terminer tout de suite.", 255));
                                    SendPacket(General.MyPackets.NPCLink("Je veux mettre fin � la Qu�te.", 9));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                else if (MyChar.QuestFrom == "PC")
                                {
                                    SendPacket(General.MyPackets.NPCSay("La qu�te vous avez obtenu de PCCaptain n'a pas encore �t� termin�. S�il vous pla�t finissez la ou abandonner pour pouvoir avoir une nouvelle quete."));
                                    SendPacket(General.MyPackets.NPCLink("Je vais terminer tout de suite.", 255));
                                    SendPacket(General.MyPackets.NPCLink("Je veux mettre fin � la Qu�te.", 9));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                else if (MyChar.QuestFrom == "AC")
                                {
                                    SendPacket(General.MyPackets.NPCSay("La qu�te vous avez obtenu de ACCaptain n'a pas encore �t� termin�. S�il vous pla�t finissez la ou abandonner pour pouvoir avoir une nouvelle quete."));
                                    SendPacket(General.MyPackets.NPCLink("Je vais terminer tout de suite.", 255));
                                    SendPacket(General.MyPackets.NPCLink("Je veux mettre fin � la Qu�te.", 9));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                else if (MyChar.QuestFrom == "DC")
                                {
                                    SendPacket(General.MyPackets.NPCSay("La qu�te vous avez obtenu de DCCaptain n'a pas encore �t� termin�. S�il vous pla�t finissez la ou abandonner pour pouvoir avoir une nouvelle quete."));
                                    SendPacket(General.MyPackets.NPCLink("Je vais terminer tout de suite.", 255));
                                    SendPacket(General.MyPackets.NPCLink("Je veux mettre fin � la Qu�te.", 9));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                else if (MyChar.QuestFrom == "BI")
                                {
                                    SendPacket(General.MyPackets.NPCSay("La qu�te vous avez obtenu de BICaptain n'a pas encore �t� termin�. S�il vous pla�t finissez la ou abandonner pour pouvoir avoir une nouvelle quete."));
                                    SendPacket(General.MyPackets.NPCLink("Je vais terminer tout de suite.", 255));
                                    SendPacket(General.MyPackets.NPCLink("Je veux mettre fin � la Qu�te.", 9));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                            }
                            #endregion
                            #region Meteor Tears
                            /*if (CurrentNPC == 128)
                            {
                                if (MyChar.InventoryContains(721000, 1))
                                {
                                    SendPacket(General.MyPackets.NPCSay("Ma soeur est dans l'�le de Grue."));
                                    SendPacket(General.MyPackets.NPCLink("Je vois", 255));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }


                                else
                                {
                                    SendPacket(General.MyPackets.NPCSay("Nos sentiments sont tr�s profonds, et nous nous sommes aim�s. Quand il est parti je me suis senti tr�s triste."));
                                    SendPacket(General.MyPackets.NPCLink("Que puis-je faire?", 1));
                                    SendPacket(General.MyPackets.NPCLink("C'est vraiment triste.", 255));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());


                                }
                            }
                            if (CurrentNPC == 129)
                            {
                                if (MyChar.InventoryContains(721000, 1))
                                {
                                    SendPacket(General.MyPackets.NPCSay("I feel good or bad I need to live and I can not leave aside my feelings by Jorge."));
                                    SendPacket(General.MyPackets.NPCLink("That you worried about??", 1));
                                    SendPacket(General.MyPackets.NPCLink("That good words", 255));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                else
                                {
                                    if (MyChar.InventoryContains(721002, 1))
                                    {
                                        SendPacket(General.MyPackets.NPCSay("A gift from my boyfriend?. Thanks! Recompensar� you with a tear of meteor for help."));
                                        SendPacket(General.MyPackets.NPCLink("Thanks.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                        if (MyChar.InventoryContains(721002, 1))
                                        {
                                            MyChar.RemoveItem(MyChar.ItemNext(721002));
                                            MyChar.AddItem("1088002-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                        }
                                    }


                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("I feel good or bad I need to live and I can not leave aside my feelings by Jorge."));
                                        SendPacket(General.MyPackets.NPCLink("That good words", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                            }
                            if (CurrentNPC == 130)
                            {

                                if (MyChar.InventoryContains(1000030, 1))
                                {


                                    SendPacket(General.MyPackets.NPCSay("It is very hot, you got me wine?."));
                                    SendPacket(General.MyPackets.NPCLink("Nectar I Want? ", 1));
                                    SendPacket(General.MyPackets.NPCLink("No, I do not have", 255));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());

                                }


                                else
                                {
                                    SendPacket(General.MyPackets.NPCSay("It is very hot, you got me wine?."));
                                    SendPacket(General.MyPackets.NPCLink("No, sorry", 255));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                            }*/
                            #endregion
                            if (CurrentNPC == 2565)
                            {
                                SendPacket(General.MyPackets.NPCSay("C'est No�l, j'ai une grande puissance et je vais vous donner la chance d'avoir un niveau de plus!"));
                                SendPacket(General.MyPackets.NPCLink("Je n'ai pas encore re�u mon cadeau!", 1));
                                SendPacket(General.MyPackets.NPCLink("J'ai d�j� re�u mon cadeau.", 255));
                                SendPacket(General.MyPackets.NPCSetFace(197));
                                SendPacket(General.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 3010)
                            {
                                if (MyChar.QuestNB < 5)
                                {
                                    if (MyChar.InventoryContains(721636, 1))
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous avez obtenu la Gra�ne f�licitation! Vous avez sauv� notre grande f�te! Pour vous remercier voici une r�compense!"));
                                        SendPacket(General.MyPackets.NPCLink("Merci", 2));
                                        SendPacket(General.MyPackets.NPCSetFace(6));
                                        SendPacket(General.MyPackets.NPCFinish());
                                        MyChar.QuestNB++;
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Bient�t, une nouvelle ann�e commencera. Comme les anciennes traditions, les parents donneront des cadeaux � leurs enfants. Par contre, cette ann�e, un terrible"));
                                        SendPacket(General.MyPackets.NPCSay(" malheur est arriv�! Les monstres qui ont attaqu� et pill� la Ville Dragon ont vol� les cadeaux! Aidez moi et je vous r�compenserez."));
                                        SendPacket(General.MyPackets.NPCLink("Je veux vous aider.", 1));
                                        SendPacket(General.MyPackets.NPCLink("Je n'est pas le temps!", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(6));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                            }
                            if (CurrentNPC == 3020)
                            {
                                if (MyChar.InventoryContains(721634, 1))
                                {
                                    SendPacket(General.MyPackets.NPCSay("Je vais vous aider � lire cette lettre."));
                                    SendPacket(General.MyPackets.NPCLink("Merci.", 1));
                                    SendPacket(General.MyPackets.NPCLink("Je ne veux pas.", 255));
                                    SendPacket(General.MyPackets.NPCSetFace(6));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                            }
                            if (CurrentNPC == 3030)
                            {
                                SendPacket(General.MyPackets.NPCSay("Je n'ai pas eu le temps d'acheter des cadeaux. Si vous me donnez 10 cadeaux pour mes enfants, je vous dirai o� trouver la Gra�ne."));
                                SendPacket(General.MyPackets.NPCLink("J'ai les cadeaux.", 1));
                                SendPacket(General.MyPackets.NPCLink("Je reviens!", 255));
                                SendPacket(General.MyPackets.NPCSetFace(200));
                                SendPacket(General.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 390)
                            {
                                    SendPacket(General.MyPackets.NPCSay("Voulez-vous proposer votre amour?"));
                                    SendPacket(General.MyPackets.NPCSay("Rappelez-vous, ce mariage est s�rieux."));
                                    SendPacket(General.MyPackets.NPCLink("Oui, je veux proposer", 1));
                                    SendPacket(General.MyPackets.NPCLink("Je ne suis pas pr�s.", 255));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 9999)
                            {
                                if (MyChar.Spouse == "" || MyChar.Spouse == "Non")
                                {
                                    SendPacket(General.MyPackets.NPCSay("Hey! Vous n'�tes pas mari�!"));
                                    SendPacket(General.MyPackets.NPCLink("Je sais.", 255));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                else
                                {
                                    SendPacket(General.MyPackets.NPCSay("Les �toiles dans le ciel repr�sentent la vrai amour..."));
                                    SendPacket(General.MyPackets.NPCSay(" Malheureusement certaines �toiles ne brillent pas assez et nous devons les d�truire."));
                                    SendPacket(General.MyPackets.NPCSay(" Le mariage repr�sente c'est �toile. Je peux vous divorcer avec votre femme si votre"));
                                    SendPacket(General.MyPackets.NPCSay(" amour n'est plus assez grande."));
                                    SendPacket(General.MyPackets.NPCLink("Je veux divorcer.", 1));
                                    SendPacket(General.MyPackets.NPCLink("Je suis heureux.", 255));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                            }
                            if (CurrentNPC == 260) // ArenaGuard
                            {
                                SendPacket(General.MyPackets.NPCSay("Je suis le Gardin de l'ar�ne. Voulez-vous rentrer? Il va vous en couter 50$."));
                                SendPacket(General.MyPackets.NPCLink("Oui je veux rentrer.", 1));
                                SendPacket(General.MyPackets.NPCLink("Non, pas maintenant.", 255));
                                SendPacket(General.MyPackets.NPCSetFace(30));
                                SendPacket(General.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 7500)
                            {
                                SendPacket(General.MyPackets.NPCSay("Je peux vous donner 215 CPs pour chaque Perle de Dragon que vous me donnerez. Aussi je peux vous �changer 100000$ contre 10 CPs. Finalement je peux vous �changer 1000000$ contre 100 CPs."));
                                SendPacket(General.MyPackets.NPCLink("Oui, je veux des CPs contre ma Perle de Dragon!", 1));
                                SendPacket(General.MyPackets.NPCLink("Oui, je veux des CPs contre mon argent!", 2));
                                SendPacket(General.MyPackets.NPCLink("Oui, je veux des CPs contre mon milion!", 3));
                                SendPacket(General.MyPackets.NPCLink("Je n'ai pas de Perle de Dragon", 255));
                                SendPacket(General.MyPackets.NPCSetFace(30));
                                SendPacket(General.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 8487)
                            {
                                SendPacket(General.MyPackets.NPCSay("Vous avez un probl�me avec votre inventaire? Je peux vous le n�ttoyer si vous voulez."));
                                SendPacket(General.MyPackets.NPCLink("Oui, vider mon inventaire S.V.P", 1));
                                SendPacket(General.MyPackets.NPCLink("Non, je n'ai pas de probl�me", 255));
                                SendPacket(General.MyPackets.NPCSetFace(30));
                                SendPacket(General.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 9812)
                            {
                                SendPacket(General.MyPackets.NPCSay("Avoir des m�t�ors et des Perles de Dragon est exitant. Mais votre inventaire est rapidement plein."));
                                SendPacket(General.MyPackets.NPCLink("Oui, je vous comprends. Que me proposez-vous?", 1));
                                SendPacket(General.MyPackets.NPCLink("Je suis pauvre je n'ai pas ce probl�me.", 255));
                                SendPacket(General.MyPackets.NPCSetFace(30));
                                SendPacket(General.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 6700)
                            {
                                uint HealPole = 0;
                                foreach (DictionaryEntry DE in NPCs.AllNPCs)
                                {
                                    SingleNPC Npcc = (SingleNPC)DE.Value;
                                    if (Npcc.Sob == 2)
                                    {
                                        HealPole = Npcc.MaxHP - Npcc.CurHP;
                                    }
                                }
                                SendPacket(General.MyPackets.NPCSay("Voulez-vous redonner de la vie au Pole? Il va vous en couter " + HealPole + "$. L'argent sera retir� des fonds de votre guilde."));
                                SendPacket(General.MyPackets.NPCLink("Oui", 1));
                                SendPacket(General.MyPackets.NPCLink("Non", 255));
                                SendPacket(General.MyPackets.NPCSetFace(30));
                                SendPacket(General.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 6701 || CurrentNPC == 6702)
                            {
                                SendPacket(General.MyPackets.NPCSay("Que voulez-vous faire?"));
                                SendPacket(General.MyPackets.NPCLink("Ouvrir/Fermer la porte.", 1));
                                SendPacket(General.MyPackets.NPCSetFace(30));
                                SendPacket(General.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 1152) //Simon
                            {
                                SendPacket(General.MyPackets.NPCSay("Les grandes r�compenses attirent beaucoup de personnes courageuses. Je recherche les personnes courageuses pour m'aider � rapporter mon patrimoine. Pouvez-vous m'aider ? Les r�compenses sont belles."));
                                SendPacket(General.MyPackets.NPCLink("Pouvez-vous m'en dire plus?", 1));
                                SendPacket(General.MyPackets.NPCLink("Quelles r�compenses?", 2));
                                SendPacket(General.MyPackets.NPCLink("J'ai les DiamantDeSoleils", 3));
                                SendPacket(General.MyPackets.NPCLink("J'ai les DiamantDeLunes", 4));
                                SendPacket(General.MyPackets.NPCLink("J'ai les DiamantD`�toiles", 5));
                                SendPacket(General.MyPackets.NPCLink("J'ai les DiamantDeNuages", 6));
                                SendPacket(General.MyPackets.NPCLink("Je n'ai rien...", 255));
                                SendPacket(General.MyPackets.NPCSetFace(26));
                                SendPacket(General.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 1153)//Lab 2 General
                            {
                                SendPacket(General.MyPackets.NPCSay("Je suis le G�n�ral de l'Ouest. Ici c'est le deuxi�me �tage du labyrinthe. Si vous voulez aller au prochain �tage il me faut une PreuveDeLaTerre. Sinon, voulez-vous retourner � la Ville Dragon?"));
                                SendPacket(General.MyPackets.NPCLink("Au prochain �tage.", 1));
                                SendPacket(General.MyPackets.NPCLink("� la Ville Dragon.", 2));
                                SendPacket(General.MyPackets.NPCLink("Je veux rester ici.", 255));
                                SendPacket(General.MyPackets.NPCSetFace(30));
                                SendPacket(General.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 1154)//Lab 3 General
                            {
                                SendPacket(General.MyPackets.NPCSay("Je suis le G�n�ral du Sud. Ici c'est le troisi�me �tage du labyrinthe. Si vous voulez aller au prochain �tage il me faut une PreuveD`Ame. Sinon, voulez-vous retourner � la Ville Dragon?"));
                                SendPacket(General.MyPackets.NPCLink("Au prochain �tage.", 1));
                                SendPacket(General.MyPackets.NPCLink("� la Ville Dragon.", 2));
                                SendPacket(General.MyPackets.NPCLink("Je veux rester ici.", 255));
                                SendPacket(General.MyPackets.NPCSetFace(30));
                                SendPacket(General.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 1234)//Lab 1 General
                            {
                                SendPacket(General.MyPackets.NPCSay("Je suis le G�n�ral de l'Est. Ici c'est le premier �tage du labyrinthe. Si vous voulez aller au prochain �tage il me faut une PreuveDuCiel. Sinon, voulez-vous retourner � la Ville Dragon?"));
                                SendPacket(General.MyPackets.NPCLink("Au prochain �tage.", 1));
                                SendPacket(General.MyPackets.NPCLink("� la Ville Dragon.", 2));
                                SendPacket(General.MyPackets.NPCLink("Je veux rester ici.", 255));
                                SendPacket(General.MyPackets.NPCSetFace(30));
                                SendPacket(General.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 5555)//npc to check the virtue points
                            {
                                SendPacket(General.MyPackets.NPCSay("Bonjour, en quoi puis-je vous aider ?"));
                                SendPacket(General.MyPackets.NPCLink("Combien ais-je de points de vertue ?", 1));
                                SendPacket(General.MyPackets.NPCLink("Je voudrais un prix", 2));
                                SendPacket(General.MyPackets.NPCLink("Je passais juste par l�...", 255));
                                SendPacket(General.MyPackets.NPCSetFace(30));
                                SendPacket(General.MyPackets.NPCFinish());
                            }

                            if (CurrentNPC == 2410)
                            {
                                SendPacket(General.MyPackets.NPCSay("Voulez-vous changer votre taille? C'est un grand cadeau que je vous fais."));
                                SendPacket(General.MyPackets.NPCLink("Je veux changer ma taille.", 1));
                                SendPacket(General.MyPackets.NPCLink("Je ne veux pas changer.", 255));
                                SendPacket(General.MyPackets.NPCSetFace(30));
                                SendPacket(General.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 8460)
                            {
                                SendPacket(General.MyPackets.NPCSay("Pour une db je peux vous remettre vos points comme si vous �tiez niveau 1 et vous pourrez distribuer les autres."));
                                SendPacket(General.MyPackets.NPCLink("Je veux redistribuer mes points.", 1));
                                SendPacket(General.MyPackets.NPCLink("Je ne veux pas.", 255));
                                SendPacket(General.MyPackets.NPCSetFace(30));
                                SendPacket(General.MyPackets.NPCFinish());
                            } 
                            if (CurrentNPC == 959)
                            {
                                SendPacket(General.MyPackets.NPCSay("Je suis ici pour composer vos gemmes. 15 gemmes normals pour une raffin� et 20 gemmes raffin�s pour 1 superbe."));
                                SendPacket(General.MyPackets.NPCLink("Je veux composer une gemme Ph�nix.", 1));
                                SendPacket(General.MyPackets.NPCLink("Je veux composer une gemme Dragon.", 2));
                                SendPacket(General.MyPackets.NPCLink("Je veux composer une gemme Fureur.", 3));
                                SendPacket(General.MyPackets.NPCLink("Je veux composer une gemme Arc en Ciel.", 4));
                                SendPacket(General.MyPackets.NPCLink("Je veux composer une gemme Ivoire.", 5));
                                SendPacket(General.MyPackets.NPCLink("Je veux composer une gemme Violet.", 6));
                                SendPacket(General.MyPackets.NPCLink("Je veux composer une gemme de Lune.", 7));
                                SendPacket(General.MyPackets.NPCLink("Je veux composer une gemme Mythique.", 8));
                                SendPacket(General.MyPackets.NPCSetFace(30));
                                SendPacket(General.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 999)
                            {
                                UppAgree = false;
                                SendPacket(General.MyPackets.NPCSay("Je consacre toute ma vie � am�liorer les �quipements. Je souhaite que ma cause peut vous aidez � devenir c�l�bre. Que voulez-vous faire?"));
                                SendPacket(General.MyPackets.NPCLink("Pouvez-vous m'en dire plus?", 1));
                                SendPacket(General.MyPackets.NPCLink("Arme/Arc/Glaive.", 2));
                                SendPacket(General.MyPackets.NPCLink("Collier/Sac.", 3));
                                SendPacket(General.MyPackets.NPCLink("Bague/Bracelet.", 4));
                                SendPacket(General.MyPackets.NPCLink("Botte.", 5));
                                SendPacket(General.MyPackets.NPCLink("Armure/Robe.", 6));
                                SendPacket(General.MyPackets.NPCLink("Casque/Boucle.", 7));
                                SendPacket(General.MyPackets.NPCSetFace(30));
                                SendPacket(General.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 700)
                            {
                                SendPacket(General.MyPackets.NPCSay("M�me si l'Artisant du Vent peut faire un bon boulot, il ne r�ussi pas � tous les coups. Je ne veux pas voir le peuple perdre tout espoir. Donc j'ai d�cid� de les aider. J'ai besoin de plus d'objet mais je vous promet de r�ussir."));
                                SendPacket(General.MyPackets.NPCLink("Am�liorer la qualit�.", 1));
                                SendPacket(General.MyPackets.NPCLink("Am�liorer le niveau.", 2));
                                SendPacket(General.MyPackets.NPCSetFace(30));
                                SendPacket(General.MyPackets.NPCFinish());
                            }

                            if (CurrentNPC < 618 && CurrentNPC > 613)
                            {
                                SendPacket(General.MyPackets.NPCSay("Voulez-vous rentrer au si�ge central des guildes dans la ville Dragon?."));
                                SendPacket(General.MyPackets.NPCLink("Je veux y aller.", 1));
                                SendPacket(General.MyPackets.NPCLink("Je ne veux pas encore rentrer.", 255));
                                SendPacket(General.MyPackets.NPCSetFace(30));
                                SendPacket(General.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC < 614 && CurrentNPC > 609)
                            {
                                SendPacket(General.MyPackets.NPCSay("Voulez-vous aller � la transmission suivante de celle-ci?."));
                                SendPacket(General.MyPackets.NPCLink("Oui, je veux y aller.", 1));
                                SendPacket(General.MyPackets.NPCLink("Je veux acheter une transmission.", 2));
                                SendPacket(General.MyPackets.NPCLink("Je ne veux pas encore rentrer.", 255));
                                SendPacket(General.MyPackets.NPCSetFace(30));
                                SendPacket(General.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 601)
                            {
                                SendPacket(General.MyPackets.NPCSay("Voulez-vous sortir d'ici?"));
                                SendPacket(General.MyPackets.NPCLink("Oui.", 1));
                                SendPacket(General.MyPackets.NPCLink("Non.", 255));
                                SendPacket(General.MyPackets.NPCSetFace(30));
                                SendPacket(General.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 600)
                            {
                                SendPacket(General.MyPackets.NPCSay("Qu'est-ce que je peux faire pour vous?"));
                                SendPacket(General.MyPackets.NPCLink("Je veux entrer dans la carte de guilde.", 1));
                                SendPacket(General.MyPackets.NPCLink("Notre guilde a gagn� la GW je veux notre prix.", 2));
                                SendPacket(General.MyPackets.NPCLink("Rien.", 255));
                                SendPacket(General.MyPackets.NPCSetFace(30));
                                SendPacket(General.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 573)
                            {
                                SendPacket(General.MyPackets.NPCSay("Voulez-vous entrer dans la mine de la ville Dragon?."));
                                SendPacket(General.MyPackets.NPCLink("Oui, bien sure!", 1));
                                SendPacket(General.MyPackets.NPCLink("Non merci.", 255));
                                SendPacket(General.MyPackets.NPCSetFace(30));
                                SendPacket(General.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 198)
                            {
                                SendPacket(General.MyPackets.NPCSay("C'est la route pour la ville du D�sert. Vous �tes fort mais cette endroit est vraiment dangereux."));
                                SendPacket(General.MyPackets.NPCLink("Je veux quand m�me y aller!", 1));
                                SendPacket(General.MyPackets.NPCSetFace(30));
                                SendPacket(General.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC >= 104 && CurrentNPC <= 107)
                            {
                                SendPacket(General.MyPackets.NPCSay("O� voulez-vous aller? Je peux vous t�l�porter pour 100$."));
                                SendPacket(General.MyPackets.NPCLink("Ville Dragon", 1));
                                if (MyChar.LocMap == 1000)
                                    SendPacket(General.MyPackets.NPCLink("Citadelle Ancienne", 3));
                                SendPacket(General.MyPackets.NPCLink("March�", 2));
                                SendPacket(General.MyPackets.NPCLink("Je fais juste passer.", 255));
                                SendPacket(General.MyPackets.NPCSetFace(134));
                                SendPacket(General.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 180)
                            {
                                SendPacket(General.MyPackets.NPCSay("Voulez vous sortir du terrain d'entrainnement ?"));
                                SendPacket(General.MyPackets.NPCLink("Oui.", 1));
                                SendPacket(General.MyPackets.NPCLink("Non, Je passais juste par l�.", 2));
                                SendPacket(General.MyPackets.NPCSetFace(30));
                                SendPacket(General.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 155)
                            {
                                SendPacket(General.MyPackets.NPCSay("Je suis en charge de toutes les guildes. Vous devez me consulter avant de faire n'importe quoi."));
                                SendPacket(General.MyPackets.NPCLink("Cr�er une guilde", 1));
                                SendPacket(General.MyPackets.NPCLink("Mettre un Sous-Chef", 3));
                                SendPacket(General.MyPackets.NPCLink("D�grouper ma guilde", 5));
                                SendPacket(General.MyPackets.NPCSetFace(30));
                                SendPacket(General.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC < 506 && CurrentNPC > 499)
                            {
                                SendPacket(General.MyPackets.NPCSay("Voulez-vous choisir cette bo�te?"));
                                SendPacket(General.MyPackets.NPCLink("Oui.", 1));
                                SendPacket(General.MyPackets.NPCLink("Non.", 255));
                                SendPacket(General.MyPackets.NPCSetFace(30));
                                SendPacket(General.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC < 10013 && CurrentNPC > 9999)
                            {
                                SendPacket(General.MyPackets.NPCSay("Voulez-vous choisir cette bo�te?"));
                                SendPacket(General.MyPackets.NPCLink("Oui.", 1));
                                SendPacket(General.MyPackets.NPCLink("Non.", 255));
                                SendPacket(General.MyPackets.NPCSetFace(30));
                                SendPacket(General.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC < 10020 && CurrentNPC > 10014)
                            {
                                SendPacket(General.MyPackets.NPCSay("Voulez-vous choisir cette bo�te?"));
                                SendPacket(General.MyPackets.NPCLink("Oui.", 1));
                                SendPacket(General.MyPackets.NPCLink("Non.", 255));
                                SendPacket(General.MyPackets.NPCSetFace(30));
                                SendPacket(General.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 10020)
                            {
                                SendPacket(General.MyPackets.NPCSay("Voulez-vous choisir cette bo�te?"));
                                SendPacket(General.MyPackets.NPCLink("Oui.", 1));
                                SendPacket(General.MyPackets.NPCLink("Non.", 255));
                                SendPacket(General.MyPackets.NPCSetFace(30));
                                SendPacket(General.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 1846)
                            {
                                if (MyChar.LotoCount < 30)
                                {
                                    SendPacket(General.MyPackets.NPCSay("Voulez vous tenter votre chance � la lotterie ? Il vous en co�tera 27 cps.	"));
                                    SendPacket(General.MyPackets.NPCLink("Oui pourquoi pas.", 1));
                                    SendPacket(General.MyPackets.NPCLink("Non c'est trop cher...", 255));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                else
                                {
                                    SendPacket(General.MyPackets.NPCSay("Vous �tes d�j� entr� 30 fois aujourd'hui!"));
                                    SendPacket(General.MyPackets.NPCLink("Je vois...", 255));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                            }
                            if (CurrentNPC == 9997)
                            {
                                SendPacket(General.MyPackets.NPCSay("Je suis le gardien du PKTour voulez-vous entrer?"));
                                SendPacket(General.MyPackets.NPCLink("Oui, je veux rentrer", 1));
                                SendPacket(General.MyPackets.NPCLink("Non, je suis faible", 255));
                                SendPacket(General.MyPackets.NPCSetFace(30));
                                SendPacket(General.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 9996)
                            {
                                SendPacket(General.MyPackets.NPCSay("Il est l'heure de passer � la 2e carte. Voulez-vous entrer?"));
                                SendPacket(General.MyPackets.NPCLink("Oui, je veux rentrer", 1));
                                SendPacket(General.MyPackets.NPCLink("Non, je suis faible", 255));
                                SendPacket(General.MyPackets.NPCSetFace(30));
                                SendPacket(General.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 9995)
                            {
                                SendPacket(General.MyPackets.NPCSay("Il est l'heure de passer � la 3e carte. Voulez-vous entrer?"));
                                SendPacket(General.MyPackets.NPCLink("Oui, je veux rentrer", 1));
                                SendPacket(General.MyPackets.NPCLink("Non, je suis faible", 255));
                                SendPacket(General.MyPackets.NPCSetFace(30));
                                SendPacket(General.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 9994)
                            {
                                SendPacket(General.MyPackets.NPCSay("Si vous-�tes le gagnant du tournois vous pouvez r�clamer le prix. L'�tes-vous?"));
                                SendPacket(General.MyPackets.NPCLink("Oui", 1));
                                SendPacket(General.MyPackets.NPCLink("Non", 255));
                                SendPacket(General.MyPackets.NPCSetFace(30));
                                SendPacket(General.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 9993)
                            {
                                SendPacket(General.MyPackets.NPCSay("Si vous-�tes le gagnant du tournois vous pouvez r�clamer le prix. L'�tes-vous?"));
                                SendPacket(General.MyPackets.NPCLink("Oui", 1));
                                SendPacket(General.MyPackets.NPCLink("Non", 255));
                                SendPacket(General.MyPackets.NPCSetFace(30));
                                SendPacket(General.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 9992)
                            {
                                SendPacket(General.MyPackets.NPCSay("Si vous-�tes le gagnant du tournois vous pouvez r�clamer le prix. L'�tes-vous?"));
                                SendPacket(General.MyPackets.NPCLink("Oui", 1));
                                SendPacket(General.MyPackets.NPCLink("Non", 255));
                                SendPacket(General.MyPackets.NPCSetFace(30));
                                SendPacket(General.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 3825)
                            {
                                SendPacket(General.MyPackets.NPCSay("Je ne peux plus vous aider... On m'interdit de vous aider maintenant..."));
                                SendPacket(General.MyPackets.NPCLink("Je vois...", 255));
                                SendPacket(General.MyPackets.NPCSetFace(6));
                                SendPacket(General.MyPackets.NPCFinish());
                                /*if (MyChar.Level >= 40)
                                {
                                    SendPacket(General.MyPackets.NPCSay("La Perle de Dragon est un objet rare qui peut am�liorer le niveau et la"));
                                    SendPacket(General.MyPackets.NPCSay(" qualit� des �quipements, peut vous aider � am�liorer rapidement votre"));
                                    SendPacket(General.MyPackets.NPCSay(" niveau."));
                                    SendPacket(General.MyPackets.NPCLink("Je voudrais en savoir plus.", 1));
                                    SendPacket(General.MyPackets.NPCLink("Excellent.", 255));
                                    SendPacket(General.MyPackets.NPCSetFace(6));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                else
                                {
                                    SendPacket(General.MyPackets.NPCSay("Vous devez �tre au moins niveau 40 pour pouvoir utiliser l'�nergie de la Perle de Dragon."));
                                    SendPacket(General.MyPackets.NPCLink("Je vois.", 255));
                                    SendPacket(General.MyPackets.NPCSetFace(6));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }*/
                            }
                            if (CurrentNPC == 1275)
                            {
                                SendPacket(General.MyPackets.NPCSay("Voulez-vous rentrer en prison? Il vous faut 10000$ pour entrer.."));
                                SendPacket(General.MyPackets.NPCLink("Oui, aucun probl�me", 1));
                                SendPacket(General.MyPackets.NPCLink("Non, je ne veux pas", 255));
                                SendPacket(General.MyPackets.NPCSetFace(30));
                                SendPacket(General.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 1250)
                            {
                                SendPacket(General.MyPackets.NPCSay("Voulez-vous sortir de prison?"));
                                SendPacket(General.MyPackets.NPCLink("Oui.", 1));
                                SendPacket(General.MyPackets.NPCLink("Non, pas maintenant.", 255));
                                SendPacket(General.MyPackets.NPCSetFace(30));
                                SendPacket(General.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 385)
                            {
                                SendPacket(General.MyPackets.NPCSay("Voulez-vous sortir de prison? Si vous avez �t� envoy� ici plus de trois fois vous �tes prit. Il vous coutera 108 CPs pour sortir."));
                                SendPacket(General.MyPackets.NPCLink("Oui, je veux sortir", 1));
                                SendPacket(General.MyPackets.NPCLink("Non, je ne veux pas", 255));
                                SendPacket(General.MyPackets.NPCSetFace(30));
                                SendPacket(General.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 1310)
                            {
                                if (World.DisOn == true && MyChar.Level > 109)
                                {
                                    SendPacket(General.MyPackets.NPCSay("Dis City est commenc� et vous �tes assez fort pour m'aider! �tes-vous pr�t � d�loger la vermine?"));
                                    SendPacket(General.MyPackets.NPCLink("Je veux y aller.", 1));
                                    SendPacket(General.MyPackets.NPCLink("Au revoir...", 255));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                else if (World.DisOn == true && MyChar.Level < 110)
                                {
                                    SendPacket(General.MyPackets.NPCSay("Revenez lorsque vous aurez un niveau sup�rieur � 110..."));
                                    SendPacket(General.MyPackets.NPCLink("Je vois", 255));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                else
                                {
                                    SendPacket(General.MyPackets.NPCSay("Il n'est pas encore l'heure..."));
                                    SendPacket(General.MyPackets.NPCLink("Je vois", 255));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                            }
                            if (CurrentNPC == 1311)
                            {
                                SendPacket(General.MyPackets.NPCSay("Avez-vous les 5 Jades d'enfer? Si oui vous devez aller � la deuxi�me carte."));
                                SendPacket(General.MyPackets.NPCLink("Je veux y aller.", 1));
                                SendPacket(General.MyPackets.NPCLink("Au revoir...", 255));
                                SendPacket(General.MyPackets.NPCSetFace(30));
                                SendPacket(General.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 1312)
                            {
                                SendPacket(General.MyPackets.NPCSay("Vous-avez tu� " + MyChar.DisKO + " monstres. Voulez-vous entrer dans la troisi�me carte?"));
                                SendPacket(General.MyPackets.NPCLink("Je veux y aller.", 1));
                                SendPacket(General.MyPackets.NPCLink("Au revoir...", 255));
                                SendPacket(General.MyPackets.NPCSetFace(30));
                                SendPacket(General.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 8)
                            {
                                if (WHOpen == 1)
                                {
                                    SendPacket(General.MyPackets.ETCPacket(MyChar, 4));
                                }
                                else
                                {
                                    if (MyChar.WHPWcheck == 0)
                                    {
                                        SendPacket(General.MyPackets.ETCPacket(MyChar, 4));
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous avez cr�� un mot de passe pour votre banque. Veuillez entrer le mot de passe pour ouvrir votre banque."));
                                        SendPacket(General.MyPackets.NPCLink2("Mot de passe.", 1));
                                        SendPacket(General.MyPackets.NPCLink("Je veux r�fl�chir.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                            }
                            if (CurrentNPC == 81)
                            {
                                if (WHOpen == 1)
                                {
                                    SendPacket(General.MyPackets.ETCPacket(MyChar, 4));
                                }
                                else
                                {
                                    if (MyChar.WHPWcheck == 0)
                                    {
                                        SendPacket(General.MyPackets.ETCPacket(MyChar, 4));
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous avez cr�� un mot de passe pour votre banque. Veuillez entrer le mot de passe pour ouvrir votre banque."));
                                        SendPacket(General.MyPackets.NPCLink2("Mot de passe.", 1));
                                        SendPacket(General.MyPackets.NPCLink("Je veux r�fl�chir.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                            }
                            if (CurrentNPC == 82)
                            {
                                if (WHOpen == 1)
                                {
                                    SendPacket(General.MyPackets.ETCPacket(MyChar, 4));
                                }
                                else
                                {
                                    if (MyChar.WHPWcheck == 0)
                                    {
                                        SendPacket(General.MyPackets.ETCPacket(MyChar, 4));
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous avez cr�� un mot de passe pour votre banque. Veuillez entrer le mot de passe pour ouvrir votre banque."));
                                        SendPacket(General.MyPackets.NPCLink2("Mot de passe.", 1));
                                        SendPacket(General.MyPackets.NPCLink("Je veux r�fl�chir.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                            }
                            if (CurrentNPC == 83)
                            {
                                if (WHOpen == 1)
                                {
                                    SendPacket(General.MyPackets.ETCPacket(MyChar, 4));
                                }
                                else
                                {
                                    if (MyChar.WHPWcheck == 0)
                                    {
                                        SendPacket(General.MyPackets.ETCPacket(MyChar, 4));
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous avez cr�� un mot de passe pour votre banque. Veuillez entrer le mot de passe pour ouvrir votre banque."));
                                        SendPacket(General.MyPackets.NPCLink2("Mot de passe.", 1));
                                        SendPacket(General.MyPackets.NPCLink("Je veux r�fl�chir.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                            }
                            if (CurrentNPC == 84)
                            {
                                if (WHOpen == 1)
                                {
                                    SendPacket(General.MyPackets.ETCPacket(MyChar, 4));
                                }
                                else
                                {
                                    if (MyChar.WHPWcheck == 0)
                                    {
                                        SendPacket(General.MyPackets.ETCPacket(MyChar, 4));
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous avez cr�� un mot de passe pour votre banque. Veuillez entrer le mot de passe pour ouvrir votre banque."));
                                        SendPacket(General.MyPackets.NPCLink2("Mot de passe.", 1));
                                        SendPacket(General.MyPackets.NPCLink("Je veux r�fl�chir.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                            }
                            if (CurrentNPC == 85)
                            {
                                if (WHOpen == 1)
                                {
                                    SendPacket(General.MyPackets.ETCPacket(MyChar, 4));
                                }
                                else
                                {
                                    if (MyChar.WHPWcheck == 0)
                                    {
                                        SendPacket(General.MyPackets.ETCPacket(MyChar, 4));
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous avez cr�� un mot de passe pour votre banque. Veuillez entrer le mot de passe pour ouvrir votre banque."));
                                        SendPacket(General.MyPackets.NPCLink2("Mot de passe.", 1));
                                        SendPacket(General.MyPackets.NPCLink("Je veux r�fl�chir.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                            }
                            if (CurrentNPC == 278)
                            {
                                SendPacket(General.MyPackets.NPCSay("Le winxfour peut composer et enchanter des items."));
                                SendPacket(General.MyPackets.NPCLink("Composer(+1~+9)", 1));
                                SendPacket(General.MyPackets.NPCLink("Enchanter", 2));
                                SendPacket(General.MyPackets.NPCLink("Composer(+10~+12)", 21));
                                SendPacket(General.MyPackets.NPCSetFace(30));
                                SendPacket(General.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 6690)
                            {
                                SendPacket(General.MyPackets.NPCSay("Je peux vous am�liorer le bless de vos objets. Il me faut 5 amulettes mythique pour le -1. Une amulette pour le -2. Deux amulettes pour le -3. Trois amulettes pour le -4. Quatre amulettes pour le -5. Cinq amulette pour le -6. Sept amulette pour le -7."));
                                SendPacket(General.MyPackets.NPCLink("Allez � la carte mythique.", 1));
                                SendPacket(General.MyPackets.NPCLink("Arme/Arc/Glaive.", 2));
                                SendPacket(General.MyPackets.NPCLink("Collier/Sac.", 3));
                                SendPacket(General.MyPackets.NPCLink("Bague/Bracelet.", 4));
                                SendPacket(General.MyPackets.NPCLink("Botte.", 5));
                                SendPacket(General.MyPackets.NPCLink("Armure/Robe.", 6));
                                SendPacket(General.MyPackets.NPCLink("Casque/Boucle.", 7));
                                SendPacket(General.MyPackets.NPCLink("Bouclier/Arme.", 8));
                                SendPacket(General.MyPackets.NPCSetFace(30));
                                SendPacket(General.MyPackets.NPCFinish());
                            }

                            if (CurrentNPC == 1010)
                            {
                                SendPacket(General.MyPackets.NPCSay("Bonjour, je suis le ForgeronLee. Je peux socker votre premier trou dans un �quipement pour 3 ForeuseDiamant et le second trou pour 9 ForeuseDiamant."));
                                SendPacket(General.MyPackets.NPCLink("1er sock", 1));
                                SendPacket(General.MyPackets.NPCLink("2nd sock", 2));
                                SendPacket(General.MyPackets.NPCLink("Je ne fais que passer", 255));
                                SendPacket(General.MyPackets.NPCSetFace(30));
                                SendPacket(General.MyPackets.NPCFinish());
                            }

                            if (CurrentNPC == 2)
                            {
                                SendPacket(General.MyPackets.NPCSay("Voulez-vous changer la couleur de votre armure, casque ou bouclier?"));
                                SendPacket(General.MyPackets.NPCLink("Armure", 1));
                                SendPacket(General.MyPackets.NPCLink("Casque", 2));
                                SendPacket(General.MyPackets.NPCLink("Bouclier", 3));
                                SendPacket(General.MyPackets.NPCSetFace(30));
                                SendPacket(General.MyPackets.NPCFinish());
                            }

                            if (CurrentNPC == 30015)
                            {
                                SendPacket(General.MyPackets.NPCSay("Notre teinturier est le meilleur en ville. Si vous voulez teindre votre �quipement vous devez entrer. Vous avez un grand choix de couleur. Une m�t�ore est charg�e pour teindre votre �quipement."));
                                SendPacket(General.MyPackets.NPCLink("Oui, voici une m�t�ore.", 1));
                                SendPacket(General.MyPackets.NPCLink("Pouvez-vous teindre mon armure en noir?", 2));
                                SendPacket(General.MyPackets.NPCLink("Je ne fais que passer.", 255));
                                SendPacket(General.MyPackets.NPCSetFace(30));
                                SendPacket(General.MyPackets.NPCFinish());
                            }

                            if (CurrentNPC == 12)
                            {
                                SendPacket(General.MyPackets.NPCSay("Bonjour, je suis le ma�tre des taoistes de feu. Que puis-je faire pour vous?"));
                                SendPacket(General.MyPackets.NPCLink("Je veux avancer.", 1));
                                SendPacket(General.MyPackets.NPCLink("Je veux apprendre de nouveaux pouvoirs.", 100));
                                SendPacket(General.MyPackets.NPCLink("Je ne fais que passer.", 255));
                                SendPacket(General.MyPackets.NPCSetFace(30));
                                SendPacket(General.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 264)
                            {
                                SendPacket(General.MyPackets.NPCSay("Bonjour, je suis le ma�tre des taoistes d'eau. Que puis-je faire pour vous?"));
                                SendPacket(General.MyPackets.NPCLink("Je veux avancer.", 1));
                                SendPacket(General.MyPackets.NPCLink("Je veux apprendre de nouveaux pouvoirs.", 100));
                                SendPacket(General.MyPackets.NPCLink("Je ne fais que passer.", 255));
                                SendPacket(General.MyPackets.NPCSetFace(30));
                                SendPacket(General.MyPackets.NPCFinish());
                            }

                            if (CurrentNPC == 17)
                            {
                                SendPacket(General.MyPackets.NPCSay("Bonjour, je suis le ma�tre des braves. Que puis-je faire pour vous?"));
                                SendPacket(General.MyPackets.NPCLink("Je veux avancer.", 1));
                                SendPacket(General.MyPackets.NPCLink("Je veux apprendre de nouveaux pouvoirs.", 222));
                                SendPacket(General.MyPackets.NPCLink("Je ne fais que passer.", 255));
                                SendPacket(General.MyPackets.NPCSetFace(30));
                                SendPacket(General.MyPackets.NPCFinish());
                            }

                            if (CurrentNPC == 16)
                            {
                                SendPacket(General.MyPackets.NPCSay("Bonjour, je suis le ma�tre des guerriers. Que puis-je faire pour vous?"));
                                SendPacket(General.MyPackets.NPCLink("Je veux avancer.", 1));
                                SendPacket(General.MyPackets.NPCLink("Je veux apprendre de nouveaux pouvoirs.", 150));
                                SendPacket(General.MyPackets.NPCLink("Je ne fais que passer.", 255));
                                SendPacket(General.MyPackets.NPCSetFace(30));
                                SendPacket(General.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 6)
                            {
                                SendPacket(General.MyPackets.NPCSay("Bonjour, je suis le ma�tre des archers. Que puis-je faire pour vous?"));
                                SendPacket(General.MyPackets.NPCLink("Je veux avancer.", 1));
                                SendPacket(General.MyPackets.NPCLink("Je veux apprendre de nouveaux pouvoirs.", 20));
                                SendPacket(General.MyPackets.NPCLink("Je ne fais que passer.", 255));
                                SendPacket(General.MyPackets.NPCSetFace(30));
                                SendPacket(General.MyPackets.NPCFinish());
                            }

                            if (CurrentNPC == 44)
                            {
                                SendPacket(General.MyPackets.NPCSay("C'est vraiment difficile d'avoir des trous dans les armes. J'ai de grandes capacit�s. Que puis-je faire pour vous?"));
                                SendPacket(General.MyPackets.NPCLink("Pouvez-vous cr�er un trou dans mon arme?", 1));
                                SendPacket(General.MyPackets.NPCLink("Je ne fais que passer.", 255));
                                SendPacket(General.MyPackets.NPCSetFace(30));
                                SendPacket(General.MyPackets.NPCFinish());
                            }

                            if (CurrentNPC == 21 || CurrentNPC == 181 || CurrentNPC == 182 || CurrentNPC == 183 || CurrentNPC == 184)
                            {
                                SendPacket(General.MyPackets.NPCSay("Si vous �tes niveau 20 ou plus, je peux vous faire entrer sur le terrain d'entrainement. Voulez vous y aller pour 1.000 d'argent ? Je peux aussi vous envoyez au OfflineTG"));
                                SendPacket(General.MyPackets.NPCLink("Oui s'il vous pla�t.", 1));
                                SendPacket(General.MyPackets.NPCLink("OfflineTG S.V.P.", 2)); 
                                SendPacket(General.MyPackets.NPCLink("Non merci.", 255));
                                SendPacket(General.MyPackets.NPCSetFace(30));
                                SendPacket(General.MyPackets.NPCFinish());
                            }

                            if (CurrentNPC == 103)
                            {
                                SendPacket(General.MyPackets.NPCSay("O� voulez-vous aller? Je peux vous t�l�porter pour 100$."));
                                SendPacket(General.MyPackets.NPCLink("Citadelle Ph�nix", 1));
                                SendPacket(General.MyPackets.NPCLink("Ville D�sert", 2));
                                SendPacket(General.MyPackets.NPCLink("Ville Tigre", 3));
                                SendPacket(General.MyPackets.NPCLink("�le de Grue", 4));
                                SendPacket(General.MyPackets.NPCLink("Carverne Mine", 5));
                                SendPacket(General.MyPackets.NPCLink("March�", 6));
                                SendPacket(General.MyPackets.NPCLink("Je ne fais que passer.", 7));
                                SendPacket(General.MyPackets.NPCSetFace(30));
                                SendPacket(General.MyPackets.NPCFinish());
                            }
                            if (CurrentNPC == 211)
                            {
                                SendPacket(General.MyPackets.NPCSay("Voulez-vous sortir du march�? Je peux vous t�l�porter gratuitement."));
                                SendPacket(General.MyPackets.NPCLink("Oui, Merci.", 1));
                                SendPacket(General.MyPackets.NPCLink("Non, je reste ici.", 2));
                                SendPacket(General.MyPackets.NPCSetFace(30));
                                SendPacket(General.MyPackets.NPCFinish());
                            }

                            if (CurrentNPC == 266)
                            {
                                SendPacket(General.MyPackets.NPCSay("Maintenant je peux offrir trois types de coupes : Style nouveau, style nostalgique et style sp�cial. Cela co�te 500 argent."));
                                SendPacket(General.MyPackets.NPCLink("Nouveau style", 1));
                                SendPacket(General.MyPackets.NPCLink("Style nostalgique", 2));
                                SendPacket(General.MyPackets.NPCLink("Style sp�ciale", 3));
                                SendPacket(General.MyPackets.NPCLink("Je garde ma coupe ! Escroc ! Voleur ! xD", 255));
                                SendPacket(General.MyPackets.NPCSetFace(30));
                                SendPacket(General.MyPackets.NPCFinish());
                            }
                            MyChar.Ready = true;
                            break;
                        }
                    case 2032:
                        {
                            MyChar.Ready = false;
                            int Control = (int)Data[10];

                            if (Control == 0)
                            {
                                string Name = "";
                                for (int i = 14; i < 14 + Data[13]; i++)
                                {
                                    Name += Convert.ToChar(Data[i]);
                                }

                                uint CharID = 0;
                                byte Pos = 0;

                                foreach (DictionaryEntry DE in MyChar.MyGuild.Members)
                                {
                                    string nm = (string)DE.Value;
                                    string[] Splitter = nm.Split(':');

                                    if (Splitter[0] == Name)
                                    {
                                        CharID = uint.Parse(Splitter[1]);
                                        Pos = 50;
                                    }
                                }
                                foreach (DictionaryEntry DE in MyChar.MyGuild.DLs)
                                {
                                    string dl = (string)DE.Value;
                                    string[] Splitter = dl.Split(':');

                                    if (Splitter[0] == Name)
                                    {
                                        CharID = uint.Parse(Splitter[1]);
                                        Pos = 90;
                                    }
                                }
                                if (CharID != 0)
                                    MyChar.MyGuild.KickPlayer(CharID, Name, Pos);

                            }
                            if (CurrentNPC == 9543)
                            {
                                if (Control == 1)
                                {
                                    SendPacket(General.MyPackets.NPCSay("Le mot de passe peut avoir 9 caract�res, le premier ne doit pas �tre un 0."));
                                    SendPacket(General.MyPackets.NPCSay(" Entrez des chiffres. Si vous faites une faute la banque vas �tre bloqu�."));
                                    SendPacket(General.MyPackets.NPCSay(" Veuillez �tre sure avant d'entrer le mot de passe."));
                                    SendPacket(General.MyPackets.NPCLink2("Mot de passe.", 2));
                                    SendPacket(General.MyPackets.NPCLink("Je dois r�fl�chir.", 255));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 2)
                                {
                                    string WHPW = "";
                                    bool ValidPW = true;
                                    for (int i = 14; i < 14 + Data[13]; i++)
                                    {
                                        WHPW += Convert.ToChar(Data[i]);
                                    }
                                    if (WHPW.IndexOfAny(new char[27] { ' ', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' }) > -1)
                                    {
                                        ValidPW = false;
                                    }

                                    try
                                    {
                                        if (ValidPW)
                                        {
                                            MyChar.WHPW = WHPW;
                                            MyChar.WHPWcheck = 1;
                                            SendPacket(General.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "Le mot de passe est " + MyChar.WHPW, 2005));
                                            MyChar.SaveWHPW();
                                        }
                                        else
                                        {
                                            SendPacket(General.MyPackets.NPCSay("Ce nom n'est pas valide!"));
                                            SendPacket(General.MyPackets.NPCLink("R�essayer.", 1));
                                            SendPacket(General.MyPackets.NPCLink("Je vois.", 255));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                    }
                                    catch (Exception Exc) { General.WriteLine(Exc.ToString()); }

                                    break;
                                }
                                if (Control == 4)
                                {
                                    SendPacket(General.MyPackets.NPCSay("Veuillez entrer votre ancien mot de passe."));
                                    SendPacket(General.MyPackets.NPCLink2("Mot de passe", 5));
                                    SendPacket(General.MyPackets.NPCLink("Laissez moi r�fl�chir", 255));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 5)
                                {
                                    string WHPW2 = "";
                                    bool ValidPW2 = false;
                                    for (int i = 14; i < 14 + Data[13]; i++)
                                    {
                                        WHPW2 += Convert.ToChar(Data[i]);
                                    }
                                    if (WHPW2 == MyChar.WHPW)
                                    {
                                        ValidPW2 = true;
                                    }

                                    try
                                    {
                                        if (ValidPW2)
                                        {
                                            SendPacket(General.MyPackets.NPCSay("Vous voulez changer le mot de passe ou le supprimer ?"));
                                            SendPacket(General.MyPackets.NPCLink("Changer", 6));
                                            SendPacket(General.MyPackets.NPCLink("Supprimer", 7));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                        else
                                        {
                                            SendPacket(General.MyPackets.NPCSay("Mauvais mot de passe, r�essayez."));
                                            SendPacket(General.MyPackets.NPCLink("D'accord", 4));
                                            SendPacket(General.MyPackets.NPCLink("Je vois", 255));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                    }
                                    catch (Exception Exc) { General.WriteLine(Exc.ToString()); }

                                    break;
                                }
                                if (Control == 6)
                                {
                                    WHOpen = 0;
                                    SendPacket(General.MyPackets.NPCSay("Entrez votre futur mot de passe de magasinier."));
                                    SendPacket(General.MyPackets.NPCSay(" Entrez seulement des chiffrez !"));
                                    SendPacket(General.MyPackets.NPCSay(" Retenez bien votre mot de passe."));
                                    SendPacket(General.MyPackets.NPCLink2("Cr�er un mot de passe.", 2));
                                    SendPacket(General.MyPackets.NPCLink("Laissez moi r�fl�chir.", 255));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 7)
                                {
                                    MyChar.WHPW = "";
                                    MyChar.WHPWcheck = 0;
                                    WHOpen = 0;
                                    SendPacket(General.MyPackets.NPCSay("Mot de passe supprim� avec succ�s !"));
                                    SendPacket(General.MyPackets.NPCLink("Merci", 255));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                            }
                            if (CurrentNPC == 8)
                            {
                                if (Control == 1)
                                {
                                    string WHPW1 = "";
                                    bool ValidPW1 = false;
                                    for (int i = 14; i < 14 + Data[13]; i++)
                                    {
                                        WHPW1 += Convert.ToChar(Data[i]);
                                    }

                                    if (WHPW1 == MyChar.WHPW)
                                    {
                                        ValidPW1 = true;
                                    }
                                    try
                                    {
                                        if (ValidPW1)
                                        {
                                            SendPacket(General.MyPackets.ETCPacket(MyChar, 4));
                                            WHOpen = 1;
                                        }
                                        else
                                        {
                                            SendPacket(General.MyPackets.NPCSay("Mauvais mot de passe. R�essayer."));
                                            SendPacket(General.MyPackets.NPCLink("Ok.", 0));
                                            SendPacket(General.MyPackets.NPCLink("Je vois.", 255));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                    }
                                    catch (Exception Exc) { General.WriteLine(Exc.ToString()); }

                                    break;
                                }
                            }
                            if (CurrentNPC == 81)
                            {
                                if (Control == 1)
                                {
                                    string WHPW1 = "";
                                    bool ValidPW1 = false;
                                    for (int i = 14; i < 14 + Data[13]; i++)
                                    {
                                        WHPW1 += Convert.ToChar(Data[i]);
                                    }

                                    if (WHPW1 == MyChar.WHPW)
                                    {
                                        ValidPW1 = true;
                                    }
                                    try
                                    {
                                        if (ValidPW1)
                                        {
                                            SendPacket(General.MyPackets.ETCPacket(MyChar, 4));
                                            WHOpen = 1;
                                        }
                                        else
                                        {
                                            SendPacket(General.MyPackets.NPCSay("Mauvais mot de passe. R�essayer."));
                                            SendPacket(General.MyPackets.NPCLink("Ok.", 0));
                                            SendPacket(General.MyPackets.NPCLink("Je vois.", 255));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                    }
                                    catch (Exception Exc) { General.WriteLine(Exc.ToString()); }

                                    break;
                                }
                            }
                            if (CurrentNPC == 82)
                            {
                                if (Control == 1)
                                {
                                    string WHPW1 = "";
                                    bool ValidPW1 = false;
                                    for (int i = 14; i < 14 + Data[13]; i++)
                                    {
                                        WHPW1 += Convert.ToChar(Data[i]);
                                    }

                                    if (WHPW1 == MyChar.WHPW)
                                    {
                                        ValidPW1 = true;
                                    }
                                    try
                                    {
                                        if (ValidPW1)
                                        {
                                            SendPacket(General.MyPackets.ETCPacket(MyChar, 4));
                                            WHOpen = 1;
                                        }
                                        else
                                        {
                                            SendPacket(General.MyPackets.NPCSay("Mauvais mot de passe. R�essayer."));
                                            SendPacket(General.MyPackets.NPCLink("Ok.", 0));
                                            SendPacket(General.MyPackets.NPCLink("Je vois.", 255));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                    }
                                    catch (Exception Exc) { General.WriteLine(Exc.ToString()); }

                                    break;
                                }
                            }
                            if (CurrentNPC == 83)
                            {
                                if (Control == 1)
                                {
                                    string WHPW1 = "";
                                    bool ValidPW1 = false;
                                    for (int i = 14; i < 14 + Data[13]; i++)
                                    {
                                        WHPW1 += Convert.ToChar(Data[i]);
                                    }

                                    if (WHPW1 == MyChar.WHPW)
                                    {
                                        ValidPW1 = true;
                                    }
                                    try
                                    {
                                        if (ValidPW1)
                                        {
                                            SendPacket(General.MyPackets.ETCPacket(MyChar, 4));
                                            WHOpen = 1;
                                        }
                                        else
                                        {
                                            SendPacket(General.MyPackets.NPCSay("Mauvais mot de passe. R�essayer."));
                                            SendPacket(General.MyPackets.NPCLink("Ok.", 0));
                                            SendPacket(General.MyPackets.NPCLink("Je vois.", 255));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                    }
                                    catch (Exception Exc) { General.WriteLine(Exc.ToString()); }

                                    break;
                                }
                            }
                            if (CurrentNPC == 84)
                            {
                                if (Control == 1)
                                {
                                    string WHPW1 = "";
                                    bool ValidPW1 = false;
                                    for (int i = 14; i < 14 + Data[13]; i++)
                                    {
                                        WHPW1 += Convert.ToChar(Data[i]);
                                    }

                                    if (WHPW1 == MyChar.WHPW)
                                    {
                                        ValidPW1 = true;
                                    }
                                    try
                                    {
                                        if (ValidPW1)
                                        {
                                            SendPacket(General.MyPackets.ETCPacket(MyChar, 4));
                                            WHOpen = 1;
                                        }
                                        else
                                        {
                                            SendPacket(General.MyPackets.NPCSay("Mauvais mot de passe. R�essayer."));
                                            SendPacket(General.MyPackets.NPCLink("Ok.", 0));
                                            SendPacket(General.MyPackets.NPCLink("Je vois.", 255));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                    }
                                    catch (Exception Exc) { General.WriteLine(Exc.ToString()); }

                                    break;
                                }
                            }
                            if (CurrentNPC == 85)
                            {
                                if (Control == 1)
                                {
                                    string WHPW1 = "";
                                    bool ValidPW1 = false;
                                    for (int i = 14; i < 14 + Data[13]; i++)
                                    {
                                        WHPW1 += Convert.ToChar(Data[i]);
                                    }

                                    if (WHPW1 == MyChar.WHPW)
                                    {
                                        ValidPW1 = true;
                                    }
                                    try
                                    {
                                        if (ValidPW1)
                                        {
                                            SendPacket(General.MyPackets.ETCPacket(MyChar, 4));
                                            WHOpen = 1;
                                        }
                                        else
                                        {
                                            SendPacket(General.MyPackets.NPCSay("Mauvais mot de passe. R�essayer."));
                                            SendPacket(General.MyPackets.NPCLink("Ok.", 0));
                                            SendPacket(General.MyPackets.NPCLink("Je vois.", 255));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                    }
                                    catch (Exception Exc) { General.WriteLine(Exc.ToString()); }

                                    break;
                                }
                            }
                            if (CurrentNPC == 127)
                            {
                                if (Control == 1)
                                {
                                    SendPacket(General.MyPackets.NPCSay("Si vous voulez rena�tre vous devez atteindre un certain niveau, aussi vous devez avoir le plus haut"));
                                    SendPacket(General.MyPackets.NPCSay(" titre de votre classe et avoir une CelestialStone. Apr�s la renaissance, vous pouvez distribuer"));
                                    SendPacket(General.MyPackets.NPCSay(" votre attribut plus librement. Et vous pouvez apprendre plusieurs comp�tences puissantes."));
                                    SendPacket(General.MyPackets.NPCLink("Qu'es ce qu'une CelestialStone?", 8));
                                    SendPacket(General.MyPackets.NPCLink("Je veux rena�tre.", 7));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 8)
                                {
                                    SendPacket(General.MyPackets.NPCSay("La CelestialStone est compos� des 7 gemmes du monde. Il me la faut pour que je pr�pare la renaissance."));
                                    SendPacket(General.MyPackets.NPCLink("Je vois.", 255));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 7)
                                {
                                    if (MyChar.InventoryContains(721259, 1) && (MyChar.Job == 15 || MyChar.Job == 25 || MyChar.Job == 45 || MyChar.Job == 125 || MyChar.Job == 135 || MyChar.Job == 145 || MyChar.Job == 155))
                                    {
                                        SendPacket(General.MyPackets.NPCSay("En quel classe voulez-vous rena�tre?"));
                                        SendPacket(General.MyPackets.NPCLink("Brave", 2));
                                        SendPacket(General.MyPackets.NPCLink("Guerrier", 3));
                                        SendPacket(General.MyPackets.NPCLink("Archer", 4));
                                        SendPacket(General.MyPackets.NPCLink("Tao�ste de feu", 5));
                                        SendPacket(General.MyPackets.NPCLink("Tao�ste d'eau", 6));
                                        SendPacket(General.MyPackets.NPCLink("Je dois r�fl�chir.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous ne pouvez pas rena�tre si vous n'avez pas la CelestialStone ou vous n'avez pas fait toutes vos promotions."));
                                        SendPacket(General.MyPackets.NPCLink("je vois.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                                if (Control == 2)
                                {
                                    MyChar.RemoveItem(MyChar.ItemNext(721259));
                                    MyChar.ReBorn(11);
                                }
                                if (Control == 3)
                                {
                                    MyChar.RemoveItem(MyChar.ItemNext(721259));
                                    MyChar.ReBorn(21);
                                }
                                if (Control == 4)
                                {
                                    MyChar.RemoveItem(MyChar.ItemNext(721259));
                                    MyChar.ReBorn(41);
                                }
                                if (Control == 5)
                                {
                                    MyChar.RemoveItem(MyChar.ItemNext(721259));
                                    MyChar.ReBorn(142);
                                }
                                if (Control == 6)
                                {
                                    MyChar.RemoveItem(MyChar.ItemNext(721259));
                                    MyChar.ReBorn(132);
                                }
                            }
                            if (CurrentNPC == 128)
                            {
                                if (Control == 11) //2Rb Troj
                                {
                                    MyChar.RemoveItem(MyChar.ItemNext(723701));
                                    MyChar.SecReBorn(11);
                                }
                                if (Control == 21) //2Rb War
                                {
                                    MyChar.RemoveItem(MyChar.ItemNext(723701));
                                    MyChar.SecReBorn(21);
                                }
                                if (Control == 41) //2Rb Archer
                                {
                                    MyChar.RemoveItem(MyChar.ItemNext(723701));
                                    MyChar.SecReBorn(41);
                                }
                                if (Control == 132) //2Rb Wat
                                {
                                    MyChar.RemoveItem(MyChar.ItemNext(723701));
                                    MyChar.SecReBorn(132);
                                }
                                if (Control == 142) //2Rb Fire
                                {
                                    MyChar.RemoveItem(MyChar.ItemNext(723701));
                                    MyChar.SecReBorn(142);
                                }
                            }
                            #region MCCaptain 2
                            if (CurrentNPC == 285)
                            {
                                if (Control == 15)
                                {
                                    if (MyChar.InventoryContains(750000, 1))
                                    {
                                        MyChar.RemoveItem(MyChar.ItemNext(750000));
                                        MyChar.QuestKO = 0;
                                        MyChar.QuestMob = "";
                                        MyChar.QuestFrom = "";
                                    }
                                    else
                                    {
                                        MyChar.QuestKO = 0;
                                        MyChar.QuestMob = "";
                                        MyChar.QuestFrom = "";
                                    }
                                }
                                if (Control == 9)
                                {
                                    SendPacket(General.MyPackets.NPCSay("�tes-vous s�r de vouloir mettre fin � la qu�te?"));
                                    SendPacket(General.MyPackets.NPCLink("Oui, mettre fin � la Qu�te.", 15));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 1)
                                {
                                    SendPacket(General.MyPackets.NPCSay("Si vous pouvez aller tuer 300 BatTombales, la Citadelle Ancienne deviendra beaucoup plus tranquille."));
                                    SendPacket(General.MyPackets.NPCSay(" D'ailleurs, si vous faites �quipe avec d'autres joueurs, l'�me des monstres tu�s par vos co�quipiers peuvent �tre r�cup�r�es dans votre gourde."));
                                    SendPacket(General.MyPackets.NPCLink("J'accepte la Qu�te.", 5));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 2)
                                {
                                    SendPacket(General.MyPackets.NPCSay("Si vous pouvez aller tuer 300 BatSanglants, la Citadelle Ancienne deviendra beaucoup plus tranquille."));
                                    SendPacket(General.MyPackets.NPCSay(" D'ailleurs, si vous faites �quipe avec d'autres joueurs, l'�me des monstres tu�s par vos co�quipiers peuvent �tre r�cup�r�es dans votre gourde."));
                                    SendPacket(General.MyPackets.NPCLink("J'accepte la Qu�te.", 6));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 3)
                                {
                                    SendPacket(General.MyPackets.NPCSay("Si vous pouvez aller tuer 300 MonstreTaureaux, la Citadelle Ancienne deviendra beaucoup plus tranquille."));
                                    SendPacket(General.MyPackets.NPCSay(" D'ailleurs, si vous faites �quipe avec d'autres joueurs, l'�me des monstres tu�s par vos co�quipiers peuvent �tre r�cup�r�es dans votre gourde."));
                                    SendPacket(General.MyPackets.NPCLink("J'accepte la Qu�te.", 7));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 4)
                                {
                                    SendPacket(General.MyPackets.NPCSay("Si vous pouvez aller tuer 300 D�monRougeL117, la Citadelle Ancienne deviendra beaucoup plus tranquille."));
                                    SendPacket(General.MyPackets.NPCSay(" D'ailleurs, si vous faites �quipe avec d'autres joueurs, l'�me des monstres tu�s par vos co�quipiers peuvent �tre r�cup�r�es dans votre gourde."));
                                    SendPacket(General.MyPackets.NPCLink("J'accepte la Qu�te.", 8));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 5)
                                {
                                    MyChar.QuestMob = "BatTombale";
                                    MyChar.QuestFrom = "MC";
                                    MyChar.QuestKO = 0;
                                    MyChar.AddItem("750000-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                }
                                if (Control == 6)
                                {
                                    MyChar.QuestMob = "BatSanglant";
                                    MyChar.QuestFrom = "MC";
                                    MyChar.QuestKO = 0;
                                    MyChar.AddItem("750000-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                }
                                if (Control == 7)
                                {
                                    MyChar.QuestMob = "MonstreTaureau";
                                    MyChar.QuestFrom = "MC";
                                    MyChar.QuestKO = 0;
                                    MyChar.AddItem("750000-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                }
                                if (Control == 8)
                                {
                                    MyChar.QuestMob = "D�monRougeL117";
                                    MyChar.QuestFrom = "MC";
                                    MyChar.QuestKO = 0;
                                    MyChar.AddItem("750000-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                }
                                if (Control == 10)
                                {
                                    MyChar.Teleport(1011, 230, 258);
                                }
                                if (Control == 11)
                                {
                                    MyChar.Teleport(1020, 569, 622);
                                }
                                if (Control == 12)
                                {
                                    MyChar.Teleport(1000, 477, 634);
                                }
                                if (Control == 13)
                                {
                                    MyChar.Teleport(1015, 791, 569);
                                }
                                if (Control == 14)
                                {
                                    MyChar.Teleport(1000, 083, 319);
                                }
                            }
                            #endregion
                            #region BICaptain 2
                            if (CurrentNPC == 284)
                            {
                                if (Control == 15)
                                {
                                    if (MyChar.InventoryContains(750000, 1))
                                    {
                                        MyChar.RemoveItem(MyChar.ItemNext(750000));
                                        MyChar.QuestKO = 0;
                                        MyChar.QuestMob = "";
                                        MyChar.QuestFrom = "";
                                    }
                                    else
                                    {
                                        MyChar.QuestKO = 0;
                                        MyChar.QuestMob = "";
                                        MyChar.QuestFrom = "";
                                    }
                                }
                                if (Control == 9)
                                {
                                    SendPacket(General.MyPackets.NPCSay("�tes-vous s�r de vouloir mettre fin � la qu�te?"));
                                    SendPacket(General.MyPackets.NPCLink("Oui, mettre fin � la Qu�te.", 15));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 1)
                                {
                                    SendPacket(General.MyPackets.NPCSay("Si vous pouvez aller tuer 300 HommeOiseaux, l'�le de Grue deviendra beaucoup plus tranquille."));
                                    SendPacket(General.MyPackets.NPCSay(" D'ailleurs, si vous faites �quipe avec d'autres joueurs, l'�me des monstres tu�s par vos co�quipiers peuvent �tre r�cup�r�es dans votre gourde."));
                                    SendPacket(General.MyPackets.NPCLink("J'accepte la Qu�te.", 5));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 2)
                                {
                                    SendPacket(General.MyPackets.NPCSay("Si vous pouvez aller tuer 300 RoiAigle, l'�le de Grue deviendra beaucoup plus tranquille."));
                                    SendPacket(General.MyPackets.NPCSay(" D'ailleurs, si vous faites �quipe avec d'autres joueurs, l'�me des monstres tu�s par vos co�quipiers peuvent �tre r�cup�r�es dans votre gourde."));
                                    SendPacket(General.MyPackets.NPCLink("J'accepte la Qu�te.", 6));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 3)
                                {
                                    SendPacket(General.MyPackets.NPCSay("Si vous pouvez aller tuer 300 BanditL97, l'�le de Grue deviendra beaucoup plus tranquille."));
                                    SendPacket(General.MyPackets.NPCSay(" D'ailleurs, si vous faites �quipe avec d'autres joueurs, l'�me des monstres tu�s par vos co�quipiers peuvent �tre r�cup�r�es dans votre gourde."));
                                    SendPacket(General.MyPackets.NPCLink("J'accepte la Qu�te.", 7));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 5)
                                {
                                    MyChar.QuestMob = "HommeOiseau";
                                    MyChar.QuestFrom = "BI";
                                    MyChar.QuestKO = 0;
                                    MyChar.AddItem("750000-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                }
                                if (Control == 6)
                                {
                                    MyChar.QuestMob = "RoiAigle";
                                    MyChar.QuestFrom = "BI";
                                    MyChar.QuestKO = 0;
                                    MyChar.AddItem("750000-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                }
                                if (Control == 7)
                                {
                                    MyChar.QuestMob = "BanditL97";
                                    MyChar.QuestFrom = "BI";
                                    MyChar.QuestKO = 0;
                                    MyChar.AddItem("750000-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                }
                                if (Control == 10)
                                {
                                    MyChar.Teleport(1011, 230, 258);
                                }
                                if (Control == 11)
                                {
                                    MyChar.Teleport(1020, 569, 622);
                                }
                                if (Control == 12)
                                {
                                    MyChar.Teleport(1000, 477, 634);
                                }
                                if (Control == 13)
                                {
                                    MyChar.Teleport(1015, 791, 569);
                                }
                                if (Control == 14)
                                {
                                    MyChar.Teleport(1000, 083, 319);
                                }
                            }
                            #endregion
                            #region DCCaptain 2
                            if (CurrentNPC == 283)
                            {
                                if (Control == 15)
                                {
                                    if (MyChar.InventoryContains(750000, 1))
                                    {
                                        MyChar.RemoveItem(MyChar.ItemNext(750000));
                                        MyChar.QuestKO = 0;
                                        MyChar.QuestMob = "";
                                        MyChar.QuestFrom = "";
                                    }
                                    else
                                    {
                                        MyChar.QuestKO = 0;
                                        MyChar.QuestMob = "";
                                        MyChar.QuestFrom = "";
                                    }
                                }
                                if (Control == 9)
                                {
                                    SendPacket(General.MyPackets.NPCSay("�tes-vous s�r de vouloir mettre fin � la qu�te?"));
                                    SendPacket(General.MyPackets.NPCLink("Oui, mettre fin � la Qu�te.", 15));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 1)
                                {
                                    SendPacket(General.MyPackets.NPCSay("Si vous pouvez aller tuer 300 MonstreSables, la Ville de D�sert deviendra beaucoup plus tranquille."));
                                    SendPacket(General.MyPackets.NPCSay(" D'ailleurs, si vous faites �quipe avec d'autres joueurs, l'�me des monstres tu�s par vos co�quipiers peuvent �tre r�cup�r�es dans votre gourde."));
                                    SendPacket(General.MyPackets.NPCLink("J'accepte la Qu�te.", 5));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 2)
                                {
                                    SendPacket(General.MyPackets.NPCSay("Si vous pouvez aller tuer 300 MonstreHerbes, la Ville de D�sert deviendra beaucoup plus tranquille."));
                                    SendPacket(General.MyPackets.NPCSay(" D'ailleurs, si vous faites �quipe avec d'autres joueurs, l'�me des monstres tu�s par vos co�quipiers peuvent �tre r�cup�r�es dans votre gourde."));
                                    SendPacket(General.MyPackets.NPCLink("J'accepte la Qu�te.", 6));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 3)
                                {
                                    SendPacket(General.MyPackets.NPCSay("Si vous pouvez aller tuer 300 MonstreRocs, la Ville de D�sert deviendra beaucoup plus tranquille."));
                                    SendPacket(General.MyPackets.NPCSay(" D'ailleurs, si vous faites �quipe avec d'autres joueurs, l'�me des monstres tu�s par vos co�quipiers peuvent �tre r�cup�r�es dans votre gourde."));
                                    SendPacket(General.MyPackets.NPCLink("J'accepte la Qu�te.", 7));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 4)
                                {
                                    SendPacket(General.MyPackets.NPCSay("Si vous pouvez aller tuer 300 Fant�meSangs, la Ville de D�sert deviendra beaucoup plus tranquille."));
                                    SendPacket(General.MyPackets.NPCSay(" D'ailleurs, si vous faites �quipe avec d'autres joueurs, l'�me des monstres tu�s par vos co�quipiers peuvent �tre r�cup�r�es dans votre gourde."));
                                    SendPacket(General.MyPackets.NPCLink("J'accepte la Qu�te.", 8));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 5)
                                {
                                    MyChar.QuestMob = "MonstreSable";
                                    MyChar.QuestFrom = "DC";
                                    MyChar.QuestKO = 0;
                                    MyChar.AddItem("750000-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                }
                                if (Control == 6)
                                {
                                    MyChar.QuestMob = "MonstreHerbe";
                                    MyChar.QuestFrom = "DC";
                                    MyChar.QuestKO = 0;
                                    MyChar.AddItem("750000-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                }
                                if (Control == 7)
                                {
                                    MyChar.QuestMob = "MonstreRoc";
                                    MyChar.QuestFrom = "DC";
                                    MyChar.QuestKO = 0;
                                    MyChar.AddItem("750000-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                }
                                if (Control == 8)
                                {
                                    MyChar.QuestMob = "Fant�meSang";
                                    MyChar.QuestFrom = "DC";
                                    MyChar.QuestKO = 0;
                                    MyChar.AddItem("750000-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                }
                                if (Control == 10)
                                {
                                    MyChar.Teleport(1011, 230, 258);
                                }
                                if (Control == 11)
                                {
                                    MyChar.Teleport(1020, 569, 622);
                                }
                                if (Control == 12)
                                {
                                    MyChar.Teleport(1000, 477, 634);
                                }
                                if (Control == 13)
                                {
                                    MyChar.Teleport(1015, 791, 569);
                                }
                                if (Control == 14)
                                {
                                    MyChar.Teleport(1000, 083, 319);
                                }
                            }
                            #endregion
                            #region ACCaptain 2
                            if (CurrentNPC == 282)
                            {
                                if (Control == 15)
                                {
                                    if (MyChar.InventoryContains(750000, 1))
                                    {
                                        MyChar.RemoveItem(MyChar.ItemNext(750000));
                                        MyChar.QuestKO = 0;
                                        MyChar.QuestMob = "";
                                        MyChar.QuestFrom = "";
                                    }
                                    else
                                    {
                                        MyChar.QuestKO = 0;
                                        MyChar.QuestMob = "";
                                        MyChar.QuestFrom = "";
                                    }
                                }
                                if (Control == 9)
                                {
                                    SendPacket(General.MyPackets.NPCSay("�tes-vous s�r de vouloir mettre fin � la qu�te?"));
                                    SendPacket(General.MyPackets.NPCLink("Oui, mettre fin � la Qu�te.", 15));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 1)
                                {
                                    SendPacket(General.MyPackets.NPCSay("Si vous pouvez aller tuer 300 Macaques, la Ville Tigre deviendra beaucoup plus tranquille."));
                                    SendPacket(General.MyPackets.NPCSay(" D'ailleurs, si vous faites �quipe avec d'autres joueurs, l'�me des monstres tu�s par vos co�quipiers peuvent �tre r�cup�r�es dans votre gourde."));
                                    SendPacket(General.MyPackets.NPCLink("J'accepte la Qu�te.", 5));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 2)
                                {
                                    SendPacket(General.MyPackets.NPCSay("Si vous pouvez aller tuer 300 Gorilles, la Ville Tigre deviendra beaucoup plus tranquille."));
                                    SendPacket(General.MyPackets.NPCSay(" D'ailleurs, si vous faites �quipe avec d'autres joueurs, l'�me des monstres tu�s par vos co�quipiers peuvent �tre r�cup�r�es dans votre gourde."));
                                    SendPacket(General.MyPackets.NPCLink("J'accepte la Qu�te.", 6));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 3)
                                {
                                    SendPacket(General.MyPackets.NPCSay("Si vous pouvez aller tuer 300 GibbonFoudres, la Ville Tigre deviendra beaucoup plus tranquille."));
                                    SendPacket(General.MyPackets.NPCSay(" D'ailleurs, si vous faites �quipe avec d'autres joueurs, l'�me des monstres tu�s par vos co�quipiers peuvent �tre r�cup�r�es dans votre gourde."));
                                    SendPacket(General.MyPackets.NPCLink("J'accepte la Qu�te.", 7));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 4)
                                {
                                    SendPacket(General.MyPackets.NPCSay("Si vous pouvez aller tuer 300 HommeSerpents, la Ville Tigre deviendra beaucoup plus tranquille."));
                                    SendPacket(General.MyPackets.NPCSay(" D'ailleurs, si vous faites �quipe avec d'autres joueurs, l'�me des monstres tu�s par vos co�quipiers peuvent �tre r�cup�r�es dans votre gourde."));
                                    SendPacket(General.MyPackets.NPCLink("J'accepte la Qu�te.", 8));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 5)
                                {
                                    MyChar.QuestMob = "Macaque";
                                    MyChar.QuestFrom = "AC";
                                    MyChar.QuestKO = 0;
                                    MyChar.AddItem("750000-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                }
                                if (Control == 6)
                                {
                                    MyChar.QuestMob = "Gorille";
                                    MyChar.QuestFrom = "AC";
                                    MyChar.QuestKO = 0;
                                    MyChar.AddItem("750000-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                }
                                if (Control == 7)
                                {
                                    MyChar.QuestMob = "GibbonFoudre";
                                    MyChar.QuestFrom = "AC";
                                    MyChar.QuestKO = 0;
                                    MyChar.AddItem("750000-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                }
                                if (Control == 8)
                                {
                                    MyChar.QuestMob = "HommeSerpent";
                                    MyChar.QuestFrom = "AC";
                                    MyChar.QuestKO = 0;
                                    MyChar.AddItem("750000-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                }
                                if (Control == 10)
                                {
                                    MyChar.Teleport(1011, 230, 258);
                                }
                                if (Control == 11)
                                {
                                    MyChar.Teleport(1020, 569, 622);
                                }
                                if (Control == 12)
                                {
                                    MyChar.Teleport(1000, 477, 634);
                                }
                                if (Control == 13)
                                {
                                    MyChar.Teleport(1015, 791, 569);
                                }
                                if (Control == 14)
                                {
                                    MyChar.Teleport(1000, 083, 319);
                                }
                            }
                            #endregion
                            #region PCCaptain 2
                            if (CurrentNPC == 281)
                            {
                                if (Control == 15)
                                {
                                    if (MyChar.InventoryContains(750000, 1))
                                    {
                                        MyChar.RemoveItem(MyChar.ItemNext(750000));
                                        MyChar.QuestKO = 0;
                                        MyChar.QuestMob = "";
                                        MyChar.QuestFrom = "";
                                    }
                                    else
                                    {
                                        MyChar.QuestKO = 0;
                                        MyChar.QuestMob = "";
                                        MyChar.QuestFrom = "";
                                    }
                                }
                                if (Control == 9)
                                {
                                    SendPacket(General.MyPackets.NPCSay("�tes-vous s�r de vouloir mettre fin � la qu�te?"));
                                    SendPacket(General.MyPackets.NPCLink("Oui, mettre fin � la Qu�te.", 15));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 1)
                                {
                                    SendPacket(General.MyPackets.NPCSay("Si vous pouvez aller tuer 300 SerpentAil�s, la Citadelle Ph�nix deviendra beaucoup plus tranquille."));
                                    SendPacket(General.MyPackets.NPCSay(" D'ailleurs, si vous faites �quipe avec d'autres joueurs, l'�me des monstres tu�s par vos co�quipiers peuvent �tre r�cup�r�es dans votre gourde."));
                                    SendPacket(General.MyPackets.NPCLink("J'accepte la Qu�te.", 5));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 2)
                                {
                                    SendPacket(General.MyPackets.NPCSay("Si vous pouvez aller tuer 300 Bandits, la Citadelle Ph�nix deviendra beaucoup plus tranquille."));
                                    SendPacket(General.MyPackets.NPCSay(" D'ailleurs, si vous faites �quipe avec d'autres joueurs, l'�me des monstres tu�s par vos co�quipiers peuvent �tre r�cup�r�es dans votre gourde."));
                                    SendPacket(General.MyPackets.NPCLink("J'accepte la Qu�te.", 6));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 3)
                                {
                                    SendPacket(General.MyPackets.NPCSay("Si vous pouvez aller tuer 300 RatDeFeus, la Citadelle Ph�nix deviendra beaucoup plus tranquille."));
                                    SendPacket(General.MyPackets.NPCSay(" D'ailleurs, si vous faites �quipe avec d'autres joueurs, l'�me des monstres tu�s par vos co�quipiers peuvent �tre r�cup�r�es dans votre gourde."));
                                    SendPacket(General.MyPackets.NPCLink("J'accepte la Qu�te.", 7));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 4)
                                {
                                    SendPacket(General.MyPackets.NPCSay("Si vous pouvez aller tuer 300 EspritDuFeus, la Citadelle Ph�nix deviendra beaucoup plus tranquille."));
                                    SendPacket(General.MyPackets.NPCSay(" D'ailleurs, si vous faites �quipe avec d'autres joueurs, l'�me des monstres tu�s par vos co�quipiers peuvent �tre r�cup�r�es dans votre gourde."));
                                    SendPacket(General.MyPackets.NPCLink("J'accepte la Qu�te.", 8));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 5)
                                {
                                    MyChar.QuestMob = "SerpentAil�";
                                    MyChar.QuestFrom = "PC";
                                    MyChar.QuestKO = 0;
                                    MyChar.AddItem("750000-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                }
                                if (Control == 6)
                                {
                                    MyChar.QuestMob = "Bandit";
                                    MyChar.QuestFrom = "TC";
                                    MyChar.QuestKO = 0;
                                    MyChar.AddItem("750000-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                }
                                if (Control == 7)
                                {
                                    MyChar.QuestMob = "RatDeFeu";
                                    MyChar.QuestFrom = "PC";
                                    MyChar.QuestKO = 0;
                                    MyChar.AddItem("750000-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                }
                                if (Control == 8)
                                {
                                    MyChar.QuestMob = "EspritDuFeu";
                                    MyChar.QuestFrom = "PC";
                                    MyChar.QuestKO = 0;
                                    MyChar.AddItem("750000-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                }
                                if (Control == 10)
                                {
                                    MyChar.Teleport(1011, 230, 258);
                                }
                                if (Control == 11)
                                {
                                    MyChar.Teleport(1020, 569, 622);
                                }
                                if (Control == 12)
                                {
                                    MyChar.Teleport(1000, 477, 634);
                                }
                                if (Control == 13)
                                {
                                    MyChar.Teleport(1015, 791, 569);
                                }
                                if (Control == 14)
                                {
                                    MyChar.Teleport(1000, 083, 319);
                                }
                            }
                            #endregion
                            #region TCCaptain 2
                            if (CurrentNPC == 280)
                            {
                                if (Control == 15)
                                {
                                    if (MyChar.InventoryContains(750000, 1))
                                    {
                                        MyChar.RemoveItem(MyChar.ItemNext(750000));
                                        MyChar.QuestKO = 0;
                                        MyChar.QuestMob = "";
                                        MyChar.QuestFrom = "";
                                    }
                                    else
                                    {
                                        MyChar.QuestKO = 0;
                                        MyChar.QuestMob = "";
                                        MyChar.QuestFrom = "";
                                    }
                                }
                                if (Control == 9)
                                {
                                    SendPacket(General.MyPackets.NPCSay("�tes-vous s�r de vouloir mettre fin � la qu�te?"));
                                    SendPacket(General.MyPackets.NPCLink("Oui, mettre fin � la Qu�te.", 15));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 1)
                                {
                                    SendPacket(General.MyPackets.NPCSay("Si vous pouvez aller tuer 300 Tourterelle, la Ville Dragon deviendra beaucoup plus tranquille."));
                                    SendPacket(General.MyPackets.NPCSay(" D'ailleurs, si vous faites �quipe avec d'autres joueurs, l'�me des monstres tu�s par vos co�quipiers peuvent �tre r�cup�r�es dans votre gourde."));
                                    SendPacket(General.MyPackets.NPCLink("J'accepte la Qu�te.", 5));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 2)
                                {
                                    SendPacket(General.MyPackets.NPCSay("Si vous pouvez aller tuer 300 Rouge-Gorges, la Ville Dragon deviendra beaucoup plus tranquille."));
                                    SendPacket(General.MyPackets.NPCSay(" D'ailleurs, si vous faites �quipe avec d'autres joueurs, l'�me des monstres tu�s par vos co�quipiers peuvent �tre r�cup�r�es dans votre gourde."));
                                    SendPacket(General.MyPackets.NPCLink("J'accepte la Qu�te.", 6));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 3)
                                {
                                    SendPacket(General.MyPackets.NPCSay("Si vous pouvez aller tuer 300 Apparitions, la Ville Dragon deviendra beaucoup plus tranquille."));
                                    SendPacket(General.MyPackets.NPCSay(" D'ailleurs, si vous faites �quipe avec d'autres joueurs, l'�me des monstres tu�s par vos co�quipiers peuvent �tre r�cup�r�es dans votre gourde."));
                                    SendPacket(General.MyPackets.NPCLink("J'accepte la Qu�te.", 7));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 4)
                                {
                                    SendPacket(General.MyPackets.NPCSay("Si vous pouvez aller tuer 300 Fant�mes, la Ville Dragon deviendra beaucoup plus tranquille."));
                                    SendPacket(General.MyPackets.NPCSay(" D'ailleurs, si vous faites �quipe avec d'autres joueurs, l'�me des monstres tu�s par vos co�quipiers peuvent �tre r�cup�r�es dans votre gourde."));
                                    SendPacket(General.MyPackets.NPCLink("J'accepte la Qu�te.", 8));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 5)
                                {
                                    MyChar.QuestMob = "Tourterelle";
                                    MyChar.QuestFrom = "TC";
                                    MyChar.QuestKO = 0;
                                    MyChar.AddItem("750000-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                }
                                if (Control == 6)
                                {
                                    MyChar.QuestMob = "Rouge-Gorge";
                                    MyChar.QuestFrom = "TC";
                                    MyChar.QuestKO = 0;
                                    MyChar.AddItem("750000-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                }
                                if (Control == 7)
                                {
                                    MyChar.QuestMob = "Apparition";
                                    MyChar.QuestFrom = "TC";
                                    MyChar.QuestKO = 0;
                                    MyChar.AddItem("750000-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                }
                                if (Control == 8)
                                {
                                    MyChar.QuestMob = "Fant�me";
                                    MyChar.QuestFrom = "TC";
                                    MyChar.QuestKO = 0;
                                    MyChar.AddItem("750000-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                }
                                if (Control == 10)
                                {
                                    MyChar.Teleport(1011, 230, 258);
                                }
                                if (Control == 11)
                                {
                                    MyChar.Teleport(1020, 569, 622);
                                }
                                if (Control == 12)
                                {
                                    MyChar.Teleport(1000, 477, 634);
                                }
                                if (Control == 13)
                                {
                                    MyChar.Teleport(1015, 791, 569);
                                }
                                if (Control == 14)
                                {
                                    MyChar.Teleport(1000, 083, 319);
                                }
                            }
                            #endregion
                            if (CurrentNPC == 390)
                            {
                                if (Control == 1)
                                {
                                    SendPacket(General.MyPackets.NPCSay("Cliquez sur votre amour pour lui proposer."));
                                    SendPacket(General.MyPackets.NPCLink("OK", 2));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 2)
                                {
                                    if (MyChar.Spouse == "" || MyChar.Spouse == "Non")
                                    {
                                        MyChar.MyClient.SendPacket(Packets.MarriageMouse(MyChar.UID));
                                    }
                                }
                            }
                            if (CurrentNPC == 2565)
                            {
                                if (MyChar.NoelGift == false)
                                {
                                    if (MyChar.Level < 137)
                                    {
                                        ulong NeedExp = DataBase.NeededXP(MyChar.Level);
                                        ulong StatExp = NeedExp - MyChar.Exp + 1000;
                                        MyChar.AddExp(StatExp, false);
                                        MyChar.NoelGift = true;
                                        DataBase.SaveNoel(MyChar);
                                        SendPacket(General.MyPackets.NPCSay("Ho! Ho! Joyeux No�l!"));
                                        SendPacket(General.MyPackets.NPCLink("Merci!", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(197));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous �tes trop puissant je ne peux plus vous aider!"));
                                        SendPacket(General.MyPackets.NPCLink("Je vois...", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(197));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                            }
                            if (CurrentNPC == 3010)
                            {
                                if (Control == 1)
                                {
                                    SendPacket(General.MyPackets.NPCSay("Les monstres qui ont les cadeaux veulent une Gra�ne qui est tr�s rare. Allez voir le G�nie, il sait comment la trouv�."));
                                    SendPacket(General.MyPackets.NPCLink("J'y vais.", 255));
                                    SendPacket(General.MyPackets.NPCSetFace(6));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 2)
                                {
                                    byte Gift = (byte)General.Rand.Next(1, 2);
                                    MyChar.RemoveItem(MyChar.ItemNext(721636));
                                    if (Gift == 1)
                                        MyChar.AddItem("720393-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                    else if (Gift == 2)
                                        MyChar.AddItem("720394-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                }
                            }
                            if (CurrentNPC == 3020)
                            {
                                if (Control == 1)
                                {
                                    MyChar.RemoveItem(MyChar.ItemNext(721634));
                                    MyChar.AddItem("721635-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                    SendPacket(General.MyPackets.NPCSay("Cette lettre est en r�alit� un parchemin qui indique l'endroit o� la Gra�ne se trouve."));
                                    SendPacket(General.MyPackets.NPCLink("Je vois.", 255));
                                    SendPacket(General.MyPackets.NPCSetFace(6));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                            }
                            if (CurrentNPC == 3030)
                            {
                                if (Control == 1)
                                {
                                    if (MyChar.InventoryContains(721633, 10))
                                    {
                                        MyChar.RemoveItem(MyChar.ItemNext(721633));
                                        MyChar.RemoveItem(MyChar.ItemNext(721633));
                                        MyChar.RemoveItem(MyChar.ItemNext(721633));
                                        MyChar.RemoveItem(MyChar.ItemNext(721633));
                                        MyChar.RemoveItem(MyChar.ItemNext(721633));
                                        MyChar.RemoveItem(MyChar.ItemNext(721633));
                                        MyChar.RemoveItem(MyChar.ItemNext(721633));
                                        MyChar.RemoveItem(MyChar.ItemNext(721633));
                                        MyChar.RemoveItem(MyChar.ItemNext(721633));
                                        MyChar.RemoveItem(MyChar.ItemNext(721633));
                                        MyChar.AddItem("721634-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                        SendPacket(General.MyPackets.NPCSay("Voici une lettre. Ha! Ha! Essayer de la lire vous ne trouverai jamais la Gra�ne!"));
                                        SendPacket(General.MyPackets.NPCLink("Arnaqueur...", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(200));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous n'avez pas les 10 cadeaux!"));
                                        SendPacket(General.MyPackets.NPCLink("D�sol�.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(200));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                            }
                            if (CurrentNPC == 9999)
                            {
                                if (Control == 1)
                                {
                                    SendPacket(General.MyPackets.NPCSay("Pour le moment j'ai beaucoup de LarmeDeM�t�ore. Mais il me faut des M�t�ores. "));
                                    SendPacket(General.MyPackets.NPCSay("Si vous me donnez une m�t�orite, je peux vous divorcer de votre femme."));
                                    SendPacket(General.MyPackets.NPCLink("Voici une m�t�orite", 2));
                                    SendPacket(General.MyPackets.NPCLink("J'aime ma femme", 255));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 2)
                                {
                                    if (MyChar.InventoryContains(1088001, 1))
                                    {
                                        MyChar.RemoveItem(MyChar.ItemNext(1088001));
                                        SendPacket(General.MyPackets.NPCSay("C'est bien vous avez une m�t�orite. Mais �tes-vous certain(e) de vouloir divorcer?"));
                                        SendPacket(General.MyPackets.NPCLink("Oui.", 3));
                                        SendPacket(General.MyPackets.NPCLink("Je ne suis pas certain...", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous n'avez pas de m�t�orite... Revenez lorsque vous en aurez."));
                                        SendPacket(General.MyPackets.NPCLink("Je vois.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                                if (Control == 3)
                                {
                                    World.SendMsgToAll(MyChar.Name + " et " + MyChar.Spouse + " ont divorc�... Bonne chance lors de votre prochain mariage! Vous devez vous d�connectez pour afficher!", "Astre", 2011);
                                    MyChar.RemoveItem(MyChar.ItemNext(1088001));
                                    SendPacket(General.MyPackets.String(MyChar.UID, 6, "Non"));
                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character charc = (Character)DE.Value;
                                        if (charc.Name == MyChar.Spouse)
                                        {
                                            charc.MyClient.SendPacket(General.MyPackets.String(charc.UID, 6, "Non"));
                                        }
                                    }
                                    Ini MyCli = new Ini(System.Windows.Forms.Application.StartupPath + @"\Characters\\" + MyChar.Name + ".chr");
                                    MyCli.WriteString("Character", "Spouse", "Non");

                                    Ini SpCli = new Ini(System.Windows.Forms.Application.StartupPath + @"\Characters\\" + MyChar.Spouse + ".chr");
                                    SpCli.WriteString("Character", "Spouse", "Non");
                                }
                            }
                            if (CurrentNPC == 260) // ArenaGuard
                            {
                                if (Control == 1 && MyChar.Silvers >= 50)
                                {
                                    MyChar.Silvers -= 50;
									SendPacket(General.MyPackets.Vital(MyChar.UID, 4, MyChar.Silvers));
                                    MyChar.Teleport(1005, 51, 69);
                                }
                                if (MyChar.Silvers <= 50)
                                {
                                    SendPacket(General.MyPackets.NPCSay("Vous n'avez pas 50$."));
                                    SendPacket(General.MyPackets.NPCLink("OK", 2));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                            }
                            if (CurrentNPC == 7500)
                            {
                                if (Control == 1)
                                {
                                    if (MyChar.InventoryContains(1088000, 1))
                                    {
                                        MyChar.RemoveItem(MyChar.ItemNext(1088000));
                                        MyChar.CPs += 215;
                                        SendPacket(General.MyPackets.Vital(MyChar.UID, 30, MyChar.CPs));
                                    }
                                }
                                if (Control == 2)
                                {
                                    if (MyChar.Silvers >= 100000)
                                    {
                                        MyChar.Silvers -= 100000;
                                        MyChar.CPs += 10;
                                        SendPacket(General.MyPackets.Vital(MyChar.UID, 30, MyChar.CPs));
                                        SendPacket(General.MyPackets.Vital(MyChar.UID, 4, MyChar.Silvers));
                                    }
                                }
                                if (Control == 3)
                                {
                                    if (MyChar.Silvers >= 1000000)
                                    {
                                        MyChar.Silvers -= 1000000;
                                        MyChar.CPs += 100;
                                        SendPacket(General.MyPackets.Vital(MyChar.UID, 30, MyChar.CPs));
                                        SendPacket(General.MyPackets.Vital(MyChar.UID, 4, MyChar.Silvers));
                                    }
                                }
                            }
                            if (CurrentNPC == 6700)
                            {
                                if (Control == 1 && MyChar.MyGuild != null && MyChar.MyGuild == World.PoleHolder && MyChar.GuildPosition == 100)
                                {
                                    uint HealPole = 0;
                                    foreach (DictionaryEntry DE in NPCs.AllNPCs)
                                    {
                                        SingleNPC Npcc = (SingleNPC)DE.Value;
                                        if (Npcc.Sob == 2)
                                        {
                                            HealPole = Npcc.MaxHP - Npcc.CurHP;
                                            if (MyChar.MyGuild.Fund > HealPole)
                                            {
                                                MyChar.MyGuild.Fund -= HealPole;
                                                World.RemoveEntity(Npcc);
                                                Npcc.CurHP = Npcc.MaxHP;
                                                World.NPCSpawns(Npcc);
                                                MyChar.MyGuild.Refresh(MyChar);
                                                SendPacket(General.MyPackets.GuildInfo(MyChar.MyGuild, MyChar));
                                                World.SendMsgToAll(MyChar.Name + " des " + MyChar.MyGuild.GuildName + " a r�par� le Pole avec l'argent de ses capitaux!", "SYSTEM", 2011);
                                            }
                                        }
                                    }
                                }
                            }
                            if (CurrentNPC == 6701 || CurrentNPC == 6702)
                            {
                                if (Control == 1)
                                {
                                    if (MyChar.MyGuild != null && MyChar.MyGuild == World.PoleHolder)
                                    {
                                        SingleNPC Gate = (SingleNPC)NPCs.AllNPCs[(uint)CurrentNPC];
                                        if (Gate.Type == 240 && World.LGateDead == false)
                                            Gate.Type += 10;
                                        else if (Gate.Type == 270 && World.RGateDead == false)
                                            Gate.Type += 10;
                                        else if (Gate.Type == 250 && World.LGateDead == false)
                                            Gate.Type -= 10;
                                        else if (Gate.Type == 280 && World.RGateDead == false)
                                            Gate.Type -= 10;
                                        else if (MyChar.GuildPosition == 100 && MyChar.MyGuild.Fund >= 300000 && World.LGateDead == true || World.RGateDead == true)
                                        {
                                            if (Gate.Type == 240)
                                            {
                                                Gate.Type += 10;
                                                World.LGateDead = false;
                                            }
                                            else if (Gate.Type == 270)
                                            {
                                                Gate.Type += 10;
                                                World.RGateDead = false;
                                            }
                                            else if (Gate.Type == 250)
                                            {
                                                Gate.Type -= 10;
                                                World.LGateDead = false;
                                            }
                                            else if (Gate.Type == 280)
                                            {
                                                Gate.Type -= 10;
                                                World.RGateDead = false; 
                                            }

                                            Gate.CurHP = Gate.MaxHP;
                                            MyChar.MyGuild.Fund -= 300000;
                                            MyChar.MyGuild.Refresh(MyChar);
                                            MyChar.MyClient.SendPacket(General.MyPackets.GuildInfo(MyChar.MyGuild, MyChar));
                                        }
                                        World.NPCSpawns(Gate);
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Juste la guilde poss�dant le Pole peut contr�ler les portes."));
                                        SendPacket(General.MyPackets.NPCLink("Je vois", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                            }
                            if (CurrentNPC == 8487)
                            {
                                if (Control == 1)
                                {
                                    SendPacket(General.MyPackets.NPCSay("Voulez vous vraiment faire cela ?"));
                                    SendPacket(General.MyPackets.NPCLink("Oui je le suis.", 2));
                                    SendPacket(General.MyPackets.NPCLink("Non, j'ai chang� d'avis.", 255));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 2)
                                {
                                    SendPacket(General.MyPackets.NPCSay("Vous �tes vraiment s�re de vouloir faire ceci ?"));
                                    SendPacket(General.MyPackets.NPCLink("Oui !", 3));
                                    SendPacket(General.MyPackets.NPCLink("Non j'ai chang� d'avis.", 255));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 3)
                                {
                                    SendPacket(General.MyPackets.NPCSay("Etes vous vraiment certain ?"));
                                    SendPacket(General.MyPackets.NPCLink("Non j'ai chang� d'avis.", 255));
                                    SendPacket(General.MyPackets.NPCLink("Oui Oui Oui !", 4));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 4)
                                {
                                    SendPacket(General.MyPackets.NPCSay("C'est s�re hein qu'on vienne pas se pleindre apr�s..."));
                                    SendPacket(General.MyPackets.NPCLink("Non, j'ai chang� d'avis.", 255));
                                    SendPacket(General.MyPackets.NPCLink("Oui ! Videz mon sac !", 5));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 5)
                                {
                                    SendPacket(General.MyPackets.NPCSay("Vous �tes VRAIMENT VRAIMENT VRAIMENT VRAIMENT s�re de vouloir vider votre sac ?"));
                                    SendPacket(General.MyPackets.NPCLink("Oui !!!!", 6));
                                    SendPacket(General.MyPackets.NPCLink("Non, j'ai chang� d'avis.", 255));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 6)
                                {
                                    foreach (uint uid in MyChar.Inventory_UIDs)
                                    {
                                        if (uid != 0)
                                            SendPacket(General.MyPackets.RemoveItem(uid, 0, 3));
                                    }
                                    MyChar.Inventory_UIDs = new uint[41];
                                    MyChar.Inventory = new string[41];
                                    MyChar.ItemsInInventory = 0;
                                    SendPacket(General.MyPackets.NPCSay("Ok ! C'est bon =)"));
                                    SendPacket(General.MyPackets.NPCLink("Merci...", 255));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                            }
                            if (CurrentNPC == 9812)
                            {
                                if (Control == 1)
                                {
                                    SendPacket(General.MyPackets.NPCSay("Je peux empacter vos m�t�ore et vos Perle de Dragon pour vous. Donnez moi 10 m�t�ores ou 10 Perles de Dragon et je vais vous faire un rouleau de m�t�ores ou un rouleau de Perles de Dragon. Avec un clique droit dessus vous aurez vos m�t�ores ou vos Perles de Dragon."));
                                    SendPacket(General.MyPackets.NPCSay("J'ai une offre sp�ciale pour vous. Quatre rouleaux de m�t�ores pour une Perle de Dragon ou une Perle de Dragon pour 6 rouleaux de m�t�ores."));
                                    SendPacket(General.MyPackets.NPCLink("Cool. Empactez mes m�t�ores", 2));
                                    SendPacket(General.MyPackets.NPCLink("Cool. Empactez mes Perles de Dragon", 3));
                                    SendPacket(General.MyPackets.NPCLink("Ok. Je veux 4 rouleaux de m�t�ores pour ma Perle de Dragon.", 4));
                                    SendPacket(General.MyPackets.NPCLink("Ok. Je veux une Perle de Dragon pour mes 6 rouleaux de m�t�ores.", 5));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 2)
                                {
                                    if (MyChar.InventoryContains(1088001, 10))
                                    {
                                        MyChar.RemoveItem(MyChar.ItemNext(1088001));
                                        MyChar.RemoveItem(MyChar.ItemNext(1088001));
                                        MyChar.RemoveItem(MyChar.ItemNext(1088001));
                                        MyChar.RemoveItem(MyChar.ItemNext(1088001));
                                        MyChar.RemoveItem(MyChar.ItemNext(1088001));
                                        MyChar.RemoveItem(MyChar.ItemNext(1088001));
                                        MyChar.RemoveItem(MyChar.ItemNext(1088001));
                                        MyChar.RemoveItem(MyChar.ItemNext(1088001));
                                        MyChar.RemoveItem(MyChar.ItemNext(1088001));
                                        MyChar.RemoveItem(MyChar.ItemNext(1088001));
                                        MyChar.AddItem("720027-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                    }
                                }
                                if (Control == 3)
                                {
                                    if (MyChar.InventoryContains(1088000, 10))
                                    {
                                        MyChar.RemoveItem(MyChar.ItemNext(1088000));
                                        MyChar.RemoveItem(MyChar.ItemNext(1088000));
                                        MyChar.RemoveItem(MyChar.ItemNext(1088000));
                                        MyChar.RemoveItem(MyChar.ItemNext(1088000));
                                        MyChar.RemoveItem(MyChar.ItemNext(1088000));
                                        MyChar.RemoveItem(MyChar.ItemNext(1088000));
                                        MyChar.RemoveItem(MyChar.ItemNext(1088000));
                                        MyChar.RemoveItem(MyChar.ItemNext(1088000));
                                        MyChar.RemoveItem(MyChar.ItemNext(1088000));
                                        MyChar.RemoveItem(MyChar.ItemNext(1088000));
                                        MyChar.AddItem("720028-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                    }
                                }
                                if (Control == 4)
                                {
                                    if (MyChar.InventoryContains(1088000, 1) && MyChar.ItemsInInventory < 37)
                                    {
                                        MyChar.RemoveItem(MyChar.ItemNext(1088000));
                                        MyChar.AddItem("720027-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                        MyChar.AddItem("720027-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                        MyChar.AddItem("720027-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                        MyChar.AddItem("720027-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                    }
                                }
                                if (Control == 5)
                                {
                                    if (MyChar.InventoryContains(720027, 6))
                                    {
                                        MyChar.RemoveItem(MyChar.ItemNext(720027));
                                        MyChar.RemoveItem(MyChar.ItemNext(720027));
                                        MyChar.RemoveItem(MyChar.ItemNext(720027));
                                        MyChar.RemoveItem(MyChar.ItemNext(720027));
                                        MyChar.RemoveItem(MyChar.ItemNext(720027));
                                        MyChar.RemoveItem(MyChar.ItemNext(720027));
                                        MyChar.AddItem("1088000-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                    }
                                }
                            }
                            if (CurrentNPC == 959)
                            {
                                if (Control <= 8 && Control >= 1)
                                {
                                    GemId = (byte)Control;
                                    SendPacket(General.MyPackets.NPCSay("Quelle qualit�e de gemme voulez vous ?"));
                                    SendPacket(General.MyPackets.NPCLink("Une gemme raffin�e", 9));
                                    SendPacket(General.MyPackets.NPCLink("Une gemme super", 10));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 9)
                                {
                                    uint TheGem = (uint)(700001 + (GemId - 1) * 10);

                                    if (MyChar.InventoryContains(TheGem, 15))
                                    {
                                        for (int i = 0; i < 15; i++)
                                        {
                                            MyChar.RemoveItem(MyChar.ItemNext(TheGem));
                                        }
                                        MyChar.AddItem((TheGem + 1).ToString() + "-0-0-0-0-0", 0, (uint)General.Rand.Next(364573656));
                                    }
                                }
                                if (Control == 10)
                                {
                                    uint TheGem = (uint)(700002 + (GemId - 1) * 10);

                                    if (MyChar.InventoryContains(TheGem, 20))
                                    {
                                        for (int i = 0; i < 20; i++)
                                        {
                                            MyChar.RemoveItem(MyChar.ItemNext(TheGem));
                                        }
                                        MyChar.AddItem((TheGem + 1).ToString() + "-0-0-0-0-0", 0, (uint)General.Rand.Next(364573656));
                                    }
                                }
                            }
                            if (CurrentNPC == 999)
                            {
                                if (Control == 1)
                                {
                                    SendPacket(General.MyPackets.NPCSay("Lorsque le niveau de l'objet est plus haut que 120. L'artisant magique ne peut pas le monter de niveau. Je suis donc ici pour faire ce travail. Il me faut 1 Perle de Dragon si l'objet est plus bas que 130 et 3 si il est plus haut."));
                                    SendPacket(General.MyPackets.NPCLink("Je vois.", 255));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control <= 7 && Control >= 2)
                                {
                                    if (!UppAgree)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Il me faut une PerleDeDragon ou 3 PerleDeDragons si l'objet est plus haut que 130, vous �tes pr�t ?"));
                                        SendPacket(General.MyPackets.NPCLink("Bien s�re !", (byte)Control));
                                        SendPacket(General.MyPackets.NPCLink("Pas maintenant.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                        UppAgree = true;
                                    }
                                    else
                                    {
                                        byte Pos = 0;

                                        if (Control == 2)
                                            Pos = 4;
                                        if (Control == 3)
                                            Pos = 2;
                                        if (Control == 4)
                                            Pos = 6;
                                        if (Control == 5)
                                            Pos = 8;
                                        if (Control == 6)
                                            Pos = 3;
                                        if (Control == 7)
                                            Pos = 1;

                                        string[] Splitter = MyChar.Equips[Pos].Split('-');
                                        uint ItemId = uint.Parse(Splitter[0]);

                                        if (!Other.Upgradable(ItemId))
                                            return;
                                        if (!Other.EquipMaxedLvl(ItemId))
                                        {
                                            if (MyChar.Level >= Other.ItemInfo(Other.EquipNextLevel(ItemId))[3])
                                            {
                                                if (Other.ItemInfo(ItemId)[3] >= 120 && Other.ItemInfo(Other.EquipNextLevel(ItemId))[3] <= 130)
                                                {
                                                    if (MyChar.InventoryContains(1088000, 1))
                                                    {
                                                        MyChar.RemoveItem(MyChar.ItemNext(1088000));

                                                        ItemId = Other.EquipNextLevel(ItemId);

                                                        MyChar.GetEquipStats(Pos, true);
                                                        MyChar.Equips[Pos] = ItemId.ToString() + "-" + Splitter[1] + "-" + Splitter[2] + "-" + Splitter[3] + "-" + Splitter[4] + "-" + Splitter[5];
                                                        MyChar.GetEquipStats(Pos, false);

                                                        SendPacket(General.MyPackets.AddItem((long)MyChar.Equips_UIDs[Pos], (int)ItemId, byte.Parse(Splitter[1]), byte.Parse(Splitter[2]), byte.Parse(Splitter[3]), byte.Parse(Splitter[4]), byte.Parse(Splitter[5]), Pos, 100, 100));

                                                        SendPacket(General.MyPackets.NPCSay("Votre objet a �t� am�lior�!"));
                                                        SendPacket(General.MyPackets.NPCLink("Merci beaucoup!", 255));
                                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                                        SendPacket(General.MyPackets.NPCFinish());
                                                    }
                                                    else
                                                    {
                                                        SendPacket(General.MyPackets.NPCSay("Vous n'avez pas de Perle de Dragon"));
                                                        SendPacket(General.MyPackets.NPCLink("Je vois.", 255));
                                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                                        SendPacket(General.MyPackets.NPCFinish());
                                                    }
                                                }
                                                else if (Other.ItemInfo(Other.EquipNextLevel(ItemId))[3] >= 131 && MyChar.Level >= Other.ItemInfo(Other.EquipNextLevel(ItemId))[3])
                                                {
                                                    if (MyChar.InventoryContains(1088000, 3))
                                                    {
                                                        MyChar.RemoveItem(MyChar.ItemNext(1088000));
                                                        MyChar.RemoveItem(MyChar.ItemNext(1088000));
                                                        MyChar.RemoveItem(MyChar.ItemNext(1088000));

                                                        ItemId = Other.EquipNextLevel(ItemId);

                                                        MyChar.GetEquipStats(Pos, true);
                                                        MyChar.Equips[Pos] = ItemId.ToString() + "-" + Splitter[1] + "-" + Splitter[2] + "-" + Splitter[3] + "-" + Splitter[4] + "-" + Splitter[5];
                                                        MyChar.GetEquipStats(Pos, false);

                                                        SendPacket(General.MyPackets.AddItem((long)MyChar.Equips_UIDs[Pos], (int)ItemId, byte.Parse(Splitter[1]), byte.Parse(Splitter[2]), byte.Parse(Splitter[3]), byte.Parse(Splitter[4]), byte.Parse(Splitter[5]), Pos, 100, 100));

                                                        SendPacket(General.MyPackets.NPCSay("Votre objet a �t� am�lior�!"));
                                                        SendPacket(General.MyPackets.NPCLink("Merci beaucoup!", 255));
                                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                                        SendPacket(General.MyPackets.NPCFinish());
                                                    }
                                                    else
                                                    {
                                                        SendPacket(General.MyPackets.NPCSay("Vous n'avez pas de Perle de Dragon"));
                                                        SendPacket(General.MyPackets.NPCLink("Je vois.", 255));
                                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                                        SendPacket(General.MyPackets.NPCFinish());
                                                    }
                                                }
                                                else
                                                {
                                                    SendPacket(General.MyPackets.NPCSay("Votre �quipement est en dessous de 120, ne venez pas ici gaspiller une Perle de Dragon."));
                                                    SendPacket(General.MyPackets.NPCLink("Je vois.", 255));
                                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                                    SendPacket(General.MyPackets.NPCFinish());
                                                }
                                            }
                                            else
                                            {
                                                SendPacket(General.MyPackets.NPCSay("Vous n'avez pas le niveau requis."));
                                                SendPacket(General.MyPackets.NPCLink("Ok.", 255));
                                                SendPacket(General.MyPackets.NPCSetFace(30));
                                                SendPacket(General.MyPackets.NPCFinish());
                                            }
                                        }
                                        else
                                        {
                                            SendPacket(General.MyPackets.NPCSay("Votre �quipement est d�j� a sont plus grand niveau."));
                                            SendPacket(General.MyPackets.NPCLink("Ok.", 255));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                    }
                                }
                            }
                            if (CurrentNPC == 700)
                            {
                                if (Control == 1)
                                {
                                    SendPacket(General.MyPackets.NPCSay("Ah, excellent. Augmenter la qualit�e d'un objet am�liore ses comp�tences. Que voulez vous upgrader ?"));
                                    SendPacket(General.MyPackets.NPCLink("Monter le coronet, casque, boucles.", 3));
                                    SendPacket(General.MyPackets.NPCLink("Monter le collier de qualit�.", 4));
                                    SendPacket(General.MyPackets.NPCLink("Monter l'armure de qualit�.", 5));
                                    SendPacket(General.MyPackets.NPCLink("Monter la qualit� de l'arme.", 6));
                                    SendPacket(General.MyPackets.NPCLink("Monter la qualit� de la bague.", 7));
                                    SendPacket(General.MyPackets.NPCLink("Monter la qualit� des bottes.", 8));
                                    SendPacket(General.MyPackets.NPCLink("Monter la qualit� du bouclier.", 9));
                                    SendPacket(General.MyPackets.NPCLink("Au revoir.", 255));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                    UppAgree = false;
                                }
                                if (Control == 2)
                                {
                                    SendPacket(General.MyPackets.NPCSay("Je vois, monter le niveau d'un objet pour le rendre plus puissant. C'est tr�s dure de faire cela. Que monter de niveau ?"));
                                    SendPacket(General.MyPackets.NPCLink("Casque, boucle, coronet", 10));
                                    SendPacket(General.MyPackets.NPCLink("Monter le niveau du collier.", 11));
                                    SendPacket(General.MyPackets.NPCLink("Monter le niveau de l'armure.", 12));
                                    SendPacket(General.MyPackets.NPCLink("Monter le niveau de l'arme.", 13));
                                    SendPacket(General.MyPackets.NPCLink("Monter le niveau de la bague, anneau.", 14));
                                    SendPacket(General.MyPackets.NPCLink("Monter le niveau des bottes.", 15));
                                    SendPacket(General.MyPackets.NPCLink("Monter le niveau du bouclier.", 16));
                                    SendPacket(General.MyPackets.NPCLink("Au revoir.", 255));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                    UppAgree = false;
                                }
                                if (Control <= 9 && Control >= 3)
                                {
                                    string TheEquip = "";

                                    if (Control == 3)
                                        TheEquip = MyChar.Equips[1];
                                    if (Control == 4)
                                        TheEquip = MyChar.Equips[2];
                                    if (Control == 5)
                                        TheEquip = MyChar.Equips[3];
                                    if (Control == 6)
                                        TheEquip = MyChar.Equips[4];
                                    if (Control == 7)
                                        TheEquip = MyChar.Equips[6];
                                    if (Control == 8)
                                        TheEquip = MyChar.Equips[8];
                                    if (Control == 9)
                                        TheEquip = MyChar.Equips[5];

                                    byte Pos = 0;

                                    if (Control == 3)
                                        Pos = 1;
                                    if (Control == 4)
                                        Pos = 2;
                                    if (Control == 5)
                                        Pos = 3;
                                    if (Control == 6)
                                        Pos = 4;
                                    if (Control == 7)
                                        Pos = 6;
                                    if (Control == 8)
                                        Pos = 8;
                                    if (Control == 9)
                                        Pos = 5;


                                    string[] Splitter = TheEquip.Split('-');
                                    uint ItemId = uint.Parse(Splitter[0]);

                                    if (!Other.Upgradable(ItemId) || Other.ItemQuality(ItemId) == 9)
                                        return;

                                    byte RequiredDBs = 0;
                                    RequiredDBs = (byte)(Other.ItemInfo(ItemId)[3] / 20);
                                    if (RequiredDBs == 0)
                                        RequiredDBs = 1;

                                    if (Other.ItemQuality(ItemId) == 6)
                                        RequiredDBs += 2;
                                    if (Other.ItemQuality(ItemId) == 7)
                                        RequiredDBs += 3;
                                    if (Other.ItemQuality(ItemId) == 8)
                                        RequiredDBs += 4;

                                    if (!UppAgree)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous avez besoin de " + RequiredDBs + " Perles de Dragon pour l'am�liorer. Voulez-vous l'am�liorer?"));
                                        SendPacket(General.MyPackets.NPCLink("Oui", (byte)Control));
                                        SendPacket(General.MyPackets.NPCLink("Non, j'ai chang� d'avis.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else
                                    {
                                        if (MyChar.InventoryContains(1088000, RequiredDBs))
                                        {
                                            for (int i = 0; i < RequiredDBs; i++)
                                            {
                                                MyChar.RemoveItem(MyChar.ItemNext(1088000));
                                            }

                                            if (Other.ItemQuality(ItemId) < 6)
                                                ItemId = Other.ItemQualityChange(ItemId, 6);
                                            else
                                                ItemId++;

                                            MyChar.GetEquipStats(Pos, true);
                                            MyChar.Equips[Pos] = ItemId.ToString() + "-" + Splitter[1] + "-" + Splitter[2] + "-" + Splitter[3] + "-" + Splitter[4] + "-" + Splitter[5];
                                            MyChar.GetEquipStats(Pos, false);

                                            SendPacket(General.MyPackets.AddItem((long)MyChar.Equips_UIDs[Pos], (int)ItemId, byte.Parse(Splitter[1]), byte.Parse(Splitter[2]), byte.Parse(Splitter[3]), byte.Parse(Splitter[4]), byte.Parse(Splitter[5]), Pos, 100, 100));
                                        }
                                        else
                                        {
                                            SendPacket(General.MyPackets.NPCSay("Vous n'avez pas assez de Perles de Dragon."));
                                            SendPacket(General.MyPackets.NPCLink("Je vois.", 255));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                    }

                                    UppAgree = true;
                                }
                                if (Control <= 16 && Control >= 10)
                                {
                                    string TheEquip = "";

                                    if (Control == 10)
                                        TheEquip = MyChar.Equips[1];
                                    if (Control == 11)
                                        TheEquip = MyChar.Equips[2];
                                    if (Control == 12)
                                        TheEquip = MyChar.Equips[3];
                                    if (Control == 13)
                                        TheEquip = MyChar.Equips[4];
                                    if (Control == 14)
                                        TheEquip = MyChar.Equips[6];
                                    if (Control == 15)
                                        TheEquip = MyChar.Equips[8];
                                    if (Control == 16)
                                        TheEquip = MyChar.Equips[5];

                                    byte Pos = 0;

                                    if (Control == 10)
                                        Pos = 1;
                                    if (Control == 11)
                                        Pos = 2;
                                    if (Control == 12)
                                        Pos = 3;
                                    if (Control == 13)
                                        Pos = 4;
                                    if (Control == 14)
                                        Pos = 6;
                                    if (Control == 15)
                                        Pos = 8;
                                    if (Control == 16)
                                        Pos = 5;

                                    string[] Splitter = TheEquip.Split('-');
                                    uint ItemId = uint.Parse(Splitter[0]);

                                    if (!Other.Upgradable(ItemId))
                                        return;



                                    byte RequiredMets = 0;
                                    if ((Other.ItemInfo(ItemId)[3] < 120 && Other.ItemType2(ItemId) != 90) || (Other.ItemInfo(ItemId)[3] < 110))
                                    {
                                        RequiredMets = (byte)(Other.ItemInfo(ItemId)[3] / 10);
                                        if (RequiredMets == 0)
                                            RequiredMets = 1;
                                    }
                                    if (RequiredMets != 0)
                                    {
                                        if (Other.ItemQuality(ItemId) < 7)
                                            RequiredMets = 2;
                                        if (Other.ItemQuality(ItemId) == 7)
                                            RequiredMets = (byte)(2 + RequiredMets / 5);
                                        if (Other.ItemQuality(ItemId) == 8)
                                            RequiredMets = (byte)(RequiredMets * 2.6);
                                        if (Other.ItemQuality(ItemId) == 9)
                                            RequiredMets = (byte)(RequiredMets * 3.1);
                                    }

                                    if (RequiredMets != 0)
                                    {
                                        if (!UppAgree)
                                        {
                                            SendPacket(General.MyPackets.NPCSay("Il vous faut " + RequiredMets + " m�t�orites pour que vous am�liorez l'objet. Voulez-vous quand m�me?"));
                                            SendPacket(General.MyPackets.NPCLink("Oui", (byte)Control));
                                            SendPacket(General.MyPackets.NPCLink("Non, j'ai chang� d'avis.", 255));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                        else
                                        {
                                            if (MyChar.InventoryContains(1088001, RequiredMets) && MyChar.Level >= Other.ItemInfo(Other.EquipNextLevel(ItemId))[3])
                                            {
                                                ItemId = Other.EquipNextLevel(ItemId);

                                                for (int i = 0; i < RequiredMets; i++)
                                                {
                                                    MyChar.RemoveItem(MyChar.ItemNext(1088001));
                                                }

                                                MyChar.GetEquipStats(Pos, true);
                                                MyChar.Equips[Pos] = ItemId.ToString() + "-" + Splitter[1] + "-" + Splitter[2] + "-" + Splitter[3] + "-" + Splitter[4] + "-" + Splitter[5];
                                                MyChar.GetEquipStats(Pos, false);

                                                SendPacket(General.MyPackets.AddItem((long)MyChar.Equips_UIDs[Pos], (int)ItemId, byte.Parse(Splitter[1]), byte.Parse(Splitter[2]), byte.Parse(Splitter[3]), byte.Parse(Splitter[4]), byte.Parse(Splitter[5]), Pos, 100, 100));

                                                SendPacket(General.MyPackets.NPCSay("Votre objet a �t� am�lior� avec succ�s."));
                                                SendPacket(General.MyPackets.NPCLink("Merci beaucoup!", 255));
                                                SendPacket(General.MyPackets.NPCSetFace(30));
                                                SendPacket(General.MyPackets.NPCFinish());
                                            }
                                            else
                                            {
                                                SendPacket(General.MyPackets.NPCSay("Vous n'avez pas assez de m�t�ores ou vous n'avez pas le niveau requis pour le prochain niveau de l'objet."));
                                                SendPacket(General.MyPackets.NPCLink("Je vois.", 255));
                                                SendPacket(General.MyPackets.NPCSetFace(30));
                                                SendPacket(General.MyPackets.NPCFinish());
                                            }

                                            UppAgree = false;
                                        }
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Je ne peux plus am�liorer votre objet. Il est � son plus haut niveau."));
                                        SendPacket(General.MyPackets.NPCLink("Je vois.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    UppAgree = true;
                                }
                            }

                            if (CurrentNPC < 618 && CurrentNPC > 613)
                                if (Control == 1)
                                {
                                    if (MyChar.Silvers >= 500)
                                    {
                                        MyChar.Teleport(1038, 350, 339);
                                        MyChar.Silvers -= 500;
                                        SendPacket(General.MyPackets.Vital(MyChar.UID, 4, MyChar.Silvers));
                                    }
                                }
                            if (CurrentNPC == 610)
                            {
                                if (Control == 1)
                                {
                                    if (MyChar.Silvers >= 1000)
                                        if (DataBase.GC1Map != 0)
                                        {
                                            MyChar.Teleport(DataBase.GC1Map, (ushort)(DataBase.GC1X - 2), DataBase.GC1Y);
                                            MyChar.Silvers -= 1000;
                                            SendPacket(General.MyPackets.Vital(MyChar.UID, 4, MyChar.Silvers));
                                        }
                                }
                                if (Control == 2)
                                {
                                    if (MyChar.GuildPosition == 100 && MyChar.MyGuild.Fund >= 50000)
                                    {
                                        MyChar.MyGuild.Fund -= 50000;
                                        MyChar.MyGuild.Refresh(MyChar);
                                        MyChar.MyClient.SendPacket(General.MyPackets.GuildInfo(MyChar.MyGuild, MyChar));
                                        MyChar.AddItem("720021-0-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                                    }
                                }
                            }
                            if (CurrentNPC == 611)
                            {
                                if (Control == 1)
                                {
                                    if (MyChar.Silvers >= 1000)
                                        if (DataBase.GC2Map != 0)
                                        {
                                            MyChar.Teleport(DataBase.GC2Map, (ushort)(DataBase.GC2X - 2), DataBase.GC2Y);
                                            MyChar.Silvers -= 1000;
                                            SendPacket(General.MyPackets.Vital(MyChar.UID, 4, MyChar.Silvers));
                                        }
                                }
                                if (Control == 2)
                                {
                                    if (MyChar.GuildPosition == 100 && MyChar.MyGuild.Fund >= 50000)
                                    {
                                        MyChar.MyGuild.Fund -= 50000;
                                        MyChar.MyGuild.Refresh(MyChar);
                                        MyChar.MyClient.SendPacket(General.MyPackets.GuildInfo(MyChar.MyGuild, MyChar));
                                        MyChar.AddItem("720022-0-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                                    }
                                }
                            }
                            if (CurrentNPC == 612)
                            {
                                if (Control == 1)
                                {
                                    if (MyChar.Silvers >= 1000)
                                        if (DataBase.GC3Map != 0)
                                        {
                                            MyChar.Teleport(DataBase.GC3Map, (ushort)(DataBase.GC3X - 2), DataBase.GC3Y);
                                            MyChar.Silvers -= 1000;
                                            SendPacket(General.MyPackets.Vital(MyChar.UID, 4, MyChar.Silvers));
                                        }
                                }
                                if (Control == 2)
                                {
                                    if (MyChar.GuildPosition == 100 && MyChar.MyGuild.Fund >= 50000)
                                    {
                                        MyChar.MyGuild.Fund -= 50000;
                                        MyChar.MyGuild.Refresh(MyChar);
                                        MyChar.MyClient.SendPacket(General.MyPackets.GuildInfo(MyChar.MyGuild, MyChar));
                                        MyChar.AddItem("720023-0-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                                    }
                                }
                            }
                            if (CurrentNPC == 613)
                            {
                                if (Control == 1)
                                {
                                    if (MyChar.Silvers >= 1000)
                                        if (DataBase.GC4Map != 0)
                                        {
                                            MyChar.Teleport(DataBase.GC4Map, (ushort)(DataBase.GC4X - 2), DataBase.GC4Y);
                                            MyChar.Silvers -= 1000;
                                            SendPacket(General.MyPackets.Vital(MyChar.UID, 4, MyChar.Silvers));
                                        }
                                }
                                if (Control == 2)
                                {
                                    if (MyChar.GuildPosition == 100 && MyChar.MyGuild.Fund >= 50000)
                                    {
                                        MyChar.MyGuild.Fund -= 50000;
                                        MyChar.MyGuild.Refresh(MyChar);
                                        MyChar.MyClient.SendPacket(General.MyPackets.GuildInfo(MyChar.MyGuild, MyChar));
                                        MyChar.AddItem("720024-0-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                                    }
                                }
                            }
                            if (CurrentNPC == 6183)
                            {
                                if (Control == 1)
                                {
                                    if (MyChar.Blessed == true)
                                    {
                                        MyChar.Teleport(601, 64, 55);
                                        MyChar.OfflineTG = true;
                                        MyChar.MyClient.Drop();

                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous n'�tes pas niveau 50 et vous n'�tes pas en b�n�diction..."));
                                        SendPacket(General.MyPackets.NPCLink("Envoyez moi juste au TG normal...", 1));
                                        SendPacket(General.MyPackets.NPCLink("Je m'en vais...", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                            } 
                            if (CurrentNPC == 198)
                                if (Control == 1)
                                    MyChar.Teleport(1000, 971, 666);

                            if (CurrentNPC == 573)
                                if (Control == 1)
                                    MyChar.Teleport(1028, 160, 96);

                            if (CurrentNPC == 600)
                            {
                                if (Control == 1)
                                    MyChar.Teleport(1038, 351, 341);
                                if (Control == 2)
                                {
                                    if (MyChar.MyGuild != null && MyChar.MyGuild == World.PoleHolder && World.GWOn != true && MyChar.GuildPosition == 100 && !MyChar.MyGuild.ClaimedPrize && MyChar.ItemsInInventory < 40)
                                    {
                                        MyChar.AddItem("720028-0-0-0-0-0", 0, (uint)General.Rand.Next(345636635));
                                        MyChar.MyGuild.ClaimedPrize = true;
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Votre guilde n'est pas victorieuse ou la GW n'est pas fini. Aussi seul le chef de guilde peut r�clamer le prix."));
                                        SendPacket(General.MyPackets.NPCLink("Ok.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                            }

                            if (CurrentNPC == 601)
                                if (Control == 1)
                                    MyChar.Teleport(1002, 430, 380);

                            if (CurrentNPC >= 104 && CurrentNPC <= 107)
                            {
                                if (Control == 1)
                                {
                                    ushort ToX = 300;
                                    ushort ToY = 300;
                                    if (CurrentNPC == 107)
                                    {
                                        ToX = 1010;
                                        ToY = 710;
                                    }
                                    if (CurrentNPC == 104)
                                    {
                                        ToX = 11;
                                        ToY = 376;
                                    }
                                    if (CurrentNPC == 105)
                                    {
                                        ToX = 381;
                                        ToY = 21;
                                    }
                                    if (CurrentNPC == 106)
                                    {
                                        ToX = 971;
                                        ToY = 666;
                                    }
                                    MyChar.Teleport(MyChar.LocMap, ToX, ToY);
                                }
                                if (Control == 2)
                                    MyChar.Teleport(1036, 211, 196);
                                if (Control == 3)
                                    MyChar.Teleport(1000, 84, 328);
                            }
                            if (CurrentNPC == 155)
                            {
                                if (Control == 6)
                                {
                                    MyChar.MyGuild.Disband(MyChar);
                                }
                                if (Control == 5)
                                {
                                    if (MyChar.GuildPosition == 100)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("�tes-vous sure de vouloir d�grouper votre guilde?"));
                                        SendPacket(General.MyPackets.NPCLink("Oui.", 6));
                                        SendPacket(General.MyPackets.NPCLink("Non pas maintenant.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Que le chef de guilde peut d�grouper sa guilde."));
                                        SendPacket(General.MyPackets.NPCLink("Ok.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                                if (Control == 3)
                                {
                                    SendPacket(General.MyPackets.NPCSay("Entrez le nom du membre que vous voulez mettre sous-chef."));
                                    SendPacket(General.MyPackets.NPCLink2("Commettre un sous-chef.", 4));
                                    SendPacket(General.MyPackets.NPCLink("Rien du tout.", 255));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 4)
                                {
                                    if (MyChar.GuildPosition == 100)
                                    {
                                        if (MyChar.MyGuild.DLs.Count <= 6)
                                        {
                                            string Name = "";
                                            for (int i = 14; i < 14 + Data[13]; i++)
                                            {
                                                Name += Convert.ToChar(Data[i]);
                                            }

                                            uint CharID = 0;

                                            foreach (DictionaryEntry DE in MyChar.MyGuild.Members)
                                            {
                                                string nm = (string)DE.Value;
                                                string[] Splitter = nm.Split(':');

                                                if (Splitter[0] == Name)
                                                    CharID = uint.Parse(Splitter[1]);
                                            }

                                            if (World.AllChars.Contains(CharID))
                                            {
                                                if (MyChar.MyGuild.Members.Contains(CharID))
                                                    MyChar.MyGuild.Members.Remove(CharID);
                                                Character Char = (Character)World.AllChars[CharID];
                                                Char.GuildPosition = 90;

                                                MyChar.MyGuild.DLs.Add(CharID, Char.Name + ":" + Char.UID.ToString() + ":" + Char.Level.ToString() + ":" + Char.GuildDonation.ToString());

                                                Char.MyClient.SendPacket(General.MyPackets.GuildInfo(Char.MyGuild, Char));
                                            }
                                            else
                                            {
                                                SendPacket(General.MyPackets.NPCSay("Le joueur que vous voulez mettre sous-chef doit �tre en-ligne et dans votre guilde."));
                                                SendPacket(General.MyPackets.NPCLink("Je vois.", 255));
                                                SendPacket(General.MyPackets.NPCSetFace(30));
                                                SendPacket(General.MyPackets.NPCFinish());
                                            }
                                        }
                                        else
                                        {
                                            SendPacket(General.MyPackets.NPCSay("Une guilde peut avoir que 6 sous-chefs."));
                                            SendPacket(General.MyPackets.NPCLink("Ok.", 255));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Seul le chef de guilde peut commettre un sous-chef."));
                                        SendPacket(General.MyPackets.NPCLink("Je vois.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                                if (Control == 1)
                                {
                                    if (MyChar.MyGuild == null && MyChar.GuildPosition == 0)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Cela va vous couter 1 000 000$ et vous devez �tre niveau 90."));
                                        SendPacket(General.MyPackets.NPCLink2("Cr�er une guilde", 2));
                                        SendPacket(General.MyPackets.NPCLink("Rien du tout.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                                if (Control == 2)
                                {
                                    if (MyChar.MyGuild == null && MyChar.GuildPosition == 0)
                                    {
                                        if (MyChar.Silvers >= 1000000)
                                        {
                                            if (MyChar.Level >= 90)
                                            {
                                                string GuildName = "";
                                                for (int i = 14; i < 14 + Data[13]; i++)
                                                {
                                                    GuildName += Convert.ToChar(Data[i]);
                                                }

                                                MyChar.Silvers -= 1000000;
                                                SendPacket(General.MyPackets.Vital(MyChar.UID, 4, MyChar.Silvers));

                                                ushort GuildID = (ushort)General.Rand.Next(0, 65000);

                                                DataBase.NewGuild(GuildID, GuildName, MyChar);
                                                Guilds.NewGuild(GuildID, GuildName, MyChar);
                                                MyChar.GuildID = GuildID;
                                                MyChar.GuildPosition = 100;
                                                MyChar.GuildDonation = 1000000;
                                                MyChar.MyGuild = (Guild)Guilds.AllGuilds[GuildID];

                                                SendPacket(General.MyPackets.GuildName(MyChar.GuildID, MyChar.MyGuild.GuildName));
                                                SendPacket(General.MyPackets.GuildInfo(MyChar.MyGuild, MyChar));
                                                SendPacket(General.MyPackets.GuildName(MyChar.GuildID, MyChar.MyGuild.GuildName));
                                            }
                                            else
                                            {
                                                SendPacket(General.MyPackets.NPCSay("Vous n'�tes pas assez fort."));
                                                SendPacket(General.MyPackets.NPCLink("Ok.", 255));
                                                SendPacket(General.MyPackets.NPCSetFace(30));
                                                SendPacket(General.MyPackets.NPCFinish());
                                            }
                                        }
                                        else
                                        {
                                            SendPacket(General.MyPackets.NPCSay("Vous n'avez pas assez d'argent."));
                                            SendPacket(General.MyPackets.NPCLink("Ok.", 255));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                    }

                                }
                            }
                            if (CurrentNPC < 506 && CurrentNPC > 499)
                            {
                                if (Control == 1)
                                {
                                    MyChar.LotoCount++;
                                    MyChar.Teleport(1036, 219, 192);
                                    if (Other.ChanceSuccess(25))
                                    {
                                        byte Quality = 8;

                                        if (Other.ChanceSuccess(15))
                                            Quality = 9;

                                        byte Plus = 0;

                                        if (Quality == 8)
                                            if (Other.ChanceSuccess(1))
                                                Plus = 8;

                                        byte Soc1 = 0;
                                        byte Soc2 = 0;

                                        if (Plus == 0)
                                            if (Quality == 8)
                                                if (Other.ChanceSuccess(1))
                                                {
                                                    Soc1 = 255;
                                                    if (Other.ChanceSuccess(0.5))
                                                        Soc2 = 255;
                                                }

                                        byte Enchant = (byte)General.Rand.Next(0, 255);
                                        byte Bless = 0;
                                        if (Other.ChanceSuccess(20))
                                            Bless = (byte)General.Rand.Next(0, 3);

                                        uint EquipID = Other.GenerateEquip((byte)General.Rand.Next(15, 119), Quality);
                                        while (EquipID == 0)
                                        {
                                            EquipID = Other.GenerateEquip((byte)General.Rand.Next(15, 119), Quality);
                                        }

                                        MyChar.AddItem(EquipID.ToString() + "-" + Plus.ToString() + "-" + Bless.ToString() + "-" + Enchant.ToString() + "-" + Soc1.ToString() + "-" + Soc2.ToString(), 0, (uint)General.Rand.Next(263573635));
                                    }
                                    else if (Other.ChanceSuccess(15))
                                    {
                                        MyChar.AddItem(Other.GenerateGarment().ToString() + "-0-0-0-0-0", 0, (uint)General.Rand.Next(263573635));
                                    }
                                    else if (Other.ChanceSuccess(20))
                                    {
                                        uint ID = Other.GenerateEtc();
                                        if (Other.ItemType2(ID) == 73)
                                            MyChar.AddItem(ID.ToString() + "-" + (ID - 730000) + "-0-0-0-0", 0, (uint)General.Rand.Next(263573635));
                                        else
                                            MyChar.AddItem(ID.ToString() + "-0-0-0-0-0", 0, (uint)General.Rand.Next(263573635));
                                    }
                                    else if (Other.ChanceSuccess(25))
                                    {
                                        MyChar.AddItem(Other.GenerateSpecial().ToString() + "-0-0-0-0-0", 0, (uint)General.Rand.Next(263573635));
                                    }
                                    else
                                    {
                                        uint ID = Other.GenerateCrap();
                                        if (ID != 730001)
                                            MyChar.AddItem(ID.ToString() + "-0-0-0-0-0", 0, (uint)General.Rand.Next(263573635));
                                        else
                                            MyChar.AddItem("730001-1-0-0-0-0", 0, (uint)General.Rand.Next(263573635));
                                    }
                                }
                            }
                            if (CurrentNPC < 10013 && CurrentNPC > 9999)
                            {
                                if (MyChar.ItemsInInventory < 40)
                                {
                                    if (Control == 1)
                                    {
                                        MyChar.LotoCount++;
                                        MyChar.Teleport(1036, 219, 192);

                                        if (Other.ChanceSuccess(15))
                                        {
                                            byte Quality = 8;

                                            if (Other.ChanceSuccess(5))
                                                Quality = 9;

                                            byte Plus = 0;

                                            if (Quality == 8)
                                                if (Other.ChanceSuccess(1))
                                                    Plus = 8;

                                            byte Soc1 = 0;
                                            byte Soc2 = 0;

                                            if (Plus == 0)
                                                if (Quality == 8)
                                                    if (Other.ChanceSuccess(1))
                                                    {
                                                        Soc1 = 255;
                                                        if (Other.ChanceSuccess(0.5))
                                                            Soc2 = 255;
                                                    }

                                            byte Enchant = (byte)General.Rand.Next(0, 255);
                                            byte Bless = (byte)General.Rand.Next(0, 7);
                                            if (Other.ChanceSuccess(20))
                                                Bless = (byte)General.Rand.Next(0, 3);

                                            uint EquipID = Other.GenerateEquip((byte)General.Rand.Next(15, 119), Quality);
                                            while (EquipID == 0)
                                            {
                                                EquipID = Other.GenerateEquip((byte)General.Rand.Next(15, 119), Quality);
                                            }

                                            MyChar.AddItem(EquipID.ToString() + "-" + Plus.ToString() + "-" + Bless.ToString() + "-" + Enchant.ToString() + "-" + Soc1.ToString() + "-" + Soc2.ToString(), 0, (uint)General.Rand.Next(263573635));
                                        }
                                        else if (Other.ChanceSuccess(15))
                                        {
                                            MyChar.AddItem(Other.GenerateGarment().ToString() + "-0-0-0-0-0", 0, (uint)General.Rand.Next(263573635));
                                        }
                                        else if (Other.ChanceSuccess(20))
                                        {
                                            uint ID = Other.GenerateEtc();
                                            if (Other.ItemType2(ID) == 73)
                                                MyChar.AddItem(ID.ToString() + "-" + (ID - 730000) + "-0-0-0-0", 0, (uint)General.Rand.Next(263573635));
                                            else
                                                MyChar.AddItem(ID.ToString() + "-0-0-0-0-0", 0, (uint)General.Rand.Next(263573635));
                                        }
                                        else if (Other.ChanceSuccess(25))
                                        {
                                            MyChar.AddItem(Other.GenerateSpecial().ToString() + "-0-0-0-0-0", 0, (uint)General.Rand.Next(263573635));
                                        }
                                        else
                                        {
                                            uint ID = Other.GenerateCrap();
                                            if (ID != 730001)
                                                MyChar.AddItem(ID.ToString() + "-0-0-0-0-0", 0, (uint)General.Rand.Next(263573635));
                                            else
                                                MyChar.AddItem("730001-1-0-0-0-0", 0, (uint)General.Rand.Next(263573635));
                                        }
                                    }
                                }
                                else
                                {
                                    SendPacket(General.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "Votre sac est complet!", 2005));
                                }
                            }
                            if (CurrentNPC < 10020 && CurrentNPC > 10014)
                            {
                                if (MyChar.ItemsInInventory < 40)
                                {
                                    if (Control == 1)
                                    {
                                        MyChar.LotoCount++;
                                        MyChar.Teleport(1036, 219, 192);

                                        if (Other.ChanceSuccess(15))
                                        {
                                            byte Quality = 8;

                                            if (Other.ChanceSuccess(5))
                                                Quality = 9;

                                            byte Plus = 0;

                                            if (Quality == 8)
                                                if (Other.ChanceSuccess(1))
                                                    Plus = 8;

                                            byte Soc1 = 0;
                                            byte Soc2 = 0;

                                            if (Plus == 0)
                                                if (Quality == 8)
                                                    if (Other.ChanceSuccess(1))
                                                    {
                                                        Soc1 = 255;
                                                        if (Other.ChanceSuccess(0.5))
                                                            Soc2 = 255;
                                                    }

                                            byte Enchant = (byte)General.Rand.Next(0, 255);
                                            byte Bless = (byte)General.Rand.Next(0, 8);
                                            if (Other.ChanceSuccess(20))
                                                Bless = (byte)General.Rand.Next(0, 3);

                                            uint EquipID = Other.GenerateEquip((byte)General.Rand.Next(15, 119), Quality);
                                            while (EquipID == 0)
                                            {
                                                EquipID = Other.GenerateEquip((byte)General.Rand.Next(15, 119), Quality);
                                            }

                                            MyChar.AddItem(EquipID.ToString() + "-" + Plus.ToString() + "-" + Bless.ToString() + "-" + Enchant.ToString() + "-" + Soc1.ToString() + "-" + Soc2.ToString(), 0, (uint)General.Rand.Next(263573635));
                                        }
                                        else if (Other.ChanceSuccess(15))
                                        {
                                            MyChar.AddItem(Other.GenerateGarment().ToString() + "-0-0-0-0-0", 0, (uint)General.Rand.Next(263573635));
                                        }
                                        else if (Other.ChanceSuccess(20))
                                        {
                                            uint ID = Other.GenerateEtc();
                                            if (Other.ItemType2(ID) == 73)
                                                MyChar.AddItem(ID.ToString() + "-" + (ID - 730000) + "-0-0-0-0", 0, (uint)General.Rand.Next(263573635));
                                            else
                                                MyChar.AddItem(ID.ToString() + "-0-0-0-0-0", 0, (uint)General.Rand.Next(263573635));
                                        }
                                        else if (Other.ChanceSuccess(25))
                                        {
                                            MyChar.AddItem(Other.GenerateSpecial().ToString() + "-0-0-0-0-0", 0, (uint)General.Rand.Next(263573635));
                                        }
                                        else
                                        {
                                            uint ID = Other.GenerateCrap();
                                            if (ID != 730001)
                                                MyChar.AddItem(ID.ToString() + "-0-0-0-0-0", 0, (uint)General.Rand.Next(263573635));
                                            else
                                                MyChar.AddItem("730001-1-0-0-0-0", 0, (uint)General.Rand.Next(263573635));
                                        }
                                    }
                                }
                                else
                                {
                                    SendPacket(General.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "Votre sac est complet!", 2005));
                                }
                            }
                            if (CurrentNPC == 10020)
                            {
                                if (MyChar.ItemsInInventory < 40)
                                {
                                    if (Control == 1)
                                    {
                                        MyChar.LotoCount++;
                                        MyChar.Teleport(1036, 219, 192);

                                        if (Other.ChanceSuccess(25))
                                        {
                                            byte Quality = 8;

                                            if (Other.ChanceSuccess(15))
                                                Quality = 9;

                                            byte Plus = 0;

                                            if (Quality == 8)
                                                if (Other.ChanceSuccess(1))
                                                    Plus = 8;

                                            byte Soc1 = 0;
                                            byte Soc2 = 0;

                                            if (Plus == 0)
                                                if (Quality == 8)
                                                    if (Other.ChanceSuccess(1))
                                                    {
                                                        Soc1 = 255;
                                                        if (Other.ChanceSuccess(0.5))
                                                            Soc2 = 255;
                                                    }

                                            byte Enchant = (byte)General.Rand.Next(0, 255);
                                            byte Bless = (byte)General.Rand.Next(0, 8);
                                            if (Other.ChanceSuccess(20))
                                                Bless = (byte)General.Rand.Next(0, 3);

                                            uint EquipID = Other.GenerateEquip((byte)General.Rand.Next(15, 119), Quality);
                                            while (EquipID == 0)
                                            {
                                                EquipID = Other.GenerateEquip((byte)General.Rand.Next(15, 119), Quality);
                                            }

                                            MyChar.AddItem(EquipID.ToString() + "-" + Plus.ToString() + "-" + Bless.ToString() + "-" + Enchant.ToString() + "-" + Soc1.ToString() + "-" + Soc2.ToString(), 0, (uint)General.Rand.Next(263573635));
                                        }
                                        else if (Other.ChanceSuccess(15))
                                        {
                                            MyChar.AddItem(Other.GenerateGarment().ToString() + "-0-0-0-0-0", 0, (uint)General.Rand.Next(263573635));
                                        }
                                        else if (Other.ChanceSuccess(20))
                                        {
                                            uint ID = Other.GenerateEtc();
                                            if (Other.ItemType2(ID) == 73)
                                                MyChar.AddItem(ID.ToString() + "-" + (ID - 730000) + "-0-0-0-0", 0, (uint)General.Rand.Next(263573635));
                                            else
                                                MyChar.AddItem(ID.ToString() + "-0-0-0-0-0", 0, (uint)General.Rand.Next(263573635));
                                        }
                                        else if (Other.ChanceSuccess(25))
                                        {
                                            MyChar.AddItem(Other.GenerateSpecial().ToString() + "-0-0-0-0-0", 0, (uint)General.Rand.Next(263573635));
                                        }
                                        else
                                        {
                                            uint ID = Other.GenerateCrap();
                                            if (ID != 730001)
                                                MyChar.AddItem(ID.ToString() + "-0-0-0-0-0", 0, (uint)General.Rand.Next(263573635));
                                            else
                                                MyChar.AddItem("730001-1-0-0-0-0", 0, (uint)General.Rand.Next(263573635));
                                        }
                                    }
                                }
                                else
                                {
                                    SendPacket(General.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "Votre sac est complet!", 2005));
                                }
                            }
                            if (CurrentNPC == 1250)
                            {
                                if (Control == 1)
                                {
                                    if (MyChar.PKPoints >= 100)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous ne pouvez pas sortir dehors. Vous avez " + MyChar.PKPoints + "pk points. Pour sortir vous devez payer 108 CPs si vous avez 100 � 300 pk points et si vous en avez 301+ vous devez payer 504 CPs."));
                                        SendPacket(General.MyPackets.NPCLink("Je veux payer.", 3));
                                        SendPacket(General.MyPackets.NPCLink("Oh,ok.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous pouvez sortir de prison quand vous voulez. Voulez-vous sortir maintenant?"));
                                        SendPacket(General.MyPackets.NPCLink("Oui.", 2));
                                        SendPacket(General.MyPackets.NPCLink("Non.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                                if (Control == 2)
                                {
                                    MyChar.Teleport(1002, 517, 351);
                                }
                                if (Control == 3)
                                {
                                    if (MyChar.PKPoints >= 100 && MyChar.PKPoints <= 300 && MyChar.CPs >= 108)
                                    {
                                        MyChar.CPs -= 108;
                                        SendPacket(General.MyPackets.Vital(MyChar.UID, 30, MyChar.CPs));
                                        MyChar.Teleport(1002, 517, 351);
                                        World.SendMsgToAll(MyChar.Name + " a pay� pour ses crimes et a �t� lib�r�!", "Police", 2011);
                                    }
                                    if (MyChar.PKPoints >= 301 && MyChar.CPs >= 504)
                                    {
                                        MyChar.CPs -= 504;
                                        SendPacket(General.MyPackets.Vital(MyChar.UID, 30, MyChar.CPs));
                                        MyChar.Teleport(1002, 517, 351);
                                        World.SendMsgToAll(MyChar.Name + " a pay� pour ses crimes et a �t� lib�r�!", "Police", 2011);
                                    }
                                }
                            }
                            if (CurrentNPC == 385)
                            {
                                if (Control == 1)
                                {
                                    if (MyChar.BanNB <= 3)
                                    {
                                        if (MyChar.CPs >= 108)
                                        {
                                            MyChar.CPs -= 108;
                                            SendPacket(General.MyPackets.Vital(MyChar.UID, 30, MyChar.CPs));
                                            MyChar.Teleport(1002, 430, 378);
                                            World.SendMsgToAll(MyChar.Name + " a pay� pour ses crimes et a �t� lib�r�!", "Police", 2011);
                                        }
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous ne pouvez pas sortir de prison car vous avez �t� banni plus de 3 fois."));
                                        SendPacket(General.MyPackets.NPCLink("Je vois.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                            }
                            if (CurrentNPC == 1310)
                            {
                                if (Control == 1)
                                {
                                    int Teleport = 0;

                                    while (MyChar.InventoryContains(723085, 1))
                                    {
                                        MyChar.RemoveItem(MyChar.ItemNext(723085));
                                    }

                                    if (MyChar.Level < 100)
                                        MyChar.AddExp((ulong)((1295000 + MyChar.Level * 50000) / 2), false);
                                    else if (MyChar.Level < 110)
                                        MyChar.AddExp((ulong)((1395000 + MyChar.Level * 80000) / 2), false);
                                    else if (MyChar.Level < 115)
                                        MyChar.AddExp((ulong)((1595000 + MyChar.Level * 100000) / 2), false);
                                    else if (MyChar.Level < 120)
                                        MyChar.AddExp((ulong)((1895000 + MyChar.Level * 120000) / 2), false);
                                    else if (MyChar.Level < 125)
                                        MyChar.AddExp((ulong)((2095000 + MyChar.Level * 150000) / 2), false);
                                    else if (MyChar.Level < 130)
                                        MyChar.AddExp((ulong)((2395000 + MyChar.Level * 180000) / 2), false);
                                    else if (MyChar.Level < 137)
                                        MyChar.AddExp((ulong)((2895000 + MyChar.Level * 200000) / 2), false);

                                    Teleport = General.Rand.Next(1, 4);

                                    if (Teleport == 1)
                                        MyChar.Teleport(2021, 298, 414);
                                    if (Teleport == 2)
                                        MyChar.Teleport(2021, 365, 375);
                                    if (Teleport == 3)
                                        MyChar.Teleport(2021, 302, 324);
                                    if (Teleport == 4)
                                        MyChar.Teleport(2021, 223, 329);

                                    SendPacket(General.MyPackets.SendMsg(MyChar.MyClient.MessageId, "DisCity", MyChar.Name, "Vous devez obtenir 5 Jades d'enfer..", 2000));
                                }
                            }
                            if (CurrentNPC == 1311)
                            {
                                if (Control == 1)
                                {
                                    World.DisMap2.Clear();
                                    if (MyChar.InventoryContains(723085, 5))
                                    {
                                        MyChar.Teleport(2022, 228, 343);
                                        MyChar.DisKO = 0;

                                        foreach (DictionaryEntry DE in World.AllChars)
                                        {
                                            Character Charr = (Character)DE.Value;

                                            if (Charr.LocMap == 2022)
                                                World.DisMap2.Add(Charr.Name, Charr.UID);
                                        }

                                        if (MyChar.MyGuild == null)
                                            World.SendMsgToAll(World.DisMap2.Count + ": " + MyChar.Name + " est entr� dans la deuxi�me carte de Dis City!", "SYSTEM", 2005);
                                        else
                                            World.SendMsgToAll(World.DisMap2.Count + ": " + MyChar.Name + " des " + MyChar.MyGuild.GuildName + " est entr� dans la deuxi�me carte de Dis City!", "SYSTEM", 2005);

                                        if (World.DisMap2.Count == 1)
                                            World.SendMsgToAll(MyChar.Name + " est entr� dans la SalleInfernale!", "SYSTEM", 2011);


                                        if (World.DisMap2.Count > 59)
                                        {
                                            foreach (DictionaryEntry DE in World.AllChars)
                                            {
                                                Character Chaar = (Character)DE.Value;
                                                if (Chaar.LocMap == 2021)
                                                {
                                                    Chaar.Teleport(1002, 430, 380);
                                                    World.SendMsgToAll("Pour prot�ger les joueurs, " + MyChar.Name + " a t�l�port� les joueurs hors du dang�!", "SYSTEM", 2011);
                                                }
                                            }
                                        }

                                        while (MyChar.InventoryContains(723085, 1))
                                        {
                                            MyChar.RemoveItem(MyChar.ItemNext(723085));
                                        }

                                        if (MyChar.Level < 100)
                                            MyChar.AddExp((ulong)(1295000 + MyChar.Level * 50000), false);
                                        else if (MyChar.Level < 110)
                                            MyChar.AddExp((ulong)(1395000 + MyChar.Level * 80000), false);
                                        else if (MyChar.Level < 115)
                                            MyChar.AddExp((ulong)(1595000 + MyChar.Level * 100000), false);
                                        else if (MyChar.Level < 120)
                                            MyChar.AddExp((ulong)(1895000 + MyChar.Level * 120000), false);
                                        else if (MyChar.Level < 125)
                                            MyChar.AddExp((ulong)(2095000 + MyChar.Level * 150000), false);
                                        else if (MyChar.Level < 130)
                                            MyChar.AddExp((ulong)(2395000 + MyChar.Level * 180000), false);
                                        else if (MyChar.Level < 137)
                                            MyChar.AddExp((ulong)(2895000 + MyChar.Level * 200000), false);

                                        SendPacket(General.MyPackets.SendMsg(MyChar.MyClient.MessageId, "DisCity", MyChar.Name, "Tuez 800 monstres pour les Braves, 900 monstres pour les Guerriers, 1300 monstres pour les Archers, 1000 monstres pour les Taoistes de Feu, 600 monstres pour les Taoistes d�Eau. Je ne peux envoyer que 30 personnes au niveau suivant.", 2000));
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous n'avez pas 5 jades..."));
                                        SendPacket(General.MyPackets.NPCLink("Je vois.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                            }
                            if (CurrentNPC == 1312)
                            {
                                if (Control == 1)
                                {
                                    uint DisKo = 0;

                                    if (MyChar.Job > 9 && MyChar.Job < 16)
                                        DisKo = 800;
                                    if (MyChar.Job > 19 && MyChar.Job < 26)
                                        DisKo = 900;
                                    if (MyChar.Job > 39 && MyChar.Job < 46)
                                        DisKo = 1300;
                                    if (MyChar.Job > 129 && MyChar.Job < 136)
                                        DisKo = 600;
                                    if (MyChar.Job > 139 && MyChar.Job < 146)
                                        DisKo = 1000;

                                    World.DisMap3.Clear();

                                    if (MyChar.ItemsInInventory < 40 && MyChar.DisKO >= DisKo)
                                    {
                                        MyChar.Teleport(2023, 301, 653);
                                        MyChar.AddItem("723087-0-0-0-0-0", 0, (uint)General.Rand.Next(345636635));

                                        foreach (DictionaryEntry DE in World.AllChars)
                                        {
                                            Character Charr = (Character)DE.Value;

                                            if (Charr.LocMap == 2023)
                                                World.DisMap3.Add(Charr.Name, Charr.UID);
                                        }

                                        if (MyChar.MyGuild == null)
                                            World.SendMsgToAll(World.DisMap3.Count + ": " + MyChar.Name + " est entr� dans la troisi�me carte de Dis City!", "SYSTEM", 2005);
                                        else
                                            World.SendMsgToAll(World.DisMap3.Count + ": " + MyChar.Name + " des " + MyChar.MyGuild.GuildName + " est entr� dans la troisi�me carte de Dis City!", "SYSTEM", 2005);

                                        if (World.DisMap3.Count == 1)
                                            World.SendMsgToAll(MyChar.Name + " est entr� dans la Clo�treInfernal!", "SYSTEM", 2011);

                                        if (World.DisMap3.Count > 29)
                                        {
                                            foreach (DictionaryEntry DE in World.AllChars)
                                            {
                                                Character Chaar = (Character)DE.Value;
                                                if (Chaar.LocMap == 2022)
                                                {
                                                    Chaar.Teleport(1002, 430, 380);
                                                    World.SendMsgToAll("Pour prot�ger les joueurs, " + MyChar.Name + " a t�l�port� les joueurs hors du dang�!", "SYSTEM", 2011);
                                                }
                                                if (Chaar.LocMap == 2023)
                                                {
                                                    Chaar.Teleport(2024, 150, 283);
                                                }
                                            }
                                        }

                                        if (MyChar.Level < 100)
                                            MyChar.AddExp((ulong)(1295000 + MyChar.Level * 50000), false);
                                        else if (MyChar.Level < 110)
                                            MyChar.AddExp((ulong)(1395000 + MyChar.Level * 80000), false);
                                        else if (MyChar.Level < 115)
                                            MyChar.AddExp((ulong)(1595000 + MyChar.Level * 100000), false);
                                        else if (MyChar.Level < 120)
                                            MyChar.AddExp((ulong)(1895000 + MyChar.Level * 120000), false);
                                        else if (MyChar.Level < 125)
                                            MyChar.AddExp((ulong)(2095000 + MyChar.Level * 150000), false);
                                        else if (MyChar.Level < 130)
                                            MyChar.AddExp((ulong)(2395000 + MyChar.Level * 180000), false);
                                        else if (MyChar.Level < 137)
                                            MyChar.AddExp((ulong)(2895000 + MyChar.Level * 200000), false);

                                        SendPacket(General.MyPackets.SendMsg(MyChar.MyClient.MessageId, "DisCity", MyChar.Name, "Lorsque la 30e personne ou la derni�re personne rentrera dans cette carte vous irez � la prochaine!", 2000));
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous n'avez pas tu� assez de monstre ou votre inventaire est plein!"));
                                        SendPacket(General.MyPackets.NPCLink("Je vois.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                            }
                            if (CurrentNPC == 1315)
                            {
                                MyChar.RemoveItem(MyChar.ItemNext(790001));

                                if (MyChar.Level < 100)
                                    MyChar.AddExp((ulong)((1295000 + MyChar.Level * 50000) * 8), false);
                                else if (MyChar.Level < 110)
                                    MyChar.AddExp((ulong)((1395000 + MyChar.Level * 80000) * 8), false);
                                else if (MyChar.Level < 115)
                                    MyChar.AddExp((ulong)((1595000 + MyChar.Level * 100000) * 8), false);
                                else if (MyChar.Level < 120)
                                    MyChar.AddExp((ulong)((1895000 + MyChar.Level * 120000) * 8), false);
                                else if (MyChar.Level < 125)
                                    MyChar.AddExp((ulong)((2095000 + MyChar.Level * 150000) * 8), false);
                                else if (MyChar.Level < 130)
                                    MyChar.AddExp((ulong)((2395000 + MyChar.Level * 180000) * 8), false);
                                else if (MyChar.Level < 137)
                                    MyChar.AddExp((ulong)((2895000 + MyChar.Level * 200000) * 8), false);

                                byte Dress = 0;
                                Dress = (byte)General.Rand.Next(1, 21);

                                if (Dress == 1)
                                    MyChar.AddItem("137320-0-0-0-0-0", 0, (uint)General.Rand.Next(345636635));
                                if (Dress == 2)
                                    MyChar.AddItem("137330-0-0-0-0-0", 0, (uint)General.Rand.Next(345636635));
                                if (Dress == 3)
                                    MyChar.AddItem("137340-0-0-0-0-0", 0, (uint)General.Rand.Next(345636635));
                                if (Dress == 4)
                                    MyChar.AddItem("137420-0-0-0-0-0", 0, (uint)General.Rand.Next(345636635));
                                if (Dress == 5)
                                    MyChar.AddItem("137430-0-0-0-0-0", 0, (uint)General.Rand.Next(345636635));
                                if (Dress == 6)
                                    MyChar.AddItem("137440-0-0-0-0-0", 0, (uint)General.Rand.Next(345636635));
                                if (Dress == 7)
                                    MyChar.AddItem("137520-0-0-0-0-0", 0, (uint)General.Rand.Next(345636635));
                                if (Dress == 8)
                                    MyChar.AddItem("137530-0-0-0-0-0", 0, (uint)General.Rand.Next(345636635));
                                if (Dress == 9)
                                    MyChar.AddItem("137540-0-0-0-0-0", 0, (uint)General.Rand.Next(345636635));
                                if (Dress == 10)
                                    MyChar.AddItem("137620-0-0-0-0-0", 0, (uint)General.Rand.Next(345636635));
                                if (Dress == 11)
                                    MyChar.AddItem("137630-0-0-0-0-0", 0, (uint)General.Rand.Next(345636635));
                                if (Dress == 12)
                                    MyChar.AddItem("137640-0-0-0-0-0", 0, (uint)General.Rand.Next(345636635));
                                if (Dress == 13)
                                    MyChar.AddItem("137720-0-0-0-0-0", 0, (uint)General.Rand.Next(345636635));
                                if (Dress == 14)
                                    MyChar.AddItem("137730-0-0-0-0-0", 0, (uint)General.Rand.Next(345636635));
                                if (Dress == 15)
                                    MyChar.AddItem("137740-0-0-0-0-0", 0, (uint)General.Rand.Next(345636635));
                                if (Dress == 16)
                                    MyChar.AddItem("137820-0-0-0-0-0", 0, (uint)General.Rand.Next(345636635));
                                if (Dress == 17)
                                    MyChar.AddItem("137830-0-0-0-0-0", 0, (uint)General.Rand.Next(345636635));
                                if (Dress == 18)
                                    MyChar.AddItem("137840-0-0-0-0-0", 0, (uint)General.Rand.Next(345636635));
                                if (Dress == 19)
                                    MyChar.AddItem("137920-0-0-0-0-0", 0, (uint)General.Rand.Next(345636635));
                                if (Dress == 20)
                                    MyChar.AddItem("137930-0-0-0-0-0", 0, (uint)General.Rand.Next(345636635));
                                if (Dress == 21)
                                    MyChar.AddItem("137940-0-0-0-0-0", 0, (uint)General.Rand.Next(345636635));
                            }
                            if (CurrentNPC == 1275)
                            {
                                if (Control == 1)
                                {
                                    if (MyChar.Silvers >= 10000)
                                    {
                                        MyChar.Silvers -= 10000;
                                        MyChar.Teleport(6000, 028, 072);
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous n'avez pas 10000$, revenez plus tard."));
                                        SendPacket(General.MyPackets.NPCLink("Oh,ok.", 2));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                            }
                            if (CurrentNPC == 1846)
                            {
                                if (Control == 1)
                                {
                                    if (MyChar.CPs >= 27)
                                    {
                                        MyChar.Teleport(700, 50, 50);
                                        MyChar.CPs -= 27;
                                        SendPacket(General.MyPackets.Vital(MyChar.UID, 30, MyChar.CPs));
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous n'avez pas assez de cps"));
                                        SendPacket(General.MyPackets.NPCLink("Zut !", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                            }
                            if (CurrentNPC == 9997)
                            {
                                if (Control == 1)
                                {
                                    int Teleport = 0;

                                    if (MyChar.Level < 100)
                                        MyChar.AddExp((ulong)((1295000 + MyChar.Level * 50000) / 2), false);
                                    else if (MyChar.Level < 110)
                                        MyChar.AddExp((ulong)((1395000 + MyChar.Level * 80000) / 2), false);
                                    else if (MyChar.Level < 115)
                                        MyChar.AddExp((ulong)((1595000 + MyChar.Level * 100000) / 2), false);
                                    else if (MyChar.Level < 120)
                                        MyChar.AddExp((ulong)((1895000 + MyChar.Level * 120000) / 2), false);
                                    else if (MyChar.Level < 125)
                                        MyChar.AddExp((ulong)((2095000 + MyChar.Level * 150000) / 2), false);
                                    else if (MyChar.Level < 130)
                                        MyChar.AddExp((ulong)((2395000 + MyChar.Level * 180000) / 2), false);
                                    else if (MyChar.Level < 137)
                                        MyChar.AddExp((ulong)((2895000 + MyChar.Level * 200000) / 2), false);

                                    Teleport = General.Rand.Next(1, 4);

                                    if (Teleport == 1)
                                        MyChar.Teleport(1081, 107, 111);
                                    if (Teleport == 2)
                                        MyChar.Teleport(1081, 146, 172);
                                    if (Teleport == 3)
                                        MyChar.Teleport(1081, 135, 126);
                                    if (Teleport == 4)
                                        MyChar.Teleport(1081, 124, 85);
                                }
                            }
                            if (CurrentNPC == 9996)
                            {
                                if (Control == 1)
                                {
                                    int Teleport = 0;

                                    if (MyChar.Level < 100)
                                        MyChar.AddExp((ulong)(1295000 + MyChar.Level * 50000), false);
                                    else if (MyChar.Level < 110)
                                        MyChar.AddExp((ulong)(1395000 + MyChar.Level * 80000), false);
                                    else if (MyChar.Level < 115)
                                        MyChar.AddExp((ulong)(1595000 + MyChar.Level * 100000), false);
                                    else if (MyChar.Level < 120)
                                        MyChar.AddExp((ulong)(1895000 + MyChar.Level * 120000), false);
                                    else if (MyChar.Level < 125)
                                        MyChar.AddExp((ulong)(2095000 + MyChar.Level * 150000), false);
                                    else if (MyChar.Level < 130)
                                        MyChar.AddExp((ulong)(2395000 + MyChar.Level * 180000), false);
                                    else if (MyChar.Level < 137)
                                        MyChar.AddExp((ulong)(2895000 + MyChar.Level * 200000), false);

                                    Teleport = General.Rand.Next(1, 4);

                                    if (Teleport == 1)
                                        MyChar.Teleport(1090, 42, 61);
                                    if (Teleport == 2)
                                        MyChar.Teleport(1090, 98, 63);
                                    if (Teleport == 3)
                                        MyChar.Teleport(1090, 94, 98);
                                    if (Teleport == 4)
                                        MyChar.Teleport(1090, 119, 111);
                                }
                            }
                            if (CurrentNPC == 9995)
                            {
                                if (Control == 1)
                                {
                                    int Teleport = 0;

                                    if (MyChar.Level < 100)
                                        MyChar.AddExp((ulong)((1295000 + MyChar.Level * 50000) * 2), false);
                                    else if (MyChar.Level < 110)
                                        MyChar.AddExp((ulong)((1395000 + MyChar.Level * 80000) * 2), false);
                                    else if (MyChar.Level < 115)
                                        MyChar.AddExp((ulong)((1595000 + MyChar.Level * 100000) * 2), false);
                                    else if (MyChar.Level < 120)
                                        MyChar.AddExp((ulong)((1895000 + MyChar.Level * 120000) * 2), false);
                                    else if (MyChar.Level < 125)
                                        MyChar.AddExp((ulong)((2095000 + MyChar.Level * 150000) * 2), false);
                                    else if (MyChar.Level < 130)
                                        MyChar.AddExp((ulong)((2395000 + MyChar.Level * 180000) * 2), false);
                                    else if (MyChar.Level < 137)
                                        MyChar.AddExp((ulong)((2895000 + MyChar.Level * 200000) * 2), false);

                                    Teleport = General.Rand.Next(1, 2);

                                    if (Teleport == 1)
                                        MyChar.Teleport(1091, 32, 22);
                                    if (Teleport == 2)
                                        MyChar.Teleport(1091, 20, 31);
                                }
                            }
                            if (CurrentNPC == 9994)
                            {
                                if (Control == 1)
                                {
                                    World.PktMap1.Clear();
                                    World.PktMap2.Clear();
                                    World.PktMap3.Clear();
                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character Charr = (Character)DE.Value;

                                        if (Charr.LocMap == 1081)
                                            World.PktMap1.Add(Charr.Name, Charr.UID);

                                        if (Charr.LocMap == 1090)
                                            World.PktMap2.Add(Charr.Name, Charr.UID);

                                        if (Charr.LocMap == 1091)
                                            World.PktMap3.Add(Charr.Name, Charr.UID);
                                    }
                                    if (World.PktMap1.Count == 1 && World.PktMap2.Count == 0 && World.PktMap3.Count == 0)
                                    {
                                        DateTime myDateTime = DateTime.Now;
                                        string DayNow = Convert.ToString(myDateTime.DayOfWeek);

                                        if (DayNow == "Saturday" && myDateTime.Day != 1)
                                        {
                                            if (MyChar.Level < 100)
                                                MyChar.AddExp((ulong)((1295000 + MyChar.Level * 50000) * 5), false);
                                            else if (MyChar.Level < 110)
                                                MyChar.AddExp((ulong)((1395000 + MyChar.Level * 80000) * 5), false);
                                            else if (MyChar.Level < 115)
                                                MyChar.AddExp((ulong)((1595000 + MyChar.Level * 100000) * 5), false);
                                            else if (MyChar.Level < 120)
                                                MyChar.AddExp((ulong)((1895000 + MyChar.Level * 120000) * 5), false);
                                            else if (MyChar.Level < 125)
                                                MyChar.AddExp((ulong)((2095000 + MyChar.Level * 150000) * 5), false);
                                            else if (MyChar.Level < 130)
                                                MyChar.AddExp((ulong)((2395000 + MyChar.Level * 180000) * 5), false);
                                            else if (MyChar.Level < 137)
                                                MyChar.AddExp((ulong)((2895000 + MyChar.Level * 200000) * 5), false);

                                            MyChar.AddItem("1088000-0-0-0-0-0", 0, (uint)General.Rand.Next(345636635));
                                            MyChar.Teleport(1002, 430, 380);
                                            World.SendMsgToAll(MyChar.Name + " a gagn� le tournois de pk! F�licitation!", "SYSTEM", 2011);
                                        }
                                        if (myDateTime.Day == 1)
                                        {
                                            if (MyChar.Level < 100)
                                                MyChar.AddExp((ulong)((1295000 + MyChar.Level * 50000) * 5), false);
                                            else if (MyChar.Level < 110)
                                                MyChar.AddExp((ulong)((1395000 + MyChar.Level * 80000) * 5), false);
                                            else if (MyChar.Level < 115)
                                                MyChar.AddExp((ulong)((1595000 + MyChar.Level * 100000) * 5), false);
                                            else if (MyChar.Level < 120)
                                                MyChar.AddExp((ulong)((1895000 + MyChar.Level * 120000) * 5), false);
                                            else if (MyChar.Level < 125)
                                                MyChar.AddExp((ulong)((2095000 + MyChar.Level * 150000) * 5), false);
                                            else if (MyChar.Level < 130)
                                                MyChar.AddExp((ulong)((2395000 + MyChar.Level * 180000) * 5), false);
                                            else if (MyChar.Level < 137)
                                                MyChar.AddExp((ulong)((2895000 + MyChar.Level * 200000) * 5), false);

                                            byte WinGem = (byte)General.Rand.Next(1, 3);
                                            if (WinGem == 1)
                                                MyChar.AddItem("700003-0-0-0-0-0", 0, (uint)General.Rand.Next(345636635));
                                            if (WinGem == 2)
                                                MyChar.AddItem("700013-0-0-0-0-0", 0, (uint)General.Rand.Next(345636635));
                                            if (WinGem == 3)
                                                MyChar.AddItem("700033-0-0-0-0-0", 0, (uint)General.Rand.Next(345636635));

                                            MyChar.Teleport(1002, 430, 380);
                                            World.SendMsgToAll(MyChar.Name + " a gagn� le tournois de pk! F�licitation!", "SYSTEM", 2011);
                                            World.PktMap1.Clear();
                                            World.PktMap2.Clear();
                                            World.PktMap3.Clear();
                                        }
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous n'�tes pas le gagnant d�sol�."));
                                        SendPacket(General.MyPackets.NPCLink("Je vois", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                            }
                            if (CurrentNPC == 9993)
                            {
                                if (Control == 1)
                                {
                                    World.PktMap1.Clear();
                                    World.PktMap2.Clear();
                                    World.PktMap3.Clear();
                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character Charr = (Character)DE.Value;

                                        if (Charr.LocMap == 1081)
                                            World.PktMap1.Add(Charr.Name, Charr.UID);

                                        if (Charr.LocMap == 1090)
                                            World.PktMap2.Add(Charr.Name, Charr.UID);

                                        if (Charr.LocMap == 1091)
                                            World.PktMap3.Add(Charr.Name, Charr.UID);
                                    }
                                    if (World.PktMap1.Count == 0 && World.PktMap2.Count == 1 && World.PktMap3.Count == 0)
                                    {
                                        DateTime myDateTime = DateTime.Now;
                                        string DayNow = Convert.ToString(myDateTime.DayOfWeek);

                                        if (DayNow == "Saturday" && myDateTime.Day != 1)
                                        {
                                            if (MyChar.Level < 100)
                                                MyChar.AddExp((ulong)((1295000 + MyChar.Level * 50000) * 5), false);
                                            else if (MyChar.Level < 110)
                                                MyChar.AddExp((ulong)((1395000 + MyChar.Level * 80000) * 5), false);
                                            else if (MyChar.Level < 115)
                                                MyChar.AddExp((ulong)((1595000 + MyChar.Level * 100000) * 5), false);
                                            else if (MyChar.Level < 120)
                                                MyChar.AddExp((ulong)((1895000 + MyChar.Level * 120000) * 5), false);
                                            else if (MyChar.Level < 125)
                                                MyChar.AddExp((ulong)((2095000 + MyChar.Level * 150000) * 5), false);
                                            else if (MyChar.Level < 130)
                                                MyChar.AddExp((ulong)((2395000 + MyChar.Level * 180000) * 5), false);
                                            else if (MyChar.Level < 137)
                                                MyChar.AddExp((ulong)((2895000 + MyChar.Level * 200000) * 5), false);

                                            MyChar.AddItem("1088000-0-0-0-0-0", 0, (uint)General.Rand.Next(345636635));
                                            MyChar.Teleport(1002, 430, 380);
                                            World.SendMsgToAll(MyChar.Name + " a gagn� le tournois de pk! F�licitation!", "SYSTEM", 2011);
                                        }
                                        if (myDateTime.Day == 1)
                                        {
                                            if (MyChar.Level < 100)
                                                MyChar.AddExp((ulong)((1295000 + MyChar.Level * 50000) * 5), false);
                                            else if (MyChar.Level < 110)
                                                MyChar.AddExp((ulong)((1395000 + MyChar.Level * 80000) * 5), false);
                                            else if (MyChar.Level < 115)
                                                MyChar.AddExp((ulong)((1595000 + MyChar.Level * 100000) * 5), false);
                                            else if (MyChar.Level < 120)
                                                MyChar.AddExp((ulong)((1895000 + MyChar.Level * 120000) * 5), false);
                                            else if (MyChar.Level < 125)
                                                MyChar.AddExp((ulong)((2095000 + MyChar.Level * 150000) * 5), false);
                                            else if (MyChar.Level < 130)
                                                MyChar.AddExp((ulong)((2395000 + MyChar.Level * 180000) * 5), false);
                                            else if (MyChar.Level < 137)
                                                MyChar.AddExp((ulong)((2895000 + MyChar.Level * 200000) * 5), false);

                                            byte WinGem = (byte)General.Rand.Next(1, 3);
                                            if (WinGem == 1)
                                                MyChar.AddItem("700003-0-0-0-0-0", 0, (uint)General.Rand.Next(345636635));
                                            if (WinGem == 2)
                                                MyChar.AddItem("700013-0-0-0-0-0", 0, (uint)General.Rand.Next(345636635));
                                            if (WinGem == 3)
                                                MyChar.AddItem("700033-0-0-0-0-0", 0, (uint)General.Rand.Next(345636635));

                                            MyChar.Teleport(1002, 430, 380);
                                            World.SendMsgToAll(MyChar.Name + " a gagn� le tournois de pk! F�licitation!", "SYSTEM", 2011);
                                            World.PktMap1.Clear();
                                            World.PktMap2.Clear();
                                            World.PktMap3.Clear();
                                        }
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous n'�tes pas le gagnant d�sol�."));
                                        SendPacket(General.MyPackets.NPCLink("Je vois", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                            }
                            if (CurrentNPC == 9992)
                            {
                                if (Control == 1)
                                {
                                    World.PktMap1.Clear();
                                    World.PktMap2.Clear();
                                    World.PktMap3.Clear();
                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character Charr = (Character)DE.Value;

                                        if (Charr.LocMap == 1081)
                                            World.PktMap1.Add(Charr.Name, Charr.UID);

                                        if (Charr.LocMap == 1090)
                                            World.PktMap2.Add(Charr.Name, Charr.UID);

                                        if (Charr.LocMap == 1091)
                                            World.PktMap3.Add(Charr.Name, Charr.UID);
                                    }
                                    if (World.PktMap1.Count == 0 && World.PktMap2.Count == 0 && World.PktMap3.Count == 1)
                                    {
                                        DateTime myDateTime = DateTime.Now;
                                        string DayNow = Convert.ToString(myDateTime.DayOfWeek);

                                        if (DayNow == "Saturday" && myDateTime.Day != 1)
                                        {
                                            if (MyChar.Level < 100)
                                                MyChar.AddExp((ulong)((1295000 + MyChar.Level * 50000) * 5), false);
                                            else if (MyChar.Level < 110)
                                                MyChar.AddExp((ulong)((1395000 + MyChar.Level * 80000) * 5), false);
                                            else if (MyChar.Level < 115)
                                                MyChar.AddExp((ulong)((1595000 + MyChar.Level * 100000) * 5), false);
                                            else if (MyChar.Level < 120)
                                                MyChar.AddExp((ulong)((1895000 + MyChar.Level * 120000) * 5), false);
                                            else if (MyChar.Level < 125)
                                                MyChar.AddExp((ulong)((2095000 + MyChar.Level * 150000) * 5), false);
                                            else if (MyChar.Level < 130)
                                                MyChar.AddExp((ulong)((2395000 + MyChar.Level * 180000) * 5), false);
                                            else if (MyChar.Level < 137)
                                                MyChar.AddExp((ulong)((2895000 + MyChar.Level * 200000) * 5), false);

                                            MyChar.AddItem("1088000-0-0-0-0-0", 0, (uint)General.Rand.Next(345636635));
                                            MyChar.Teleport(1002, 430, 380);
                                            World.SendMsgToAll(MyChar.Name + " a gagn� le tournois de pk! F�licitation!", "SYSTEM", 2011);
                                        }
                                        if (myDateTime.Day == 1)
                                        {
                                            if (MyChar.Level < 100)
                                                MyChar.AddExp((ulong)((1295000 + MyChar.Level * 50000) * 5), false);
                                            else if (MyChar.Level < 110)
                                                MyChar.AddExp((ulong)((1395000 + MyChar.Level * 80000) * 5), false);
                                            else if (MyChar.Level < 115)
                                                MyChar.AddExp((ulong)((1595000 + MyChar.Level * 100000) * 5), false);
                                            else if (MyChar.Level < 120)
                                                MyChar.AddExp((ulong)((1895000 + MyChar.Level * 120000) * 5), false);
                                            else if (MyChar.Level < 125)
                                                MyChar.AddExp((ulong)((2095000 + MyChar.Level * 150000) * 5), false);
                                            else if (MyChar.Level < 130)
                                                MyChar.AddExp((ulong)((2395000 + MyChar.Level * 180000) * 5), false);
                                            else if (MyChar.Level < 137)
                                                MyChar.AddExp((ulong)((2895000 + MyChar.Level * 200000) * 5), false);

                                            byte WinGem = (byte)General.Rand.Next(1, 3);
                                            if (WinGem == 1)
                                                MyChar.AddItem("700003-0-0-0-0-0", 0, (uint)General.Rand.Next(345636635));
                                            if (WinGem == 2)
                                                MyChar.AddItem("700013-0-0-0-0-0", 0, (uint)General.Rand.Next(345636635));
                                            if (WinGem == 3)
                                                MyChar.AddItem("700033-0-0-0-0-0", 0, (uint)General.Rand.Next(345636635));

                                            MyChar.Teleport(1002, 430, 380);
                                            World.SendMsgToAll(MyChar.Name + " a gagn� le tournois de pk! F�licitation!", "SYSTEM", 2011);
                                            World.PktMap1.Clear();
                                            World.PktMap2.Clear();
                                            World.PktMap3.Clear();
                                        }
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous n'�tes pas le gagnant d�sol�."));
                                        SendPacket(General.MyPackets.NPCLink("Je vois", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                            }
                            #region Unknown Man - DB Leveling Guy
                            if (CurrentNPC == 3825)
                            {
                                if (Control == 1)
                                {
                                    int AddExp = 0;

                                    if (MyChar.Level >= 40 && MyChar.Level <= 59)
                                    {
                                        AddExp = 1000000;
                                    }
                                    else if (MyChar.Level >= 60 && MyChar.Level <= 69)
                                    {
                                        AddExp = 1200000;
                                    }
                                    else if (MyChar.Level >= 70 && MyChar.Level <= 79)
                                    {
                                        AddExp = 1400000;
                                    }
                                    else if (MyChar.Level >= 80 && MyChar.Level <= 89)
                                    {
                                        AddExp = 1700000;
                                    }
                                    else if (MyChar.Level >= 90 && MyChar.Level <= 99)
                                    {
                                        AddExp = 2000000;
                                    }
                                    else if (MyChar.Level >= 100 && MyChar.Level <= 109)
                                    {
                                        AddExp = 2400000;
                                    }
                                    else if (MyChar.Level >= 110 && MyChar.Level <= 119)
                                    {
                                        AddExp = 2900000;
                                    }
                                    else if (MyChar.Level >= 120 && MyChar.Level <= 130)
                                    {
                                        AddExp = 3000000;
                                    }
                                    else if (MyChar.Level >= 131 && MyChar.Level <= 136)
                                    {
                                        AddExp = 3500000;
                                    }
                                    SendPacket(General.MyPackets.NPCSay("Apportez moi une Perle de Dragon, puis je vous aiderai � transmettre l'�nergie de celle-ci. Vous gagnerez: " + AddExp + " points d'exp�rience!"));
                                    SendPacket(General.MyPackets.NPCLink("Veuillez commencer S.V.P.", 2));
                                    SendPacket(General.MyPackets.NPCSetFace(6));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 2)
                                {
                                    if (MyChar.InventoryContains(1088000, 1))
                                    {
                                        if (MyChar.Level >= 40 && MyChar.Level <= 59)
                                        {
                                            MyChar.RemoveItem(MyChar.ItemNext(1088000));
                                            MyChar.AddExp((ulong)(1295000 + MyChar.Level * 1000000), false);
                                        }
                                        else if (MyChar.Level >= 60 && MyChar.Level <= 69)
                                        {
                                            MyChar.RemoveItem(MyChar.ItemNext(1088000));
                                            MyChar.AddExp((ulong)(1295000 + MyChar.Level * 1200000), false);
                                        }
                                        else if (MyChar.Level >= 70 && MyChar.Level <= 79)
                                        {
                                            MyChar.RemoveItem(MyChar.ItemNext(1088000));
                                            MyChar.AddExp((ulong)(1295000 + MyChar.Level * 1400000), false);
                                        }
                                        else if (MyChar.Level >= 80 && MyChar.Level <= 89)
                                        {
                                            MyChar.RemoveItem(MyChar.ItemNext(1088000));
                                            MyChar.AddExp((ulong)(1295000 + MyChar.Level * 1700000), false);
                                        }
                                        else if (MyChar.Level >= 90 && MyChar.Level <= 99)
                                        {
                                            MyChar.RemoveItem(MyChar.ItemNext(1088000));
                                            MyChar.AddExp((ulong)(1295000 + MyChar.Level * 2000000), false);
                                        }
                                        else if (MyChar.Level >= 100 && MyChar.Level <= 109)
                                        {
                                            MyChar.RemoveItem(MyChar.ItemNext(1088000));
                                            MyChar.AddExp((ulong)(1295000 + MyChar.Level * 2400000), false);
                                        }
                                        else if (MyChar.Level >= 110 && MyChar.Level <= 119)
                                        {
                                            MyChar.RemoveItem(MyChar.ItemNext(1088000));
                                            MyChar.AddExp((ulong)(1295000 + MyChar.Level * 2900000), false);
                                        }
                                        else if (MyChar.Level >= 120 && MyChar.Level <= 129)
                                        {
                                            MyChar.RemoveItem(MyChar.ItemNext(1088000));
                                            MyChar.AddExp((ulong)(1295000 + MyChar.Level * 3500000), false);
                                        }
                                        else if (MyChar.Level >= 130 && MyChar.Level <= 136)
                                        {
                                            MyChar.RemoveItem(MyChar.ItemNext(1088000));
                                            MyChar.AddExp((ulong)(1295000 + MyChar.Level * 3900000), false);
                                        }
                                        else if (MyChar.Level == 137)
                                        {
                                            SendPacket(General.MyPackets.NPCSay("Vous �tes au plus au niveau, vous ne pouvez plus gagner de point d'exp�rience."));
                                            SendPacket(General.MyPackets.NPCLink("Je vois.", 255));
                                            SendPacket(General.MyPackets.NPCSetFace(6));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("D�sol�e, vous n'avez pas de Perle de Dargon."));
                                        SendPacket(General.MyPackets.NPCLink("D�sol�.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(6));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                            }
                            #endregion
                            if (CurrentNPC == 278)
                            {
                                if (Control == 1)
                                {
                                    SendPacket(General.MyPackets.ETCPacket(MyChar, 1));
                                }
                                if (Control == 2)
                                {
                                    SendPacket(General.MyPackets.EnchantOpen(MyChar));
                                }
                                /*if (Control == 2)
                                {
                                    GemHp = 700000;
                                    SendPacket(General.MyPackets.NPCSay("Je suis ici pour enchanter vos objets. Quel gemme avez-vous?."));
                                    SendPacket(General.MyPackets.NPCLink("Une gemme Ph�nix.", 3));
                                    SendPacket(General.MyPackets.NPCLink("Une gemme Dragon.", 4));
                                    SendPacket(General.MyPackets.NPCLink("Une gemme Fureur.", 5));
                                    SendPacket(General.MyPackets.NPCLink("Une gemme Arc en Ciel.", 6));
                                    SendPacket(General.MyPackets.NPCLink("Une gemme Ivoire.", 7));
                                    SendPacket(General.MyPackets.NPCLink("Une gemme Violet.", 8));
                                    SendPacket(General.MyPackets.NPCLink("Une gemme de Lune.", 9));
                                    SendPacket(General.MyPackets.NPCLink("Une gemme Mythique.", 10));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control >= 3 && Control <= 10)
                                {
                                    if (Control == 3)
                                        GemHp += 1;
                                    if (Control == 4)
                                        GemHp += 11;
                                    if (Control == 5)
                                        GemHp += 21;
                                    if (Control == 6)
                                        GemHp += 31;
                                    if (Control == 7)
                                        GemHp += 41;
                                    if (Control == 8)
                                        GemHp += 51;
                                    if (Control == 9)
                                        GemHp += 61;
                                    if (Control == 10)
                                        GemHp += 71;

                                    SendPacket(General.MyPackets.NPCSay("Quelle qualit�e de gemme avez-vous ?"));
                                    SendPacket(General.MyPackets.NPCLink("Une gemme normal", 11));
                                    SendPacket(General.MyPackets.NPCLink("Une gemme raffin�e", 12));
                                    SendPacket(General.MyPackets.NPCLink("Une gemme superbe", 13));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }

                                if (Control >= 11 && Control <= 13)
                                {
                                    if (Control == 11)
                                        GemHp += 0;
                                    if (Control == 12)
                                        GemHp += 1;
                                    if (Control == 13)
                                        GemHp += 2;

                                    SendPacket(General.MyPackets.NPCSay("Quel objet voulez-vous enchanter?"));
                                    SendPacket(General.MyPackets.NPCLink("Arme/Arc/Glaive.", 14));
                                    SendPacket(General.MyPackets.NPCLink("Collier/Sac.", 15));
                                    SendPacket(General.MyPackets.NPCLink("Bague/Bracelet.", 16));
                                    SendPacket(General.MyPackets.NPCLink("Botte.", 17));
                                    SendPacket(General.MyPackets.NPCLink("Armure/Robe.", 18));
                                    SendPacket(General.MyPackets.NPCLink("Casque/Boucle.", 19));
                                    SendPacket(General.MyPackets.NPCLink("Bouclier/Arme.", 20));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }

                                if (Control <= 20 && Control >= 14)
                                {

                                    Ini Enchant = new Ini(System.Windows.Forms.Application.StartupPath + @"\enchant.ini");
                                    int MinHp = int.Parse(Enchant.ReadValue(Convert.ToString(GemHp), "MinHp"));
                                    int MaxHp = int.Parse(Enchant.ReadValue(Convert.ToString(GemHp), "MaxHp"));

                                    byte Pos = 0;

                                    if (Control == 14)
                                        Pos = 4;
                                    if (Control == 15)
                                        Pos = 2;
                                    if (Control == 16)
                                        Pos = 6;
                                    if (Control == 17)
                                        Pos = 8;
                                    if (Control == 18)
                                        Pos = 3;
                                    if (Control == 19)
                                        Pos = 1;
                                    if (Control == 20)
                                        Pos = 5;

                                    string[] Splitter = MyChar.Equips[Pos].Split('-');
                                    uint ItemId = uint.Parse(Splitter[0]);
                                    byte ItemEnchant = byte.Parse(Splitter[3]);

                                    if (MyChar.InventoryContains(GemHp, 1))
                                    {
                                        MyChar.RemoveItem(MyChar.ItemNext(GemHp));
                                        int ItemHp = General.Rand.Next(Math.Min(MinHp, MinHp), Math.Max(MaxHp, MaxHp));

                                        if (ItemHp > ItemEnchant)
                                        {
                                            MyChar.GetEquipStats(Pos, true);
                                            MyChar.Equips[Pos] = ItemId + "-" + Splitter[1] + "-" + Splitter[2] + "-" + ItemHp + "-" + Splitter[4] + "-" + Splitter[5];
                                            MyChar.GetEquipStats(Pos, false);

                                            SendPacket(General.MyPackets.AddItem((long)MyChar.Equips_UIDs[Pos], (int)ItemId, byte.Parse(Splitter[1]), byte.Parse(Splitter[2]), (byte)ItemHp, byte.Parse(Splitter[4]), byte.Parse(Splitter[5]), Pos, 100, 100));

                                            SendPacket(General.MyPackets.NPCSay("Votre objet a �t� am�lior�!"));
                                            SendPacket(General.MyPackets.NPCLink("Merci beaucoup!", 255));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous n'avez pas la gemme."));
                                        SendPacket(General.MyPackets.NPCLink("Je vois.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }*/

                                if (Control == 21)
                                {
                                    SendPacket(General.MyPackets.NPCSay("Il vous faut 12 Perles de Dragon pour le +10, 25 Perles de Dragon pour le +11 et 40 Perles de Dragon pour le +12."));
                                    SendPacket(General.MyPackets.NPCLink("Arme/Arc/Glaive.", 22));
                                    SendPacket(General.MyPackets.NPCLink("Collier/Sac.", 23));
                                    SendPacket(General.MyPackets.NPCLink("Bague/Bracelet.", 24));
                                    SendPacket(General.MyPackets.NPCLink("Botte.", 25));
                                    SendPacket(General.MyPackets.NPCLink("Armure/Robe.", 26));
                                    SendPacket(General.MyPackets.NPCLink("Casque/Boucle.", 27));
                                    SendPacket(General.MyPackets.NPCLink("Bouclier/Arme.", 28));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }

                                if (Control <= 28 && Control >= 22)
                                {
                                    byte Pos = 0;

                                    if (Control == 22)
                                        Pos = 4;
                                    if (Control == 23)
                                        Pos = 2;
                                    if (Control == 24)
                                        Pos = 6;
                                    if (Control == 25)
                                        Pos = 8;
                                    if (Control == 26)
                                        Pos = 3;
                                    if (Control == 27)
                                        Pos = 1;
                                    if (Control == 28)
                                        Pos = 5;

                                    string[] Splitter = MyChar.Equips[Pos].Split('-');
                                    uint ItemId = uint.Parse(Splitter[0]);
                                    byte Plus = byte.Parse(Splitter[1]);

                                    if (Plus >= 9 && Plus <= 11)
                                    {
                                        byte DBs = 0;
                                        byte DBsCount = 0;

                                        if (Plus == 9)
                                            DBs = 12;
                                        if (Plus == 10)
                                            DBs = 25;
                                        if (Plus == 11)
                                            DBs = 40;

                                        if (MyChar.InventoryContains(1088000, (uint)DBs))
                                        {
                                            while (DBsCount < DBs)
                                            {
                                                MyChar.RemoveItem(MyChar.ItemNext(1088000));
                                                DBsCount++;
                                            }
                                            Plus += 1;

                                            MyChar.GetEquipStats(Pos, true);
                                            MyChar.Equips[Pos] = ItemId + "-" + Plus + "-" + Splitter[2] + "-" + Splitter[3] + "-" + Splitter[4] + "-" + Splitter[5];
                                            MyChar.GetEquipStats(Pos, false);

                                            SendPacket(General.MyPackets.AddItem((long)MyChar.Equips_UIDs[Pos], (int)ItemId, (byte)Plus, byte.Parse(Splitter[2]), byte.Parse(Splitter[3]), byte.Parse(Splitter[4]), byte.Parse(Splitter[5]), Pos, 100, 100));

                                            SendPacket(General.MyPackets.NPCSay("Votre objet a �t� am�lior�!"));
                                            SendPacket(General.MyPackets.NPCLink("Merci beaucoup!", 255));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                        else
                                        {
                                            SendPacket(General.MyPackets.NPCSay("Vous n'avez pas assez de Perles de Dragon."));
                                            SendPacket(General.MyPackets.NPCLink("Je vois.", 255));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Votre objet n'est pas +9 ou votre objet est d�j� +12."));
                                        SendPacket(General.MyPackets.NPCLink("Je vois.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                            }
                            if (CurrentNPC == 6690)
                            {
                                if (Control == 1)
                                {
                                    MyChar.Teleport(1767, 94, 64);
                                }
                                if (Control <= 8 && Control >= 2)
                                {
                                    byte Pos = 0;

                                    if (Control == 2)
                                        Pos = 4;
                                    if (Control == 3)
                                        Pos = 2;
                                    if (Control == 4)
                                        Pos = 6;
                                    if (Control == 5)
                                        Pos = 8;
                                    if (Control == 6)
                                        Pos = 3;
                                    if (Control == 7)
                                        Pos = 1;
                                    if (Control == 8)
                                        Pos = 5;

                                    string[] Splitter = MyChar.Equips[Pos].Split('-');
                                    uint ItemId = uint.Parse(Splitter[0]);
                                    byte ItemBless = byte.Parse(Splitter[2]);
                                    byte RealBless = 0;

                                    if (ItemBless > 9 && ItemBless < 18)
                                        RealBless = (byte)(ItemBless - 10);
                                    else if (ItemBless > 19 && ItemBless < 28)
                                        RealBless = (byte)(ItemBless - 20);
                                    else if (ItemBless > 29 && ItemBless < 38)
                                        RealBless = (byte)(ItemBless - 30);
                                    else if (ItemBless > 39 && ItemBless < 48)
                                        RealBless = (byte)(ItemBless - 40);
                                    else
                                        RealBless = ItemBless;

                                    if (RealBless < 7)
                                    {
                                        byte amulette = 0;

                                        if (RealBless == 0)
                                            amulette = 5;
                                        if (RealBless == 1)
                                            amulette = 1;
                                        if (RealBless == 2)
                                            amulette = 2;
                                        if (RealBless == 3)
                                            amulette = 3;
                                        if (RealBless == 4)
                                            amulette = 4;
                                        if (RealBless == 5)
                                            amulette = 5;
                                        if (RealBless == 6)
                                            amulette = 7;

                                        if (MyChar.InventoryContains(700074, amulette))
                                        {
                                            if (RealBless == 0)
                                            {
                                                MyChar.RemoveItem(MyChar.ItemNext(700074));
                                                MyChar.RemoveItem(MyChar.ItemNext(700074));
                                                MyChar.RemoveItem(MyChar.ItemNext(700074));
                                                MyChar.RemoveItem(MyChar.ItemNext(700074));
                                                MyChar.RemoveItem(MyChar.ItemNext(700074));
                                            }
                                            if (RealBless == 1)
                                            {
                                                MyChar.RemoveItem(MyChar.ItemNext(700074));
                                            }
                                            if (RealBless == 2)
                                            {
                                                MyChar.RemoveItem(MyChar.ItemNext(700074));
                                                MyChar.RemoveItem(MyChar.ItemNext(700074));
                                            }
                                            if (RealBless == 3)
                                            {
                                                MyChar.RemoveItem(MyChar.ItemNext(700074));
                                                MyChar.RemoveItem(MyChar.ItemNext(700074));
                                                MyChar.RemoveItem(MyChar.ItemNext(700074));
                                            }
                                            if (RealBless == 4)
                                            {
                                                MyChar.RemoveItem(MyChar.ItemNext(700074));
                                                MyChar.RemoveItem(MyChar.ItemNext(700074));
                                                MyChar.RemoveItem(MyChar.ItemNext(700074));
                                                MyChar.RemoveItem(MyChar.ItemNext(700074));
                                            }
                                            if (RealBless == 5)
                                            {
                                                MyChar.RemoveItem(MyChar.ItemNext(700074));
                                                MyChar.RemoveItem(MyChar.ItemNext(700074));
                                                MyChar.RemoveItem(MyChar.ItemNext(700074));
                                                MyChar.RemoveItem(MyChar.ItemNext(700074));
                                                MyChar.RemoveItem(MyChar.ItemNext(700074));
                                            }
                                            if (RealBless == 6)
                                            {
                                                MyChar.RemoveItem(MyChar.ItemNext(700074));
                                                MyChar.RemoveItem(MyChar.ItemNext(700074));
                                                MyChar.RemoveItem(MyChar.ItemNext(700074));
                                                MyChar.RemoveItem(MyChar.ItemNext(700074));
                                                MyChar.RemoveItem(MyChar.ItemNext(700074));
                                                MyChar.RemoveItem(MyChar.ItemNext(700074));
                                                MyChar.RemoveItem(MyChar.ItemNext(700074));
                                            }

                                            ItemBless += 1;

                                            MyChar.GetEquipStats(Pos, true);
                                            MyChar.Equips[Pos] = ItemId + "-" + Splitter[1] + "-" + ItemBless + "-" + Splitter[3] + "-" + Splitter[4] + "-" + Splitter[5];
                                            MyChar.GetEquipStats(Pos, false);

                                            SendPacket(General.MyPackets.AddItem((long)MyChar.Equips_UIDs[Pos], (int)ItemId, byte.Parse(Splitter[1]), (byte)ItemBless, byte.Parse(Splitter[3]), byte.Parse(Splitter[4]), byte.Parse(Splitter[5]), Pos, 100, 100));

                                            SendPacket(General.MyPackets.NPCSay("Votre objet a �t� am�lior�!"));
                                            SendPacket(General.MyPackets.NPCLink("Merci beaucoup!", 255));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                        else
                                        {
                                            SendPacket(General.MyPackets.NPCSay("Vous n'avez pas assez d'amulette."));
                                            SendPacket(General.MyPackets.NPCLink("Je vois.", 255));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Votre �quipement ne peut pas �tre am�lior�."));
                                        SendPacket(General.MyPackets.NPCLink("Ok.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                            }
                            if (CurrentNPC == 1010)
                            {
                                if (Control == 1)
                                {
                                    SendPacket(General.MyPackets.NPCSay("� quel objet voulez-vous ajouter un trou?"));
                                    SendPacket(General.MyPackets.NPCLink("Casque/Boucle", 3));
                                    SendPacket(General.MyPackets.NPCLink("Collier/Sac", 4));
                                    SendPacket(General.MyPackets.NPCLink("Armure/Robe", 5));
                                    SendPacket(General.MyPackets.NPCLink("Bague/Bracelet", 6));
                                    SendPacket(General.MyPackets.NPCLink("Botte", 7));
                                    SendPacket(General.MyPackets.NPCLink("Bouclier", 13));
                                    SendPacket(General.MyPackets.NPCLink("Je ne fais que passer.", 255));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 2)
                                {
                                    SendPacket(General.MyPackets.NPCSay("� quel objet voulez-vous ajouter un trou?"));
                                    SendPacket(General.MyPackets.NPCLink("Casque/Boucle", 8));
                                    SendPacket(General.MyPackets.NPCLink("Collier/Sac", 9));
                                    SendPacket(General.MyPackets.NPCLink("Armure/Robe", 10));
                                    SendPacket(General.MyPackets.NPCLink("Bague/Bracelet", 11));
                                    SendPacket(General.MyPackets.NPCLink("Botte", 12));
                                    SendPacket(General.MyPackets.NPCLink("Bouclier", 14));
                                    SendPacket(General.MyPackets.NPCLink("Je ne fais que passer.", 255));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 3 || Control == 4 || Control == 5 || Control == 6 || Control == 7 || Control == 13)
                                {
                                    int into = 0;

                                    if (Control == 3 && MyChar.Equips[1] != null && MyChar.Equips[1] != "0")
                                        into = 1;
                                    else if (Control == 4 && MyChar.Equips[2] != null && MyChar.Equips[2] != "0")
                                        into = 2;
                                    else if (Control == 5 && MyChar.Equips[3] != null && MyChar.Equips[3] != "0")
                                        into = 3;
                                    else if (Control == 6 && MyChar.Equips[6] != null && MyChar.Equips[6] != "0")
                                        into = 6;
                                    else if (Control == 7 && MyChar.Equips[8] != null && MyChar.Equips[8] != "0")
                                        into = 8;
                                    else if (Control == 13 && MyChar.Equips[5] != null && MyChar.Equips[5] != "0")
                                        into = 5;
                                    else
                                        return;

                                    if (MyChar.InventoryContains(1200005, 3))
                                    {
                                        string[] item = MyChar.Equips[into].Split('-');
                                        if (item[4] != "0")
                                            return;


                                        MyChar.RemoveItem(MyChar.ItemNext(1200005));
                                        MyChar.RemoveItem(MyChar.ItemNext(1200005));
                                        MyChar.RemoveItem(MyChar.ItemNext(1200005));


                                        item[4] = "255";

                                        MyChar.Equips[into] = item[0] + "-" + item[1] + "-" + item[2] + "-" + item[3] + "-" + item[4] + "-" + item[5];
                                        SendPacket(General.MyPackets.AddItem((long)MyChar.Equips_UIDs[into], int.Parse(item[0]), byte.Parse(item[1]), byte.Parse(item[2]), byte.Parse(item[3]), byte.Parse(item[4]), byte.Parse(item[5]), (byte)into, 100, 100));
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous n'avez pas assez de ForeusesDiamants."));
                                        SendPacket(General.MyPackets.NPCLink("Je vois.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }

                                if (Control == 8 || Control == 9 || Control == 10 || Control == 11 || Control == 12 || Control == 14)
                                {
                                    int into = 0;

                                    if (Control == 8 && MyChar.Equips[1] != null && MyChar.Equips[1] != "0")
                                        into = 1;
                                    else if (Control == 9 && MyChar.Equips[2] != null && MyChar.Equips[2] != "0")
                                        into = 2;
                                    else if (Control == 10 && MyChar.Equips[3] != null && MyChar.Equips[3] != "0")
                                        into = 3;
                                    else if (Control == 11 && MyChar.Equips[6] != null && MyChar.Equips[6] != "0")
                                        into = 6;
                                    else if (Control == 12 && MyChar.Equips[8] != null && MyChar.Equips[8] != "0")
                                        into = 8;
                                    else if (Control == 14 && MyChar.Equips[5] != null && MyChar.Equips[5] != "0")
                                        into = 5;
                                    else
                                        return;

                                    if (MyChar.InventoryContains(1200005, 9))
                                    {
                                        string[] item = MyChar.Equips[into].Split('-');
                                        if (item[5] != "0" || item[4] == "0")
                                            return;

                                        MyChar.RemoveItem(MyChar.ItemNext(1200005));
                                        MyChar.RemoveItem(MyChar.ItemNext(1200005));
                                        MyChar.RemoveItem(MyChar.ItemNext(1200005));
                                        MyChar.RemoveItem(MyChar.ItemNext(1200005));
                                        MyChar.RemoveItem(MyChar.ItemNext(1200005));
                                        MyChar.RemoveItem(MyChar.ItemNext(1200005));
                                        MyChar.RemoveItem(MyChar.ItemNext(1200005));
                                        MyChar.RemoveItem(MyChar.ItemNext(1200005));
                                        MyChar.RemoveItem(MyChar.ItemNext(1200005));

                                        item[5] = "255";

                                        MyChar.Equips[into] = item[0] + "-" + item[1] + "-" + item[2] + "-" + item[3] + "-" + item[4] + "-" + item[5];
                                        SendPacket(General.MyPackets.AddItem((long)MyChar.Equips_UIDs[into], int.Parse(item[0]), byte.Parse(item[1]), byte.Parse(item[2]), byte.Parse(item[3]), byte.Parse(item[4]), byte.Parse(item[5]), (byte)into, 100, 100));
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous n'avez pas assez de ForeusesDiamants."));
                                        SendPacket(General.MyPackets.NPCLink("Je vois.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                            }

                            if (CurrentNPC == 2)
                            {
                                if (Control == 1 || Control == 2 || Control == 3)
                                {
                                    ChColor = Control;
                                    SendPacket(General.MyPackets.NPCSay("Voici les sept couleurs que je peux utiliser. Vous pouvez essayer toutes les couleurs sans payer plus. Quel est la meilleur couleur selon vous?"));
                                    SendPacket(General.MyPackets.NPCLink("Orange", 4));
                                    SendPacket(General.MyPackets.NPCLink("Bleu ciel", 5));
                                    SendPacket(General.MyPackets.NPCLink("Rouge", 6));
                                    SendPacket(General.MyPackets.NPCLink("Bleu", 7));
                                    SendPacket(General.MyPackets.NPCLink("Jaune", 8));
                                    SendPacket(General.MyPackets.NPCLink("Mauve", 9));
                                    SendPacket(General.MyPackets.NPCLink("Blanc", 10));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 4 || Control == 5 || Control == 6 || Control == 7 || Control == 8 || Control == 9 || Control == 10)
                                {
                                    try
                                    {
                                        string[] item;
                                        string[] item2 = null;
                                        if (ChColor == 3)
                                            item2 = MyChar.Equips[5].Split('-');
                                        string newitem;

                                        if (ChColor == 1 && MyChar.Equips[3] != null && MyChar.Equips[3] != "0")
                                            item = MyChar.Equips[3].Split('-');
                                        else if (ChColor == 2 && MyChar.Equips[1] != null && MyChar.Equips[1] != "0")
                                            item = MyChar.Equips[1].Split('-');
                                        else if (ChColor == 3)
                                            if (MyChar.Equips[5] != null && Other.WeaponType(uint.Parse(item2[0])) == 900)
                                                item = MyChar.Equips[5].Split('-');
                                            else
                                            {
                                                SendPacket(General.MyPackets.NPCSay("Vous n'avez pas de bouclier d'�quip�."));
                                                SendPacket(General.MyPackets.NPCLink("Je suis d�sol�.", 255));
                                                SendPacket(General.MyPackets.NPCSetFace(30));
                                                SendPacket(General.MyPackets.NPCFinish());
                                                return;
                                            }
                                        else
                                            return;

                                        newitem = item[0];
                                        newitem = newitem.Remove(newitem.Length - 3, 1);

                                        if (Control == 4)
                                            newitem = newitem.Insert(newitem.Length - 2, "3");
                                        if (Control == 5)
                                            newitem = newitem.Insert(newitem.Length - 2, "4");
                                        if (Control == 6)
                                            newitem = newitem.Insert(newitem.Length - 2, "5");
                                        if (Control == 7)
                                            newitem = newitem.Insert(newitem.Length - 2, "6");
                                        if (Control == 8)
                                            newitem = newitem.Insert(newitem.Length - 2, "7");
                                        if (Control == 9)
                                            newitem = newitem.Insert(newitem.Length - 2, "8");
                                        if (Control == 10)
                                            newitem = newitem.Insert(newitem.Length - 2, "9");

                                        if (ChColor == 1)
                                        {
                                            MyChar.Equips[3] = newitem + "-" + item[1] + "-" + item[2] + "-" + item[3] + "-" + item[4] + "-" + item[5];
                                            SendPacket(General.MyPackets.AddItem((long)MyChar.Equips_UIDs[3], int.Parse(newitem), byte.Parse(item[1]), byte.Parse(item[2]), byte.Parse(item[3]), byte.Parse(item[4]), byte.Parse(item[5]), 3, 100, 100));
                                        }
                                        if (ChColor == 2)
                                        {
                                            MyChar.Equips[1] = newitem + "-" + item[1] + "-" + item[2] + "-" + item[3] + "-" + item[4] + "-" + item[5];
                                            SendPacket(General.MyPackets.AddItem((long)MyChar.Equips_UIDs[1], int.Parse(newitem), byte.Parse(item[1]), byte.Parse(item[2]), byte.Parse(item[3]), byte.Parse(item[4]), byte.Parse(item[5]), 1, 100, 100));
                                        }
                                        if (ChColor == 3)
                                        {
                                            MyChar.Equips[5] = newitem + "-" + item[1] + "-" + item[2] + "-" + item[3] + "-" + item[4] + "-" + item[5];
                                            SendPacket(General.MyPackets.AddItem((long)MyChar.Equips_UIDs[5], int.Parse(newitem), byte.Parse(item[1]), byte.Parse(item[2]), byte.Parse(item[3]), byte.Parse(item[4]), byte.Parse(item[5]), 5, 100, 100));
                                        }
                                        ChColor = 0;
                                    }
                                    catch (Exception Exc) { General.WriteLine(Convert.ToString(Exc)); }
                                }
                            }

                            if (CurrentNPC == 30015)
                            {
                                if (Control == 1)
                                {
                                    if (MyChar.InventoryContains(1088001, 1))
                                    {
                                        MyChar.RemoveItem(MyChar.ItemNext(1088001));
                                        MyChar.Teleport(1008, 22, 26);
                                    }
                                }
                                if (Control == 2)
                                {
                                    SendPacket(General.MyPackets.NPCSay("Je peux teindre votre armure en noire pour une Perle de Dragon. Vous pourrez toujours la changer de couleur par la suite. Voulez-vous toujours la teindre?"));
                                    SendPacket(General.MyPackets.NPCLink("Oui, voici une Perle de Dragon.", 3));
                                    SendPacket(General.MyPackets.NPCLink("Laisser moi y penser.", 255));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 3)
                                {
                                    if (MyChar.Equips[3] != null && MyChar.Equips[3] != "0")
                                    {
                                        if (MyChar.InventoryContains(1088000, 1))
                                        {
                                            MyChar.RemoveItem(MyChar.ItemNext(1088000));
                                            string[] item = MyChar.Equips[3].Split('-');
                                            string newitem = item[0];
                                            newitem = newitem.Remove(newitem.Length - 3, 1);
                                            newitem = newitem.Insert(newitem.Length - 2, "2");
                                            MyChar.Equips[3] = newitem + "-" + item[1] + "-" + item[2] + "-" + item[3] + "-" + item[4] + "-" + item[5];
                                            SendPacket(General.MyPackets.AddItem((long)MyChar.Equips_UIDs[3], int.Parse(newitem), byte.Parse(item[1]), byte.Parse(item[2]), byte.Parse(item[3]), byte.Parse(item[4]), byte.Parse(item[5]), 3, 100, 100));
                                        }
                                        else
                                        {
                                            SendPacket(General.MyPackets.NPCSay("Vous n'avez pas une Perle de Dragon."));
                                            SendPacket(General.MyPackets.NPCLink("Je vois.", 255));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                    }
                                }
                            }

                            #region Simon 2
                            if (CurrentNPC == 1152)
                            {
                                if (Control == 1)
                                {
                                    SendPacket(General.MyPackets.NPCSay("Mes anc�tres ont cr��s ce labyrinthe. Aujourd'hui les monstres en on prit possession. Plusieurs tr�sors y sont toujours."));
                                    SendPacket(General.MyPackets.NPCLink("Quel malheur!", 7));
                                    SendPacket(General.MyPackets.NPCLink("Je ne suis pas int�ress�.", 255));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 2)
                                {
                                    SendPacket(General.MyPackets.NPCSay("Deux m�t�ores pour 17 DiamantDeSoleils, quatres m�t�ores pour 17 DiamantDeLunes, une gemme normal pour 17 DiamantD`�toiles et une Bo�teAncienne pour 17 DiamantDeNuages."));
                                    SendPacket(General.MyPackets.NPCLink("Je vois. Merci.", 255));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }

                                if (Control == 3)//SunDiamonds 721533
                                {
                                    if (MyChar.InventoryContains(721533, 17))
                                    {
                                        MyChar.RemoveItem(MyChar.ItemNext(721533));
                                        MyChar.RemoveItem(MyChar.ItemNext(721533));
                                        MyChar.RemoveItem(MyChar.ItemNext(721533));
                                        MyChar.RemoveItem(MyChar.ItemNext(721533));
                                        MyChar.RemoveItem(MyChar.ItemNext(721533));
                                        MyChar.RemoveItem(MyChar.ItemNext(721533));
                                        MyChar.RemoveItem(MyChar.ItemNext(721533));
                                        MyChar.RemoveItem(MyChar.ItemNext(721533));
                                        MyChar.RemoveItem(MyChar.ItemNext(721533));
                                        MyChar.RemoveItem(MyChar.ItemNext(721533));
                                        MyChar.RemoveItem(MyChar.ItemNext(721533));
                                        MyChar.RemoveItem(MyChar.ItemNext(721533));
                                        MyChar.RemoveItem(MyChar.ItemNext(721533));
                                        MyChar.RemoveItem(MyChar.ItemNext(721533));
                                        MyChar.RemoveItem(MyChar.ItemNext(721533));
                                        MyChar.RemoveItem(MyChar.ItemNext(721533));
                                        MyChar.RemoveItem(MyChar.ItemNext(721533));
                                        MyChar.AddItem("1088001-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                        MyChar.AddItem("1088001-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous n'avez pas les 17 diamants..."));
                                        SendPacket(General.MyPackets.NPCLink("Je reviens", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                                if (Control == 4)//MoonDiamonds 721534
                                {
                                    if (MyChar.InventoryContains(721534, 17))
                                    {
                                        MyChar.RemoveItem(MyChar.ItemNext(721534));
                                        MyChar.RemoveItem(MyChar.ItemNext(721534));
                                        MyChar.RemoveItem(MyChar.ItemNext(721534));
                                        MyChar.RemoveItem(MyChar.ItemNext(721534));
                                        MyChar.RemoveItem(MyChar.ItemNext(721534));
                                        MyChar.RemoveItem(MyChar.ItemNext(721534));
                                        MyChar.RemoveItem(MyChar.ItemNext(721534));
                                        MyChar.RemoveItem(MyChar.ItemNext(721534));
                                        MyChar.RemoveItem(MyChar.ItemNext(721534));
                                        MyChar.RemoveItem(MyChar.ItemNext(721534));
                                        MyChar.RemoveItem(MyChar.ItemNext(721534));
                                        MyChar.RemoveItem(MyChar.ItemNext(721534));
                                        MyChar.RemoveItem(MyChar.ItemNext(721534));
                                        MyChar.RemoveItem(MyChar.ItemNext(721534));
                                        MyChar.RemoveItem(MyChar.ItemNext(721534));
                                        MyChar.RemoveItem(MyChar.ItemNext(721534));
                                        MyChar.RemoveItem(MyChar.ItemNext(721534));
                                        MyChar.AddItem("1088001-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                        MyChar.AddItem("1088001-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                        MyChar.AddItem("1088001-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                        MyChar.AddItem("1088001-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous n'avez pas les 17 diamants..."));
                                        SendPacket(General.MyPackets.NPCLink("Je reviens", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                                if (Control == 5)//StarDiamonds 721535
                                {
                                    if (MyChar.InventoryContains(721535, 17))
                                    {
                                        MyChar.RemoveItem(MyChar.ItemNext(721535));
                                        MyChar.RemoveItem(MyChar.ItemNext(721535));
                                        MyChar.RemoveItem(MyChar.ItemNext(721535));
                                        MyChar.RemoveItem(MyChar.ItemNext(721535));
                                        MyChar.RemoveItem(MyChar.ItemNext(721535));
                                        MyChar.RemoveItem(MyChar.ItemNext(721535));
                                        MyChar.RemoveItem(MyChar.ItemNext(721535));
                                        MyChar.RemoveItem(MyChar.ItemNext(721535));
                                        MyChar.RemoveItem(MyChar.ItemNext(721535));
                                        MyChar.RemoveItem(MyChar.ItemNext(721535));
                                        MyChar.RemoveItem(MyChar.ItemNext(721535));
                                        MyChar.RemoveItem(MyChar.ItemNext(721535));
                                        MyChar.RemoveItem(MyChar.ItemNext(721535));
                                        MyChar.RemoveItem(MyChar.ItemNext(721535));
                                        MyChar.RemoveItem(MyChar.ItemNext(721535));
                                        MyChar.RemoveItem(MyChar.ItemNext(721535));
                                        MyChar.RemoveItem(MyChar.ItemNext(721535));
                                        MyChar.AddItem("700011-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous n'avez pas les 17 diamants..."));
                                        SendPacket(General.MyPackets.NPCLink("Je reviens", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                                if (Control == 6)//CloudDiamonds 721536
                                {
                                    if (MyChar.InventoryContains(721536, 17))
                                    {
                                        MyChar.RemoveItem(MyChar.ItemNext(721536));
                                        MyChar.RemoveItem(MyChar.ItemNext(721536));
                                        MyChar.RemoveItem(MyChar.ItemNext(721536));
                                        MyChar.RemoveItem(MyChar.ItemNext(721536));
                                        MyChar.RemoveItem(MyChar.ItemNext(721536));
                                        MyChar.RemoveItem(MyChar.ItemNext(721536));
                                        MyChar.RemoveItem(MyChar.ItemNext(721536));
                                        MyChar.RemoveItem(MyChar.ItemNext(721536));
                                        MyChar.RemoveItem(MyChar.ItemNext(721536));
                                        MyChar.RemoveItem(MyChar.ItemNext(721536));
                                        MyChar.RemoveItem(MyChar.ItemNext(721536));
                                        MyChar.RemoveItem(MyChar.ItemNext(721536));
                                        MyChar.RemoveItem(MyChar.ItemNext(721536));
                                        MyChar.RemoveItem(MyChar.ItemNext(721536));
                                        MyChar.RemoveItem(MyChar.ItemNext(721536));
                                        MyChar.RemoveItem(MyChar.ItemNext(721536));
                                        MyChar.RemoveItem(MyChar.ItemNext(721536));
                                        MyChar.AddItem("721540-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous n'avez pas les 17 diamants..."));
                                        SendPacket(General.MyPackets.NPCLink("Je reviens", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }

                                if (Control == 7)
                                {
                                    SendPacket(General.MyPackets.NPCSay("J'ai toujours attendu une personne brave. Mais je ne peux pas croire qu'une personne n'a pas 2000 points de vertu."));
                                    SendPacket(General.MyPackets.NPCLink("Je veux bien.", 8));
                                    SendPacket(General.MyPackets.NPCLink("D�sol� ce n'est pas pour moi.", 255));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 8)
                                {
                                    if (MyChar.VP >= 2000)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Bien. Vous avez un grand coeur. Je ne peux pas croire que vous acceptiez de m'aider. Laissez moi vous dire quelques informations."));
                                        SendPacket(General.MyPackets.NPCLink("Merci.", 9));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous n'avez pas assez de points de vertu."));
                                        SendPacket(General.MyPackets.NPCLink("D�sol�...", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                                if (Control == 9)
                                {
                                    SendPacket(General.MyPackets.NPCSay("Une PreuveDuCiel, une PreuveDeLaTerre, une PreuveD`Ame sont requis pour passer au prochain �tage. While the eak monsters usually drop treasure. After you get a token, find a general who will send you to the next floor. Some boss monsters drop rare items."));
                                    SendPacket(General.MyPackets.NPCLink("Merci, je veux rentrer.", 10));
                                    SendPacket(General.MyPackets.NPCLink("Non merci je suis occup�.", 255));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 10)
                                {
                                    MyChar.VP -= 2000;
                                    MyChar.Teleport(1351, 016, 128);
                                }
                            }
                            #endregion

                            if (CurrentNPC == 5555)
                            {
                                if (Control == 1)
                                {
                                    SendPacket(General.MyPackets.NPCSay("Vous en avez " + MyChar.VP));
                                    SendPacket(General.MyPackets.NPCLink("Ok, merci.", 255));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 2)
                                {
                                    SendPacket(General.MyPackets.NPCSay("Je peux vous donner une m�t�ore pour 5.000 points de vertue et une perle de dragon pour 100.000 points de vertue."));
                                    SendPacket(General.MyPackets.NPCLink("M�t�ore", 3));
                                    SendPacket(General.MyPackets.NPCLink("Perle De Dragon", 4));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 3)
                                {
                                    if (MyChar.VP >= 5000)
                                    {
                                        if (MyChar.ItemsInInventory < 40)
                                        {
                                            if (MyChar.ItemsInInventory < 40)
                                            {
                                                MyChar.VP -= 5000;
                                                MyChar.AddItem("1088001-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                            }
                                            else
                                            {
                                                SendPacket(General.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "Votre sac est complet!", 2005));
                                            }
                                        }
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous n'avez pas 5,000 points de vertue."));
                                        SendPacket(General.MyPackets.NPCLink("D�sol�", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                                if (Control == 4)
                                {
                                    if (MyChar.VP >= 20000)
                                    {
                                        if (MyChar.ItemsInInventory < 40)
                                        {
                                            MyChar.VP -= 20000;
                                            MyChar.AddItem("1088000-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                        }
                                        else
                                        {
                                            SendPacket(General.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "Votre sac est complet!", 2005));
                                        }
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous n'avez pas 100,000 points de vertue."));
                                        SendPacket(General.MyPackets.NPCLink("D�sol�", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                            }
                            if (CurrentNPC == 8460)
                            {
                                if (Control == 1)
                                {
                                    if (MyChar.RBCount > 0 && (MyChar.InventoryContains(1088000, 1)))
                                    {
                                        MyChar.RemoveItem(MyChar.ItemNext(1088000));

                                        if (MyChar.Job > 9 && MyChar.Job < 16)
                                        {
                                            MyChar.Str = 5;
                                            MyChar.Agi = 2;
                                            MyChar.Vit = 3;
                                            MyChar.Spi = 0;
                                        }
                                        if (MyChar.Job > 19 && MyChar.Job < 26)
                                        {
                                            MyChar.Str = 5;
                                            MyChar.Agi = 2;
                                            MyChar.Vit = 3;
                                            MyChar.Spi = 0;
                                        }
                                        if (MyChar.Job > 39 && MyChar.Job < 46)
                                        {
                                            MyChar.Str = 2;
                                            MyChar.Agi = 7;
                                            MyChar.Vit = 1;
                                            MyChar.Spi = 0;
                                        }
                                        if (MyChar.Job > 129 && MyChar.Job < 136 || MyChar.Job > 139 && MyChar.Job < 146 || MyChar.Job == 100 || MyChar.Job == 101)
                                        {
                                            MyChar.Str = 0;
                                            MyChar.Agi = 2;
                                            MyChar.Vit = 3;
                                            MyChar.Spi = 5;
                                        }

                                        MyChar.StatP = 0;

                                        if ((MyChar.FirstLevel == 110) || (MyChar.FirstLevel == 111) && (MyChar.FirstJob == 135))
                                        {
                                            MyChar.StatP += 0;
                                        }
                                        else if ((MyChar.FirstLevel == 112) || (MyChar.FirstLevel == 113) && (MyChar.FirstJob == 135))
                                        {
                                            MyChar.StatP += 1;
                                        }
                                        else if ((MyChar.FirstLevel == 114) || (MyChar.FirstLevel == 115) && (MyChar.FirstJob == 135))
                                        {
                                            MyChar.StatP += 3;
                                        }
                                        else if ((MyChar.FirstLevel == 116) || (MyChar.FirstLevel == 117) && (MyChar.FirstJob == 135))
                                        {
                                            MyChar.StatP += 6;
                                        }
                                        else if ((MyChar.FirstLevel == 118) || (MyChar.FirstLevel == 119) && (MyChar.FirstJob == 135))
                                        {
                                            MyChar.StatP += 10;
                                        }
                                        else if (MyChar.FirstLevel == 120)
                                        {
                                            if (MyChar.FirstJob == 135)
                                            {
                                                MyChar.StatP += 15;
                                            }
                                            else
                                            {
                                                MyChar.StatP += 0;
                                            }
                                        }
                                        else if (MyChar.FirstLevel == 121)
                                        {
                                            if (MyChar.FirstJob == 135)
                                            {
                                                MyChar.StatP += 15;
                                            }
                                            else
                                            {
                                                MyChar.StatP += 1;
                                            }
                                        }
                                        else if (MyChar.FirstLevel == 122)
                                        {
                                            if (MyChar.FirstJob == 135)
                                            {
                                                MyChar.StatP += 21;
                                            }
                                            else
                                            {
                                                MyChar.StatP += 3;
                                            }
                                        }
                                        else if (MyChar.FirstLevel == 123)
                                        {
                                            if (MyChar.FirstJob == 135)
                                            {
                                                MyChar.StatP += 21;
                                            }
                                            else
                                            {
                                                MyChar.StatP += 6;
                                            }
                                        }
                                        else if (MyChar.FirstLevel == 124)
                                        {
                                            if (MyChar.FirstJob == 135)
                                            {
                                                MyChar.StatP += 28;
                                            }
                                            else
                                            {
                                                MyChar.StatP += 10;
                                            }
                                        }
                                        else if (MyChar.FirstLevel == 125)
                                        {
                                            if (MyChar.FirstJob == 135)
                                            {
                                                MyChar.StatP += 28;
                                            }
                                            else
                                            {
                                                MyChar.StatP += 15;
                                            }
                                        }
                                        else if (MyChar.FirstLevel == 126)
                                        {
                                            if (MyChar.FirstJob == 135)
                                            {
                                                MyChar.StatP += 36;
                                            }
                                            else
                                            {
                                                MyChar.StatP += 21;
                                            }
                                        }
                                        else if (MyChar.FirstLevel == 127)
                                        {
                                            if (MyChar.FirstJob == 135)
                                            {
                                                MyChar.StatP += 36;
                                            }
                                            else
                                            {
                                                MyChar.StatP += 28;
                                            }
                                        }
                                        else if (MyChar.FirstLevel == 128)
                                        {
                                            if (MyChar.FirstJob == 135)
                                            {
                                                MyChar.StatP += 45;
                                            }
                                            else
                                            {
                                                MyChar.StatP += 36;
                                            }
                                        }
                                        else if (MyChar.FirstLevel == 129)
                                        {
                                            if (MyChar.FirstJob == 135)
                                            {
                                                MyChar.StatP += 45;
                                            }
                                            else
                                            {
                                                MyChar.StatP += 45;
                                            }
                                        }
                                        else if (MyChar.FirstLevel == 130)
                                        {
                                            if (MyChar.FirstJob == 135)
                                            {
                                                MyChar.StatP += 55;
                                            }
                                            else
                                            {
                                                MyChar.StatP += 55;
                                            }
                                        }
                                        else if (MyChar.FirstLevel == 131)
                                        {
                                            if (MyChar.FirstJob == 135)
                                            {
                                                MyChar.StatP += 60;
                                            }
                                            else
                                            {
                                                MyChar.StatP += 60;
                                            }
                                        }
                                        else if (MyChar.FirstLevel == 132)
                                        {
                                            if (MyChar.FirstJob == 135)
                                            {
                                                MyChar.StatP += 65;
                                            }
                                            else
                                            {
                                                MyChar.StatP += 65;
                                            }
                                        }
                                        else if (MyChar.FirstLevel == 133)
                                        {
                                            if (MyChar.FirstJob == 135)
                                            {
                                                MyChar.StatP += 70;
                                            }
                                            else
                                            {
                                                MyChar.StatP += 70;
                                            }
                                        }
                                        else if (MyChar.FirstLevel == 134)
                                        {
                                            if (MyChar.FirstJob == 135)
                                            {
                                                MyChar.StatP += 75;
                                            }
                                            else
                                            {
                                                MyChar.StatP += 75;
                                            }
                                        }
                                        else if (MyChar.FirstLevel == 135)
                                        {
                                            if (MyChar.FirstJob == 135)
                                            {
                                                MyChar.StatP += 80;
                                            }
                                            else
                                            {
                                                MyChar.StatP += 80;
                                            }
                                        }
                                        else if (MyChar.FirstLevel == 136)
                                        {
                                            if (MyChar.FirstJob == 135)
                                            {
                                                MyChar.StatP += 85;
                                            }
                                            else
                                            {
                                                MyChar.StatP += 85;
                                            }
                                        }
                                        else if (MyChar.FirstLevel == 137)
                                        {
                                            if (MyChar.FirstJob == 135)
                                            {
                                                MyChar.StatP += 90;
                                            }
                                            else
                                            {
                                                MyChar.StatP += 90;
                                            }
                                        }


                                        if ((MyChar.SecondLevel == 110) || (MyChar.SecondLevel == 111) && (MyChar.SecondJob == 135))
                                        {
                                            MyChar.StatP += 0;
                                        }
                                        else if ((MyChar.SecondLevel == 112) || (MyChar.SecondLevel == 113) && (MyChar.SecondJob == 135))
                                        {
                                            MyChar.StatP += 1;
                                        }
                                        else if ((MyChar.SecondLevel == 114) || (MyChar.SecondLevel == 115) && (MyChar.SecondJob == 135))
                                        {
                                            MyChar.StatP += 3;
                                        }
                                        else if ((MyChar.SecondLevel == 116) || (MyChar.SecondLevel == 117) && (MyChar.SecondJob == 135))
                                        {
                                            MyChar.StatP += 6;
                                        }
                                        else if ((MyChar.SecondLevel == 118) || (MyChar.SecondLevel == 119) && (MyChar.SecondJob == 135))
                                        {
                                            MyChar.StatP += 10;
                                        }
                                        else if (MyChar.SecondLevel == 120)
                                        {
                                            if (MyChar.SecondJob == 135)
                                            {
                                                MyChar.StatP += 15;
                                            }
                                            else
                                            {
                                                MyChar.StatP += 0;
                                            }
                                        }
                                        else if (MyChar.SecondLevel == 121)
                                        {
                                            if (MyChar.SecondJob == 135)
                                            {
                                                MyChar.StatP += 15;
                                            }
                                            else
                                            {
                                                MyChar.StatP += 1;
                                            }
                                        }
                                        else if (MyChar.SecondLevel == 122)
                                        {
                                            if (MyChar.SecondJob == 135)
                                            {
                                                MyChar.StatP += 21;
                                            }
                                            else
                                            {
                                                MyChar.StatP += 3;
                                            }
                                        }
                                        else if (MyChar.SecondLevel == 123)
                                        {
                                            if (MyChar.SecondJob == 135)
                                            {
                                                MyChar.StatP += 21;
                                            }
                                            else
                                            {
                                                MyChar.StatP += 6;
                                            }
                                        }
                                        else if (MyChar.SecondLevel == 124)
                                        {
                                            if (MyChar.SecondJob == 135)
                                            {
                                                MyChar.StatP += 28;
                                            }
                                            else
                                            {
                                                MyChar.StatP += 10;
                                            }
                                        }
                                        else if (MyChar.SecondLevel == 125)
                                        {
                                            if (MyChar.SecondJob == 135)
                                            {
                                                MyChar.StatP += 28;
                                            }
                                            else
                                            {
                                                MyChar.StatP += 15;
                                            }
                                        }
                                        else if (MyChar.SecondLevel == 126)
                                        {
                                            if (MyChar.SecondJob == 135)
                                            {
                                                MyChar.StatP += 36;
                                            }
                                            else
                                            {
                                                MyChar.StatP += 21;
                                            }
                                        }
                                        else if (MyChar.SecondLevel == 127)
                                        {
                                            if (MyChar.SecondJob == 135)
                                            {
                                                MyChar.StatP += 36;
                                            }
                                            else
                                            {
                                                MyChar.StatP += 28;
                                            }
                                        }
                                        else if (MyChar.SecondLevel == 128)
                                        {
                                            if (MyChar.SecondJob == 135)
                                            {
                                                MyChar.StatP += 45;
                                            }
                                            else
                                            {
                                                MyChar.StatP += 36;
                                            }
                                        }
                                        else if (MyChar.SecondLevel == 129)
                                        {
                                            if (MyChar.SecondJob == 135)
                                            {
                                                MyChar.StatP += 45;
                                            }
                                            else
                                            {
                                                MyChar.StatP += 45;
                                            }
                                        }
                                        else if (MyChar.SecondLevel == 130)
                                        {
                                            if (MyChar.SecondJob == 135)
                                            {
                                                MyChar.StatP += 55;
                                            }
                                            else
                                            {
                                                MyChar.StatP += 55;
                                            }
                                        }
                                        else if (MyChar.SecondLevel == 131)
                                        {
                                            if (MyChar.SecondJob == 135)
                                            {
                                                MyChar.StatP += 60;
                                            }
                                            else
                                            {
                                                MyChar.StatP += 60;
                                            }
                                        }
                                        else if (MyChar.SecondLevel == 132)
                                        {
                                            if (MyChar.SecondJob == 135)
                                            {
                                                MyChar.StatP += 65;
                                            }
                                            else
                                            {
                                                MyChar.StatP += 65;
                                            }
                                        }
                                        else if (MyChar.SecondLevel == 133)
                                        {
                                            if (MyChar.SecondJob == 135)
                                            {
                                                MyChar.StatP += 70;
                                            }
                                            else
                                            {
                                                MyChar.StatP += 70;
                                            }
                                        }
                                        else if (MyChar.SecondLevel == 134)
                                        {
                                            if (MyChar.SecondJob == 135)
                                            {
                                                MyChar.StatP += 75;
                                            }
                                            else
                                            {
                                                MyChar.StatP += 75;
                                            }
                                        }
                                        else if (MyChar.SecondLevel == 135)
                                        {
                                            if (MyChar.SecondJob == 135)
                                            {
                                                MyChar.StatP += 80;
                                            }
                                            else
                                            {
                                                MyChar.StatP += 80;
                                            }
                                        }
                                        else if (MyChar.SecondLevel == 136)
                                        {
                                            if (MyChar.SecondJob == 135)
                                            {
                                                MyChar.StatP += 85;
                                            }
                                            else
                                            {
                                                MyChar.StatP += 85;
                                            }
                                        }
                                        else if (MyChar.SecondLevel == 137)
                                        {
                                            if (MyChar.SecondJob == 135)
                                            {
                                                MyChar.StatP += 90;
                                            }
                                            else
                                            {
                                                MyChar.StatP += 90;
                                            }
                                        }

                                        uint levelstat = 0;

                                        levelstat = ((uint)MyChar.Level - 15) * 3;
                                        MyChar.StatP += levelstat;
                                        MyChar.StatP += 42;
                                        MyChar.StatP += 30;

                                        MyChar.MyClient.SendPacket(General.MyPackets.Vital(MyChar.UID, 11, MyChar.StatP));
                                        MyChar.MyClient.SendPacket(General.MyPackets.Vital((long)MyChar.UID, 16, MyChar.Str));
                                        MyChar.MyClient.SendPacket(General.MyPackets.Vital((long)MyChar.UID, 17, MyChar.Agi));
                                        MyChar.MyClient.SendPacket(General.MyPackets.Vital((long)MyChar.UID, 15, MyChar.Vit));
                                        MyChar.MyClient.SendPacket(General.MyPackets.Vital((long)MyChar.UID, 14, MyChar.Spi));
                                        MyChar.MyClient.SendPacket(General.MyPackets.Vital((long)MyChar.UID, 2, MyChar.MaxMana()));
                                        MyChar.MyClient.SendPacket(General.MyPackets.Vital((long)MyChar.UID, 0, MyChar.CurHP));
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous n'�tes pas qualifi� pour cette action. Revenez quand vous serez rena�t et niveau 70."));
                                        SendPacket(General.MyPackets.NPCLink("Je vois.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                            }
                            if (CurrentNPC == 2410)
                            {
                                if (Control == 1)
                                {
                                    SendPacket(General.MyPackets.NPCSay("Si vous me donnez une Perle de Dragon je peux changer votre taille. "));
                                    SendPacket(General.MyPackets.NPCLink("Voici une Perle de Dragon.", 2));
                                    SendPacket(General.MyPackets.NPCLink("Je n'ai pas de Perle de Dragon.", 255));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 2)
                                {
                                    if (MyChar.InventoryContains(1088000, 1))
                                    {
                                        MyChar.RemoveItem(MyChar.ItemNext(1088000));

                                        if (MyChar.Model == 1004)
                                            MyChar.Model -= 1;
                                        else if (MyChar.Model == 1003)
                                            MyChar.Model += 1;
                                        if (MyChar.Model == 2002)
                                            MyChar.Model -= 1;
                                        else if (MyChar.Model == 2001)
                                            MyChar.Model += 1;


                                        SendPacket(General.MyPackets.Vital(MyChar.UID, 12, ulong.Parse(MyChar.Avatar.ToString() + MyChar.Model.ToString())));
                                        World.UpdateSpawn(MyChar);

                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("D�sol�, vous n'avez pas de Perle de Dragon."));
                                        SendPacket(General.MyPackets.NPCLink("Ok, je vois.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                            }

                            #region LAB NPCS
                            if (CurrentNPC == 1234)
                            {
                                if (Control == 1)
                                {
                                    if (MyChar.InventoryContains(721537, 1))
                                    {
                                        MyChar.Teleport(1352, 028, 223);
                                        MyChar.RemoveItem(MyChar.ItemNext(721537));
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous n'avez pas la PreuveDuCiel!"));
                                        SendPacket(General.MyPackets.NPCLink("D�sol�...", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                                if (Control == 2)
                                {
                                    MyChar.Teleport(1002, 396, 233);
                                }
                            }
                            if (CurrentNPC == 1153)
                            {
                                if (Control == 1)
                                {
                                    if (MyChar.InventoryContains(721538, 1))
                                    {
                                        MyChar.Teleport(1353, 028, 268);
                                        MyChar.RemoveItem(MyChar.ItemNext(721538));
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous n'avez pas la PreuveDeLaTerre!"));
                                        SendPacket(General.MyPackets.NPCLink("D�sol�...", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                                if (Control == 2)
                                {
                                    MyChar.Teleport(1002, 396, 233);
                                }
                            }
                            if (CurrentNPC == 1154)
                            {
                                if (Control == 1)
                                {
                                    if (MyChar.InventoryContains(721539, 1))
                                    {
                                        MyChar.Teleport(1354, 009, 290);
                                        MyChar.RemoveItem(MyChar.ItemNext(721539));
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous n'avez pas la PreuveD`Ame!"));
                                        SendPacket(General.MyPackets.NPCLink("D�sol�...", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                                if (Control == 2)
                                {
                                    MyChar.Teleport(1002, 396, 233);
                                }
                            }
                            #endregion
                            #region FireTaoTrainer
                            if (CurrentNPC == 12)
                            {
                                if (Control == 100)
                                {
                                    if (MyChar.Job < 146 && MyChar.Job > 139)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Voici les pouvoirs que je peux vous enseigner si vous avez le niveau requis."));
                                        SendPacket(General.MyPackets.NPCLink("�clair: Lv 15", 104));
                                        SendPacket(General.MyPackets.NPCLink("Feu: Lv 40", 105));
                                        SendPacket(General.MyPackets.NPCLink("Volcan: Lv 40", 106));
                                        SendPacket(General.MyPackets.NPCLink("M�ditation: Lv 44", 107));
                                        SendPacket(General.MyPackets.NPCLink("M�t�ore de feu: Lv 52", 103));
                                        SendPacket(General.MyPackets.NPCLink("Anneau de feu: Lv 55", 102));
                                        SendPacket(General.MyPackets.NPCLink("Cercle de feu: Lv 65", 101));
                                        SendPacket(General.MyPackets.NPCLink("Vent: Lv 90", 108));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous n'�tes pas un tao�ste de feu partez!"));
                                        SendPacket(General.MyPackets.NPCLink("Je suis d�sol�.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                                if (MyChar.Job < 146 && MyChar.Job > 139)
                                {
                                    if (Control == 250)
                                    {
                                        if (MyChar.Level >= 70)
                                            if (MyChar.Job < 136 && MyChar.Job > 129)
                                            {
                                                MyChar.LearnSkill(1120, 0);
                                            }
                                    }
                                    if (Control == 101)
                                    {
                                        if (MyChar.Level >= 65)
                                        {
                                            MyChar.LearnSkill(1120, 0);
                                        }
                                    }
                                    if (Control == 102)
                                    {
                                        if (MyChar.Level >= 55)
                                        {
                                            MyChar.LearnSkill(1150, 0);
                                        }
                                    }
                                    if (Control == 103)
                                    {
                                        if (MyChar.Level >= 52)
                                        {
                                            MyChar.LearnSkill(1180, 0);
                                        }
                                    }
                                    if (Control == 104)
                                    {
                                        if (MyChar.Level >= 15)
                                        {
                                            MyChar.LearnSkill(1010, 0);
                                        }
                                    }
                                    if (Control == 105)
                                    {
                                        if (MyChar.Level >= 40)
                                        {
                                            MyChar.LearnSkill(1001, 0);
                                        }
                                    }
                                    if (Control == 106)
                                    {
                                        if (MyChar.Level >= 40)
                                        {
                                            MyChar.LearnSkill(1125, 0);
                                        }
                                    }
                                    if (Control == 107)
                                    {
                                        if (MyChar.Level >= 44)
                                        {
                                            MyChar.LearnSkill(1195, 0);
                                        }
                                    }
                                    if (Control == 108)
                                    {
                                        if (MyChar.Level >= 90)
                                        {
                                            MyChar.LearnSkill(1002, 0);
                                        }
                                    }
                                }
                                if (Control == 1)
                                {
                                    if (MyChar.Job == 100)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Pour avancer en Tao�ste vous devez �tre niveau 15."));
                                    }
                                    if (MyChar.Job == 101)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Pour avancer en Tao�ste De Feu ou D'eau vous devez �tre niveau 40. Quel classe voulez-vous choisir?"));
                                    }
                                    if (MyChar.Job == 142)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Pour avancer en Magicien De Feu vous devez �tre niveau 70."));
                                    }
                                    if (MyChar.Job == 143)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Pour avancer en Sorcier De Feu vous devez �tre niveau 100."));
                                    }
                                    if (MyChar.Job == 144)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Pour avancer en Enchanteur De Feu vous devez �tre niveau 110."));
                                    }
                                    if (MyChar.Job != 145 && MyChar.Job != 101)
                                    {
                                        SendPacket(General.MyPackets.NPCLink("Je veux avancer.", 3));
                                        SendPacket(General.MyPackets.NPCLink("Je sais, je ne suis pas qualifi�.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else if (MyChar.Job == 101)
                                    {
                                        SendPacket(General.MyPackets.NPCLink("Tao�ste De Feu", 3));
                                        SendPacket(General.MyPackets.NPCLink("Tao�ste D`eau", 2));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous ne pouvez plus avancer."));
                                        SendPacket(General.MyPackets.NPCLink("D�sol�.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                                if (Control == 2)
                                {
                                    bool Promoted = false;

                                    if (MyChar.Job == 100 && MyChar.Level >= 15)
                                    {
                                        MyChar.Job = 101;
                                        Promoted = true;
                                    }
                                    else if (MyChar.Job == 101 && MyChar.Level >= 40)
                                    {
                                        MyChar.Job = 132;
                                        Promoted = true;
                                    }

                                    if (Promoted)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("F�licitation, vous avez avancer."));
                                        SendPacket(General.MyPackets.NPCLink("Cool!.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());

                                        SendPacket(General.MyPackets.Vital(MyChar.UID, 7, MyChar.Job));
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous ne pouvez pas avancer."));
                                        SendPacket(General.MyPackets.NPCLink("Je vois.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }

                                if (Control == 3)
                                {
                                    bool Promoted = false;

                                    if (MyChar.Job == 100 && MyChar.Level >= 15)
                                    {
                                        MyChar.Job = 101;
                                        Promoted = true;
                                    }
                                    else if (MyChar.Job == 101 && MyChar.Level >= 40)
                                    {
                                        MyChar.Job = 142;
                                        Promoted = true;
                                    }
                                    else if (MyChar.Job == 142 && MyChar.Level >= 70)
                                    {
                                        MyChar.Job = 143;
                                        Promoted = true;
                                    }
                                    else if (MyChar.Job == 143 && MyChar.Level >= 100)
                                    {
                                        if (MyChar.RBCount == 1)
                                        {
                                            MyChar.AddItem("134287-0-0-0-31-0", 0, (uint)General.Rand.Next(36457836));
                                        }
                                        else if (MyChar.RBCount == 2)
                                        {
                                            MyChar.AddItem("1088000-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                        }
                                        MyChar.Job = 144;
                                        Promoted = true;
                                    }
                                    else if (MyChar.Job == 144 && MyChar.Level >= 110)
                                    {
                                        if (MyChar.RBCount == 0)
                                        {
                                            MyChar.AddItem("700002-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                        }
                                        else if (MyChar.RBCount == 1)
                                        {
                                            MyChar.AddItem("1088000-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                        }
                                        else if (MyChar.RBCount == 2)
                                        {
                                            MyChar.AddItem("160199-0-0-0-33-0", 0, (uint)General.Rand.Next(36457836));
                                        }
                                        MyChar.Job = 145;
                                        Promoted = true;
                                    }

                                    if (Promoted)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("F�licitation, vous avez avancer."));
                                        SendPacket(General.MyPackets.NPCLink("Cool!.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());

                                        SendPacket(General.MyPackets.Vital(MyChar.UID, 7, MyChar.Job));
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous ne pouvez pas avancer."));
                                        SendPacket(General.MyPackets.NPCLink("Je vois.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                            }

                            #endregion
                            #region WaterTaoTrainer
                            if (CurrentNPC == 264)
                            {
                                if (Control == 100)
                                {
                                    if (MyChar.Job < 136 && MyChar.Job > 129)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Voici les pouvoirs que je peux vous enseigner si vous avez le niveau requis."));
                                        SendPacket(General.MyPackets.NPCLink("�clair: Lv 15", 106));
                                        SendPacket(General.MyPackets.NPCLink("Feu: Lv 40", 107));
                                        SendPacket(General.MyPackets.NPCLink("Volcan: Lv 40", 108));
                                        SendPacket(General.MyPackets.NPCLink("Cure de pluie: Lv 40", 109));
                                        SendPacket(General.MyPackets.NPCLink("Revivre: Lv 40", 110));
                                        SendPacket(General.MyPackets.NPCLink("M�ditation: Lv 44", 103));
                                        SendPacket(General.MyPackets.NPCLink("�toile exacte: Lv 45", 111));
                                        SendPacket(General.MyPackets.NPCLink("Suivante...", 200)); ;
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous n'�tes pas un Tao�ste D'eau partez!"));
                                        SendPacket(General.MyPackets.NPCLink("Je suis d�sol�.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                                if (Control == 200)
                                {
                                    if (MyChar.Job < 136 && MyChar.Job > 129)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Voici les pouvoirs que je peux vous enseigner si vous avez le niveau requis."));
                                        SendPacket(General.MyPackets.NPCLink("Bouclier magique: Lv 50", 112));
                                        SendPacket(General.MyPackets.NPCLink("Stigmate rouge: Lv 55", 101));
                                        SendPacket(General.MyPackets.NPCLink("Invisibilit�: Lv 60", 113));
                                        SendPacket(General.MyPackets.NPCLink("Pri�re: Lv 70", 102));
                                        SendPacket(General.MyPackets.NPCLink("Cure avanc�e: Lv 81", 114));
                                        SendPacket(General.MyPackets.NPCLink("Nectar: Lv 94", 115));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous n'�tes pas un Tao�ste D'eau partez!"));
                                        SendPacket(General.MyPackets.NPCLink("Je suis d�sol�.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                                if (MyChar.Job < 136 && MyChar.Job > 129)
                                {

                                    if (Control == 101)
                                    {
                                        if (MyChar.Level >= 55)
                                        {
                                            MyChar.LearnSkill(1095, 0);
                                        }
                                    }
                                    if (Control == 102)
                                    {
                                        if (MyChar.Level >= 70)
                                        {
                                            MyChar.LearnSkill(1100, 0);
                                        }
                                    }
                                    if (Control == 103)
                                    {
                                        if (MyChar.Level >= 44)
                                        {
                                            MyChar.LearnSkill(1195, 0);
                                        }
                                    }
                                    if (Control == 106)
                                    {
                                        if (MyChar.Level >= 15)
                                        {
                                            MyChar.LearnSkill(1010, 0);
                                        }
                                    }
                                    if (Control == 107)
                                    {
                                        if (MyChar.Level >= 40)
                                        {
                                            MyChar.LearnSkill(1001, 0);
                                        }
                                    }
                                    if (Control == 108)
                                    {
                                        if (MyChar.Level >= 40)
                                        {
                                            MyChar.LearnSkill(1125, 0);
                                        }
                                    }
                                    if (Control == 109)
                                    {
                                        if (MyChar.Level >= 40)
                                        {
                                            MyChar.LearnSkill(1055, 0);
                                        }
                                    }
                                    if (Control == 110)
                                    {
                                        if (MyChar.Level >= 40)
                                        {
                                            MyChar.LearnSkill(1050, 0);
                                        }
                                    }
                                    if (Control == 111)
                                    {
                                        if (MyChar.Level >= 45)
                                        {
                                            MyChar.LearnSkill(1085, 0);
                                        }
                                    }
                                    if (Control == 112)
                                    {
                                        if (MyChar.Level >= 50)
                                        {
                                            MyChar.LearnSkill(1090, 0);
                                        }
                                    }
                                    if (Control == 113)
                                    {
                                        if (MyChar.Level >= 60)
                                        {
                                            MyChar.LearnSkill(1075, 0);
                                        }
                                    }
                                    if (Control == 114)
                                    {
                                        if (MyChar.Level >= 81)
                                        {
                                            MyChar.LearnSkill(1175, 0);
                                        }
                                    }
                                    if (Control == 115)
                                    {
                                        if (MyChar.Level >= 94)
                                        {
                                            MyChar.LearnSkill(1170, 0);
                                        }
                                    }
                                }
                                if (Control == 1)
                                {
                                    if (MyChar.Job == 100)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Pour avancer en Tao�ste vous devez �tre niveau 15."));
                                    }
                                    if (MyChar.Job == 101)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Pour avancer en Tao�ste De Feu ou D'eau vous devez �tre niveau 40. Quel classe voulez-vous choisir?"));
                                    }
                                    if (MyChar.Job == 132)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Pour avancer en Magicien D'eau vous devez �tre niveau 70."));
                                    }
                                    if (MyChar.Job == 133)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Pour avancer en Sorcier D'eau vous devez �tre niveau 100."));
                                    }
                                    if (MyChar.Job == 134)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Pour avancer en Enchanteur D'eau vous devez �tre niveau 110."));
                                    }
                                    if (MyChar.Job != 135 && MyChar.Job != 101)
                                    {
                                        SendPacket(General.MyPackets.NPCLink("Je veux avancer.", 2));
                                        SendPacket(General.MyPackets.NPCLink("Je sais, je ne suis pas qualifi�.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else if (MyChar.Job == 101)
                                    {
                                        SendPacket(General.MyPackets.NPCLink("Tao�ste De Feu", 3));
                                        SendPacket(General.MyPackets.NPCLink("Tao�ste D`eau", 2));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous ne pouvez plus avancer."));
                                        SendPacket(General.MyPackets.NPCLink("D�sol�.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                                if (Control == 2)
                                {
                                    bool Promoted = false;

                                    if (MyChar.Job == 100 && MyChar.Level >= 15)
                                    {
                                        MyChar.Job = 101;
                                        Promoted = true;
                                    }
                                    else if (MyChar.Job == 101 && MyChar.Level >= 40)
                                    {
                                        MyChar.Job = 132;
                                        Promoted = true;
                                    }
                                    else if (MyChar.Job == 132 && MyChar.Level >= 70)
                                    {
                                        MyChar.Job = 133;
                                        Promoted = true;
                                    }
                                    else if (MyChar.Job == 133 && MyChar.Level >= 100)
                                    {
                                        if (MyChar.RBCount == 1)
                                        {
                                            MyChar.AddItem("134287-0-0-0-31-0", 0, (uint)General.Rand.Next(36457836));
                                        }
                                        else if (MyChar.RBCount == 2)
                                        {
                                            MyChar.AddItem("1088000-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                        }
                                        MyChar.Job = 134;
                                        Promoted = true;
                                    }
                                    else if (MyChar.Job == 134 && MyChar.Level >= 110)
                                    {
                                        if (MyChar.RBCount == 0)
                                        {
                                            MyChar.AddItem("700032-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                        }
                                        else if (MyChar.RBCount == 1)
                                        {
                                            MyChar.AddItem("1088000-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                        }
                                        else if (MyChar.RBCount == 2)
                                        {
                                            MyChar.AddItem("160199-0-0-0-33-0", 0, (uint)General.Rand.Next(36457836));
                                        }
                                        MyChar.Job = 135;
                                        Promoted = true;
                                    }

                                    if (Promoted)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("F�licitation, vous avez avancer."));
                                        SendPacket(General.MyPackets.NPCLink("Cool!.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());

                                        SendPacket(General.MyPackets.Vital(MyChar.UID, 7, MyChar.Job));
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous ne pouvez pas avancer."));
                                        SendPacket(General.MyPackets.NPCLink("Je vois.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }

                                if (Control == 3)
                                {
                                    bool Promoted = false;

                                    if (MyChar.Job == 100 && MyChar.Level >= 15)
                                    {
                                        MyChar.Job = 101;
                                        Promoted = true;
                                    }
                                    else if (MyChar.Job == 101 && MyChar.Level >= 40)
                                    {
                                        MyChar.Job = 142;
                                        Promoted = true;
                                    }

                                    if (Promoted)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("F�licitation, vous avez avancer."));
                                        SendPacket(General.MyPackets.NPCLink("Cool!.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());

                                        SendPacket(General.MyPackets.Vital(MyChar.UID, 7, MyChar.Job));
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous ne pouvez pas avancer."));
                                        SendPacket(General.MyPackets.NPCLink("Je vois.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                            }

                            #endregion

                            if (CurrentNPC == 17)
                            {
                                if (MyChar.Job < 16 && MyChar.Job > 9 || Control == 255)
                                {
                                    if (Control == 222)
                                    {

                                        SendPacket(General.MyPackets.NPCSay("Voici les pouvoirs que je peux vous enseigner si vous avez le niveau requis."));
                                        SendPacket(General.MyPackets.NPCLink("Cyclone: Lv 15", 34));
                                        SendPacket(General.MyPackets.NPCLink("Exactitude: Lv 15", 35));
                                        SendPacket(General.MyPackets.NPCLink("Hercule: Lv 40", 31));
                                        SendPacket(General.MyPackets.NPCLink("Cure d'esprit: Lv 40", 32));
                                        SendPacket(General.MyPackets.NPCLink("Robot: Lv 40", 33));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());

                                    }
                                    if (Control == 31)
                                    {
                                        if (MyChar.Level >= 40)
                                            MyChar.LearnSkill(1115, 0);
                                        else
                                        {
                                            SendPacket(General.MyPackets.NPCSay("Vous n'avez pas le niveau requis."));
                                            SendPacket(General.MyPackets.NPCLink("Ok.", 255));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                    }
                                    if (Control == 32)
                                    {
                                        if (MyChar.Level >= 40)
                                            MyChar.LearnSkill(1190, 0);
                                        else
                                        {
                                            SendPacket(General.MyPackets.NPCSay("Vous n'avez pas le niveau requis."));
                                            SendPacket(General.MyPackets.NPCLink("Ok.", 255));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                    }
                                    if (Control == 33)
                                    {
                                        if (MyChar.Level >= 40)
                                            MyChar.LearnSkill(1270, 0);
                                        else
                                        {
                                            SendPacket(General.MyPackets.NPCSay("Vous n'avez pas le niveau requis."));
                                            SendPacket(General.MyPackets.NPCLink("Ok.", 255));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                    }
                                    if (Control == 34)
                                    {
                                        if (MyChar.Level >= 15)
                                            MyChar.LearnSkill(1110, 0);
                                        else
                                        {
                                            SendPacket(General.MyPackets.NPCSay("Vous n'avez pas le niveau requis."));
                                            SendPacket(General.MyPackets.NPCLink("Ok.", 255));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                    }
                                    if (Control == 35)
                                    {
                                        if (MyChar.Level >= 15)
                                            MyChar.LearnSkill(1015, 0);
                                        else
                                        {
                                            SendPacket(General.MyPackets.NPCSay("Vous n'avez pas le niveau requis."));
                                            SendPacket(General.MyPackets.NPCLink("Ok.", 255));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                    }
                                }

                                if (Control == 1)
                                {
                                    if (MyChar.Job == 10)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Pour avancer en Brave vous devez �tre niveau 15."));
                                    }
                                    if (MyChar.Job == 11)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Pour avancer en Brave De Force vous devez �tre niveau 40."));
                                    }
                                    if (MyChar.Job == 12)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Pour avancer en Brave De Tigre vous devez �tre niveau 70."));
                                    }
                                    if (MyChar.Job == 13)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Pour avancer en Brave De Dragon vous devez �tre niveau 100."));
                                    }
                                    if (MyChar.Job == 14)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Pour avancer en Brave De Force vous devez �tre niveau 110."));
                                    }
                                    if (MyChar.Job != 15)
                                    {
                                        SendPacket(General.MyPackets.NPCLink("Je veux avancer.", 2));
                                        SendPacket(General.MyPackets.NPCLink("Je sais, je ne suis pas qualifi�.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous ne pouvez plus avancer."));
                                        SendPacket(General.MyPackets.NPCLink("D�sol�.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                                if (Control == 2)
                                {
                                    bool Promoted = false;

                                    if (MyChar.Job == 10 && MyChar.Level >= 15)
                                    {
                                        MyChar.Job = 11;
                                        Promoted = true;
                                    }
                                    else if (MyChar.Job == 11 && MyChar.Level >= 40)
                                    {
                                        MyChar.Job = 12;
                                        Promoted = true;
                                    }
                                    else if (MyChar.Job == 12 && MyChar.Level >= 70)
                                    {
                                        MyChar.Job = 13;
                                        Promoted = true;
                                    }
                                    else if (MyChar.Job == 13 && MyChar.Level >= 100)
                                    {
                                        if (MyChar.RBCount == 1)
                                        {
                                            MyChar.AddItem("130287-0-0-0-11-0", 0, (uint)General.Rand.Next(36457836));
                                        }
                                        else if (MyChar.RBCount == 2)
                                        {
                                            MyChar.AddItem("1088000-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                        }
                                        MyChar.Job = 14;
                                        Promoted = true;
                                    }
                                    else if (MyChar.Job == 14 && MyChar.Level >= 110)
                                    {
                                        if (MyChar.RBCount == 0)
                                        {
                                            MyChar.AddItem("700012-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                        }
                                        else if (MyChar.RBCount == 1)
                                        {
                                            MyChar.AddItem("1088000-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                        }
                                        else if (MyChar.RBCount == 2)
                                        {
                                            MyChar.AddItem("160199-0-0-0-13-0", 0, (uint)General.Rand.Next(36457836));
                                        }
                                        MyChar.Job = 15;
                                        Promoted = true;
                                    }

                                    if (Promoted)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("F�licitation, vous avez avancer."));
                                        SendPacket(General.MyPackets.NPCLink("Cool!.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());

                                        SendPacket(General.MyPackets.Vital(MyChar.UID, 7, MyChar.Job));
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous ne pouvez pas avancer."));
                                        SendPacket(General.MyPackets.NPCLink("Je vois.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                            }

                            if (CurrentNPC == 6)
                            {
                                if (MyChar.Job < 46 && MyChar.Job > 39 || Control == 255)
                                {
                                    if (Control == 20)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Voici les pouvoirs que je peux vous enseigner si vous avez le niveau requis."));
                                        SendPacket(General.MyPackets.NPCLink("Vol(XP Skill): Lv 15", 24));
                                        SendPacket(General.MyPackets.NPCLink("�parpillement: Lv 23", 21));
                                        SendPacket(General.MyPackets.NPCLink("Feu Rapide: Lv 46", 22));
                                        SendPacket(General.MyPackets.NPCLink("Intensification: Lv 71", 23));
                                        SendPacket(General.MyPackets.NPCLink("Fl�cheDePluie: Lv 70", 25));
                                        SendPacket(General.MyPackets.NPCLink("Vol: Lv 70", 27));
                                        SendPacket(General.MyPackets.NPCLink("Vol avanc�: Lv 100", 26));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    if (Control == 21)
                                    {
                                        if (MyChar.Level < 23)
                                        {

                                            SendPacket(General.MyPackets.NPCSay("Vous n'avez pas le niveau requis."));
                                            SendPacket(General.MyPackets.NPCLink("Ok.", 255));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                        else
                                        {
                                            MyChar.LearnSkill(8001, 0);
                                            SendPacket(General.MyPackets.NPCSay("F�licitation! Vous avez appris le pouvoir!"));
                                            SendPacket(General.MyPackets.NPCLink("Cool!", 255));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                    }
                                    if (Control == 22)
                                    {
                                        if (MyChar.Level < 46)
                                        {

                                            SendPacket(General.MyPackets.NPCSay("Vous n'avez pas le niveau requis."));
                                            SendPacket(General.MyPackets.NPCLink("Ok.", 255));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                        else
                                        {
                                            MyChar.LearnSkill(8000, 0);
                                            SendPacket(General.MyPackets.NPCSay("F�licitation! Vous avez appris le pouvoir!"));
                                            SendPacket(General.MyPackets.NPCLink("Cool!", 255));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                    }
                                    if (Control == 23)
                                    {
                                        if (MyChar.Level < 71)
                                        {

                                            SendPacket(General.MyPackets.NPCSay("Vous n'avez pas le niveau requis."));
                                            SendPacket(General.MyPackets.NPCLink("Ok.", 255));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                        else
                                        {
                                            MyChar.LearnSkill(9000, 0);
                                            SendPacket(General.MyPackets.NPCSay("F�licitation! Vous avez appris le pouvoir!"));
                                            SendPacket(General.MyPackets.NPCLink("Cool!", 255));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                    }
                                    if (Control == 24)
                                    {
                                        if (MyChar.Level < 15)
                                        {

                                            SendPacket(General.MyPackets.NPCSay("Vous n'avez pas le niveau requis."));
                                            SendPacket(General.MyPackets.NPCLink("Ok.", 255));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                        else
                                        {
                                            MyChar.LearnSkill(8002, 0);
                                            SendPacket(General.MyPackets.NPCSay("F�licitation! Vous avez appris le pouvoir!"));
                                            SendPacket(General.MyPackets.NPCLink("Cool!", 255));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                    }
                                    if (Control == 25)
                                    {
                                        if (MyChar.Level < 70)
                                        {
                                            SendPacket(General.MyPackets.NPCSay("Vous n'avez pas le niveau requis."));
                                            SendPacket(General.MyPackets.NPCLink("Ok.", 255));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                        else
                                        {
                                            MyChar.LearnSkill(8030, 0);
                                            SendPacket(General.MyPackets.NPCSay("F�licitation! Vous avez appris le pouvoir!"));
                                            SendPacket(General.MyPackets.NPCLink("Cool!", 255));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                    }

                                    if (Control == 26)
                                    {
                                        if (MyChar.Level < 100)
                                        {

                                            SendPacket(General.MyPackets.NPCSay("Vous n'avez pas le niveau requis."));
                                            SendPacket(General.MyPackets.NPCLink("Ok.", 255));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                        else
                                        {
                                            MyChar.LearnSkill2(8003, 1);
                                            SendPacket(General.MyPackets.NPCSay("F�licitation! Vous avez appris le pouvoir!"));
                                            SendPacket(General.MyPackets.NPCLink("Cool!", 255));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                    }
                                    if (Control == 27)
                                    {
                                        if (MyChar.Level < 70)
                                        {
                                            SendPacket(General.MyPackets.NPCSay("Vous n'avez pas le niveau requis."));
                                            SendPacket(General.MyPackets.NPCLink("Ok.", 255));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                        else
                                        {
                                            MyChar.LearnSkill(8003, 0);
                                            SendPacket(General.MyPackets.NPCSay("F�licitation! Vous avez appris le pouvoir!"));
                                            SendPacket(General.MyPackets.NPCLink("Cool!", 255));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                    }
                                }
                                else
                                {
                                    SendPacket(General.MyPackets.NPCSay("Vous n'�tes pas un archer..."));
                                    SendPacket(General.MyPackets.NPCLink("Ok.", 255));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 1)
                                {
                                    if (MyChar.Job == 40)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Pour avancer en Archer vous devez �tre niveau 15."));
                                    }
                                    if (MyChar.Job == 41)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Pour avancer en Archer De Vautour vous devez �tre niveau 40."));
                                    }
                                    if (MyChar.Job == 42)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Pour avancer en Archer De Tigre vous devez �tre niveau 70."));
                                    }
                                    if (MyChar.Job == 43)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Pour avancer en Archer De Dragon vous devez �tre niveau 100."));
                                    }
                                    if (MyChar.Job == 44)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Pour avancer en Archer D`arc D`or vous devez �tre niveau 110."));
                                    }
                                    if (MyChar.Job != 45)
                                    {
                                        SendPacket(General.MyPackets.NPCLink("Je veux avancer.", 2));
                                        SendPacket(General.MyPackets.NPCLink("Je sais, je ne suis pas qualifi�.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous ne pouvez plus avancer."));
                                        SendPacket(General.MyPackets.NPCLink("D�sol�.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                                if (Control == 2)
                                {
                                    bool Promoted = false;

                                    if (MyChar.Job == 40 && MyChar.Level >= 15)
                                    {
                                        MyChar.Job = 41;
                                        Promoted = true;
                                    }
                                    else if (MyChar.Job == 41 && MyChar.Level >= 40)
                                    {
                                        MyChar.Job = 42;
                                        Promoted = true;
                                    }
                                    else if (MyChar.Job == 42 && MyChar.Level >= 70)
                                    {
                                        MyChar.Job = 43;
                                        Promoted = true;
                                    }
                                    else if (MyChar.Job == 43 && MyChar.Level >= 100)
                                    {
                                        if (MyChar.RBCount == 1)
                                        {
                                            MyChar.AddItem("133277-0-0-0-11-0", 0, (uint)General.Rand.Next(36457836));
                                        }
                                        else if (MyChar.RBCount == 2)
                                        {
                                            MyChar.AddItem("1088000-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                        }
                                        MyChar.Job = 44;
                                        Promoted = true;
                                    }
                                    else if (MyChar.Job == 44 && MyChar.Level >= 110)
                                    {
                                        if (MyChar.RBCount == 0)
                                        {
                                            MyChar.AddItem("700012-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                        }
                                        else if (MyChar.RBCount == 1)
                                        {
                                            MyChar.AddItem("1088000-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                        }
                                        else if (MyChar.RBCount == 2)
                                        {
                                            MyChar.AddItem("160199-0-0-0-13-0", 0, (uint)General.Rand.Next(36457836));
                                        }
                                        MyChar.Job = 45;
                                        Promoted = true;
                                    }

                                    if (Promoted)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("F�licitation, vous avez avancer."));
                                        SendPacket(General.MyPackets.NPCLink("Cool!.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());

                                        SendPacket(General.MyPackets.Vital(MyChar.UID, 7, MyChar.Job));
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous ne pouvez pas avancer."));
                                        SendPacket(General.MyPackets.NPCLink("Je vois.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                            }

                            if (CurrentNPC == 16)
                            {
                                if (MyChar.Job < 26 && MyChar.Job > 19 || Control == 255)
                                {
                                    if (Control == 150)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Voici les pouvoirs que je peux vous enseigner si vous avez le niveau requis."));
                                        SendPacket(General.MyPackets.NPCLink("Puissance: Lv 3", 154));
                                        SendPacket(General.MyPackets.NPCLink("Exactitude: Lv 3", 155));
                                        SendPacket(General.MyPackets.NPCLink("Bouclier: Lv 15", 156));
                                        SendPacket(General.MyPackets.NPCLink("Hurlement: Lv 15", 157));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    if (Control == 154)
                                    {
                                        if (MyChar.Level < 3)
                                        {

                                            SendPacket(General.MyPackets.NPCSay("Vous n'avez pas le niveau requis."));
                                            SendPacket(General.MyPackets.NPCLink("Ok.", 255));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                        else
                                        {
                                            MyChar.LearnSkill(1025, 0);
                                            SendPacket(General.MyPackets.NPCSay("F�licitation! Vous avez appris le pouvoir!"));
                                            SendPacket(General.MyPackets.NPCLink("Cool!", 255));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                    }
                                    if (Control == 155)
                                    {
                                        if (MyChar.Level < 3)
                                        {

                                            SendPacket(General.MyPackets.NPCSay("Vous n'avez pas le niveau requis."));
                                            SendPacket(General.MyPackets.NPCLink("Ok.", 255));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                        else
                                        {
                                            MyChar.LearnSkill(1015, 0);
                                            SendPacket(General.MyPackets.NPCSay("F�licitation! Vous avez appris le pouvoir!"));
                                            SendPacket(General.MyPackets.NPCLink("Cool!", 255));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                    }
                                    if (Control == 156)
                                    {
                                        if (MyChar.Level < 15)
                                        {

                                            SendPacket(General.MyPackets.NPCSay("Vous n'avez pas le niveau requis."));
                                            SendPacket(General.MyPackets.NPCLink("Ok.", 255));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                        else
                                        {
                                            MyChar.LearnSkill(1020, 0);
                                            SendPacket(General.MyPackets.NPCSay("F�licitation! Vous avez appris le pouvoir!"));
                                            SendPacket(General.MyPackets.NPCLink("Cool!", 255));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                    }
                                    if (Control == 157)
                                    {
                                        if (MyChar.Level < 15)
                                        {

                                            SendPacket(General.MyPackets.NPCSay("Vous n'avez pas le niveau requis."));
                                            SendPacket(General.MyPackets.NPCLink("Ok.", 255));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                        else
                                        {
                                            MyChar.LearnSkill(1040, 0);
                                            SendPacket(General.MyPackets.NPCSay("F�licitation! Vous avez appris le pouvoir!"));
                                            SendPacket(General.MyPackets.NPCLink("Cool!", 255));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                    }
                                }
                                else
                                {
                                    SendPacket(General.MyPackets.NPCSay("Vous n'�tes pas un guerrier..."));
                                    SendPacket(General.MyPackets.NPCLink("Ok.", 255));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 1)
                                {
                                    if (MyChar.Job == 20)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Pour avancer en Guerrier vous devez �tre niveau 15."));
                                    }
                                    if (MyChar.Job == 21)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Pour avancer en Guerrier-Bronze vous devez �tre niveau 40."));
                                    }
                                    if (MyChar.Job == 22)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Pour avancer en Guerrier-Argent vous devez �tre niveau 70."));
                                    }
                                    if (MyChar.Job == 23)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Pour avancer en Guerrier D`or vous devez �tre niveau 100."));
                                    }
                                    if (MyChar.Job == 24)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Pour avancer en Guerre-Dieu vous devez �tre niveau 110."));
                                    }
                                    if (MyChar.Job != 25)
                                    {
                                        SendPacket(General.MyPackets.NPCLink("Je veux avancer.", 2));
                                        SendPacket(General.MyPackets.NPCLink("Je sais que je ne suis pas pr�t.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous ne pouvez plus avancer."));
                                        SendPacket(General.MyPackets.NPCLink("D�sol�.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                                if (Control == 2)
                                {
                                    bool Promoted = false;

                                    if (MyChar.Job == 20 && MyChar.Level >= 15)
                                    {
                                        MyChar.Job = 21;
                                        Promoted = true;
                                    }
                                    else if (MyChar.Job == 21 && MyChar.Level >= 40)
                                    {
                                        MyChar.Job = 22;
                                        Promoted = true;
                                    }
                                    else if (MyChar.Job == 22 && MyChar.Level >= 70)
                                    {
                                        MyChar.Job = 23;
                                        Promoted = true;
                                    }
                                    else if (MyChar.Job == 23 && MyChar.Level >= 100)
                                    {
                                        if (MyChar.RBCount == 1)
                                        {
                                            MyChar.AddItem("131287-0-0-0-11-0", 0, (uint)General.Rand.Next(36457836));
                                        }
                                        else if (MyChar.RBCount == 2)
                                        {
                                            MyChar.AddItem("1088000-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                        }
                                        MyChar.Job = 24;
                                        Promoted = true;
                                    }
                                    else if (MyChar.Job == 24 && MyChar.Level >= 110)
                                    {
                                        if (MyChar.RBCount == 0)
                                        {
                                            MyChar.AddItem("700012-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                        }
                                        else if (MyChar.RBCount == 1)
                                        {
                                            MyChar.AddItem("1088000-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                        }
                                        else if (MyChar.RBCount == 2)
                                        {
                                            MyChar.AddItem("160199-0-0-0-13-0", 0, (uint)General.Rand.Next(36457836));
                                        }
                                        MyChar.Job = 25;
                                        Promoted = true;
                                    }

                                    if (Promoted)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("F�licitation, vous avez avancer."));
                                        SendPacket(General.MyPackets.NPCLink("Cool!.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());

                                        SendPacket(General.MyPackets.Vital(MyChar.UID, 7, MyChar.Job));
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous ne pouvez pas avancer."));
                                        SendPacket(General.MyPackets.NPCLink("Je vois.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                            }

                            if (CurrentNPC == 44)
                            {
                                if (Control == 1)
                                {
                                    SendPacket(General.MyPackets.NPCSay("Si vous me donner 1 Perle de Dragon pour le premier trou ou 5 Perles de Dragon pour le second je vais vous aider."));
                                    SendPacket(General.MyPackets.NPCLink("Ok voici les Perles de Dragon requisent.", 2));
                                    SendPacket(General.MyPackets.NPCLink("J'ai chang� d'id�e.", 255));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 2)
                                {
                                    SendPacket(General.MyPackets.NPCSay("Veuillez �quiper l'arme dans la main droite. Maintenant je vais rajouter un trou � votre arme."));
                                    SendPacket(General.MyPackets.NPCLink("Je suis pr�t.", 3));
                                    SendPacket(General.MyPackets.NPCLink("J'ai chang� d'id�e.", 255));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 3)
                                {
                                    if (MyChar.Equips[4] != null || MyChar.Equips[4] != "0")
                                    {
                                        string[] item = MyChar.Equips[4].Split('-');

                                        if (item[4] == "0" && MyChar.InventoryContains(1088000, 1) || item[5] == "0" && MyChar.InventoryContains(1088000, 5))
                                        {
                                            if (item[4] == "0")
                                            {
                                                item[4] = "255";
                                                MyChar.RemoveItem(MyChar.ItemNext(1088000));
                                                MyChar.GetEquipStats(4, true);
                                                MyChar.Equips[4] = item[0] + "-" + item[1] + "-" + item[2] + "-" + item[3] + "-" + item[4] + "-" + item[5];
                                                MyChar.GetEquipStats(4, false);
                                                SendPacket(General.MyPackets.AddItem((long)MyChar.Equips_UIDs[4], int.Parse(item[0]), byte.Parse(item[1]), byte.Parse(item[2]), byte.Parse(item[3]), byte.Parse(item[4]), byte.Parse(item[5]), 4, 100, 100));
                                            }
                                            else if (item[5] == "0")
                                            {
                                                item[5] = "255";
                                                MyChar.RemoveItem(MyChar.ItemNext(1088000));
                                                MyChar.RemoveItem(MyChar.ItemNext(1088000));
                                                MyChar.RemoveItem(MyChar.ItemNext(1088000));
                                                MyChar.RemoveItem(MyChar.ItemNext(1088000));
                                                MyChar.RemoveItem(MyChar.ItemNext(1088000));
                                                MyChar.GetEquipStats(4, true);
                                                MyChar.Equips[4] = item[0] + "-" + item[1] + "-" + item[2] + "-" + item[3] + "-" + item[4] + "-" + item[5];
                                                MyChar.GetEquipStats(4, false);
                                                SendPacket(General.MyPackets.AddItem((long)MyChar.Equips_UIDs[4], int.Parse(item[0]), byte.Parse(item[1]), byte.Parse(item[2]), byte.Parse(item[3]), byte.Parse(item[4]), byte.Parse(item[5]), 4, 100, 100));
                                            }
                                        }
                                        else
                                        {
                                            SendPacket(General.MyPackets.NPCSay("Vous n'avez pas assez de Perle(s) de Dragon."));
                                            SendPacket(General.MyPackets.NPCLink("Ok.", 255));
                                            SendPacket(General.MyPackets.NPCSetFace(30));
                                            SendPacket(General.MyPackets.NPCFinish());
                                        }
                                    }
                                }
                            }

                            if (CurrentNPC == 266)
                            {
                                if (Control == 1 || Control == 111)
                                {
                                    if (Control == 111)
                                    {
                                        SendPacket(General.MyPackets.Vital(MyChar.UID, 27, MyChar.Hair));
                                    }
                                    SendPacket(General.MyPackets.NPCSay("Quelle coupe voulez vous ?"));
                                    SendPacket(General.MyPackets.NPCLink("nouveau style01", 4));
                                    SendPacket(General.MyPackets.NPCLink("nouveau style02", 5));
                                    SendPacket(General.MyPackets.NPCLink("nouveau style03", 6));
                                    SendPacket(General.MyPackets.NPCLink("nouveau style04", 7));
                                    SendPacket(General.MyPackets.NPCLink("nouveau style05", 8));
                                    SendPacket(General.MyPackets.NPCLink("nouveau style06", 9));
                                    SendPacket(General.MyPackets.NPCLink("nouveau style07", 10));
                                    SendPacket(General.MyPackets.NPCLink("suivant", 11));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 11)
                                {
                                    SendPacket(General.MyPackets.NPCSay("Quelle coupe voulez vous ?"));
                                    SendPacket(General.MyPackets.NPCLink("nouveau style08", 12));
                                    SendPacket(General.MyPackets.NPCLink("nouveau style09", 13));
                                    SendPacket(General.MyPackets.NPCLink("nouveau style10", 14));
                                    SendPacket(General.MyPackets.NPCLink("nouveau style11", 15));
                                    SendPacket(General.MyPackets.NPCLink("nouveau style12", 16));
                                    SendPacket(General.MyPackets.NPCLink("pr�c�dent", 1));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 2 || Control == 112)
                                {
                                    if (Control == 112)
                                    {
                                        SendPacket(General.MyPackets.Vital(MyChar.UID, 27, MyChar.Hair));
                                    }
                                    SendPacket(General.MyPackets.NPCSay("Quelle coupe voulez vous ?"));
                                    SendPacket(General.MyPackets.NPCLink("Nostalgique01", 17));
                                    SendPacket(General.MyPackets.NPCLink("Nostalgique02", 18));
                                    SendPacket(General.MyPackets.NPCLink("Nostalgique03", 19));
                                    SendPacket(General.MyPackets.NPCLink("Nostalgique04", 20));
                                    SendPacket(General.MyPackets.NPCLink("Nostalgique05", 21));
                                    SendPacket(General.MyPackets.NPCLink("Nostalgique06", 22));
                                    SendPacket(General.MyPackets.NPCLink("Nostalgique07", 23));
                                    SendPacket(General.MyPackets.NPCLink("J'ai chang� d'avis.", 255));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 3 || Control == 113)
                                {
                                    if (Control == 113)
                                    {
                                        SendPacket(General.MyPackets.Vital(MyChar.UID, 27, MyChar.Hair));
                                    }
                                    SendPacket(General.MyPackets.NPCSay("Quelle coupe voulez vous ?"));
                                    SendPacket(General.MyPackets.NPCLink("Sp�cial01", 25));
                                    SendPacket(General.MyPackets.NPCLink("Sp�cial02", 26));
                                    SendPacket(General.MyPackets.NPCLink("Sp�cial03", 27));
                                    SendPacket(General.MyPackets.NPCLink("Sp�cial04", 28));
                                    SendPacket(General.MyPackets.NPCLink("Sp�cial05", 29));
                                    SendPacket(General.MyPackets.NPCLink("J'ai chang� d'avis.", 255));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 254)
                                {
                                    if (MyChar.Silvers >= 500)
                                    {
                                        MyChar.Silvers -= 500;
                                        MyChar.Hair = MyChar.ShowHair;

                                        SendPacket(General.MyPackets.Vital(MyChar.UID, 4, MyChar.Silvers));
                                    }
                                }
                                if (Control > 3 && Control < 30 && Control != 11)
                                {
                                    if (MyChar.Silvers >= 500)
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Voila, �tes-vous satisfait?"));
                                        SendPacket(General.MyPackets.NPCLink("Cool!", 254));
                                        if (Control < 17)
                                            SendPacket(General.MyPackets.NPCLink("Je veux la changer.", 111));
                                        else if (Control < 25)
                                            SendPacket(General.MyPackets.NPCLink("Je veux la changer.", 112));
                                        else if (Control < 30)
                                            SendPacket(General.MyPackets.NPCLink("Je veux la changer.", 113));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());

                                        if (Control == 4)
                                            MyChar.ShowHair = ushort.Parse(Convert.ToString(MyChar.Hair)[0] + "30");
                                        if (Control == 5)
                                            MyChar.ShowHair = ushort.Parse(Convert.ToString(MyChar.Hair)[0] + "31");
                                        if (Control == 6)
                                            MyChar.ShowHair = ushort.Parse(Convert.ToString(MyChar.Hair)[0] + "32");
                                        if (Control == 7)
                                            MyChar.ShowHair = ushort.Parse(Convert.ToString(MyChar.Hair)[0] + "33");
                                        if (Control == 8)
                                            MyChar.ShowHair = ushort.Parse(Convert.ToString(MyChar.Hair)[0] + "34");
                                        if (Control == 9)
                                            MyChar.ShowHair = ushort.Parse(Convert.ToString(MyChar.Hair)[0] + "35");
                                        if (Control == 10)
                                            MyChar.ShowHair = ushort.Parse(Convert.ToString(MyChar.Hair)[0] + "36");
                                        if (Control == 12)
                                            MyChar.ShowHair = ushort.Parse(Convert.ToString(MyChar.Hair)[0] + "37");
                                        if (Control == 13)
                                            MyChar.ShowHair = ushort.Parse(Convert.ToString(MyChar.Hair)[0] + "38");
                                        if (Control == 14)
                                            MyChar.ShowHair = ushort.Parse(Convert.ToString(MyChar.Hair)[0] + "39");
                                        if (Control == 15)
                                            MyChar.ShowHair = ushort.Parse(Convert.ToString(MyChar.Hair)[0] + "40");
                                        if (Control == 16)
                                            MyChar.ShowHair = ushort.Parse(Convert.ToString(MyChar.Hair)[0] + "41");

                                        if (Control == 17)
                                            MyChar.ShowHair = ushort.Parse(Convert.ToString(MyChar.Hair)[0] + "11");
                                        if (Control == 18)
                                            MyChar.ShowHair = ushort.Parse(Convert.ToString(MyChar.Hair)[0] + "12");
                                        if (Control == 19)
                                            MyChar.ShowHair = ushort.Parse(Convert.ToString(MyChar.Hair)[0] + "13");
                                        if (Control == 20)
                                            MyChar.ShowHair = ushort.Parse(Convert.ToString(MyChar.Hair)[0] + "14");
                                        if (Control == 21)
                                            MyChar.ShowHair = ushort.Parse(Convert.ToString(MyChar.Hair)[0] + "15");
                                        if (Control == 22)
                                            MyChar.ShowHair = ushort.Parse(Convert.ToString(MyChar.Hair)[0] + "16");
                                        if (Control == 23)
                                            MyChar.ShowHair = ushort.Parse(Convert.ToString(MyChar.Hair)[0] + "17");
                                        if (Control == 24)
                                            MyChar.ShowHair = ushort.Parse(Convert.ToString(MyChar.Hair)[0] + "10");

                                        if (Control == 25)
                                            MyChar.ShowHair = ushort.Parse(Convert.ToString(MyChar.Hair)[0] + "21");
                                        if (Control == 26)
                                            MyChar.ShowHair = ushort.Parse(Convert.ToString(MyChar.Hair)[0] + "22");
                                        if (Control == 27)
                                            MyChar.ShowHair = ushort.Parse(Convert.ToString(MyChar.Hair)[0] + "23");
                                        if (Control == 28)
                                            MyChar.ShowHair = ushort.Parse(Convert.ToString(MyChar.Hair)[0] + "24");
                                        if (Control == 29)
                                            MyChar.ShowHair = ushort.Parse(Convert.ToString(MyChar.Hair)[0] + "25");

                                        SendPacket(General.MyPackets.Vital(MyChar.UID, 27, MyChar.ShowHair));
                                        World.UpdateSpawn(MyChar);
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous n'avez pas assez d'argent."));
                                        SendPacket(General.MyPackets.NPCLink("Je suis d�sol�.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                            }

                            if (CurrentNPC == 211 || CurrentNPC == 180)
                            {
                                if (Control == 1)
                                {
                                    ushort XTo = 429;
                                    ushort YTo = 378;

                                    if (MyChar.PrevMap == 1015)
                                    {
                                        XTo = 717;
                                        YTo = 571;
                                    }
                                    if (MyChar.PrevMap == 1000)
                                    {
                                        XTo = 500;
                                        YTo = 650;
                                    }
                                    if (MyChar.PrevMap == 1011)
                                    {
                                        XTo = 188;
                                        YTo = 264;
                                    }
                                    if (MyChar.PrevMap == 1020)
                                    {
                                        XTo = 565;
                                        YTo = 562;
                                    }

                                    MyChar.Teleport(MyChar.PrevMap, XTo, YTo);
                                }
                            }

                            if (CurrentNPC == 21 || CurrentNPC == 181 || CurrentNPC == 182 || CurrentNPC == 183 || CurrentNPC == 184)
                            {
                                if (Control == 1)
                                {
                                    MyChar.Teleport(1039, 217, 215);
                                    SendPacket(General.MyPackets.NPCSay("Vous devez attaquer un poteau/�ventail de votre niveau ou moins. Si vous voulez attaquer un �ventail/poteau d�passant votre niveau, rien ne se passera."));
                                    SendPacket(General.MyPackets.NPCLink("Je vois.", 255));
                                    SendPacket(General.MyPackets.NPCSetFace(30));
                                    SendPacket(General.MyPackets.NPCFinish());
                                }
                                if (Control == 2)
                                {
                                    if (MyChar.Blessed == true)
                                    {
                                        MyChar.Teleport(601, 64, 55);
                                        MyChar.OfflineTG = true;
                                        MyChar.MyClient.Drop();

                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("Vous n'�tes pas niveau 50 et vous n'�tes pas en b�n�diction..."));
                                        SendPacket(General.MyPackets.NPCLink("Envoyez moi juste au TG normal...", 1));
                                        SendPacket(General.MyPackets.NPCLink("Je m'en vais...", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                            }

                            if (CurrentNPC == 103)
                            {
                                if (Control == 1)
                                {
                                    if (MyChar.Silvers >= 100)
                                    {
                                        MyChar.Teleport(1002, 958, 555);
                                        MyChar.Silvers -= 100;
                                        SendPacket(General.MyPackets.Vital(MyChar.UID, 4, MyChar.Silvers));
                                        SendPacket(General.MyPackets.Vital(MyChar.UID, 30, MyChar.CPs));
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("D�sol�, vous n'avez pas 100 argents."));
                                        SendPacket(General.MyPackets.NPCLink("Je vois.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                                if (Control == 2)
                                {
                                    if (MyChar.Silvers >= 100)
                                    {
                                        MyChar.Teleport(1002, 69, 473);
                                        MyChar.Silvers -= 100;
                                        SendPacket(General.MyPackets.Vital(MyChar.UID, 4, MyChar.Silvers));
                                        SendPacket(General.MyPackets.Vital(MyChar.UID, 30, MyChar.CPs));
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("D�sol�, vous n'avez pas 100 argents."));
                                        SendPacket(General.MyPackets.NPCLink("Je vois.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                                if (Control == 3)
                                {
                                    if (MyChar.Silvers >= 100)
                                    {
                                        MyChar.Teleport(1002, 555, 957);
                                        MyChar.Silvers -= 100;
                                        SendPacket(General.MyPackets.Vital(MyChar.UID, 4, MyChar.Silvers));
                                        SendPacket(General.MyPackets.Vital(MyChar.UID, 30, MyChar.CPs));
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("D�sol�, vous n'avez pas 100 argents."));
                                        SendPacket(General.MyPackets.NPCLink("Je vois.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                                if (Control == 4)
                                {
                                    if (MyChar.Silvers >= 100)
                                    {
                                        MyChar.Teleport(1002, 232, 190);
                                        MyChar.Silvers -= 100;
                                        SendPacket(General.MyPackets.Vital(MyChar.UID, 4, MyChar.Silvers));
                                        SendPacket(General.MyPackets.Vital(MyChar.UID, 30, MyChar.CPs));
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("D�sol�, vous n'avez pas 100 argents."));
                                        SendPacket(General.MyPackets.NPCLink("Je vois.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                                if (Control == 5)
                                {
                                    if (MyChar.Silvers >= 100)
                                    {
                                        MyChar.Teleport(1002, 53, 399);
                                        MyChar.Silvers -= 100;
                                        SendPacket(General.MyPackets.Vital(MyChar.UID, 4, MyChar.Silvers));
                                        SendPacket(General.MyPackets.Vital(MyChar.UID, 30, MyChar.CPs));
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("D�sol�, vous n'avez pas 100 argents."));
                                        SendPacket(General.MyPackets.NPCLink("Je vois.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                                if (Control == 6)
                                {
                                    if (MyChar.Silvers >= 100)
                                    {
                                        MyChar.Teleport(1036, 211, 196);
                                        MyChar.Silvers -= 100;
                                        SendPacket(General.MyPackets.Vital(MyChar.UID, 4, MyChar.Silvers));
                                        SendPacket(General.MyPackets.Vital(MyChar.UID, 30, MyChar.CPs));
                                    }
                                    else
                                    {
                                        SendPacket(General.MyPackets.NPCSay("D�sol�, vous n'avez pas 100 argents."));
                                        SendPacket(General.MyPackets.NPCLink("Je vois.", 255));
                                        SendPacket(General.MyPackets.NPCSetFace(30));
                                        SendPacket(General.MyPackets.NPCFinish());
                                    }
                                }
                            }
                            MyChar.Ready = true;
                            break;
                        }
                    case 1005:
                        {
                            sbyte AddX = 0;
                            sbyte AddY = 0;
                            byte Dir = (byte)(Data[8] % 8);
                            MyChar.Direction = Dir;

                            switch (Dir)
                            {
                                case 0:
                                    {
                                        AddY = 1;
                                        break;
                                    }
                                case 1:
                                    {
                                        AddX = -1;
                                        AddY = 1;
                                        break;
                                    }
                                case 2:
                                    {
                                        AddX = -1;
                                        break;
                                    }
                                case 3:
                                    {
                                        AddX = -1;
                                        AddY = -1;
                                        break;
                                    }
                                case 4:
                                    {
                                        AddY = -1;
                                        break;
                                    }
                                case 5:
                                    {
                                        AddX = 1;
                                        AddY = -1;
                                        break;
                                    }
                                case 6:
                                    {
                                        AddX = 1;
                                        break;
                                    }
                                case 7:
                                    {
                                        AddY = 1;
                                        AddX = 1;
                                        break;
                                    }
                            }
                            World.PlayerMoves(MyChar, Data);

                            MyChar.PrevX = MyChar.LocX;
                            MyChar.PrevY = MyChar.LocY;
                            MyChar.LocX = (ushort)(MyChar.LocX + AddX);
                            MyChar.LocY = (ushort)(MyChar.LocY + AddY);

                            MyChar.TargetUID = 0;
                            MyChar.MobTarget = null;
                            MyChar.PTarget = null;
                            MyChar.TGTarget = null;
                            MyChar.AtkType = 0;
                            MyChar.SkillLooping = 0;                            
                            MyChar.Action = 100;
                            World.SpawnMeToOthers(MyChar, true);
                            World.SpawnOthersToMe(MyChar, true);                            
                            World.SurroundNPCs(MyChar, true);
                            World.SurroundMobs(MyChar, true);
                            World.SurroundDroppedItems(MyChar, true);
                            MyChar.Attacking = false;
                            if (MyChar.Mining)
                                MyChar.Mining = false;

                            break;
                        }
                    case 1004:
                        {
                            short ChatType = (short)(Data[8] | (Data[9] << 8));
                            int Pos = 26;
                            int Length = 0;
                            string From = "";
                            string To = "";
                            string Message = "";
                            for (int Count = 0; Count < Data[25]; Count += 1)
                            {
                                From += Convert.ToChar(Data[Pos]);
                                Pos += 1;
                            }

                            Length = Data[Pos];
                            Pos += 1;

                            for (int Count = 0; Count < Length; Count += 1)
                            {
                                To += Convert.ToChar(Data[Pos]);
                                Pos += 1;
                            }

                            Pos += 1;
                            Length = Data[Pos];
                            Pos += 1;

                            for (int Count = 0; Count < Length; Count += 1)
                            {
                                if (Pos <= Data.Length)
                                    Message += Convert.ToChar(Data[Pos]);
                                Pos += 1;
                            }                          

                            try
                            {
                                if (Message[0] == '/' || Message[0] == '!' || Message[0] == '#')
                                {
                                    string[] Splitter = Message.Split(' ');

                                    if (Status != null)
                                    {
                                        if (Splitter[0] == "/exp")
                                        {
                                            double CurXP = (double)MyChar.Exp;
                                            double MaxXP = (double)DataBase.NeededXP(MyChar.Level);
                                            double StatXP = ((CurXP * 100) / MaxXP);
                                            SendPacket(General.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "Vous �tes � " + StatXP + "% de votre prochain niveau!", 2011));
                                        }
                                    }

                                    if (Status == 0)
                                    {
                                        if (Splitter[0] == "/save")
                                        {
                                            MyChar.Save();
                                        }
                                        if (Splitter[0] == "/servertime")
                                        {
                                            SendPacket(General.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "Server Time: " + DateTime.Now, 2011));
                                        }

                                        if (Splitter[0] == "/coords")
                                        {
                                            SendPacket(General.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "Coords: " + MyChar.LocX + ", " + MyChar.LocY, 2000));
                                        }

                                        if (Splitter[0] == "/quest")
                                        {
                                            SendPacket(General.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "Vous devez tuer 300 " + MyChar.QuestMob + " et vous en avez tuer " + MyChar.QuestKO, 2000));
                                        }

                                        if (Splitter[0] == "/vp")
                                        {
                                            SendPacket(General.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "Vous avez " + MyChar.VP + " points de vertu.", 2000));
                                        }

                                        if (Splitter[0] == "/KO")
                                        {
                                            SendPacket(General.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "Vous avez tu� " + MyChar.OldKO, 2000));
                                        }
                                        if (Splitter[0] == "/str")
                                        {
                                            if (ushort.Parse(Splitter[1]) <= MyChar.StatP)
                                            {
                                                ushort StrAdd = ushort.Parse(Splitter[1]);
                                                MyChar.Str = (ushort)(MyChar.Str + StrAdd);
                                                MyChar.StatP -= StrAdd;
                                                MyChar.Save();
                                                SendPacket(General.MyPackets.Vital((long)MyChar.UID, 16, MyChar.Str));
                                                MyChar.MaxHP = MyChar.BaseMaxHP();
                                                MyChar.CurHP = MyChar.MaxHP;
                                                SendPacket(General.MyPackets.Vital((long)MyChar.UID, 0, MyChar.CurHP));
                                                SendPacket(General.MyPackets.Vital((long)MyChar.UID, 2, MyChar.MaxMana()));
                                                SendPacket(General.MyPackets.GeneralData((long)MyChar.UID, 0, 0, 0, 92));
                                                SendPacket(General.MyPackets.Vital(MyChar.UID, 11, MyChar.StatP));
                                            }
                                        }
                                        if (Splitter[0] == "/vit")
                                        {
                                            if (ushort.Parse(Splitter[1]) <= MyChar.StatP)
                                            {
                                                ushort VitAdd = ushort.Parse(Splitter[1]);
                                                MyChar.Vit = (ushort)(MyChar.Vit + VitAdd);
                                                MyChar.StatP -= VitAdd;
                                                MyChar.Save();
                                                SendPacket(General.MyPackets.Vital((long)MyChar.UID, 15, MyChar.Vit));
                                                MyChar.MaxHP = MyChar.BaseMaxHP();
                                                MyChar.CurHP = MyChar.MaxHP;
                                                SendPacket(General.MyPackets.Vital((long)MyChar.UID, 0, MyChar.CurHP));
                                                SendPacket(General.MyPackets.Vital((long)MyChar.UID, 2, MyChar.MaxMana()));
                                                SendPacket(General.MyPackets.GeneralData((long)MyChar.UID, 0, 0, 0, 92));
                                                SendPacket(General.MyPackets.Vital(MyChar.UID, 11, MyChar.StatP));
                                            }
                                        }
                                        if (Splitter[0] == "/agi")
                                        {
                                            if (ushort.Parse(Splitter[1]) <= MyChar.StatP)
                                            {
                                                ushort AgiAdd = ushort.Parse(Splitter[1]);
                                                MyChar.Agi = (ushort)(MyChar.Agi + AgiAdd);
                                                MyChar.StatP -= AgiAdd;
                                                MyChar.Save();
                                                SendPacket(General.MyPackets.Vital((long)MyChar.UID, 17, MyChar.Agi));
                                                MyChar.MaxHP = MyChar.BaseMaxHP();
                                                MyChar.CurHP = MyChar.MaxHP;
                                                SendPacket(General.MyPackets.Vital((long)MyChar.UID, 0, MyChar.CurHP));
                                                SendPacket(General.MyPackets.Vital((long)MyChar.UID, 2, MyChar.MaxMana()));
                                                SendPacket(General.MyPackets.GeneralData((long)MyChar.UID, 0, 0, 0, 92));
                                                SendPacket(General.MyPackets.Vital(MyChar.UID, 11, MyChar.StatP));
                                            }
                                        }
                                        if (Splitter[0] == "/spi")
                                        {
                                            if (ushort.Parse(Splitter[1]) <= MyChar.StatP)
                                            {
                                                ushort SpiAdd = ushort.Parse(Splitter[1]);
                                                MyChar.Spi = (ushort)(MyChar.Spi + SpiAdd);
                                                MyChar.StatP -= SpiAdd;
                                                MyChar.Save();
                                                SendPacket(General.MyPackets.Vital((long)MyChar.UID, 14, MyChar.Spi));
                                                MyChar.MaxHP = MyChar.BaseMaxHP();
                                                MyChar.CurHP = MyChar.MaxHP;
                                                SendPacket(General.MyPackets.Vital((long)MyChar.UID, 0, MyChar.CurHP));
                                                SendPacket(General.MyPackets.Vital((long)MyChar.UID, 2, MyChar.MaxMana()));
                                                SendPacket(General.MyPackets.GeneralData((long)MyChar.UID, 0, 0, 0, 92));
                                                SendPacket(General.MyPackets.Vital(MyChar.UID, 11, MyChar.StatP));
                                            }
                                        }
                                    }
                                    if (Status == 7)
                                    {
                                        if (Splitter[0] == "/save")
                                        {
                                            MyChar.Save();
                                        }
                                        if (Splitter[0] == "/servertime")
                                        {
                                            SendPacket(General.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "Server Time: " + DateTime.Now, 2011));
                                        }

                                        if (Splitter[0] == "/coords")
                                        {
                                            SendPacket(General.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "Coords: " + MyChar.LocX + ", " + MyChar.LocY, 2000));
                                        }
                                        if (Splitter[0] == "/dc")
                                        {
                                            MyChar.Save();
                                            Drop();
                                        }
                                        if (Splitter[0] == "/clearinv")
                                        {
                                            foreach (uint uid in MyChar.Inventory_UIDs)
                                            {
                                                if (uid != 0)
                                                    SendPacket(General.MyPackets.RemoveItem(uid, 0, 3));
                                            }
                                            MyChar.Inventory_UIDs = new uint[41];
                                            MyChar.Inventory = new string[41];
                                            MyChar.ItemsInInventory = 0;
                                        }
                                        if (Splitter[0] == "/rez")
                                        {
                                            foreach (DictionaryEntry DE in World.AllChars)
                                            {
                                                Character Char = (Character)DE.Value;
                                                if (Splitter[1] == Char.Name)
                                                {
                                                    Char.CurHP = Char.MaxHP;
                                                    Char.Alive = true;
                                                    Char.MyClient.SendPacket(General.MyPackets.Status1(Char.UID, 0));
                                                    Char.MyClient.SendPacket(General.MyPackets.Status3(Char.UID));
                                                    Char.MyClient.SendPacket(General.MyPackets.Vital(Char.UID, 26, Char.GetStat()));
                                                    Char.MyClient.SendPacket(General.MyPackets.CharacterInfo(Char));
                                                    Char.SendEquips(false);
                                                    Char.BlueName = false;
                                                    Char.MyClient.SendPacket(General.MyPackets.Vital(Char.UID, 26, Char.GetStat()));
                                                    Char.Stamina = 100;
                                                    Char.MyClient.SendPacket(General.MyPackets.Vital(Char.UID, 9, Char.Stamina));
                                                    World.UpdateSpawn(Char);
                                                }
                                            }
                                        }

                                        if (Splitter[0] == "/recall")
                                        {
                                            foreach (DictionaryEntry DE in World.AllChars)
                                            {
                                                Character Char = (Character)DE.Value;
                                                if (Char.Name == Splitter[1])
                                                {
                                                    Char.Teleport(MyChar.LocMap, MyChar.LocX, MyChar.LocY);
                                                    break;
                                                }
                                            }
                                        }
                                        if (Splitter[0] == "/goto")
                                        {
                                            foreach (DictionaryEntry DE in World.AllChars)
                                            {
                                                Character Char = (Character)DE.Value;
                                                if (Char.Name == Splitter[1])
                                                {
                                                    MyChar.Teleport(Char.LocMap, Char.LocX, Char.LocY);
                                                    break;
                                                }
                                            }
                                        }
                                        if (Splitter[0] == "/restart")
                                        {
                                            World.SaveAllChars();
                                            General.ServerRestart();
                                        }
                                        if (Splitter[0] == "/updates")
                                        {
                                            World.SaveAllChars();
                                            General.ServerUpdates();
                                        }
                                        if (Splitter[0] == "/str")
                                        {
                                            if (ushort.Parse(Splitter[1]) <= MyChar.StatP)
                                            {
                                                ushort StrAdd = ushort.Parse(Splitter[1]);
                                                MyChar.Str = (ushort)(MyChar.Str + StrAdd);
                                                MyChar.StatP -= StrAdd;
                                                MyChar.Save();
                                                SendPacket(General.MyPackets.Vital((long)MyChar.UID, 16, MyChar.Str));
                                                MyChar.MaxHP = MyChar.BaseMaxHP();
                                                MyChar.CurHP = MyChar.MaxHP;
                                                SendPacket(General.MyPackets.Vital((long)MyChar.UID, 0, MyChar.CurHP));
                                                SendPacket(General.MyPackets.Vital((long)MyChar.UID, 2, MyChar.MaxMana()));
                                                SendPacket(General.MyPackets.GeneralData((long)MyChar.UID, 0, 0, 0, 92));
                                                SendPacket(General.MyPackets.Vital(MyChar.UID, 11, MyChar.StatP));
                                            }
                                        }
                                        if (Splitter[0] == "/vit")
                                        {
                                            if (ushort.Parse(Splitter[1]) <= MyChar.StatP)
                                            {
                                                ushort VitAdd = ushort.Parse(Splitter[1]);
                                                MyChar.Vit = (ushort)(MyChar.Vit + VitAdd);
                                                MyChar.StatP -= VitAdd;
                                                MyChar.Save();
                                                SendPacket(General.MyPackets.Vital((long)MyChar.UID, 15, MyChar.Vit));
                                                MyChar.MaxHP = MyChar.BaseMaxHP();
                                                MyChar.CurHP = MyChar.MaxHP;
                                                SendPacket(General.MyPackets.Vital((long)MyChar.UID, 0, MyChar.CurHP));
                                                SendPacket(General.MyPackets.Vital((long)MyChar.UID, 2, MyChar.MaxMana()));
                                                SendPacket(General.MyPackets.GeneralData((long)MyChar.UID, 0, 0, 0, 92));
                                                SendPacket(General.MyPackets.Vital(MyChar.UID, 11, MyChar.StatP));
                                            }
                                        }
                                        if (Splitter[0] == "/agi")
                                        {
                                            if (ushort.Parse(Splitter[1]) <= MyChar.StatP)
                                            {
                                                ushort AgiAdd = ushort.Parse(Splitter[1]);
                                                MyChar.Agi = (ushort)(MyChar.Agi + AgiAdd);
                                                MyChar.StatP -= AgiAdd;
                                                MyChar.Save();
                                                SendPacket(General.MyPackets.Vital((long)MyChar.UID, 17, MyChar.Agi));
                                                MyChar.MaxHP = MyChar.BaseMaxHP();
                                                MyChar.CurHP = MyChar.MaxHP;
                                                SendPacket(General.MyPackets.Vital((long)MyChar.UID, 0, MyChar.CurHP));
                                                SendPacket(General.MyPackets.Vital((long)MyChar.UID, 2, MyChar.MaxMana()));
                                                SendPacket(General.MyPackets.GeneralData((long)MyChar.UID, 0, 0, 0, 92));
                                                SendPacket(General.MyPackets.Vital(MyChar.UID, 11, MyChar.StatP));
                                            }
                                        }
                                        if (Splitter[0] == "/spi")
                                        {
                                            if (ushort.Parse(Splitter[1]) <= MyChar.StatP)
                                            {
                                                ushort SpiAdd = ushort.Parse(Splitter[1]);
                                                MyChar.Spi = (ushort)(MyChar.Spi + SpiAdd);
                                                MyChar.StatP -= SpiAdd;
                                                MyChar.Save();
                                                SendPacket(General.MyPackets.Vital((long)MyChar.UID, 14, MyChar.Spi));
                                                MyChar.MaxHP = MyChar.BaseMaxHP();
                                                MyChar.CurHP = MyChar.MaxHP;
                                                SendPacket(General.MyPackets.Vital((long)MyChar.UID, 0, MyChar.CurHP));
                                                SendPacket(General.MyPackets.Vital((long)MyChar.UID, 2, MyChar.MaxMana()));
                                                SendPacket(General.MyPackets.GeneralData((long)MyChar.UID, 0, 0, 0, 92));
                                                SendPacket(General.MyPackets.Vital(MyChar.UID, 11, MyChar.StatP));
                                            }
                                        }
                                        if (Splitter[0] == "/hair")
                                        {
                                            ushort mid = ushort.Parse(Splitter[1]);
                                            MyChar.Hair = mid;

                                            SendPacket(General.MyPackets.Vital(MyChar.UID, 27, ulong.Parse(MyChar.Hair.ToString())));
                                            World.UpdateSpawn(MyChar);
                                        }
                                        if (Splitter[0] == "/quest")
                                        {
                                            SendPacket(General.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "Vous devez tuer 300 " + MyChar.QuestMob + " et vous en avez tuer " + MyChar.QuestKO, 2000));
                                        }
                                        if (Splitter[0] == "/vp")
                                        {
                                            SendPacket(General.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "Vous avez " + MyChar.VP + " points de vertu.", 2000));
                                        }
                                        if (Splitter[0] == "/KO")
                                        {
                                            SendPacket(General.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "Vous avez tu� " + MyChar.OldKO, 2000));
                                        }
                                        if (Splitter[0] == "/msg")
                                        {
                                            Message = Message.Remove(0, 4);
                                            World.SendMsgToAll(Message, MyChar.Name, 2011);
                                        }
                                        if (Splitter[0] == "/service")
                                        {
                                            Message = Message.Remove(0, 8);
                                            World.SendMsgToAll(Message, "[Service]", 2014);
                                        }
                                        if (Splitter[0] == "/gm")
                                        {
                                            Message = Message.Remove(0, 3);
                                            World.SendMsgToAll(Message, "SYSTEM", 2011);
                                        }
                                        if (Splitter[0] == "/xp")
                                        {
                                            MyChar.XpList = true;
                                            SendPacket(General.MyPackets.Vital(MyChar.UID, 26, MyChar.GetStat()));
                                        }
                                        if (Splitter[0] == "/skill")
                                        {
                                            MyChar.LearnSkill2(short.Parse(Splitter[1]), byte.Parse(Splitter[2]));
                                        }
                                        if (Splitter[0] == "/model")
                                        {
                                            if (Splitter[1] == "smale")
                                                MyChar.Model = 1003;
                                            if (Splitter[1] == "lmale")
                                                MyChar.Model = 1004;
                                            if (Splitter[1] == "sfemale")
                                                MyChar.Model = 2001;
                                            if (Splitter[1] == "lfemale")
                                                MyChar.Model = 2002;
                                            if (Splitter[1] == "guard")
                                                MyChar.Model = 900;
                                            if (Splitter[1] == "guard2")
                                                MyChar.Model = 910;
                                            if (Splitter[1] == "guard3")
                                                MyChar.Model = 920;
                                            if (Splitter[1] == "nd")
                                                MyChar.Model = 377;
                                            if (Splitter[1] == "satan")
                                                MyChar.Model = 166;
                                            if (Splitter[1] == "vampire")
                                                MyChar.Model = 111;
                                            if (Splitter[1] == "bunny")
                                                MyChar.Model = 222;
                                            if (Splitter[1] == "bunny2")
                                                MyChar.Model = 224;
                                            if (Splitter[1] == "bunny3")
                                                MyChar.Model = 225;
                                            if (Splitter[1] == "fairy")
                                                MyChar.Model = 130;
                                            if (Splitter[1] == "pig")
                                                MyChar.Model = 215;
                                            if (Splitter[1] == "titan")
                                                MyChar.Model = 153;
                                            if (Splitter[1] == "pluto")
                                                MyChar.Model = 168;
                                            if (Splitter[1] == "revenant")
                                                MyChar.Model = 265;
                                            if (Splitter[1] == "eidolon")
                                                MyChar.Model = 266;
                                            if (Splitter[1] == "troll")
                                                MyChar.Model = 353;
                                            if (Splitter[1] == "soldier")
                                                MyChar.Model = 263;
                                            if (Splitter[1] == "phantom")
                                                MyChar.Model = 363;
                                            if (Splitter[1] == "spearman")
                                                MyChar.Model = 165;
                                            if (Splitter[1] == "titan")
                                                MyChar.Model = 153;
                                            if (Splitter[1] == "gano")
                                                MyChar.Model = 133;
                                            if (Splitter[1] == "phe")
                                                MyChar.Model = 104;
                                            if (Splitter[1] == "gm")
                                                MyChar.Model = 2223;

                                            SendPacket(General.MyPackets.Vital(MyChar.UID, 12, ulong.Parse(MyChar.Avatar.ToString() + MyChar.Model.ToString())));
                                            World.UpdateSpawn(MyChar);
                                        }
                                        if (Splitter[0] == "/model")
                                        {
                                            uint mid = uint.Parse(Splitter[1]);
                                            MyChar.Model = mid;

                                            SendPacket(General.MyPackets.Vital(MyChar.UID, 12, ulong.Parse(MyChar.Avatar.ToString() + MyChar.Model.ToString())));
                                            World.UpdateSpawn(MyChar);
                                        }
                                        if (Splitter[0] == "/scrollall")
                                        {
                                            foreach (DictionaryEntry DE in World.AllChars)
                                            {
                                                Character Char = (Character)DE.Value;
                                                Char.Teleport(MyChar.LocMap, MyChar.LocX, MyChar.LocY);
                                            }
                                        }
                                        if (Splitter[0] == "/vps")
                                        {
                                            uint NewVP = uint.Parse(Splitter[1]);

                                            MyChar.VP = NewVP;
                                        }
                                        if (Splitter[0] == "/mana")
                                        {
                                            MyChar.CurMP = MyChar.MaxMP;
                                            SendPacket(General.MyPackets.Vital(MyChar.UID, 2, MyChar.CurMP));
                                        }

                                        if (Splitter[0] == "/hp")
                                        {
                                            MyChar.CurHP = MyChar.MaxHP;
                                            SendPacket(General.MyPackets.Vital(MyChar.UID, 0, MyChar.CurHP));
                                        }
                                        if (Splitter[0] == "/info")
                                        {
                                            string BackMsg = "";
                                            foreach (DictionaryEntry DE in World.AllChars)
                                            {
                                                Character Char = (Character)DE.Value;
                                                BackMsg += Char.Name + ", ";
                                            }
                                            if (BackMsg.Length > 1)
                                                BackMsg = BackMsg.Remove(BackMsg.Length - 2, 2);
                                            SendPacket(General.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "Joueurs en ligne: " + World.AllChars.Count, 2000));
                                            SendPacket(General.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, BackMsg, 2000));
                                        }

                                        if (Splitter[0] == "/mm")
                                        {
                                            if (MyChar.LocMap != 1505)
                                            {
                                                if (Splitter[1] != "1038")
                                                {
                                                    MyChar.Teleport(ushort.Parse(Splitter[1]), ushort.Parse(Splitter[2]), ushort.Parse(Splitter[3]));
                                                }
                                            }
                                        }
                                        if (Splitter[0] == "/scroll")
                                        {
                                            if (MyChar.LocMap != 1505)
                                            {
                                                if (Splitter[1] == "tc")
                                                {
                                                    MyChar.Teleport(1002, 431, 379);
                                                }
                                                if (Splitter[1] == "faction")
                                                {
                                                    MyChar.Teleport(1037, 150, 150);
                                                }
                                                if (Splitter[1] == "tg")
                                                {
                                                    MyChar.Teleport(1039, 200, 200);
                                                }
                                                if (Splitter[1] == "jail")
                                                {
                                                    MyChar.Teleport(6000, 30, 75);
                                                }
                                                if (Splitter[1] == "am")
                                                {
                                                    MyChar.Teleport(1020, 567, 576);
                                                }
                                                if (Splitter[1] == "dc")
                                                {
                                                    MyChar.Teleport(1000, 500, 650);
                                                }
                                                if (Splitter[1] == "mc")
                                                {
                                                    MyChar.Teleport(1001, 316, 642);
                                                }
                                                if (Splitter[1] == "bi")
                                                {
                                                    MyChar.Teleport(1015, 723, 573);
                                                }
                                                if (Splitter[1] == "pc")
                                                {
                                                    MyChar.Teleport(1011, 190, 271);
                                                }
                                                if (Splitter[1] == "ma")
                                                {
                                                    MyChar.Teleport(1036, 200, 200);
                                                }
                                                if (Splitter[1] == "arena")
                                                {
                                                    MyChar.Teleport(1005, 52, 69);
                                                }
                                                if (Splitter[1] == "lotto")
                                                {
                                                    MyChar.Teleport(700, 52, 69);
                                                }
                                                if (Splitter[1] == "birth")
                                                {
                                                    MyChar.Teleport(1010, 60, 110);
                                                }
                                            }
                                        }
                                        if (Splitter[0] == "/clearpkp")
                                        {
                                            MyChar.PKPoints = 0;
                                            World.UpdateSpawn(MyChar);
                                            SendPacket(General.MyPackets.CharacterInfo(MyChar));
                                            MyChar.SendEquips(false);
                                            SendPacket(General.MyPackets.Vital(MyChar.UID, 26, MyChar.GetStat()));
                                        }
                                        if (Splitter[0] == "/jail")
                                        {
                                            foreach (DictionaryEntry DE in World.AllChars)
                                            {
                                                Character Char = (Character)DE.Value;

                                                if (Char.Name == Splitter[1])
                                                {
                                                    Char.BanNB += 1;
                                                    DataBase.SaveBan(Char);
                                                    World.SendMsgToAll(Char.Name + " a �t� envoyez en prison. Il a �t� banni " + Char.BanNB + " fois. Attention si vous �tes banni trop souvent vous resterez en prison!", "SYSTEM", 2011);
                                                    Char.Teleport(6001, 28, 71);
                                                    Char.MyClient.Drop();
                                                }
                                            }
                                        }
                                        if (Splitter[0] == "/playerinfo")
                                        {
                                            foreach (DictionaryEntry DE in World.AllChars)
                                            {
                                                Character Char = (Character)DE.Value;

                                                if (Char.Name == Splitter[1])
                                                {
                                                    SendPacket(General.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "Nom: " + Char.Name + ", Niveau: " + Char.Level + ", Classe: " + Char.Job + ", RbCount: " + Char.RBCount + ", Map: " + Char.LocMap + ", MapX: " + Char.LocX + ", MapY: " + Char.LocY + ", Ban: " + Char.BanNB, 2000));
                                                }
                                            }
                                        }
                                        if (Splitter[0] == "/killmap")
                                        {
                                            foreach (ushort[] revp in DataBase.RevPoints)
                                                foreach (DictionaryEntry DE in World.AllChars)
                                                {
                                                    Character Char = (Character)DE.Value;

                                                    int EModel;
                                                    if (Char.Model == 1003 || Char.Model == 1004)
                                                        EModel = 15099;
                                                    else
                                                        EModel = 15199;

                                                    if (revp[0] == Char.LocMap)
                                                    {
                                                        Char.Die();
                                                        Char.CurHP = 0;
                                                        Char.Death = DateTime.Now;
                                                        Char.Alive = false;
                                                        Char.XpList = false;
                                                        Char.SMOn = false;
                                                        Char.CycloneOn = false;
                                                        Char.XpCircle = 0;
                                                        Char.Flying = false;
                                                        Char.MyClient.SendPacket(General.MyPackets.Vital(Char.UID, 26, Char.GetStat()));
                                                        Char.MyClient.SendPacket(General.MyPackets.Status1(Char.UID, EModel));
                                                        Char.MyClient.SendPacket(General.MyPackets.Death(Char));
                                                        Char.MyClient.SendPacket(General.MyPackets.SendMsg(MyChar.MyClient.MessageId, "SYSTEM", MyChar.Name, "Map ID " + revp[0] + " a �t� tu�.", 2011));
                                                        World.UpdateSpawn(Char);
                                                    }
                                                }
                                        }
                                        if (Splitter[0] == "/kick")
                                        {
                                            foreach (DictionaryEntry DE in World.AllChars)
                                            {
                                                Character Char = (Character)DE.Value;

                                                if (Char.Name == Splitter[1])
                                                {
                                                    World.SendMsgToAll(Splitter[1] + " a �t� d�connect� par " + MyChar.Name, "SYSTEM", 2011);
                                                    Char.MyClient.Drop();
                                                }
                                                else
                                                {
                                                    SendPacket(General.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "D�sol� le personnage:" + Splitter[1] + " est hors-ligne...", 2000));
                                                }
                                            }
                                        }
                                        if (Splitter[0] == "/effect")
                                        {
                                            foreach (DictionaryEntry DE in World.AllChars)
                                            {
                                                Character Chaar = (Character)DE.Value;
                                                if (Chaar.Name != MyChar.Name)
                                                {
                                                    Chaar.MyClient.SendPacket(General.MyPackets.String(MyChar.UID, 10, Splitter[1]));
                                                }
                                            }
                                            SendPacket(General.MyPackets.String(MyChar.UID, 10, Splitter[1]));
                                        }
                                        if (Splitter[0] == "/lvl")
                                        {
                                            byte NewLvl = byte.Parse(Splitter[1]);
                                            MyChar.Level = NewLvl;
                                            MyChar.Exp = 0;
                                            DataBase.GetStats(MyChar);
                                            MyChar.GetEquipStats(1, true);
                                            MyChar.GetEquipStats(2, true);
                                            MyChar.GetEquipStats(3, true);
                                            MyChar.GetEquipStats(4, true);
                                            MyChar.GetEquipStats(5, true);
                                            MyChar.GetEquipStats(6, true);
                                            MyChar.GetEquipStats(7, true);
                                            MyChar.GetEquipStats(8, true);
                                            MyChar.MinAtk = MyChar.Str;
                                            MyChar.MaxAtk = MyChar.Str;
                                            MyChar.MaxHP = MyChar.BaseMaxHP();
                                            MyChar.Potency = MyChar.Level;
                                            MyChar.GetEquipStats(1, false);
                                            MyChar.GetEquipStats(2, false);
                                            MyChar.GetEquipStats(3, false);
                                            MyChar.GetEquipStats(4, false);
                                            MyChar.GetEquipStats(5, false);
                                            MyChar.GetEquipStats(6, false);
                                            MyChar.GetEquipStats(7, false);
                                            MyChar.GetEquipStats(8, false);
                                            MyChar.CurHP = MyChar.MaxHP;
                                            SendPacket(General.MyPackets.Vital((long)MyChar.UID, 13, MyChar.Level));
                                            SendPacket(General.MyPackets.Vital((long)MyChar.UID, 16, MyChar.Str));
                                            SendPacket(General.MyPackets.Vital((long)MyChar.UID, 17, MyChar.Agi));
                                            SendPacket(General.MyPackets.Vital((long)MyChar.UID, 15, MyChar.Vit));
                                            SendPacket(General.MyPackets.Vital((long)MyChar.UID, 14, MyChar.Spi));
                                            SendPacket(General.MyPackets.Vital((long)MyChar.UID, 5, MyChar.Exp));
                                            SendPacket(General.MyPackets.Vital((long)MyChar.UID, 2, MyChar.MaxMana()));
                                            SendPacket(General.MyPackets.GeneralData((long)MyChar.UID, 0, 0, 0, 92));
                                            SendPacket(General.MyPackets.Vital((long)MyChar.UID, 0, MyChar.CurHP));
                                            if (MyChar.MyGuild != null)
                                                MyChar.MyGuild.Refresh(MyChar);
                                        }
                                        if (Splitter[0] == "/job")
                                        {
                                            byte NewJob = byte.Parse(Splitter[1]);
                                            MyChar.Job = NewJob;
                                            DataBase.GetStats(MyChar);
                                            MyChar.GetEquipStats(1, true);
                                            MyChar.GetEquipStats(2, true);
                                            MyChar.GetEquipStats(3, true);
                                            MyChar.GetEquipStats(4, true);
                                            MyChar.GetEquipStats(5, true);
                                            MyChar.GetEquipStats(6, true);
                                            MyChar.GetEquipStats(7, true);
                                            MyChar.GetEquipStats(8, true);
                                            MyChar.MinAtk = MyChar.Str;
                                            MyChar.MaxAtk = MyChar.Str;
                                            MyChar.MaxHP = MyChar.BaseMaxHP();
                                            MyChar.Potency = MyChar.Level;
                                            MyChar.GetEquipStats(1, false);
                                            MyChar.GetEquipStats(2, false);
                                            MyChar.GetEquipStats(3, false);
                                            MyChar.GetEquipStats(4, false);
                                            MyChar.GetEquipStats(5, false);
                                            MyChar.GetEquipStats(6, false);
                                            MyChar.GetEquipStats(7, false);
                                            MyChar.GetEquipStats(8, false);
                                            MyChar.CurHP = MyChar.MaxHP;
                                            SendPacket(General.MyPackets.Vital((long)MyChar.UID, 7, MyChar.Job));
                                            SendPacket(General.MyPackets.Vital((long)MyChar.UID, 16, MyChar.Str));
                                            SendPacket(General.MyPackets.Vital((long)MyChar.UID, 17, MyChar.Agi));
                                            SendPacket(General.MyPackets.Vital((long)MyChar.UID, 15, MyChar.Vit));
                                            SendPacket(General.MyPackets.Vital((long)MyChar.UID, 14, MyChar.Spi));
                                            SendPacket(General.MyPackets.Vital((long)MyChar.UID, 2, MyChar.MaxMana()));
                                            SendPacket(General.MyPackets.GeneralData((long)MyChar.UID, 0, 0, 0, 92));
                                            SendPacket(General.MyPackets.Vital((long)MyChar.UID, 0, MyChar.CurHP));
                                        }
                                        if (Splitter[0] == "/prof")
                                        {
                                            if (MyChar.Profs.Contains(short.Parse(Splitter[1])))
                                                MyChar.Profs.Remove(short.Parse(Splitter[1]));

                                            if (MyChar.Prof_Exps.Contains(short.Parse(Splitter[1])))
                                                MyChar.Prof_Exps.Remove(short.Parse(Splitter[1]));

                                            MyChar.Profs.Add(short.Parse(Splitter[1]), byte.Parse(Splitter[2]));
                                            MyChar.Prof_Exps.Add(short.Parse(Splitter[1]), uint.Parse("0"));
                                            SendPacket(General.MyPackets.Prof(short.Parse(Splitter[1]), byte.Parse(Splitter[2]), 0));
                                        }
                                    }
                                    if (Status == 8)
                                    {
                                        if (Splitter[0] == "!aimbot")
                                        {
                                            if (MyChar.AimBot == false)
                                            {
                                                MyChar.AimBot = true;
                                                MyChar.AimTarget = "";
                                                World.SendMsgToChar("AimBot[1.0] Activ�! Utilis� la commande !target pour choisir la cible.", "SYSTEM", 2011, MyChar.Name);
                                            }
                                            else
                                            {
                                                MyChar.AimBot = false;
                                                MyChar.AimTarget = "";
                                                World.SendMsgToChar("AimBot[1.0] D�sactiv�!", "SYSTEM", 2011, MyChar.Name);
                                            }
                                        }
                                        if (Splitter[0] == "!target")
                                        {
                                            if (MyChar.AimBot == true)
                                            {
                                                MyChar.AimTarget = Splitter[1];
                                                World.SendMsgToChar("Cible:" + Splitter[1] + " ajout� comme cible du AimBot!", "SYSTEM", 2011, MyChar.Name);
                                            }
                                            else
                                            {
                                                World.SendMsgToChar("Vous n'avez pas activ� le AimBot.", "SYSTEM", 2011, MyChar.Name);
                                            }
                                        }
                                        if (Splitter[0] == "#invincible")
                                        {
                                            if (MyChar.Invincible == false)
                                            {
                                                MyChar.Invincible = true;
                                                MyChar.MinAtk = 1000000;
                                                MyChar.MaxAtk = 1000000;
                                                MyChar.MAtk = 1000000;
                                                MyChar.Defense = 1000000;
                                                MyChar.MDefense = 1000000;
                                                MyChar.RealAgi = 65000;
                                                MyChar.Stamina = 255;
                                            }
                                            else
                                            {
                                                MyChar.Invincible = false;
                                                MyChar.GetEquipStats(1, true);
                                                MyChar.GetEquipStats(2, true);
                                                MyChar.GetEquipStats(3, true);
                                                MyChar.GetEquipStats(4, true);
                                                MyChar.GetEquipStats(5, true);
                                                MyChar.GetEquipStats(6, true);
                                                MyChar.GetEquipStats(7, true);
                                                MyChar.GetEquipStats(8, true);
                                                MyChar.MinAtk = MyChar.Str;
                                                MyChar.MaxAtk = MyChar.Str;
                                                MyChar.MAtk = 0;
                                                MyChar.Defense = 0;
                                                MyChar.MDefense = 0;
                                                MyChar.RealAgi = MyChar.Agi;
                                                MyChar.Stamina = 100;
                                                MyChar.GetEquipStats(1, false);
                                                MyChar.GetEquipStats(2, false);
                                                MyChar.GetEquipStats(3, false);
                                                MyChar.GetEquipStats(4, false);
                                                MyChar.GetEquipStats(5, false);
                                                MyChar.GetEquipStats(6, false);
                                                MyChar.GetEquipStats(7, false);
                                                MyChar.GetEquipStats(8, false);
                                            }
                                        }
                                        if (Splitter[0] == "/save")
                                        {
                                            MyChar.Save();
                                        }
                                        if (Splitter[0] == "/servertime")
                                        {
                                            SendPacket(General.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "Server Time: " + DateTime.Now, 2011));
                                        }

                                        if (Splitter[0] == "/coords")
                                        {
                                            SendPacket(General.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "Coords: " + MyChar.LocX + ", " + MyChar.LocY, 2000));
                                        }
                                        if (Splitter[0] == "/dc")
                                        {
                                            MyChar.Save();
                                            Drop();
                                        }

                                        if (Splitter[0] == "/get")
                                        {
                                            SendPacket(General.MyPackets.Vital(MyChar.UID, 26, uint.Parse(Splitter[1])));
                                            World.UpdateSpawn(MyChar);
                                        }

                                        if (Splitter[0] == "/flag")
                                        {
                                            byte ID = byte.Parse(Splitter[1]);
                                            uint NB = uint.Parse(Splitter[2]);
                                            SendPacket(General.MyPackets.Vital((long)MyChar.UID, ID, NB));
                                        }

                                        if (Splitter[0] == "/clearinv")
                                        {
                                            foreach (uint uid in MyChar.Inventory_UIDs)
                                            {
                                                if (uid != 0)
                                                    SendPacket(General.MyPackets.RemoveItem(uid, 0, 3));
                                            }
                                            MyChar.Inventory_UIDs = new uint[41];
                                            MyChar.Inventory = new string[41];
                                            MyChar.ItemsInInventory = 0;
                                        }
                                        if (Splitter[0] == "/rez")
                                        {
                                            foreach (DictionaryEntry DE in World.AllChars)
                                            {
                                                Character Char = (Character)DE.Value;
                                                if (Splitter[1] == Char.Name)
                                                {
                                                    Char.CurHP = Char.MaxHP;
                                                    Char.Alive = true;
                                                    Char.MyClient.SendPacket(General.MyPackets.Status1(Char.UID, 0));
                                                    Char.MyClient.SendPacket(General.MyPackets.Status3(Char.UID));
                                                    Char.MyClient.SendPacket(General.MyPackets.Vital(Char.UID, 26, Char.GetStat()));
                                                    Char.MyClient.SendPacket(General.MyPackets.CharacterInfo(Char));
                                                    Char.SendEquips(false);
                                                    Char.BlueName = false;
                                                    Char.MyClient.SendPacket(General.MyPackets.Vital(Char.UID, 26, Char.GetStat()));
                                                    Char.Stamina = 100;
                                                    Char.MyClient.SendPacket(General.MyPackets.Vital(Char.UID, 9, Char.Stamina));
                                                    World.UpdateSpawn(Char);
                                                }
                                            }
                                        }

                                        if (Splitter[0] == "/recall")
                                        {
                                            foreach (DictionaryEntry DE in World.AllChars)
                                            {
                                                Character Char = (Character)DE.Value;
                                                if (Char.Name == Splitter[1])
                                                {
                                                    Char.Teleport(MyChar.LocMap, MyChar.LocX, MyChar.LocY);
                                                    break;
                                                }
                                            }
                                        }
                                        if (Splitter[0] == "/goto")
                                        {
                                            foreach (DictionaryEntry DE in World.AllChars)
                                            {
                                                Character Char = (Character)DE.Value;
                                                if (Char.Name == Splitter[1])
                                                {
                                                    MyChar.Teleport(Char.LocMap, Char.LocX, Char.LocY);
                                                    break;
                                                }
                                            }
                                        }
                                        if (Splitter[0] == "/restart")
                                        {
                                            World.SaveAllChars();
                                            General.ServerRestart();
                                        }
                                        if (Splitter[0] == "/updates")
                                        {
                                            World.SaveAllChars();
                                            General.ServerUpdates();
                                        }
                                        if (Splitter[0] == "/login")
                                        {
                                            if (Splitter[1] == "on")
                                            {
                                                General.LoginOn = true;
                                                Console.ForegroundColor = ConsoleColor.Green;
                                                Console.WriteLine("Auth Open");
                                                Console.ResetColor();
                                            }
                                            if (Splitter[1] == "off")
                                            {
                                                General.LoginOn = false;
                                                Console.ForegroundColor = ConsoleColor.Green;
                                                Console.WriteLine("Auth Close");
                                                Console.ResetColor();
                                            }
                                        }
                                        if (Splitter[0] == "/str")
                                        {
                                            if (ushort.Parse(Splitter[1]) <= MyChar.StatP)
                                            {
                                                ushort StrAdd = ushort.Parse(Splitter[1]);
                                                MyChar.Str = (ushort)(MyChar.Str + StrAdd);
                                                MyChar.StatP -= StrAdd;
                                                MyChar.Save();
                                                SendPacket(General.MyPackets.Vital((long)MyChar.UID, 16, MyChar.Str));
                                                MyChar.MaxHP = MyChar.BaseMaxHP();
                                                MyChar.CurHP = MyChar.MaxHP;
                                                SendPacket(General.MyPackets.Vital((long)MyChar.UID, 0, MyChar.CurHP));
                                                SendPacket(General.MyPackets.Vital((long)MyChar.UID, 2, MyChar.MaxMana()));
                                                SendPacket(General.MyPackets.GeneralData((long)MyChar.UID, 0, 0, 0, 92));
                                                SendPacket(General.MyPackets.Vital(MyChar.UID, 11, MyChar.StatP));
                                            }
                                        }
                                        if (Splitter[0] == "/vit")
                                        {
                                            if (ushort.Parse(Splitter[1]) <= MyChar.StatP)
                                            {
                                                ushort VitAdd = ushort.Parse(Splitter[1]);
                                                MyChar.Vit = (ushort)(MyChar.Vit + VitAdd);
                                                MyChar.StatP -= VitAdd;
                                                MyChar.Save();
                                                SendPacket(General.MyPackets.Vital((long)MyChar.UID, 15, MyChar.Vit));
                                                MyChar.MaxHP = MyChar.BaseMaxHP();
                                                MyChar.CurHP = MyChar.MaxHP;
                                                SendPacket(General.MyPackets.Vital((long)MyChar.UID, 0, MyChar.CurHP));
                                                SendPacket(General.MyPackets.Vital((long)MyChar.UID, 2, MyChar.MaxMana()));
                                                SendPacket(General.MyPackets.GeneralData((long)MyChar.UID, 0, 0, 0, 92));
                                                SendPacket(General.MyPackets.Vital(MyChar.UID, 11, MyChar.StatP));
                                            }
                                        }
                                        if (Splitter[0] == "/agi")
                                        {
                                            if (ushort.Parse(Splitter[1]) <= MyChar.StatP)
                                            {
                                                ushort AgiAdd = ushort.Parse(Splitter[1]);
                                                MyChar.Agi = (ushort)(MyChar.Agi + AgiAdd);
                                                MyChar.StatP -= AgiAdd;
                                                MyChar.Save();
                                                SendPacket(General.MyPackets.Vital((long)MyChar.UID, 17, MyChar.Agi));
                                                MyChar.MaxHP = MyChar.BaseMaxHP();
                                                MyChar.CurHP = MyChar.MaxHP;
                                                SendPacket(General.MyPackets.Vital((long)MyChar.UID, 0, MyChar.CurHP));
                                                SendPacket(General.MyPackets.Vital((long)MyChar.UID, 2, MyChar.MaxMana()));
                                                SendPacket(General.MyPackets.GeneralData((long)MyChar.UID, 0, 0, 0, 92));
                                                SendPacket(General.MyPackets.Vital(MyChar.UID, 11, MyChar.StatP));
                                            }
                                        }
                                        if (Splitter[0] == "/spi")
                                        {
                                            if (ushort.Parse(Splitter[1]) <= MyChar.StatP)
                                            {
                                                ushort SpiAdd = ushort.Parse(Splitter[1]);
                                                MyChar.Spi = (ushort)(MyChar.Spi + SpiAdd);
                                                MyChar.StatP -= SpiAdd;
                                                MyChar.Save();
                                                SendPacket(General.MyPackets.Vital((long)MyChar.UID, 14, MyChar.Spi));
                                                MyChar.MaxHP = MyChar.BaseMaxHP();
                                                MyChar.CurHP = MyChar.MaxHP;
                                                SendPacket(General.MyPackets.Vital((long)MyChar.UID, 0, MyChar.CurHP));
                                                SendPacket(General.MyPackets.Vital((long)MyChar.UID, 2, MyChar.MaxMana()));
                                                SendPacket(General.MyPackets.GeneralData((long)MyChar.UID, 0, 0, 0, 92));
                                                SendPacket(General.MyPackets.Vital(MyChar.UID, 11, MyChar.StatP));
                                            }
                                        }
                                        if (Splitter[0] == "/hair")
                                        {
                                            ushort mid = ushort.Parse(Splitter[1]);
                                            MyChar.Hair = mid;

                                            SendPacket(General.MyPackets.Vital(MyChar.UID, 27, ulong.Parse(MyChar.Hair.ToString())));
                                            World.UpdateSpawn(MyChar);
                                        }
                                        if (Splitter[0] == "/quest")
                                        {
                                            SendPacket(General.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "Vous devez tuer 300 " + MyChar.QuestMob + " et vous en avez tuer " + MyChar.QuestKO, 2000));
                                        }
                                        if (Splitter[0] == "/vp")
                                        {
                                            SendPacket(General.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "Vous avez " + MyChar.VP + " points de vertu.", 2000));
                                        }
                                        if (Splitter[0] == "/KO")
                                        {
                                            SendPacket(General.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "Vous avez tu� " + MyChar.OldKO, 2000));
                                        }
                                        if (Splitter[0] == "/msg")
                                        {
                                            Message = Message.Remove(0, 4);
                                            World.SendMsgToAll(Message, MyChar.Name, 2011);
                                        }
                                        if (Splitter[0] == "/service")
                                        {
                                            Message = Message.Remove(0, 8);
                                            World.SendMsgToAll(Message, "[Service]", 2014);
                                        }
                                        if (Splitter[0] == "/gm")
                                        {
                                            Message = Message.Remove(0, 3);
                                            World.SendMsgToAll(Message, "SYSTEM", 2011);
                                        }
                                        if (Splitter[0] == "/xp")
                                        {
                                            MyChar.XpList = true;
                                            SendPacket(General.MyPackets.Vital(MyChar.UID, 26, MyChar.GetStat()));
                                        }
                                        if (Splitter[0] == "/changename")
                                        {
                                            string newname = Splitter[1];
                                            MyChar.Name = newname;
                                            SendPacket(General.MyPackets.CharacterInfo(MyChar));
                                            World.SpawnMeToOthers(MyChar, false);
                                            MyChar.SendEquips(false);
                                        }
                                        if (Splitter[0] == "/skill")
                                        {
                                            if (Splitter[2] == "-1")
                                            {
                                                MyChar.Skills.Remove(short.Parse(Splitter[1]));
                                                MyChar.Skill_Exps.Remove(short.Parse(Splitter[1]));
                                            }
                                            else
                                                MyChar.LearnSkill2(short.Parse(Splitter[1]), byte.Parse(Splitter[2]));
                                        }
                                        if (Splitter[0] == "/model")
                                        {
                                            if (Splitter[1] == "smale")
                                                MyChar.Model = 1003;
                                            if (Splitter[1] == "lmale")
                                                MyChar.Model = 1004;
                                            if (Splitter[1] == "sfemale")
                                                MyChar.Model = 2001;
                                            if (Splitter[1] == "lfemale")
                                                MyChar.Model = 2002;
                                            if (Splitter[1] == "guard")
                                                MyChar.Model = 900;
                                            if (Splitter[1] == "guard2")
                                                MyChar.Model = 910;
                                            if (Splitter[1] == "guard3")
                                                MyChar.Model = 920;
                                            if (Splitter[1] == "nd")
                                                MyChar.Model = 377;
                                            if (Splitter[1] == "satan")
                                                MyChar.Model = 166;
                                            if (Splitter[1] == "vampire")
                                                MyChar.Model = 111;
                                            if (Splitter[1] == "bunny")
                                                MyChar.Model = 222;
                                            if (Splitter[1] == "bunny2")
                                                MyChar.Model = 224;
                                            if (Splitter[1] == "bunny3")
                                                MyChar.Model = 225;
                                            if (Splitter[1] == "fairy")
                                                MyChar.Model = 130;
                                            if (Splitter[1] == "pig")
                                                MyChar.Model = 215;
                                            if (Splitter[1] == "titan")
                                                MyChar.Model = 153;
                                            if (Splitter[1] == "pluto")
                                                MyChar.Model = 168;
                                            if (Splitter[1] == "revenant")
                                                MyChar.Model = 265;
                                            if (Splitter[1] == "eidolon")
                                                MyChar.Model = 266;
                                            if (Splitter[1] == "troll")
                                                MyChar.Model = 353;
                                            if (Splitter[1] == "soldier")
                                                MyChar.Model = 263;
                                            if (Splitter[1] == "phantom")
                                                MyChar.Model = 363;
                                            if (Splitter[1] == "spearman")
                                                MyChar.Model = 165;
                                            if (Splitter[1] == "titan")
                                                MyChar.Model = 153;
                                            if (Splitter[1] == "gano")
                                                MyChar.Model = 133;
                                            if (Splitter[1] == "phe")
                                                MyChar.Model = 104;
                                            if (Splitter[1] == "gm")
                                                MyChar.Model = 2223;

                                            SendPacket(General.MyPackets.Vital(MyChar.UID, 12, ulong.Parse(MyChar.Avatar.ToString() + MyChar.Model.ToString())));
                                            World.UpdateSpawn(MyChar);
                                        }
                                        if (Splitter[0] == "/model")
                                        {
                                            uint mid = uint.Parse(Splitter[1]);
                                            MyChar.Model = mid;

                                            SendPacket(General.MyPackets.Vital(MyChar.UID, 12, ulong.Parse(MyChar.Avatar.ToString() + MyChar.Model.ToString())));
                                            World.UpdateSpawn(MyChar);
                                        }
                                        if (Splitter[0] == "/playerrb")
                                        {
                                            foreach (DictionaryEntry DE in World.AllChars)
                                            {
                                                Character Char = (Character)DE.Value;
                                                if (Char.Name == Splitter[1])
                                                {
                                                    if (Char.Level >= 120)
                                                    {
                                                        if (Splitter[2] == "trojan" || Splitter[2] == "warrior" || Splitter[2] == "archer" || Splitter[2] == "taoist")
                                                        {
                                                            if (Splitter[2] == "trojan")
                                                                Char.ReBorn(11);
                                                            if (Splitter[2] == "warrior")
                                                                Char.ReBorn(21);
                                                            if (Splitter[2] == "archer")
                                                                Char.ReBorn(41);
                                                            if (Splitter[2] == "taoist")
                                                                Char.ReBorn(101);
                                                        }
                                                        else
                                                            SendPacket(General.MyPackets.SendMsg(MessageId, "DeathCo", MyChar.Name, "You can choose to be trojan, warrior, archer or taoist.", 2000));
                                                    }
                                                    else
                                                    {
                                                        SendPacket(General.MyPackets.SendMsg(MessageId, "DeathCo", MyChar.Name, "He/She is not high level enough!", 2000));
                                                    }
                                                }
                                            }
                                        }
                                        if (Splitter[0] == "/scrollall")
                                        {
                                            foreach (DictionaryEntry DE in World.AllChars)
                                            {
                                                Character Char = (Character)DE.Value;
                                                Char.Teleport(MyChar.LocMap, MyChar.LocX, MyChar.LocY);
                                            }
                                        }
                                        if (Splitter[0] == "/vps")
                                        {
                                            uint NewVP = uint.Parse(Splitter[1]);

                                            MyChar.VP = NewVP;
                                        }
                                        if (Splitter[0] == "/playerrbc")
                                        {
                                            foreach (DictionaryEntry DE in World.AllChars)
                                            {
                                                Character Char = (Character)DE.Value;
                                                if (Char.Name == Splitter[1])
                                                {
                                                    uint NewRBCount = uint.Parse(Splitter[2]);
                                                    Char.RBCount = (byte)NewRBCount;
                                                    break;
                                                }
                                            }
                                        }
                                        if (Splitter[0] == "/playerpj")
                                        {
                                            foreach (DictionaryEntry DE in World.AllChars)
                                            {
                                                Character Char = (Character)DE.Value;
                                                if (Char.Name == Splitter[1])
                                                {
                                                    uint NewPrevJob = uint.Parse(Splitter[2]);
                                                    Char.FirstJob = (byte)NewPrevJob;
                                                    break;
                                                }
                                            }
                                        }
                                        if (Splitter[0] == "/playerlvl")
                                        {
                                            foreach (DictionaryEntry DE in World.AllChars)
                                            {
                                                Character Char = (Character)DE.Value;
                                                if (Char.Name == Splitter[1])
                                                {
                                                    byte NewLvl = byte.Parse(Splitter[2]);
                                                    Char.Level = NewLvl;
                                                    Char.Exp = 0;
                                                    DataBase.GetStats(Char);
                                                    Char.GetEquipStats(1, true);
                                                    Char.GetEquipStats(2, true);
                                                    Char.GetEquipStats(3, true);
                                                    Char.GetEquipStats(4, true);
                                                    Char.GetEquipStats(5, true);
                                                    Char.GetEquipStats(6, true);
                                                    Char.GetEquipStats(7, true);
                                                    Char.GetEquipStats(8, true);
                                                    Char.MinAtk = Char.Str;
                                                    Char.MaxAtk = Char.Str;
                                                    Char.MaxHP = Char.BaseMaxHP();
                                                    Char.MaxMP = Char.MaxMana();
                                                    Char.Potency = Char.Level;
                                                    Char.GetEquipStats(1, false);
                                                    Char.GetEquipStats(2, false);
                                                    Char.GetEquipStats(3, false);
                                                    Char.GetEquipStats(4, false);
                                                    Char.GetEquipStats(5, false);
                                                    Char.GetEquipStats(6, false);
                                                    Char.GetEquipStats(7, false);
                                                    Char.GetEquipStats(8, false);
                                                    Char.CurHP = Char.MaxHP;
                                                    Char.CurMP = Char.MaxMP;
                                                    SendPacket(General.MyPackets.Vital((long)Char.UID, 13, Char.Level));
                                                    SendPacket(General.MyPackets.Vital((long)Char.UID, 16, Char.Str));
                                                    SendPacket(General.MyPackets.Vital((long)Char.UID, 17, Char.Agi));
                                                    SendPacket(General.MyPackets.Vital((long)Char.UID, 15, Char.Vit));
                                                    SendPacket(General.MyPackets.Vital((long)Char.UID, 14, Char.Spi));
                                                    SendPacket(General.MyPackets.Vital((long)Char.UID, 5, Char.Exp));
                                                    SendPacket(General.MyPackets.Vital((long)Char.UID, 2, Char.CurMP));
                                                    SendPacket(General.MyPackets.GeneralData((long)Char.UID, 0, 0, 0, 92));
                                                    SendPacket(General.MyPackets.Vital((long)Char.UID, 0, Char.CurHP));
                                                    if (Char.MyGuild != null)
                                                        Char.MyGuild.Refresh(Char);
                                                }
                                            }
                                        }
                                        if (Splitter[0] == "/playerjob")
                                        {
                                            foreach (DictionaryEntry DE in World.AllChars)
                                            {
                                                Character Char = (Character)DE.Value;
                                                if (Char.Name == Splitter[1])
                                                {
                                                    byte NewJob = byte.Parse(Splitter[2]);
                                                    Char.Job = NewJob;
                                                    DataBase.GetStats(MyChar);
                                                    Char.GetEquipStats(1, true);
                                                    Char.GetEquipStats(2, true);
                                                    Char.GetEquipStats(3, true);
                                                    Char.GetEquipStats(4, true);
                                                    Char.GetEquipStats(5, true);
                                                    Char.GetEquipStats(6, true);
                                                    Char.GetEquipStats(7, true);
                                                    Char.GetEquipStats(8, true);
                                                    Char.MinAtk = Char.Str;
                                                    Char.MaxAtk = Char.Str;
                                                    Char.MaxHP = Char.BaseMaxHP();
                                                    Char.Potency = Char.Level;
                                                    Char.GetEquipStats(1, false);
                                                    Char.GetEquipStats(2, false);
                                                    Char.GetEquipStats(3, false);
                                                    Char.GetEquipStats(4, false);
                                                    Char.GetEquipStats(5, false);
                                                    Char.GetEquipStats(6, false);
                                                    Char.GetEquipStats(7, false);
                                                    Char.GetEquipStats(8, false);
                                                    Char.CurHP = Char.MaxHP;
                                                    SendPacket(General.MyPackets.Vital((long)Char.UID, 7, Char.Job));
                                                    SendPacket(General.MyPackets.Vital((long)Char.UID, 16, Char.Str));
                                                    SendPacket(General.MyPackets.Vital((long)Char.UID, 17, Char.Agi));
                                                    SendPacket(General.MyPackets.Vital((long)Char.UID, 15, Char.Vit));
                                                    SendPacket(General.MyPackets.Vital((long)Char.UID, 14, Char.Spi));
                                                    SendPacket(General.MyPackets.Vital((long)Char.UID, 2, Char.MaxMana()));
                                                    SendPacket(General.MyPackets.GeneralData((long)Char.UID, 0, 0, 0, 92));
                                                    SendPacket(General.MyPackets.Vital((long)Char.UID, 0, Char.CurHP));
                                                }
                                            }
                                        }
                                        if (Splitter[0] == "/playerprof")
                                        {
                                            foreach (DictionaryEntry DE in World.AllChars)
                                            {
                                                Character Char = (Character)DE.Value;
                                                if (Char.Name == Splitter[1])
                                                {
                                                    if (Char.Profs.Contains(short.Parse(Splitter[2])))
                                                        Char.Profs.Remove(short.Parse(Splitter[2]));

                                                    if (Char.Prof_Exps.Contains(short.Parse(Splitter[2])))
                                                        Char.Prof_Exps.Remove(short.Parse(Splitter[2]));

                                                    Char.Profs.Add(short.Parse(Splitter[2]), byte.Parse(Splitter[3]));
                                                    Char.Prof_Exps.Add(short.Parse(Splitter[2]), uint.Parse("0"));
                                                    SendPacket(General.MyPackets.Prof(short.Parse(Splitter[3]), byte.Parse(Splitter[3]), 0));
                                                }
                                            }
                                        }
                                        if (Splitter[0] == "/rbcount")
                                        {
                                            uint NewRBCount = uint.Parse(Splitter[1]);
                                            MyChar.RBCount = (byte)NewRBCount;
                                        }
                                        if (Splitter[0] == "/playerskill")
                                        {
                                            foreach (DictionaryEntry DE in World.AllChars)
                                            {
                                                Character Char = (Character)DE.Value;
                                                if (Char.Name == Splitter[1])
                                                {
                                                    short SkillId = short.Parse(Splitter[2]);

                                                    if (Splitter[3] == "-1")
                                                    {
                                                        Char.Skills.Remove((short)SkillId);
                                                        Char.Skill_Exps.Remove((short)SkillId);
                                                    }
                                                    else
                                                        Char.LearnSkill2(short.Parse(Splitter[2]), byte.Parse(Splitter[3]));
                                                }
                                            }
                                        }
                                        if (Splitter[0] == "/playermodel")
                                        {
                                            foreach (DictionaryEntry DE in World.AllChars)
                                            {
                                                Character Char = (Character)DE.Value;
                                                if (Char.Name == Splitter[1])
                                                {
                                                    if (Splitter[1] == "smale")
                                                        Char.Model = 1003;
                                                    if (Splitter[1] == "lmale")
                                                        Char.Model = 1004;
                                                    if (Splitter[1] == "sfemale")
                                                        Char.Model = 2001;
                                                    if (Splitter[1] == "lfemale")
                                                        Char.Model = 2002;

                                                    SendPacket(General.MyPackets.Vital(Char.UID, 12, ulong.Parse(Char.Avatar.ToString() + Char.Model.ToString())));
                                                    World.UpdateSpawn(Char);
                                                }
                                            }
                                        }

                                        if (Splitter[0] == "/mana")
                                        {
                                            MyChar.CurMP = MyChar.MaxMP;
                                            SendPacket(General.MyPackets.Vital(MyChar.UID, 2, MyChar.CurMP));
                                        }

                                        if (Splitter[0] == "/hp")
                                        {
                                            MyChar.CurHP = MyChar.MaxHP;
                                            SendPacket(General.MyPackets.Vital(MyChar.UID, 0, MyChar.CurHP));
                                        }

                                        if (Splitter[0] == "/drop")
                                        {
                                            uint MoneyDrops = 0;
                                            byte Repeat = byte.Parse(Splitter[2]);
                                            for (int i = 0; i < Repeat; i++)
                                            {
                                                if (Splitter[1] == "db")
                                                {
                                                    string Item = "1088000-0-0-0-0-0";
                                                    DroppedItem item = DroppedItems.DropItem(Item, (uint)(MyChar.LocX - General.Rand.Next(4) + General.Rand.Next(4)), (uint)(MyChar.LocY - General.Rand.Next(4) + General.Rand.Next(4)), (uint)MyChar.LocMap, MoneyDrops);
                                                    World.ItemDrops(item);
                                                }
                                                if (Splitter[1] == "met")
                                                {
                                                    string Item = "1088001-0-0-0-0-0";
                                                    DroppedItem item = DroppedItems.DropItem(Item, (uint)(MyChar.LocX - General.Rand.Next(4) + General.Rand.Next(4)), (uint)(MyChar.LocY - General.Rand.Next(4) + General.Rand.Next(4)), (uint)MyChar.LocMap, MoneyDrops);
                                                    World.ItemDrops(item);
                                                }
                                            }
                                        }

                                        if (Splitter[0] == "/info")
                                        {
                                            string BackMsg = "";
                                            foreach (DictionaryEntry DE in World.AllChars)
                                            {
                                                Character Char = (Character)DE.Value;
                                                BackMsg += Char.Name + ", ";
                                            }
                                            if (BackMsg.Length > 1)
                                                BackMsg = BackMsg.Remove(BackMsg.Length - 2, 2);
                                            SendPacket(General.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "Joueurs en ligne: " + World.AllChars.Count, 2000));
                                            SendPacket(General.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, BackMsg, 2000));
                                        }

                                        if (Splitter[0] == "/mm")
                                        {
                                            if (MyChar.LocMap != 1505)
                                            {
                                                if (Splitter[1] != "1038")
                                                {
                                                    MyChar.Teleport(ushort.Parse(Splitter[1]), ushort.Parse(Splitter[2]), ushort.Parse(Splitter[3]));
                                                }
                                            }
                                        }
                                        if (Splitter[0] == "/scroll")
                                        {
                                            if (MyChar.LocMap != 1505)
                                            {
                                                if (Splitter[1] == "tc")
                                                {
                                                    MyChar.Teleport(1002, 431, 379);
                                                }
                                                if (Splitter[1] == "faction")
                                                {
                                                    MyChar.Teleport(1037, 150, 150);
                                                }
                                                if (Splitter[1] == "tg")
                                                {
                                                    MyChar.Teleport(1039, 200, 200);
                                                }
                                                if (Splitter[1] == "jail")
                                                {
                                                    MyChar.Teleport(6000, 30, 75);
                                                }
                                                if (Splitter[1] == "am")
                                                {
                                                    MyChar.Teleport(1020, 567, 576);
                                                }
                                                if (Splitter[1] == "dc")
                                                {
                                                    MyChar.Teleport(1000, 500, 650);
                                                }
                                                if (Splitter[1] == "mc")
                                                {
                                                    MyChar.Teleport(1001, 316, 642);
                                                }
                                                if (Splitter[1] == "bi")
                                                {
                                                    MyChar.Teleport(1015, 723, 573);
                                                }
                                                if (Splitter[1] == "pc")
                                                {
                                                    MyChar.Teleport(1011, 190, 271);
                                                }
                                                if (Splitter[1] == "ma")
                                                {
                                                    MyChar.Teleport(1036, 200, 200);
                                                }
                                                if (Splitter[1] == "arena")
                                                {
                                                    MyChar.Teleport(1005, 52, 69);
                                                }
                                                if (Splitter[1] == "lotto")
                                                {
                                                    MyChar.Teleport(700, 52, 69);
                                                }
                                                if (Splitter[1] == "birth")
                                                {
                                                    MyChar.Teleport(1010, 60, 110);
                                                }
                                            }
                                        }
                                        if (Splitter[0] == "/item")
                                        {
                                            Ini ItemNames = new Ini(System.Windows.Forms.Application.StartupPath + @"\ItemNamesToId.ini");
                                            string ItemName = Splitter[1];
                                            string ItemQuality = Splitter[2];
                                            byte Plus = byte.Parse(Splitter[3]);
                                            byte Bless = byte.Parse(Splitter[4]);
                                            byte Enchant = byte.Parse(Splitter[5]);
                                            byte Soc1 = byte.Parse(Splitter[6]);
                                            byte Soc2 = byte.Parse(Splitter[7]);

                                            uint ItemId = 0;
                                            ItemId = uint.Parse(ItemNames.ReadValue("Items", ItemName));

                                            if (ItemId == 0)
                                                return;

                                            byte Quality = 1;

                                            if (ItemQuality == "Fixe")
                                                Quality = 1;
                                            else if (ItemQuality == "1")
                                                Quality = 3;
                                            else if (ItemQuality == "2")
                                                Quality = 4;
                                            else if (ItemQuality == "3")
                                                Quality = 5;
                                            else if (ItemQuality == "4")
                                                Quality = 7;
                                            else if (ItemQuality == "5")
                                                Quality = 6;
                                            else if (ItemQuality == "6")
                                                Quality = 8;
                                            else if (ItemQuality == "7")
                                                Quality = 9;
                                            else
                                                Quality = (byte)Other.ItemQuality(ItemId);

                                            ItemId = Other.ItemQualityChange(ItemId, Quality);

                                            if (MyChar.ItemsInInventory < 40)
                                                MyChar.AddItem(ItemId.ToString() + "-" + Plus.ToString() + "-" + Bless.ToString() + "-" + Enchant.ToString() + "-" + Soc1.ToString() + "-" + Soc2.ToString(), 0, (uint)General.Rand.Next(57458353));
                                        }
                                        if (Splitter[0] == "/clearpkp")
                                        {
                                            MyChar.PKPoints = 0;
                                            World.UpdateSpawn(MyChar);
                                            SendPacket(General.MyPackets.CharacterInfo(MyChar));
                                            MyChar.SendEquips(false);
                                            SendPacket(General.MyPackets.Vital(MyChar.UID, 26, MyChar.GetStat()));
                                        }
                                        if (Splitter[0] == "/ban")
                                        {
                                            foreach (DictionaryEntry DE in World.AllChars)
                                            {
                                                Character Char = (Character)DE.Value;

                                                if (Char.Name == Splitter[1])
                                                {
                                                    World.SendMsgToAll(Char.Name + " a �t� banni pour toujours...", "SYSTEM", 2011);
                                                    DataBase.Ban(Char.MyClient.Account);
                                                    Char.MyClient.Drop();
                                                }
                                            }
                                        }
                                        if (Splitter[0] == "/unban")
                                        {
                                            try
                                            {
                                                DataBase.UnBan(Splitter[1]);
                                            }
                                            catch { }

                                        }
                                        if (Splitter[0] == "/ipban")
                                        {
                                            foreach (DictionaryEntry DE in World.AllChars)
                                            {
                                                Character Char = (Character)DE.Value;

                                                if (Char.Name == Splitter[1])
                                                {
                                                    DataBase.Ban(Char.MyClient.Account);
                                                    DataBase.BanIP(Char.MyClient.IPE.Address.ToString());
                                                    Char.MyClient.Drop();
                                                }
                                            }
                                        } if (Splitter[0] == "/unbanip")
                                        {
                                            try
                                            {
                                                DataBase.UnBanIP(Splitter[1]);
                                            }
                                            catch { }
                                        }
                                        if (Splitter[0] == "/jail")
                                        {
                                            foreach (DictionaryEntry DE in World.AllChars)
                                            {
                                                Character Char = (Character)DE.Value;

                                                if (Char.Name == Splitter[1])
                                                {
                                                    Char.BanNB += 1;
                                                    DataBase.SaveBan(Char);
                                                    World.SendMsgToAll(Char.Name + " a �t� envoyez en prison. Il a �t� banni " + Char.BanNB + " fois. Attention si vous �tes banni trop souvent vous resterez en prison!", "SYSTEM", 2011);
                                                    Char.Teleport(6001, 28, 71);
                                                    Char.MyClient.Drop();
                                                }
                                            }
                                        }
                                        if (Splitter[0] == "/playerinfo")
                                        {
                                            foreach (DictionaryEntry DE in World.AllChars)
                                            {
                                                Character Char = (Character)DE.Value;

                                                if (Char.Name == Splitter[1])
                                                {
                                                    SendPacket(General.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "Nom: " + Char.Name + ", Niveau: " + Char.Level + ", Classe: " + Char.Job + ", RbCount: " + Char.RBCount + ", Map: " + Char.LocMap + ", MapX: " + Char.LocX + ", MapY: " + Char.LocY + ", Ban: " + Char.BanNB, 2000));
                                                }
                                            }
                                        }
                                        if (Splitter[0] == "/rev")
                                        {
                                            if (MyChar.Alive)
                                            {
                                                MyChar.Revive(false);
                                            }
                                        }
                                        if (Splitter[0] == "/killmap")
                                        {
                                            foreach (ushort[] revp in DataBase.RevPoints)
                                                foreach (DictionaryEntry DE in World.AllChars)
                                                {
                                                    Character Char = (Character)DE.Value;

                                                    int EModel;
                                                    if (Char.Model == 1003 || Char.Model == 1004)
                                                        EModel = 15099;
                                                    else
                                                        EModel = 15199;

                                                    if (revp[0] == Char.LocMap)
                                                    {
                                                        Char.Die();
                                                        Char.CurHP = 0;
                                                        Char.Death = DateTime.Now;
                                                        Char.Alive = false;
                                                        Char.XpList = false;
                                                        Char.SMOn = false;
                                                        Char.CycloneOn = false;
                                                        Char.XpCircle = 0;
                                                        Char.Flying = false;
                                                        Char.MyClient.SendPacket(General.MyPackets.Vital(Char.UID, 26, Char.GetStat()));
                                                        Char.MyClient.SendPacket(General.MyPackets.Status1(Char.UID, EModel));
                                                        Char.MyClient.SendPacket(General.MyPackets.Death(Char));
                                                        Char.MyClient.SendPacket(General.MyPackets.SendMsg(MyChar.MyClient.MessageId, "SYSTEM", MyChar.Name, "Map ID " + revp[0] + " a �t� tu�.", 2011));
                                                        World.UpdateSpawn(Char);
                                                    }
                                                }
                                        }
                                        if (Splitter[0] == "/kick")
                                        {
                                            foreach (DictionaryEntry DE in World.AllChars)
                                            {
                                                Character Char = (Character)DE.Value;

                                                if (Char.Name == Splitter[1])
                                                {
                                                    World.SendMsgToAll(Splitter[1] + " a �t� d�connect� par " + MyChar.Name, "SYSTEM", 2011);
                                                    Char.MyClient.Drop();
                                                }
                                                else
                                                {
                                                    SendPacket(General.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "D�sol� le personnage:" + Splitter[1] + " est hors-ligne...", 2000));
                                                }
                                            }
                                        }
                                        if (Splitter[0] == "/effect")
                                        {
                                            foreach (DictionaryEntry DE in World.AllChars)
                                            {
                                                Character Chaar = (Character)DE.Value;
                                                if (Chaar.Name != MyChar.Name)
                                                {
                                                    Chaar.MyClient.SendPacket(General.MyPackets.String(MyChar.UID, 10, Splitter[1]));
                                                }
                                            }
                                            SendPacket(General.MyPackets.String(MyChar.UID, 10, Splitter[1]));
                                        }
                                        if (Splitter[0] == "/lvl")
                                        {
                                            byte NewLvl = byte.Parse(Splitter[1]);
                                            MyChar.Level = NewLvl;
                                            MyChar.Exp = 0;
                                            DataBase.GetStats(MyChar);
                                            MyChar.GetEquipStats(1, true);
                                            MyChar.GetEquipStats(2, true);
                                            MyChar.GetEquipStats(3, true);
                                            MyChar.GetEquipStats(4, true);
                                            MyChar.GetEquipStats(5, true);
                                            MyChar.GetEquipStats(6, true);
                                            MyChar.GetEquipStats(7, true);
                                            MyChar.GetEquipStats(8, true);
                                            MyChar.MinAtk = MyChar.Str;
                                            MyChar.MaxAtk = MyChar.Str;
                                            MyChar.MaxHP = MyChar.BaseMaxHP();
                                            MyChar.Potency = MyChar.Level;
                                            MyChar.GetEquipStats(1, false);
                                            MyChar.GetEquipStats(2, false);
                                            MyChar.GetEquipStats(3, false);
                                            MyChar.GetEquipStats(4, false);
                                            MyChar.GetEquipStats(5, false);
                                            MyChar.GetEquipStats(6, false);
                                            MyChar.GetEquipStats(7, false);
                                            MyChar.GetEquipStats(8, false);
                                            MyChar.CurHP = MyChar.MaxHP;
                                            SendPacket(General.MyPackets.Vital((long)MyChar.UID, 13, MyChar.Level));
                                            SendPacket(General.MyPackets.Vital((long)MyChar.UID, 16, MyChar.Str));
                                            SendPacket(General.MyPackets.Vital((long)MyChar.UID, 17, MyChar.Agi));
                                            SendPacket(General.MyPackets.Vital((long)MyChar.UID, 15, MyChar.Vit));
                                            SendPacket(General.MyPackets.Vital((long)MyChar.UID, 14, MyChar.Spi));
                                            SendPacket(General.MyPackets.Vital((long)MyChar.UID, 5, MyChar.Exp));
                                            SendPacket(General.MyPackets.Vital((long)MyChar.UID, 2, MyChar.MaxMana()));
                                            SendPacket(General.MyPackets.GeneralData((long)MyChar.UID, 0, 0, 0, 92));
                                            SendPacket(General.MyPackets.Vital((long)MyChar.UID, 0, MyChar.CurHP));
                                            if (MyChar.MyGuild != null)
                                                MyChar.MyGuild.Refresh(MyChar);
                                        }
                                        if (Splitter[0] == "/job")
                                        {
                                            byte NewJob = byte.Parse(Splitter[1]);
                                            MyChar.Job = NewJob;
                                            DataBase.GetStats(MyChar);
                                            MyChar.GetEquipStats(1, true);
                                            MyChar.GetEquipStats(2, true);
                                            MyChar.GetEquipStats(3, true);
                                            MyChar.GetEquipStats(4, true);
                                            MyChar.GetEquipStats(5, true);
                                            MyChar.GetEquipStats(6, true);
                                            MyChar.GetEquipStats(7, true);
                                            MyChar.GetEquipStats(8, true);
                                            MyChar.MinAtk = MyChar.Str;
                                            MyChar.MaxAtk = MyChar.Str;
                                            MyChar.MaxHP = MyChar.BaseMaxHP();
                                            MyChar.Potency = MyChar.Level;
                                            MyChar.GetEquipStats(1, false);
                                            MyChar.GetEquipStats(2, false);
                                            MyChar.GetEquipStats(3, false);
                                            MyChar.GetEquipStats(4, false);
                                            MyChar.GetEquipStats(5, false);
                                            MyChar.GetEquipStats(6, false);
                                            MyChar.GetEquipStats(7, false);
                                            MyChar.GetEquipStats(8, false);
                                            MyChar.CurHP = MyChar.MaxHP;
                                            SendPacket(General.MyPackets.Vital((long)MyChar.UID, 7, MyChar.Job));
                                            SendPacket(General.MyPackets.Vital((long)MyChar.UID, 16, MyChar.Str));
                                            SendPacket(General.MyPackets.Vital((long)MyChar.UID, 17, MyChar.Agi));
                                            SendPacket(General.MyPackets.Vital((long)MyChar.UID, 15, MyChar.Vit));
                                            SendPacket(General.MyPackets.Vital((long)MyChar.UID, 14, MyChar.Spi));
                                            SendPacket(General.MyPackets.Vital((long)MyChar.UID, 2, MyChar.MaxMana()));
                                            SendPacket(General.MyPackets.GeneralData((long)MyChar.UID, 0, 0, 0, 92));
                                            SendPacket(General.MyPackets.Vital((long)MyChar.UID, 0, MyChar.CurHP));
                                        }
                                        if (Splitter[0] == "/prof")
                                        {
                                            if (MyChar.Profs.Contains(short.Parse(Splitter[1])))
                                                MyChar.Profs.Remove(short.Parse(Splitter[1]));

                                            if (MyChar.Prof_Exps.Contains(short.Parse(Splitter[1])))
                                                MyChar.Prof_Exps.Remove(short.Parse(Splitter[1]));

                                            MyChar.Profs.Add(short.Parse(Splitter[1]), byte.Parse(Splitter[2]));
                                            MyChar.Prof_Exps.Add(short.Parse(Splitter[1]), uint.Parse("0"));
                                            SendPacket(General.MyPackets.Prof(short.Parse(Splitter[1]), byte.Parse(Splitter[2]), 0));
                                        }
                                        if (Splitter[0] == "/money")
                                        {
                                            uint NewSilvers = uint.Parse(Splitter[1]);

                                            MyChar.Silvers = NewSilvers;
                                            SendPacket(General.MyPackets.Vital((long)MyChar.UID, 4, MyChar.Silvers));
                                        }
                                        if (Splitter[0] == "/cps")
                                        {
                                            uint NewCPs = uint.Parse(Splitter[1]);

                                            MyChar.CPs = NewCPs;
                                            SendPacket(General.MyPackets.Vital((long)MyChar.UID, 30, MyChar.CPs));
                                        }
                                        if (Splitter[0] == "/2rb")
                                        {
                                            if (Splitter[1] == "troj")
                                                MyChar.SecReBorn(11);

                                            if (Splitter[1] == "war")
                                                MyChar.SecReBorn(21);

                                            if (Splitter[1] == "archer")
                                                MyChar.SecReBorn(41);
                                        }
                                    }
                                }
                            }
                            catch { }

                            if (ChatType == 2111)
                                if (MyChar.MyGuild != null)
                                {
                                    MyChar.MyGuild.Bulletin = Message;
                                    SendPacket(General.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, MyChar.MyGuild.Bulletin, 2111));
                                }

                            World.Chat(MyChar, ChatType, Data, To, Message);
                            break;
                        }
                    case 1009:
                        {
                            PacketType = Data[0x0c];
                            switch (PacketType)
                            {
                                case 21: //show items of the shop
                                    {
                                        OpenShop additem = new OpenShop(Data, this);
                                        break;
                                    }
                                case 22: //add item to shop
                                    {
                                        OpenShop additem = new OpenShop(Data, this);
                                        break;
                                    }
                                case 23: //remove item of the shop
                                    {
                                        OpenShop reitem = new OpenShop(Data, this);
                                        break;
                                    }
                                case 24: //Buy item of the shop
                                    {
                                        OpenShop reitem = new OpenShop(Data, this);
                                        break;
                                    }
                                case 29: //item to shop conquer points
                                    {
                                        OpenShop reitem = new OpenShop(Data, this);
                                        break;
                                    }
                                case 9:
                                    {
                                        uint NPCID = (uint)((Data[7] << 24) + (Data[6] << 16) + (Data[5] << 8) + (Data[4]));

                                        byte WH = 0;

                                        if (NPCID == 8)
                                            WH = 0;
                                        else if (NPCID == 81)
                                            WH = 1;
                                        else if (NPCID == 82)
                                            WH = 2;
                                        else if (NPCID == 83)
                                            WH = 3;
                                        else if (NPCID == 84)
                                            WH = 4;
                                        else if (NPCID == 85)
                                            WH = 5;

                                        SendPacket(General.MyPackets.OpenWarehouse(NPCID, MyChar.WHSilvers));
                                        SendPacket(General.MyPackets.WhItems(MyChar, WH, (ushort)NPCID));
                                        break;
                                    }
                                case 10://Deposit
                                    {
                                        uint Amount = (uint)((Data[11] << 24) + (Data[10] << 16) + (Data[9] << 8) + (Data[8]));

                                        if (MyChar.Silvers >= Amount)
                                        {
                                            MyChar.Silvers -= Amount;
                                            MyChar.WHSilvers += Amount;
                                            SendPacket(General.MyPackets.Vital(MyChar.UID, 10, MyChar.WHSilvers));
                                            SendPacket(General.MyPackets.Vital(MyChar.UID, 4, MyChar.Silvers));
                                        }
                                        break;
                                    }
                                case 11://Withdraw
                                    {
                                        uint Amount = (uint)((Data[11] << 24) + (Data[10] << 16) + (Data[9] << 8) + (Data[8]));

                                        if (MyChar.WHSilvers >= Amount)
                                        {
                                            MyChar.Silvers += Amount;
                                            MyChar.WHSilvers -= Amount;
                                            SendPacket(General.MyPackets.Vital(MyChar.UID, 10, MyChar.WHSilvers));
                                            SendPacket(General.MyPackets.Vital(MyChar.UID, 4, MyChar.Silvers));
                                        }

                                        break;
                                    }
                                case 20:
                                    {
                                        MyChar.Ready = false;
                                        uint UppedItemUID = (uint)((Data[7] << 24) + (Data[6] << 16) + (Data[5] << 8) + Data[4]);
                                        uint UppingItemUID = (uint)((Data[11] << 24) + (Data[10] << 16) + (Data[9] << 8) + Data[8]);

                                        string UppedItem = "";
                                        string UppingItem = "";
                                        int Counter = 0;

                                        foreach (uint uid in MyChar.Inventory_UIDs)
                                        {
                                            if (uid == UppedItemUID)
                                                UppedItem = MyChar.Inventory[Counter];

                                            Counter++;
                                        }

                                        Counter = 0;

                                        foreach (uint uid in MyChar.Inventory_UIDs)
                                        {
                                            if (uid == UppingItemUID)
                                                UppingItem = MyChar.Inventory[Counter];

                                            Counter++;
                                        }

                                        string[] Splitter = UppedItem.Split('-');
                                        uint UppedItem2 = uint.Parse(Splitter[0]);
                                        string[] Splitter2 = UppingItem.Split('-');
                                        uint UppingItem2 = uint.Parse(Splitter2[0]);

                                        if (UppingItem2 == 1088001)
                                            if (Other.Upgradable(UppedItem2))
                                                if (Other.ItemInfo(UppedItem2)[3] < 130)
                                                    if (Other.ItemType2(UppedItem2) == 11 && Other.WeaponType(UppedItem2) != 117 && Other.ItemInfo(UppedItem2)[3] < 120 || Other.WeaponType(UppedItem2) == 117 && Other.ItemInfo(UppedItem2)[3] < 112 || Other.ItemType2(UppedItem2) == 13 && Other.ItemInfo(UppedItem2)[3] < 120 || Other.ItemType2(UppedItem2) == 15 && Other.ItemInfo(UppedItem2)[3] < 127 || Other.ItemType2(UppedItem2) == 16 && Other.ItemInfo(UppedItem2)[3] < 129 || Other.ItemType(UppedItem2) == 4 || Other.ItemType(UppedItem2) == 5 || Other.ItemType2(UppedItem2) == 12 || Other.WeaponType(UppedItem2) == 132 && Other.ItemInfo(UppedItem2)[3] <= 12)
                                                    {
                                                        bool Success = false;
                                                        double RemoveChance = 0;

                                                        RemoveChance = Other.ItemInfo(UppedItem2)[3] / 3;

                                                        if (Other.ItemQuality(UppedItem2) == 3 || Other.ItemQuality(UppedItem2) == 4 || Other.ItemQuality(UppedItem2) == 5)
                                                            if (Other.ChanceSuccess(90 - RemoveChance))
                                                                Success = true;

                                                        if (Other.ItemQuality(UppedItem2) == 6)
                                                            if (Other.ChanceSuccess(75 - RemoveChance))
                                                                Success = true;

                                                        if (Other.ItemQuality(UppedItem2) == 7)
                                                            if (Other.ChanceSuccess(60 - RemoveChance))
                                                                Success = true;

                                                        if (Other.ItemQuality(UppedItem2) == 8)
                                                            if (Other.ChanceSuccess(50 - RemoveChance))
                                                                Success = true;

                                                        if (Other.ItemQuality(UppedItem2) == 9)
                                                            if (Other.ChanceSuccess(45 - RemoveChance))
                                                                Success = true;

                                                        if (Success)
                                                        {
                                                            MyChar.RemoveItem((ulong)UppedItemUID);

                                                            UppedItem2 = Other.EquipNextLevel(UppedItem2);

                                                            if (Splitter[4] == "0")
                                                                if (Other.ChanceSuccess(0.5))
                                                                    Splitter[4] = "255";

                                                            if (Splitter[5] == "0")
                                                                if (Splitter[4] != "0")
                                                                    if (Other.ChanceSuccess(0.25))
                                                                        Splitter[5] = "255";

                                                            MyChar.AddItem(Convert.ToString(UppedItem2) + "-" + Splitter[1] + "-" + Splitter[2] + "-" + Splitter[3] + "-" + Splitter[4] + "-" + Splitter[5], 0, UppedItemUID);
                                                        }

                                                        MyChar.RemoveItem((ulong)UppingItemUID);
                                                    }
                                        MyChar.Ready = true;
                                        break;
                                    }
                                case 19:
                                    {
                                        MyChar.Ready = false;
                                        uint UppedItemUID = (uint)((Data[7] << 24) + (Data[6] << 16) + (Data[5] << 8) + Data[4]);
                                        uint UppingItemUID = (uint)((Data[11] << 24) + (Data[10] << 16) + (Data[9] << 8) + Data[8]);

                                        string UppedItem = "";
                                        string UppingItem = "";
                                        int Counter = 0;

                                        foreach (uint uid in MyChar.Inventory_UIDs)
                                        {
                                            if (uid == UppedItemUID)
                                                UppedItem = MyChar.Inventory[Counter];

                                            Counter++;
                                        }

                                        Counter = 0;

                                        foreach (uint uid in MyChar.Inventory_UIDs)
                                        {
                                            if (uid == UppingItemUID)
                                                UppingItem = MyChar.Inventory[Counter];

                                            Counter++;
                                        }

                                        string[] Splitter = UppedItem.Split('-');
                                        uint UppedItem2 = uint.Parse(Splitter[0]);
                                        string[] Splitter2 = UppingItem.Split('-');
                                        uint UppingItem2 = uint.Parse(Splitter2[0]);

                                        if (UppingItem2 == 1088000)
                                            if (Other.Upgradable(UppedItem2))
                                                if (Other.ItemQuality(UppedItem2) != 9)
                                                {
                                                    bool Success = false;
                                                    double RemoveChance = 0;

                                                    RemoveChance = Other.ItemInfo(UppedItem2)[3] / 3;

                                                    if (Other.ItemQuality(UppedItem2) == 3 || Other.ItemQuality(UppedItem2) == 4 || Other.ItemQuality(UppedItem2) == 5)
                                                        if (Other.ChanceSuccess(64 - RemoveChance))
                                                            Success = true;

                                                    if (Other.ItemQuality(UppedItem2) == 6)
                                                        if (Other.ChanceSuccess(54 - RemoveChance))
                                                            Success = true;

                                                    if (Other.ItemQuality(UppedItem2) == 7)
                                                        if (Other.ChanceSuccess(48 - RemoveChance))
                                                            Success = true;

                                                    if (Other.ItemQuality(UppedItem2) == 8)
                                                        if (Other.ChanceSuccess(44 - RemoveChance))
                                                            Success = true;

                                                    if (Success)
                                                    {
                                                        if (Other.ItemQuality(UppedItem2) == 3 || Other.ItemQuality(UppedItem2) == 4)
                                                            UppedItem2 += 6 - (uint)Other.ItemQuality(UppedItem2);
                                                        else
                                                            UppedItem2 += 1;

                                                        MyChar.RemoveItem((ulong)UppedItemUID);

                                                        if (Splitter[4] == "0")
                                                            if (Other.ChanceSuccess(1))
                                                                Splitter[4] = "255";

                                                        if (Splitter[5] == "0")
                                                            if (Splitter[4] != "0")
                                                                if (Other.ChanceSuccess(0.5))
                                                                    Splitter[5] = "255";

                                                        MyChar.AddItem(Convert.ToString(UppedItem2) + "-" + Splitter[1] + "-" + Splitter[2] + "-" + Splitter[3] + "-" + Splitter[4] + "-" + Splitter[5], 0, UppedItemUID);
                                                    }

                                                    MyChar.RemoveItem((uint)UppingItemUID);
                                                }
                                        MyChar.Ready = true;
                                        break;
                                    }
                                case 27:
                                    {
                                        SendPacket(Data);
                                        break;
                                    }
                                case 28:
                                    {
                                        int TheItemUID = (Data[0x07] << 24) + (Data[0x06] << 16) + (Data[0x05] << 8) + Data[0x04];
                                        int GemUID = (Data[11] << 24) + (Data[10] << 16) + (Data[9] << 8) + Data[8];
                                        string MainItem = MyChar.FindItem((uint)TheItemUID);
                                        string Minor1Item = MyChar.FindItem((uint)GemUID);

                                        if (MainItem == null || Minor1Item == null)
                                            return;

                                        uint MainItemId = 0;
                                        uint Minor1ItemId = 0;

                                        string[] Splitter;
                                        string[] MainItemE;
                                        MainItemE = MainItem.Split('-');
                                        MainItemId = uint.Parse(MainItemE[0]);
                                        Splitter = Minor1Item.Split('-');
                                        Minor1ItemId = uint.Parse(Splitter[0]);

                                        if (Minor1ItemId <= 700073 && Minor1ItemId >= 700001)
                                        {
                                            Gems GemID = (Gems)Minor1ItemId - 700000;

                                            if (DataBase.Enchant.ContainsKey(GemID))
                                            {
                                                EnchantData E = DataBase.Enchant[GemID];

                                                uint EnchantHP = (uint)General.Rand.Next(E.Min, E.Max);
                                                if (EnchantHP >= uint.Parse(MainItemE[3]))
                                                {
                                                    MyChar.RemoveItem((uint)GemUID);
                                                    MyChar.RemoveItem((uint)TheItemUID);
                                                    MyChar.AddItem(MainItemE[0] + "-" + MainItemE[1] + "-" + MainItemE[2] + "-" + EnchantHP + "-" + MainItemE[4] + "-" + MainItemE[5], 0, (uint)TheItemUID);
                                                    SendPacket(General.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "You were so lucky, u just got " + EnchantHP + ".", 2000));
                                                    DataBase.SaveChar(MyChar);
                                                    return;
                                                }
                                                else
                                                {
                                                    MyChar.RemoveItem((uint)GemUID);
                                                    SendPacket(General.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "You were so unlucky, u got only " + EnchantHP + " and it could not be changed with the odl one.", 2000));
                                                    DataBase.SaveChar(MyChar);
                                                    return;
                                                }
                                            }
                                        }
                                        return;
                                    }
                                case 3:
                                    {
                                        MyChar.Ready = false;
                                        uint ItemUID = (uint)((Data[7] << 24) + (Data[6] << 16) + (Data[5] << 8) + Data[4]);

                                        int Count = 0;

                                        foreach (uint uid in MyChar.Inventory_UIDs)
                                        {
                                            if (uid == ItemUID)
                                            {
                                                string Item = MyChar.Inventory[Count];

                                                DroppedItem e = DroppedItems.DropItem(Item, (uint)(MyChar.LocX - General.Rand.Next(4) + General.Rand.Next(4)), (uint)(MyChar.LocY - General.Rand.Next(4) + General.Rand.Next(4)), (uint)MyChar.LocMap, 0);
                                                World.ItemDrops(e);

                                                MyChar.RemoveItem(ItemUID);
                                            }
                                            Count++;
                                        }
                                        MyChar.Ready = true;
                                        break;
                                    }
                                case 2:
                                    {
                                        MyChar.Ready = false;
                                        uint ItemUID = (uint)((Data[11] << 24) + (Data[10] << 16) + (Data[9] << 8) + Data[8]);
                                        int Count = 0;
                                        foreach (uint uid in MyChar.Inventory_UIDs)
                                        {
                                            if (uid == ItemUID)
                                            {
                                                string Item = MyChar.Inventory[Count];
                                                string[] Splitter = Item.Split('-');
                                                uint ItemId = uint.Parse(Splitter[0]);

                                                foreach (uint[] item in DataBase.Items)
                                                {
                                                    if (item[0] == ItemId)
                                                    {
                                                        uint SellFor = item[7] / 3;

                                                        MyChar.RemoveItem(ItemUID);
                                                        MyChar.Silvers += SellFor;

                                                        SendPacket(General.MyPackets.Vital(MyChar.UID, 4, MyChar.Silvers));
                                                        break;
                                                    }
                                                }
                                                break;
                                            }
                                            Count++;
                                        }
                                        MyChar.Ready = true;
                                        break;
                                    }
                                case 1:
                                    {
                                        MyChar.Ready = false;
                                        uint ItemID = (uint)((Data[0x0b] << 24) + (Data[0x0a] << 16) + (Data[0x09] << 8) + Data[0x08]);
                                        uint CPsVal = Data[18];
                                        uint Value = (uint)((Data[0x07] << 24) + (Data[0x06] << 16) + (Data[0x05] << 8) + Data[0x04]);
                                        byte Amount = Data[20];
                                        int Money = Data[5];
                                        if (Amount == 0)
                                            Amount = 1;

                                        string TehShop = System.IO.File.ReadAllText(System.Windows.Forms.Application.StartupPath + @"\Shop.dat");

                                        try
                                        {
                                            if (Other.CharExist(Convert.ToString(ItemID), TehShop))
                                            {
                                                foreach (uint[] item in DataBase.Items)
                                                {
                                                    if (ItemID == item[0])
                                                    {
                                                        Value = item[7];
                                                        CPsVal = item[15];
                                                    }
                                                }
                                                for (int i = 0; i < Amount; i++)
                                                {
                                                    if (MyChar.ItemsInInventory > 39)
                                                        return;
                                                    if (MyChar.Silvers >= Value && CPsVal == 0 || MyChar.CPs > CPsVal && CPsVal != 0)
                                                    {
                                                        if (CPsVal == 0)
                                                            MyChar.Silvers -= Value;
                                                        if (CPsVal > 0)
                                                            MyChar.CPs -= CPsVal;

                                                        if (MyChar.Silvers < 0)
                                                            MyChar.Silvers = 0;
                                                        if (MyChar.CPs < 0)
                                                            MyChar.CPs = 0;

                                                        byte WithPlus = 0;
                                                        if (ItemID == 730001)
                                                            WithPlus = 1;														
														if (ItemID == 730002)
                                                            WithPlus = 2;
                                                        if (ItemID == 730003)
                                                            WithPlus = 3;
                                                        if (ItemID == 730004)
                                                            WithPlus = 4;
                                                        if (ItemID == 730005)
                                                            WithPlus = 5;
                                                        if (ItemID == 730006)
                                                            WithPlus = 6;
                                                        if (ItemID == 730007)
                                                            WithPlus = 7;
                                                        if (ItemID == 730008)
                                                            WithPlus = 8;															

                                                        MyChar.AddItem(Convert.ToString(ItemID) + "-" + WithPlus + "-0-0-0-0", 0, (uint)General.Rand.Next(10000000));
                                                        SendPacket(General.MyPackets.Vital(MyChar.UID, 4, MyChar.Silvers));
                                                        SendPacket(General.MyPackets.Vital(MyChar.UID, 30, MyChar.CPs));
                                                    }
                                                    else
                                                    {
                                                        SendPacket(General.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "Vous n'avez pas " + Value + " argents ou " + CPsVal + " CPs.", 2005));
                                                        break;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                General.WriteLine("There is no such item in Shop.dat(" + ItemID + ")");
                                            }
                                        }
                                        catch (Exception Exc) { General.WriteLine(Convert.ToString(Exc)); }
                                        MyChar.Ready = true;
                                        break;
                                    }
                                case 4:
                                    {                                        
                                        MyChar.Ready = false;
                                        ulong ItemUID = (ulong)((Data[7] << 24) + (Data[6] << 16) + (Data[5] << 8) + Data[4]);
                                        byte RequestPos = Data[8];

                                        string TheItem = "";
                                        uint TheUID = 0;
                                        int count = 0;
                                        uint ItemID = 0;

                                        foreach (uint uid in MyChar.Inventory_UIDs)
                                        {
                                            if (ItemUID == uid)
                                            {
                                                TheUID = uid;
                                                TheItem = MyChar.Inventory[count];
                                                string[] Splitter = TheItem.Split('-');
                                                ItemID = uint.Parse(Splitter[0]);
                                            }

                                            count++;
                                        }
                                        if (ItemID == 1050000 || ItemID == 1050001 || ItemID == 1050002)
                                            RequestPos = 5;
                                        if (RequestPos != 0)
                                        {
                                            if (Other.CanEquip(TheItem, MyChar))
                                            {
                                                if (Other.ItemType(ItemID) != 5 && RequestPos != 0)
                                                {
                                                    MyChar.RemoveItem(TheUID);
                                                    if (MyChar.Equips[RequestPos] == null || MyChar.Equips[RequestPos] == "0")
                                                    {
                                                        MyChar.AddItem(TheItem, RequestPos, TheUID);
                                                    }
                                                    else
                                                    {
                                                        MyChar.UnEquip(RequestPos);
                                                        MyChar.AddItem(TheItem, RequestPos, TheUID);
                                                    }
                                                    World.UpdateSpawn(MyChar);                                                    
                                                }
                                                else if (RequestPos != 0)
                                                    if (MyChar.ItemsInInventory < 40)
                                                    {
                                                        if (MyChar.Equips[4] == null && MyChar.Equips[5] == null)
                                                        {
                                                            MyChar.AddItem(TheItem, RequestPos, TheUID);
                                                            World.UpdateSpawn(MyChar);
                                                            MyChar.RemoveItem(TheUID);
                                                        }
                                                        else
                                                        {
                                                            MyChar.RemoveItem(TheUID);
                                                            if (MyChar.Equips[4] != null)
                                                            {
                                                                MyChar.UnEquip(4);
                                                            }
                                                            if (MyChar.Equips[5] != null)
                                                            {
                                                                MyChar.UnEquip(5);
                                                            }
                                                            MyChar.AddItem(TheItem, 4, TheUID);

                                                            World.UpdateSpawn(MyChar);
                                                        }
                                                    }

                                            }
                                        }
                                        else
                                        {
                                            MyChar.UseItem(TheUID, TheItem);
                                        }
                                        MyChar.Ready = true;
                                        break;
                                    }
                                case 6:
                                    {
                                        MyChar.Ready = false;
                                        if (MyChar.ItemsInInventory > 39)
                                            return;

                                        ulong ItemUID = (ulong)((Data[7] << 24) + (Data[6] << 16) + (Data[5] << 8) + Data[4]);

                                        int count = 0;
                                        foreach (ulong uid in MyChar.Equips_UIDs)
                                        {
                                            if (uid == ItemUID)
                                            {
                                                MyChar.UnEquip((byte)count);
                                            }
                                            count++;
                                        }

                                        World.UpdateSpawn(MyChar);

                                        MyChar.Ready = true;
                                        break;
                                    }
                            }
                            break;
                        }
                    case 1010:
                        {
                            PacketType = Data[22];

                            switch (PacketType)
                            {
                                case 111: //Open Shop by junior
                                    {
                                        OpenShop op = new OpenShop(Data, this);
                                        break;
                                    }
                                case 114: //shop and map infos
                                    {
                                        if (MyChar.ShopID != 0)
                                        {
                                            if (World.ShopHash.Contains(MyChar.ShopID))
                                            { World.ShopHash.Remove(MyChar.ShopID); }

                                            //World.SendLocal(Client, PacketBuilder.RemoveShop(Client.Char.ShopID, 2));
                                            MyChar.ShopID = 0;
                                        }
                                        break;
                                    }
                                case 120:
                                    {
                                        if (MyChar.Flying)
                                        {
                                            MyChar.Flying = false;
                                            SendPacket(General.MyPackets.Vital(MyChar.UID, 26, MyChar.GetStat()));
                                        }
                                        break;
                                    }
                                case 99:
                                    {
                                        if (MyChar.LocMap == 1028)
                                            MyChar.Mining = true;
                                        break;
                                    }
                                case 54:
                                    {
                                        uint VUID = (uint)((Data[15] << 24) + (Data[14] << 16) + (Data[13] << 8) + Data[12]);
                                        Character ViewedChar = (Character)World.AllChars[VUID];
                                        break;
                                    }
                                case 140:
                                    {
                                        uint UID = BitConverter.ToUInt32(Data, 12);
                                        Character Char = (Character)World.AllChars[UID];
                                        if (Char != null)
                                            SendPacket(General.MyPackets.FriendEnemyInfoPacket(Char, 0));
                                        break;
                                    }
                                case 94:
                                    {
                                        MyChar.Revive(true);
                                        break;
                                    }
                                case 117:
                                    {
                                        MyChar.Ready = false;
                                        int Value1 = (Data[7] << 24) + (Data[6] << 16) + (Data[5] << 8) + Data[4];
                                        uint CharID = (uint)((Data[11] << 24) + (Data[10] << 16) + (Data[9] << 8) + Data[8]);
                                        uint VUID = (uint)((Data[15] << 24) + (Data[14] << 16) + (Data[13] << 8) + Data[12]);

                                        Character ViewedChar = (Character)World.AllChars[VUID];
                                        string[] Splitter;
                                        SendPacket(General.MyPackets.ViewEquip(ViewedChar));
                                        ViewedChar.MyClient.SendPacket(General.MyPackets.SendMsg(ViewedChar.MyClient.MessageId, "SYSTEM", ViewedChar.Name, MyChar.Name + " est en train de regarder votre �quipement.", 2005));

                                        if (ViewedChar.Equips[1] != null && ViewedChar.Equips[1] != "0")
                                        {
                                            Splitter = ViewedChar.Equips[1].Split('-');
                                            SendPacket(General.MyPackets.ViewEquipAdd(VUID, uint.Parse(Splitter[0]), byte.Parse(Splitter[1]), byte.Parse(Splitter[2]), byte.Parse(Splitter[3]), byte.Parse(Splitter[4]), byte.Parse(Splitter[5]), 1, 100, 100));
                                        }
                                        else
                                            SendPacket(General.MyPackets.ViewEquipAdd(VUID, 0, 0, 0, 0, 0, 0, 1, 0, 0));

                                        if (ViewedChar.Equips[2] != null && ViewedChar.Equips[2] != "0")
                                        {
                                            Splitter = ViewedChar.Equips[2].Split('-');
                                            SendPacket(General.MyPackets.ViewEquipAdd(VUID, uint.Parse(Splitter[0]), byte.Parse(Splitter[1]), byte.Parse(Splitter[2]), byte.Parse(Splitter[3]), byte.Parse(Splitter[4]), byte.Parse(Splitter[5]), 2, 100, 100));
                                        }
                                        else
                                            SendPacket(General.MyPackets.ViewEquipAdd(VUID, 0, 0, 0, 0, 0, 0, 2, 0, 0));

                                        if (ViewedChar.Equips[3] != null && ViewedChar.Equips[3] != "0")
                                        {
                                            Splitter = ViewedChar.Equips[3].Split('-');
                                            SendPacket(General.MyPackets.ViewEquipAdd(VUID, uint.Parse(Splitter[0]), byte.Parse(Splitter[1]), byte.Parse(Splitter[2]), byte.Parse(Splitter[3]), byte.Parse(Splitter[4]), byte.Parse(Splitter[5]), 3, 100, 100));
                                        }
                                        else
                                            SendPacket(General.MyPackets.ViewEquipAdd(VUID, 0, 0, 0, 0, 0, 0, 3, 0, 0));

                                        if (ViewedChar.Equips[4] != null && ViewedChar.Equips[4] != "0")
                                        {
                                            Splitter = ViewedChar.Equips[4].Split('-');
                                            SendPacket(General.MyPackets.ViewEquipAdd(VUID, uint.Parse(Splitter[0]), byte.Parse(Splitter[1]), byte.Parse(Splitter[2]), byte.Parse(Splitter[3]), byte.Parse(Splitter[4]), byte.Parse(Splitter[5]), 4, 100, 100));
                                        }
                                        else
                                            SendPacket(General.MyPackets.ViewEquipAdd(VUID, 0, 0, 0, 0, 0, 0, 4, 0, 0));
                                        if (ViewedChar.Equips[5] != null && ViewedChar.Equips[5] != "0")
                                        {
                                            Splitter = ViewedChar.Equips[5].Split('-');
                                            SendPacket(General.MyPackets.ViewEquipAdd(VUID, uint.Parse(Splitter[0]), byte.Parse(Splitter[1]), byte.Parse(Splitter[2]), byte.Parse(Splitter[3]), byte.Parse(Splitter[4]), byte.Parse(Splitter[5]), 5, 100, 100));
                                        }
                                        else
                                            SendPacket(General.MyPackets.ViewEquipAdd(VUID, 0, 0, 0, 0, 0, 0, 5, 0, 0));

                                        if (ViewedChar.Equips[6] != null && ViewedChar.Equips[6] != "0")
                                        {
                                            Splitter = ViewedChar.Equips[6].Split('-');
                                            SendPacket(General.MyPackets.ViewEquipAdd(VUID, uint.Parse(Splitter[0]), byte.Parse(Splitter[1]), byte.Parse(Splitter[2]), byte.Parse(Splitter[3]), byte.Parse(Splitter[4]), byte.Parse(Splitter[5]), 6, 100, 100));
                                        }
                                        else
                                            SendPacket(General.MyPackets.ViewEquipAdd(VUID, 0, 0, 0, 0, 0, 0, 6, 0, 0));

                                        if (ViewedChar.Equips[7] != null && ViewedChar.Equips[7] != "0")
                                        {
                                            Splitter = ViewedChar.Equips[7].Split('-');
                                            SendPacket(General.MyPackets.ViewEquipAdd(VUID, uint.Parse(Splitter[0]), byte.Parse(Splitter[1]), byte.Parse(Splitter[2]), byte.Parse(Splitter[3]), byte.Parse(Splitter[4]), byte.Parse(Splitter[5]), 7, 100, 100));
                                        }
                                        else
                                            SendPacket(General.MyPackets.ViewEquipAdd(VUID, 0, 0, 0, 0, 0, 0, 7, 0, 0));

                                        if (ViewedChar.Equips[8] != null && ViewedChar.Equips[8] != "0")
                                        {
                                            Splitter = ViewedChar.Equips[8].Split('-');
                                            SendPacket(General.MyPackets.ViewEquipAdd(VUID, uint.Parse(Splitter[0]), byte.Parse(Splitter[1]), byte.Parse(Splitter[2]), byte.Parse(Splitter[3]), byte.Parse(Splitter[4]), byte.Parse(Splitter[5]), 8, 100, 100));
                                        }
                                        else
                                            SendPacket(General.MyPackets.ViewEquipAdd(VUID, 0, 0, 0, 0, 0, 0, 8, 0, 0));

                                        if (ViewedChar.Equips[9] != null && ViewedChar.Equips[9] != "0")
                                        {
                                            Splitter = ViewedChar.Equips[9].Split('-');
                                            SendPacket(General.MyPackets.ViewEquipAdd(VUID, uint.Parse(Splitter[0]), byte.Parse(Splitter[1]), byte.Parse(Splitter[2]), byte.Parse(Splitter[3]), byte.Parse(Splitter[4]), byte.Parse(Splitter[5]), 9, 100, 100));
                                        }
                                        else
                                            SendPacket(General.MyPackets.ViewEquipAdd(VUID, 0, 0, 0, 0, 0, 0, 9, 0, 0));


                                        MyChar.Ready = true;
                                        break;
                                    }
                                case 142:
                                    {
                                        MyChar.Ready = false;
                                        SendPacket(Data);
                                        uint Face = Data[12];

                                        if (MyChar.Model == 1003 || MyChar.Model == 1004)
                                        {
                                            if (Face > 200)
                                                Face -= 200;
                                        }

                                        if (MyChar.Model == 2001 || MyChar.Model == 2002)
                                        {
                                            if (Face > 200)
                                                Face += 0;
                                        }

                                        uint Multiply = (uint)(Data[13] * 56);
                                        Face += Multiply;

                                        MyChar.Avatar = (byte)Face;
                                        MyChar.Silvers -= 500;

                                        SendPacket(General.MyPackets.Vital(MyChar.UID, 4, MyChar.Silvers));

                                        World.UpdateSpawn(MyChar);
                                        MyChar.Ready = true;
                                        break;
                                    }
                                case 133:
                                    {
                                        if (MyChar.Mining)
                                            MyChar.Mining = false;

                                        SendPacket(Data);
                                        short PrevX = (short)((Data[0x11] << 8) + Data[0x10]);
                                        short PrevY = (short)((Data[0x13] << 8) + Data[0x12]);
                                        short NewX = (short)((Data[0xd] << 8) + Data[0xc]);
                                        short NewY = (short)((Data[0xf] << 8) + Data[0xe]);

                                        MyChar.Attacking = false;
                                        MyChar.TargetUID = 0;
                                        MyChar.MobTarget = null;
                                        MyChar.TGTarget = null;
                                        MyChar.PTarget = null;
                                        MyChar.SkillLooping = 0;
                                        MyChar.AtkType = 0;
                                        MyChar.PrevX = MyChar.LocX;
                                        MyChar.PrevY = MyChar.LocY;
                                        MyChar.LocX = (ushort)NewX;
                                        MyChar.LocY = (ushort)NewY;
                                        MyChar.Action = 100;

                                        World.SpawnMeToOthers(MyChar, true);
                                        World.SpawnOthersToMe(MyChar, true);
                                        World.PlayerMoves(MyChar, Data);
                                        World.SurroundNPCs(MyChar, true);
                                        World.SurroundMobs(MyChar, true);
                                        World.SurroundDroppedItems(MyChar, true);

                                        break;
                                    }
                                case 74:
                                    {
                                        if (There)
                                            return;
                                        if (MyChar == null)
                                            return;

                                        SendPacket(General.MyPackets.PlacePacket1(MyChar));
                                        SendPacket(General.MyPackets.PlacePacket2(MyChar));
                                        SendPacket(General.MyPackets.PlacePacket3(MyChar));
                                        SendPacket(General.MyPackets.LogonPacket());
                                        SendPacket(General.MyPackets.ShowMinimap(true));
                                        SendPacket(General.MyPackets.GeneralData((long)(MyChar.UID), 3, 0, 0, 96));

                                        SendPacket(General.MyPackets.Vital(MyChar.UID, 26, MyChar.GetStat()));
                                        MyChar.StartXPCircle();

                                        MyChar.PKMode = 3;

                                        World.SpawnMeToOthers(MyChar, false);
                                        World.SpawnOthersToMe(MyChar, false);
                                        World.SurroundNPCs(MyChar, false);
                                        World.SurroundMobs(MyChar, false);
                                        World.SurroundDroppedItems(MyChar, false);
                                        MyChar.UnPackInventory();
                                        MyChar.SendInventory();
                                        MyChar.UnPackEquips();
                                        MyChar.SendEquips(true);
                                        MyChar.UnPackSkills();
                                        MyChar.SendSkills();
                                        MyChar.UnPackProfs();
                                        MyChar.SendProfs();
                                        MyChar.UnPackWarehouses();
                                        MyChar.UnPackEnemies();
                                        MyChar.UnPackFriends();
                                        MyChar.KO = 0;
                                        Status = DataBase.GetStatus(Account);

                                        foreach (DictionaryEntry DE in Guilds.AllGuilds)
                                        {
                                            Guild AGuild = (Guild)DE.Value;
                                            SendPacket(General.MyPackets.GuildName(AGuild.GuildID, AGuild.GuildName));
                                        }
                                        if (MyChar.MyGuild != null)
                                        {
                                            SendPacket(General.MyPackets.GuildInfo(MyChar.MyGuild, MyChar));
                                            SendPacket(General.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, MyChar.MyGuild.Bulletin, 2111));
                                            SendPacket(General.MyPackets.GuildName(MyChar.GuildID, MyChar.MyGuild.GuildName));
                                            SendPacket(General.MyPackets.GeneralData(MyChar.UID, 0, 0, 0, 97));
                                        }

                                        if (MyChar.RBCount == 2)
                                        {
                                            foreach (DictionaryEntry DE in World.AllChars)
                                            {
                                                Character Chaar = (Character)DE.Value;
                                                if (Chaar.Name != MyChar.Name)
                                                {
                                                    Chaar.MyClient.SendPacket(General.MyPackets.String(MyChar.UID, 10, "2NDMetempsychosis"));
                                                }
                                            }
                                        }

                                        if (General.LoginOn == false && Status != 8)
                                        {
                                            SendPacket(General.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "Le serveur de login est ferm� pour des tests!", 2101));
                                            Drop();
                                        }

                                        if (World.AllChars.Count > 100)
                                        {
                                            SendPacket(General.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "Le serveur est plein!", 2101));
                                            Drop();
                                        }
                                        SendPacket(General.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "Bienvenu sur le serveur COPS v3 Beta 2.5", 2000));
                                        SendPacket(General.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "Veuillez reporter tous les bugs que vous trouverez.", 2000));
                                        SendPacket(General.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "Donnez votre avis sur le serveur sur le forum.", 2000));
                                        SendPacket(General.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "Exp Rate: " + DataBase.ExpRate + "x", 2000));
                                        SendPacket(General.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "Joueurs en ligne: "
                                            + World.AllChars.Count, 2005));
                                        SendPacket(General.MyPackets.GeneralData(MyChar.UID, 0, 0, 0, 75));
                                        MyChar.RemoveAttr();
                                        There = true;
                                        World.SpawnMeToOthers(MyChar, true);

                                        if (MyChar.WhichBless > 0)
                                        {
                                            SendPacket(General.MyPackets.SendMsg(MessageId, "SYSTEM", MyChar.Name, "La b�n�diction finira le: " + MyChar.HBEnd2.ToString(), 2011));
                                            MyChar.Blessed = true;
                                            MyChar.HBEnd = DateTime.Parse(MyChar.HBEnd2);
                                            SendPacket(General.MyPackets.String(MyChar.UID, 10, "zf2-e128"));
                                            if (MyChar.WhichBless == 1)
                                                SendPacket(General.MyPackets.Vital(MyChar.UID, 18, (3 * 24 * 60 * 60)));
                                            if (MyChar.WhichBless == 2)
                                                SendPacket(General.MyPackets.Vital(MyChar.UID, 18, (7 * 24 * 60 * 60)));
                                            if (MyChar.WhichBless == 3)
                                                SendPacket(General.MyPackets.Vital(MyChar.UID, 18, (30 * 24 * 60 * 60)));
                                            SendPacket(General.MyPackets.Vital(MyChar.UID, 26, MyChar.GetStat()));
                                            World.UpdateSpawn(MyChar);
                                        }

                                        break;
                                    }
                                case 96:
                                    {
                                        MyChar.Ready = false;
                                        SendPacket(General.MyPackets.GeneralData((long)(MyChar.UID), Data[12], 0, 0, 96));
                                        MyChar.PKMode = Data[12];
                                        MyChar.Ready = true;
                                        break;
                                    }
                                case 79:
                                    {
                                        MyChar.Ready = false;
                                        MyChar.Direction = Data[20];
                                        World.PlayerMoves(MyChar, Data);
                                        MyChar.Ready = true;
                                        break;
                                    }
                                case 81:
                                    {
                                        MyChar.Ready = false;
                                        MyChar.Action = Data[12];
                                        World.PlayerMoves(MyChar, Data);
                                        MyChar.Ready = true;
                                        break;
                                    }
                                case 85:
                                    {
                                        MyChar.Ready = false;
                                        ulong CharID = (ulong)((Data[11] << 24) + (Data[10] << 16) + (Data[9] << 8) + (Data[8]));
                                        int x = (Data[13] << 8) + (Data[12]);
                                        int y = (Data[15] << 8) + (Data[14]);

                                        if (MyChar.UID == CharID)
                                            MyChar.UsePortal();
                                        MyChar.Ready = true;
                                        break;
                                    }
                            }

                            break;
                        }

                    case 1001:
                        {
                            uint Model = 0;
                            uint Avatar = 0;
                            byte Class = 0;
                            string CharName = "";
                            bool ValidName = true;

                            Model = (uint)Data[0x35];
                            Model = (Model << 8) | (uint)(Data[0x34]);

                            Class = Data[0x36];

                            int x = 0x14;
                            while (x < 0x24 && Data[x] != 0x00)
                            {
                                CharName += Convert.ToChar(Data[x]);
                                x++;
                            }
                            if (Model == 1003)
                                Avatar = 67;
                            else if (Model == 1004)
                                Avatar = 67;
                            else if (Model == 2001)
                                Avatar = 201;
                            else if (Model == 2002)
                                Avatar = 201;

                            if (CharName.IndexOfAny(new char[3] { ' ', '[', ']' }) > -1)
                            {
                                ValidName = false;
                            }

                            foreach (string name in DataBase.ForbiddenNames)
                            {
                                if (name == CharName)
                                {
                                    ValidName = false;
                                    break;
                                }
                            }

                            try
                            {
                                if (ValidName)
                                {
                                    bool Success = DataBase.CreateCharacter(CharName, Class, Model, Avatar, this);
                                    if (Success)
                                        Console.WriteLine("New character: " + CharName);
                                    Online = false;
                                    MySocket.Disconnect();
                                }
                                else
                                {
                                    Online = false;
                                    MySocket.Disconnect();
                                }
                            }
                            catch (Exception Exc) { General.WriteLine(Exc.ToString()); }


                            break;
                        }
                    case 2044:
                        {

                            CurrentNPC = 6183;
                            SendPacket(General.MyPackets.NPCSay("Vous �tes b�ni. Voulez-vous que je vous envoie � l'OfflineTG?"));
                            SendPacket(General.MyPackets.NPCLink("Oui", 1));
                            SendPacket(General.MyPackets.NPCLink("Non merci.", 255));
                            SendPacket(General.MyPackets.NPCSetFace(30));
                            SendPacket(General.MyPackets.NPCFinish());

                            break;
                        } 
                }
            }
            catch (Exception Exc) { General.WriteLine(Convert.ToString(Exc)); }
        }


        public void SendPacket(byte[] Dat)
        {
            try
            {
                if (ListenSock != null)
                    if (ListenSock.WinSock.Connected)
                    {
                        int Len = Dat[0] + (Dat[1] << 8);
                        if (Dat.Length != Len)
                            return;

                        System.Threading.Monitor.TryEnter(this, new TimeSpan(0, 0, 0, 8, 0));

                        byte[] Data = new byte[Dat.Length];
                        Dat.CopyTo(Data, 0);

                        Crypto.Encrypt(ref Data);
                        ListenSock.WinSock.Send(Data);

                        System.Threading.Monitor.Exit(this);
                    }
            }
            catch { }
        }

        public void Drop()
        {
            try
            {
                if (Online)
                {
                    Online = false;
                    DataBase.ChangeOnlineStatus(Account, 0);

                    if (MyChar != null)
                    {
                        World.RemoveEntity(MyChar);
                        if (MyChar.TheTimer != null)
                        {
                            MyChar.TheTimer.Stop();
                            MyChar.TheTimer.Dispose();
                            MyChar.TheTimer = null;
                        }
                        if (MyChar.LocMap == 700)
                        {
                            MyChar.CPs += 27;
                            SendPacket(General.MyPackets.Vital(MyChar.UID, 30, MyChar.CPs));
                            MyChar.Teleport(1036, 200, 200);
                        }
                        if (MyChar.LocMap == 1036 || MyChar.LocMap == 1091 || MyChar.LocMap == 1081 || MyChar.LocMap == 1090 || MyChar.LocMap == 2021 || MyChar.LocMap == 2022 || MyChar.LocMap == 2023 || MyChar.LocMap == 2024 || MyChar.LocMap == 1039)
                        {
                            MyChar.Teleport(1002, 431, 379);
                        }
                        if (MyChar.LocMap == 1038)
                        {
                            if (MyChar.CurHP == 0)
                                MyChar.Teleport(1505, 155, 218);
                            else
                                MyChar.Teleport(1002, 431, 379);
                        }

                        foreach (DictionaryEntry DE in MyChar.Friends)
                        {
                            uint FriendID = (uint)DE.Key;
                            if (World.AllChars.Contains(FriendID))
                            {
                                Character Friend = (Character)World.AllChars[FriendID];
                                if (Friend != null)
                                {
                                    Friend.MyClient.SendPacket(General.MyPackets.FriendEnemyPacket(MyChar.UID, MyChar.Name, 14, 0));
                                    Friend.MyClient.SendPacket(General.MyPackets.FriendEnemyPacket(MyChar.UID, MyChar.Name, 15, 0));
                                    Friend.MyClient.SendPacket(General.MyPackets.SendMsg(Friend.MyClient.MessageId, "SYSTEM", Friend.Name, "Votre ami " + MyChar.Name + " vient de quitter le jeu.", 2005));
                                }
                            }
                        }
                        if (MyChar.Model == 223)
                        {
                            MyChar.Model = MyChar.RModel;
                        }
                        DataBase.SaveChar(MyChar);
                        MyChar.Trading = false;
                        MyChar.TradingSilvers = 0;
                        MyChar.TradingCPs = 0;
                        MyChar.TradeOK = false;
                        MyChar.Trading = false;
                        MyChar.AtkType = 0;
                        MyChar.JoinForbidden = false;
                        MyChar.SkillLoopingTarget = 0;
                        MyChar.SkillLoopingX = 0;
                        MyChar.SkillLoopingY = 0;
                        MyChar.SkillLooping = 0;
                        MyChar.SMOn = false;
                        MyChar.CycloneOn = false;
                        MyChar.CastingPray = false;
                        MyChar.Inventory = new string[41];
                        MyChar.Equips = new string[10];
                        MyChar.TCWH = new string[20];
                        MyChar.PCWH = new string[20];
                        MyChar.ACWH = new string[20];
                        MyChar.DCWH = new string[20];
                        MyChar.BIWH = new string[20];
                        MyChar.MAWH = new string[40];
                        MyChar.Skills.Clear();
                        MyChar.Skill_Exps.Clear();
                        MyChar.Profs.Clear();
                        MyChar.Prof_Exps.Clear();
                        MyChar.Friends.Clear();
                        MyChar.Enemies.Clear();
                        MyChar.KO = 0;
                        DataBase.SaveKO(MyChar);
                        DataBase.SaveDisKO(MyChar);

                        if (MyChar.Trading)
                        {
                            Character Who = (Character)World.AllChars[MyChar.TradingWith];
                            Who.MyClient.SendPacket(General.MyPackets.TradePacket(MyChar.TradingWith, 5));
                            Who.TradingSilvers = 0;
                            Who.TradingCPs = 0;
                            Who.TradeOK = false;
                            Who.Trading = false;
                            MyChar.TradingWith = 0;
                            Who.MyClient.SendPacket(General.MyPackets.SendMsg(Who.MyClient.MessageId, "SYSTEM", Who.Name, "�change rat�e!", 2005));
                        }

                        if (MyChar.TeamLeader)
                            MyChar.TeamDismiss();

                        if (MyChar.MyTeamLeader != null && !MyChar.TeamLeader)
                            MyChar.MyTeamLeader.TeamRemove(MyChar, false);
                        
                        try
                        {
                            if (World.AllChars.Contains(MyChar.UID))
                                World.AllChars.Remove(MyChar.UID);                          
                        }
                        catch (Exception Exc) { General.WriteLine(Exc.ToString()); }
                                                
                        MyChar = null;
                    }
                    ListenSock.WinSock.Close();                    
                    ListenSock = null;
                }
            }
            catch (Exception Exc) { General.WriteLine(Convert.ToString(Exc)); }
        }        
    }
}

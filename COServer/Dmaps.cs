﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.IO;

namespace COServer_Project
{
    public static class DMaps // Credits to Future (leavemealone)
    {
        public static Dictionary<Maps, DMAPClass> DMapz = new Dictionary<Maps, DMAPClass>();
        public static Int32 NumberMaps;

        public static void LoadDMapz(string GameMapFile)
        {
            DMapz.Clear();

            FileStream GameMap = new FileStream(GameMapFile, FileMode.Open);
            BinaryReader GameMapL = new BinaryReader(GameMap);

            NumberMaps = GameMapL.ReadInt32();

            Ini MapInf = new Ini(System.Windows.Forms.Application.StartupPath + @"\GameMap.ini");
            MapInf.WriteString("Info", "MapCount", Convert.ToString(NumberMaps));

            Console.WriteLine("Loading DMaps from GameMap.dat");
            Console.Write("__________________________________________________");

            double PercentPerMap = (double)100 / (double)NumberMaps;

            for (int M = 0; M < NumberMaps; M++)
            {
                Int32 MapID = GameMapL.ReadInt32();
                int MapNameLength = GameMapL.ReadInt32();
                string FilesName = Encoding.ASCII.GetString(GameMapL.ReadBytes(MapNameLength));

                DMAPClass DMapExtra = new DMAPClass(General.CODirectory + FilesName);

                DMapExtra.ReadMap();
                DMapz.Add((Maps)MapID, DMapExtra);

                GameMapL.ReadInt32();

                MapInf.WriteString(Convert.ToString(MapID), "Length", Convert.ToString(MapNameLength));
                MapInf.WriteString(Convert.ToString(MapID), "Path", FilesName);

                double Percent = (double)M * PercentPerMap;
                Percent /= 2;

                Console.Write("\r");

                for (int i = 0; i < 50; i++)
                {
                    if (i < Percent)
                    {
                        Console.BackgroundColor = ConsoleColor.Cyan;
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.BackgroundColor = ConsoleColor.Blue;
                    }

                    if (i < Percent)
                        Console.Write(" ");
                    else
                        Console.Write("_");
                }
            }
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("");
            Console.WriteLine("Loaded {0} DMaps.", NumberMaps);
        }
    }

    public class DMAPClass
    {
        string FileName;
        public DMAPPiece[,] DMapInfo;
        public UInt32 MapX;
        public UInt32 MapY;

        public DMAPClass(string File)
        {
            FileName = File;
        }

        public void ReadMap()
        {
            try
            {
                BinaryReader Binary = new BinaryReader(new FileStream(FileName, FileMode.Open));

                Binary.ReadBytes(268);

                MapX = Binary.ReadUInt32();
                MapY = Binary.ReadUInt32();

                DMapInfo = new DMAPPiece[MapX, MapY];
                byte[] Buffer2 = new byte[MapY * MapX];
                for (int y = 0; y < MapY; y++)
                {
                    for (int x = 0; x < MapX; x++)
                    {
                        Buffer2[y + x] = Binary.ReadByte();
                        Binary.ReadBytes(5);
                    }
                    Binary.ReadInt32();
                }
                Binary.Close();
            }
            catch
            {
                Console.WriteLine("Dmap not found!");
            }
        }

        public DMAPPiece Show(int x, int y)
        {
            return DMapInfo[x, y];
        }
    }
}

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Timers;
using System.Threading;

namespace COServer_Project
{
    public class Character
    {
        public Client MyClient;

        public uint UID = 0;
        public uint Model = 0;
        public uint RealModel = 0;
        public ushort Avatar = 0;
        public uint RModel = 0;
        public ushort RealAvatar = 0;
        public bool Alive = true;
        public byte dexp = 0;
        public uint dexptime = 0;
        public uint SkillExpNull = 0;
        public byte Stamina = 0;
        public bool Vending = false;
        public int ShopID = 0;
        public bool CanSend = true;

        public bool NoelGift = false;
        public byte QuestNB = 0;
        public byte LotoCount = 0;

        public string Name = "";
        public string Spouse = "Non";
        public string PackedInventory = "";
        public string PackedEquips = "";
        public string PackedSkills = "";
        public string PackedProfs = "";
        public string PackedWHs = "";
        public string PackedFriends = "";
        public string PackedEnemies = "";
        public bool Invincible = false;
        public bool Attacking = false;
        public bool AimBot = false;
        public string AimTarget = "";
        public ushort AimX = 0;
        public ushort AimY = 0;

        public byte Level = 0;
        public byte Job = 0;

        public byte Action = 100;
        public byte Direction = 3;
        public byte PKMode = 0;

        public byte RBCount = 0;
        public uint StatP = 0;
        public byte FirstJob = 0;
        public byte FirstLevel = 0;
        public byte SecondJob = 0;
        public byte SecondLevel = 0;

        public uint KO = 0;
        public uint OldKO = 0;

        public string QuestFrom = "";
        public string QuestMob = "";
        public uint QuestKO = 0;

        public uint DisKO = 0;

        public uint BanNB = 0;

        public uint WHPWcheck = 0;
        public string WHPW = "";

        public ulong Exp = 0;
        public uint WHSilvers = 0;
        public uint Silvers = 0;
        public uint CPs = 0;
        public ushort PKPoints = 0;
        public uint VP = 0;

        public ushort Str = 0;
        public ushort Agi = 0;
        public ushort Vit = 0;
        public ushort Spi = 0;
        public ushort MaxHP = 0;
        public ushort CurHP = 0;
        public ushort ExtraHP = 0;
        public ushort MaxMP = 0;
        public ushort CurMP = 0;
        public ushort Hair = 0;
        public ushort ShowHair = 0;
        public ushort LocX = 0;
        public ushort LocY = 0;
        public ushort LocMap = 0;
        public ushort PrevX = 0;
        public ushort PrevY = 0;
        public ushort RealAgi = 0;
        public double AddAtkPc = 1;
        public double AddMAtkPc = 1;
        public double AddExpPc = 1;
        public double AddProfPc = 1;
        public double AddSpellPc = 1;
        public double AddMythique = 0;

        public double Mythique = 0;
        public uint Defense = 0;
        public uint MDefense = 0;
        public uint MagicBlock = 0;
        public double MinAtk = 0;
        public double MaxAtk = 0;
        public double MAtk = 0;
        public double Bless = 0;
        public byte Dodge = 0;

        public uint LuckTime = 0;
        public DateTime PrayCasted = DateTime.Now;
        public ushort PrayX = 0;
        public ushort PrayY = 0;
        public bool CastingPray = false;
        public bool Praying = false;
        public bool CanPray = false;

        public byte WhichBless = 0;
        public bool Blessed = false;
        public DateTime HBEnd;
        public DateTime HBStart;
        public string HBEnd2 = "";
        public uint Curse = 0;
        public ulong LogoutTime = 0;
        public bool OfflineTG = false;

        public byte Nobility = 0;
        public uint Donation = 0;

        public ushort PrevMap = 0;

        public Hashtable Skills = new Hashtable();
        public Hashtable Skill_Exps = new Hashtable();
        public Hashtable Profs = new Hashtable();
        public Hashtable Prof_Exps = new Hashtable();
        public Hashtable Friends = new Hashtable();
        public Hashtable Enemies = new Hashtable();

        public string[] Equips = new string[10];
        public string[] Inventory = new string[42];
        public string[] TCWH = new string[20];
        public string[] PCWH = new string[20];
        public string[] BIWH = new string[20];
        public string[] ACWH = new string[20];
        public string[] DCWH = new string[20];
        public string[] MAWH = new string[40];

        public uint[] Equips_UIDs = new uint[10];
        public uint[] Inventory_UIDs = new uint[41];
        public uint[][] WHIDs = new uint[6][];

        public byte ItemsInInventory = 0;
        public byte AddedSkills = 0;
        public byte TCWHCount = 0;
        public byte PCWHCount = 0;
        public byte BIWHCount = 0;
        public byte ACWHCount = 0;
        public byte DCWHCount = 0;
        public byte MAWHCount = 0;

        public SingleMob MobTarget = null;
        public SingleMob Guard = null;
        public Character PTarget = null;
        public SingleNPC TGTarget = null;
        public byte AtkType = 0;
        public ushort Potency = 0;
        public ushort SkillLooping = 0;
        public ushort SkillLoopingX = 0;
        public ushort SkillLoopingY = 0;
        public uint SkillLoopingTarget = 0;
        public bool BlueName = false;
        public bool TeamLeader = false;
        public bool SMOn = false;
        public bool CycloneOn = false;
        public bool ReflectOn = false;
        public bool RobotOn = false;
        public bool XpList = false;
        public byte XpCircle = 0;
        public ArrayList Team = new ArrayList(6);
        public Character MyTeamLeader = null;
        public byte PlayersInTeam = 0;
        public byte TeamCount = 0;
        public bool JoinForbidden = false;
        public bool Trading = false;
        public uint TradingWith = 0;
        public ArrayList MyTradeSide = new ArrayList(20);
        public uint TradingSilvers = 0;
        public uint TradingCPs = 0;
        public bool TradeOK = false;
        public byte MyTradeSideCount = 0;
        public bool Flying = false;

        public bool Restoring = false;
        public DateTime RestoreTime = DateTime.Now;

        public bool StigBuff = false;
        public bool DodgeBuff = false;
        public bool AccuracyBuff = false;
        public bool MShieldBuff = false;
        public bool ShieldBuff = false;
        public bool PoisonBuff = false;
        public bool Invisible = false;
        public bool RobotBuff = false;
        public bool FreezeBuff = false;
        public bool BurnBuff = false;
        public bool IronBuff = false;
        public double ExtraXP = 0;
        public bool AccuracyOn = false;
        public bool Visible = false;
        public bool TInvisible = false;


        public uint TargetUID = 0;
        public uint RequestFriendWith = 0;

        public uint GuildDonation = 0;
        public ushort GuildID = 0;
        public byte GuildPosition = 0;

        public Guild MyGuild;
        public DateTime LastAttack;
        public DateTime LastXPC;
        public DateTime Death;
        public DateTime LastSave;
        public DateTime XPActivated;
        public DateTime GotBlueName;
        public DateTime LostPKP;
        public DateTime LastSwing = DateTime.Now;
        public DateTime AccuracyActivated = DateTime.Now;
        public DateTime LastTargetting = DateTime.Now;
        public DateTime LastGWList = DateTime.Now;
        public DateTime FlyActivated = DateTime.Now;
        public DateTime Stigged = DateTime.Now;
        public DateTime Dodged = DateTime.Now;
        public DateTime Shielded = DateTime.Now;
        public DateTime IronShirted = DateTime.Now;
        public DateTime MShielded = DateTime.Now;
        public DateTime Invisibility = DateTime.Now;
        public DateTime ExpBuffed = DateTime.Now;
        public DateTime Poisoned = DateTime.Now;
        public DateTime Freezed = DateTime.Now;
        public DateTime Burned = DateTime.Now;
        public DateTime RobotBuffed = DateTime.Now;
        public byte FlyType = 0;
        bool DeathSent = false;
        public bool Mining;
        public byte StigLevel = 0;
        public byte DodgeLevel = 0;
        public byte InvLevel = 0;
        public byte MShieldLevel = 0;
        public byte AccuLevel = 0;

        public System.Timers.Timer TheTimer = new System.Timers.Timer();

        public System.Timers.Timer Radio = new System.Timers.Timer();

        public void Radio_Elapsed(object sender, ElapsedEventArgs e)
        {
            CanSend = true;
            Radio.Stop();
        }

        public void RemoveAttr()
        {
            byte Pos = 1;
            while (Pos < 10)
            {
                if (Equips[Pos] != null && Equips[Pos] != "0")
                {
                    string[] Splitter = Equips[Pos].Split('-');
                    uint ItemId = uint.Parse(Splitter[0]);
                    byte ItemBless = byte.Parse(Splitter[2]);
                    if (ItemBless > 7)
                    {
                        GetEquipStats(Pos, true);
                        Equips[Pos] = ItemId + "-" + Splitter[1] + "-7-" + Splitter[3] + "-" + Splitter[4] + "-" + Splitter[5];
                        GetEquipStats(Pos, false);
                        MyClient.SendPacket(General.MyPackets.AddItem((long)Equips_UIDs[Pos], (int)ItemId, byte.Parse(Splitter[1]), 7, byte.Parse(Splitter[3]), byte.Parse(Splitter[4]), byte.Parse(Splitter[5]), Pos, 100, 100));
                    }
                }
                Pos++;
            }
        }

        public void SwingPickAxe()
        {
            if (Mining)
            {
                string[] Splitter = Equips[4].Split('-');
                if (LocMap == 1028 && Splitter[0] == "562000" || Splitter[0] == "562001")
                {
                    LastSwing = DateTime.Now;
                    MyClient.SendPacket(General.MyPackets.GeneralData(UID, 99, LocX, LocY, 99));
                    World.PlayerMoves(this, General.MyPackets.GeneralData(UID, 99, LocX, LocY, 99));

                    if (ItemsInInventory < 40)
                        if (Other.ChanceSuccess(25))
                        {
                            uint ItemId = (uint)(1072010 + General.Rand.Next(9));
                            if (Other.ChanceSuccess(35))
                                ItemId = (uint)(1072040 + General.Rand.Next(9));
                            if (Other.ChanceSuccess(20))
                                ItemId = (uint)(1072050 + General.Rand.Next(9));
                            if (Other.ChanceSuccess(10))
                                ItemId = 1072031;
                            if (Other.ChanceSuccess(2))
                            {
                                ItemId = (uint)(700001 + General.Rand.Next(7) * 10);
                                if (Other.ChanceSuccess(40))
                                    ItemId++;
                                if (Other.ChanceSuccess(15))
                                    ItemId++;
                            }
                            AddItem(ItemId.ToString() + "-0-0-0-0-0", 0, (uint)General.Rand.Next(23453246));
                        }
                }
                else
                    Mining = false;
            }
        }

        public void GemEffect()
        {
            if (Other.ChanceSuccess(25))
            {
                    string TheEquip1 = Equips[1];
                    string TheEquip2 = Equips[2];
                    string TheEquip3 = Equips[3];
                    string TheEquip4 = Equips[4];
                    string TheEquip8 = Equips[8];
                    string TheEquip6 = Equips[6];

                    if (Equips[1] != "0" && Equips[1] != null)
                    {
                        string[] Splitter = Equips[1].Split('-');

                        byte ItemGem1 = byte.Parse(Splitter[4]);
                        byte ItemGem2 = byte.Parse(Splitter[5]);

                        uint[] TheItem = Other.ItemInfo(uint.Parse(Splitter[0]));

                        string PItemID = Splitter[0];
                        uint ItemId = uint.Parse(Splitter[0]);

                        if (Other.ChanceSuccess(25))
                        {
                            if (ItemGem1 == 13 || ItemGem2 == 13 || ItemGem1 == 13 && ItemGem2 == 13)
                            {
                                if (Other.ChanceSuccess(15))
                                {
                                    //MyClient.SendPacket(General.MyPackets.String(UID, 10, "goldendragon"));
                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character Chaar = (Character)DE.Value;
                                        if (Chaar.Name != Name)
                                        {
                                            Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "goldendragon"));
                                        }
                                    }
                                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "goldendragon"));
                                }
                            }
                            if (ItemGem1 == 3 || ItemGem2 == 3 || ItemGem1 == 3 && ItemGem2 == 3)
                            {
                                if (Other.ChanceSuccess(15))
                                {
                                    //MyClient.SendPacket(General.MyPackets.String(UID, 10, "phoenix"));
                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character Chaar = (Character)DE.Value;
                                        if (Chaar.Name != Name)
                                        {
                                            Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "phoenix"));
                                        }
                                    }
                                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "phoenix"));
                                }
                            }
                            if (ItemGem1 == 63 || ItemGem2 == 63 || ItemGem1 == 63 && ItemGem2 == 63)
                            {
                                if (Other.ChanceSuccess(15))
                                {
                                    //MyClient.SendPacket(General.MyPackets.String(UID, 10, "moon"));
                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character Chaar = (Character)DE.Value;
                                        if (Chaar.Name != Name)
                                        {
                                            Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "moon"));
                                        }
                                    }
                                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "moon"));
                                }
                            }
                            if (ItemGem1 == 33 || ItemGem2 == 33 || ItemGem1 == 33 && ItemGem2 == 33)
                            {
                                if (Other.ChanceSuccess(15))
                                {
                                    //MyClient.SendPacket(General.MyPackets.String(UID, 10, "rainbow"));
                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character Chaar = (Character)DE.Value;
                                        if (Chaar.Name != Name)
                                        {
                                            Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "rainbow"));
                                        }
                                    }
                                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "rainbow"));
                                }
                            }
                            if (ItemGem1 == 23 || ItemGem2 == 23 || ItemGem1 == 23 && ItemGem2 == 23)
                            {
                                if (Other.ChanceSuccess(15))
                                {
                                    //MyClient.SendPacket(General.MyPackets.String(UID, 10, "fastflash"));
                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character Chaar = (Character)DE.Value;
                                        if (Chaar.Name != Name)
                                        {
                                            Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "fastflash"));
                                        }
                                    }
                                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "fastflash"));
                                }
                            }
                            if (ItemGem1 == 43 || ItemGem2 == 43 || ItemGem1 == 43 && ItemGem2 == 43)
                            {
                                if (Other.ChanceSuccess(15))
                                {
                                    //MyClient.SendPacket(General.MyPackets.String(UID, 10, "goldenkylin"));
                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character Chaar = (Character)DE.Value;
                                        if (Chaar.Name != Name)
                                        {
                                            Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "goldenkylin"));
                                        }
                                    }
                                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "goldenkylin"));
                                }
                            }
                            if (ItemGem1 == 53 || ItemGem2 == 53 || ItemGem1 == 53 && ItemGem2 == 53)
                            {
                                if (Other.ChanceSuccess(15))
                                {
                                    //MyClient.SendPacket(General.MyPackets.String(UID, 10, "purpleray"));
                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character Chaar = (Character)DE.Value;
                                        if (Chaar.Name != Name)
                                        {
                                            Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "purpleray"));
                                        }
                                    }
                                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "purpleray"));
                                }
                            }
                            if (ItemGem1 == 73 || ItemGem2 == 73 || ItemGem1 == 73 && ItemGem2 == 73)
                            {
                                if (Other.ChanceSuccess(15))
                                {
                                    //MyClient.SendPacket(General.MyPackets.String(UID, 10, "recovery"));
                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character Chaar = (Character)DE.Value;
                                        if (Chaar.Name != Name)
                                        {
                                            Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "recovery"));
                                        }
                                    }
                                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "recovery"));
                                }
                            }
                        }
                    }
                    if (Equips[2] != "0" && Equips[2] != null)
                    {
                        string[] Splitter = Equips[2].Split('-');

                        byte ItemGem1 = byte.Parse(Splitter[4]);
                        byte ItemGem2 = byte.Parse(Splitter[5]);

                        uint[] TheItem = Other.ItemInfo(uint.Parse(Splitter[0]));

                        string PItemID = Splitter[0];
                        uint ItemId = uint.Parse(Splitter[0]);

                        if (Other.ChanceSuccess(25))
                        {
                            if (ItemGem1 == 13 || ItemGem2 == 13 || ItemGem1 == 13 && ItemGem2 == 13)
                            {
                                if (Other.ChanceSuccess(15))
                                {
                                    //MyClient.SendPacket(General.MyPackets.String(UID, 10, "goldendragon"));
                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character Chaar = (Character)DE.Value;
                                        if (Chaar.Name != Name)
                                        {
                                            Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "goldendragon"));
                                        }
                                    }
                                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "goldendragon"));
                                }
                            }
                            if (ItemGem1 == 3 || ItemGem2 == 3 || ItemGem1 == 3 && ItemGem2 == 3)
                            {
                                if (Other.ChanceSuccess(15))
                                {
                                    //MyClient.SendPacket(General.MyPackets.String(UID, 10, "phoenix"));
                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character Chaar = (Character)DE.Value;
                                        if (Chaar.Name != Name)
                                        {
                                            Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "phoenix"));
                                        }
                                    }
                                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "phoenix"));
                                }
                            }
                            if (ItemGem1 == 63 || ItemGem2 == 63 || ItemGem1 == 63 && ItemGem2 == 63)
                            {
                                if (Other.ChanceSuccess(15))
                                {
                                    //MyClient.SendPacket(General.MyPackets.String(UID, 10, "moon"));
                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character Chaar = (Character)DE.Value;
                                        if (Chaar.Name != Name)
                                        {
                                            Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "moon"));
                                        }
                                    }
                                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "moon"));
                                }
                            }
                            if (ItemGem1 == 33 || ItemGem2 == 33 || ItemGem1 == 33 && ItemGem2 == 33)
                            {
                                if (Other.ChanceSuccess(15))
                                {
                                    //MyClient.SendPacket(General.MyPackets.String(UID, 10, "rainbow"));
                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character Chaar = (Character)DE.Value;
                                        if (Chaar.Name != Name)
                                        {
                                            Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "rainbow"));
                                        }
                                    }
                                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "rainbow"));
                                }
                            }
                            if (ItemGem1 == 23 || ItemGem2 == 23 || ItemGem1 == 23 && ItemGem2 == 23)
                            {
                                if (Other.ChanceSuccess(15))
                                {
                                    //MyClient.SendPacket(General.MyPackets.String(UID, 10, "fastflash"));
                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character Chaar = (Character)DE.Value;
                                        if (Chaar.Name != Name)
                                        {
                                            Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "fastflash"));
                                        }
                                    }
                                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "fastflash"));
                                }
                            }
                            if (ItemGem1 == 43 || ItemGem2 == 43 || ItemGem1 == 43 && ItemGem2 == 43)
                            {
                                if (Other.ChanceSuccess(15))
                                {
                                    //MyClient.SendPacket(General.MyPackets.String(UID, 10, "goldenkylin"));
                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character Chaar = (Character)DE.Value;
                                        if (Chaar.Name != Name)
                                        {
                                            Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "goldenkylin"));
                                        }
                                    }
                                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "goldenkylin"));
                                }
                            }
                            if (ItemGem1 == 53 || ItemGem2 == 53 || ItemGem1 == 53 && ItemGem2 == 53)
                            {
                                if (Other.ChanceSuccess(15))
                                {
                                    //MyClient.SendPacket(General.MyPackets.String(UID, 10, "purpleray"));
                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character Chaar = (Character)DE.Value;
                                        if (Chaar.Name != Name)
                                        {
                                            Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "purpleray"));
                                        }
                                    }
                                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "purpleray"));
                                }
                            }
                            if (ItemGem1 == 73 || ItemGem2 == 73 || ItemGem1 == 73 && ItemGem2 == 73)
                            {
                                if (Other.ChanceSuccess(15))
                                {
                                    //MyClient.SendPacket(General.MyPackets.String(UID, 10, "recovery"));
                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character Chaar = (Character)DE.Value;
                                        if (Chaar.Name != Name)
                                        {
                                            Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "recovery"));
                                        }
                                    }
                                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "recovery"));
                                }
                            }
                        }
                    }
                    if (Equips[3] != "0" && Equips[3] != null)
                    {
                        string[] Splitter = Equips[3].Split('-');

                        byte ItemGem1 = byte.Parse(Splitter[4]);
                        byte ItemGem2 = byte.Parse(Splitter[5]);

                        uint[] TheItem = Other.ItemInfo(uint.Parse(Splitter[0]));

                        string PItemID = Splitter[0];
                        uint ItemId = uint.Parse(Splitter[0]);

                        if (Other.ChanceSuccess(25))
                        {
                            if (ItemGem1 == 13 || ItemGem2 == 13 || ItemGem1 == 13 && ItemGem2 == 13)
                            {
                                if (Other.ChanceSuccess(15))
                                {
                                    //MyClient.SendPacket(General.MyPackets.String(UID, 10, "goldendragon"));
                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character Chaar = (Character)DE.Value;
                                        if (Chaar.Name != Name)
                                        {
                                            Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "goldendragon"));
                                        }
                                    }
                                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "goldendragon"));
                                }
                            }
                            if (ItemGem1 == 3 || ItemGem2 == 3 || ItemGem1 == 3 && ItemGem2 == 3)
                            {
                                if (Other.ChanceSuccess(15))
                                {
                                    //MyClient.SendPacket(General.MyPackets.String(UID, 10, "phoenix"));
                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character Chaar = (Character)DE.Value;
                                        if (Chaar.Name != Name)
                                        {
                                            Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "phoenix"));
                                        }
                                    }
                                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "phoenix"));
                                }
                            }
                            if (ItemGem1 == 63 || ItemGem2 == 63 || ItemGem1 == 63 && ItemGem2 == 63)
                            {
                                if (Other.ChanceSuccess(15))
                                {
                                    //MyClient.SendPacket(General.MyPackets.String(UID, 10, "moon"));
                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character Chaar = (Character)DE.Value;
                                        if (Chaar.Name != Name)
                                        {
                                            Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "moon"));
                                        }
                                    }
                                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "moon"));
                                }
                            }
                            if (ItemGem1 == 33 || ItemGem2 == 33 || ItemGem1 == 33 && ItemGem2 == 33)
                            {
                                if (Other.ChanceSuccess(15))
                                {
                                    //MyClient.SendPacket(General.MyPackets.String(UID, 10, "rainbow"));
                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character Chaar = (Character)DE.Value;
                                        if (Chaar.Name != Name)
                                        {
                                            Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "rainbow"));
                                        }
                                    }
                                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "rainbow"));
                                }
                            }
                            if (ItemGem1 == 23 || ItemGem2 == 23 || ItemGem1 == 23 && ItemGem2 == 23)
                            {
                                if (Other.ChanceSuccess(15))
                                {
                                    //MyClient.SendPacket(General.MyPackets.String(UID, 10, "fastflash"));
                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character Chaar = (Character)DE.Value;
                                        if (Chaar.Name != Name)
                                        {
                                            Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "fastflash"));
                                        }
                                    }
                                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "fastflash"));
                                }
                            }
                            if (ItemGem1 == 43 || ItemGem2 == 43 || ItemGem1 == 43 && ItemGem2 == 43)
                            {
                                if (Other.ChanceSuccess(15))
                                {
                                    //MyClient.SendPacket(General.MyPackets.String(UID, 10, "goldenkylin"));
                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character Chaar = (Character)DE.Value;
                                        if (Chaar.Name != Name)
                                        {
                                            Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "goldenkylin"));
                                        }
                                    }
                                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "goldenkylin"));
                                }
                            }
                            if (ItemGem1 == 53 || ItemGem2 == 53 || ItemGem1 == 53 && ItemGem2 == 53)
                            {
                                if (Other.ChanceSuccess(15))
                                {
                                    //MyClient.SendPacket(General.MyPackets.String(UID, 10, "purpleray"));
                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character Chaar = (Character)DE.Value;
                                        if (Chaar.Name != Name)
                                        {
                                            Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "purpleray"));
                                        }
                                    }
                                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "purpleray"));
                                }
                            }
                            if (ItemGem1 == 73 || ItemGem2 == 73 || ItemGem1 == 73 && ItemGem2 == 73)
                            {
                                if (Other.ChanceSuccess(15))
                                {
                                    //MyClient.SendPacket(General.MyPackets.String(UID, 10, "recovery"));
                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character Chaar = (Character)DE.Value;
                                        if (Chaar.Name != Name)
                                        {
                                            Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "recovery"));
                                        }
                                    }
                                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "recovery"));
                                }
                            }
                        }
                    }
                    if (Equips[4] != "0" && Equips[4] != null)
                    {
                        string[] Splitter = Equips[4].Split('-');

                        byte ItemGem1 = byte.Parse(Splitter[4]);
                        byte ItemGem2 = byte.Parse(Splitter[5]);

                        uint[] TheItem = Other.ItemInfo(uint.Parse(Splitter[0]));

                        string PItemID = Splitter[0];
                        uint ItemId = uint.Parse(Splitter[0]);

                        if (Other.ChanceSuccess(25))
                        {
                            if (ItemGem1 == 13 || ItemGem2 == 13 || ItemGem1 == 13 && ItemGem2 == 13)
                            {
                                if (Other.ChanceSuccess(15))
                                {
                                    //MyClient.SendPacket(General.MyPackets.String(UID, 10, "goldendragon"));
                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character Chaar = (Character)DE.Value;
                                        if (Chaar.Name != Name)
                                        {
                                            Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "goldendragon"));
                                        }
                                    }
                                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "goldendragon"));
                                }
                            }
                            if (ItemGem1 == 3 || ItemGem2 == 3 || ItemGem1 == 3 && ItemGem2 == 3)
                            {
                                if (Other.ChanceSuccess(15))
                                {
                                    //MyClient.SendPacket(General.MyPackets.String(UID, 10, "phoenix"));
                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character Chaar = (Character)DE.Value;
                                        if (Chaar.Name != Name)
                                        {
                                            Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "phoenix"));
                                        }
                                    }
                                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "phoenix"));
                                }
                            }
                            if (ItemGem1 == 63 || ItemGem2 == 63 || ItemGem1 == 63 && ItemGem2 == 63)
                            {
                                if (Other.ChanceSuccess(15))
                                {
                                    //MyClient.SendPacket(General.MyPackets.String(UID, 10, "moon"));
                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character Chaar = (Character)DE.Value;
                                        if (Chaar.Name != Name)
                                        {
                                            Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "moon"));
                                        }
                                    }
                                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "moon"));
                                }
                            }
                            if (ItemGem1 == 33 || ItemGem2 == 33 || ItemGem1 == 33 && ItemGem2 == 33)
                            {
                                if (Other.ChanceSuccess(15))
                                {
                                    //MyClient.SendPacket(General.MyPackets.String(UID, 10, "rainbow"));
                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character Chaar = (Character)DE.Value;
                                        if (Chaar.Name != Name)
                                        {
                                            Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "rainbow"));
                                        }
                                    }
                                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "rainbow"));
                                }
                            }
                            if (ItemGem1 == 23 || ItemGem2 == 23 || ItemGem1 == 23 && ItemGem2 == 23)
                            {
                                if (Other.ChanceSuccess(15))
                                {
                                    //MyClient.SendPacket(General.MyPackets.String(UID, 10, "fastflash"));
                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character Chaar = (Character)DE.Value;
                                        if (Chaar.Name != Name)
                                        {
                                            Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "fastflash"));
                                        }
                                    }
                                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "fastflash"));
                                }
                            }
                            if (ItemGem1 == 43 || ItemGem2 == 43 || ItemGem1 == 43 && ItemGem2 == 43)
                            {
                                if (Other.ChanceSuccess(15))
                                {
                                    //MyClient.SendPacket(General.MyPackets.String(UID, 10, "goldenkylin"));
                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character Chaar = (Character)DE.Value;
                                        if (Chaar.Name != Name)
                                        {
                                            Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "goldenkylin"));
                                        }
                                    }
                                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "goldenkylin"));
                                }
                            }
                            if (ItemGem1 == 53 || ItemGem2 == 53 || ItemGem1 == 53 && ItemGem2 == 53)
                            {
                                if (Other.ChanceSuccess(15))
                                {
                                    //MyClient.SendPacket(General.MyPackets.String(UID, 10, "purpleray"));
                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character Chaar = (Character)DE.Value;
                                        if (Chaar.Name != Name)
                                        {
                                            Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "purpleray"));
                                        }
                                    }
                                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "purpleray"));
                                }
                            }
                            if (ItemGem1 == 73 || ItemGem2 == 73 || ItemGem1 == 73 && ItemGem2 == 73)
                            {
                                if (Other.ChanceSuccess(15))
                                {
                                    //MyClient.SendPacket(General.MyPackets.String(UID, 10, "recovery"));
                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character Chaar = (Character)DE.Value;
                                        if (Chaar.Name != Name)
                                        {
                                            Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "recovery"));
                                        }
                                    }
                                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "recovery"));
                                }
                            }
                        }
                    }
                    if (Equips[6] != "0" && Equips[6] != null)
                    {
                        string[] Splitter = Equips[6].Split('-');

                        byte ItemGem1 = byte.Parse(Splitter[4]);
                        byte ItemGem2 = byte.Parse(Splitter[5]);

                        uint[] TheItem = Other.ItemInfo(uint.Parse(Splitter[0]));

                        string PItemID = Splitter[0];
                        uint ItemId = uint.Parse(Splitter[0]);

                        if (Other.ChanceSuccess(25))
                        {
                            if (ItemGem1 == 13 || ItemGem2 == 13 || ItemGem1 == 13 && ItemGem2 == 13)
                            {
                                if (Other.ChanceSuccess(15))
                                {
                                    //MyClient.SendPacket(General.MyPackets.String(UID, 10, "goldendragon"));
                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character Chaar = (Character)DE.Value;
                                        if (Chaar.Name != Name)
                                        {
                                            Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "goldendragon"));
                                        }
                                    }
                                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "goldendragon"));
                                }
                            }
                            if (ItemGem1 == 3 || ItemGem2 == 3 || ItemGem1 == 3 && ItemGem2 == 3)
                            {
                                if (Other.ChanceSuccess(15))
                                {
                                    //MyClient.SendPacket(General.MyPackets.String(UID, 10, "phoenix"));
                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character Chaar = (Character)DE.Value;
                                        if (Chaar.Name != Name)
                                        {
                                            Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "phoenix"));
                                        }
                                    }
                                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "phoenix"));
                                }
                            }
                            if (ItemGem1 == 63 || ItemGem2 == 63 || ItemGem1 == 63 && ItemGem2 == 63)
                            {
                                if (Other.ChanceSuccess(15))
                                {
                                    //MyClient.SendPacket(General.MyPackets.String(UID, 10, "moon"));
                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character Chaar = (Character)DE.Value;
                                        if (Chaar.Name != Name)
                                        {
                                            Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "moon"));
                                        }
                                    }
                                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "moon"));
                                }
                            }
                            if (ItemGem1 == 33 || ItemGem2 == 33 || ItemGem1 == 33 && ItemGem2 == 33)
                            {
                                if (Other.ChanceSuccess(15))
                                {
                                    //MyClient.SendPacket(General.MyPackets.String(UID, 10, "rainbow"));
                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character Chaar = (Character)DE.Value;
                                        if (Chaar.Name != Name)
                                        {
                                            Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "rainbow"));
                                        }
                                    }
                                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "rainbow"));
                                }
                            }
                            if (ItemGem1 == 23 || ItemGem2 == 23 || ItemGem1 == 23 && ItemGem2 == 23)
                            {
                                if (Other.ChanceSuccess(15))
                                {
                                    //MyClient.SendPacket(General.MyPackets.String(UID, 10, "fastflash"));
                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character Chaar = (Character)DE.Value;
                                        if (Chaar.Name != Name)
                                        {
                                            Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "fastflash"));
                                        }
                                    }
                                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "fastflash"));
                                }
                            }
                            if (ItemGem1 == 43 || ItemGem2 == 43 || ItemGem1 == 43 && ItemGem2 == 43)
                            {
                                if (Other.ChanceSuccess(15))
                                {
                                    //MyClient.SendPacket(General.MyPackets.String(UID, 10, "goldenkylin"));
                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character Chaar = (Character)DE.Value;
                                        if (Chaar.Name != Name)
                                        {
                                            Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "goldenkylin"));
                                        }
                                    }
                                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "goldenkylin"));
                                }
                            }
                            if (ItemGem1 == 53 || ItemGem2 == 53 || ItemGem1 == 53 && ItemGem2 == 53)
                            {
                                if (Other.ChanceSuccess(15))
                                {
                                    //MyClient.SendPacket(General.MyPackets.String(UID, 10, "purpleray"));
                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character Chaar = (Character)DE.Value;
                                        if (Chaar.Name != Name)
                                        {
                                            Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "purpleray"));
                                        }
                                    }
                                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "purpleray"));
                                }
                            }
                            if (ItemGem1 == 73 || ItemGem2 == 73 || ItemGem1 == 73 && ItemGem2 == 73)
                            {
                                if (Other.ChanceSuccess(15))
                                {
                                    //MyClient.SendPacket(General.MyPackets.String(UID, 10, "recovery"));
                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character Chaar = (Character)DE.Value;
                                        if (Chaar.Name != Name)
                                        {
                                            Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "recovery"));
                                        }
                                    }
                                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "recovery"));
                                }
                            }
                        }
                    }
                    if (Equips[8] != "0" && Equips[8] != null)
                    {
                        string[] Splitter = Equips[8].Split('-');

                        byte ItemGem1 = byte.Parse(Splitter[4]);
                        byte ItemGem2 = byte.Parse(Splitter[5]);

                        uint[] TheItem = Other.ItemInfo(uint.Parse(Splitter[0]));

                        string PItemID = Splitter[0];
                        uint ItemId = uint.Parse(Splitter[0]);

                        if (Other.ChanceSuccess(25))
                        {
                            if (ItemGem1 == 13 || ItemGem2 == 13 || ItemGem1 == 13 && ItemGem2 == 13)
                            {
                                if (Other.ChanceSuccess(15))
                                {
                                    //MyClient.SendPacket(General.MyPackets.String(UID, 10, "goldendragon"));
                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character Chaar = (Character)DE.Value;
                                        if (Chaar.Name != Name)
                                        {
                                            Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "goldendragon"));
                                        }
                                    }
                                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "goldendragon"));
                                }
                            }
                            if (ItemGem1 == 3 || ItemGem2 == 3 || ItemGem1 == 3 && ItemGem2 == 3)
                            {
                                if (Other.ChanceSuccess(15))
                                {
                                    //MyClient.SendPacket(General.MyPackets.String(UID, 10, "phoenix"));
                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character Chaar = (Character)DE.Value;
                                        if (Chaar.Name != Name)
                                        {
                                            Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "phoenix"));
                                        }
                                    }
                                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "phoenix"));
                                }
                            }
                            if (ItemGem1 == 63 || ItemGem2 == 63 || ItemGem1 == 63 && ItemGem2 == 63)
                            {
                                if (Other.ChanceSuccess(15))
                                {
                                    //MyClient.SendPacket(General.MyPackets.String(UID, 10, "moon"));
                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character Chaar = (Character)DE.Value;
                                        if (Chaar.Name != Name)
                                        {
                                            Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "moon"));
                                        }
                                    }
                                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "moon"));
                                }
                            }
                            if (ItemGem1 == 33 || ItemGem2 == 33 || ItemGem1 == 33 && ItemGem2 == 33)
                            {
                                if (Other.ChanceSuccess(15))
                                {
                                    //MyClient.SendPacket(General.MyPackets.String(UID, 10, "rainbow"));
                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character Chaar = (Character)DE.Value;
                                        if (Chaar.Name != Name)
                                        {
                                            Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "rainbow"));
                                        }
                                    }
                                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "rainbow"));
                                }
                            }
                            if (ItemGem1 == 23 || ItemGem2 == 23 || ItemGem1 == 23 && ItemGem2 == 23)
                            {
                                if (Other.ChanceSuccess(15))
                                {
                                    //MyClient.SendPacket(General.MyPackets.String(UID, 10, "fastflash"));
                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character Chaar = (Character)DE.Value;
                                        if (Chaar.Name != Name)
                                        {
                                            Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "fastflash"));
                                        }
                                    }
                                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "fastflash"));
                                }
                            }
                            if (ItemGem1 == 43 || ItemGem2 == 43 || ItemGem1 == 43 && ItemGem2 == 43)
                            {
                                if (Other.ChanceSuccess(15))
                                {
                                    //MyClient.SendPacket(General.MyPackets.String(UID, 10, "goldenkylin"));
                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character Chaar = (Character)DE.Value;
                                        if (Chaar.Name != Name)
                                        {
                                            Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "goldenkylin"));
                                        }
                                    }
                                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "goldenkylin"));
                                }
                            }
                            if (ItemGem1 == 53 || ItemGem2 == 53 || ItemGem1 == 53 && ItemGem2 == 53)
                            {
                                if (Other.ChanceSuccess(15))
                                {
                                    //MyClient.SendPacket(General.MyPackets.String(UID, 10, "purpleray"));
                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character Chaar = (Character)DE.Value;
                                        if (Chaar.Name != Name)
                                        {
                                            Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "purpleray"));
                                        }
                                    }
                                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "purpleray"));
                                }
                            }
                            if (ItemGem1 == 73 || ItemGem2 == 73 || ItemGem1 == 73 && ItemGem2 == 73)
                            {
                                if (Other.ChanceSuccess(15))
                                {
                                    //MyClient.SendPacket(General.MyPackets.String(UID, 10, "recovery"));
                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character Chaar = (Character)DE.Value;
                                        if (Chaar.Name != Name)
                                        {
                                            Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "recovery"));
                                        }
                                    }
                                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "recovery"));
                                }
                            }
                        }
                    }
                }
        }

        public void BlessEffect()
        {
            if (Other.ChanceSuccess(25))
            {
                string TheEquip1 = Equips[1];
                string TheEquip2 = Equips[2];
                string TheEquip3 = Equips[3];
                string TheEquip4 = Equips[4];
                string TheEquip8 = Equips[8];
                string TheEquip6 = Equips[6];

                if (Equips[1] != "0" && Equips[1] != null)
                {
                    string[] Splitter = Equips[1].Split('-');

                    byte ItemBless = byte.Parse(Splitter[2]);

                    uint[] TheItem = Other.ItemInfo(uint.Parse(Splitter[0]));

                    string PItemID = Splitter[0];
                    uint ItemId = uint.Parse(Splitter[0]);

                    if (Other.ChanceSuccess(25))
                    {
                        if (ItemBless == 1)
                        {
                            if (Other.ChanceSuccess(15))
                            {
                                foreach (DictionaryEntry DE in World.AllChars)
                                {
                                    Character Chaar = (Character)DE.Value;
                                    if (Chaar.Name != Name)
                                    {
                                        Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "Aegis1"));
                                    }
                                }
                                MyClient.SendPacket(General.MyPackets.String(UID, 10, "Aegis1"));
                            }
                        }
                        if (ItemBless == 2 || ItemBless == 3)
                        {
                            if (Other.ChanceSuccess(15))
                            {
                                foreach (DictionaryEntry DE in World.AllChars)
                                {
                                    Character Chaar = (Character)DE.Value;
                                    if (Chaar.Name != Name)
                                    {
                                        Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "Aegis2"));
                                    }
                                }
                                MyClient.SendPacket(General.MyPackets.String(UID, 10, "Aegis2"));
                            }
                        }
                        if (ItemBless == 4 || ItemBless == 5)
                        {
                            if (Other.ChanceSuccess(15))
                            {
                                foreach (DictionaryEntry DE in World.AllChars)
                                {
                                    Character Chaar = (Character)DE.Value;
                                    if (Chaar.Name != Name)
                                    {
                                        Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "Aegis3"));
                                    }
                                }
                                MyClient.SendPacket(General.MyPackets.String(UID, 10, "Aegis3"));
                            }
                        }
                        if (ItemBless == 6 || ItemBless == 7 || ItemBless >= 7)
                        {
                            if (Other.ChanceSuccess(15))
                            {
                                foreach (DictionaryEntry DE in World.AllChars)
                                {
                                    Character Chaar = (Character)DE.Value;
                                    if (Chaar.Name != Name)
                                    {
                                        Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "Aegis4"));
                                    }
                                }
                                MyClient.SendPacket(General.MyPackets.String(UID, 10, "Aegis4"));
                            }
                        }
                    }
                }
                if (Equips[2] != "0" && Equips[2] != null)
                {
                    string[] Splitter = Equips[2].Split('-');

                    byte ItemBless = byte.Parse(Splitter[2]);

                    uint[] TheItem = Other.ItemInfo(uint.Parse(Splitter[0]));

                    string PItemID = Splitter[0];
                    uint ItemId = uint.Parse(Splitter[0]);

                    if (Other.ChanceSuccess(25))
                    {
                        if (ItemBless == 1)
                        {
                            if (Other.ChanceSuccess(15))
                            {
                                foreach (DictionaryEntry DE in World.AllChars)
                                {
                                    Character Chaar = (Character)DE.Value;
                                    if (Chaar.Name != Name)
                                    {
                                        Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "Aegis1"));
                                    }
                                }
                                MyClient.SendPacket(General.MyPackets.String(UID, 10, "Aegis1"));
                            }
                        }
                        if (ItemBless == 2 || ItemBless == 3)
                        {
                            if (Other.ChanceSuccess(15))
                            {
                                foreach (DictionaryEntry DE in World.AllChars)
                                {
                                    Character Chaar = (Character)DE.Value;
                                    if (Chaar.Name != Name)
                                    {
                                        Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "Aegis2"));
                                    }
                                }
                                MyClient.SendPacket(General.MyPackets.String(UID, 10, "Aegis2"));
                            }
                        }
                        if (ItemBless == 4 || ItemBless == 5)
                        {
                            if (Other.ChanceSuccess(15))
                            {
                                foreach (DictionaryEntry DE in World.AllChars)
                                {
                                    Character Chaar = (Character)DE.Value;
                                    if (Chaar.Name != Name)
                                    {
                                        Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "Aegis3"));
                                    }
                                }
                                MyClient.SendPacket(General.MyPackets.String(UID, 10, "Aegis3"));
                            }
                        }
                        if (ItemBless == 6 || ItemBless == 7 || ItemBless >= 7)
                        {
                            if (Other.ChanceSuccess(15))
                            {
                                foreach (DictionaryEntry DE in World.AllChars)
                                {
                                    Character Chaar = (Character)DE.Value;
                                    if (Chaar.Name != Name)
                                    {
                                        Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "Aegis4"));
                                    }
                                }
                                MyClient.SendPacket(General.MyPackets.String(UID, 10, "Aegis4"));
                            }
                        }
                    }
                }
                if (Equips[3] != "0" && Equips[3] != null)
                {
                    string[] Splitter = Equips[3].Split('-');

                    byte ItemBless = byte.Parse(Splitter[2]);

                    uint[] TheItem = Other.ItemInfo(uint.Parse(Splitter[0]));

                    string PItemID = Splitter[0];
                    uint ItemId = uint.Parse(Splitter[0]);

                    if (Other.ChanceSuccess(25))
                    {
                        if (ItemBless == 1)
                        {
                            if (Other.ChanceSuccess(15))
                            {
                                foreach (DictionaryEntry DE in World.AllChars)
                                {
                                    Character Chaar = (Character)DE.Value;
                                    if (Chaar.Name != Name)
                                    {
                                        Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "Aegis1"));
                                    }
                                }
                                MyClient.SendPacket(General.MyPackets.String(UID, 10, "Aegis1"));
                            }
                        }
                        if (ItemBless == 2 || ItemBless == 3)
                        {
                            if (Other.ChanceSuccess(15))
                            {
                                foreach (DictionaryEntry DE in World.AllChars)
                                {
                                    Character Chaar = (Character)DE.Value;
                                    if (Chaar.Name != Name)
                                    {
                                        Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "Aegis2"));
                                    }
                                }
                                MyClient.SendPacket(General.MyPackets.String(UID, 10, "Aegis2"));
                            }
                        }
                        if (ItemBless == 4 || ItemBless == 5)
                        {
                            if (Other.ChanceSuccess(15))
                            {
                                foreach (DictionaryEntry DE in World.AllChars)
                                {
                                    Character Chaar = (Character)DE.Value;
                                    if (Chaar.Name != Name)
                                    {
                                        Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "Aegis3"));
                                    }
                                }
                                MyClient.SendPacket(General.MyPackets.String(UID, 10, "Aegis3"));
                            }
                        }
                        if (ItemBless == 6 || ItemBless == 7 || ItemBless >= 7)
                        {
                            if (Other.ChanceSuccess(15))
                            {
                                foreach (DictionaryEntry DE in World.AllChars)
                                {
                                    Character Chaar = (Character)DE.Value;
                                    if (Chaar.Name != Name)
                                    {
                                        Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "Aegis4"));
                                    }
                                }
                                MyClient.SendPacket(General.MyPackets.String(UID, 10, "Aegis4"));
                            }
                        }
                    }
                }
                if (Equips[4] != "0" && Equips[4] != null)
                {
                    string[] Splitter = Equips[4].Split('-');

                    byte ItemBless = byte.Parse(Splitter[2]);

                    uint[] TheItem = Other.ItemInfo(uint.Parse(Splitter[0]));

                    string PItemID = Splitter[0];
                    uint ItemId = uint.Parse(Splitter[0]);

                    if (Other.ChanceSuccess(25))
                    {
                        if (ItemBless == 1)
                        {
                            if (Other.ChanceSuccess(15))
                            {
                                foreach (DictionaryEntry DE in World.AllChars)
                                {
                                    Character Chaar = (Character)DE.Value;
                                    if (Chaar.Name != Name)
                                    {
                                        Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "Aegis1"));
                                    }
                                }
                                MyClient.SendPacket(General.MyPackets.String(UID, 10, "Aegis1"));
                            }
                        }
                        if (ItemBless == 2 || ItemBless == 3)
                        {
                            if (Other.ChanceSuccess(15))
                            {
                                foreach (DictionaryEntry DE in World.AllChars)
                                {
                                    Character Chaar = (Character)DE.Value;
                                    if (Chaar.Name != Name)
                                    {
                                        Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "Aegis2"));
                                    }
                                }
                                MyClient.SendPacket(General.MyPackets.String(UID, 10, "Aegis2"));
                            }
                        }
                        if (ItemBless == 4 || ItemBless == 5)
                        {
                            if (Other.ChanceSuccess(15))
                            {
                                foreach (DictionaryEntry DE in World.AllChars)
                                {
                                    Character Chaar = (Character)DE.Value;
                                    if (Chaar.Name != Name)
                                    {
                                        Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "Aegis3"));
                                    }
                                }
                                MyClient.SendPacket(General.MyPackets.String(UID, 10, "Aegis3"));
                            }
                        }
                        if (ItemBless == 6 || ItemBless == 7 || ItemBless >= 7)
                        {
                            if (Other.ChanceSuccess(15))
                            {
                                foreach (DictionaryEntry DE in World.AllChars)
                                {
                                    Character Chaar = (Character)DE.Value;
                                    if (Chaar.Name != Name)
                                    {
                                        Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "Aegis4"));
                                    }
                                }
                                MyClient.SendPacket(General.MyPackets.String(UID, 10, "Aegis4"));
                            }
                        }
                    }
                }
                if (Equips[6] != "0" && Equips[6] != null)
                {
                    string[] Splitter = Equips[6].Split('-');

                    byte ItemBless = byte.Parse(Splitter[2]);

                    uint[] TheItem = Other.ItemInfo(uint.Parse(Splitter[0]));

                    string PItemID = Splitter[0];
                    uint ItemId = uint.Parse(Splitter[0]);

                    if (Other.ChanceSuccess(25))
                    {
                        if (ItemBless == 1)
                        {
                            if (Other.ChanceSuccess(15))
                            {
                                foreach (DictionaryEntry DE in World.AllChars)
                                {
                                    Character Chaar = (Character)DE.Value;
                                    if (Chaar.Name != Name)
                                    {
                                        Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "Aegis1"));
                                    }
                                }
                                MyClient.SendPacket(General.MyPackets.String(UID, 10, "Aegis1"));
                            }
                        }
                        if (ItemBless == 2 || ItemBless == 3)
                        {
                            if (Other.ChanceSuccess(15))
                            {
                                foreach (DictionaryEntry DE in World.AllChars)
                                {
                                    Character Chaar = (Character)DE.Value;
                                    if (Chaar.Name != Name)
                                    {
                                        Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "Aegis2"));
                                    }
                                }
                                MyClient.SendPacket(General.MyPackets.String(UID, 10, "Aegis2"));
                            }
                        }
                        if (ItemBless == 4 || ItemBless == 5)
                        {
                            if (Other.ChanceSuccess(15))
                            {
                                foreach (DictionaryEntry DE in World.AllChars)
                                {
                                    Character Chaar = (Character)DE.Value;
                                    if (Chaar.Name != Name)
                                    {
                                        Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "Aegis3"));
                                    }
                                }
                                MyClient.SendPacket(General.MyPackets.String(UID, 10, "Aegis3"));
                            }
                        }
                        if (ItemBless == 6 || ItemBless == 7 || ItemBless >= 7)
                        {
                            if (Other.ChanceSuccess(15))
                            {
                                foreach (DictionaryEntry DE in World.AllChars)
                                {
                                    Character Chaar = (Character)DE.Value;
                                    if (Chaar.Name != Name)
                                    {
                                        Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "Aegis4"));
                                    }
                                }
                                MyClient.SendPacket(General.MyPackets.String(UID, 10, "Aegis4"));
                            }
                        }
                    }
                }
                if (Equips[8] != "0" && Equips[8] != null)
                {
                    string[] Splitter = Equips[8].Split('-');

                    byte ItemBless = byte.Parse(Splitter[2]);

                    uint[] TheItem = Other.ItemInfo(uint.Parse(Splitter[0]));

                    string PItemID = Splitter[0];
                    uint ItemId = uint.Parse(Splitter[0]);

                    if (Other.ChanceSuccess(25))
                    {
                        if (ItemBless == 1)
                        {
                            if (Other.ChanceSuccess(15))
                            {
                                foreach (DictionaryEntry DE in World.AllChars)
                                {
                                    Character Chaar = (Character)DE.Value;
                                    if (Chaar.Name != Name)
                                    {
                                        Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "Aegis1"));
                                    }
                                }
                                MyClient.SendPacket(General.MyPackets.String(UID, 10, "Aegis1"));
                            }
                        }
                        if (ItemBless == 2 || ItemBless == 3)
                        {
                            if (Other.ChanceSuccess(15))
                            {
                                foreach (DictionaryEntry DE in World.AllChars)
                                {
                                    Character Chaar = (Character)DE.Value;
                                    if (Chaar.Name != Name)
                                    {
                                        Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "Aegis2"));
                                    }
                                }
                                MyClient.SendPacket(General.MyPackets.String(UID, 10, "Aegis2"));
                            }
                        }
                        if (ItemBless == 4 || ItemBless == 5)
                        {
                            if (Other.ChanceSuccess(15))
                            {
                                foreach (DictionaryEntry DE in World.AllChars)
                                {
                                    Character Chaar = (Character)DE.Value;
                                    if (Chaar.Name != Name)
                                    {
                                        Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "Aegis3"));
                                    }
                                }
                                MyClient.SendPacket(General.MyPackets.String(UID, 10, "Aegis3"));
                            }
                        }
                        if (ItemBless == 6 || ItemBless == 7 || ItemBless >= 7)
                        {
                            if (Other.ChanceSuccess(15))
                            {
                                foreach (DictionaryEntry DE in World.AllChars)
                                {
                                    Character Chaar = (Character)DE.Value;
                                    if (Chaar.Name != Name)
                                    {
                                        Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "Aegis4"));
                                    }
                                }
                                MyClient.SendPacket(General.MyPackets.String(UID, 10, "Aegis4"));
                            }
                        }
                    }
                }
            }
        }

        public bool AllSuper = false;

        public void FullSuper()
        {
            if (Equips[1] != null && Equips[2] != null && Equips[3] != null && Equips[4] != null && Equips[8] != null && Equips[6] != null)
            {
                string TheEquip1 = Equips[1];
                string TheEquip2 = Equips[2];
                string TheEquip3 = Equips[3];
                string TheEquip4 = Equips[4];
                string TheEquip8 = Equips[8];
                string TheEquip6 = Equips[6];

                string[] Splitter1 = TheEquip1.Split('-');
                uint ItemId1 = uint.Parse(Splitter1[0]);

                string[] Splitter2 = TheEquip2.Split('-');
                uint ItemId2 = uint.Parse(Splitter2[0]);

                string[] Splitter3 = TheEquip3.Split('-');
                uint ItemId3 = uint.Parse(Splitter3[0]);

                string[] Splitter4 = TheEquip4.Split('-');
                uint ItemId4 = uint.Parse(Splitter4[0]);

                string[] Splitter8 = TheEquip8.Split('-');
                uint ItemId8 = uint.Parse(Splitter8[0]);

                string[] Splitter6 = TheEquip6.Split('-');
                uint ItemId6 = uint.Parse(Splitter6[0]);

                if (Other.ItemQuality(ItemId1) == 9 && Other.ItemQuality(ItemId2) == 9 && Other.ItemQuality(ItemId3) == 9 && Other.ItemQuality(ItemId4) == 9 && Other.ItemQuality(ItemId8) == 9 && Other.ItemQuality(ItemId6) == 9)
                {
                    AllSuper = true;
                }
            }
        }

        public bool OneSuper = false;

        public void ArmorSuper()
        {
            if (Equips[3] != null)
            {
                string TheEquip3 = Equips[3];

                string[] Splitter3 = TheEquip3.Split('-');
                uint ItemId3 = uint.Parse(Splitter3[0]);

                if (Other.ItemQuality(ItemId3) == 9)
                {
                    OneSuper = true;
                }
            }
        }

        void TimerElapsed(object source, ElapsedEventArgs e)
        {
            if (StigBuff)
                if (DateTime.Now > Stigged.AddSeconds(20 + StigLevel * 5))
                {
                    StigBuff = false;
                    MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                    World.UpdateSpawn(this);
                }
            if (DodgeBuff)
                if (DateTime.Now > Dodged.AddSeconds(20 + DodgeLevel * 5 * 2))
                {
                    DodgeBuff = false;
                    MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                    World.UpdateSpawn(this);
                }
            if (Invisible)
                if (DateTime.Now > Invisibility.AddSeconds(20 + InvLevel * 5 * 2))
                {
                    Invisible = false;
                    MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                    World.UpdateSpawn(this);
                }
            if (MShieldBuff)
                if (DateTime.Now > MShielded.AddSeconds(20 + MShieldLevel * 5))
                {
                    MShieldBuff = false;
                    MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                    World.UpdateSpawn(this);
                }
            if (RobotBuff)
                if (DateTime.Now > RobotBuffed.AddSeconds(60))
                {
                    RobotBuff = false;
                    MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                    World.UpdateSpawn(this);
                }
            if (FreezeBuff)
                if (DateTime.Now > Freezed.AddSeconds(30))
                {
                    GetEquipStats(1, true);
                    GetEquipStats(2, true);
                    GetEquipStats(3, true);
                    GetEquipStats(4, true);
                    GetEquipStats(5, true);
                    GetEquipStats(6, true);
                    GetEquipStats(7, true);
                    GetEquipStats(8, true);
                    MinAtk = Str;
                    MaxAtk = Str;
                    GetEquipStats(1, false);
                    GetEquipStats(2, false);
                    GetEquipStats(3, false);
                    GetEquipStats(4, false);
                    GetEquipStats(5, false);
                    GetEquipStats(6, false);
                    GetEquipStats(7, false);
                    GetEquipStats(8, false);
                    FreezeBuff = false;
                    MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                    World.UpdateSpawn(this);
                }
            if (Restoring)
            {
                if (CurHP < MaxHP)
                {
                    CurHP += (ushort)(MaxHP / 15);
                    if (CurHP > MaxHP)
                        CurHP = MaxHP;

                    MyClient.SendPacket(General.MyPackets.Vital(UID, 0, CurHP));
                }
                if (DateTime.Now > RestoreTime.AddSeconds(50))
                {
                    Restoring = false;
                    MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                    World.UpdateSpawn(this);
                }
            }
            if (BurnBuff)
            {
                ushort BurnHP = (ushort)General.Rand.Next(1, 75);
                if (CurHP > BurnHP)
                    CurHP -= BurnHP;
                else
                {
                    int EModel;
                    if (Model == 1003 || Model == 1004)
                        EModel = 15099;
                    else
                        EModel = 15199;

                    Die();
                    CurHP = 0;
                    Death = DateTime.Now;
                    Alive = false;
                    XpList = false;
                    SMOn = false;
                    CycloneOn = false;
                    XpCircle = 0;
                    Flying = false;
                    BurnBuff = false;
                    Restoring = false;
                    FreezeBuff = false;
                    PoisonBuff = false;
                    MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                    MyClient.SendPacket(General.MyPackets.Status1(UID, EModel));
                    MyClient.SendPacket(General.MyPackets.Death(this));
                    World.UpdateSpawn(this);
                }
                MyClient.SendPacket(General.MyPackets.Vital(UID, 0, CurHP));

                if (DateTime.Now > Burned.AddSeconds(30))
                {
                    BurnBuff = false;
                    MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                    World.UpdateSpawn(this);
                }
            }
            if (PoisonBuff)
            {
                ushort PoisonHP = (ushort)General.Rand.Next(1, 50);
                if (CurHP > PoisonHP)
                    CurHP -= PoisonHP;
                else
                {
                    int EModel;
                    if (Model == 1003 || Model == 1004)
                        EModel = 15099;
                    else
                        EModel = 15199;

                    Die();
                    CurHP = 0;
                    Death = DateTime.Now;
                    Alive = false;
                    XpList = false;
                    SMOn = false;
                    CycloneOn = false;
                    XpCircle = 0;
                    Flying = false;
                    BurnBuff = false;
                    Restoring = false;
                    FreezeBuff = false;
                    PoisonBuff = false;
                    MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                    MyClient.SendPacket(General.MyPackets.Status1(UID, EModel));
                    MyClient.SendPacket(General.MyPackets.Death(this));
                    World.UpdateSpawn(this);
                }
                MyClient.SendPacket(General.MyPackets.Vital(UID, 0, CurHP));

                if (DateTime.Now > Poisoned.AddSeconds(30))
                {
                    PoisonBuff = false;
                    MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                    World.UpdateSpawn(this);
                }
            }
            if (ShieldBuff)
                if (DateTime.Now > Shielded.AddSeconds(120))
                {
                    ShieldBuff = false;
                    MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                    World.UpdateSpawn(this);
                }

            if (IronBuff)
                if (DateTime.Now > IronShirted.AddSeconds(10))
                {
                    IronBuff = false;
                    MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                    World.UpdateSpawn(this);
                }

            //----Lucky Time
            //--Gain LuckyTime--
            if (CastingPray)//Caster
                if (LuckTime < (2 * 60 * 60 * 1000))
                {
                    LuckTime += 1500;
                    if (LuckTime >= (2 * 60 * 60 * 1000))
                        LuckTime = (2 * 60 * 60 * 1000);

                    MyClient.SendPacket(General.MyPackets.Vital(UID, 29, LuckTime));
                }
            if (Praying)//Others
                if (LuckTime < (2 * 60 * 60 * 1000))
                {
                    LuckTime += 500;
                    if (LuckTime >= (2 * 60 * 60 * 1000))
                        LuckTime = (2 * 60 * 60 * 1000);

                    MyClient.SendPacket(General.MyPackets.Vital(UID, 29, LuckTime));
                }
            //------------------
            //--Lose LuckyTime--
            if (!Praying && !CastingPray)
                if (LuckTime > 0)
                {
                    LuckTime -= 500;
                    MyClient.SendPacket(General.MyPackets.Vital(UID, 29, LuckTime));
                }
            //------------------
            //--Caster Stops Luckytime--
            if (CastingPray)
            {
                if (LocX != PrayX || LocY != PrayY)
                {
                    CastingPray = false;
                    Praying = false;

                    MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                    World.UpdateSpawn(this);
                    MyClient.SendPacket(General.MyPackets.Vital(UID, 29, LuckTime));
                    PrayX = 0;
                    PrayY = 0;
                    //World.PlayersPraying.Remove(this);
                }
                else if (!Alive)
                {
                    CastingPray = false;
                    Praying = false;

                    MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                    World.UpdateSpawn(this);
                    MyClient.SendPacket(General.MyPackets.Vital(UID, 29, LuckTime));
                    PrayX = 0;
                    PrayY = 0;
                }
            }
            //--------------------------
            //--Others Start Praying--
            foreach (Character Caster in World.PlayersPraying)
            {
                if (LocMap == Caster.LocMap)
                    if (this != Caster)
                        if (Caster.CastingPray)
                            if ((MyMath.PointDistance(LocX, LocY, Caster.LocX, Caster.LocY) < 4) || (LocX == Caster.LocX && LocY == Caster.LocY))
                                if (!Praying && !CastingPray)
                                {
                                    Thread.Sleep(TimeSpan.FromSeconds(3));
                                    {
                                        if (!Mining)
                                        {
                                            Praying = true;
                                            MyClient.SendPacket(General.MyPackets.Vital(UID, 29, LuckTime));
                                            MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                                            World.UpdateSpawn(this);
                                        }
                                    }
                                }
            }
            //------------------------
            //--Others Stop Praying--
            foreach (Character Caster in World.PlayersPraying)
            {
                //if (LocMap == Caster.LocMap)
                if (this != Caster)
                    if (MyMath.PointDistance(LocX, LocY, Caster.LocX, Caster.LocY) > 3)
                    {
                        if (Praying)
                        {
                            Praying = false;
                            MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                            World.UpdateSpawn(this);
                            MyClient.SendPacket(General.MyPackets.Vital(UID, 29, LuckTime));
                        }
                    }
                    else if (!Caster.CastingPray)
                    {
                        if (Praying)
                        {
                            Praying = false;
                            MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                            World.UpdateSpawn(this);
                            MyClient.SendPacket(General.MyPackets.Vital(UID, 29, LuckTime));
                        }
                    }
            }
            //------------------------

            if (LocMap == 1038)
                if (DateTime.Now > LastGWList.AddMilliseconds(5000))
                {
                    LastGWList = DateTime.Now;
                    SendGuildWar();
                }
            if (Action == 250)
                if (Stamina < 100)
                {
                    Stamina += 8;
                    if (Stamina > 100)
                        Stamina = 100;
                    MyClient.SendPacket(General.MyPackets.Vital(UID, 9, Stamina));
                }
            if (Action == 100)
                if (Stamina < 100)
                {
                    if (Flying == false)
                        Stamina += 1;
                    if (Stamina > 100)
                        Stamina = 100;
                    MyClient.SendPacket(General.MyPackets.Vital(UID, 9, Stamina));
                }
            if (Action == 230)
            {
                if (Equips[3] != null)
                {
                    FullSuper();
                    ArmorSuper();
                    string TheEquip = Equips[3];
                    string[] Splitter = TheEquip.Split('-');
                    uint ItemId = uint.Parse(Splitter[0]);
                    if (AllSuper == false && OneSuper == true && Equips[3] != null)
                    {
                        if (Job <= 16 && Job >= 9 && AllSuper == false && Other.ItemQuality(ItemId) == 9 && Equips[3] != null)
                        {
                            foreach (DictionaryEntry DE in World.AllChars)
                            {
                                Character Chaar = (Character)DE.Value;
                                if (Chaar.Name != Name)
                                {
                                    Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "warrior-s"));
                                }
                            }
                            MyClient.SendPacket(General.MyPackets.String(UID, 10, "warrior-s"));
                            Action = 100;
                        }
                        if (Job <= 26 && Job >= 19 && AllSuper == false && Other.ItemQuality(ItemId) == 9 && Equips[3] != null)
                        {
                            foreach (DictionaryEntry DE in World.AllChars)
                            {
                                Character Chaar = (Character)DE.Value;
                                if (Chaar.Name != Name)
                                {
                                    Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "fighter-s"));
                                }
                            }
                            MyClient.SendPacket(General.MyPackets.String(UID, 10, "fighter-s"));
                            Action = 100;

                        }
                        if (Job <= 46 && Job >= 39 && AllSuper == false && Other.ItemQuality(ItemId) == 9 && Equips[3] != null)
                        {
                            foreach (DictionaryEntry DE in World.AllChars)
                            {
                                Character Chaar = (Character)DE.Value;
                                if (Chaar.Name != Name)
                                {
                                    Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "archer-s"));
                                }
                            }
                            MyClient.SendPacket(General.MyPackets.String(UID, 10, "archer-s"));
                            Action = 100;
                        }
                        if (Job <= 146 && Job >= 100 && AllSuper == false && Other.ItemQuality(ItemId) == 9 && Equips[3] != null)
                        {
                            foreach (DictionaryEntry DE in World.AllChars)
                            {
                                Character Chaar = (Character)DE.Value;
                                if (Chaar.Name != Name)
                                {
                                    Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "taoist-s"));
                                }
                            }
                            MyClient.SendPacket(General.MyPackets.String(UID, 10, "taoist-s"));
                            Action = 100;
                        }
                    }
                    if (AllSuper == true && Other.ItemQuality(ItemId) == 9 && Equips[1] != null && Equips[2] != null && Equips[3] != null && Equips[4] != null && Equips[8] != null && Equips[6] != null)
                    {
                        if (Job <= 16 && Job >= 9)
                        {
                            foreach (DictionaryEntry DE in World.AllChars)
                            {
                                Character Chaar = (Character)DE.Value;
                                if (Chaar.Name != Name)
                                {
                                    Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "warrior"));
                                }
                            }
                            MyClient.SendPacket(General.MyPackets.String(UID, 10, "warrior"));
                            Action = 100;
                        }
                        if (Job <= 26 && Job >= 19)
                        {
                            foreach (DictionaryEntry DE in World.AllChars)
                            {
                                Character Chaar = (Character)DE.Value;
                                if (Chaar.Name != Name)
                                {
                                    Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "fighter"));
                                }
                            }
                            MyClient.SendPacket(General.MyPackets.String(UID, 10, "fighter"));
                            Action = 100;

                        }
                        if (Job <= 46 && Job >= 39)
                        {
                            foreach (DictionaryEntry DE in World.AllChars)
                            {
                                Character Chaar = (Character)DE.Value;
                                if (Chaar.Name != Name)
                                {
                                    Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "archer"));
                                }
                            }
                            MyClient.SendPacket(General.MyPackets.String(UID, 10, "archer"));
                            Action = 100;
                        }
                        if (Job <= 146 && Job >= 100)
                        {
                            foreach (DictionaryEntry DE in World.AllChars)
                            {
                                Character Chaar = (Character)DE.Value;
                                if (Chaar.Name != Name)
                                {
                                    Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "taoist"));
                                }
                            }
                            MyClient.SendPacket(General.MyPackets.String(UID, 10, "taoist"));
                            Action = 100;
                        }
                    }
                }
            }

            if (LocMap == 1505)
                if (World.JailOn == false)
                    Teleport(1002, 430, 380);

            if (AccuracyOn)
                if (DateTime.Now > AccuracyActivated.AddSeconds(200))
                    AccuracyOn = false;

            if (Flying)
            {
                if (Equips[4] == null || Equips[4] == "0")
                {
                    if (Attacking == true)
                    {
                        Flying = false;
                        FlyType = 0;
                        MyClient.SendPacket(General.MyPackets.Vital(UID, 26, 8));
                        MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                        World.UpdateSpawn(this);
                        BanNB += 1;
                        DataBase.SaveBan(this);
                        World.SendMsgToAll(Name + " a �t� envoyez en prison pour avoir utilis� le bug de voler sans arc! Il/Elle a �t� banni " + BanNB + " fois. Attention si vous �tes banni trop souvent vous resterez en prison!", "SYSTEM", 2011);
                        Teleport(6001, 28, 71);
                    }
                }
                if (DateTime.Now > FlyActivated.AddSeconds(40 + FlyType * 20))
                {
                    Flying = false;
                    FlyType = 0;
                    MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                    MyClient.SendPacket(General.MyPackets.Vital(UID, 26, 8));
                    World.UpdateSpawn(this);
                }
            }

            if (DateTime.Now > LastSwing.AddMilliseconds(2500))
                SwingPickAxe();

            if (!CycloneOn)
            {
                if (AtkType == 2 || AtkType == 21)
                {
                    if (DateTime.Now > LastAttack.AddMilliseconds(1000))
                    {
                        if (Attacking)
                            Attack();
                    }
                }
                else if (Attacking)
                    Attack();
            }
            else if (PTarget != null && !PTarget.Flying || AtkType != 2 || PTarget == null)
                Attack();

            if (FirstJob == 25 && Job > 19 && Job < 26 || SecondJob == 25)
                ReflectOn = true;

            if (DateTime.Now > LastXPC.AddMilliseconds(3000))
                AddXPC();

            if (!Alive)
                if (!DeathSent)
                    if (DateTime.Now > Death.AddMilliseconds(1500))
                        Die();

            if (DateTime.Now > LastSave.AddMilliseconds(15000))
                Save();

            if (DateTime.Now > XPActivated.AddMilliseconds(ExtraXP))
                if (SMOn || CycloneOn)
                {
                    XPEnd();
                    if (KO >= 100)
                    {
                        World.SendMsgToAll(Name + " killed " + KO + " monsters with their XP skill! Check the website for KO Board rankings!", "SYSTEM", 2005);
                        SaveKO();
                        KO = 0;
                    }
                    else
                    {
                        if (KO >= 1)
                        {
                            MyClient.SendPacket(General.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "You killed " + KO + " monsters with their XP skill! Check the website for KO Board rankings!", 2005));
                            SaveKO();
                            KO = 0;
                        }
                    }
                }

            if (DateTime.Now > GotBlueName.AddMilliseconds(25000))
                if (BlueName)
                    BlueNameGone();

            if (DateTime.Now > LostPKP.AddMilliseconds(120000))
                if (PKPoints > 0)
                    PKTimer_Elapsed();

            if (DateTime.Now >= HBEnd && Blessed == true)
            {
                Blessed = false;
                HBEnd2 = "";
                WhichBless = 0;
                SaveHB();
                MyClient.SendPacket(General.MyPackets.Vital(UID, 18, 0));
                MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                World.UpdateSpawn(this);
            }
        }

        public Character()
        {
            TheTimer.Interval = 500;
            TheTimer.Elapsed += new ElapsedEventHandler(TimerElapsed);
            TheTimer.Start();
            Poisoned = DateTime.Now;
            LastAttack = DateTime.Now;
            LastXPC = DateTime.Now;
            Death = DateTime.Now;
            LastSave = DateTime.Now;
            LostPKP = DateTime.Now;


            WHIDs[0] = new uint[21];
            WHIDs[1] = new uint[21];
            WHIDs[2] = new uint[21];
            WHIDs[3] = new uint[21];
            WHIDs[4] = new uint[21];
            WHIDs[5] = new uint[41];
        }
        public void PKTimer_Elapsed()
        {
            LostPKP = DateTime.Now;
            if (PKPoints > 0)
            {
                PKPoints -= 1;
                MyClient.SendPacket(General.MyPackets.Vital(UID, 6, PKPoints));

                if ((PKPoints < 30 && PKPoints + 1 > 29) || (PKPoints < 100 && PKPoints + 1 > 99))
                {
                    MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                    World.UpdateSpawn(this);
                }
            }
        }

        public void ReBorn(byte ToJob)
        {
            FirstLevel = Level;
            FirstJob = Job;
            SaveFirstRB();

            try
            {
                RBCount = 1;

                #region FirstRbStats
                if ((Level == 110) || (Level == 111) && (Job == 135))
                {
                    StatP += 0;
                }
                else if ((Level == 112) || (Level == 113) && (Job == 135))
                {
                    StatP += 1;
                }
                else if ((Level == 114) || (Level == 115) && (Job == 135))
                {
                    StatP += 3;
                }
                else if ((Level == 116) || (Level == 117) && (Job == 135))
                {
                    StatP += 6;
                }
                else if ((Level == 118) || (Level == 119) && (Job == 135))
                {
                    StatP += 10;
                }
                else if (Level == 120)
                {
                    if (Job == 135)
                    {
                        StatP += 15;
                    }
                    else
                    {
                        StatP += 0;
                    }
                }
                else if (Level == 121)
                {
                    if (Job == 135)
                    {
                        StatP += 15;
                    }
                    else
                    {
                        StatP += 1;
                    }
                }
                else if (Level == 122)
                {
                    if (Job == 135)
                    {
                        StatP += 21;
                    }
                    else
                    {
                        StatP += 3;
                    }
                }
                else if (Level == 123)
                {
                    if (Job == 135)
                    {
                        StatP += 21;
                    }
                    else
                    {
                        StatP += 6;
                    }
                }
                else if (Level == 124)
                {
                    if (Job == 135)
                    {
                        StatP += 28;
                    }
                    else
                    {
                        StatP += 10;
                    }
                }
                else if (Level == 125)
                {
                    if (Job == 135)
                    {
                        StatP += 28;
                    }
                    else
                    {
                        StatP += 15;
                    }
                }
                else if (Level == 126)
                {
                    if (Job == 135)
                    {
                        StatP += 36;
                    }
                    else
                    {
                        StatP += 21;
                    }
                }
                else if (Level == 127)
                {
                    if (Job == 135)
                    {
                        StatP += 36;
                    }
                    else
                    {
                        StatP += 28;
                    }
                }
                else if (Level == 128)
                {
                    if (Job == 135)
                    {
                        StatP += 45;
                    }
                    else
                    {
                        StatP += 36;
                    }
                }
                else if (Level == 129)
                {
                    if (Job == 135)
                    {
                        StatP += 45;
                    }
                    else
                    {
                        StatP += 45;
                    }
                }
                else if (Level == 130)
                {
                    if (Job == 135)
                    {
                        StatP += 55;
                    }
                    else
                    {
                        StatP += 55;
                    }
                }
                else if (Level == 131)
                {
                    if (Job == 135)
                    {
                        StatP += 60;
                    }
                    else
                    {
                        StatP += 60;
                    }
                }
                else if (Level == 132)
                {
                    if (Job == 135)
                    {
                        StatP += 65;
                    }
                    else
                    {
                        StatP += 65;
                    }
                }
                else if (Level == 133)
                {
                    if (Job == 135)
                    {
                        StatP += 70;
                    }
                    else
                    {
                        StatP += 70;
                    }
                }
                else if (Level == 134)
                {
                    if (Job == 135)
                    {
                        StatP += 75;
                    }
                    else
                    {
                        StatP += 75;
                    }
                }
                else if (Level == 135)
                {
                    if (Job == 135)
                    {
                        StatP += 80;
                    }
                    else
                    {
                        StatP += 80;
                    }
                }
                else if (Level == 136)
                {
                    if (Job == 135)
                    {
                        StatP += 85;
                    }
                    else
                    {
                        StatP += 85;
                    }
                }
                else if (Level == 137)
                {
                    if (Job == 135)
                    {
                        StatP += 90;
                    }
                    else
                    {
                        StatP += 90;
                    }
                }
                #endregion
                MyClient.SendPacket(General.MyPackets.Vital(UID, 11, StatP));

                Level = 15;
                Exp = 0;
                Skills.Clear();
                Skill_Exps.Clear();

                #region Skills
                if (Job == 25)//Warrior
                {
                    FirstJob = 25;
                    if (ToJob == 41)//Archer
                    {
                        LearnSkill(1020, 0);//Shield
                        LearnSkill(1040, 0);//Roar
                        LearnSkill(4000, 1);//SummonGuard
                    }
                    if (ToJob == 11)//Trojan
                    {
                        LearnSkill(1015, 0);//Accuracy
                        LearnSkill(1040, 0);//Roar
                        LearnSkill(1320, 0);//FlyingMoon
                        LearnSkill(4000, 1);//SummonGuard
                    }
                    if (ToJob == 21)//Warrior
                    {
                        //LearnSkill(3060, 1);//Reflect
                        LearnSkill(4000, 1);//SummonGuard
                    }
                    if (ToJob == 142)//FireTaoist
                    {
                        LearnSkill(1020, 0);//Shield
                        LearnSkill(1040, 0);//Roar
                        LearnSkill(4000, 1);//SummonGuard
                    }
                    if (ToJob == 132)//WaterTaoist
                    {
                        LearnSkill(1025, 0);//Superman
                        LearnSkill(1020, 0);//Shield
                        LearnSkill(1040, 0);//Roar
                        LearnSkill(4000, 1);//SummonGuard
                    }
                }
                if (Job == 15)//Trojan
                {
                    FirstJob = 15;
                    if (ToJob == 41)//Archer
                    {
                        LearnSkill(1110, 0);//Cyclone
                        LearnSkill(1190, 1);//SpiritualHealing
                        LearnSkill(4000, 1);//SummonGuard
                    }
                    if (ToJob == 11)//Trojan
                    {
                        LearnSkill(3050, 1);//CruelShade
                        LearnSkill(4000, 1);//SummonGuard
                    }
                    if (ToJob == 21)//Warrior
                    {
                        LearnSkill(1110, 0);//Cyclone
                        LearnSkill(1190, 1);//SpiritualHealing
                        LearnSkill(5100, 0);//IronShirt
                        LearnSkill(4000, 1);//SummonGuard
                    }
                    if (ToJob == 142)//FireTaoist
                    {
                        LearnSkill(1110, 0);//Cyclone
                        LearnSkill(1190, 1);//SpiritualHealing
                        LearnSkill(1270, 1);//Robot
                        LearnSkill(4000, 1);//SummonGuard
                    }
                    if (ToJob == 132)//WaterTaoist
                    {
                        LearnSkill(1110, 0);//Cyclone
                        LearnSkill(1190, 1);//SpiritualHealing
                        LearnSkill(1270, 1);//Robot
                        LearnSkill(4000, 1);//SummonGuard
                    }
                }
                if (Job == 45)//Archer
                {
                    FirstJob = 45;
                    if (ToJob == 41)//Archer
                    {
                        LearnSkill(5000, 0);//FreezingArrow
                        LearnSkill(4000, 1);//SummonGuard
                    }
                    if (ToJob == 11)//Trojan
                    {
                        LearnSkill(5002, 0);//Poison
                        LearnSkill(4000, 1);//SummonGuard
                    }
                    if (ToJob == 21)//Warrior
                    {
                        LearnSkill(5002, 0);//Poison
                        LearnSkill(4000, 1);//SummonGuard
                    }
                    if (ToJob == 142)//FireTaoist
                    {
                        LearnSkill(5002, 0);//Poison
                        LearnSkill(4000, 1);//SummonGuard
                    }
                    if (ToJob == 132)//WaterTaoist
                    {
                        LearnSkill(5002, 0);//Poison
                        LearnSkill(4000, 1);//SummonGuard
                    }
                }
                if (Job == 145)//FireTaoist
                {
                    FirstJob = 145;
                    if (ToJob == 41)//Archer
                    {
                        LearnSkill(1000, 1);//Thunder
                        LearnSkill(1001, 1);//Fire
                        LearnSkill(1005, 1);//Cure
                        LearnSkill(1195, 1);//Meditation
                        LearnSkill(4000, 1);//SummonGuard
                    }
                    if (ToJob == 11)//Trojan
                    {
                        LearnSkill(1000, 1);//Thunder
                        LearnSkill(1001, 1);//Fire
                        LearnSkill(1005, 1);//Cure
                        LearnSkill(1195, 1);//Meditation
                        LearnSkill(4000, 1);//SummonGuard
                    }
                    if (ToJob == 21)//Warrior
                    {
                        LearnSkill(1000, 1);//Thunder
                        LearnSkill(1001, 1);//Fire
                        LearnSkill(1005, 1);//Cure
                        LearnSkill(1195, 1);//Meditation
                        LearnSkill(4000, 1);//SummonGuard
                    }
                    if (ToJob == 142)//FireTaoist
                    {
                        LearnSkill(3080, 1);//Dodge
                        LearnSkill(4000, 1);//SummonGuard
                    }
                    if (ToJob == 132)//WaterTaoist
                    {
                        LearnSkill(1120, 1);//FireCircle
                        LearnSkill(4000, 1);//SummonGuard
                    }
                }
                if (Job == 135)//WaterTaoist
                {
                    FirstJob = 135;
                    if (ToJob == 41)//Archer
                    {
                        LearnSkill(1005, 1);//Cure
                        LearnSkill(1095, 1);//Stigma
                        LearnSkill(1075, 1);//Invisibility
                        LearnSkill(1090, 1);//Magic Sheild
                        LearnSkill(1195, 1);//Meditation
                        LearnSkill(4000, 1);//SummonGuard
                        LearnSkill(1350, 1);//DivineHare
                    }
                    if (ToJob == 11)//Trojan
                    {
                        LearnSkill(1005, 1);//Cure
                        LearnSkill(1095, 1);//Stigma
                        LearnSkill(1085, 1);//Star of Accuracy
                        LearnSkill(1090, 1);//Magic Sheild
                        LearnSkill(1195, 1);//Meditation
                        LearnSkill(4000, 1);//SummonGuard
                        LearnSkill(1350, 1);//DivineHare
                    }
                    if (ToJob == 21)//Warrior
                    {
                        LearnSkill(1005, 1);//Cure
                        LearnSkill(1095, 1);//Stigma
                        LearnSkill(1085, 1);//Star of Accuracy
                        LearnSkill(1090, 1);//Magic Sheild
                        LearnSkill(1195, 1);//Meditation
                        LearnSkill(4000, 1);//SummonGuard
                        LearnSkill(1350, 1);//DivineHare
                    }
                    if (ToJob == 142)//FireTaoist
                    {
                        LearnSkill(1175, 1);//Adv. Cure
                        LearnSkill(1075, 1);//Invisibility
                        LearnSkill(1050, 0);//Revive
                        LearnSkill(1055, 1);//HealingRain
                        LearnSkill(4000, 1);//SummonGuard
                        LearnSkill(1350, 1);//DivineHare
                    }
                    if (ToJob == 132)//WaterTaoist
                    {
                        LearnSkill(3090, 1);//Pervade
                        LearnSkill(4000, 1);//SummonGuard
                        LearnSkill(1350, 1);//DivineHare
                    }
                }
                #endregion

                Job = ToJob;

                DataBase.GetStats(this);
                GetEquipStats(1, true);
                GetEquipStats(2, true);
                GetEquipStats(3, true);
                GetEquipStats(4, true);
                GetEquipStats(5, true);
                GetEquipStats(6, true);
                GetEquipStats(7, true);
                GetEquipStats(8, true);
                MinAtk = Str;
                MaxAtk = Str;
                MaxHP = BaseMaxHP();
                Potency = Level;
                GetEquipStats(1, false);
                GetEquipStats(2, false);
                GetEquipStats(3, false);
                GetEquipStats(4, false);
                GetEquipStats(5, false);
                GetEquipStats(6, false);
                GetEquipStats(7, false);
                GetEquipStats(8, false);
                CurHP = MaxHP;
                if (Job == 11)
                {
                    Str = 5;
                    Agi = 2;
                    Vit = 3;
                    Spi = 0;
                }
                if (Job == 21)
                {
                    Str = 5;
                    Agi = 2;
                    Vit = 3;
                    Spi = 0;
                }
                if (Job == 41)
                {
                    Str = 2;
                    Agi = 7;
                    Vit = 1;
                    Spi = 0;
                }
                if (Job == 132)
                {
                    Str = 0;
                    Agi = 2;
                    Vit = 3;
                    Spi = 5;
                }
                if (Job == 142)
                {
                    Str = 0;
                    Agi = 2;
                    Vit = 3;
                    Spi = 5;
                }
                AddItem("720028-0-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                MyClient.SendPacket(General.MyPackets.Vital((long)UID, 7, Job));
                MyClient.SendPacket(General.MyPackets.Vital((long)UID, 16, Str));
                MyClient.SendPacket(General.MyPackets.Vital((long)UID, 17, Agi));
                MyClient.SendPacket(General.MyPackets.Vital((long)UID, 15, Vit));
                MyClient.SendPacket(General.MyPackets.Vital((long)UID, 14, Spi));
                MyClient.SendPacket(General.MyPackets.Vital((long)UID, 2, MaxMana()));
                MyClient.SendPacket(General.MyPackets.GeneralData((long)UID, 0, 0, 0, 92));
                MyClient.SendPacket(General.MyPackets.Vital((long)UID, 0, CurHP));
                StatP += 30;
                StatP += 42;
                MyClient.SendPacket(General.MyPackets.Vital(UID, 11, StatP));

                #region Stuff
                for (byte i = 1; i < 9; i++)
                {
                    if (Equips[i] == null || Equips[i] == "") continue;
                    string I = Equips[i];
                    string[] II = I.Split('-');
                    uint IID = uint.Parse(II[0]);
                    byte Quality = (byte)Other.ItemQuality(IID);

                    if (i == 1)
                    {
                        string NewID = "";
                        if (Other.ItemInfo(IID)[0] >= 117493 && Other.ItemInfo(IID)[0] <= 117499)
                        {
                            NewID = II[0].Remove(4, 2);
                            NewID = 1173 + "0" + Quality.ToString();

                            Equips[i] = NewID + "-" + II[1] + "-" + II[2] + "-" + II[3] + "-" + II[4] + "-" + II[5];
                            II[0] = NewID;
                            MyClient.SendPacket(General.MyPackets.AddItem(Equips_UIDs[i], int.Parse(II[0]), byte.Parse(II[1]), byte.Parse(II[2]), byte.Parse(II[3]), byte.Parse(II[4]), byte.Parse(II[5]), i, 100, 100));
                        }
                        else
                        {
                            if (Other.WeaponType(IID) == 111 || Other.WeaponType(IID) == 113 || Other.WeaponType(IID) == 114 || Other.WeaponType(IID) == 118 || Other.WeaponType(IID) == 117)
                            {
                                NewID = II[0].Remove(4, 2);
                                NewID = NewID + "0" + Quality.ToString();

                                Equips[i] = NewID + "-" + II[1] + "-" + II[2] + "-" + II[3] + "-" + II[4] + "-" + II[5];
                                II[0] = NewID;
                                MyClient.SendPacket(General.MyPackets.AddItem(Equips_UIDs[i], int.Parse(II[0]), byte.Parse(II[1]), byte.Parse(II[2]), byte.Parse(II[3]), byte.Parse(II[4]), byte.Parse(II[5]), i, 100, 100));
                            }
                            else if (Other.WeaponType(IID) == 112)
                            {
                                byte Type = byte.Parse(II[0][4].ToString());
                                byte Color = byte.Parse(II[0][3].ToString());
                                NewID = "11" + Type.ToString() + Color.ToString() + "0" + Quality.ToString();
                                Equips[i] = NewID + "-" + II[1] + "-" + II[2] + "-" + II[3] + "-" + II[4] + "-" + II[5];
                                II[0] = NewID;
                                MyClient.SendPacket(General.MyPackets.AddItem(Equips_UIDs[i], int.Parse(II[0]), byte.Parse(II[1]), byte.Parse(II[2]), byte.Parse(II[3]), byte.Parse(II[4]), byte.Parse(II[5]), i, 100, 100));
                            }
                        }
                    }
                    else if (i == 2)
                    {
                        string NewID = "";

                        NewID = II[0].Remove(3, 3);
                        NewID += "00" + Quality.ToString();
                        Equips[i] = NewID + "-" + II[1] + "-" + II[2] + "-" + II[3] + "-" + II[4] + "-" + II[5];
                        II[0] = NewID;
                        MyClient.SendPacket(General.MyPackets.AddItem(Equips_UIDs[i], int.Parse(II[0]), byte.Parse(II[1]), byte.Parse(II[2]), byte.Parse(II[3]), byte.Parse(II[4]), byte.Parse(II[5]), i, 100, 100));
                    }
                    else if (i == 3)
                    {
                        string NewID = "";
                        if (Other.WeaponType(IID) == 130 || Other.WeaponType(IID) == 131 || Other.WeaponType(IID) == 133 || Other.WeaponType(IID) == 134)
                        {
                            NewID = II[0].Remove(4, 2);
                            NewID = NewID + "0" + Quality.ToString();

                            Equips[i] = NewID + "-" + II[1] + "-" + II[2] + "-" + II[3] + "-" + II[4] + "-" + II[5];
                            II[0] = NewID;
                            MyClient.SendPacket(General.MyPackets.AddItem(Equips_UIDs[i], int.Parse(II[0]), byte.Parse(II[1]), byte.Parse(II[2]), byte.Parse(II[3]), byte.Parse(II[4]), byte.Parse(II[5]), i, 100, 100));
                        }
                        else if (Other.WeaponType(IID) == 135 || Other.WeaponType(IID) == 136 || Other.WeaponType(IID) == 138 || Other.WeaponType(IID) == 139)
                        {
                            byte Type = byte.Parse(II[0][2].ToString());
                            byte Color = byte.Parse(II[0][3].ToString());
                            Type -= 5;
                            NewID = "13" + Type.ToString() + Color.ToString() + "0" + Quality.ToString();
                            Equips[i] = NewID + "-" + II[1] + "-" + II[2] + "-" + II[3] + "-" + II[4] + "-" + II[5];
                            II[0] = NewID;
                            MyClient.SendPacket(General.MyPackets.AddItem(Equips_UIDs[i], int.Parse(II[0]), byte.Parse(II[1]), byte.Parse(II[2]), byte.Parse(II[3]), byte.Parse(II[4]), byte.Parse(II[5]), i, 100, 100));
                        }
                    }
                    else if (i == 4)
                    {
                        string NewID = "";

                        NewID = II[0].Remove(3, 3);
                        NewID += "02" + Quality.ToString();
                        Equips[i] = NewID + "-" + II[1] + "-" + II[2] + "-" + II[3] + "-" + II[4] + "-" + II[5];
                        II[0] = NewID;
                        MyClient.SendPacket(General.MyPackets.AddItem(Equips_UIDs[i], int.Parse(II[0]), byte.Parse(II[1]), byte.Parse(II[2]), byte.Parse(II[3]), byte.Parse(II[4]), byte.Parse(II[5]), i, 100, 100));
                    }
                    else if (i == 5)
                    {
                        string NewID = "";

                        if (Other.WeaponType(IID) == 900)
                        {
                            NewID = II[0].Remove(4, 2);
                            NewID += "0" + Quality.ToString();
                            Equips[i] = NewID + "-" + II[1] + "-" + II[2] + "-" + II[3] + "-" + II[4] + "-" + II[5];
                            II[0] = NewID;
                            MyClient.SendPacket(General.MyPackets.AddItem(Equips_UIDs[i], int.Parse(II[0]), byte.Parse(II[1]), byte.Parse(II[2]), byte.Parse(II[3]), byte.Parse(II[4]), byte.Parse(II[5]), i, 100, 100));
                        }
                        else if (Other.ItemType(IID) == 4 || Other.ItemType(IID) == 5)
                        {
                            NewID = II[0].Remove(3, 3);
                            NewID += "02" + Quality.ToString();
                            Equips[i] = NewID + "-" + II[1] + "-" + II[2] + "-" + II[3] + "-" + II[4] + "-" + II[5];
                            II[0] = NewID;
                            MyClient.SendPacket(General.MyPackets.AddItem(Equips_UIDs[i], int.Parse(II[0]), byte.Parse(II[1]), byte.Parse(II[2]), byte.Parse(II[3]), byte.Parse(II[4]), byte.Parse(II[5]), i, 100, 100));
                        }
                    }
                    else if (i == 6)
                    {
                        string NewID = "";

                        NewID = II[0].Remove(3, 3);
                        NewID += "01" + Quality.ToString();
                        Equips[i] = NewID + "-" + II[1] + "-" + II[2] + "-" + II[3] + "-" + II[4] + "-" + II[5];
                        II[0] = NewID;
                        MyClient.SendPacket(General.MyPackets.AddItem(Equips_UIDs[i], int.Parse(II[0]), byte.Parse(II[1]), byte.Parse(II[2]), byte.Parse(II[3]), byte.Parse(II[4]), byte.Parse(II[5]), i, 100, 100));
                    }
                    else if (i == 8)
                    {
                        string NewID = "";

                        NewID = II[0].Remove(3, 3);
                        NewID += "01" + Quality.ToString();
                        Equips[i] = NewID + "-" + II[1] + "-" + II[2] + "-" + II[3] + "-" + II[4] + "-" + II[5];
                        II[0] = NewID;
                        MyClient.SendPacket(General.MyPackets.AddItem(Equips_UIDs[i], int.Parse(II[0]), byte.Parse(II[1]), byte.Parse(II[2]), byte.Parse(II[3]), byte.Parse(II[4]), byte.Parse(II[5]), i, 100, 100));
                    }

                }
                #endregion

                MyClient.SendPacket(General.MyPackets.Vital(UID, 13, Level));

                MyClient.SendPacket(General.MyPackets.String(UID, 10, "hitstar"));
                MyClient.SendPacket(General.MyPackets.String(UID, 10, "fastflash"));
                MyClient.SendPacket(General.MyPackets.String(UID, 10, "impulse"));
                MyClient.SendPacket(General.MyPackets.String(UID, 10, "minimeteor"));

                World.SendMsgToAll(Name + " a complet� la premi�re renaissance!", "SYSTEM", 2011);
                World.SendMsgToAll(Name + " a complet� la premi�re renaissance!", "SYSTEM", 2005);
                World.UpdateSpawn(this);
                Save();
                MyClient.Drop();
            }
            catch (Exception Exc) { Console.WriteLine(Exc); }
            World.UpdateSpawn(this);
        }
        public void SecReBorn(byte ToJob)
        {
            SecondLevel = Level;
            SecondJob = Job;
            SaveSecondRB();

            try
            {
                RBCount = 2;

                #region SecRbStats
                if ((Level == 110) || (Level == 111) && (Job == 135))
                {
                    StatP += 0;
                }
                else if ((Level == 112) || (Level == 113) && (Job == 135))
                {
                    StatP += 1;
                }
                else if ((Level == 114) || (Level == 115) && (Job == 135))
                {
                    StatP += 3;
                }
                else if ((Level == 116) || (Level == 117) && (Job == 135))
                {
                    StatP += 6;
                }
                else if ((Level == 118) || (Level == 119) && (Job == 135))
                {
                    StatP += 10;
                }
                else if (Level == 120)
                {
                    if (Job == 135)
                    {
                        StatP += 15;
                    }
                    else
                    {
                        StatP += 0;
                    }
                }
                else if (Level == 121)
                {
                    if (Job == 135)
                    {
                        StatP += 15;
                    }
                    else
                    {
                        StatP += 1;
                    }
                }
                else if (Level == 122)
                {
                    if (Job == 135)
                    {
                        StatP += 21;
                    }
                    else
                    {
                        StatP += 3;
                    }
                }
                else if (Level == 123)
                {
                    if (Job == 135)
                    {
                        StatP += 21;
                    }
                    else
                    {
                        StatP += 6;
                    }
                }
                else if (Level == 124)
                {
                    if (Job == 135)
                    {
                        StatP += 28;
                    }
                    else
                    {
                        StatP += 10;
                    }
                }
                else if (Level == 125)
                {
                    if (Job == 135)
                    {
                        StatP += 28;
                    }
                    else
                    {
                        StatP += 15;
                    }
                }
                else if (Level == 126)
                {
                    if (Job == 135)
                    {
                        StatP += 36;
                    }
                    else
                    {
                        StatP += 21;
                    }
                }
                else if (Level == 127)
                {
                    if (Job == 135)
                    {
                        StatP += 36;
                    }
                    else
                    {
                        StatP += 28;
                    }
                }
                else if (Level == 128)
                {
                    if (Job == 135)
                    {
                        StatP += 45;
                    }
                    else
                    {
                        StatP += 36;
                    }
                }
                else if (Level == 129)
                {
                    if (Job == 135)
                    {
                        StatP += 45;
                    }
                    else
                    {
                        StatP += 45;
                    }
                }
                else if (Level == 130)
                {
                    if (Job == 135)
                    {
                        StatP += 55;
                    }
                    else
                    {
                        StatP += 55;
                    }
                }
                else if (Level == 131)
                {
                    if (Job == 135)
                    {
                        StatP += 60;
                    }
                    else
                    {
                        StatP += 60;
                    }
                }
                else if (Level == 132)
                {
                    if (Job == 135)
                    {
                        StatP += 65;
                    }
                    else
                    {
                        StatP += 65;
                    }
                }
                else if (Level == 133)
                {
                    if (Job == 135)
                    {
                        StatP += 70;
                    }
                    else
                    {
                        StatP += 70;
                    }
                }
                else if (Level == 134)
                {
                    if (Job == 135)
                    {
                        StatP += 75;
                    }
                    else
                    {
                        StatP += 75;
                    }
                }
                else if (Level == 135)
                {
                    if (Job == 135)
                    {
                        StatP += 80;
                    }
                    else
                    {
                        StatP += 80;
                    }
                }
                else if (Level == 136)
                {
                    if (Job == 135)
                    {
                        StatP += 85;
                    }
                    else
                    {
                        StatP += 85;
                    }
                }
                else if (Level == 137)
                {
                    if (Job == 135)
                    {
                        StatP += 90;
                    }
                    else
                    {
                        StatP += 90;
                    }
                }
                #endregion
                #region FirstRbStats
                if ((FirstLevel == 110) || (FirstLevel == 111) && (FirstJob == 135))
                {
                    StatP += 0;
                }
                else if ((FirstLevel == 112) || (FirstLevel == 113) && (FirstJob == 135))
                {
                    StatP += 1;
                }
                else if ((FirstLevel == 114) || (FirstLevel == 115) && (FirstJob == 135))
                {
                    StatP += 3;
                }
                else if ((FirstLevel == 116) || (FirstLevel == 117) && (FirstJob == 135))
                {
                    StatP += 6;
                }
                else if ((FirstLevel == 118) || (FirstLevel == 119) && (FirstJob == 135))
                {
                    StatP += 10;
                }
                else if (FirstLevel == 120)
                {
                    if (FirstJob == 135)
                    {
                        StatP += 15;
                    }
                    else
                    {
                        StatP += 0;
                    }
                }
                else if (FirstLevel == 121)
                {
                    if (FirstJob == 135)
                    {
                        StatP += 15;
                    }
                    else
                    {
                        StatP += 1;
                    }
                }
                else if (FirstLevel == 122)
                {
                    if (FirstJob == 135)
                    {
                        StatP += 21;
                    }
                    else
                    {
                        StatP += 3;
                    }
                }
                else if (FirstLevel == 123)
                {
                    if (FirstJob == 135)
                    {
                        StatP += 21;
                    }
                    else
                    {
                        StatP += 6;
                    }
                }
                else if (FirstLevel == 124)
                {
                    if (FirstJob == 135)
                    {
                        StatP += 28;
                    }
                    else
                    {
                        StatP += 10;
                    }
                }
                else if (FirstLevel == 125)
                {
                    if (FirstJob == 135)
                    {
                        StatP += 28;
                    }
                    else
                    {
                        StatP += 15;
                    }
                }
                else if (FirstLevel == 126)
                {
                    if (FirstJob == 135)
                    {
                        StatP += 36;
                    }
                    else
                    {
                        StatP += 21;
                    }
                }
                else if (FirstLevel == 127)
                {
                    if (FirstJob == 135)
                    {
                        StatP += 36;
                    }
                    else
                    {
                        StatP += 28;
                    }
                }
                else if (FirstLevel == 128)
                {
                    if (FirstJob == 135)
                    {
                        StatP += 45;
                    }
                    else
                    {
                        StatP += 36;
                    }
                }
                else if (FirstLevel == 129)
                {
                    if (FirstJob == 135)
                    {
                        StatP += 45;
                    }
                    else
                    {
                        StatP += 45;
                    }
                }
                else if (FirstLevel == 130)
                {
                    if (FirstJob == 135)
                    {
                        StatP += 55;
                    }
                    else
                    {
                        StatP += 55;
                    }
                }
                else if (FirstLevel == 131)
                {
                    if (FirstJob == 135)
                    {
                        StatP += 60;
                    }
                    else
                    {
                        StatP += 60;
                    }
                }
                else if (FirstLevel == 132)
                {
                    if (FirstJob == 135)
                    {
                        StatP += 65;
                    }
                    else
                    {
                        StatP += 65;
                    }
                }
                else if (FirstLevel == 133)
                {
                    if (FirstJob == 135)
                    {
                        StatP += 70;
                    }
                    else
                    {
                        StatP += 70;
                    }
                }
                else if (FirstLevel == 134)
                {
                    if (FirstJob == 135)
                    {
                        StatP += 75;
                    }
                    else
                    {
                        StatP += 75;
                    }
                }
                else if (FirstLevel == 135)
                {
                    if (FirstJob == 135)
                    {
                        StatP += 80;
                    }
                    else
                    {
                        StatP += 80;
                    }
                }
                else if (FirstLevel == 136)
                {
                    if (FirstJob == 135)
                    {
                        StatP += 85;
                    }
                    else
                    {
                        StatP += 85;
                    }
                }
                else if (FirstLevel == 137)
                {
                    if (FirstJob == 135)
                    {
                        StatP += 90;
                    }
                    else
                    {
                        StatP += 90;
                    }
                }
                #endregion
                MyClient.SendPacket(General.MyPackets.Vital(UID, 11, StatP));

                Level = 15;
                Exp = 0;
                Skills.Clear();
                Skill_Exps.Clear();

                #region Skills
                if (FirstJob == 15)//Trojan
                {
                    if (Job == 15)//Trojan
                    {
                        if (ToJob == 11)//Trojan
                        {
                            LearnSkill(3050, 1);//CruelShade
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 21)//Warrior
                        {
                            LearnSkill(1110, 0);//Cyclone		
                            LearnSkill(3050, 1);//CruelShade
                            LearnSkill(1190, 1);//SpiritHealing
                            LearnSkill(1270, 1);//Robot
                            LearnSkill(5100, 0);//IronShirt
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 41)//Archer
                        {
                            LearnSkill(1110, 0);//Cyclone
                            LearnSkill(3050, 1);//CruelShade
                            LearnSkill(1190, 1);//SpiritHealing
                            LearnSkill(1270, 1);//Robot
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 132)//WaterTaoist
                        {
                            LearnSkill(1110, 0);//Cyclone
                            LearnSkill(3050, 1);//CruelShade
                            LearnSkill(1190, 1);//SpiritHealing
                            LearnSkill(1270, 1);//Robot
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 142)//FireTaoist
                        {
                            LearnSkill(1110, 0);//Cyclone
                            LearnSkill(3050, 1);//CruelShade
                            LearnSkill(1190, 1);//SpiritHealing
                            LearnSkill(1270, 1);//Robot
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                    }
                        if (Job == 25)//Warrior
                        {
                            if (ToJob == 11)//Trojan
                            {
                                LearnSkill(1110, 0);//Cyclone		
                                LearnSkill(5100, 0);//IronShirt
                                LearnSkill(1190, 1);//SpiritHealing
                                LearnSkill(1015, 0);//Accuracy
                                LearnSkill(1270, 1);//Robot
                                LearnSkill(1040, 0);//Roar
                                LearnSkill(1320, 1);//FlyingMoon
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 21)//Warrior
                            {
                                LearnSkill(1110, 0);//Cyclone		
                                LearnSkill(1190, 1);//SpiritHealing
                                LearnSkill(1270, 1);//Robot
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 41)//Archer
                            {
                                LearnSkill(1110, 0);//Cyclone
                                LearnSkill(1190, 1);//SpiritHealing
                                LearnSkill(5100, 0);//IronShirt
                                LearnSkill(1040, 0);//Roar
                                LearnSkill(1020, 0);//Shield
                                LearnSkill(1270, 1);//Robot
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 132)//WaterTaoist
                            {
                                LearnSkill(1110, 0);//Cyclone
                                LearnSkill(1190, 1);//SpiritHealing
                                LearnSkill(5100, 0);//IronShirt
                                LearnSkill(1040, 0);//Roar
                                LearnSkill(1020, 0);//Shield
                                LearnSkill(1270, 1);//Robot
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 142)//FireTaoist
                            {
                                LearnSkill(1110, 0);//Cyclone
                                LearnSkill(1190, 1);//SpiritHealing
                                LearnSkill(5100, 0);//IronShirt
                                LearnSkill(1040, 0);//Roar
                                LearnSkill(1020, 0);//Shield
                                LearnSkill(1270, 1);//Robot
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                        }
                        if (Job == 45)//Archer
                        {
                            if (ToJob == 11)//Trojan
                            {
                                LearnSkill(1110, 0);//Cyclone
                                LearnSkill(1190, 1);//SpiritHealing
                                LearnSkill(1270, 1);//Robot
                                LearnSkill(5002, 0);//Poison
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 21)//Warrior
                            {
                                LearnSkill(1110, 0);//Cyclone
                                LearnSkill(1190, 1);//SpiritHealing
                                LearnSkill(1270, 1);//Robot
                                LearnSkill(5002, 0);//Poison
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 41)//Archer
                            {
                                LearnSkill(1110, 0);//Cyclone
                                LearnSkill(1190, 1);//SpiritHealing
                                LearnSkill(1270, 1);//Robot
                                LearnSkill(5002, 0);//Poison
                                LearnSkill(5000, 0);//FreezingArrow
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 132)//WaterTaoist
                            {
                                LearnSkill(1110, 0);//Cyclone
                                LearnSkill(1190, 1);//SpiritHealing
                                LearnSkill(1270, 1);//Robot
                                LearnSkill(5002, 0);//Poison
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 142)//FireTaoist
                            {
                                LearnSkill(1110, 0);//Cyclone
                                LearnSkill(1190, 1);//SpiritHealing
                                LearnSkill(1270, 1);//Robot
                                LearnSkill(5002, 0);//Poison
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                        }
                        if (Job == 135)//WaterTaoist
                        {
                            if (ToJob == 11)//Trojan
                            {
                                LearnSkill(1270, 1);//Robot
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1085, 1);//StarofAccuracy
                                LearnSkill(1090, 1);//MagicShield
                                LearnSkill(1095, 1);//Stigma
                                LearnSkill(1195, 1);//Meditation
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 21)//Warrior
                            {
                                LearnSkill(1110, 0);//Cyclone
                                LearnSkill(1190, 1);//SpiritHealing
                                LearnSkill(1270, 1);//Robot
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1085, 1);//StarofAccuracy
                                LearnSkill(1090, 1);//MagicShield
                                LearnSkill(1095, 1);//Stigma
                                LearnSkill(1195, 1);//Meditation
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 41)//Archer
                            {
                                LearnSkill(1110, 0);//Cyclone
                                LearnSkill(1190, 1);//SpiritHealing
                                LearnSkill(1270, 1);//Robot
                                LearnSkill(1005, 1);//Cure
                                LearnSkill(1075, 1);//Invisibility
                                LearnSkill(1090, 1);//MagicShield
                                LearnSkill(1095, 1);//Stigma
                                LearnSkill(1195, 1);//Meditation
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 132)//WaterTaoist
                            {
                                LearnSkill(1110, 0);//Cyclone
                                LearnSkill(1190, 1);//SpiritHealing
                                LearnSkill(1270, 1);//Robot
                                LearnSkill(3090, 1);//Pervade	
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                            if (ToJob == 142)//FireTaoist
                            {
                                LearnSkill(1110, 0);//Cyclone
                                LearnSkill(1190, 1);//SpiritHealing
                                LearnSkill(1270, 1);//Robot
                                LearnSkill(1050, 0);//Revive
                                LearnSkill(1175, 1);//AdvancedCure
                                LearnSkill(1075, 1);//Invisibility
                                LearnSkill(1170, 1);//Nectar
                                LearnSkill(4000, 1);//SummonGuard
                                LearnSkill(9876, 0);//GodBlessing
                            }
                        }
                            if (Job == 145)//FireTaoist
                            {
                                if (ToJob == 11)//Trojan
                                {
                                    LearnSkill(1270, 1);//Robot
                                    LearnSkill(1000, 1);//Thunder
                                    LearnSkill(1001, 1);//Fire
                                    LearnSkill(1005, 1);//Cure
                                    LearnSkill(1195, 1);//Meditation
                                    LearnSkill(4000, 1);//SummonGuard
                                    LearnSkill(9876, 0);//GodBlessing
                                }
                                if (ToJob == 21)//Warrior
                                {
                                    LearnSkill(1270, 1);//Robot
                                    LearnSkill(1000, 1);//Thunder
                                    LearnSkill(1001, 1);//Fire
                                    LearnSkill(1005, 1);//Cure
                                    LearnSkill(1195, 1);//Meditation
                                    LearnSkill(4000, 1);//SummonGuard
                                    LearnSkill(9876, 0);//GodBlessing
                                }
                                if (ToJob == 41)//Archer
                                {
                                    LearnSkill(1110, 0);//Cyclone
                                    LearnSkill(1190, 1);//SpiritHealing
                                    LearnSkill(1270, 1);//Robot
                                    LearnSkill(1000, 1);//Thunder
                                    LearnSkill(1001, 1);//Fire
                                    LearnSkill(1005, 1);//Cure
                                    LearnSkill(1195, 1);//Meditation
                                    LearnSkill(4000, 1);//SummonGuard
                                    LearnSkill(9876, 0);//GodBlessing
                                }
                                if (ToJob == 132)//WaterTaoist
                                {
                                    LearnSkill(1110, 0);//Cyclone
                                    LearnSkill(1190, 1);//SpiritHealing
                                    LearnSkill(1270, 1);//Robot
                                    LearnSkill(1120, 1);//FireCircle
                                    LearnSkill(4000, 1);//SummonGuard
                                    LearnSkill(9876, 0);//GodBlessing
                                }
                                if (ToJob == 142)//FireTaoist
                                {
                                    LearnSkill(1110, 0);//Cyclone
                                    LearnSkill(1190, 1);//SpiritHealing
                                    LearnSkill(1270, 1);//Robot
                                    LearnSkill(3080, 0);//Dodge	
                                    LearnSkill(4000, 1);//SummonGuard
                                    LearnSkill(9876, 0);//GodBlessing
                                }
                            }
                }
                if (FirstJob == 25)//Warrior
                {
                    if (Job == 15)//Trojan
                    {
                        if (ToJob == 11)//Trojan
                        {
                            LearnSkill(1320, 0);//Flying Moon
                            LearnSkill(1040, 0);//Roar
                            LearnSkill(3050, 1);//CruelShade
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 21)//Warrior
                        {
                            LearnSkill(1110, 0);//Cyclone
                            LearnSkill(1190, 1);//SpiritHealing
                            LearnSkill(5100, 0);//IronShirt
                            LearnSkill(1270, 1);//Robot
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 41)//Archer
                        {
                            LearnSkill(1320, 0);//FlyingMoon
                            LearnSkill(1040, 0);//Roar
                            LearnSkill(1110, 0);//Cyclone
                            LearnSkill(1190, 1);//SpiritHealing
                            LearnSkill(1270, 1);//Robot
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 142)//FireTao
                        {
                            LearnSkill(1320, 0);//FlyingMoon
                            LearnSkill(1040, 0);//Roar
                            LearnSkill(1110, 0);//Cyclone
                            LearnSkill(1190, 1);//SpiritHealing
                            LearnSkill(1270, 1);//Robot
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 132)//WaterTao
                        {
                            LearnSkill(1320, 0);//FlyingMoon
                            LearnSkill(1040, 0);//Roar
                            LearnSkill(1110, 0);//Cyclone
                            LearnSkill(1190, 1);//SpiritHealing
                            LearnSkill(1270, 1);//Robot
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                    }
                    if (Job == 25)//Warrior
                    {
                        if (ToJob == 11)//Trojan
                        {
                            LearnSkill(1015, 0);//Accuracy
                            LearnSkill(1040, 0);//Roar
                            LearnSkill(1320, 0);//FlyingMoon
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 21)//Warrior
                        {
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 41)//Archer
                        {
                            LearnSkill(1020, 0);//Shield
                            LearnSkill(1040, 0);//Roar
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 142)//FireTao
                        {
                            LearnSkill(1025, 0);//SuperMan
                            LearnSkill(1020, 0);//Shield
                            LearnSkill(1040, 0);//Roar
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 132)//WaterTao
                        {
                            LearnSkill(1020, 0);//Shield
                            LearnSkill(1040, 0);//Roar
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                    }
                    if (Job == 45)//Archer
                    {
                        if (ToJob == 11)//Trojan
                        {
                            LearnSkill(1040, 0);//Roar
                            LearnSkill(5002, 0);//Poison
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 21)//Warrior
                        {
                            LearnSkill(5002, 0);//Poison
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 41)//Archer
                        {
                            LearnSkill(5000, 0);//FreezingArrow
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 142)//FireTao
                        {
                            LearnSkill(1020, 0);//Shield
                            LearnSkill(1040, 0);//Roar
                            LearnSkill(5000, 0);//Poison
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 132)//WaterTao
                        {
                            LearnSkill(1020, 0);//Shield
                            LearnSkill(1040, 0);//Roar
                            LearnSkill(5000, 0);//Poison
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                    }
                    if (Job == 135)//WaterTao
                    {
                        if (ToJob == 11)//Trojan
                        {
                            LearnSkill(1005, 1);//Cure
                            LearnSkill(1085, 1);//StarOfAccuracy
                            LearnSkill(1090, 1);//MagicShield
                            LearnSkill(1095, 1);//Stigma
                            LearnSkill(1195, 1);//M�ditation
                            LearnSkill(1040, 0);//Roar
                            LearnSkill(1280, 1);//WaterElf
                            LearnSkill(1350, 1);//DivineHare
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 21)//Warrior
                        {
                            LearnSkill(1005, 1);//Cure
                            LearnSkill(1085, 1);//StarOfAccuracy
                            LearnSkill(1090, 1);//MagicShield
                            LearnSkill(1095, 1);//Stigma
                            LearnSkill(1195, 1);//M�ditation
                            LearnSkill(1280, 1);//WaterElf
                            LearnSkill(1350, 1);//DivineHare
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 41)//Archer
                        {
                            LearnSkill(1020, 0);//Shield
                            LearnSkill(1040, 0);//Roar
                            LearnSkill(1005, 1);//Cure
                            LearnSkill(1075, 1);//Invisibility
                            LearnSkill(1090, 1);//MagicShield
                            LearnSkill(1095, 1);//Stigma
                            LearnSkill(1195, 1);//M�ditation
                            LearnSkill(1280, 1);//WaterElf
                            LearnSkill(1350, 1);//DivineHare
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 142)//FireTao
                        {
                            LearnSkill(1020, 0);//Shield
                            LearnSkill(1040, 0);//Roar
                            LearnSkill(1050, 0);//XPRevive
                            LearnSkill(1175, 1);//AdvancedCure
                            LearnSkill(1075, 1);//Invisibility
                            LearnSkill(1055, 1);//HealingRain
                            LearnSkill(1280, 1);//WaterElf
                            LearnSkill(1350, 1);//DivineHare
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 132)//WaterTao
                        {
                            LearnSkill(1025, 0);//SuperMan
                            LearnSkill(1020, 0);//Shield
                            LearnSkill(1040, 0);//Roar
                            LearnSkill(1280, 1);//WaterElf
                            LearnSkill(1350, 1);//DivineHare
                            LearnSkill(3090, 1);//Pervade
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                    }
                    if (Job == 145)//FireTao
                    {
                        if (ToJob == 11)//Trojan
                        {
                            LearnSkill(1005, 1);//Cure
                            LearnSkill(1195, 1);//M�ditation
                            LearnSkill(1040, 0);//Roar
                            LearnSkill(1000, 1);//Thunder
                            LearnSkill(1001, 1);//Fire
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 21)//Warrior
                        {
                            LearnSkill(1005, 1);//Cure
                            LearnSkill(1195, 1);//M�ditation
                            LearnSkill(1000, 1);//Thunder
                            LearnSkill(1001, 1);//Fire
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 41)//Archer
                        {
                            LearnSkill(1005, 1);//Cure
                            LearnSkill(1195, 1);//M�ditation
                            LearnSkill(1040, 0);//Roar
                            LearnSkill(1000, 1);//Thunder
                            LearnSkill(1001, 1);//Fire
                            LearnSkill(1020, 0);//Shield
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 142)//FireTao
                        {
                            LearnSkill(1020, 0);//Shield
                            LearnSkill(1040, 0);//Roar
                            LearnSkill(3080, 0);//Dodge
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 132)//WaterTao
                        {
                            LearnSkill(1020, 0);//Shield
                            LearnSkill(1040, 0);//Roar
                            LearnSkill(1120, 1);//FireCicle
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                    }
                }
                if (FirstJob == 45)//Archer
                {
                    if (Job == 15)//Trojan
                    {
                        if (ToJob == 11)//Trojan
                        {
                            LearnSkill(5002, 0);//Poison
                            LearnSkill(3050, 1);//CruelShade
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 21)//Warrior
                        {
                            LearnSkill(5002, 0);//Poison
                            LearnSkill(1110, 0);//Cyclone
                            LearnSkill(1190, 1);//SpiritHealing
                            LearnSkill(5100, 0);//IronShirt
                            LearnSkill(1270, 1);//Robot			
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 41)//Archer
                        {
                            LearnSkill(5002, 0);//Poison
                            LearnSkill(1110, 0);//Cyclone
                            LearnSkill(1190, 1);//SpiritHealing
                            LearnSkill(1270, 1);//Robot			
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 132)//WaterTaoist
                        {
                            LearnSkill(5002, 0);//Poison
                            LearnSkill(1110, 0);//Cyclone
                            LearnSkill(1190, 1);//SpiritHealing
                            LearnSkill(1270, 1);//Robot			
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 142)//FireTaoist
                        {
                            LearnSkill(5002, 0);//Poison
                            LearnSkill(1110, 0);//Cyclone
                            LearnSkill(1190, 1);//SpiritHealing
                            LearnSkill(1270, 1);//Robot			
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                    }
                    if (Job == 25)//Warrior
                    {
                        if (ToJob == 11)//Trojan
                        {
                            LearnSkill(5002, 0);//Poison
                            LearnSkill(1015, 0);//Accuracy
                            LearnSkill(1040, 0);//Roar
                            LearnSkill(1320, 1);//FlyingMoon			
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 21)//Warrior
                        {
                            LearnSkill(5002, 0);//Poison		
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 41)//Archer
                        {
                            LearnSkill(5002, 0);//Poison
                            LearnSkill(1020, 0);//Shield
                            LearnSkill(1040, 0);//Roar		
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 132)//WaterTaoist
                        {
                            LearnSkill(5002, 0);//Poison
                            LearnSkill(1025, 0);//Superman			
                            LearnSkill(1020, 0);//Shield
                            LearnSkill(1040, 0);//Roar		
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 142)//FireTaoist
                        {
                            LearnSkill(5002, 0);//Poison
                            LearnSkill(1020, 0);//Shield
                            LearnSkill(1040, 0);//Roar		
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                    }
                    if (Job == 45)//Archer
                    {
                        if (ToJob == 11)//Trojan
                        {
                            LearnSkill(5002, 0);//Poison
                            LearnSkill(5000, 0);//FreezingArrow			
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 21)//Warrior
                        {
                            LearnSkill(5002, 0);//Poison
                            LearnSkill(5000, 0);//FreezingArrow			
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 41)//Archer
                        {
                            LearnSkill(5000, 0);//FreezingArrow			
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 132)//WaterTaoist
                        {
                            LearnSkill(5002, 0);//Poison
                            LearnSkill(5000, 0);//FreezingArrow			
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 142)//FireTaoist
                        {
                            LearnSkill(5002, 0);//Poison
                            LearnSkill(5000, 0);//FreezingArrow			
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                    }
                    if (Job == 135)//WaterTaoist
                    {
                        if (ToJob == 11)//Trojan
                        {
                            LearnSkill(1005, 1);//Cure
                            LearnSkill(1085, 1);//StarofAccuracy
                            LearnSkill(1090, 1);//MagicShield
                            LearnSkill(1095, 1);//Stigma
                            LearnSkill(1195, 1);//Meditation
                            LearnSkill(5002, 0);//Poison
                            LearnSkill(1280, 1);//WaterElf			
                            LearnSkill(1350, 1);//DivineHare		
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 21)//Warrior
                        {
                            LearnSkill(1005, 1);//Cure
                            LearnSkill(1085, 1);//StarofAccuracy
                            LearnSkill(1090, 1);//MagicShield
                            LearnSkill(1095, 1);//Stigma
                            LearnSkill(1195, 1);//Meditation
                            LearnSkill(5002, 0);//Poison
                            LearnSkill(1280, 1);//WaterElf			
                            LearnSkill(1350, 1);//DivineHare		
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 41)//Archer
                        {
                            LearnSkill(1005, 1);//Cure
                            LearnSkill(1075, 1);//Invisibility
                            LearnSkill(1090, 1);//MagicShield
                            LearnSkill(1095, 1);//Stigma
                            LearnSkill(1195, 1);//Meditation
                            LearnSkill(5002, 0);//Poison
                            LearnSkill(1280, 1);//WaterElf			
                            LearnSkill(1350, 1);//DivineHare		
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 132)//WaterTaoist
                        {
                            LearnSkill(5002, 0);//Poison
                            LearnSkill(1280, 1);//WaterElf			
                            LearnSkill(1350, 1);//DivineHare
                            LearnSkill(3090, 1);//Pervade			
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 142)//FireTaoist
                        {
                            LearnSkill(1050, 0);//Revive
                            LearnSkill(1075, 1);//Invisibility
                            LearnSkill(5002, 0);//Poison
                            LearnSkill(1055, 1);//HealingRain			
                            LearnSkill(1175, 1);//AdvancedCure			
                            LearnSkill(1280, 1);//WaterElf			
                            LearnSkill(1350, 1);//DivineHare			
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                    }
                    if (Job == 145)//FireTaoist
                    {
                        if (ToJob == 11)//Trojan
                        {
                            LearnSkill(1005, 1);//Cure	
                            LearnSkill(1195, 1);//Meditation	
                            LearnSkill(5002, 0);//Poison
                            LearnSkill(1000, 1);//Thunder	
                            LearnSkill(1001, 1);//Fire			
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 21)//Warrior
                        {
                            LearnSkill(1005, 1);//Cure	
                            LearnSkill(1195, 1);//Meditation	
                            LearnSkill(5002, 0);//Poison
                            LearnSkill(1000, 1);//Thunder	
                            LearnSkill(1001, 1);//Fire			
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 41)//Archer
                        {
                            LearnSkill(1005, 1);//Cure	
                            LearnSkill(1195, 1);//Meditation	
                            LearnSkill(5002, 0);//Poison
                            LearnSkill(1000, 1);//Thunder	
                            LearnSkill(1001, 1);//Fire			
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 132)//WaterTaoist
                        {
                            LearnSkill(5002, 0);//Poison
                            LearnSkill(1120, 1);//FireCircle		
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 142)//FireTaoist
                        {
                            LearnSkill(5002, 0);//Poison
                            LearnSkill(3080, 0);//Dodge		
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                    }
                }
                if (FirstJob == 135)//WaterTao
                {

                    if (Job == 15)//Trojan
                    {
                        if (ToJob == 11)//Trojan
                        {
                            LearnSkill(1005, 1);//Cure
                            LearnSkill(1085, 1);//StarOfAccuracy
                            LearnSkill(1090, 1);//MagicShield
                            LearnSkill(1095, 1);//Stigma
                            LearnSkill(1195, 1);//Meditadtion
                            LearnSkill(3050, 1);//CruelShade
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 21)//Warrior
                        {
                            LearnSkill(1005, 1);//Cure
                            LearnSkill(1085, 1);//StarOfAccuracy
                            LearnSkill(1090, 1);//MagicShield
                            LearnSkill(1095, 1);//Stigma
                            LearnSkill(1195, 1);//Meditadtion
                            LearnSkill(1190, 1);//SpiritHealing
                            LearnSkill(5100, 0);//IronShirt
                            LearnSkill(1270, 1);//Robot
                            LearnSkill(1110, 0);//Cyclone
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 41)//Archer
                        {
                            LearnSkill(1005, 1);//Cure
                            LearnSkill(1085, 1);//StarOfAccuracy
                            LearnSkill(1090, 1);//MagicShield
                            LearnSkill(1095, 1);//Stigma
                            LearnSkill(1195, 1);//Meditadtion
                            LearnSkill(1190, 1);//SpiritHealing
                            LearnSkill(1270, 1);//Robot
                            LearnSkill(1110, 0);//Cyclone
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 142)//FireTao
                        {
                            LearnSkill(1005, 1);//Cure
                            LearnSkill(1085, 1);//StarOfAccuracy
                            LearnSkill(1090, 1);//MagicShield
                            LearnSkill(1095, 1);//Stigma
                            LearnSkill(1195, 1);//Meditadtion
                            LearnSkill(1190, 1);//SpiritHealing
                            LearnSkill(1270, 1);//Robot
                            LearnSkill(1110, 0);//Cyclone
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 132)//WaterTao
                        {
                            LearnSkill(1005, 1);//Cure
                            LearnSkill(1085, 1);//StarOfAccuracy
                            LearnSkill(1090, 1);//MagicShield
                            LearnSkill(1095, 1);//Stigma
                            LearnSkill(1195, 1);//Meditadtion
                            LearnSkill(1190, 1);//SpiritHealing
                            LearnSkill(1270, 1);//Robot
                            LearnSkill(1110, 0);//Cyclone
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                    }
                    if (Job == 25)//Warrior
                    {
                        if (ToJob == 11)//Trojan
                        {
                            LearnSkill(1005, 1);//Cure
                            LearnSkill(1085, 1);//StarOfAccuracy
                            LearnSkill(1090, 1);//MagicShield
                            LearnSkill(1095, 1);//Stigma
                            LearnSkill(1195, 1);//Meditadtion
                            LearnSkill(1015, 0);//Accuracy
                            LearnSkill(1040, 0);//Roar
                            LearnSkill(1320, 1);//FlyingMoon
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 21)//Warrior
                        {
                            LearnSkill(1005, 1);//Cure
                            LearnSkill(1085, 1);//StarOfAccuracy
                            LearnSkill(1090, 1);//MagicShield
                            LearnSkill(1095, 1);//Stigma
                            LearnSkill(1195, 1);//Meditadtion
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 41)//Archer
                        {
                            LearnSkill(1005, 1);//Cure
                            LearnSkill(1085, 1);//StarOfAccuracy
                            LearnSkill(1090, 1);//MagicShield
                            LearnSkill(1095, 1);//Stigma
                            LearnSkill(1195, 1);//Meditadtion
                            LearnSkill(1020, 0);//Shield
                            LearnSkill(1040, 0);//Roar
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 142)//FireTao
                        {
                            LearnSkill(1005, 1);//Cure
                            LearnSkill(1085, 1);//StarOfAccuracy
                            LearnSkill(1090, 1);//MagicShield
                            LearnSkill(1095, 1);//Stigma
                            LearnSkill(1195, 1);//Meditadtion
                            LearnSkill(1020, 0);//Shield
                            LearnSkill(1040, 0);//Roar
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 132)//WaterTao
                        {
                            LearnSkill(1005, 1);//Cure
                            LearnSkill(1085, 1);//StarOfAccuracy
                            LearnSkill(1090, 1);//MagicShield
                            LearnSkill(1095, 1);//Stigma
                            LearnSkill(1195, 1);//Meditadtion
                            LearnSkill(1025, 0);//Superman
                            LearnSkill(1020, 0);//Shield
                            LearnSkill(1040, 0);//Roar
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                    }
                    if (Job == 45)//Archer
                    {
                        if (ToJob == 11)//Trojan
                        {
                            LearnSkill(1005, 1);//Cure
                            LearnSkill(1075, 1);//Invisibility
                            LearnSkill(1090, 1);//MagicShield
                            LearnSkill(1095, 1);//Stigma
                            LearnSkill(1195, 1);//Meditation
                            LearnSkill(5002, 0);//Poison
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 21)//Warrior
                        {
                            LearnSkill(1005, 1);//Cure
                            LearnSkill(1075, 1);//Invisibility
                            LearnSkill(1090, 1);//MagicShield
                            LearnSkill(1095, 1);//Stigma
                            LearnSkill(1195, 1);//Meditation
                            LearnSkill(5002, 0);//Poison
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 41)//Archer
                        {
                            LearnSkill(1005, 1);//Cure
                            LearnSkill(1075, 1);//Invisibility
                            LearnSkill(1090, 1);//MagicShield
                            LearnSkill(1095, 1);//Stigma
                            LearnSkill(1195, 1);//Meditation
                            LearnSkill(5000, 0);//FreezingArrow
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 142)//FireTao
                        {
                            LearnSkill(1005, 1);//Cure
                            LearnSkill(1075, 1);//Invisibility
                            LearnSkill(1090, 1);//MagicShield
                            LearnSkill(1095, 1);//Stigma
                            LearnSkill(1195, 1);//Meditation
                            LearnSkill(5002, 0);//Poison
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 132)//WaterTao
                        {
                            LearnSkill(1005, 1);//Cure
                            LearnSkill(1075, 1);//Invisibility
                            LearnSkill(1090, 1);//MagicShield
                            LearnSkill(1095, 1);//Stigma
                            LearnSkill(1195, 1);//Meditation
                            LearnSkill(5002, 0);//Poison
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                    }
                    if (Job == 135)//WaterTao
                    {
                        if (ToJob == 11)//Trojan
                        {
                            LearnSkill(1005, 1);//Cure
                            LearnSkill(1085, 1);//StarOfAccuracy
                            LearnSkill(1090, 1);//MagicShield
                            LearnSkill(1095, 1);//Stigma
                            LearnSkill(1195, 1);//Meditadtion
                            LearnSkill(3090, 1);//Pervade
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 21)//Warrior
                        {
                            LearnSkill(1005, 1);//Cure
                            LearnSkill(1085, 1);//StarOfAccuracy
                            LearnSkill(1090, 1);//MagicShield
                            LearnSkill(1095, 1);//Stigma
                            LearnSkill(1195, 1);//Meditadtion
                            LearnSkill(3090, 1);//Pervade
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 41)//Archer
                        {
                            LearnSkill(1005, 1);//Cure
                            LearnSkill(1085, 1);//StarOfAccuracy
                            LearnSkill(1090, 1);//MagicShield
                            LearnSkill(1095, 1);//Stigma
                            LearnSkill(1195, 1);//Meditadtion
                            LearnSkill(3090, 1);//Pervade
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 142)//FireTao
                        {
                            LearnSkill(1050, 0);//Revive
                            LearnSkill(1075, 1);//Invisibility
                            LearnSkill(1055, 1);//HealingRain
                            LearnSkill(1175, 1);//AdvancedCure
                            LearnSkill(3090, 1);//Pervade
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 132)//WaterTao
                        {
                            LearnSkill(3090, 1);//Pervade
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                    }
                    if (Job == 145)//FireTao
                    {
                        if (ToJob == 11)//Trojan
                        {
                            LearnSkill(1050, 0);//Revive
                            LearnSkill(1175, 1);//AdvancedCure
                            LearnSkill(1075, 1);//Invisibility
                            LearnSkill(1055, 1);//HealingRain
                            LearnSkill(1000, 1);//Thunder
                            LearnSkill(1001, 1);//Fire
                            LearnSkill(1005, 1);//Cure
                            LearnSkill(1195, 1);//Meditation
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 21)//Warrior
                        {
                            LearnSkill(1050, 0);//Revive
                            LearnSkill(1175, 1);//AdvancedCure
                            LearnSkill(1075, 1);//Invisibility
                            LearnSkill(1055, 1);//HealingRain
                            LearnSkill(1000, 1);//Thunder
                            LearnSkill(1001, 1);//Fire
                            LearnSkill(1005, 1);//Cure
                            LearnSkill(1195, 1);//Meditation
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 41)//Archer
                        {
                            LearnSkill(1050, 0);//Revive
                            LearnSkill(1175, 1);//AdvancedCure
                            LearnSkill(1075, 1);//Invisibility
                            LearnSkill(1055, 1);//HealingRain
                            LearnSkill(1000, 1);//Thunder
                            LearnSkill(1001, 1);//Fire
                            LearnSkill(1005, 1);//Cure
                            LearnSkill(1195, 1);//Meditation
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 142)//FireTao
                        {
                            LearnSkill(1050, 0);//Revive
                            LearnSkill(1175, 1);//AdvancedCure
                            LearnSkill(1075, 1);//Invisibility
                            LearnSkill(1055, 1);//HealingRain
                            LearnSkill(3080, 1);//Dodge
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 132)//WaterTao
                        {
                            LearnSkill(1050, 0);//Revive
                            LearnSkill(1175, 1);//AdvancedCure
                            LearnSkill(1075, 1);//Invisibility
                            LearnSkill(1055, 1);//HealingRain
                            LearnSkill(1120, 1);//FireCircle
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                    }
                }
                if (FirstJob == 145)//Fire
                {
                    if (Job == 15)//Trojan
                    {
                        if (ToJob == 11)//Trojan
                        {
                            LearnSkill(1000, 1);//Thunder
                            LearnSkill(1001, 1);//Fire
                            LearnSkill(1005, 1);//Cure
                            LearnSkill(1195, 1);//Meditation
                            LearnSkill(3050, 1);//CrualShade
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 21)//Warrior
                        {
                            LearnSkill(1110, 0);//Cyclone		
                            LearnSkill(1000, 1);//Thunder
                            LearnSkill(1001, 1);//Fire
                            LearnSkill(1005, 1);//Cure
                            LearnSkill(1195, 1);//Meditation
                            LearnSkill(1270, 1);//Robot
                            LearnSkill(1190, 1);//SpiritHealing
                            LearnSkill(5100, 0);//IronShirt
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 41)//Archer
                        {
                            LearnSkill(1000, 1);//Thunder
                            LearnSkill(1001, 1);//Fire
                            LearnSkill(1005, 1);//Cure
                            LearnSkill(1195, 1);//Meditation
                            LearnSkill(1270, 1);//Robot
                            LearnSkill(1110, 0);//Cyclone
                            LearnSkill(1190, 1);//SpiritHealing
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 132)//WaterTaoist
                        {
                            LearnSkill(1000, 1);//Thunder
                            LearnSkill(1001, 1);//Fire
                            LearnSkill(1005, 1);//Cure
                            LearnSkill(1195, 1);//Meditation
                            LearnSkill(1270, 1);//Robot
                            LearnSkill(1110, 0);//Cyclone
                            LearnSkill(1190, 1);//SpiritHealing
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 142)//FireTaoist
                        {
                            LearnSkill(1000, 1);//Thunder
                            LearnSkill(1001, 1);//Fire
                            LearnSkill(1005, 1);//Cure
                            LearnSkill(1195, 1);//Meditation
                            LearnSkill(1270, 1);//Robot
                            LearnSkill(1110, 0);//Cyclone
                            LearnSkill(1190, 1);//SpiritHealing
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                    }
                    if (Job == 25)//Warrior
                    {
                        if (ToJob == 11)//Trojan
                        {
                            LearnSkill(1000, 1);//Thunder
                            LearnSkill(1001, 1);//Fire
                            LearnSkill(1005, 1);//Cure
                            LearnSkill(1195, 1);//Meditation
                            LearnSkill(1015, 0);//Accuracy
                            LearnSkill(1040, 0);//Roar
                            LearnSkill(1320, 1);//FlyingMoon
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 21)//Warrior
                        {
                            LearnSkill(1000, 1);//Thunder
                            LearnSkill(1001, 1);//Fire
                            LearnSkill(1005, 1);//Cure
                            LearnSkill(1195, 1);//Meditation
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 41)//Archer
                        {
                            LearnSkill(1000, 1);//Thunder
                            LearnSkill(1001, 1);//Fire
                            LearnSkill(1005, 1);//Cure
                            LearnSkill(1195, 1);//Meditation
                            LearnSkill(1020, 0);//Shield
                            LearnSkill(1040, 0);//Roar
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 132)//WaterTaoist
                        {
                            LearnSkill(1000, 1);//Thunder
                            LearnSkill(1001, 1);//Fire
                            LearnSkill(1005, 1);//Cure
                            LearnSkill(1195, 1);//Meditation
                            LearnSkill(1025, 0);//Superman
                            LearnSkill(1020, 0);//Shield
                            LearnSkill(1040, 0);//Roar
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 142)//FireTaoist
                        {
                            LearnSkill(1020, 0);//Shield
                            LearnSkill(1040, 0);//Roar
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                    }
                    if (Job == 45)//Archer
                    {
                        if (ToJob == 11)//Trojan
                        {
                            LearnSkill(1000, 1);//Thunder
                            LearnSkill(1001, 1);//Fire
                            LearnSkill(1005, 1);//Cure
                            LearnSkill(1195, 1);//Meditation
                            LearnSkill(5002, 0);//Poison
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 21)//Warrior
                        {
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(1000, 1);//Thunder
                            LearnSkill(1001, 1);//Fire
                            LearnSkill(1005, 1);//Cure
                            LearnSkill(1195, 1);//Meditation
                            LearnSkill(5002, 0);//Poison
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 41)//Archer
                        {
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(1000, 1);//Thunder
                            LearnSkill(1001, 1);//Fire
                            LearnSkill(1005, 1);//Cure
                            LearnSkill(1195, 1);//Meditation
                            LearnSkill(5000, 0);//FreezingArrow
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 132)//WaterTaoist
                        {
                            LearnSkill(1000, 1);//Thunder
                            LearnSkill(1001, 1);//Fire
                            LearnSkill(1005, 1);//Cure
                            LearnSkill(1195, 1);//Meditation
                            LearnSkill(5002, 0);//Poison
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 142)//FireTaoist
                        {
                            LearnSkill(1000, 1);//Thunder
                            LearnSkill(1001, 1);//Fire
                            LearnSkill(1005, 1);//Cure
                            LearnSkill(1195, 1);//Meditation
                            LearnSkill(5002, 0);//Poison
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                    }
                    if (Job == 135)//WaterTaoist
                    {
                        if (ToJob == 11)//Trojan
                        {
                            LearnSkill(1005, 1);//Cure
                            LearnSkill(1195, 1);//Meditation
                            LearnSkill(1085, 1);//StarOfAccuracy
                            LearnSkill(1095, 1);//Stigma
                            LearnSkill(1090, 1);//MagicShield
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 21)//Warrior
                        {
                            LearnSkill(1005, 1);//Cure
                            LearnSkill(1195, 1);//Meditation
                            LearnSkill(1085, 1);//StarOfAccuracy
                            LearnSkill(1095, 1);//Stigma
                            LearnSkill(1090, 1);//MagicShield
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 41)//Archer
                        {
                            LearnSkill(1075, 1);//Invisibility
                            LearnSkill(1005, 1);//Cure
                            LearnSkill(1090, 1);//MagicShield
                            LearnSkill(1095, 1);//Stigma
                            LearnSkill(1195, 1);//Meditation
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 132)//WaterTaoist
                        {
                            LearnSkill(1120, 1);//FireCircle
                            LearnSkill(3090, 1);//Pervade
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 142)//FireTaoist
                        {
                            LearnSkill(1050, 0);//Revivre
                            LearnSkill(1075, 1);//Invisibility
                            LearnSkill(1055, 1);//HealingRain
                            LearnSkill(1175, 1);//AdvancedCure
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                    }
                    if (Job == 145)//FireTaoist
                    {
                        if (ToJob == 11)//Trojan
                        {
                            LearnSkill(3080, 1);//Dodge
                            LearnSkill(1000, 1);//Thunder
                            LearnSkill(1001, 1);//Fire
                            LearnSkill(1005, 1);//Cure
                            LearnSkill(1195, 1);//Meditation
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 21)//Warrior
                        {
                            LearnSkill(3080, 1);//Dodge
                            LearnSkill(1000, 1);//Thunder
                            LearnSkill(1001, 1);//Fire
                            LearnSkill(1005, 1);//Cure
                            LearnSkill(1195, 1);//Meditation
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 41)//Archer
                        {
                            LearnSkill(3080, 1);//Dodge
                            LearnSkill(1000, 1);//Thunder
                            LearnSkill(1001, 1);//Fire
                            LearnSkill(1005, 1);//Cure
                            LearnSkill(1195, 1);//Meditation
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 132)//WaterTaoist
                        {
                            LearnSkill(3080, 1);//Dodge
                            LearnSkill(1120, 1);//FireCircle
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                        if (ToJob == 142)//FireTaoist
                        {
                            LearnSkill(3080, 1);//Dodge
                            LearnSkill(4000, 1);//SummonGuard
                            LearnSkill(9876, 0);//GodBlessing
                        }
                    }
                }
                #endregion

                Job = ToJob;

                DataBase.GetStats(this);
                GetEquipStats(1, true);
                GetEquipStats(2, true);
                GetEquipStats(3, true);
                GetEquipStats(4, true);
                GetEquipStats(5, true);
                GetEquipStats(6, true);
                GetEquipStats(7, true);
                GetEquipStats(8, true);
                MinAtk = Str;
                MaxAtk = Str;
                MaxHP = BaseMaxHP();
                Potency = Level;
                GetEquipStats(1, false);
                GetEquipStats(2, false);
                GetEquipStats(3, false);
                GetEquipStats(4, false);
                GetEquipStats(5, false);
                GetEquipStats(6, false);
                GetEquipStats(7, false);
                GetEquipStats(8, false);
                CurHP = MaxHP;
                if (Job == 11)
                {
                    Str = 5;
                    Agi = 2;
                    Vit = 3;
                    Spi = 0;
                }
                if (Job == 21)
                {
                    Str = 5;
                    Agi = 2;
                    Vit = 3;
                    Spi = 0;
                }
                if (Job == 41)
                {
                    Str = 2;
                    Agi = 7;
                    Vit = 1;
                    Spi = 0;
                }
                if (Job == 132)
                {
                    Str = 0;
                    Agi = 2;
                    Vit = 3;
                    Spi = 5;
                }
                if (Job == 142)
                {
                    Str = 0;
                    Agi = 2;
                    Vit = 3;
                    Spi = 5;
                }
                MyClient.SendPacket(General.MyPackets.Vital((long)UID, 7, Job));
                MyClient.SendPacket(General.MyPackets.Vital((long)UID, 16, Str));
                MyClient.SendPacket(General.MyPackets.Vital((long)UID, 17, Agi));
                MyClient.SendPacket(General.MyPackets.Vital((long)UID, 15, Vit));
                MyClient.SendPacket(General.MyPackets.Vital((long)UID, 14, Spi));
                MyClient.SendPacket(General.MyPackets.Vital((long)UID, 2, MaxMana()));
                MyClient.SendPacket(General.MyPackets.GeneralData((long)UID, 0, 0, 0, 92));
                MyClient.SendPacket(General.MyPackets.Vital((long)UID, 0, CurHP));
                StatP += 30;
                StatP += 42;
                MyClient.SendPacket(General.MyPackets.Vital(UID, 11, StatP));

                #region Stuff
                for (byte i = 1; i < 9; i++)
                {
                    if (Equips[i] == null || Equips[i] == "") continue;
                    string I = Equips[i];
                    string[] II = I.Split('-');
                    uint IID = uint.Parse(II[0]);
                    byte Quality = (byte)Other.ItemQuality(IID);

                    if (i == 1)
                    {
                        string NewID = "";
                        if (Other.ItemInfo(IID)[0] >= 117493 && Other.ItemInfo(IID)[0] <= 117499)
                        {
                            NewID = II[0].Remove(4, 2);
                            NewID = 1173 + "0" + Quality.ToString();

                            Equips[i] = NewID + "-" + II[1] + "-" + II[2] + "-" + II[3] + "-" + II[4] + "-" + II[5];
                            II[0] = NewID;
                            MyClient.SendPacket(General.MyPackets.AddItem(Equips_UIDs[i], int.Parse(II[0]), byte.Parse(II[1]), byte.Parse(II[2]), byte.Parse(II[3]), byte.Parse(II[4]), byte.Parse(II[5]), i, 100, 100));
                        }
                        else
                        {
                            if (Other.WeaponType(IID) == 111 || Other.WeaponType(IID) == 113 || Other.WeaponType(IID) == 114 || Other.WeaponType(IID) == 118 || Other.WeaponType(IID) == 117)
                            {
                                NewID = II[0].Remove(4, 2);
                                NewID = NewID + "0" + Quality.ToString();

                                Equips[i] = NewID + "-" + II[1] + "-" + II[2] + "-" + II[3] + "-" + II[4] + "-" + II[5];
                                II[0] = NewID;
                                MyClient.SendPacket(General.MyPackets.AddItem(Equips_UIDs[i], int.Parse(II[0]), byte.Parse(II[1]), byte.Parse(II[2]), byte.Parse(II[3]), byte.Parse(II[4]), byte.Parse(II[5]), i, 100, 100));
                            }
                            else if (Other.WeaponType(IID) == 112)
                            {
                                byte Type = byte.Parse(II[0][4].ToString());
                                byte Color = byte.Parse(II[0][3].ToString());
                                NewID = "11" + Type.ToString() + Color.ToString() + "0" + Quality.ToString();
                                Equips[i] = NewID + "-" + II[1] + "-" + II[2] + "-" + II[3] + "-" + II[4] + "-" + II[5];
                                II[0] = NewID;
                                MyClient.SendPacket(General.MyPackets.AddItem(Equips_UIDs[i], int.Parse(II[0]), byte.Parse(II[1]), byte.Parse(II[2]), byte.Parse(II[3]), byte.Parse(II[4]), byte.Parse(II[5]), i, 100, 100));
                            }
                        }
                    }
                    else if (i == 2)
                    {
                        string NewID = "";

                        NewID = II[0].Remove(3, 3);
                        NewID += "00" + Quality.ToString();
                        Equips[i] = NewID + "-" + II[1] + "-" + II[2] + "-" + II[3] + "-" + II[4] + "-" + II[5];
                        II[0] = NewID;
                        MyClient.SendPacket(General.MyPackets.AddItem(Equips_UIDs[i], int.Parse(II[0]), byte.Parse(II[1]), byte.Parse(II[2]), byte.Parse(II[3]), byte.Parse(II[4]), byte.Parse(II[5]), i, 100, 100));
                    }
                    else if (i == 3)
                    {
                        string NewID = "";
                        if (Other.WeaponType(IID) == 130 || Other.WeaponType(IID) == 131 || Other.WeaponType(IID) == 133 || Other.WeaponType(IID) == 134)
                        {
                            NewID = II[0].Remove(4, 2);
                            NewID = NewID + "0" + Quality.ToString();

                            Equips[i] = NewID + "-" + II[1] + "-" + II[2] + "-" + II[3] + "-" + II[4] + "-" + II[5];
                            II[0] = NewID;
                            MyClient.SendPacket(General.MyPackets.AddItem(Equips_UIDs[i], int.Parse(II[0]), byte.Parse(II[1]), byte.Parse(II[2]), byte.Parse(II[3]), byte.Parse(II[4]), byte.Parse(II[5]), i, 100, 100));
                        }
                        else if (Other.WeaponType(IID) == 135 || Other.WeaponType(IID) == 136 || Other.WeaponType(IID) == 138 || Other.WeaponType(IID) == 139)
                        {
                            byte Type = byte.Parse(II[0][2].ToString());
                            byte Color = byte.Parse(II[0][3].ToString());
                            Type -= 5;
                            NewID = "13" + Type.ToString() + Color.ToString() + "0" + Quality.ToString();
                            Equips[i] = NewID + "-" + II[1] + "-" + II[2] + "-" + II[3] + "-" + II[4] + "-" + II[5];
                            II[0] = NewID;
                            MyClient.SendPacket(General.MyPackets.AddItem(Equips_UIDs[i], int.Parse(II[0]), byte.Parse(II[1]), byte.Parse(II[2]), byte.Parse(II[3]), byte.Parse(II[4]), byte.Parse(II[5]), i, 100, 100));
                        }
                    }
                    else if (i == 4)
                    {
                        string NewID = "";

                        NewID = II[0].Remove(3, 3);
                        NewID += "02" + Quality.ToString();
                        Equips[i] = NewID + "-" + II[1] + "-" + II[2] + "-" + II[3] + "-" + II[4] + "-" + II[5];
                        II[0] = NewID;
                        MyClient.SendPacket(General.MyPackets.AddItem(Equips_UIDs[i], int.Parse(II[0]), byte.Parse(II[1]), byte.Parse(II[2]), byte.Parse(II[3]), byte.Parse(II[4]), byte.Parse(II[5]), i, 100, 100));
                    }
                    else if (i == 5)
                    {
                        string NewID = "";

                        if (Other.WeaponType(IID) == 900)
                        {
                            NewID = II[0].Remove(4, 2);
                            NewID += "0" + Quality.ToString();
                            Equips[i] = NewID + "-" + II[1] + "-" + II[2] + "-" + II[3] + "-" + II[4] + "-" + II[5];
                            II[0] = NewID;
                            MyClient.SendPacket(General.MyPackets.AddItem(Equips_UIDs[i], int.Parse(II[0]), byte.Parse(II[1]), byte.Parse(II[2]), byte.Parse(II[3]), byte.Parse(II[4]), byte.Parse(II[5]), i, 100, 100));
                        }
                        else if (Other.ItemType(IID) == 4 || Other.ItemType(IID) == 5)
                        {
                            NewID = II[0].Remove(3, 3);
                            NewID += "02" + Quality.ToString();
                            Equips[i] = NewID + "-" + II[1] + "-" + II[2] + "-" + II[3] + "-" + II[4] + "-" + II[5];
                            II[0] = NewID;
                            MyClient.SendPacket(General.MyPackets.AddItem(Equips_UIDs[i], int.Parse(II[0]), byte.Parse(II[1]), byte.Parse(II[2]), byte.Parse(II[3]), byte.Parse(II[4]), byte.Parse(II[5]), i, 100, 100));
                        }
                    }
                    else if (i == 6)
                    {
                        string NewID = "";

                        NewID = II[0].Remove(3, 3);
                        NewID += "01" + Quality.ToString();
                        Equips[i] = NewID + "-" + II[1] + "-" + II[2] + "-" + II[3] + "-" + II[4] + "-" + II[5];
                        II[0] = NewID;
                        MyClient.SendPacket(General.MyPackets.AddItem(Equips_UIDs[i], int.Parse(II[0]), byte.Parse(II[1]), byte.Parse(II[2]), byte.Parse(II[3]), byte.Parse(II[4]), byte.Parse(II[5]), i, 100, 100));
                    }
                    else if (i == 8)
                    {
                        string NewID = "";

                        NewID = II[0].Remove(3, 3);
                        NewID += "01" + Quality.ToString();
                        Equips[i] = NewID + "-" + II[1] + "-" + II[2] + "-" + II[3] + "-" + II[4] + "-" + II[5];
                        II[0] = NewID;
                        MyClient.SendPacket(General.MyPackets.AddItem(Equips_UIDs[i], int.Parse(II[0]), byte.Parse(II[1]), byte.Parse(II[2]), byte.Parse(II[3]), byte.Parse(II[4]), byte.Parse(II[5]), i, 100, 100));
                    }

                }
                #endregion

                MyClient.SendPacket(General.MyPackets.Vital(UID, 13, Level));

                MyClient.SendPacket(General.MyPackets.String(UID, 10, "hitstar"));
                MyClient.SendPacket(General.MyPackets.String(UID, 10, "fastflash"));
                MyClient.SendPacket(General.MyPackets.String(UID, 10, "impulse"));
                MyClient.SendPacket(General.MyPackets.String(UID, 10, "minimeteor"));

                World.SendMsgToAll(Name + " a complet� la deuxi�me renaissance!", "SYSTEM", 2011);
                World.SendMsgToAll(Name + " a complet� la deuxi�me renaissance!", "SYSTEM", 2005);
                Save();
                MyClient.Drop();
            }
            catch (Exception Exc) { Console.WriteLine(Exc); }
            World.UpdateSpawn(this);
        }
        public void AddFriend(Character Who)
        {
            Friends.Add(Who.UID, Who.Name);
            MyClient.SendPacket(General.MyPackets.FriendEnemyPacket(Who.UID, Who.Name, 15, 1));
        }

        public bool TeamAdd(Character Player)
        {
            if (PlayersInTeam < 6)
            {
                Player.MyTeamLeader = this;
                Player.MyClient.SendPacket(General.MyPackets.PlayerJoinsTeam(this));
                for (int i = 0; i < Team.Count; i++)
                {
                    Character Member = (Character)Team[i];
                    if (Member != null)
                    {
                        Member.MyClient.SendPacket(General.MyPackets.PlayerJoinsTeam(Player));
                        Member.Team.Add(Player);

                        Player.MyClient.SendPacket(General.MyPackets.PlayerJoinsTeam(Member));
                    }
                }
                Team.Add(Player);
                Player.Team = Team;
                MyClient.SendPacket(General.MyPackets.PlayerJoinsTeam(Player));
                Player.MyClient.SendPacket(General.MyPackets.PlayerJoinsTeam(Player));
                PlayersInTeam++;
                return true;
            }
            else
                return false;
        }

        public void TeamRemove(Character Player, bool Kick)
        {
            Player.MyTeamLeader = null;
            if (Kick)
                Player.MyClient.SendPacket(General.MyPackets.TeamPacket(Player.UID, 7));
            else
                Player.MyClient.SendPacket(General.MyPackets.TeamPacket(Player.UID, 2));

            if (Kick)
                MyClient.SendPacket(General.MyPackets.TeamPacket(Player.UID, 7));
            else
                MyClient.SendPacket(General.MyPackets.TeamPacket(Player.UID, 2));

            Team.Remove(Player);

            for (int i = 0; i < Team.Count; i++)
            {
                Character Member = (Character)Team[i];
                if (Member != null)
                {
                    if (Kick)
                        Member.MyClient.SendPacket(General.MyPackets.TeamPacket(Player.UID, 7));
                    else
                        Member.MyClient.SendPacket(General.MyPackets.TeamPacket(Player.UID, 2));

                    Member.Team.Remove(Player);

                    Player.MyClient.SendPacket(General.MyPackets.TeamPacket(Member.UID, 2));
                }
            }
            Player.Team = new ArrayList(4);
            PlayersInTeam--;
        }

        public void TeamDismiss()
        {
            TeamLeader = false;
            MyTeamLeader = null;
            PlayersInTeam = 0;

            MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
            World.UpdateSpawn(this);

            for (int i = 0; i < Team.Count; i++)
            {
                Character Member = (Character)Team[i];
                Member.MyTeamLeader = null;
                Member.Team = new ArrayList(6);
                Member.MyClient.SendPacket(General.MyPackets.TeamPacket(UID, 6));
                Member.MyClient.SendPacket(General.MyPackets.TeamPacket(Member.UID, 6));
            }
            MyClient.SendPacket(General.MyPackets.TeamPacket(UID, 6));
        }


        public string FindItem(uint ItemUID)
        {
            int Count = 0;
            foreach (uint item in Inventory_UIDs)
            {
                if (item == ItemUID)
                    return Inventory[Count];


                Count++;
            }
            return null;
        }

        public string FindWHItem(uint ItemUID, byte WH)
        {
            int Count = 0;
            foreach (uint item in WHIDs[WH])
            {
                if (item == ItemUID)
                {
                    if (WH == 0)
                        return TCWH[Count];
                    else if (WH == 1)
                        return PCWH[Count];
                    else if (WH == 2)
                        return ACWH[Count];
                    else if (WH == 3)
                        return DCWH[Count];
                    else if (WH == 4)
                        return BIWH[Count];
                    else if (WH == 5)
                        return MAWH[Count];
                    else
                        return null;
                }
                Count++;
            }
            return null;
        }

        public void StartXPCircle()
        {

        }

        public void AddXPC()
        {
            LastXPC = DateTime.Now;
            XpCircle += 1;

            if (XpCircle >= 100)
            {
                XpList = true;
                if (Job > 9 && Job < 16)
                {
                    foreach (DictionaryEntry DE in World.AllChars)
                    {
                        Character Chaar = (Character)DE.Value;
                        if (Chaar.Name != Name)
                        {
                            Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "CircleUp"));
                        }
                    }
                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "CircleUp"));
                }
                if (Job > 19 && Job < 26)
                {
                    foreach (DictionaryEntry DE in World.AllChars)
                    {
                        Character Chaar = (Character)DE.Value;
                        if (Chaar.Name != Name)
                        {
                            Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "CircleUp7"));
                        }
                    }
                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "CircleUp7"));
                }
                if (Job > 39 && Job < 46)
                {
                    foreach (DictionaryEntry DE in World.AllChars)
                    {
                        Character Chaar = (Character)DE.Value;
                        if (Chaar.Name != Name)
                        {
                            Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "CircleUp2"));
                        }
                    }
                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "CircleUp2"));
                }
                if (Job > 119 && Job < 126)
                {
                    foreach (DictionaryEntry DE in World.AllChars)
                    {
                        Character Chaar = (Character)DE.Value;
                        if (Chaar.Name != Name)
                        {
                            Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "CircleUp1"));
                        }
                    }
                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "CircleUp1"));
                }
                if (Job > 149 && Job < 156)
                {
                    foreach (DictionaryEntry DE in World.AllChars)
                    {
                        Character Chaar = (Character)DE.Value;
                        if (Chaar.Name != Name)
                        {
                            Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "CircleUp3"));
                        }
                    }
                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "CircleUp3"));
                }
                if (Job == 100 || Job == 101)
                {
                    foreach (DictionaryEntry DE in World.AllChars)
                    {
                        Character Chaar = (Character)DE.Value;
                        if (Chaar.Name != Name)
                        {
                            Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "CircleUp6"));
                        }
                    }
                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "CircleUp6"));
                }
                if (Job > 129 && Job < 136)
                {
                    foreach (DictionaryEntry DE in World.AllChars)
                    {
                        Character Chaar = (Character)DE.Value;
                        if (Chaar.Name != Name)
                        {
                            Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "CircleUp8"));
                        }
                    }
                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "CircleUp8"));
                }
                if (Job > 139 && Job < 146)
                {
                    foreach (DictionaryEntry DE in World.AllChars)
                    {
                        Character Chaar = (Character)DE.Value;
                        if (Chaar.Name != Name)
                        {
                            Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "CircleUp4"));
                        }
                    }
                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "CircleUp4"));
                }
                MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                XpCircle = 0;
            }
        }

        public ulong GetStat()
        {
            ulong it = 0;

            if (PKPoints >= 30 && PKPoints < 100)
                it += 16384;
            if (PKPoints >= 100)
                it += 32768;
            if (BlueName)
                it++;
            if (TeamLeader)
                it += 64;
            if (PoisonBuff)
                it += 2;
            if (SMOn)
                it += 262144;
            if (Invisible)
                it += 4194304;
            if (TInvisible) //Completely Invisible
                it += 4;
            if (Visible)
                it += 8;
            if (Restoring)
                it += 2097152;
            if (BurnBuff)
                it += 16777216;
            if (AccuracyBuff)
                it += 128;
            if (ShieldBuff)
                it += 256;
            if (MShieldBuff)
                it += 256;
            if (IronBuff)
                it += 256;
            if (StigBuff)
                it += 512;
            if (CycloneOn)
                it += 8388608;
            if (RobotBuff)
                it += 0x40000000;
            if (XpList)
                it += 16;
            if (CastingPray)
                it += 0x40000000;
            if (Praying)
                it += 0x80000000;
            if (!Alive)
                it += 1024;
            if (Flying)
                it += 134217728;

            return it;
        }

        public void Revive(bool Tele)
        {
            if (Alive)
                return;

            if (Tele)
            {
                foreach (ushort[] revp in DataBase.RevPoints)
                {
                    if (revp[0] == LocMap)
                    {
                        Teleport(revp[1], revp[2], revp[3]);
                        break;
                    }
                }
            }

            CurHP = MaxHP;
            Alive = true;

            MyClient.SendPacket(General.MyPackets.Status1(UID, 0));
            MyClient.SendPacket(General.MyPackets.Status3(UID));
            MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));

            MyClient.SendPacket(General.MyPackets.CharacterInfo(this));
            SendEquips(false);
            BlueName = false;
            Restoring = false;
            FreezeBuff = false;
            BurnBuff = false;
            PoisonBuff = false;

            MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
            Stamina = 100;
            MyClient.SendPacket(General.MyPackets.Vital(UID, 9, Stamina));

            World.UpdateSpawn(this);
            DeathSent = false;

        }

        public void Die()
        {
            try
            {
                uint MoneyDrop = 0;

                DeathSent = true;
                World.UpdateSpawn(this);

                int EModel;
                if (Model == 1003 || Model == 1004)
                    EModel = 15099;
                else
                    EModel = 15199;

                PoisonBuff = false;
                XpList = false;
                SMOn = false;
                CycloneOn = false;
                AccuracyOn = false;
                StigBuff = false;
                DodgeBuff = false;
                AccuracyBuff = false;
                ShieldBuff = false;
                MShieldBuff = false;
                RobotBuff = false;
                FreezeBuff = false;
                BurnBuff = false;
                CastingPray = false;
                Praying = false;

                XpCircle = 0;

                try
                {
                    if (Level > 15)
                    {
                        if (!Other.CanPK(LocMap))
                        {
                            Random Rand = new Random();
                            ulong ItemUID = Inventory_UIDs[0];
                            ulong ItemUID2 = Inventory_UIDs[1];
                            ulong ItemUID3 = Inventory_UIDs[3];
                            ulong ItemUID4 = Inventory_UIDs[4];

                            int Count = 0;
                            if (ItemUID2 != 0 && ItemUID != 0 && ItemUID3 != 0 && ItemUID4 != 0)
                            {
                                foreach (ushort Variable in DataBase.PKMaps)
                                {
                                    if (LocMap != Variable)
                                    {
                                        foreach (uint uid in Inventory_UIDs)
                                        {
                                            if (uid == ItemUID)
                                            {
                                                if (ItemUID != 750000)
                                                {
                                                    string Item = Inventory[Count];

                                                    DroppedItem e = DroppedItems.DropItem(Item, (uint)(LocX - General.Rand.Next(4) + General.Rand.Next(4)), (uint)(LocY - General.Rand.Next(4) + General.Rand.Next(4)), (uint)LocMap, 0);
                                                    World.ItemDrops(e);

                                                    RemoveItem(ItemUID);
                                                }
                                            }
                                            else if (uid == ItemUID2)
                                            {
                                                if (ItemUID2 != 750000)
                                                {
                                                    string Item2 = Inventory[Count];

                                                    DroppedItem e = DroppedItems.DropItem(Item2, (uint)(LocX - General.Rand.Next(4) + General.Rand.Next(4)), (uint)(LocY - General.Rand.Next(4) + General.Rand.Next(4)), (uint)LocMap, 0);
                                                    World.ItemDrops(e);

                                                    RemoveItem(ItemUID2);
                                                }
                                            }
                                            else if (uid == ItemUID3)
                                            {
                                                if (ItemUID3 != 750000)
                                                {
                                                    string Item3 = Inventory[Count];

                                                    DroppedItem e = DroppedItems.DropItem(Item3, (uint)(LocX - General.Rand.Next(4) + General.Rand.Next(4)), (uint)(LocY - General.Rand.Next(4) + General.Rand.Next(4)), (uint)LocMap, 0);
                                                    World.ItemDrops(e);

                                                    RemoveItem(ItemUID3);
                                                }
                                            }
                                            else if (uid == ItemUID4)
                                            {
                                                if (ItemUID4 != 750000)
                                                {
                                                    string Item4 = Inventory[Count];

                                                    DroppedItem e = DroppedItems.DropItem(Item4, (uint)(LocX - General.Rand.Next(4) + General.Rand.Next(4)), (uint)(LocY - General.Rand.Next(4) + General.Rand.Next(4)), (uint)LocMap, 0);
                                                    World.ItemDrops(e);

                                                    RemoveItem(ItemUID4);
                                                }
                                            }
                                            Count++;

                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch { }

                int Money = (int)Silvers / 10;
                if (Money < 0)
                    Money = 0;

                MoneyDrop = (uint)General.Rand.Next(0, Money);

                if (MoneyDrop > 0 && !Other.CanPK(LocMap))
                {
                    string Item = "";

                    if (MoneyDrop < 10)
                        Item = "1090000-0-0-0-0-0";
                    else if (MoneyDrop < 100)
                        Item = "1090010-0-0-0-0-0";
                    else if (MoneyDrop < 1000)
                        Item = "1090020-0-0-0-0-0";
                    else if (MoneyDrop < 3000)
                        Item = "1091000-0-0-0-0-0";
                    else if (MoneyDrop < 10000)
                        Item = "1091010-0-0-0-0-0";
                    else
                        Item = "1091020-0-0-0-0-0";

                    DroppedItem item = DroppedItems.DropItem(Item, (uint)(LocX - General.Rand.Next(4) + General.Rand.Next(4)), (uint)(LocY - General.Rand.Next(4) + General.Rand.Next(4)), (uint)LocMap, MoneyDrop);
                    World.ItemDrops(item);

                    Silvers -= MoneyDrop;
                    MyClient.SendPacket(General.MyPackets.Vital(UID, 4, Silvers));
                }

                if (PKPoints >= 30 && !Other.CanPK(LocMap))
                {
                    int Slot = 0;
                    uint MoneyDrops = 0;
                    uint EquipId = 0;
                    byte EquipPlus = 0;
                    byte EquipBless = 0;
                    byte EquipEnchant = 0;
                    byte EquipSoc1 = 0;
                    byte EquipSoc2 = 0;

                    Slot = General.Rand.Next(0, 9);
                    if (Equips[Slot] != null && Equips[Slot] != "0")
                    {
                        string[] Splitter = Equips[Slot].Split('-');
                        EquipId = uint.Parse(Splitter[0]);
                        EquipPlus = byte.Parse(Splitter[1]);
                        EquipBless = byte.Parse(Splitter[2]);
                        EquipEnchant = byte.Parse(Splitter[3]);
                        EquipSoc1 = byte.Parse(Splitter[4]);
                        EquipSoc2 = byte.Parse(Splitter[5]);

                        UnEquip((byte)Slot);

                        RemoveItem(ItemNext(EquipId));

                        string Item = Convert.ToString(EquipId + "-" + EquipPlus + "-" + EquipBless + "-" + EquipEnchant + "-" + EquipSoc1 + "-" + EquipSoc2);
                        DroppedItem item = DroppedItems.DropItem(Item, (uint)(LocX - General.Rand.Next(4) + General.Rand.Next(4)), (uint)(LocY - General.Rand.Next(4) + General.Rand.Next(4)), (uint)LocMap, MoneyDrops);
                        World.ItemDrops(item);
                    }
                }

                MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                MyClient.SendPacket(General.MyPackets.Status1(UID, EModel));
                MyClient.SendPacket(General.MyPackets.Death(this));

                if (LocMap != 6000 && LocMap != 1038 && LocMap != 6001 && LocMap != 1005 && LocMap != 1091 && LocMap != 1081 && LocMap != 1090)
                {
                    if (PKPoints > 100)
                    {
                        Teleport(6000, 028, 071);
                        World.SendMsgToAll(Name + " a �t� envoy� en prison!", "Police", 2008);
                    }
                }
            }
            catch (Exception Exc) { General.WriteLine(Convert.ToString(Exc)); }
        }

        public bool GetHitDie(uint Damage)
        {
            if (Action == 250)
            {
                if (Stamina >= 35)                
                    Stamina -= 25;                
                else
                    Stamina = 0;
                MyClient.SendPacket(General.MyPackets.Vital(UID, 9, Stamina));
            }
            Action = 100;
            if (Damage >= CurHP)
            {
                CurHP = 0;
                Death = DateTime.Now;
                Alive = false;

                MyClient.SendPacket(General.MyPackets.Vital(UID, 0, CurHP));

                return true;
            }
            else
            {
                CurHP -= (ushort)Damage;
                MyClient.SendPacket(General.MyPackets.Vital(UID, 0, CurHP));
                return false;
            }
        }

        public void XPEnd()
        {
            CycloneOn = false;
            SMOn = false;
            AccuracyOn = false;
            MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
            World.UpdateSpawn(this);
        }

        public void UseSkill(ushort SkillId, ushort X, ushort Y, uint TargID)
        {
            if (!Alive)
                return;
            Ready = false;
            if (Skills.Contains((short)SkillId))
                if (DataBase.SkillsDone.Contains((int)SkillId))
                {
                    Action = 100;
                    byte SkillLvl = (byte)Skills[(short)SkillId];
                    Hashtable MobTargets = new Hashtable();
                    Hashtable PlayerTargets = new Hashtable();
                    Hashtable NPCTargets = new Hashtable();

                    ushort[] SkillAttributes = DataBase.SkillAttributes[SkillId][SkillLvl];
                    #region SkillAttributes8
                    if (SkillAttributes[0] == 8)
                    {
                        Character Target = (Character)World.AllChars[TargID];

                        if (Target == null)
                            return;

                        if (SkillId == 1095 && CurMP >= SkillAttributes[4]) //Stig
                        {
                            if (LocMap != 1039)
                                CurMP -= (ushort)SkillAttributes[4];
                            MyClient.SendPacket(General.MyPackets.Vital(UID, 2, CurMP));
                            Target.StigBuff = true;
                            Target.Stigged = DateTime.Now;
                            Target.StigLevel = SkillLvl;
                            Target.MyClient.SendPacket(General.MyPackets.Vital(Target.UID, 26, Target.GetStat()));
                            World.UpdateSpawn(Target);
                            Target.MyClient.SendPacket(General.MyPackets.SendMsg(Target.MyClient.MessageId, "System", Target.Name, "Stig activ�: +" + (10 + Target.StigLevel * 5) + " % attaque de plus durant " + (20 + Target.StigLevel * 5) + " secondes.", 2005));
                        }

                        if (SkillId == 1090 && CurMP >= SkillAttributes[4]) //MShield
                        {
                            if (LocMap != 1039)
                                CurMP -= (ushort)SkillAttributes[4];
                            MyClient.SendPacket(General.MyPackets.Vital(UID, 2, CurMP));
                            Target.MShieldBuff = true;
                            Target.MShielded = DateTime.Now;
                            Target.MShieldLevel = SkillLvl;
                            Target.MyClient.SendPacket(General.MyPackets.Vital(Target.UID, 26, Target.GetStat()));
                            World.UpdateSpawn(Target);
                            Target.MyClient.SendPacket(General.MyPackets.SendMsg(Target.MyClient.MessageId, "System", Target.Name, "Bouclier Magique activ�: +" + (10 + Target.MShieldLevel * 5) + " % d�fense durant " + (20 + Target.MShieldLevel * 5) + " secondes.", 2005));
                        }

                        if (SkillId == 3080 && CurMP >= SkillAttributes[4]) //Dodge
                        {
                            if (LocMap != 1039)
                                CurMP -= (ushort)SkillAttributes[4];
                            MyClient.SendPacket(General.MyPackets.Vital(UID, 2, CurMP));
                            Target.DodgeBuff = true;
                            Target.Dodged = DateTime.Now;
                            Target.DodgeLevel = SkillLvl;
                            Target.MyClient.SendPacket(General.MyPackets.Vital(Target.UID, 26, Target.GetStat()));
                            World.UpdateSpawn(Target);
                            Target.MyClient.SendPacket(General.MyPackets.SendMsg(Target.MyClient.MessageId, "System", Target.Name, "Dodge activ�: +" + (20 + Target.DodgeLevel * 5) + " % esquive de plus durant " + (20 + Target.DodgeLevel * 5 * 2) + " secondes.", 2005));

                        }

                        if (SkillId == 1075 && CurMP >= SkillAttributes[4]) //Invisibility
                        {
                            if (LocMap != 1039)
                                CurMP -= (ushort)SkillAttributes[4];
                            MyClient.SendPacket(General.MyPackets.Vital(UID, 2, CurMP));
                            Target.Invisible = true;
                            Target.Invisibility = DateTime.Now;
                            Target.InvLevel = SkillLvl;
                            Target.MyClient.SendPacket(General.MyPackets.Vital(Target.UID, 26, Target.GetStat()));
                            World.UpdateSpawn(Target);
                            Target.MyClient.SendPacket(General.MyPackets.SendMsg(Target.MyClient.MessageId, "System", Target.Name, "Invisibilit� activ�: Vous �tes invisible durant " + (20 + Target.InvLevel * 5) + " secondes.", 2005));
                        }

                        if (SkillId == 1100 && CurMP >= SkillAttributes[4]) //Pri�re
                        {
                            if (!Target.Alive)
                            {
                                if (LocMap != 1039)
                                    CurMP -= (ushort)SkillAttributes[4];
                                MyClient.SendPacket(General.MyPackets.Vital(UID, 2, CurMP));
                                Target.Revive(false);
                            }
                        }

                        if (SkillId == 1050)
                        {
                            if (!Target.Alive)
                            {
                                Target.Revive(false);
                                XpList = false;
                                XpCircle = 0;
                                XPActivated = DateTime.Now;

                                MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                            }
                        }

                        if (SkillId == 1080)
                        {
                            foreach (DictionaryEntry DE in World.AllChars)
                            {
                                Character Chaar = (Character)DE.Value;
                                if (UID != General.Rand.Next((int)Chaar.UID, (int)Chaar.UID))
                                {
                                    Teleport(Chaar.LocMap, Chaar.LocX, Chaar.LocY);
                                }
                            }

                            XpList = false;
                            XpCircle = 0;
                            XPActivated = DateTime.Now;

                            MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                        }

                        if (SkillId == 5000)
                        {
                            Target.MAtk = 1;
                            Target.MinAtk = 1;
                            Target.MaxAtk = 1;
                            Target.FreezeBuff = true;
                            Target.Freezed = DateTime.Now;
                            Target.MyClient.SendPacket(General.MyPackets.Vital(Target.UID, 26, Target.GetStat()));
                            World.UpdateSpawn(Target);
                        }

                        World.UsingSkill(this, (short)SkillId, SkillLvl, TargID, 0, (short)Target.LocX, (short)Target.LocY);
                    }
                    #endregion
                    #region SkillAttributes7
                    if (SkillAttributes[0] == 7)
                    {
                        if (SkillId == 8003 && Stamina >= 100) //Fly avanc�
                        {
                            Flying = true;
                            XpList = false;
                            if (SkillLvl == 0)
                                FlyType = 0;
                            else if (SkillLvl == 1)
                                FlyType = 1;
                            if (LocMap != 1039)
                                Stamina = 0;
                            FlyActivated = DateTime.Now;

                            MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                            MyClient.SendPacket(General.MyPackets.Vital(UID, 9, Stamina));
                        }
                        if (SkillId == 8002) //XP fly
                        {
                            Flying = true;
                            XpList = false;
                            FlyType = 0;
                            FlyActivated = DateTime.Now;

                            MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                        }
                        if (SkillId == 1110) //Cyclone
                        {
                            if ((!CycloneOn || !SMOn) & (XpList = false))
                                KO = 0;
                            CycloneOn = true;
                            XpList = false;
                            XpCircle = 0;
                            XPActivated = DateTime.Now;
                            ExtraXP = 20000;

                            MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                        }

                        if (SkillId == 4000 && Stamina >= 100)
                        {
                            if (Guard == null)
                            {
                                if (SkillLvl == 0)
                                    Mobs.NewRBGuard(Convert.ToInt16(MyClient.MyChar.LocX), Convert.ToInt16(MyClient.MyChar.LocY), Convert.ToInt16(MyClient.MyChar.LocMap), MyClient.MyChar.UID, 0);
                                if (SkillLvl == 1)
                                    Mobs.NewRBGuard(Convert.ToInt16(MyClient.MyChar.LocX), Convert.ToInt16(MyClient.MyChar.LocY), Convert.ToInt16(MyClient.MyChar.LocMap), MyClient.MyChar.UID, 1);
                                if (SkillLvl == 2)
                                    Mobs.NewRBGuard(Convert.ToInt16(MyClient.MyChar.LocX), Convert.ToInt16(MyClient.MyChar.LocY), Convert.ToInt16(MyClient.MyChar.LocMap), MyClient.MyChar.UID, 2);
                                if (SkillLvl == 3)
                                    Mobs.NewRBGuard(Convert.ToInt16(MyClient.MyChar.LocX), Convert.ToInt16(MyClient.MyChar.LocY), Convert.ToInt16(MyClient.MyChar.LocMap), MyClient.MyChar.UID, 3);

                            }
                            else
                            {
                                Guard.Dissappear();

                                if (SkillLvl == 0)
                                    Mobs.NewRBGuard(Convert.ToInt16(MyClient.MyChar.LocX), Convert.ToInt16(MyClient.MyChar.LocY), Convert.ToInt16(MyClient.MyChar.LocMap), MyClient.MyChar.UID, 0);
                                if (SkillLvl == 1)
                                    Mobs.NewRBGuard(Convert.ToInt16(MyClient.MyChar.LocX), Convert.ToInt16(MyClient.MyChar.LocY), Convert.ToInt16(MyClient.MyChar.LocMap), MyClient.MyChar.UID, 1);
                                if (SkillLvl == 2)
                                    Mobs.NewRBGuard(Convert.ToInt16(MyClient.MyChar.LocX), Convert.ToInt16(MyClient.MyChar.LocY), Convert.ToInt16(MyClient.MyChar.LocMap), MyClient.MyChar.UID, 2);
                                if (SkillLvl == 3)
                                    Mobs.NewRBGuard(Convert.ToInt16(MyClient.MyChar.LocX), Convert.ToInt16(MyClient.MyChar.LocY), Convert.ToInt16(MyClient.MyChar.LocMap), MyClient.MyChar.UID, 3);
                            }
                            Stamina = 0;
                            MyClient.SendPacket(General.MyPackets.Vital(UID, 9, Stamina));
                        }

                        if (SkillId == 3321 && Stamina >= 100)
                        {
                            if (Guard == null)
                            {
                                Mobs.NewRBGuard(Convert.ToInt16(MyClient.MyChar.LocX), Convert.ToInt16(MyClient.MyChar.LocY), Convert.ToInt16(MyClient.MyChar.LocMap), MyClient.MyChar.UID, 5);
                            }
                            else
                            {
                                Guard.Dissappear();
                                Mobs.NewRBGuard(Convert.ToInt16(MyClient.MyChar.LocX), Convert.ToInt16(MyClient.MyChar.LocY), Convert.ToInt16(MyClient.MyChar.LocMap), MyClient.MyChar.UID, 5);
                            }
                            Stamina = 0;
                            MyClient.SendPacket(General.MyPackets.Vital(UID, 9, Stamina));
                        }

                        if (SkillId == 1270)
                        {
                            MyClient.SendPacket(General.MyPackets.Disguise((int)UID, 2771003, 5000, 5000));
                            MyClient.SendPacket(General.MyPackets.ShowDisguiseTime((int)UID));
                        }

                        if (SkillId == 9876 && Stamina >= 100)//Bless
                        {
                            if (!CastingPray && !Praying)
                            {
                                if (!Mining)
                                {
                                    PrayCasted = DateTime.Now;
                                    CastingPray = true;
                                    Stamina = 0;
                                    PrayX = LocX;
                                    PrayY = LocY;
                                    if (World.PlayersPraying.Contains(this))
                                    {
                                        World.PlayersPraying.Remove(this);
                                        World.PlayersPraying.Add(this);
                                    }
                                    else
                                        World.PlayersPraying.Add(this);

                                    MyClient.SendPacket(General.MyPackets.Vital(UID, 29, LuckTime));
                                    MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                                    MyClient.SendPacket(General.MyPackets.Vital(UID, 9, Stamina));
                                    World.UpdateSpawn(MyClient.MyChar);
                                }
                                else
                                    MyClient.SendPacket(General.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Vous ne pouvez pas faire la b�n�diction lorsque vous minez!", 2005));
                            }
                        }

                        if (SkillId == 1190 && Stamina >= 100) // Cure Esprit
                        {
                            if (LocMap != 1039)
                            {
                                if (SkillLvl == 0)
                                {
                                    CurHP += 500;
                                    if (CurHP > MaxHP)
                                        CurHP = MaxHP;
                                    Stamina = 0;
                                    MyClient.SendPacket(General.MyPackets.Vital(UID, 9, Stamina));
                                    MyClient.SendPacket(General.MyPackets.Vital(UID, 0, CurHP));
                                }
                                if (SkillLvl == 1)
                                {
                                    CurHP += 800;
                                    if (CurHP > MaxHP)
                                        CurHP = MaxHP;
                                    Stamina = 0;
                                    MyClient.SendPacket(General.MyPackets.Vital(UID, 9, Stamina));
                                    MyClient.SendPacket(General.MyPackets.Vital(UID, 0, CurHP));
                                }
                                if (SkillLvl == 2)
                                {
                                    CurHP += 1300;
                                    if (CurHP > MaxHP)
                                        CurHP = MaxHP;
                                    Stamina = 0;
                                    MyClient.SendPacket(General.MyPackets.Vital(UID, 9, Stamina));
                                    MyClient.SendPacket(General.MyPackets.Vital(UID, 0, CurHP));
                                }
                            }
                            else if (LocMap == 1039)
                            {
                                if (SkillLvl == 0)
                                {
                                    CurHP += 500;
                                    if (CurHP > MaxHP)
                                        CurHP = MaxHP;
                                    MyClient.SendPacket(General.MyPackets.Vital(UID, 0, CurHP));
                                }
                                if (SkillLvl == 1)
                                {
                                    CurHP += 800;
                                    if (CurHP > MaxHP)
                                        CurHP = MaxHP;
                                    MyClient.SendPacket(General.MyPackets.Vital(UID, 0, CurHP));
                                }
                                if (SkillLvl == 2)
                                {

                                    CurHP += 1300;
                                    if (CurHP > MaxHP)
                                        CurHP = MaxHP;
                                    MyClient.SendPacket(General.MyPackets.Vital(UID, 0, CurHP));
                                }
                            }
                        }
                        if (SkillId == 1025) //Superman
                        {
                            if ((!CycloneOn || !SMOn) & (XpList = false))
                                KO = 0;
                            SMOn = true;
                            XpList = false;
                            XpCircle = 0;
                            XPActivated = DateTime.Now;
                            ExtraXP = 20000;

                            MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                        }

                        if (SkillId == 1015) //XP Accuracy
                        {
                            XpList = false;
                            AccuracyOn = true;
                            AccuracyActivated = DateTime.Now;
                            MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                            MyClient.SendPacket(General.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Durant 200 secondes vous avez plus de chance de r�ussir vos coups.", 2005));
                        }

                        if (SkillId == 1020) //Shield
                        {
                            if (IronBuff != true)
                            {
                                XpList = false;
                                XpCircle = 0;
                                XPActivated = DateTime.Now;
                                ShieldBuff = true;
                                Shielded = DateTime.Now;
                                MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                                MyClient.SendPacket(General.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Durant 120 secondes votre defense est tripl�.", 2005));
                            }
                        }

                        if (SkillId == 5100) //IronShirt
                        {
                            if (ShieldBuff != true)
                            {
                                XpList = false;
                                XpCircle = 0;
                                XPActivated = DateTime.Now;
                                IronBuff = true;
                                IronShirted = DateTime.Now;
                                MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                                MyClient.SendPacket(General.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Durant 10 secondes votre defense est quadrupl�.", 2005));
                            }
                        }

                        if (SkillId == 1105)
                        {
                            XpList = false;
                            XpCircle = 0;
                            Restoring = true;
                            RestoreTime = DateTime.Now;
                            MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                            World.UpdateSpawn(this);
                        }
                        World.UsingSkill(this, (short)SkillId, SkillLvl, UID, 0, (short)LocX, (short)LocY);
                        World.UpdateSpawn(this);
                    }
                    #endregion
                    #region SkillAttributes5
                    if (SkillAttributes[0] == 5)
                    {
                        foreach (DictionaryEntry DE in Mobs.AllMobs)
                        {
                            SingleMob Mob = (SingleMob)DE.Value;

                            if (Mob.Alive)
                                if (Mob.Map == LocMap)
                                    if (MyMath.PointDistance(LocX, LocY, Mob.PosX, Mob.PosY) < SkillAttributes[1])
                                    {
                                        if (PKMode == 0)
                                            if (Mob.MType == 1 || Mob.MType == 7)
                                            {
                                                GotBlueName = DateTime.Now;
                                                BlueName = true;
                                                MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                                                World.UpdateSpawn(this);
                                            }

                                        double MobAngle = MyMath.PointDirecton(LocX, LocY, Mob.PosX, Mob.PosY);
                                        double Aim = MyMath.PointDirecton(LocX, LocY, X, Y);
                                        int Sector = (int)SkillAttributes[2];

                                        if (PKMode == 0 || Mob.MType != 1 && Mob.MType != 7)
                                        {
                                            if (MobAngle < Aim + Sector)
                                                if (MobAngle > Aim - Sector)
                                                    if (!MobTargets.Contains(Mob))
                                                        MobTargets.Add(Mob, Other.CalculateDamage(this, Mob, 2, SkillId, SkillLvl));

                                            if (Aim < Sector)
                                                if (MobAngle + Sector - Aim > 360)
                                                    if (MobAngle > 360 - Sector + Aim)
                                                        if (!MobTargets.Contains(Mob))
                                                            MobTargets.Add(Mob, Other.CalculateDamage(this, Mob, 2, SkillId, SkillLvl));

                                            if (360 - Aim < Sector)
                                                if (MobAngle < 90 - (360 - Aim))
                                                    if (!MobTargets.Contains(Mob))
                                                        MobTargets.Add(Mob, Other.CalculateDamage(this, Mob, 2, SkillId, SkillLvl));
                                        }
                                    }
                        }

                        foreach (DictionaryEntry DE in NPCs.AllNPCs)
                        {
                            SingleNPC NPC = (SingleNPC)DE.Value;

                            if (NPC.Flags == 21 || NPC.Flags == 22 || NPC.Flags == 10)
                                if (Level >= NPC.Level)
                                    if (NPC.Map == LocMap)
                                        if (MyMath.PointDistance(LocX, LocY, NPC.X, NPC.Y) <= SkillAttributes[1])
                                        {
                                            double NPCAngle = MyMath.PointDirecton(LocX, LocY, NPC.X, NPC.Y);
                                            double Aim = MyMath.PointDirecton(LocX, LocY, X, Y);
                                            int Sector = (int)SkillAttributes[2];

                                            if (NPCAngle < Aim + Sector)
                                                if (NPCAngle > Aim - Sector)
                                                    if (!NPCTargets.Contains(NPC))
                                                        NPCTargets.Add(NPC, Other.CalculateDamage(this, NPC, 2, SkillId, SkillLvl));

                                            if (Aim < Sector)
                                                if (NPCAngle + Sector - Aim > 360)
                                                    if (NPCAngle > 360 - Sector + Aim)
                                                        if (!NPCTargets.Contains(NPC))
                                                            NPCTargets.Add(NPC, Other.CalculateDamage(this, NPC, 2, SkillId, SkillLvl));

                                            if (360 - Aim < Sector)
                                                if (NPCAngle < 90 - (360 - Aim))
                                                    if (!NPCTargets.Contains(NPC))
                                                        NPCTargets.Add(NPC, Other.CalculateDamage(this, NPC, 2, SkillId, SkillLvl));
                                        }

                        }

                        if (!Other.NoPK(LocMap))
                            if (PKMode == 2 || PKMode == 0)
                            {
                                foreach (DictionaryEntry DE in World.AllChars)
                                {
                                    Character Char = (Character)DE.Value;

                                    if (Char.LocMap == LocMap)
                                        if (Char != this)
                                            if (Char.Alive)
                                                if (MyMath.PointDistance(LocX, LocY, Char.LocX, Char.LocY) <= SkillAttributes[1])
                                                {
                                                    double CharAngle = MyMath.PointDirecton(LocX, LocY, Char.LocX, Char.LocY);
                                                    double Aim = MyMath.PointDirecton(LocX, LocY, X, Y);
                                                    int Sector = (int)SkillAttributes[2];

                                                    if (CharAngle < Aim + Sector)
                                                        if (CharAngle > Aim - Sector)
                                                            if (!PlayerTargets.Contains(Char))
                                                                PlayerTargets.Add(Char, Other.CalculateDamage(this, Char, 2, SkillId, SkillLvl));

                                                    if (Aim < Sector)
                                                        if (CharAngle + Sector - Aim > 360)
                                                            if (CharAngle > 360 - Sector + Aim)
                                                                if (!PlayerTargets.Contains(Char))
                                                                    PlayerTargets.Add(Char, Other.CalculateDamage(this, Char, 2, SkillId, SkillLvl));

                                                    if (360 - Aim < Sector)
                                                        if (CharAngle < 90 - (360 - Aim))
                                                            if (!PlayerTargets.Contains(Char))
                                                                PlayerTargets.Add(Char, Other.CalculateDamage(this, Char, 2, SkillId, SkillLvl));
                                                }

                                }

                            }
                        World.UsingSkill(this, MobTargets, NPCTargets, PlayerTargets, (short)X, (short)Y, (short)SkillId, SkillLvl);
                    }
                    #endregion

                    #region SkillAttributes3n11
                    if (SkillAttributes[0] == 3)
                    {
                        if (SkillAttributes[0] == 3 && SkillAttributes[5] == 1)
                        {
                            XpList = false;
                            MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                        }
                        if (TargID < 7000 && TargID >= 5000)
                        {
                            SingleNPC Target = (SingleNPC)NPCs.AllNPCs[TargID];
                            X = (ushort)Target.X;
                            Y = (ushort)Target.Y;
                        }
                        else if (TargID > 400000 && TargID <= 500000)
                        {
                            SingleMob Target = (SingleMob)Mobs.AllMobs[TargID];
                            X = (ushort)Target.PosX;
                            Y = (ushort)Target.PosY;
                        }
                        else
                        {
                            Character Target = (Character)World.AllChars[TargID];
                            X = Target.LocX;
                            Y = Target.LocY;
                        }

                        foreach (DictionaryEntry DE in Mobs.AllMobs)
                        {
                            SingleMob Mob = (SingleMob)DE.Value;

                            if (Mob.Map == LocMap)
                                if (Mob.Alive)
                                    if (MyMath.PointDistance(X, Y, Mob.PosX, Mob.PosY) <= SkillAttributes[1])
                                    {
                                        if (PKMode == 0)
                                            if (Mob.MType == 1 || Mob.MType == 7)
                                            {
                                                GotBlueName = DateTime.Now;
                                                BlueName = true;
                                                MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                                                World.UpdateSpawn(this);
                                            }

                                        if (PKMode == 0 || Mob.MType != 1 && Mob.MType != 7)
                                            MobTargets.Add(Mob, Other.CalculateDamage(this, Mob, 3, SkillId, SkillLvl));
                                    }
                        }

                        foreach (DictionaryEntry DE in NPCs.AllNPCs)
                        {
                            SingleNPC NPC = (SingleNPC)DE.Value;
                            if (Level >= NPC.Level)

                                if (NPC.Map == LocMap)
                                    if (NPC.Flags == 21 || NPC.Flags == 22 || NPC.Flags == 10)
                                        if (MyMath.PointDistance(X, Y, NPC.X, NPC.Y) <= SkillAttributes[1])
                                            NPCTargets.Add(NPC, Other.CalculateDamage(this, NPC, 3, SkillId, SkillLvl));

                        }

                        if (!Other.NoPK(LocMap))
                            if (PKMode == 2 || PKMode == 0)
                            {
                                foreach (DictionaryEntry DE in World.AllChars)
                                {
                                    Character Char = (Character)DE.Value;

                                    if (Char.LocMap == LocMap)
                                        if (Char != this)
                                            if (Char.Alive)
                                                if (MyMath.PointDistance(X, Y, Char.LocX, Char.LocY) <= SkillAttributes[1])
                                                {
                                                    PlayerTargets.Add(Char, Other.CalculateDamage(this, Char, 3, SkillId, SkillLvl));
                                                }
                                }
                            }
                        if (SkillId == 1120 || SkillId == 1021 || SkillId == 3090 || SkillId == 1165)
                        {
                            if (CurMP >= SkillAttributes[4])
                            {
                                if (LocMap != 1039)
                                    CurMP -= SkillAttributes[4];
                                MyClient.SendPacket(General.MyPackets.Vital((long)UID, 2, CurMP));
                                World.UsingSkill(this, MobTargets, NPCTargets, PlayerTargets, (short)X, (short)Y, (short)SkillId, SkillLvl);
                            }
                        }
                        else
                            World.UsingSkill(this, MobTargets, NPCTargets, PlayerTargets, (short)X, (short)Y, (short)SkillId, SkillLvl);
                    }
                    #endregion
                    #region SkillAttributes16n12
                    if (SkillAttributes[0] == 16 || SkillAttributes[0] == 12 || SkillAttributes[0] == 13)
                    {
                        if (LocMap != 1039 && Stamina >= SkillAttributes[4])
                        {
                            Stamina -= (byte)SkillAttributes[4];
                            MyClient.SendPacket(General.MyPackets.Vital(UID, 9, Stamina));
                        }
                        else if (LocMap != 1039)
                            return;

                        short Heal = 0;
                        if (TargID < 7000 && TargID >= 5000)
                        {
                            SingleNPC Target = (SingleNPC)NPCs.AllNPCs[TargID];

                            X = (ushort)Target.X;
                            Y = (ushort)Target.Y;



                            if (SkillAttributes[0] == 16)
                            {
                                Heal = (short)SkillAttributes[3];

                                if (Target.MaxHP - Target.CurHP < Heal)
                                    Heal = (short)(Target.MaxHP - Target.CurHP);

                                Target.CurHP += (ushort)Heal;

                                if (SkillId == 3050)
                                {
                                    Heal = (short)(Target.CurHP / 5);

                                    Target.CurHP -= (ushort)Heal;
                                }


                            }

                            if (Level >= Target.Level)
                            {
                                if (SkillAttributes[0] == 12)
                                {
                                    if (MyMath.PointDistance(LocX, LocY, Target.X, Target.Y) < SkillAttributes[1])
                                        NPCTargets.Add(Target, Other.CalculateDamage(this, Target, 1, SkillId, SkillLvl));
                                    else
                                        Attacking = false;
                                }

                                if (SkillAttributes[0] == 13)
                                    NPCTargets.Add(Target, Other.CalculateDamage(this, Target, 2, SkillId, SkillLvl));
                            }
                        }
                        else if (TargID > 400000 && TargID <= 500000)
                        {
                            SingleMob Target = (SingleMob)Mobs.AllMobs[TargID];

                            X = (ushort)Target.PosX;
                            Y = (ushort)Target.PosY;

                            if (SkillAttributes[0] == 16)
                            {
                                Heal = (short)SkillAttributes[3];

                                if (Target.MaxHP - Target.CurHP < Heal)
                                    Heal = (short)(Target.MaxHP - Target.CurHP);

                                Target.CurHP += (uint)Heal;

                                if (SkillId == 3050)
                                {
                                    Heal = (short)(Target.CurHP / 5);

                                    Target.CurHP -= (ushort)Heal;
                                }


                            }
                            if (SkillAttributes[0] == 2)
                            {
                                if (PKMode == 0)
                                    if (Target.MType == 1 || Target.MType == 7)
                                    {
                                        GotBlueName = DateTime.Now;
                                        BlueName = true;
                                        MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                                        World.UpdateSpawn(this);
                                    }
                                if (PKMode == 0 || Target.MType != 1 && Target.MType != 7)
                                    MobTargets.Add(Target, Other.CalculateDamage(this, Target, 3, SkillId, SkillLvl));
                            }
                            if (SkillAttributes[0] == 12)
                            {
                                if (PKMode == 0)
                                    if (Target.MType == 1 || Target.MType == 7)
                                    {
                                        GotBlueName = DateTime.Now;
                                        BlueName = true;
                                        MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                                        World.UpdateSpawn(this);
                                    }

                                if (PKMode == 0 || Target.MType != 1 && Target.MType != 7)
                                {
                                    if (MyMath.PointDistance(LocX, LocY, Target.PosX, Target.PosY) < SkillAttributes[1])
                                        MobTargets.Add(Target, Other.CalculateDamage(this, Target, 1, SkillId, SkillLvl));
                                    else
                                        Attacking = false;
                                }
                            }
                            if (SkillAttributes[0] == 13)
                            {
                                if (PKMode == 0)
                                    if (Target.MType == 1 || Target.MType == 7)
                                    {
                                        GotBlueName = DateTime.Now;
                                        BlueName = true;
                                        MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                                        World.UpdateSpawn(this);
                                    }

                                if (PKMode == 0 || Target.MType != 1 && Target.MType != 7)
                                    MobTargets.Add(Target, Other.CalculateDamage(this, Target, 2, SkillId, SkillLvl));
                            }
                        }
                        else
                        {
                            if (!Other.NoPK(LocMap))//!Other.NoPK(LocMap)
                                if (PKMode == 2 || PKMode == 0)
                                {

                                    Character Target = (Character)World.AllChars[TargID];
                                    if (Target.Alive)
                                        if (!Target.Flying || SkillAttributes[0] != 12)
                                        {
                                            X = Target.LocX;
                                            Y = Target.LocY;

                                            if (SkillAttributes[0] == 16)
                                            {
                                                if (SkillId == 1195)
                                                {
                                                    Heal = (short)SkillAttributes[3];

                                                    if (Target.MaxMP - Target.CurMP < Heal)
                                                        Heal = (short)(Target.MaxMP - Target.CurMP);

                                                    Target.CurMP += (ushort)Heal;
                                                }


                                                Heal = (short)SkillAttributes[3];

                                                if (Target.MaxHP - Target.CurHP < Heal)
                                                    Heal = (short)(Target.MaxHP - Target.CurHP);

                                                Target.CurHP += (ushort)Heal;
                                                Target.MyClient.SendPacket(General.MyPackets.Vital(Target.UID, 0, Target.CurHP));


                                                if (SkillId == 3050)
                                                {
                                                    Heal = (short)(Target.CurHP / 5);

                                                    Target.CurHP -= (ushort)Heal;
                                                }


                                                Target.MyClient.SendPacket(General.MyPackets.Vital(Target.UID, 0, Target.CurHP));
                                                Target.MyClient.SendPacket(General.MyPackets.Vital(Target.UID, 0, Target.CurMP));
                                            }

                                            if (SkillAttributes[0] == 12)
                                            {
                                                if (MyMath.PointDistance(LocX, LocY, Target.LocX, Target.LocY) < SkillAttributes[1])
                                                    PlayerTargets.Add(Target, Other.CalculateDamage(this, Target, 1, SkillId, SkillLvl));
                                                else
                                                    Attacking = false;
                                            }
                                            if (SkillAttributes[0] == 13)
                                            {
                                                PlayerTargets.Add(Target, Other.CalculateDamage(this, Target, 2, SkillId, SkillLvl));
                                            }
                                        }
                                }
                                else
                                {
                                    Character Target = (Character)World.AllChars[TargID];
                                    if (Target.Alive)
                                        if (!Target.Flying || SkillAttributes[0] != 12)
                                        {
                                            X = Target.LocX;
                                            Y = Target.LocY;

                                            if (SkillAttributes[0] == 16)
                                            {
                                                Heal = (short)SkillAttributes[3];

                                                if (Target.MaxHP - Target.CurHP < Heal)
                                                    Heal = (short)(Target.MaxHP - Target.CurHP);

                                                Target.CurHP += (ushort)Heal;

                                                Target.MyClient.SendPacket(General.MyPackets.Vital(Target.UID, 0, Target.CurHP));
                                            }
                                        }
                                }
                        }
                        if (SkillAttributes[0] == 12 || SkillAttributes[0] == 13)
                        {
                            if (SkillAttributes[4] == 255)
                            {
                                XpList = false;
                                MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                            }
                            World.UsingSkill(this, MobTargets, NPCTargets, PlayerTargets, (short)X, (short)Y, (short)SkillId, SkillLvl);
                        }
                        else
                            World.UsingSkill(this, (short)SkillId, SkillLvl, TargID, (uint)Heal, (short)X, (short)Y);
                    }

                    #endregion
                    #region SkillAttributes2n6
                    if (SkillAttributes[0] == 2 || SkillAttributes[0] == 6)
                    {
                        if (LocMap != 1039 && CurMP >= SkillAttributes[4])
                        {
                            CurMP -= (ushort)SkillAttributes[4];
                            MyClient.SendPacket(General.MyPackets.Vital(UID, 2, CurMP));
                        }
                        else if (LocMap != 1039)
                            return;

                        short Heal = 0;
                        if (TargID < 7000 && TargID >= 5000)
                        {
                            SingleNPC Target = (SingleNPC)NPCs.AllNPCs[TargID];

                            X = (ushort)Target.X;
                            Y = (ushort)Target.Y;

                            if (SkillAttributes[0] == 6)
                            {
                                Heal = 0;
                                if (SkillId == 1055 || SkillId == 1170)
                                {
                                    Heal = (short)SkillAttributes[3];
                                    string Effect = "";
                                    if (SkillId == 1055)
                                    {
                                        byte EffectLvl = (byte)(SkillLvl + 1);
                                        Effect = "allcure" + EffectLvl;
                                        if (SkillLvl == 0)
                                            Effect = "allcure";
                                        if (SkillLvl == 1)
                                            Effect = "allcure1";
                                    }
                                    else if (SkillId == 1170)
                                    {
                                        byte EffectLvl = (byte)(SkillLvl + 1);
                                        Effect = "zf2-e28" + EffectLvl; ;
                                    }
                                    foreach (Character Member in Team)
                                    {
                                        if (Member.LocMap == LocMap)
                                            if (MyMath.PointDistance(Member.LocX, Member.LocY, LocX, LocY) < 20)
                                            {
                                                if (Member.MaxHP - Member.CurHP < Heal)
                                                    Heal = (short)(Member.MaxHP - Member.CurHP);

                                                Member.CurHP += (ushort)Heal;

                                                Member.MyClient.SendPacket(General.MyPackets.Vital(Member.UID, 0, Member.CurHP));

                                                foreach (DictionaryEntry DE in World.AllChars)
                                                {
                                                    Character Chaar = (Character)DE.Value;
                                                    if (Chaar.Name != Member.Name)
                                                        if (Chaar.LocMap == Member.LocMap)
                                                            if (MyMath.PointDistance(Chaar.LocX, Chaar.LocY, Member.LocX, Member.LocY) < 20)
                                                            {
                                                                Chaar.MyClient.SendPacket(General.MyPackets.String(Member.UID, 10, Effect));
                                                            }
                                                }
                                                Member.MyClient.SendPacket(General.MyPackets.String(Member.UID, 10, Effect));
                                            }
                                    }
                                    if (MaxHP - CurHP < Heal)
                                        Heal = (short)(MaxHP - CurHP);

                                    CurHP += (ushort)Heal;

                                    if (CurHP > MaxHP)
                                        CurHP = MaxHP;

                                    MyClient.SendPacket(General.MyPackets.Vital(UID, 0, CurHP));

                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character Chaar = (Character)DE.Value;
                                        if (Chaar.Name != Name)
                                            if (Chaar.LocMap == LocMap)
                                                if (MyMath.PointDistance(Chaar.LocX, Chaar.LocY, LocX, LocY) < 20)
                                                {
                                                    Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, Effect));
                                                }
                                    }
                                    MyClient.SendPacket(General.MyPackets.String(UID, 10, Effect));
                                }
                            }
                            if (Level >= Target.Level)
                            {
                                if (SkillAttributes[0] == 2)
                                    NPCTargets.Add(Target, Other.CalculateDamage(this, Target, 3, SkillId, SkillLvl));

                                if (SkillAttributes[0] == 12)
                                {
                                    if (MyMath.PointDistance(LocX, LocY, Target.X, Target.Y) < SkillAttributes[1])
                                        NPCTargets.Add(Target, Other.CalculateDamage(this, Target, 1, SkillId, SkillLvl));
                                    else
                                        Attacking = false;
                                }

                                if (SkillAttributes[0] == 13)
                                    NPCTargets.Add(Target, Other.CalculateDamage(this, Target, 2, SkillId, SkillLvl));
                            }
                        }
                        else if (TargID > 400000 && TargID <= 500000)
                        {
                            SingleMob Target = (SingleMob)Mobs.AllMobs[TargID];

                            X = (ushort)Target.PosX;
                            Y = (ushort)Target.PosY;
                            if (SkillAttributes[0] == 6)
                            {
                                Heal = (short)SkillAttributes[3];

                                if (Target.MaxHP - Target.CurHP < Heal)
                                    Heal = (short)(Target.MaxHP - Target.CurHP);

                                Target.CurHP += (uint)Heal;

                            }
                            if (SkillAttributes[0] == 2)
                            {
                                if (PKMode == 0)
                                    if (Target.MType == 1 || Target.MType == 7)
                                    {
                                        GotBlueName = DateTime.Now;
                                        BlueName = true;
                                        MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                                        World.UpdateSpawn(this);
                                    }
                                if (PKMode == 0 || Target.MType != 1 && Target.MType != 7)
                                    MobTargets.Add(Target, Other.CalculateDamage(this, Target, 3, SkillId, SkillLvl));
                            }
                            if (SkillAttributes[0] == 12)
                            {
                                if (PKMode == 0)
                                    if (Target.MType == 1 || Target.MType == 7)
                                    {
                                        GotBlueName = DateTime.Now;
                                        BlueName = true;
                                        MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                                        World.UpdateSpawn(this);
                                    }

                                if (PKMode == 0 || Target.MType != 1 && Target.MType != 7)
                                {
                                    if (MyMath.PointDistance(LocX, LocY, Target.PosX, Target.PosY) < SkillAttributes[1])
                                        MobTargets.Add(Target, Other.CalculateDamage(this, Target, 1, SkillId, SkillLvl));
                                    else
                                        Attacking = false;
                                }
                            }
                            if (SkillAttributes[0] == 13)
                            {
                                if (PKMode == 0)
                                    if (Target.MType == 1 || Target.MType == 7)
                                    {
                                        GotBlueName = DateTime.Now;
                                        BlueName = true;
                                        MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                                        World.UpdateSpawn(this);
                                    }

                                if (PKMode == 0 || Target.MType != 1 && Target.MType != 7)
                                    MobTargets.Add(Target, Other.CalculateDamage(this, Target, 2, SkillId, SkillLvl));
                            }
                        }
                        else
                        {
                            if (!Other.NoPK(LocMap))//!Other.NoPK(LocMap)
                                if (PKMode == 2 || PKMode == 0)
                                {

                                    Character Target = (Character)World.AllChars[TargID];
                                    if (Target.Alive)
                                        if (!Target.Flying || SkillAttributes[0] != 12)
                                        {
                                            X = Target.LocX;
                                            Y = Target.LocY;

                                            if (SkillAttributes[0] == 6)
                                            {
                                                Heal = (short)SkillAttributes[3];

                                                if (Target.MaxHP - Target.CurHP < Heal)
                                                    Heal = (short)(Target.MaxHP - Target.CurHP);

                                                Target.CurHP += (ushort)Heal;
                                                Target.MyClient.SendPacket(General.MyPackets.Vital(Target.UID, 0, Target.CurHP));


                                                Target.MyClient.SendPacket(General.MyPackets.Vital(Target.UID, 0, Target.CurHP));
                                            }
                                            if (SkillAttributes[0] == 2)
                                            {
                                                PlayerTargets.Add(Target, Other.CalculateDamage(this, Target, 3, SkillId, SkillLvl));
                                            }
                                            if (SkillAttributes[0] == 12)
                                            {
                                                if (MyMath.PointDistance(LocX, LocY, Target.LocX, Target.LocY) < SkillAttributes[1])
                                                    PlayerTargets.Add(Target, Other.CalculateDamage(this, Target, 1, SkillId, SkillLvl));
                                                else
                                                    Attacking = false;
                                            }
                                            if (SkillAttributes[0] == 13)
                                            {
                                                PlayerTargets.Add(Target, Other.CalculateDamage(this, Target, 2, SkillId, SkillLvl));
                                            }
                                        }
                                }
                                else
                                {
                                    Character Target = (Character)World.AllChars[TargID];
                                    if (Target.Alive)
                                        if (!Target.Flying || SkillAttributes[0] != 12)
                                        {
                                            X = Target.LocX;
                                            Y = Target.LocY;

                                            if (SkillAttributes[0] == 6)
                                            {
                                                Heal = (short)SkillAttributes[3];

                                                if (Target.MaxHP - Target.CurHP < Heal)
                                                    Heal = (short)(Target.MaxHP - Target.CurHP);

                                                Target.CurHP += (ushort)Heal;

                                                Target.MyClient.SendPacket(General.MyPackets.Vital(Target.UID, 0, Target.CurHP));
                                            }
                                        }
                                }
                        }
                        if (SkillAttributes[0] == 2 || SkillAttributes[0] == 12 || SkillAttributes[0] == 13)
                        {
                            if (SkillAttributes[4] == 255)
                            {
                                XpList = false;
                                MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                            }
                            World.UsingSkill(this, MobTargets, NPCTargets, PlayerTargets, (short)X, (short)Y, (short)SkillId, SkillLvl);
                        }
                        else
                            World.UsingSkill(this, (short)SkillId, SkillLvl, TargID, (uint)Heal, (short)X, (short)Y);
                    }

                    #endregion
                    #region SkillAttributes14
                    if (SkillAttributes[0] == 14)
                    {
                        short Heal = 0;
                        if (Stamina >= SkillAttributes[4])
                        {
                            Stamina -= (byte)SkillAttributes[4];
                            MyClient.SendPacket(General.MyPackets.Vital(UID, 9, Stamina));
                        }


                        Character Target = MyClient.MyChar;
                        if (Target.Alive)
                            if (!Target.Flying || SkillAttributes[0] != 12)
                            {
                                X = Target.LocX;
                                Y = Target.LocY;

                                if (SkillAttributes[0] == 14)
                                {
                                    Heal = (short)SkillAttributes[3];

                                    if (Target.MaxMP - Target.CurMP < Heal)
                                        Heal = (short)(Target.MaxMP - Target.CurMP);

                                    Target.CurMP += (ushort)Heal;
                                    Target.MyClient.SendPacket(General.MyPackets.Vital(Target.UID, 2, Target.CurMP));


                                    Target.MyClient.SendPacket(General.MyPackets.Vital(Target.UID, 0, Target.CurHP));
                                }
                            }
                        World.UsingSkill(this, (short)SkillId, SkillLvl, TargID, (uint)Heal, (short)X, (short)Y);
                    }


                    #endregion
                    #region SkillAttributes1
                    if (SkillAttributes[0] == 1)
                    {
                        if (CurMP < SkillAttributes[4])
                            return;
                        if (LocMap != 1039)
                        {
                            CurMP -= (byte)SkillAttributes[4];
                            MyClient.SendPacket(General.MyPackets.Vital(UID, 2, CurMP));
                        }

                        if (SkillAttributes[0] == 1 || SkillAttributes[0] == 14 && SkillAttributes[5] == 1)
                        {
                            XpList = false;
                            MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                        }
                        foreach (DictionaryEntry DE in Mobs.AllMobs)
                        {
                            SingleMob Mob = (SingleMob)DE.Value;
                            if (Mob.Map == LocMap)
                                if (Mob.Alive)
                                    if (MyMath.PointDistance(LocX, LocY, Mob.PosX, Mob.PosY) <= SkillAttributes[1])
                                    {
                                        if (PKMode == 0)
                                            if (Mob.MType == 1 || Mob.MType == 7)
                                            {
                                                GotBlueName = DateTime.Now;
                                                BlueName = true;
                                                MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                                                World.UpdateSpawn(this);
                                            }
                                        if (PKMode == 0 || Mob.MType != 1 && Mob.MType != 7)
                                        {
                                            if (SkillAttributes[0] == 0)
                                                MobTargets.Add(Mob, Other.CalculateDamage(this, Mob, 1, SkillId, SkillLvl));
                                            else if (SkillAttributes[0] == 1)
                                                MobTargets.Add(Mob, Other.CalculateDamage(this, Mob, 3, SkillId, SkillLvl));
                                            else if (SkillAttributes[0] == 14)
                                                MobTargets.Add(Mob, Other.CalculateDamage(this, Mob, 2, SkillId, SkillLvl));
                                        }
                                    }
                        }
                        foreach (DictionaryEntry DE in NPCs.AllNPCs)
                        {
                            SingleNPC NPC = (SingleNPC)DE.Value;
                            if (Level >= NPC.Level)
                                if (NPC.Map == LocMap)
                                    if (NPC.Flags == 21 || NPC.Flags == 22 || NPC.Flags == 10)
                                        if (MyMath.PointDistance(LocX, LocY, NPC.X, NPC.Y) <= SkillAttributes[1])
                                        {
                                            if (SkillAttributes[0] == 0 || SkillAttributes[0] == 10)
                                                NPCTargets.Add(NPC, Other.CalculateDamage(this, NPC, 1, SkillId, SkillLvl));
                                            else if (SkillAttributes[0] == 1)
                                                NPCTargets.Add(NPC, Other.CalculateDamage(this, NPC, 3, SkillId, SkillLvl));
                                            else if (SkillAttributes[0] == 14)
                                                NPCTargets.Add(NPC, Other.CalculateDamage(this, NPC, 2, SkillId, SkillLvl));
                                        }
                        }
                        if (!Other.NoPK(LocMap))
                            if (PKMode == 2 || PKMode == 0)
                            {
                                foreach (DictionaryEntry DE in World.AllChars)
                                {
                                    Character Char = (Character)DE.Value;
                                    if (!Char.Flying)
                                        if (Char.LocMap == LocMap)
                                            if (Char != this)
                                                if (Char.Alive)
                                                    if (MyMath.PointDistance(LocX, LocY, Char.LocX, Char.LocY) <= SkillAttributes[1])
                                                    {
                                                        if (SkillAttributes[0] == 0)
                                                        {
                                                            PlayerTargets.Add(Char, Other.CalculateDamage(this, Char, 1, SkillId, SkillLvl));
                                                        }
                                                        else if (SkillAttributes[0] == 1)
                                                        {
                                                            PlayerTargets.Add(Char, Other.CalculateDamage(this, Char, 0, SkillId, SkillLvl));
                                                        }
                                                        else
                                                        {
                                                            PlayerTargets.Add(Char, Other.CalculateDamage(this, Char, 2, SkillId, SkillLvl));
                                                        }
                                                    }
                                }
                            }
                        World.UsingSkill(this, MobTargets, NPCTargets, PlayerTargets, (short)X, (short)Y, (short)SkillId, SkillLvl);
                    }
                    #endregion
                    #region SkillAttributes0
                    if (SkillAttributes[0] == 0)
                    {
                        if (Stamina < SkillAttributes[4])
                            return;
                        if (LocMap != 1039)
                        {
                            Stamina -= (byte)SkillAttributes[4];
                            MyClient.SendPacket(General.MyPackets.Vital(UID, 9, Stamina));
                        }

                        if (SkillAttributes[0] == 1 || SkillAttributes[0] == 14 && SkillAttributes[5] == 1)
                        {
                            XpList = false;
                            MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                        }
                        foreach (DictionaryEntry DE in Mobs.AllMobs)
                        {
                            SingleMob Mob = (SingleMob)DE.Value;
                            if (Mob.Map == LocMap)
                                if (Mob.Alive)
                                    if (MyMath.PointDistance(LocX, LocY, Mob.PosX, Mob.PosY) <= SkillAttributes[1])
                                    {
                                        if (PKMode == 0)
                                            if (Mob.MType == 1 || Mob.MType == 7)
                                            {
                                                GotBlueName = DateTime.Now;
                                                BlueName = true;
                                                MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                                                World.UpdateSpawn(this);
                                            }
                                        if (PKMode == 0 || Mob.MType != 1 && Mob.MType != 7)
                                        {
                                            if (SkillAttributes[0] == 0)
                                                MobTargets.Add(Mob, Other.CalculateDamage(this, Mob, 1, SkillId, SkillLvl));
                                            else if (SkillAttributes[0] == 1)
                                                MobTargets.Add(Mob, Other.CalculateDamage(this, Mob, 3, SkillId, SkillLvl));
                                            else if (SkillAttributes[0] == 14)
                                                MobTargets.Add(Mob, Other.CalculateDamage(this, Mob, 2, SkillId, SkillLvl));
                                        }
                                    }
                        }
                        foreach (DictionaryEntry DE in NPCs.AllNPCs)
                        {
                            SingleNPC NPC = (SingleNPC)DE.Value;
                            if (Level >= NPC.Level)
                                if (NPC.Map == LocMap)
                                    if (NPC.Flags == 21 || NPC.Flags == 22 || NPC.Flags == 10)
                                        if (MyMath.PointDistance(LocX, LocY, NPC.X, NPC.Y) <= SkillAttributes[1])
                                        {
                                            if (SkillAttributes[0] == 0 || SkillAttributes[0] == 10)
                                                NPCTargets.Add(NPC, Other.CalculateDamage(this, NPC, 1, SkillId, SkillLvl));
                                            else if (SkillAttributes[0] == 1)
                                                NPCTargets.Add(NPC, Other.CalculateDamage(this, NPC, 3, SkillId, SkillLvl));
                                            else if (SkillAttributes[0] == 14)
                                                NPCTargets.Add(NPC, Other.CalculateDamage(this, NPC, 2, SkillId, SkillLvl));
                                        }
                        }
                        if (!Other.NoPK(LocMap))
                            if (PKMode == 2 || PKMode == 0)
                            {
                                foreach (DictionaryEntry DE in World.AllChars)
                                {
                                    Character Char = (Character)DE.Value;
                                    if (!Char.Flying)
                                        if (Char.LocMap == LocMap)
                                            if (Char != this)
                                                if (Char.Alive)
                                                    if (MyMath.PointDistance(LocX, LocY, Char.LocX, Char.LocY) <= SkillAttributes[1])
                                                    {
                                                        if (SkillAttributes[0] == 0)
                                                        {
                                                            PlayerTargets.Add(Char, Other.CalculateDamage(this, Char, 1, SkillId, SkillLvl));
                                                        }
                                                        else if (SkillAttributes[0] == 1)
                                                        {
                                                            PlayerTargets.Add(Char, Other.CalculateDamage(this, Char, 0, SkillId, SkillLvl));
                                                        }
                                                        else
                                                        {
                                                            PlayerTargets.Add(Char, Other.CalculateDamage(this, Char, 2, SkillId, SkillLvl));
                                                        }
                                                    }
                                }
                            }
                        World.UsingSkill(this, MobTargets, NPCTargets, PlayerTargets, (short)X, (short)Y, (short)SkillId, SkillLvl);
                    }
                    #endregion
                    #region SkillAttributes4

                    if (SkillAttributes[0] == 4 && Stamina >= SkillAttributes[4])
                    {
                        if (LocMap != 1039)
                        {
                            Stamina -= (byte)SkillAttributes[4];
                            MyClient.SendPacket(General.MyPackets.Vital(UID, 9, Stamina));
                        }
                        foreach (DictionaryEntry DE in Mobs.AllMobs)
                        {
                            SingleMob Mob = (SingleMob)DE.Value;

                            if (Mob.Map == LocMap)
                                if (Mob.Alive)
                                    if (MyMath.PointDistance(LocX, LocY, Mob.PosX, Mob.PosY) < SkillAttributes[1])
                                        if (MyMath.PointDirecton(LocX, LocY, Mob.PosX, Mob.PosY) == MyMath.PointDirecton(LocX, LocY, X, Y))
                                        {
                                            if (PKMode == 0)
                                                if (Mob.MType == 1 || Mob.MType == 7)
                                                {
                                                    GotBlueName = DateTime.Now;
                                                    BlueName = true;
                                                    MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                                                    World.UpdateSpawn(this);
                                                }
                                            if (PKMode == 0 || Mob.MType != 1 && Mob.MType != 7)
                                                MobTargets.Add(Mob, Other.CalculateDamage(this, Mob, 0, SkillId, SkillLvl));
                                        }
                        }
                        foreach (DictionaryEntry DE in NPCs.AllNPCs)
                        {
                            SingleNPC NPC = (SingleNPC)DE.Value;

                            if (Level >= NPC.Level)
                                if (NPC.Map == LocMap)
                                    if (MyMath.PointDistance(LocX, LocY, NPC.X, NPC.Y) < SkillAttributes[1])
                                        if (MyMath.PointDirecton(LocX, LocY, NPC.X, NPC.Y) == MyMath.PointDirecton(LocX, LocY, X, Y))
                                            if (NPC.Flags == 21 || NPC.Flags == 22 || NPC.Flags == 10)
                                                NPCTargets.Add(NPC, Other.CalculateDamage(this, NPC, 0, SkillId, SkillLvl));

                        }
                        if (PKMode == 2 || PKMode == 0)
                            if (!Other.NoPK(LocMap))
                            {
                                foreach (DictionaryEntry DE in World.AllChars)
                                {
                                    Character Char = (Character)DE.Value;

                                    if (!Char.Flying)
                                        if (Char.LocMap == LocMap)
                                            if (Char != this)
                                                if (Char.Alive)
                                                    if (MyMath.PointDistance(LocX, LocY, Char.LocX, Char.LocY) < SkillAttributes[1])
                                                        if (MyMath.PointDirecton(LocX, LocY, Char.LocX, Char.LocY) == MyMath.PointDirecton(LocX, LocY, X, Y))
                                                        {
                                                            PlayerTargets.Add(Char, Other.CalculateDamage(this, Char, 0, SkillId, SkillLvl));
                                                        }

                                    if (AimBot == true && AimTarget != "")
                                    {
                                        if (Char.Name == AimTarget)
                                            if (Char.LocMap == LocMap)
                                                if (Char != this)
                                                    if (Char.Alive)
                                                        if (!Char.Flying)
                                                            if (MyMath.PointDistance(LocX, LocY, Char.LocX, Char.LocY) < SkillAttributes[1])
                                                                if (!PlayerTargets.Contains(Char))
                                                                    PlayerTargets.Add(Char, Other.CalculateDamage(this, Char, 0, SkillId, SkillLvl));

                                        if (Char.Name == AimTarget && Char.LocMap == LocMap && MyMath.PointDistance(LocX, LocY, Char.LocX, Char.LocY) < SkillAttributes[1])
                                        {
                                            AimX = Char.LocX;
                                            AimY = Char.LocY;
                                        }
                                        else
                                        {
                                            AimX = X;
                                            AimY = Y;
                                        }
                                    }
                                    else
                                    {
                                        AimX = X;
                                        AimY = Y;
                                    }
                                }
                            }

                        if (AimBot == true && PKMode != 2 && PKMode != 0)
                        {
                            AimX = X;
                            AimY = Y;
                        }

                        if (AimBot == true)
                            World.UsingSkill(this, MobTargets, NPCTargets, PlayerTargets, (short)AimX, (short)AimY, (short)SkillId, SkillLvl);
                        else
                            World.UsingSkill(this, MobTargets, NPCTargets, PlayerTargets, (short)X, (short)Y, (short)SkillId, SkillLvl);
                    }
                    #endregion

                    if (SkillId == 8030 || SkillId == 1320 || SkillId == 1010 || SkillId == 1125 || SkillId == 5001)
                    {
                        XpList = false;
                        XpCircle = 0;
                        XPActivated = DateTime.Now;

                        MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                    }
                }
            if (CycloneOn == false && SMOn == false)
            {
                if (Equips[4] != null && Equips[4] != "0")
                {
                    string[] Splitter = Equips[4].Split('-');
                    byte ItemBless = byte.Parse(Splitter[2]);

                    if (ItemBless > 9 && ItemBless < 18)
                    {
                        if (Other.ChanceSuccess(10))
                        {
                            if (CurHP > (MaxHP - 300))
                            {
                                CurHP += (ushort)(MaxHP - CurHP);
                            }
                            else
                                CurHP += 300;

                            foreach (DictionaryEntry DE in World.AllChars)
                            {
                                Character Charr = (Character)DE.Value;
                                if (Charr.Name != Name)
                                    Charr.MyClient.SendPacket(General.MyPackets.CharSkillUse(this, this, 300, 10201, 0));
                            }
                            MyClient.SendPacket(General.MyPackets.CharSkillUse(this, this, 300, 10201, 0));
                            MyClient.SendPacket(General.MyPackets.Vital((long)UID, 0, CurHP));
                        }
                    }
                    if (ItemBless > 19 && ItemBless < 28)
                    {
                        if (Other.ChanceSuccess(10))
                        {
                            short Heal = 0;
                            if (CurMP > (MaxMP - 310))
                            {
                                Heal = (short)(MaxMP - CurMP);
                                CurMP += (ushort)Heal;
                            }
                            else
                            {
                                Heal = 310;
                                CurMP += 310;
                            }

                            foreach (DictionaryEntry DE in World.AllChars)
                            {
                                Character Charr = (Character)DE.Value;
                                if (Charr.Name != Name)
                                    Charr.MyClient.SendPacket(General.MyPackets.CharSkillUse(this, this, (uint)Heal, 10202, 0));
                            }
                            MyClient.SendPacket(General.MyPackets.CharSkillUse(this, this, (uint)Heal, 10202, 0));
                            MyClient.SendPacket(General.MyPackets.Vital((long)UID, 2, CurMP));
                        }
                    }
                    if (ItemBless > 29 && ItemBless < 38)
                    {
                        if (!Other.NoPK(LocMap))
                        {
                            if (Other.ChanceSuccess(10))
                            {
                                Character Target = (Character)World.AllChars[TargID];

                                if (Target.UID != UID)
                                {
                                    Target.BurnBuff = true;
                                    Target.Burned = DateTime.Now;
                                    Target.MyClient.SendPacket(General.MyPackets.Vital(Target.UID, 26, Target.GetStat()));
                                    World.UpdateSpawn(Target);
                                }
                            }
                        }
                    }
                    if (ItemBless > 39 && ItemBless < 48)
                    {
                        if (!Other.NoPK(LocMap))
                        {
                            if (Other.ChanceSuccess(10))
                            {
                                Character Target = (Character)World.AllChars[TargID];

                                if (Target.UID != UID)
                                {
                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character Charr = (Character)DE.Value;
                                        if (Charr.Name != Name)
                                            Charr.MyClient.SendPacket(General.MyPackets.String(Target.UID, 10, "nomove"));
                                    }
                                    MyClient.SendPacket(General.MyPackets.String(Target.UID, 10, "nomove"));
                                    Target.PoisonBuff = true;
                                    Target.Poisoned = DateTime.Now;
                                    Target.MyClient.SendPacket(General.MyPackets.Vital(Target.UID, 26, Target.GetStat()));
                                    World.UpdateSpawn(Target);
                                }
                            }
                        }
                    }
                }
                if (Equips[5] != null && Equips[5] != "0")
                {
                    string[] Splitter = Equips[5].Split('-');
                    byte ItemBless = byte.Parse(Splitter[2]);

                    if (ItemBless > 9 && ItemBless < 18)
                    {
                        if (Other.ChanceSuccess(10))
                        {
                            if (CurHP > (MaxHP - 300))
                            {
                                CurHP += (ushort)(MaxHP - CurHP);
                            }
                            else
                                CurHP += 300;

                            foreach (DictionaryEntry DE in World.AllChars)
                            {
                                Character Charr = (Character)DE.Value;
                                if (Charr.Name != Name)
                                    Charr.MyClient.SendPacket(General.MyPackets.CharSkillUse(this, this, 300, 10201, 0));
                            }
                            MyClient.SendPacket(General.MyPackets.CharSkillUse(this, this, 300, 10201, 0));
                            MyClient.SendPacket(General.MyPackets.Vital((long)UID, 0, CurHP));
                        }
                    }
                    if (ItemBless > 19 && ItemBless < 28)
                    {
                        if (Other.ChanceSuccess(10))
                        {
                            short Heal = 0;
                            if (CurMP > (MaxMP - 310))
                            {
                                Heal = (short)(MaxMP - CurMP);
                                CurMP += (ushort)Heal;
                            }
                            else
                            {
                                Heal = 310;
                                CurMP += 310;
                            }

                            foreach (DictionaryEntry DE in World.AllChars)
                            {
                                Character Charr = (Character)DE.Value;
                                if (Charr.Name != Name)
                                    Charr.MyClient.SendPacket(General.MyPackets.CharSkillUse(this, this, (uint)Heal, 10202, 0));
                            }
                            MyClient.SendPacket(General.MyPackets.CharSkillUse(this, this, (uint)Heal, 10202, 0));
                            MyClient.SendPacket(General.MyPackets.Vital((long)UID, 2, CurMP));
                        }
                    }
                    if (ItemBless > 29 && ItemBless < 38)
                    {
                        if (!Other.NoPK(LocMap))
                        {
                            if (Other.ChanceSuccess(10))
                            {
                                Character Target = (Character)World.AllChars[TargID];

                                if (Target.UID != UID)
                                {
                                    Target.BurnBuff = true;
                                    Target.Burned = DateTime.Now;
                                    Target.MyClient.SendPacket(General.MyPackets.Vital(Target.UID, 26, Target.GetStat()));
                                    World.UpdateSpawn(Target);
                                }
                            }
                        }
                    }
                    if (ItemBless > 39 && ItemBless < 48)
                    {
                        if (!Other.NoPK(LocMap))
                        {
                            if (Other.ChanceSuccess(10))
                            {
                                Character Target = (Character)World.AllChars[TargID];

                                if (Target.UID != UID)
                                {
                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character Charr = (Character)DE.Value;
                                        if (Charr.Name != Name)
                                            Charr.MyClient.SendPacket(General.MyPackets.String(Target.UID, 10, "nomove"));
                                    }
                                    MyClient.SendPacket(General.MyPackets.String(Target.UID, 10, "nomove"));
                                    Target.PoisonBuff = true;
                                    Target.Poisoned = DateTime.Now;
                                    Target.MyClient.SendPacket(General.MyPackets.Vital(Target.UID, 26, Target.GetStat()));
                                    World.UpdateSpawn(Target);
                                }
                            }
                        }
                    }
                }
            }
            GemEffect();
            Ready = true;
        }

        public void Save()
        {
            LastSave = DateTime.Now;
            if (MyClient.There)
                if (MyClient.Online)
                    DataBase.SaveChar(this);
        }

        public void SaveHB()
        {
            if (MyClient.There)
                if (MyClient.Online)
                    DataBase.SaveHB(this);
        }

        public void SaveSpouse()
        {
            if (MyClient.There)
                if (MyClient.Online)
                    DataBase.SaveSpouse(this);
        }

        public void SaveKO()
        {
            if (MyClient.There)
                if (MyClient.Online)
                    DataBase.SaveKO(this);
        }

        public void SaveDisKO()
        {
            if (MyClient.There)
                if (MyClient.Online)
                    DataBase.SaveDisKO(this);
        }

        public void SaveBan()
        {
            if (MyClient.There)
                if (MyClient.Online)
                    DataBase.SaveBan(this);
        }

        public void SaveWHPW()
        {
            if (MyClient.There)
                if (MyClient.Online)
                    DataBase.SaveWHPW(this);
        }

        public void SaveFirstRB()
        {
            if (MyClient.There)
                if (MyClient.Online)
                    DataBase.SaveFirstRB(this);
        }
        public void SaveSecondRB()
        {
            if (MyClient.There)
                if (MyClient.Online)
                    DataBase.SaveSecondRB(this);
        }

        public bool Ready = true;

        /*
          HeadGear-1
          Necklace-2
          Armor-3
          RightHand-4
          LeftHand-5
          Ring-6
          Misc-7
          Boots-8
          Garment-9
         */

        public bool InventoryContains(uint Id, uint Amount)
        {
            Ready = false;
            uint Count = 0;

            foreach (string item in Inventory)
            {
                if (item != null && item != "")
                {
                    string[] Splitter = item.Split('-');
                    if (uint.Parse(Splitter[0]) == Id)
                        Count++;
                }
            }

            Ready = true;

            if (Count >= Amount)
                return true;
            else
                return false;
        }

        public uint ItemNext(uint Id)
        {
            Ready = false;
            int Count = 0;
            uint IUID = 0;

            foreach (string item in Inventory)
            {
                if (item != null && item != "")
                {
                    string[] Splitter = item.Split('-');
                    if (uint.Parse(Splitter[0]) == Id)
                    {
                        IUID = (uint)Inventory_UIDs[Count];
                        break;
                    }
                }
                Count++;
            }

            Ready = true;

            return IUID;
        }

        public bool AddExp(ulong Amount, bool CountMisc)
        {
            ulong ExpPot = 1;
            ulong NoXpMap = 1;
            ulong Lucky = 1;
            ulong Rb = 1;

            if (dexp == 1)
                ExpPot = 2;

            if (dexp == 2)
                ExpPot = 3;

            if (dexp == 3)
                ExpPot = 5;

            if (LocMap == 1767)
                NoXpMap = 0;

            if (RBCount == 2)
                Rb = 3;

            if (LuckTime > 0 && Other.ChanceSuccess(10))
            {
                Lucky = 2;
                foreach (DictionaryEntry DE in World.AllChars)
                {
                    Character Chaar = (Character)DE.Value;
                    if (Chaar.Name != Name)
                    {
                        Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "LuckyGuy"));
                    }
                }
                MyClient.SendPacket(General.MyPackets.String(UID, 10, "LuckyGuy"));
            }

            Ready = false;

            if (CountMisc)
                Exp += (ulong)(((((Amount * DataBase.ExpRate * AddExpPc * (Convert.ToDouble(Potency) / 100 + 1)) * ExpPot) * NoXpMap) * Lucky) / Rb);
            else
                Exp += (Amount / Rb);
            bool Leveled = false;

            if (Exp > DataBase.NeededXP(Level) && Level < 137)
            {
                Leveled = true;
                while (Exp > DataBase.NeededXP(Level) && Level < 137)
                {
                    Exp -= DataBase.NeededXP(Level);
                    Level++;
                    if (Level == 3 || Level == 20)
                    {
                        if (Job < 16 && Job > 9)
                            LearnSkill(1110, 0);
                        if (Job < 16 && Job > 9 || Job < 26 && Job > 19)
                            LearnSkill(1015, 0);
                        if (Job < 26 && Job > 19)
                            LearnSkill(1025, 0);
                    }
                }
            }
            if (Leveled)
            {
                if (MyGuild != null)
                    MyGuild.Refresh(this);
                if (RBCount == 0)
                {
                    DataBase.GetStats(this);
                    GetEquipStats(1, true);
                    GetEquipStats(2, true);
                    GetEquipStats(3, true);
                    GetEquipStats(4, true);
                    GetEquipStats(5, true);
                    GetEquipStats(6, true);
                    GetEquipStats(8, true);
                    MaxHP = BaseMaxHP();
                    MaxMana();
                    MinAtk = Str;
                    MaxAtk = Str;
                    Potency = Level;
                    GetEquipStats(1, false);
                    GetEquipStats(2, false);
                    GetEquipStats(3, false);
                    GetEquipStats(4, false);
                    GetEquipStats(5, false);
                    GetEquipStats(6, false);
                    GetEquipStats(8, false);

                    CurHP = MaxHP;

                    MyClient.SendPacket(General.MyPackets.Vital((long)UID, 13, Level));
                    MyClient.SendPacket(General.MyPackets.Vital((long)UID, 16, Str));
                    MyClient.SendPacket(General.MyPackets.Vital((long)UID, 17, Agi));
                    MyClient.SendPacket(General.MyPackets.Vital((long)UID, 15, Vit));
                    MyClient.SendPacket(General.MyPackets.Vital((long)UID, 14, Spi));
                    MyClient.SendPacket(General.MyPackets.Vital((long)UID, 0, CurHP));
                    World.LevelUp(this);
                }
                if (RBCount == 1 || RBCount == 2)
                {
                    uint AddStats = 0;

                    if (Level == 130)
                    {
                        if (RBCount == 1)
                            if (FirstLevel > 130)
                                AddStats = (uint)((FirstLevel - Level) * 3);

                        if (RBCount == 2)
                            if (SecondLevel > 130)
                                AddStats = (uint)((SecondLevel - Level) * 3);

                        if (RBCount == 1)
                            if (FirstLevel > 130)
                                Level = FirstLevel;

                        if (RBCount == 2)
                            if (SecondLevel > 130)
                                Level = SecondLevel;
                    }

                    GetEquipStats(1, true);
                    GetEquipStats(2, true);
                    GetEquipStats(3, true);
                    GetEquipStats(4, true);
                    GetEquipStats(5, true);
                    GetEquipStats(6, true);
                    GetEquipStats(8, true);
                    MaxHP = BaseMaxHP();
                    MaxMana();
                    MinAtk = Str;
                    MaxAtk = Str;
                    Potency = Level;
                    GetEquipStats(1, false);
                    GetEquipStats(2, false);
                    GetEquipStats(3, false);
                    GetEquipStats(4, false);
                    GetEquipStats(5, false);
                    GetEquipStats(6, false);
                    GetEquipStats(8, false);

                    CurHP = MaxHP;

                    MyClient.SendPacket(General.MyPackets.Vital((long)UID, 13, Level));
                    MyClient.SendPacket(General.MyPackets.Vital((long)UID, 0, CurHP));
                    World.LevelUp(this);

                    StatP += 3;
                    StatP += AddStats;
                    MyClient.SendPacket(General.MyPackets.Vital(UID, 11, StatP));
                }
            }
            MyClient.SendPacket(General.MyPackets.Vital((long)UID, 5, Exp));
            Ready = true;

            if (Leveled)
                return true;
            else
                return false;
        }

        public void AddSkillExp(short Type, double Amount)
        {
            Ready = false;
            Amount *= DataBase.ProfExpRate;

            if (Type >= 4000 && Type <= 4070)
                Amount = 1;
            if (Type == 1220)
                Amount = 0;

            if (Skills.Contains((short)Type))
            {

                bool SkillLeveled = false;
                byte Skill_Lv = (byte)Skills[(short)Type];
                uint Skill_Exp;
                if (Skill_Exps.Contains((short)Type))
                    Skill_Exp = (uint)Skill_Exps[(short)Type];
                else
                    Skill_Exp = 0;

                int MaxSkillLv = 9;

                MaxSkillLv = (int)DataBase.SkillsDone[(int)Type];

                Amount += (Amount * AddSpellPc);
                Skill_Exp += (uint)Amount;

                if (Skill_Exp >= DataBase.NeededSkillExp((short)Type, Skill_Lv) && Skill_Lv < MaxSkillLv)
                {
                    Skill_Lv++;
                    Skill_Exp = 0;
                    SkillLeveled = true;
                }
                if (SkillLeveled)
                {
                    Skills.Remove((short)Type);
                    Skills.Add((short)Type, Skill_Lv);
                    MyClient.SendPacket(General.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Le niveau de votre attaque a �t� augment�.", 2005));
                }
                if (Skill_Exps.Contains((short)Type))
                    Skill_Exps.Remove((short)Type);
                Skill_Exps.Add(Type, Skill_Exp);
                MyClient.SendPacket(General.MyPackets.LearnSkill(Type, Skill_Lv, Skill_Exp));
            }

            Ready = true;
        }

        public void AddProfExp(short Type, double Amount)
        {
            Ready = false;
            Amount *= DataBase.ProfExpRate;
            if (Prof_Exps.Contains(Type))
            {
                bool ProfLeveled = false;
                byte Prof_Lev = (byte)Profs[Type];
                uint Prof_Exp = (uint)Prof_Exps[Type];

                if (Prof_Lev < 20)
                {
                    Amount += (Amount * AddProfPc);
                    Prof_Exp += (uint)Amount;
                    if (Prof_Lev >= 20)
                    {

                        Prof_Lev = 20;
                        Prof_Exp = 0;
                        ProfLeveled = false;

                    }
                    if (Prof_Exp > DataBase.NeededProfXP(Prof_Lev))
                    {
                        Prof_Lev++;
                        Prof_Exp = 0;
                        ProfLeveled = true;
                    }
                    if (ProfLeveled)
                    {
                        Profs.Remove(Type);
                        Profs.Add(Type, Prof_Lev);
                        MyClient.SendPacket(General.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Le niveau de votre comp�tence a �t� augment�.", 2005));
                    }
                    Prof_Exps.Remove(Type);
                    Prof_Exps.Add(Type, Prof_Exp);
                    MyClient.SendPacket(General.MyPackets.Prof(Type, Prof_Lev, Prof_Exp));
                }
            }
            else
            {
                byte Lev = 1;
                uint PExp = 0;
                Profs.Add(Type, Lev);
                Prof_Exps.Add(Type, PExp);
                MyClient.SendPacket(General.MyPackets.Prof(Type, Lev, PExp));
            }
            Ready = true;
        }

        public bool WeaponSkill()
        {
            string[] Splitter;
            bool Use = false;
            int Hand = 1;

            if (Equips[5] != null && Equips[5] != "0")
                if (Other.ChanceSuccess(50))
                    Hand = 2;

            if (Equips[4] != null && Equips[4] != "0" && Hand == 1)
            {
                Splitter = Equips[4].Split('-');
                int WepType = Other.WeaponType(uint.Parse(Splitter[0]));

                if (WepType == 480)
                {
                    if (Skills.Contains((short)7020))
                    {
                        byte SkillLvl = (byte)Skills[(short)7020];
                        byte Chance = (byte)DataBase.SkillAttributes[(int)7020][(int)SkillLvl][5];
                        if (Other.ChanceSuccess(Chance))
                        {
                            UseSkill(7020, 0, 0, 0);
                            Use = true;
                        }
                    }
                }
                else if (WepType == 420 && !Use || WepType == 421 && !Use)
                {
                    if (TargetUID != 0)
                        if (Skills.Contains((short)5030))
                        {
                            byte SkillLvl = (byte)Skills[(short)5030];
                            byte Chance = (byte)DataBase.SkillAttributes[(int)5030][(int)SkillLvl][5];
                            if (Other.ChanceSuccess(Chance))
                            {
                                UseSkill(5030, 0, 0, TargetUID);
                                Use = true;
                            }
                        }
                }
                else if (WepType == 561 && !Use)
                {
                    if (Skills.Contains((short)5010))
                    {
                        byte SkillLvl = (byte)Skills[(short)5010];
                        byte Chance = (byte)DataBase.SkillAttributes[(int)5010][(int)SkillLvl][5];
                        if (Other.ChanceSuccess(Chance))
                        {
                            UseSkill(5010, 0, 0, 0);
                            Use = true;
                        }
                    }
                }
                else if (WepType == 560 && !Use)
                {
                    if (TargetUID != 0)
                        if (Skills.Contains((short)1260))
                        {
                            byte SkillLvl = (byte)Skills[(short)1260];
                            byte Chance = (byte)DataBase.SkillAttributes[(int)1260][(int)SkillLvl][5];
                            if (Other.ChanceSuccess(Chance))
                            {
                                UseSkill(1260, 0, 0, TargetUID);
                                Use = true;
                            }
                        }
                }
                else if (WepType == 490 && !Use)
                {
                    if (TargetUID != 0)
                        if (Skills.Contains((short)1290))
                        {
                            byte SkillLvl = (byte)Skills[(short)1290];
                            byte Chance = (byte)DataBase.SkillAttributes[(int)1290][(int)SkillLvl][5];
                            if (Other.ChanceSuccess(Chance))
                            {
                                UseSkill(1290, 0, 0, TargetUID);
                                Use = true;
                            }
                        }
                }
                else if (WepType == 481 && !Use)
                {
                    if (TargetUID != 0)
                        if (Skills.Contains((short)7030))
                        {
                            byte SkillLvl = (byte)Skills[(short)7030];
                            byte Chance = (byte)DataBase.SkillAttributes[(int)7030][(int)SkillLvl][5];
                            if (Other.ChanceSuccess(Chance))
                            {
                                UseSkill(7030, 0, 0, TargetUID);
                                Use = true;
                            }
                        }
                }
                else if (WepType == 410 && !Use)//found weapon skills!
                {
                    if (Skills.Contains((short)1220))
                    {
                        byte SkillLvl = (byte)Skills[(short)1220];
                        byte Chance = (byte)DataBase.SkillAttributes[(int)1220][(int)SkillLvl][5];
                        if (Other.ChanceSuccess(Chance))
                        {
                            UseSkill(1220, 0, 0, TargetUID);
                            Use = true;
                        }
                    }
                }
                else if (WepType == 500 && !Use)//FreezingArrow && Poison
                {
                    if (Skills.Contains((short)5000))
                    {
                        byte SkillLvl = (byte)Skills[(short)5000];
                        byte Chance = (byte)DataBase.SkillAttributes[(int)5000][(int)SkillLvl][5];
                        if (Other.ChanceSuccess(Chance))
                        {
                            UseSkill(5000, 0, 0, TargetUID);
                            Use = true;
                        }
                    }
                    /*if (TargetUID != 0)
                        if (Skills.Contains((short)5002))
                        {
                            byte SkillLvl = (byte)Skills[(short)5002];
                            byte Chance = (byte)DataBase.SkillAttributes[(int)5002][(int)SkillLvl][5];
                            if (Other.ChanceSuccess(Chance))
                            {
                                UseSkill(5002, 0, 0, TargetUID);
                                Use = true;
                            }
                        }*/
                }
                else if (WepType == 540 && !Use)
                {
                    if (Skills.Contains((short)1300))
                    {
                        byte SkillLvl = (byte)Skills[(short)1300];
                        byte Chance = (byte)DataBase.SkillAttributes[(int)1300][(int)SkillLvl][5];
                        if (Other.ChanceSuccess(Chance))
                        {
                            UseSkill(1300, 0, 0, 0);
                            Use = true;
                        }
                    }
                }
                else if (WepType == 580 && !Use)
                {
                    if (Skills.Contains((short)5020))
                    {
                        byte SkillLvl = (byte)Skills[(short)5020];
                        byte Chance = (byte)DataBase.SkillAttributes[(int)5020][(int)SkillLvl][5];
                        if (Other.ChanceSuccess(Chance))
                        {
                            UseSkill(5020, 0, 0, 0);
                            Use = true;
                        }
                    }
                }
                else if (WepType == 510 && !Use)
                {
                    if (Skills.Contains((short)1250))
                    {
                        byte SkillLvl = (byte)Skills[(short)1250];
                        byte Chance = (byte)DataBase.SkillAttributes[(int)1250][(int)SkillLvl][5];
                        if (Other.ChanceSuccess(Chance))
                        {
                            UseSkill(1250, 0, 0, 0);
                            Use = true;
                        }
                    }
                }
                else if (WepType == 530 && !Use)
                {
                    if (Skills.Contains((short)5050))
                    {
                        byte SkillLvl = (byte)Skills[(short)5050];
                        byte Chance = (byte)DataBase.SkillAttributes[(int)5050][(int)SkillLvl][5];
                        if (Other.ChanceSuccess(Chance))
                        {
                            UseSkill(5050, 0, 0, 0);
                            Use = true;
                        }
                    }
                }
                else if (WepType == 430 && !Use)
                {
                    if (TargetUID != 0)
                        if (Skills.Contains((short)7000))
                        {
                            byte SkillLvl = (byte)Skills[(short)7000];
                            byte Chance = (byte)DataBase.SkillAttributes[(int)7000][(int)SkillLvl][5];
                            if (Other.ChanceSuccess(Chance))
                            {
                                UseSkill(7000, 0, 0, TargetUID);
                                Use = true;
                            }
                        }
                }
                else if (WepType == 450 && !Use)
                {
                    if (TargetUID != 0)
                        if (Skills.Contains((short)7010))
                        {
                            byte SkillLvl = (byte)Skills[(short)7010];
                            byte Chance = (byte)DataBase.SkillAttributes[(int)7010][(int)SkillLvl][5];
                            if (Other.ChanceSuccess(Chance))
                            {
                                UseSkill(7010, 0, 0, TargetUID);
                                Use = true;
                            }
                        }
                }
                else if (WepType == 440 && !Use)
                {
                    if (Skills.Contains((short)7040))
                    {
                        byte SkillLvl = (byte)Skills[(short)7040];
                        byte Chance = (byte)DataBase.SkillAttributes[(int)7040][(int)SkillLvl][5];
                        if (Other.ChanceSuccess(Chance))
                        {
                            UseSkill(7040, 0, 0, 0);
                            Use = true;
                        }
                    }
                }
                else if (WepType == 460 && !Use)
                {
                    if (TargetUID != 0)
                        if (Skills.Contains((short)5040))
                        {
                            byte SkillLvl = (byte)Skills[(short)5040];
                            byte Chance = (byte)DataBase.SkillAttributes[(int)5040][(int)SkillLvl][5];
                            if (Other.ChanceSuccess(Chance))
                            {
                                UseSkill(5040, 0, 0, TargetUID);
                                Use = true;
                            }
                        }
                }
            }
            if (Equips[5] != null && Equips[5] != "0" && Hand == 2)
            {
                Splitter = Equips[5].Split('-');
                int WepType = Other.WeaponType(uint.Parse(Splitter[0]));

                if (WepType == 480 && !Use)
                {
                    if (Skills.Contains((short)7020))
                    {
                        byte SkillLvl = (byte)Skills[(short)7020];
                        byte Chance = (byte)DataBase.SkillAttributes[(int)7020][(int)SkillLvl][5];
                        if (Other.ChanceSuccess(Chance))
                        {
                            UseSkill(7020, 0, 0, 0);
                            Use = true;
                        }
                    }
                }
                else if (WepType == 420 && !Use || WepType == 421 && !Use)
                {
                    if (TargetUID != 0)
                        if (Skills.Contains((short)5030))
                        {
                            byte SkillLvl = (byte)Skills[(short)5030];
                            byte Chance = (byte)DataBase.SkillAttributes[(int)5030][(int)SkillLvl][5];
                            if (Other.ChanceSuccess(Chance))
                            {
                                UseSkill(5030, 0, 0, TargetUID);
                                Use = true;
                            }
                        }
                }
                else if (WepType == 561 && !Use)
                {
                    if (Skills.Contains((short)5010))
                    {
                        byte SkillLvl = (byte)Skills[(short)5010];
                        byte Chance = (byte)DataBase.SkillAttributes[(int)7020][(int)SkillLvl][5];
                        if (Other.ChanceSuccess(Chance))
                        {
                            UseSkill(5010, 0, 0, 0);
                            Use = true;
                        }
                    }
                }
                else if (WepType == 560 && !Use)
                {
                    if (TargetUID != 0)
                        if (Skills.Contains((short)1260))
                        {
                            byte SkillLvl = (byte)Skills[(short)1260];
                            byte Chance = (byte)DataBase.SkillAttributes[(int)1260][(int)SkillLvl][5];
                            if (Other.ChanceSuccess(Chance))
                            {
                                UseSkill(1260, 0, 0, TargetUID);
                                Use = true;
                            }
                        }
                }
                else if (WepType == 490 && !Use)
                {
                    if (TargetUID != 0)
                        if (Skills.Contains((short)1290))
                        {
                            byte SkillLvl = (byte)Skills[(short)1290];
                            byte Chance = (byte)DataBase.SkillAttributes[(int)1290][(int)SkillLvl][5];
                            if (Other.ChanceSuccess(Chance))
                            {
                                UseSkill(1290, 0, 0, TargetUID);
                                Use = true;
                            }
                        }
                }
                else if (WepType == 481 && !Use)
                {
                    if (TargetUID != 0)
                        if (Skills.Contains((short)7030))
                        {
                            byte SkillLvl = (byte)Skills[(short)7030];
                            byte Chance = (byte)DataBase.SkillAttributes[(int)7030][(int)SkillLvl][5];
                            if (Other.ChanceSuccess(Chance))
                            {
                                UseSkill(7030, 0, 0, TargetUID);
                                Use = true;
                            }
                        }
                }
                else if (WepType == 540 && !Use)
                {
                    if (Skills.Contains((short)1300))
                    {
                        byte SkillLvl = (byte)Skills[(short)1300];
                        byte Chance = (byte)DataBase.SkillAttributes[(int)1300][(int)SkillLvl][5];
                        if (Other.ChanceSuccess(Chance))
                        {
                            UseSkill(1300, 0, 0, 0);
                            Use = true;
                        }
                    }
                }
                else if (WepType == 580 && !Use)
                {
                    if (Skills.Contains((short)5020))
                    {
                        byte SkillLvl = (byte)Skills[(short)5020];
                        byte Chance = (byte)DataBase.SkillAttributes[(int)5020][(int)SkillLvl][5];
                        if (Other.ChanceSuccess(Chance))
                        {
                            UseSkill(5020, 0, 0, 0);
                            Use = true;
                        }
                    }
                }
                else if (WepType == 510 && !Use)
                {
                    if (Skills.Contains((short)1250))
                    {
                        byte SkillLvl = (byte)Skills[(short)1250];
                        byte Chance = (byte)DataBase.SkillAttributes[(int)1250][(int)SkillLvl][5];
                        if (Other.ChanceSuccess(Chance))
                        {
                            UseSkill(1250, 0, 0, 0);
                            Use = true;
                        }
                    }
                }
                else if (WepType == 530 && !Use)
                {
                    if (Skills.Contains((short)5050))
                    {
                        byte SkillLvl = (byte)Skills[(short)5050];
                        byte Chance = (byte)DataBase.SkillAttributes[(int)5050][(int)SkillLvl][5];
                        if (Other.ChanceSuccess(Chance))
                        {
                            UseSkill(5050, 0, 0, 0);
                            Use = true;
                        }
                    }
                }
                else if (WepType == 430 && !Use)
                {
                    if (TargetUID != 0)
                        if (Skills.Contains((short)7000))
                        {
                            byte SkillLvl = (byte)Skills[(short)7000];
                            byte Chance = (byte)DataBase.SkillAttributes[(int)7000][(int)SkillLvl][5];
                            if (Other.ChanceSuccess(Chance))
                            {
                                UseSkill(7000, 0, 0, TargetUID);
                                Use = true;
                            }
                        }
                }
                else if (WepType == 450 && !Use)
                {
                    if (TargetUID != 0)
                        if (Skills.Contains((short)7010))
                        {
                            byte SkillLvl = (byte)Skills[(short)7010];
                            byte Chance = (byte)DataBase.SkillAttributes[(int)7010][(int)SkillLvl][5];
                            if (Other.ChanceSuccess(Chance))
                            {
                                UseSkill(7010, 0, 0, TargetUID);
                                Use = true;
                            }
                        }
                }
                else if (WepType == 440 && !Use)
                {
                    if (Skills.Contains((short)7040))
                    {
                        byte SkillLvl = (byte)Skills[(short)7040];
                        byte Chance = (byte)DataBase.SkillAttributes[(int)7040][(int)SkillLvl][5];
                        if (Other.ChanceSuccess(Chance))
                        {
                            UseSkill(7040, 0, 0, 0);
                            Use = true;
                        }
                    }
                }
                else if (WepType == 460 && !Use)
                {
                    if (TargetUID != 0)
                        if (Skills.Contains((short)5040))
                        {
                            byte SkillLvl = (byte)Skills[(short)5040];
                            byte Chance = (byte)DataBase.SkillAttributes[(int)5040][(int)SkillLvl][5];
                            if (Other.ChanceSuccess(Chance))
                            {
                                UseSkill(5040, 0, 0, TargetUID);
                                Use = true;
                            }
                        }
                }
                else if (WepType == 410 && !Use)
                {
                    if (Skills.Contains((short)1220))
                    {
                        byte SkillLvl = (byte)Skills[(short)1220];
                        byte Chance = (byte)DataBase.SkillAttributes[(int)1220][(int)SkillLvl][5];
                        if (Other.ChanceSuccess(Chance))
                        {
                            UseSkill(1220, 0, 0, TargetUID);
                            Use = true;
                        }
                    }
                }
                else if (WepType == 500 && !Use)//FreezingArrow && Poison
                {
                    if (Skills.Contains((short)5000))
                    {
                        byte SkillLvl = (byte)Skills[(short)5000];
                        byte Chance = (byte)DataBase.SkillAttributes[(int)5000][(int)SkillLvl][5];
                        if (Other.ChanceSuccess(Chance))
                        {
                            UseSkill(5000, 0, 0, TargetUID);
                            Use = true;
                        }
                    }
                    /*if (TargetUID != 0)
                        if (Skills.Contains((short)5002))
                        {
                            byte SkillLvl = (byte)Skills[(short)5002];
                            byte Chance = (byte)DataBase.SkillAttributes[(int)5002][(int)SkillLvl][5];
                            if (Other.ChanceSuccess(Chance))
                            {
                                UseSkill(5002, 0, 0, TargetUID);
                                Use = true;
                            }
                        }*/
                }
            }
            return Use;
        }

        public void BlueNameGone()
        {
            BlueName = false;
            MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
            World.UpdateSpawn(this);
        }

        public void MissAttack(SingleMob Mob)
        {
            World.AttackMiss(this, AtkType, Mob.PosX, Mob.PosY);
        }
        public void MissAttack(SingleNPC NPC)
        {
            World.AttackMiss(this, AtkType, NPC.X, NPC.Y);
        }
        public void MissAttack(Character Player)
        {
            World.AttackMiss(this, AtkType, (short)Player.LocX, (short)Player.LocY);
        }

        public void Attack()
        {
            LastAttack = DateTime.Now;
            if (!Alive)
            {
                MobTarget = null;
                SkillLoopingTarget = 0;
                TGTarget = null;
                PTarget = null;
                return;
            }
            Ready = false;
            try
            {
                if (AtkType == 25 || AtkType == 2)
                {
                    if (!WeaponSkill())
                    {
                        if (MobTarget != null)
                        {
                            if (MobTarget.Alive)
                                if (MobTarget.Map == LocMap)
                                    if (MyMath.PointDistance(LocX, LocY, MobTarget.PosX, MobTarget.PosY) < 6 || AtkType == 25)
                                    {
                                        if (!SMOn && !AccuracyOn)
                                        {
                                            if (!Other.ChanceSuccess(RealAgi / 2 + Math.Abs((110 - Level) / 2)))
                                            {
                                                MissAttack(MobTarget);
                                                return;
                                            }
                                        }
                                        else if (SMOn)
                                        {
                                            if (!Other.ChanceSuccess(RealAgi + Math.Abs((110 - Level) / 2)))
                                            {
                                                MissAttack(MobTarget);
                                                return;
                                            }
                                        }
                                        else
                                        {
                                            if (!Other.ChanceSuccess(RealAgi * 1.2 + Math.Abs((110 - Level) / 2)))
                                            {
                                                MissAttack(MobTarget);
                                                return;
                                            }
                                        }
                                        double AttackDMG = (double)General.Rand.Next((int)MinAtk, (int)MaxAtk);

                                        short ProfId = 0;
                                        double ProfBonus = 0;

                                        if (Equips[4] != null && Equips[4] != "0")
                                        {
                                            string[] Splitter2 = Equips[4].Split('-');
                                            if (Other.ItemType(uint.Parse(Splitter2[0])) == 4 || Other.ItemType(uint.Parse(Splitter2[0])) == 5 || Other.ItemType(uint.Parse(Splitter2[0])) == 9)
                                            {
                                                ProfId = (short)Other.WeaponType(uint.Parse(Splitter2[0]));
                                                if (Profs.Contains(ProfId))
                                                {
                                                    byte Prof_Lev = (byte)Profs[ProfId];
                                                    if (Prof_Lev == 13)
                                                        ProfBonus += 0.01;
                                                    else if (Prof_Lev == 14)
                                                        ProfBonus += 0.02;
                                                    else if (Prof_Lev == 15)
                                                        ProfBonus += 0.03;
                                                    else if (Prof_Lev == 16)
                                                        ProfBonus += 0.04;
                                                    else if (Prof_Lev == 17)
                                                        ProfBonus += 0.05;
                                                    else if (Prof_Lev == 18)
                                                        ProfBonus += 0.06;
                                                    else if (Prof_Lev == 19)
                                                        ProfBonus += 0.07;
                                                    else if (Prof_Lev == 20)
                                                        ProfBonus += 0.08;
                                                    else
                                                        ProfBonus = 0;
                                                }
                                            }

                                        }
                                        if (Equips[5] != null && Equips[5] != "0")
                                        {
                                            string[] Splitter2 = Equips[5].Split('-');
                                            if (Other.ItemType(uint.Parse(Splitter2[0])) == 4 || Other.ItemType(uint.Parse(Splitter2[0])) == 5 || Other.ItemType(uint.Parse(Splitter2[0])) == 9)
                                            {
                                                ProfId = (short)Other.WeaponType(uint.Parse(Splitter2[0]));
                                                if (Profs.Contains(ProfId))
                                                {
                                                    byte Prof_Lev = (byte)Profs[ProfId];
                                                    if (Prof_Lev == 13)
                                                        ProfBonus += 0.01;
                                                    else if (Prof_Lev == 14)
                                                        ProfBonus += 0.02;
                                                    else if (Prof_Lev == 15)
                                                        ProfBonus += 0.03;
                                                    else if (Prof_Lev == 16)
                                                        ProfBonus += 0.04;
                                                    else if (Prof_Lev == 17)
                                                        ProfBonus += 0.05;
                                                    else if (Prof_Lev == 18)
                                                        ProfBonus += 0.06;
                                                    else if (Prof_Lev == 19)
                                                        ProfBonus += 0.07;
                                                    else if (Prof_Lev == 20)
                                                        ProfBonus += 0.08;
                                                    else
                                                        ProfBonus = 0;
                                                }
                                            }
                                        }

                                        if (MobTarget.MType != 7)
                                        {
                                            double LDF = (Level - MobTarget.Level + 7) / 5;
                                            LDF = Math.Max(LDF, 1);
                                            double eDMG = ((Convert.ToUInt32(LDF) - 1) * .8) + 1;
                                            AttackDMG *= eDMG;
                                        }

                                        if (StigBuff)
                                        AttackDMG = (int)(AttackDMG * (double)(1 + ((double)(10 + StigLevel) / 100)));

                                        AttackDMG = ((double)AttackDMG * AddAtkPc);
                                        AttackDMG += (AttackDMG * ProfBonus);

                                        if (SMOn)
                                            AttackDMG *= 10;

                                        double ProfExpAdd = 0;

                                        double ExpQuality = 0;

                                        if (MobTarget.Level + 4 < Level)
                                            ExpQuality = 0.1;
                                        if (MobTarget.Level + 4 >= Level)
                                            ExpQuality = 1;
                                        if (MobTarget.Level >= Level)
                                            ExpQuality = 1.1;
                                        if (MobTarget.Level - 4 > Level)
                                            ExpQuality = 1.3;

                                        double EAddExp = 0;
                                        ulong UAddExp = 0;
                                        uint MobCurHP = MobTarget.CurHP;

                                        if (MobTarget.MType == 1)
                                            AttackDMG /= 100;

                                        if (MobTarget.MType == 3)
                                            AttackDMG = 0;

                                        if (MobTarget.MType == 4 && Potency < 200 || MobTarget.MType == 7 && MobTarget.Level == 200 && Potency < 200)
                                            AttackDMG /= 100;

                                        if (MobTarget.Map == 2021 || MobTarget.Map == 2022 || MobTarget.Map == 2023 || MobTarget.Map == 2024)
                                            AttackDMG /= 10;

                                        if (MobTarget.MType == 1 || MobTarget.MType == 7)
                                        {
                                            BlueName = true;
                                            GotBlueName = DateTime.Now;
                                            MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                                            World.UpdateSpawn(this);
                                        }

                                        if (MobTarget.GetDamage((uint)AttackDMG))
                                        {
                                            if (CycloneOn || SMOn)
                                            {
                                                KO++;
                                                MyClient.SendPacket(General.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Kills:" + KO, 2005));
                                            }
                                            if (MobTarget.Name == QuestMob)
                                            {
                                                QuestKO++;
                                                if (QuestKO >= 300 && QuestFrom != "NewYear")
                                                {
                                                    MyClient.SendPacket(General.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Vous avez tu� assez de monstre pour la qu�te. Retourner voir " + QuestFrom + "Capitaine.", 2005));
                                                }
                                                if (QuestFrom == "NewYear")
                                                {
                                                    MyClient.SendPacket(General.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Quest Kills:" + QuestKO + "/300", 2005));
                                                    if (QuestKO >= 300)
                                                    {
                                                        Teleport(1002, 430, 380);
                                                        QuestKO = 0;
                                                        QuestMob = "";
                                                        QuestFrom = "";
                                                        if (!InventoryContains(721636, 1))
                                                            AddItem("721636-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                                                    }
                                                }
                                            }
                                            if (LocMap == 2022)
                                            {
                                                ushort DisRequest = 0;

                                                if (Job > 9 && Job < 16)
                                                    DisRequest = 800;
                                                if (Job > 19 && Job < 26)
                                                    DisRequest = 900;
                                                if (Job > 39 && Job < 46)
                                                    DisRequest = 1300;
                                                if (Job > 129 && Job < 136)
                                                    DisRequest = 600;
                                                if (Job > 139 && Job < 146)
                                                    DisRequest = 1000;

                                                if (MobTarget.Name == "TrollInfernal")
                                                {
                                                    DisKO += 3;
                                                    MyClient.SendPacket(General.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Dis Kills:" + DisKO + "/" + DisRequest, 2005));
                                                }
                                                else
                                                {
                                                    DisKO++;
                                                    MyClient.SendPacket(General.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Dis Kills:" + DisKO + "/" + DisRequest, 2005));
                                                }

                                                if (DisKO >= DisRequest && Job > 9 && Job < 16)
                                                    MyClient.SendPacket(General.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Vous avez tu� assez de monstre pour la qu�te.", 2011));
                                                if (DisKO >= DisRequest && Job > 19 && Job < 26)
                                                    MyClient.SendPacket(General.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Vous avez tu� assez de monstre pour la qu�te.", 2011));
                                                if (DisKO >= DisRequest && Job > 39 && Job < 46)
                                                    MyClient.SendPacket(General.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Vous avez tu� assez de monstre pour la qu�te.", 2011));
                                                if (DisKO >= DisRequest && Job > 139 && Job < 146)
                                                    MyClient.SendPacket(General.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Vous avez tu� assez de monstre pour la qu�te.", 2011));
                                                if (DisKO >= DisRequest && Job > 129 && Job < 136)
                                                    MyClient.SendPacket(General.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Vous avez tu� assez de monstre pour la qu�te.", 2011));
                                            }
                                            if (Other.ChanceSuccess(8))
                                            {
                                                CPs += 3;
                                                MyClient.SendPacket(General.MyPackets.Vital(UID, 30, CPs));
                                            }
                                            if (Other.ChanceSuccess(9))
                                            {
                                                CPs += 5;
                                                MyClient.SendPacket(General.MyPackets.Vital(UID, 30, CPs));
                                            }
                                            if (Other.ChanceSuccess(10))
                                            {
                                                CPs += 10;
                                                MyClient.SendPacket(General.MyPackets.Vital(UID, 30, CPs));
                                            }
                                            if (MobTarget.Name == "PlutoFinal")
                                            {
                                                AddItem("790001-0-0-0-0-0", 0, (uint)General.Rand.Next(345636635));
                                                foreach (DictionaryEntry DE in World.AllChars)
                                                {
                                                    Character Chaar = (Character)DE.Value;
                                                    if (Chaar.Name != Name)
                                                    {
                                                        if (Chaar.LocMap == 2021 || Chaar.LocMap == 2022 || Chaar.LocMap == 2023 || Chaar.LocMap == 2024)
                                                        {
                                                            Chaar.Teleport(1002, 430, 380);
                                                        }
                                                    }
                                                    Teleport(1020, 531, 482);
                                                    MyClient.CurrentNPC = 1315;
                                                    MyClient.SendPacket(General.MyPackets.NPCSay("Vous avez vaincu PlutoFinal, pour vous remercier je vais vous donner un grand cadeau."));
                                                    MyClient.SendPacket(General.MyPackets.NPCLink("Merci!", 1));
                                                    MyClient.SendPacket(General.MyPackets.NPCSetFace(6));
                                                    MyClient.SendPacket(General.MyPackets.NPCFinish());
                                                }
                                                World.SendMsgToAll(Name + " des " + MyGuild.GuildName + " a d�truit PlutoFinal! Les joueurs ont �t� t�l�port�s pour leurs protection.", "SYSTEM", 2011);
                                            }
                                            Attacking = false;
                                            if (Level >= 70)
                                            {
                                                foreach (Character Member in Team)
                                                {
                                                    if (!Member.Alive)
                                                        continue;
                                                    if (LocMap != 1767 && Member.LocMap != 1767)
                                                    {
                                                        double PlvlExp = 1;
                                                        double WatXP = 1;
                                                        double ExpPot = 1;
                                                        double MarriageXP = 1;
                                                        if (Member.Job == 133 || Member.Job == 134 || Member.Job == 135)
                                                            WatXP = 2;
                                                        if (Member.Spouse == Spouse)
                                                            MarriageXP = 2;
                                                        if (MobTarget.Level - 20 <= Member.Level)
                                                            PlvlExp = 0.1;
                                                        if (Member.dexp == 1)
                                                            ExpPot = 2;
                                                        if (Member != null)
                                                            if (Member.Level + 20 < Level)
                                                                if (Member.LocMap == LocMap)
                                                                    if (MyMath.PointDistance(Member.LocX, Member.LocY, LocX, LocY) < 30)
                                                                        if (Member.AddExp((ulong)(((((double)(52 + (Member.Level * 30)) * PlvlExp) * WatXP) * ExpPot) * MarriageXP), true))
                                                                        {
                                                                            double MsgXp = 0;
                                                                            MsgXp = (((((double)(52 + (Member.Level * 30)) * PlvlExp) * WatXP) * ExpPot) * MarriageXP);
                                                                            Member.MyClient.SendPacket(General.MyPackets.SendMsg(Member.MyClient.MessageId, "SYSTEM", Member.Name, Member.Name + " a gagn� " + MsgXp + " points d'exp�rience.", 2005));
                                                                            VP += (uint)((Member.Level * 17 / 13 * 12 / 2) + Member.Level * 3);
                                                                            Member.MyTeamLeader.MyClient.SendPacket(General.MyPackets.SendMsg(Member.MyClient.MessageId, "SYSTEM", Member.Name, Name + " a gagn� " + ((Member.Level * 17 / 13 * 12 / 2) + Member.Level * 3) + " points de vertu.", 2003));
                                                                            foreach (Character Member2 in Team)
                                                                            {
                                                                                if (Member2 != null)
                                                                                    Member2.MyClient.SendPacket(General.MyPackets.SendMsg(Member2.MyClient.MessageId, "SYSTEM", Member2.Name, Name + " a gagn�" + ((Member.Level * 17 / 13 * 12 / 2) + Member.Level * 3) + " points de vertu.", 2003));
                                                                            }
                                                                        }
                                                    }
                                                }
                                            }
                                            XpCircle++;
                                            if (CycloneOn || SMOn)
                                                ExtraXP += 820;
                                            TargetUID = 0;
                                            EAddExp = MobCurHP * ExpQuality;
                                            UAddExp = Convert.ToUInt64(EAddExp);
                                            AddExp(UAddExp, true);
                                            ProfExpAdd = MobCurHP;
                                            MobTarget.Death = DateTime.Now;

                                            if (Equips[4] != null && Equips[4] != "0")
                                            {
                                                string[] Splitter2 = Equips[4].Split('-');
                                                if (Other.ItemType(uint.Parse(Splitter2[0])) == 4 || Other.ItemType(uint.Parse(Splitter2[0])) == 5 || Other.ItemType(uint.Parse(Splitter2[0])) == 9)
                                                    AddProfExp((short)Other.WeaponType(uint.Parse(Splitter2[0])), (uint)(ProfExpAdd * AddProfPc));
                                            }
                                            if (Equips[5] != null && Equips[5] != "0")
                                            {
                                                string[] Splitter2 = Equips[5].Split('-');
                                                if (Other.ItemType(uint.Parse(Splitter2[0])) == 4 || Other.ItemType(uint.Parse(Splitter2[0])) == 5 || Other.ItemType(uint.Parse(Splitter2[0])) == 9)
                                                    AddProfExp((short)Other.WeaponType(uint.Parse(Splitter2[0])), (uint)(ProfExpAdd * AddProfPc));
                                            }

                                            World.AttackMob(this, MobTarget, AtkType, (uint)AttackDMG);
                                            World.AttackMob(this, MobTarget, 14, 0);

                                            World.MobDissappear(MobTarget);
                                            MobTarget.Death = DateTime.Now;

                                            MobTarget = null;
                                        }
                                        else
                                        {
                                            EAddExp = AttackDMG * ExpQuality;
                                            UAddExp = Convert.ToUInt64(EAddExp);
                                            AddExp(UAddExp, true);

                                            ProfExpAdd = AttackDMG;

                                            if (Equips[4] != null && Equips[4] != "0")
                                            {
                                                string[] Splitter2 = Equips[4].Split('-');
                                                if (Other.ItemType(uint.Parse(Splitter2[0])) == 4 || Other.ItemType(uint.Parse(Splitter2[0])) == 5 || Other.ItemType(uint.Parse(Splitter2[0])) == 9)
                                                    AddProfExp((short)Other.WeaponType(uint.Parse(Splitter2[0])), (uint)(ProfExpAdd * AddProfPc));
                                            }
                                            if (Equips[5] != null && Equips[5] != "0")
                                            {
                                                string[] Splitter2 = Equips[5].Split('-');
                                                if (Other.ItemType(uint.Parse(Splitter2[0])) == 4 || Other.ItemType(uint.Parse(Splitter2[0])) == 5 || Other.ItemType(uint.Parse(Splitter2[0])) == 9)
                                                    AddProfExp((short)Other.WeaponType(uint.Parse(Splitter2[0])), (uint)(ProfExpAdd * AddProfPc));
                                            }

                                            World.AttackMob(this, MobTarget, AtkType, (uint)AttackDMG);

                                        }
                                    }
                                    else
                                    {
                                        MobTarget = null;
                                    }
                        }
                        else if (PTarget != null && ((PKMode == 2 && PTarget.GuildID != GuildID && PTarget.Team != Team && !Friends.Contains(PTarget.UID)) || PKMode == 0) && !Other.NoPK(LocMap) && PTarget.Alive)
                        {
                            if (PTarget.Alive)
                                if (LocMap == PTarget.LocMap)
                                    if (AtkType == 2 && MyMath.PointDistance(LocX, LocY, PTarget.LocX, PTarget.LocY) < 5 || AtkType == 25 && MyMath.PointDistance(LocX, LocY, PTarget.LocX, PTarget.LocY) < 16)
                                    {
                                        double Damage = General.Rand.Next((int)MinAtk, (int)MaxAtk);

                                        if (!SMOn && !AccuracyOn)
                                        {
                                            if (!Other.ChanceSuccess(RealAgi / 1.7 + Math.Abs((110 - Level) / 2)))
                                            {
                                                MissAttack(PTarget);
                                                return;
                                            }
                                        }
                                        else if (SMOn)
                                        {
                                            if (!Other.ChanceSuccess(RealAgi + Math.Abs((110 - Level) / 2)))
                                            {
                                                MissAttack(PTarget);
                                                return;
                                            }
                                        }
                                        else
                                        {
                                            if (!Other.ChanceSuccess(RealAgi * 1.2 + Math.Abs((110 - Level) / 2)))
                                            {
                                                MissAttack(PTarget);
                                                return;
                                            }
                                        }
                                        if (AtkType == 25)
                                        {
                                            byte AtkType2 = 2;
                                            double ee = 0;
                                            double reborn = 0;
                                            double ExtraDodge = 0;
                                            double BlessStats = 0;
                                            double MythiqueStats = 0;
                                            short ProfId = 0;
                                            double ProfBonus = 0;

                                            if (Equips[4] != null && Equips[4] != "0")
                                            {
                                                string[] Splitter2 = Equips[4].Split('-');
                                                if (Other.ItemType(uint.Parse(Splitter2[0])) == 4 || Other.ItemType(uint.Parse(Splitter2[0])) == 5 || Other.ItemType(uint.Parse(Splitter2[0])) == 9)
                                                {
                                                    ProfId = (short)Other.WeaponType(uint.Parse(Splitter2[0]));
                                                    if (Profs.Contains(ProfId))
                                                    {
                                                        byte Prof_Lev = (byte)Profs[ProfId];
                                                        if (Prof_Lev == 13)
                                                            ProfBonus += 0.01;
                                                        else if (Prof_Lev == 14)
                                                            ProfBonus += 0.02;
                                                        else if (Prof_Lev == 15)
                                                            ProfBonus += 0.03;
                                                        else if (Prof_Lev == 16)
                                                            ProfBonus += 0.04;
                                                        else if (Prof_Lev == 17)
                                                            ProfBonus += 0.05;
                                                        else if (Prof_Lev == 18)
                                                            ProfBonus += 0.06;
                                                        else if (Prof_Lev == 19)
                                                            ProfBonus += 0.07;
                                                        else if (Prof_Lev == 20)
                                                            ProfBonus += 0.08;
                                                        else
                                                            ProfBonus = 0;
                                                    }
                                                }

                                            }
                                            if (Equips[5] != null && Equips[5] != "0")
                                            {
                                                string[] Splitter2 = Equips[5].Split('-');
                                                if (Other.ItemType(uint.Parse(Splitter2[0])) == 4 || Other.ItemType(uint.Parse(Splitter2[0])) == 5 || Other.ItemType(uint.Parse(Splitter2[0])) == 9)
                                                {
                                                    ProfId = (short)Other.WeaponType(uint.Parse(Splitter2[0]));
                                                    if (Profs.Contains(ProfId))
                                                    {
                                                        byte Prof_Lev = (byte)Profs[ProfId];
                                                        if (Prof_Lev == 13)
                                                            ProfBonus += 0.01;
                                                        else if (Prof_Lev == 14)
                                                            ProfBonus += 0.02;
                                                        else if (Prof_Lev == 15)
                                                            ProfBonus += 0.03;
                                                        else if (Prof_Lev == 16)
                                                            ProfBonus += 0.04;
                                                        else if (Prof_Lev == 17)
                                                            ProfBonus += 0.05;
                                                        else if (Prof_Lev == 18)
                                                            ProfBonus += 0.06;
                                                        else if (Prof_Lev == 19)
                                                            ProfBonus += 0.07;
                                                        else if (Prof_Lev == 20)
                                                            ProfBonus += 0.08;
                                                        else
                                                            ProfBonus = 0;
                                                    }
                                                }
                                            }

                                            Damage = General.Rand.Next((int)MinAtk, (int)MaxAtk);

                                            Damage = (int)((double)Damage * AddAtkPc);
                                            Damage += (int)(Damage * ProfBonus);

                                            if (SMOn)
                                                Damage *= 2;
                                            if (StigBuff)
                                                Damage = (int)(Damage * (double)(1 + ((double)(10 + StigLevel) / 100)));

                                            BlessStats = PTarget.Bless / 100;
                                            MythiqueStats = PTarget.Mythique / 100;

                                            if (PTarget.RBCount == 1)
                                                reborn = 0.7;
                                            else if (PTarget.RBCount == 2)
                                                reborn = 0.5;
                                            else if (PTarget.RBCount == 255)
                                                reborn = 0.4;
                                            else if (PTarget.RBCount == 0)
                                                reborn = 1;

                                            if (PTarget.DodgeBuff)
                                                ExtraDodge = (20 + PTarget.DodgeLevel * 5);

                                            ee = (((100 - ((double)PTarget.Dodge + ExtraDodge)) / 100) * 0.12) * reborn;
                                            Damage *= ee;
                                            Damage -= (Damage * BlessStats);
                                            Damage -= (Damage * MythiqueStats);

                                            if (LuckTime > 0 && Other.ChanceSuccess(10))
                                            {
                                                Damage *= 2;
                                                foreach (DictionaryEntry DE in World.AllChars)
                                                {
                                                    Character Chaar = (Character)DE.Value;
                                                    if (Chaar.Name != Name)
                                                    {
                                                        Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "LuckyGuy"));
                                                    }
                                                }
                                                MyClient.SendPacket(General.MyPackets.String(UID, 10, "LuckyGuy"));
                                            }

                                            if (PTarget.LuckTime > 0 && Other.ChanceSuccess(10))
                                            {
                                                Damage = 1;
                                                foreach (DictionaryEntry DE in World.AllChars)
                                                {
                                                    Character Chaar = (Character)DE.Value;
                                                    if (Chaar.Name != PTarget.Name)
                                                    {
                                                        Chaar.MyClient.SendPacket(General.MyPackets.String(PTarget.UID, 10, "LuckyGuy"));
                                                    }
                                                }
                                                PTarget.MyClient.SendPacket(General.MyPackets.String(PTarget.UID, 10, "LuckyGuy"));
                                            }

                                            if (Damage < 1)
                                                Damage = 1;

                                            if (PTarget.ReflectOn == true && Other.ChanceSuccess(15))
                                            {
                                                if (Damage >= CurHP)
                                                {
                                                    int EModel;
                                                    if (Model == 1003 || Model == 1004)
                                                        EModel = 15099;
                                                    else
                                                        EModel = 15199;

                                                    Die();
                                                    CurHP = 0;
                                                    Death = DateTime.Now;
                                                    Alive = false;
                                                    XpList = false;
                                                    SMOn = false;
                                                    CycloneOn = false;
                                                    XpCircle = 0;
                                                    Flying = false;
                                                    MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                                                    MyClient.SendPacket(General.MyPackets.Status1(UID, EModel));
                                                    MyClient.SendPacket(General.MyPackets.Death(this));
                                                    World.UpdateSpawn(this);
                                                }
                                                else
                                                {
                                                    CurHP -= (ushort)Damage;
                                                    MyClient.SendPacket(General.MyPackets.Vital(UID, 0, CurHP));
                                                }

                                                foreach (DictionaryEntry DE in World.AllChars)
                                                {
                                                    Character Chaar = (Character)DE.Value;
                                                    if (Chaar.Name != PTarget.Name)
                                                    {
                                                        Chaar.MyClient.SendPacket(General.MyPackets.String(PTarget.UID, 10, "WeaponReflect"));
                                                    }

                                                    if (Chaar != null)
                                                        if (MyMath.CanSeeBig(LocX, LocY, Chaar.LocX, Chaar.LocY))
                                                            if (Chaar.MyClient.Online)
                                                            {
                                                                Chaar.MyClient.SendPacket(General.MyPackets.Attack((uint)UID, (uint)UID, (short)LocX, (short)LocY, AtkType2, (uint)Damage));
                                                            }
                                                }
                                                PTarget.MyClient.SendPacket(General.MyPackets.String(PTarget.UID, 10, "WeaponReflect"));
                                                Damage = 0;
                                            }

                                            PTarget.BlessEffect();
                                        }
                                        else
                                        {
                                            byte AtkType2 = 2;
                                            double reborn = 0;
                                            double Shield = 0;
                                            double IronShirt = 0;
                                            double BlessStats = 0;
                                            double MythiqueStats = 0;
                                            short ProfId = 0;
                                            double ProfBonus = 0;
                                            double BonusDef = 0;

                                            if (Equips[4] != null && Equips[4] != "0")
                                            {
                                                string[] Splitter2 = Equips[4].Split('-');
                                                if (Other.ItemType(uint.Parse(Splitter2[0])) == 4 || Other.ItemType(uint.Parse(Splitter2[0])) == 5 || Other.ItemType(uint.Parse(Splitter2[0])) == 9)
                                                {
                                                    ProfId = (short)Other.WeaponType(uint.Parse(Splitter2[0]));
                                                    if (Profs.Contains(ProfId))
                                                    {
                                                        byte Prof_Lev = (byte)Profs[ProfId];
                                                        if (Prof_Lev == 13)
                                                            ProfBonus += 0.01;
                                                        else if (Prof_Lev == 14)
                                                            ProfBonus += 0.02;
                                                        else if (Prof_Lev == 15)
                                                            ProfBonus += 0.03;
                                                        else if (Prof_Lev == 16)
                                                            ProfBonus += 0.04;
                                                        else if (Prof_Lev == 17)
                                                            ProfBonus += 0.05;
                                                        else if (Prof_Lev == 18)
                                                            ProfBonus += 0.06;
                                                        else if (Prof_Lev == 19)
                                                            ProfBonus += 0.07;
                                                        else if (Prof_Lev == 20)
                                                            ProfBonus += 0.08;
                                                        else
                                                            ProfBonus = 0;
                                                    }
                                                }

                                            }
                                            if (Equips[5] != null && Equips[5] != "0")
                                            {
                                                string[] Splitter2 = Equips[5].Split('-');
                                                if (Other.ItemType(uint.Parse(Splitter2[0])) == 4 || Other.ItemType(uint.Parse(Splitter2[0])) == 5 || Other.ItemType(uint.Parse(Splitter2[0])) == 9)
                                                {
                                                    ProfId = (short)Other.WeaponType(uint.Parse(Splitter2[0]));
                                                    if (Profs.Contains(ProfId))
                                                    {
                                                        byte Prof_Lev = (byte)Profs[ProfId];
                                                        if (Prof_Lev == 13)
                                                            ProfBonus += 0.01;
                                                        else if (Prof_Lev == 14)
                                                            ProfBonus += 0.02;
                                                        else if (Prof_Lev == 15)
                                                            ProfBonus += 0.03;
                                                        else if (Prof_Lev == 16)
                                                            ProfBonus += 0.04;
                                                        else if (Prof_Lev == 17)
                                                            ProfBonus += 0.05;
                                                        else if (Prof_Lev == 18)
                                                            ProfBonus += 0.06;
                                                        else if (Prof_Lev == 19)
                                                            ProfBonus += 0.07;
                                                        else if (Prof_Lev == 20)
                                                            ProfBonus += 0.08;
                                                        else
                                                            ProfBonus = 0;
                                                    }
                                                }
                                            }

                                            Damage = (int)((double)Damage * AddAtkPc);
                                            Damage += (int)(Damage * ProfBonus);

                                            if (StigBuff)
                                                Damage = (int)(Damage * (double)(1 + ((double)(10 + StigLevel) / 100)));

                                            if (PTarget.MShieldBuff)
                                                BonusDef = (PTarget.Defense * (10 + PTarget.MShieldLevel * 5) / 100);

                                            if (SMOn)
                                                Damage *= 2;

                                            BlessStats = PTarget.Bless / 100;
                                            MythiqueStats = PTarget.Mythique / 100;

                                            if (PTarget.RBCount == 1)
                                                reborn = 0.7;
                                            else if (PTarget.RBCount == 2)
                                                reborn = 0.5;
                                            else if (PTarget.RBCount == 255)
                                                reborn = 0.4;
                                            else if (PTarget.RBCount == 0)
                                                reborn = 1;

                                            if (PTarget.ShieldBuff == true)
                                                Shield = 3;
                                            else
                                                Shield = 1;

                                            if (PTarget.IronBuff == true)
                                                IronShirt = 4;
                                            else
                                                IronShirt = 1;

                                            Damage = (int)((Damage - (PTarget.Defense * Shield * IronShirt + BonusDef)) * reborn);
                                            Damage -= (Damage * BlessStats);
                                            Damage -= (Damage * MythiqueStats);


                                            if (LuckTime > 0 && Other.ChanceSuccess(10))
                                            {
                                                Damage *= 2;
                                                foreach (DictionaryEntry DE in World.AllChars)
                                                {
                                                    Character Chaar = (Character)DE.Value;
                                                    if (Chaar.Name != Name)
                                                    {
                                                        Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "LuckyGuy"));
                                                    }
                                                }
                                                MyClient.SendPacket(General.MyPackets.String(UID, 10, "LuckyGuy"));
                                            }

                                            if (PTarget.LuckTime > 0 && Other.ChanceSuccess(10))
                                            {
                                                Damage = 1;
                                                foreach (DictionaryEntry DE in World.AllChars)
                                                {
                                                    Character Chaar = (Character)DE.Value;
                                                    if (Chaar.Name != PTarget.Name)
                                                    {
                                                        Chaar.MyClient.SendPacket(General.MyPackets.String(PTarget.UID, 10, "LuckyGuy"));
                                                    }
                                                }
                                                PTarget.MyClient.SendPacket(General.MyPackets.String(PTarget.UID, 10, "LuckyGuy"));
                                            }

                                            if (Damage < 1)
                                                Damage = 1;

                                            if (PTarget.ReflectOn == true && Other.ChanceSuccess(15))
                                            {
                                                if (Damage >= CurHP)
                                                {
                                                    int EModel;
                                                    if (Model == 1003 || Model == 1004)
                                                        EModel = 15099;
                                                    else
                                                        EModel = 15199;

                                                    Die();
                                                    CurHP = 0;
                                                    Death = DateTime.Now;
                                                    Alive = false;
                                                    XpList = false;
                                                    SMOn = false;
                                                    CycloneOn = false;
                                                    XpCircle = 0;
                                                    Flying = false;
                                                    MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                                                    MyClient.SendPacket(General.MyPackets.Status1(UID, EModel));
                                                    MyClient.SendPacket(General.MyPackets.Death(this));
                                                    World.UpdateSpawn(this);
                                                }
                                                else
                                                {
                                                    CurHP -= (ushort)Damage;
                                                    MyClient.SendPacket(General.MyPackets.Vital(UID, 0, CurHP));
                                                }

                                                foreach (DictionaryEntry DE in World.AllChars)
                                                {
                                                    Character Chaar = (Character)DE.Value;
                                                    if (Chaar.Name != PTarget.Name)
                                                    {
                                                        Chaar.MyClient.SendPacket(General.MyPackets.String(PTarget.UID, 10, "WeaponReflect"));
                                                    }

                                                    if (Chaar != null)
                                                        if (MyMath.CanSeeBig(LocX, LocY, Chaar.LocX, Chaar.LocY))
                                                            if (Chaar.MyClient.Online)
                                                            {
                                                                Chaar.MyClient.SendPacket(General.MyPackets.Attack((uint)UID, (uint)UID, (short)LocX, (short)LocY, AtkType2, (uint)Damage));
                                                            }
                                                }
                                                PTarget.MyClient.SendPacket(General.MyPackets.String(PTarget.UID, 10, "WeaponReflect"));
                                                Damage = 0;
                                            }

                                            PTarget.BlessEffect();
                                        }

                                        if (PTarget.PKPoints < 100)
                                            if (!PTarget.BlueName)
                                                if (!Other.CanPK(LocMap))
                                                {
                                                    GotBlueName = DateTime.Now;
                                                    BlueName = true;
                                                    MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                                                    World.UpdateSpawn(this);
                                                }

                                        World.PVP(this, PTarget, AtkType, (uint)Damage);

                                        if (PTarget.GetHitDie((uint)Damage))
                                        {
                                            Attacking = false;
                                            if (!Other.CanPK(LocMap))
                                                if (!PTarget.BlueName)
                                                    if (Other.CanPK(LocMap))
                                                    {
                                                        if (PTarget.PKPoints == 0 || PTarget.PKPoints > 0)
                                                        {
                                                            GotBlueName = DateTime.Now;
                                                            BlueName = true;
                                                            PKPoints += 0;
                                                            MyClient.SendPacket(General.MyPackets.Vital(UID, 6, PKPoints));

                                                        }
                                                    }
                                                    else if (!Other.CanPK(LocMap) && PTarget.PKPoints < 30)
                                                    {
                                                        GotBlueName = DateTime.Now;
                                                        BlueName = true;
                                                        PKPoints += 10;

                                                        ulong PkXp = PTarget.Exp / 100;
                                                        if (PkXp < 0)
                                                            PkXp = PTarget.Exp;
                                                        PTarget.Exp -= PkXp;
                                                        AddExp(PkXp, false);
                                                        MyClient.SendPacket(General.MyPackets.Vital((long)UID, 5, Exp));
                                                        PTarget.MyClient.SendPacket(General.MyPackets.Vital((long)PTarget.UID, 5, PTarget.Exp));

                                                        MyClient.SendPacket(General.MyPackets.Vital(UID, 6, PKPoints));
                                                        if ((PKPoints > 29 && PKPoints - 10 < 30) || (PKPoints > 99 && PKPoints - 10 < 100))
                                                        {
                                                            MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                                                            World.UpdateSpawn(this);
                                                        }
                                                    }
                                            World.PVP(this, PTarget, 14, 0);
                                            PTarget.PTarget = null;
                                            PTarget.MobTarget = null;
                                            PTarget = null;
                                        }
                                    }
                                    else
                                        PTarget = null;
                        }
                        else if (TGTarget != null)
                            if (Level >= TGTarget.Level)
                            {
                                {
                                    if (!SMOn && !AccuracyOn)
                                    {
                                        if (!Other.ChanceSuccess(RealAgi / 1.7 + Math.Abs((110 - Level) / 2)))
                                        {
                                            MissAttack(TGTarget);
                                            return;
                                        }
                                    }
                                    else if (SMOn)
                                    {
                                        if (!Other.ChanceSuccess(RealAgi + Math.Abs((110 - Level) / 2)))
                                        {
                                            MissAttack(TGTarget);
                                            return;
                                        }
                                    }
                                    else
                                    {
                                        if (!Other.ChanceSuccess(RealAgi * 1.2 + Math.Abs((110 - Level) / 2)))
                                        {
                                            MissAttack(TGTarget);
                                            return;
                                        }
                                    }
                                    short ProfId = 0;
                                    double ProfBonus = 0;

                                    if (Equips[4] != null && Equips[4] != "0")
                                    {
                                        string[] Splitter2 = Equips[4].Split('-');
                                        if (Other.ItemType(uint.Parse(Splitter2[0])) == 4 || Other.ItemType(uint.Parse(Splitter2[0])) == 5 || Other.ItemType(uint.Parse(Splitter2[0])) == 9)
                                        {
                                            ProfId = (short)Other.WeaponType(uint.Parse(Splitter2[0]));
                                            if (Profs.Contains(ProfId))
                                            {
                                                byte Prof_Lev = (byte)Profs[ProfId];
                                                if (Prof_Lev == 13)
                                                    ProfBonus += 0.01;
                                                else if (Prof_Lev == 14)
                                                    ProfBonus += 0.02;
                                                else if (Prof_Lev == 15)
                                                    ProfBonus += 0.03;
                                                else if (Prof_Lev == 16)
                                                    ProfBonus += 0.04;
                                                else if (Prof_Lev == 17)
                                                    ProfBonus += 0.05;
                                                else if (Prof_Lev == 18)
                                                    ProfBonus += 0.06;
                                                else if (Prof_Lev == 19)
                                                    ProfBonus += 0.07;
                                                else if (Prof_Lev == 20)
                                                    ProfBonus += 0.08;
                                                else
                                                    ProfBonus = 0;
                                            }
                                        }

                                    }
                                    if (Equips[5] != null && Equips[5] != "0")
                                    {
                                        string[] Splitter2 = Equips[5].Split('-');
                                        if (Other.ItemType(uint.Parse(Splitter2[0])) == 4 || Other.ItemType(uint.Parse(Splitter2[0])) == 5 || Other.ItemType(uint.Parse(Splitter2[0])) == 9)
                                        {
                                            ProfId = (short)Other.WeaponType(uint.Parse(Splitter2[0]));
                                            if (Profs.Contains(ProfId))
                                            {
                                                byte Prof_Lev = (byte)Profs[ProfId];
                                                if (Prof_Lev == 13)
                                                    ProfBonus += 0.01;
                                                else if (Prof_Lev == 14)
                                                    ProfBonus += 0.02;
                                                else if (Prof_Lev == 15)
                                                    ProfBonus += 0.03;
                                                else if (Prof_Lev == 16)
                                                    ProfBonus += 0.04;
                                                else if (Prof_Lev == 17)
                                                    ProfBonus += 0.05;
                                                else if (Prof_Lev == 18)
                                                    ProfBonus += 0.06;
                                                else if (Prof_Lev == 19)
                                                    ProfBonus += 0.07;
                                                else if (Prof_Lev == 20)
                                                    ProfBonus += 0.08;
                                                else
                                                    ProfBonus = 0;
                                            }
                                        }
                                    }

                                    uint Damage = (uint)General.Rand.Next((int)MinAtk, (int)MaxAtk);
                                    uint THP = TGTarget.CurHP;
                                    Damage = (uint)((double)Damage * AddAtkPc);
                                    Damage += (uint)(Damage * ProfBonus);
                                    if (StigBuff)
                                        Damage = (uint)(Damage * (double)(1 + ((double)(10 + StigLevel) / 100)));

                                    if (LuckTime > 0 && Other.ChanceSuccess(10))
                                    {
                                        Damage *= 2;
                                        foreach (DictionaryEntry DE in World.AllChars)
                                        {
                                            Character Chaar = (Character)DE.Value;
                                            if (Chaar.Name != Name)
                                            {
                                                Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "LuckyGuy"));
                                            }
                                        }
                                        MyClient.SendPacket(General.MyPackets.String(UID, 10, "LuckyGuy"));
                                    }

                                    if (GuildID != 0 && GuildID != null)
                                    {
                                        if (TGTarget.Sob == 2 && MyGuild.HoldingPole == false)
                                        {
                                            Silvers += (uint)(Damage / 100);
                                            MyGuild.Fund += (uint)(Damage / 100);
                                            GuildDonation += (uint)(Damage / 100);
                                            MyGuild.Refresh(this);
                                            MyClient.SendPacket(General.MyPackets.GuildInfo(MyGuild, this));
                                            MyClient.SendPacket(General.MyPackets.Vital(UID, 4, Silvers));
                                        }
                                    }

                                    double ExpQuality = 1;

                                    if (TGTarget.Level + 5 < Level)
                                        ExpQuality = 0.1;

                                    if (TGTarget.GetDamageDie((uint)Damage, this))
                                    {
                                        AddExp((ulong)(THP / 10 * ExpQuality), true);
                                        uint ProfExpAdd = THP / 10;
                                        if (Equips[4] != null && Equips[4] != "0")
                                        {
                                            string[] Splitter2 = Equips[4].Split('-');
                                            if (Other.ItemType(uint.Parse(Splitter2[0])) == 4 || Other.ItemType(uint.Parse(Splitter2[0])) == 5 || Other.ItemType(uint.Parse(Splitter2[0])) == 9)
                                                AddProfExp((short)Other.WeaponType(uint.Parse(Splitter2[0])), (uint)(ProfExpAdd * AddProfPc));
                                        }
                                        if (Equips[5] != null && Equips[5] != "0")
                                        {
                                            string[] Splitter2 = Equips[5].Split('-');
                                            if (Other.ItemType(uint.Parse(Splitter2[0])) == 4 || Other.ItemType(uint.Parse(Splitter2[0])) == 5 || Other.ItemType(uint.Parse(Splitter2[0])) == 9)
                                                AddProfExp((short)Other.WeaponType(uint.Parse(Splitter2[0])), (uint)(ProfExpAdd * AddProfPc));
                                        };

                                        World.PlAttacksTG(TGTarget, this, (byte)AtkType, (uint)Damage);
                                        World.PlAttacksTG(TGTarget, this, 14, (uint)Damage);

                                    }
                                    else
                                    {
                                        AddExp((ulong)(Damage / 10 * ExpQuality), true);
                                        uint ProfExpAdd = Damage / 10;

                                        if (Equips[4] != null && Equips[4] != "0")
                                        {
                                            string[] Splitter2 = Equips[4].Split('-');
                                            if (Other.ItemType(uint.Parse(Splitter2[0])) == 4 || Other.ItemType(uint.Parse(Splitter2[0])) == 5 || Other.ItemType(uint.Parse(Splitter2[0])) == 9)
                                                AddProfExp((short)Other.WeaponType(uint.Parse(Splitter2[0])), (uint)(ProfExpAdd * AddProfPc));
                                        }
                                        if (Equips[5] != null && Equips[5] != "0")
                                        {
                                            string[] Splitter2 = Equips[5].Split('-');
                                            if (Other.ItemType(uint.Parse(Splitter2[0])) == 4 || Other.ItemType(uint.Parse(Splitter2[0])) == 5 || Other.ItemType(uint.Parse(Splitter2[0])) == 9)
                                                AddProfExp((short)Other.WeaponType(uint.Parse(Splitter2[0])), (uint)(ProfExpAdd * AddProfPc));
                                        };

                                        World.PlAttacksTG(TGTarget, this, (byte)AtkType, (uint)Damage);


                                    }
                                }
                            }
                            else
                                MyClient.SendPacket(General.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Vous ne pouvez pas attaquer car vous n'avez pas le bon niveau. Allez � la place de votre niveau.", 2000));

                        Ready = true;
                    }
                }
                else if (AtkType == 21)
                {
                    if (SkillLooping != 0)
                    {
                        if (SkillLoopingTarget != 0)
                        {
                            if (SkillLoopingTarget > 400000 && SkillLoopingTarget < 500000)
                            {
                                SingleMob Targ = (SingleMob)Mobs.AllMobs[SkillLoopingTarget];
                                if (Targ == null || !Targ.Alive)
                                    return;
                                else
                                    UseSkill(SkillLooping, (ushort)Targ.PosX, (ushort)Targ.PosY, SkillLoopingTarget);

                            }
                            else if (SkillLoopingTarget < 7000 && SkillLoopingTarget >= 5000)
                            {
                                SingleNPC Targ = (SingleNPC)NPCs.AllNPCs[SkillLoopingTarget];
                                if (Targ == null)
                                    return;
                                else
                                    UseSkill(SkillLooping, (ushort)Targ.X, (ushort)Targ.Y, SkillLoopingTarget);

                            }
                            else
                            {
                                Character Targ = (Character)World.AllChars[SkillLoopingTarget];
                                if (Targ == null || !Targ.Alive)
                                    return;
                                else
                                    UseSkill(SkillLooping, Targ.LocX, Targ.LocY, SkillLoopingTarget);
                            }
                        }
                        else
                        {
                            UseSkill(SkillLooping, SkillLoopingX, SkillLoopingY, 0);
                        }
                    }
                    else
                    {
                        SkillLooping = 0;
                    }
                    GemEffect();
                }
                Ready = true;

            }
            catch (Exception Exc) { General.WriteLine(Convert.ToString(Exc)); }

            if (CycloneOn == false && SMOn == false)
            {
                if (Equips[4] != null && Equips[4] != "0")
                {
                    string[] Splitter = Equips[4].Split('-');
                    byte ItemBless = byte.Parse(Splitter[2]);

                    if (ItemBless > 9 && ItemBless < 18)
                    {
                        if (Other.ChanceSuccess(10))
                        {
                            if (CurHP > (MaxHP - 300))
                            {
                                CurHP += (ushort)(MaxHP - CurHP);
                            }
                            else
                                CurHP += 300;

                            foreach (DictionaryEntry DE in World.AllChars)
                            {
                                Character Charr = (Character)DE.Value;
                                if (Charr.Name != Name)
                                    Charr.MyClient.SendPacket(General.MyPackets.CharSkillUse(this, this, 300, 10201, 0));
                            }
                            MyClient.SendPacket(General.MyPackets.CharSkillUse(this, this, 300, 10201, 0));
                            MyClient.SendPacket(General.MyPackets.Vital((long)UID, 0, CurHP));
                        }
                    }
                    if (ItemBless > 19 && ItemBless < 28)
                    {
                        if (Other.ChanceSuccess(10))
                        {
                            short Heal = 0;
                            if (CurMP > (MaxMP - 310))
                            {
                                Heal = (short)(MaxMP - CurMP);
                                CurMP += (ushort)Heal;
                            }
                            else
                            {
                                Heal = 310;
                                CurMP += 310;
                            }

                            foreach (DictionaryEntry DE in World.AllChars)
                            {
                                Character Charr = (Character)DE.Value;
                                if (Charr.Name != Name)
                                    Charr.MyClient.SendPacket(General.MyPackets.CharSkillUse(this, this, (uint)Heal, 10202, 0));
                            }
                            MyClient.SendPacket(General.MyPackets.CharSkillUse(this, this, (uint)Heal, 10202, 0));
                            MyClient.SendPacket(General.MyPackets.Vital((long)UID, 2, CurMP));
                        }
                    }
                    if (ItemBless > 29 && ItemBless < 38)
                    {
                        if (!Other.NoPK(LocMap))
                        {
                            if (Other.ChanceSuccess(10))
                            {
                                Character Target = (Character)World.AllChars[PTarget.UID];

                                if (Target.UID != UID)
                                {
                                    Target.BurnBuff = true;
                                    Target.Burned = DateTime.Now;
                                    Target.MyClient.SendPacket(General.MyPackets.Vital(Target.UID, 26, Target.GetStat()));
                                    World.UpdateSpawn(Target);
                                }
                            }
                        }
                    }
                    if (ItemBless > 39 && ItemBless < 48)
                    {
                        if (!Other.NoPK(LocMap))
                        {
                            if (Other.ChanceSuccess(10))
                            {
                                Character Target = (Character)World.AllChars[PTarget.UID];

                                if (Target.UID != UID)
                                {
                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character Charr = (Character)DE.Value;
                                        if (Charr.Name != Name)
                                            Charr.MyClient.SendPacket(General.MyPackets.String(Target.UID, 10, "nomove"));
                                    }
                                    MyClient.SendPacket(General.MyPackets.String(Target.UID, 10, "nomove"));
                                    Target.PoisonBuff = true;
                                    Target.Poisoned = DateTime.Now;
                                    Target.MyClient.SendPacket(General.MyPackets.Vital(Target.UID, 26, Target.GetStat()));
                                    World.UpdateSpawn(Target);
                                }
                            }
                        }
                    }
                }
                if (Equips[5] != null && Equips[5] != "0")
                {
                    string[] Splitter = Equips[5].Split('-');
                    byte ItemBless = byte.Parse(Splitter[2]);

                    if (ItemBless > 9 && ItemBless < 18)
                    {
                        if (Other.ChanceSuccess(10))
                        {
                            if (CurHP > (MaxHP - 300))
                            {
                                CurHP += (ushort)(MaxHP - CurHP);
                            }
                            else
                                CurHP += 300;

                            foreach (DictionaryEntry DE in World.AllChars)
                            {
                                Character Charr = (Character)DE.Value;
                                if (Charr.Name != Name)
                                    Charr.MyClient.SendPacket(General.MyPackets.CharSkillUse(this, this, 300, 10201, 0));
                            }
                            MyClient.SendPacket(General.MyPackets.CharSkillUse(this, this, 300, 10201, 0));
                            MyClient.SendPacket(General.MyPackets.Vital((long)UID, 0, CurHP));
                        }
                    }
                    if (ItemBless > 19 && ItemBless < 28)
                    {
                        if (Other.ChanceSuccess(10))
                        {
                            short Heal = 0;
                            if (CurMP > (MaxMP - 310))
                            {
                                Heal = (short)(MaxMP - CurMP);
                                CurMP += (ushort)Heal;
                            }
                            else
                            {
                                Heal = 310;
                                CurMP += 310;
                            }

                            foreach (DictionaryEntry DE in World.AllChars)
                            {
                                Character Charr = (Character)DE.Value;
                                if (Charr.Name != Name)
                                    Charr.MyClient.SendPacket(General.MyPackets.CharSkillUse(this, this, (uint)Heal, 10202, 0));
                            }
                            MyClient.SendPacket(General.MyPackets.CharSkillUse(this, this, (uint)Heal, 10202, 0));
                            MyClient.SendPacket(General.MyPackets.Vital((long)UID, 2, CurMP));
                        }
                    }
                    if (ItemBless > 29 && ItemBless < 38)
                    {
                        if (!Other.NoPK(LocMap))
                        {
                            if (Other.ChanceSuccess(10))
                            {
                                Character Target = (Character)World.AllChars[PTarget.UID];

                                if (Target.UID != UID)
                                {
                                    Target.BurnBuff = true;
                                    Target.Burned = DateTime.Now;
                                    Target.MyClient.SendPacket(General.MyPackets.Vital(Target.UID, 26, Target.GetStat()));
                                    World.UpdateSpawn(Target);
                                }
                            }
                        }
                    }
                    if (ItemBless > 39 && ItemBless < 48)
                    {
                        if (!Other.NoPK(LocMap))
                        {
                            if (Other.ChanceSuccess(10))
                            {
                                Character Target = (Character)World.AllChars[PTarget.UID];

                                if (Target.UID != UID)
                                {
                                    foreach (DictionaryEntry DE in World.AllChars)
                                    {
                                        Character Charr = (Character)DE.Value;
                                        if (Charr.Name != Name)
                                            Charr.MyClient.SendPacket(General.MyPackets.String(Target.UID, 10, "nomove"));
                                    }
                                    MyClient.SendPacket(General.MyPackets.String(Target.UID, 10, "nomove"));
                                    Target.PoisonBuff = true;
                                    Target.Poisoned = DateTime.Now;
                                    Target.MyClient.SendPacket(General.MyPackets.Vital(Target.UID, 26, Target.GetStat()));
                                    World.UpdateSpawn(Target);
                                }
                            }
                        }
                    }
                }
            }
        }

        public void AddGemAtk()
        {
            Ready = false;
            MAtk *= AddMAtkPc;
            Ready = true;
        }
        public void RemoveGemAtk()
        {
            Ready = false;
            MAtk /= AddMAtkPc;
            Ready = true;
        }

        public void GetEquipStats(byte Pos, bool UnEquip)
        {
            Ready = false;
            if (Equips[Pos] != "0" && Equips[Pos] != null)
            {
                string[] Splitter = Equips[Pos].Split('-');

                byte ItemPlus = byte.Parse(Splitter[1]);
                byte ItemBless = byte.Parse(Splitter[2]);
                byte ItemEnchant = byte.Parse(Splitter[3]);
                byte ItemGem1 = byte.Parse(Splitter[4]);
                byte ItemGem2 = byte.Parse(Splitter[5]);

                uint[] TheItem = Other.ItemInfo(uint.Parse(Splitter[0]));

                string PItemID = Splitter[0];
                uint ItemId = uint.Parse(Splitter[0]);

                if (Pos == 1 || Pos == 3)
                {
                    PItemID = PItemID.Remove(3, 1);
                    PItemID = PItemID.Insert(3, "0");
                    PItemID = PItemID.Remove(5, 1);
                    PItemID = PItemID.Insert(5, "0");
                }
                if (Pos == 2 || Pos == 6 || Pos == 8)
                {
                    PItemID = PItemID.Remove(5, 1);
                    PItemID = PItemID.Insert(5, "0");
                }
                if (Pos == 4 || Pos == 5)
                {
                    if (Other.ItemType(ItemId) == 4 && Other.WeaponType(ItemId) != 421)
                    {
                        PItemID = PItemID.Remove(5, 1);
                        PItemID = PItemID.Remove(0, 3);
                        PItemID = "444" + PItemID + "0";
                    }
                    if (Other.ItemType(ItemId) == 5 && Other.WeaponType(ItemId) != 500)
                    {
                        PItemID = PItemID.Remove(5, 1);
                        PItemID = PItemID.Remove(0, 3);
                        PItemID = "555" + PItemID + "0";
                    }
                    if (Other.WeaponType(ItemId) == 500 || Other.WeaponType(ItemId) == 421)
                    {
                        PItemID = PItemID.Remove(5, 1);
                        PItemID = PItemID.Insert(5, "0");
                    }
                    if (Other.WeaponType(ItemId) == 900)
                    {
                        PItemID = PItemID.Remove(3, 1);
                        PItemID = PItemID.Insert(3, "0");
                        PItemID = PItemID.Remove(5, 1);
                        PItemID = PItemID.Insert(5, "0");
                    }
                }


                string[] PItem = new string[10] { "0", "0", "0", "0", "0", "0", "0", "0", "0", "0" };

                foreach (string[] Item in DataBase.DBPlusInfo)
                {
                    if (PItemID == Item[0])
                        if (ItemPlus == byte.Parse(Item[1]))
                        {
                            PItem = Item;
                            break;
                        }
                }
                ushort ExtraAtk = 0;

                ushort AddDef = (ushort)(TheItem[10] + ushort.Parse(PItem[5]));
                ushort AddMDef = (ushort)(TheItem[11]);//magic defence        
                ushort AddMBlock = (ushort)(ushort.Parse(PItem[7]));//magic block  
                ushort AddMinAtk = 0;
                ushort AddMaxAtk = 0;
                if (Pos != 5)
                {
                    AddMinAtk = (ushort)(TheItem[8] + uint.Parse(PItem[3]) + ExtraAtk);
                    AddMaxAtk = (ushort)(TheItem[9] + uint.Parse(PItem[4]) + ExtraAtk);
                }
                else
                {
                    AddMinAtk = (ushort)((TheItem[8] + uint.Parse(PItem[3]) + ExtraAtk) / 2);
                    AddMaxAtk = (ushort)((TheItem[9] + uint.Parse(PItem[4]) + ExtraAtk) / 2);
                }
                ushort AddMAtk = (ushort)(TheItem[12] + ushort.Parse(PItem[6]));
                ushort AddHP = (ushort)(ItemEnchant + ushort.Parse(PItem[2]));
                ushort AddDex = (ushort)(TheItem[14] + ushort.Parse(PItem[8]));
                byte AddDodge = (byte)(TheItem[13] + byte.Parse(PItem[9]));

                ushort SockPot = 0;
                ushort QualityPot = 0;

                if (Other.ItemQuality(ItemId) == 9)
                    QualityPot = 4;
                if (Other.ItemQuality(ItemId) == 8)
                    QualityPot = 3;
                if (Other.ItemQuality(ItemId) == 7)
                    QualityPot = 2;
                if (Other.ItemQuality(ItemId) == 6)
                    QualityPot = 1;

                if (ItemGem1 != 0)
                    SockPot++;
                if (Other.ItemQuality((uint)ItemGem1) == 3)
                    SockPot++;
                if (ItemGem2 != 0)
                    SockPot++;
                if (Other.ItemQuality((uint)ItemGem1) == 3)
                    SockPot++;

                ushort AddPotency = (ushort)(SockPot + ItemPlus + QualityPot);

                if (!UnEquip)
                {

                    MaxHP = BaseMaxHP();
                    MaxMP = MaxMana();
                    Potency += AddPotency;
                    Defense += AddDef;
                    MDefense += AddMDef;
                    MagicBlock += AddMBlock;
                    MinAtk += AddMinAtk;
                    MaxAtk += AddMaxAtk;
                    MAtk += AddMAtk;
                    ExtraHP += AddHP;
                    RealAgi += AddDex;
                    Dodge += AddDodge;
                    Mythique += AddMythique;

                    if (ItemBless > 9 && ItemBless < 18)
                    {
                        byte RealBless = 0;
                        RealBless = (byte)(ItemBless - 10);
                        Bless += RealBless;
                    }
                    else if (ItemBless > 19 && ItemBless < 28)
                    {
                        byte RealBless = 0;
                        RealBless = (byte)(ItemBless - 20);
                        Bless += RealBless;
                    }
                    else if (ItemBless > 29 && ItemBless < 38)
                    {
                        byte RealBless = 0;
                        RealBless = (byte)(ItemBless - 30);
                        Bless += RealBless;
                    }
                    else if (ItemBless > 39 && ItemBless < 48)
                    {
                        byte RealBless = 0;
                        RealBless = (byte)(ItemBless - 40);
                        Bless += RealBless;
                    }
                    else
                        Bless += ItemBless;

                    //PhoenixGem
                    if (ItemGem1 == 1)
                        AddMAtkPc += 0.05;
                    if (ItemGem2 == 1)
                        AddMAtkPc += 0.05;
                    if (ItemGem1 == 2)
                        AddMAtkPc += 0.1;
                    if (ItemGem2 == 2)
                        AddMAtkPc += 0.1;
                    if (ItemGem1 == 3)
                        AddMAtkPc += 0.15;
                    if (ItemGem2 == 3)
                        AddMAtkPc += 0.15;

                    //DragonGem
                    if (ItemGem1 == 11)
                        AddAtkPc += 0.05;
                    if (ItemGem2 == 11)
                        AddAtkPc += 0.05;
                    if (ItemGem1 == 12)
                        AddAtkPc += 0.1;
                    if (ItemGem2 == 12)
                        AddAtkPc += 0.1;
                    if (ItemGem1 == 13)
                        AddAtkPc += 0.15;
                    if (ItemGem2 == 13)
                        AddAtkPc += 0.15;

                    //FuryGem
                    if (ItemGem1 == 21)
                        AddExpPc += 0.1;
                    if (ItemGem2 == 21)
                        AddExpPc += 0.1;
                    if (ItemGem1 == 22)
                        AddExpPc += 0.15;
                    if (ItemGem2 == 22)
                        AddExpPc += 0.15;
                    if (ItemGem1 == 23)
                        AddExpPc += 0.25;
                    if (ItemGem2 == 23)
                        AddExpPc += 0.25;

                    //RainbowGem
                    if (ItemGem1 == 31)
                        AddExpPc += 0.1;
                    if (ItemGem2 == 31)
                        AddExpPc += 0.1;
                    if (ItemGem1 == 32)
                        AddExpPc += 0.15;
                    if (ItemGem2 == 32)
                        AddExpPc += 0.15;
                    if (ItemGem1 == 33)
                        AddExpPc += 0.25;
                    if (ItemGem2 == 33)
                        AddExpPc += 0.25;

                    //VioletGem
                    if (ItemGem1 == 51)
                        AddProfPc += 0.3;
                    if (ItemGem2 == 51)
                        AddProfPc += 0.3;
                    if (ItemGem1 == 52)
                        AddProfPc += 0.5;
                    if (ItemGem2 == 52)
                        AddProfPc += 0.5;
                    if (ItemGem1 == 53)
                        AddProfPc += 1;
                    if (ItemGem2 == 53)
                        AddProfPc += 1;

                    //MoonGem
                    if (ItemGem1 == 61)
                        AddSpellPc += 1.15;
                    if (ItemGem2 == 61)
                        AddSpellPc += 1.15;
                    if (ItemGem1 == 62)
                        AddSpellPc += 1.30;
                    if (ItemGem2 == 62)
                        AddSpellPc += 1.30;
                    if (ItemGem1 == 63)
                        AddSpellPc += 1.50;
                    if (ItemGem2 == 63)
                        AddSpellPc += 1.50;

                    //TortoiseGem
                    /*if (ItemGem1 == 71)
                        AddMythique += 2;
                    if (ItemGem2 == 71)
                        AddMythique += 2;
                    if (ItemGem1 == 72)
                        AddMythique += 4;
                    if (ItemGem2 == 72)
                        AddMythique += 4;
                    if (ItemGem1 == 73)
                        AddMythique += 6;
                    if (ItemGem2 == 73)
                        AddMythique += 6;*/
                }
                else
                {
                    MaxHP = BaseMaxHP();
                    MaxMP = MaxMana();
                    Potency -= AddPotency;
                    Defense -= AddDef;
                    MDefense -= AddMDef;
                    MagicBlock -= AddMBlock;
                    MinAtk -= AddMinAtk;
                    MaxAtk -= AddMaxAtk;
                    MAtk -= AddMAtk;
                    ExtraHP -= AddHP;
                    RealAgi -= AddDex;
                    Dodge -= AddDodge;
                    Mythique -= AddMythique;

                    if (ItemBless > 9 && ItemBless < 18)
                    {
                        byte RealBless = 0;
                        RealBless = (byte)(ItemBless - 10);
                        Bless -= RealBless;
                    }
                    else if (ItemBless > 19 && ItemBless < 28)
                    {
                        byte RealBless = 0;
                        RealBless = (byte)(ItemBless - 20);
                        Bless -= RealBless;
                    }
                    else if (ItemBless > 29 && ItemBless < 38)
                    {
                        byte RealBless = 0;
                        RealBless = (byte)(ItemBless - 30);
                        Bless -= RealBless;
                    }
                    else if (ItemBless > 39 && ItemBless < 48)
                    {
                        byte RealBless = 0;
                        RealBless = (byte)(ItemBless - 40);
                        Bless -= RealBless;
                    }
                    else
                        Bless -= ItemBless;

                    //PhoenixGem
                    if (ItemGem1 == 1)
                        AddMAtkPc -= 0.05;
                    if (ItemGem2 == 1)
                        AddMAtkPc -= 0.05;
                    if (ItemGem1 == 2)
                        AddMAtkPc -= 0.1;
                    if (ItemGem2 == 2)
                        AddMAtkPc -= 0.1;
                    if (ItemGem1 == 3)
                        AddMAtkPc -= 0.15;
                    if (ItemGem2 == 3)
                        AddMAtkPc -= 0.15;

                    //DragonGem
                    if (ItemGem1 == 11)
                        AddAtkPc -= 0.05;
                    if (ItemGem2 == 11)
                        AddAtkPc -= 0.05;
                    if (ItemGem1 == 12)
                        AddAtkPc -= 0.1;
                    if (ItemGem2 == 12)
                        AddAtkPc -= 0.1;
                    if (ItemGem1 == 13)
                        AddAtkPc -= 0.15;
                    if (ItemGem2 == 13)
                        AddAtkPc -= 0.15;

                    //FuryGem
                    if (ItemGem1 == 21)
                        AddExpPc -= 0.1;
                    if (ItemGem2 == 21)
                        AddExpPc -= 0.1;
                    if (ItemGem1 == 22)
                        AddExpPc -= 0.15;
                    if (ItemGem2 == 22)
                        AddExpPc -= 0.15;
                    if (ItemGem1 == 23)
                        AddExpPc -= 0.25;
                    if (ItemGem2 == 23)
                        AddExpPc -= 0.25;

                    //RainbowGem
                    if (ItemGem1 == 31)
                        AddExpPc -= 0.1;
                    if (ItemGem2 == 31)
                        AddExpPc -= 0.1;
                    if (ItemGem1 == 32)
                        AddExpPc -= 0.15;
                    if (ItemGem2 == 32)
                        AddExpPc -= 0.15;
                    if (ItemGem1 == 33)
                        AddExpPc -= 0.25;
                    if (ItemGem2 == 33)
                        AddExpPc -= 0.25;

                    //VioletGem
                    if (ItemGem1 == 51)
                        AddProfPc -= 0.3;
                    if (ItemGem2 == 51)
                        AddProfPc -= 0.3;
                    if (ItemGem1 == 52)
                        AddProfPc -= 0.5;
                    if (ItemGem2 == 52)
                        AddProfPc -= 0.5;
                    if (ItemGem1 == 53)
                        AddProfPc -= 1;
                    if (ItemGem2 == 53)
                        AddProfPc -= 1;

                    //MoonGem
                    if (ItemGem1 == 61)
                        AddSpellPc -= 1.15;
                    if (ItemGem2 == 61)
                        AddSpellPc -= 1.15;
                    if (ItemGem1 == 62)
                        AddSpellPc -= 1.30;
                    if (ItemGem2 == 62)
                        AddSpellPc -= 1.30;
                    if (ItemGem1 == 63)
                        AddSpellPc -= 1.50;
                    if (ItemGem2 == 63)
                        AddSpellPc -= 1.50;

                    //TortoiseGem
                    /*if (ItemGem1 == 71)
                        AddMythique -= 2;
                    if (ItemGem2 == 71)
                        AddMythique -= 2;
                    if (ItemGem1 == 72)
                        AddMythique -= 4;
                    if (ItemGem2 == 72)
                        AddMythique -= 4;
                    if (ItemGem1 == 73)
                        AddMythique -= 6;
                    if (ItemGem2 == 73)
                        AddMythique -= 6;*/
                }
            }
            Ready = true;
        }

        public void UnEquip(byte From)
        {
            if (ItemsInInventory > 40)
                return;
            if (MyClient.Status == 7)
                return;

            Ready = false;
            try
            {
                MaxHP = BaseMaxHP();
                MaxMP = MaxMana();
                GetEquipStats(From, true);
                MyClient.SendPacket(General.MyPackets.RemoveItem((long)Equips_UIDs[From], From, 6));
                AddItem(Equips[From], 0, Equips_UIDs[From]);
                Equips[From] = null;
                Equips_UIDs[From] = 0;
            }
            catch (Exception Exc)
            {
                General.WriteLine(Convert.ToString(Exc));
            }
            Ready = true;
        }

        public void UseItem(ulong ItemUID, string Item)
        {
            Ready = false;
            string[] ItemParts = Item.Split('-');
            if (ItemParts[0] == "720027")
            {
                if (ItemsInInventory <= 30)
                {
                    AddItem("1088001-0-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    AddItem("1088001-0-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    AddItem("1088001-0-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    AddItem("1088001-0-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    AddItem("1088001-0-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    AddItem("1088001-0-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    AddItem("1088001-0-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    AddItem("1088001-0-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    AddItem("1088001-0-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    AddItem("1088001-0-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    RemoveItem(ItemUID);
                }
            }
            else if (ItemParts[0] == "720028")
            {
                if (ItemsInInventory <= 30)
                {
                    AddItem("1088000-0-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    AddItem("1088000-0-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    AddItem("1088000-0-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    AddItem("1088000-0-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    AddItem("1088000-0-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    AddItem("1088000-0-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    AddItem("1088000-0-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    AddItem("1088000-0-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    AddItem("1088000-0-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    AddItem("1088000-0-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    RemoveItem(ItemUID);
                }
            }
            else if (ItemParts[0] == "723712")
            {
                if (ItemsInInventory <= 35)
                {
                    AddItem("730001-1-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    AddItem("730001-1-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    AddItem("730001-1-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    AddItem("730001-1-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    AddItem("730001-1-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    RemoveItem(ItemUID);
                }
            }
            else if (ItemParts[0] == "723728")
            {
                if (ItemsInInventory <= 35)
                {
                    AddItem("730002-2-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    AddItem("730002-2-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    AddItem("730002-2-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    AddItem("730002-2-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    AddItem("730002-2-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    RemoveItem(ItemUID);
                }
            }
            else if (ItemParts[0] == "723729")
            {
                if (ItemsInInventory <= 35)
                {
                    AddItem("730003-3-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    AddItem("730003-3-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    AddItem("730003-3-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    AddItem("730003-3-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    AddItem("730003-3-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    RemoveItem(ItemUID);
                }
            }
            else if (ItemParts[0] == "723730")
            {
                if (ItemsInInventory <= 35)
                {
                    AddItem("730004-4-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    AddItem("730004-4-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    AddItem("730004-4-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    AddItem("730004-4-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    AddItem("730004-4-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    RemoveItem(ItemUID);
                }
            }
            else if (ItemParts[0] == "723713")
            {
                Silvers += 300000;
                MyClient.SendPacket(General.MyPackets.Vital(UID, 4, Silvers));
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "723714")
            {
                Silvers += 800000;
                MyClient.SendPacket(General.MyPackets.Vital(UID, 4, Silvers));
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "723715")
            {
                Silvers += 1200000;
                MyClient.SendPacket(General.MyPackets.Vital(UID, 4, Silvers));
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "723716")
            {
                Silvers += 1800000;
                MyClient.SendPacket(General.MyPackets.Vital(UID, 4, Silvers));
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "723717")
            {
                Silvers += 5000000;
                MyClient.SendPacket(General.MyPackets.Vital(UID, 4, Silvers));
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "723718")
            {
                Silvers += 20000000;
                MyClient.SendPacket(General.MyPackets.Vital(UID, 4, Silvers));
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "723719")
            {
                Silvers += 25000000;
                MyClient.SendPacket(General.MyPackets.Vital(UID, 4, Silvers));
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "723720")
            {
                Silvers += 80000000;
                MyClient.SendPacket(General.MyPackets.Vital(UID, 4, Silvers));
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "723721")
            {
                Silvers += 100000000;
                MyClient.SendPacket(General.MyPackets.Vital(UID, 4, Silvers));
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "723722")
            {
                Silvers += 300000000;
                MyClient.SendPacket(General.MyPackets.Vital(UID, 4, Silvers));
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "723723")
            {
                Silvers += 500000000;
                MyClient.SendPacket(General.MyPackets.Vital(UID, 4, Silvers));
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "720028")
            {
                if (ItemsInInventory <= 30)
                {
                    AddItem("1088000-0-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    AddItem("1088000-0-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    AddItem("1088000-0-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    AddItem("1088000-0-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    AddItem("1088000-0-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    AddItem("1088000-0-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    AddItem("1088000-0-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    AddItem("1088000-0-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    AddItem("1088000-0-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    AddItem("1088000-0-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    RemoveItem(ItemUID);
                }
            }
            else if (ItemParts[0] == "721540")
            {
                if (ItemsInInventory <= 36)
                {
                    AddItem("1088001-0-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    AddItem("1088001-0-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    AddItem("1088001-0-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    if (Other.ChanceSuccess(50))
                    {
                        AddItem("1088000-0-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    }
                    RemoveItem(ItemUID);
                }
            }
            else if (ItemParts[0] == "723700") // ExpBall
            {
                if (Level < 100)
                    AddExp((ulong)(1295000 + Level * 50000), false);
                else if (Level < 110)
                    AddExp((ulong)(1395000 + Level * 80000), false);
                else if (Level < 115)
                    AddExp((ulong)(1595000 + Level * 100000), false);
                else if (Level < 120)
                    AddExp((ulong)(1895000 + Level * 120000), false);
                else if (Level < 125)
                    AddExp((ulong)(2095000 + Level * 150000), false);
                else if (Level < 130)
                    AddExp((ulong)(2395000 + Level * 180000), false);
                else if (Level < 137)
                    AddExp((ulong)(2895000 + Level * 200000), false);

                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "723017")//PotXp
            {
                dexptime = 3600;
                dexp = 1;
                MyClient.SendPacket(General.MyPackets.Vital(UID, 19, dexptime));
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "1200000")//PrayingStone(S)
            {
                if (!Blessed)
                {
                    HBStart = DateTime.Now;
                    HBEnd = DateTime.Now.AddDays(3);
                    WhichBless = 1;
                    Blessed = true;
                    MyClient.SendPacket(General.MyPackets.Vital(UID, 18, (3 * 24 * 60 * 60)));
                    MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                    World.UpdateSpawn(this);
                    RemoveItem(ItemUID);
                    SaveHB();
                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "zf2-e128"));
                }
                else
                {
                    MyClient.SendPacket(General.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "You are already blessed by Heaven Blessing!", 2005));
                }
            }
            else if (ItemParts[0] == "1200001")//PrayingStone(M)
            {
                if (!Blessed)
                {
                    HBStart = DateTime.Now;
                    HBEnd = DateTime.Now.AddDays(7);
                    WhichBless = 2;
                    Blessed = true;
                    MyClient.SendPacket(General.MyPackets.Vital(UID, 18, (3 * 24 * 60 * 60)));
                    MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                    World.UpdateSpawn(this);
                    RemoveItem(ItemUID);
                    SaveHB();
                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "zf2-e128"));
                }
                else
                {
                    MyClient.SendPacket(General.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "You are already blessed by Heaven Blessing!", 2005));
                }
            }
            else if (ItemParts[0] == "1200002")//PrayingStone(L)
            {
                if (!Blessed)
                {
                    HBStart = DateTime.Now;
                    HBEnd = DateTime.Now.AddDays(30);
                    WhichBless = 3;
                    Blessed = true;
                    MyClient.SendPacket(General.MyPackets.Vital(UID, 18, (3 * 24 * 60 * 60)));
                    MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                    World.UpdateSpawn(this);
                    RemoveItem(ItemUID);
                    SaveHB();
                    MyClient.SendPacket(General.MyPackets.String(UID, 10, "zf2-e128"));
                }
                else
                {
                    MyClient.SendPacket(General.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "You are already blessed by Heaven Blessing!", 2005));
                }
            }
            else if (ItemParts[0] == "720021") //GuildTransport1
            {
                if (MyGuild == World.PoleHolder && GuildPosition == 100)
                {
                    SingleNPC OldNPC = new SingleNPC(614, 1450, 2, 0, (short)DataBase.GC1X, (short)DataBase.GC1Y, (short)DataBase.GC1Map, 0);
                    NPCs.AllNPCs.Remove(614);
                    World.RemoveNPC(OldNPC);

                    DataBase.ChangeGC("GC1", LocMap, LocX, LocY);

                    SingleNPC NewNPC = new SingleNPC(614, 1450, 2, 0, (short)DataBase.GC1X, (short)DataBase.GC1Y, (short)DataBase.GC1Map, 0);
                    NPCs.AllNPCs.Add(614, NewNPC);
                    World.SpawnsNPC(NewNPC);

                    RemoveItem(ItemUID);
                }
            }
            else if (ItemParts[0] == "720022") //GuildTransport2
            {
                if (MyGuild == World.PoleHolder && GuildPosition == 100)
                {
                    SingleNPC OldNPC = new SingleNPC(615, 1460, 2, 0, (short)DataBase.GC2X, (short)DataBase.GC2Y, (short)DataBase.GC2Map, 0);
                    NPCs.AllNPCs.Remove(615);
                    World.RemoveNPC(OldNPC);

                    DataBase.ChangeGC("GC2", LocMap, LocX, LocY);

                    SingleNPC NewNPC = new SingleNPC(615, 1460, 2, 0, (short)DataBase.GC2X, (short)DataBase.GC2Y, (short)DataBase.GC2Map, 0);
                    NPCs.AllNPCs.Add(615, NewNPC);
                    World.SpawnsNPC(NewNPC);

                    RemoveItem(ItemUID);
                }
            }
            else if (ItemParts[0] == "720023") //GuildTransport3
            {
                if (MyGuild == World.PoleHolder && GuildPosition == 100)
                {
                    SingleNPC OldNPC = new SingleNPC(616, 1470, 2, 0, (short)DataBase.GC3X, (short)DataBase.GC3Y, (short)DataBase.GC3Map, 0);
                    NPCs.AllNPCs.Remove(616);
                    World.RemoveNPC(OldNPC);

                    DataBase.ChangeGC("GC3", LocMap, LocX, LocY);

                    SingleNPC NewNPC = new SingleNPC(616, 1470, 2, 0, (short)DataBase.GC3X, (short)DataBase.GC3Y, (short)DataBase.GC3Map, 0);
                    NPCs.AllNPCs.Add(616, NewNPC);
                    World.SpawnsNPC(NewNPC);

                    RemoveItem(ItemUID);
                }
            }
            else if (ItemParts[0] == "720024") //GuildTransport4
            {
                if (MyGuild == World.PoleHolder && GuildPosition == 100)
                {
                    SingleNPC OldNPC = new SingleNPC(617, 1480, 2, 0, (short)DataBase.GC4X, (short)DataBase.GC4Y, (short)DataBase.GC4Map, 0);
                    NPCs.AllNPCs.Remove(617);
                    World.RemoveNPC(OldNPC);

                    DataBase.ChangeGC("GC4", LocMap, LocX, LocY);

                    SingleNPC NewNPC = new SingleNPC(617, 1480, 2, 0, (short)DataBase.GC4X, (short)DataBase.GC4Y, (short)DataBase.GC4Map, 0);
                    NPCs.AllNPCs.Add(617, NewNPC);
                    World.SpawnsNPC(NewNPC);

                    RemoveItem(ItemUID);
                }
            }
            else if (ItemParts[0] == "723701") //ExemptionToken
            {
                if (RBCount == 1)
                {
                    if (Level >= 120)
                    {
                        if (Job == 15 || Job == 25 || Job == 45 || Job == 135 || Job == 145)
                        {
                            MyClient.CurrentNPC = 128;
                            MyClient.SendPacket(General.MyPackets.NPCSay("F�licitation, vous avez tous les pr�requis! En quel classe voulez-vous rena�tre?"));
                            MyClient.SendPacket(General.MyPackets.NPCLink("Brave.", 11));
                            MyClient.SendPacket(General.MyPackets.NPCLink("Guerrier.", 21));
                            MyClient.SendPacket(General.MyPackets.NPCLink("Archer.", 41));
                            MyClient.SendPacket(General.MyPackets.NPCLink("Tao�ste d'eau.", 132));
                            MyClient.SendPacket(General.MyPackets.NPCLink("Tao�ste de feu.", 142));
                            MyClient.SendPacket(General.MyPackets.NPCLink("Je dois r�fl�chir.", 255));
                            MyClient.SendPacket(General.MyPackets.NPCSetFace(6));
                            MyClient.SendPacket(General.MyPackets.NPCFinish());
                        }
                        else
                        {
                            MyClient.SendPacket(General.MyPackets.NPCSay("Vous devez rentrer en fonctione avant de rena�tre pour la seconde fois!"));
                            MyClient.SendPacket(General.MyPackets.NPCLink("Ok.", 255));
                            MyClient.SendPacket(General.MyPackets.NPCSetFace(6));
                            MyClient.SendPacket(General.MyPackets.NPCFinish());
                        }
                    }
                    else
                    {
                        MyClient.SendPacket(General.MyPackets.NPCSay("Vous devez �tre niveau 130 minimum!"));
                        MyClient.SendPacket(General.MyPackets.NPCLink("Ok.", 255));
                        MyClient.SendPacket(General.MyPackets.NPCSetFace(6));
                        MyClient.SendPacket(General.MyPackets.NPCFinish());
                    }
                }
                else
                {
                    MyClient.SendPacket(General.MyPackets.NPCSay("Vous n'�tes pas de premi�re renaissance je ne peux pas vous aider!"));
                    MyClient.SendPacket(General.MyPackets.NPCLink("Ok.", 255));
                    MyClient.SendPacket(General.MyPackets.NPCSetFace(6));
                    MyClient.SendPacket(General.MyPackets.NPCFinish());
                }
            }
            else if (ItemParts[0] == "1000000")
            {
                CurHP += 20;
                if (CurHP > MaxHP)
                    CurHP = MaxHP;
                MyClient.SendPacket(General.MyPackets.Vital(UID, 0, CurHP));
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "1000010")
            {
                CurHP += 100;
                if (CurHP > MaxHP)
                    CurHP = MaxHP;
                MyClient.SendPacket(General.MyPackets.Vital(UID, 0, CurHP));
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "1000020")
            {
                CurHP += 250;
                if (CurHP > MaxHP)
                    CurHP = MaxHP;
                MyClient.SendPacket(General.MyPackets.Vital(UID, 0, CurHP));
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "1000030")
            {
                CurHP += 500;
                if (CurHP > MaxHP)
                    CurHP = MaxHP;
                MyClient.SendPacket(General.MyPackets.Vital(UID, 0, CurHP));
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "1002000")
            {
                CurHP += 800;
                if (CurHP > MaxHP)
                    CurHP = MaxHP;
                MyClient.SendPacket(General.MyPackets.Vital(UID, 0, CurHP));
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "1002010")
            {
                CurHP += 1200;
                if (CurHP > MaxHP)
                    CurHP = MaxHP;
                MyClient.SendPacket(General.MyPackets.Vital(UID, 0, CurHP));
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "1002020")
            {
                CurHP += 2000;
                if (CurHP > MaxHP)
                    CurHP = MaxHP;
                MyClient.SendPacket(General.MyPackets.Vital(UID, 0, CurHP));
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "1001000")
            {
                CurMP += 70;
                if (CurMP > MaxMP)
                    CurMP = MaxMP;
                MyClient.SendPacket(General.MyPackets.Vital(UID, 2, CurMP));
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "1001010")
            {
                CurMP += 200;
                if (CurMP > MaxMP)
                    CurMP = MaxMP;
                MyClient.SendPacket(General.MyPackets.Vital(UID, 2, CurMP));
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "1001020")
            {
                CurMP += 450;
                if (CurMP > MaxMP)
                    CurMP = MaxMP;
                MyClient.SendPacket(General.MyPackets.Vital(UID, 2, CurMP));
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "1001030")
            {
                CurMP += 1000;
                if (CurMP > MaxMP)
                    CurMP = MaxMP;
                MyClient.SendPacket(General.MyPackets.Vital(UID, 2, CurMP));
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "1001040")
            {
                CurMP += 2000;
                if (CurMP > MaxMP)
                    CurMP = MaxMP;
                MyClient.SendPacket(General.MyPackets.Vital(UID, 2, CurMP));
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "1002030")
            {
                CurMP += 3000;
                if (CurMP > MaxMP)
                    CurMP = MaxMP;
                MyClient.SendPacket(General.MyPackets.Vital(UID, 2, CurMP));
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "1002040")
            {
                CurMP += 4500;
                if (CurMP > MaxMP)
                    CurMP = MaxMP;
                MyClient.SendPacket(General.MyPackets.Vital(UID, 2, CurMP));
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "1002050")
            {
                CurHP += 3000;
                if (CurHP > MaxHP)
                    CurHP = MaxHP;
                MyClient.SendPacket(General.MyPackets.Vital(UID, 0, CurHP));
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "720010")
            {
                if (ItemsInInventory <= 37)
                {
                    AddItem("1000030-0-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    AddItem("1000030-0-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    AddItem("1000030-0-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    RemoveItem(ItemUID);
                }
            }
            else if (ItemParts[0] == "720011")
            {
                if (ItemsInInventory <= 37)
                {
                    AddItem("1002000-0-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    AddItem("1002000-0-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    AddItem("1002000-0-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    RemoveItem(ItemUID);
                }
            }
            else if (ItemParts[0] == "720012")
            {
                if (ItemsInInventory <= 37)
                {
                    AddItem("1002010-0-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    AddItem("1002010-0-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    AddItem("1002010-0-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    RemoveItem(ItemUID);
                }
            }
            else if (ItemParts[0] == "720013")
            {
                if (ItemsInInventory <= 37)
                {
                    AddItem("1002020-0-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    AddItem("1002020-0-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    AddItem("1002020-0-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    RemoveItem(ItemUID);
                }
            }
            else if (ItemParts[0] == "723711")
            {
                if (ItemsInInventory <= 35)
                {
                    AddItem("1088001-0-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    AddItem("1088001-0-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    AddItem("1088001-0-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    AddItem("1088001-0-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    AddItem("1088001-0-0-0-0-0", 0, (uint)General.Rand.Next(346623472));
                    RemoveItem(ItemUID);
                }
            }
            else if (ItemParts[0] == "723583")
            {
                if (Model == 1004)
                    Model -= 1;
                else if (Model == 1003)
                    Model += 1;
                if (Model == 2002)
                    Model -= 1;
                else if (Model == 2001)
                    Model += 1;


                MyClient.SendPacket(General.MyPackets.Vital(UID, 12, ulong.Parse(Avatar.ToString() + Model.ToString())));
                World.UpdateSpawn(this);
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "1060020")
            {
                if (CurHP != 0)
                {
                    if (LocMap == 6001 || LocMap == 6000 || LocMap == 1505)
                    {
                        RemoveItem(ItemUID);
                        General.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Vous ne pouvez pas utiliser de parchemin ici!", 2005);
                    }
                    else
                    {
                        Teleport(1002, 429, 378);
                        RemoveItem(ItemUID);
                    }
                }
                else
                    General.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Vous ne pouvez pas utiliser de parchemin lorsque vous �tes mort!", 2005);
            }
            else if (ItemParts[0] == "1060021")
            {
                if (CurHP != 0)
                {
                    if (LocMap == 6001 || LocMap == 6000 || LocMap == 1505)
                    {
                        RemoveItem(ItemUID);
                        General.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Vous ne pouvez pas utiliser de parchemin ici!", 2005);
                    }
                    else
                    {
                        Teleport(1000, 500, 650);
                        RemoveItem(ItemUID);
                    }
                }
                else
                    General.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Vous ne pouvez pas utiliser de parchemin lorsque vous �tes mort!", 2005);
            }
            else if (ItemParts[0] == "1060022")
            {
                if (CurHP != 0)
                {
                    if (LocMap == 6001 || LocMap == 6000 || LocMap == 1505)
                    {
                        RemoveItem(ItemUID);
                        General.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Vous ne pouvez pas utiliser de parchemin ici!", 2005);
                    }
                    else
                    {
                        Teleport(1020, 565, 562);
                        RemoveItem(ItemUID);
                    }
                }
                else
                    General.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Vous ne pouvez pas utiliser de parchemin lorsque vous �tes mort!", 2005);
            }
            else if (ItemParts[0] == "1060023")
            {
                if (CurHP != 0)
                {
                    if (LocMap == 6001 || LocMap == 6000 || LocMap == 1505)
                    {
                        RemoveItem(ItemUID);
                        General.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Vous ne pouvez pas utiliser de parchemin ici!", 2005);
                    }
                    else
                    {
                        Teleport(1011, 188, 264);
                        RemoveItem(ItemUID);
                    }
                }
                else
                    General.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Vous ne pouvez pas utiliser de parchemin lorsque vous �tes mort!", 2005);
            }
            else if (ItemParts[0] == "1060024")
            {
                if (CurHP != 0)
                {
                    if (LocMap == 6001 || LocMap == 6000 || LocMap == 1505)
                    {
                        RemoveItem(ItemUID);
                        General.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Vous ne pouvez pas utiliser de parchemin ici!", 2005);
                    }
                    else
                    {
                        Teleport(1015, 717, 571);
                        RemoveItem(ItemUID);
                    }
                }
                else
                    General.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Vous ne pouvez pas utiliser de parchemin lorsque vous �tes mort!", 2005);
            }
            else if (ItemParts[0] == "1060030")
            {
                Hair = ushort.Parse("3" + Convert.ToString(Hair)[1] + Convert.ToString(Hair)[2]);
                MyClient.SendPacket(General.MyPackets.Vital(UID, 27, Hair));
                MyClient.SendPacket(General.MyPackets.Vital(UID, 0, CurHP));
                RemoveItem(ItemUID);
                World.UpdateSpawn(this);
            }
            else if (ItemParts[0] == "1060040")
            {
                Hair = ushort.Parse("9" + Convert.ToString(Hair)[1] + Convert.ToString(Hair)[2]);
                MyClient.SendPacket(General.MyPackets.Vital(UID, 27, Hair));
                MyClient.SendPacket(General.MyPackets.Vital(UID, 0, CurHP));
                RemoveItem(ItemUID);
                World.UpdateSpawn(this);
            }
            else if (ItemParts[0] == "1060050")
            {
                Hair = ushort.Parse("8" + Convert.ToString(Hair)[1] + Convert.ToString(Hair)[2]);
                MyClient.SendPacket(General.MyPackets.Vital(UID, 27, Hair));
                RemoveItem(ItemUID);
                World.UpdateSpawn(this);
            }
            else if (ItemParts[0] == "1060060")
            {
                Hair = ushort.Parse("7" + Convert.ToString(Hair)[1] + Convert.ToString(Hair)[2]);
                MyClient.SendPacket(General.MyPackets.Vital(UID, 27, Hair));
                RemoveItem(ItemUID);
                World.UpdateSpawn(this);
            }
            else if (ItemParts[0] == "1060070")
            {
                Hair = ushort.Parse("6" + Convert.ToString(Hair)[1] + Convert.ToString(Hair)[2]);
                MyClient.SendPacket(General.MyPackets.Vital(UID, 27, Hair));
                RemoveItem(ItemUID);
                World.UpdateSpawn(this);
            }
            else if (ItemParts[0] == "1060080")
            {
                Hair = ushort.Parse("5" + Convert.ToString(Hair)[1] + Convert.ToString(Hair)[2]);
                MyClient.SendPacket(General.MyPackets.Vital(UID, 27, Hair));
                RemoveItem(ItemUID);
                World.UpdateSpawn(this);
            }
            else if (ItemParts[0] == "1060090")
            {
                Hair = ushort.Parse("4" + Convert.ToString(Hair)[1] + Convert.ToString(Hair)[2]);
                MyClient.SendPacket(General.MyPackets.Vital(UID, 27, Hair));
                RemoveItem(ItemUID);
                World.UpdateSpawn(this);
            }
            else if (ItemParts[0] == "723727")
            {
                if (PKPoints <= 30)
                    PKPoints = 0;
                else
                    PKPoints -= 30;

                MyClient.SendPacket(General.MyPackets.Vital(UID, 26, GetStat()));
                World.UpdateSpawn(this);
                MyClient.SendPacket(General.MyPackets.Vital(UID, 6, PKPoints));
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "723584")
            {
                string[] item = Equips[3].Split('-');
                string newitem = item[0];
                newitem = newitem.Remove(newitem.Length - 3, 1);
                newitem = newitem.Insert(newitem.Length - 2, "2");
                Equips[3] = newitem + "-" + item[1] + "-" + item[2] + "-" + item[3] + "-" + item[4] + "-" + item[5];
                MyClient.SendPacket(General.MyPackets.AddItem((long)Equips_UIDs[3], int.Parse(newitem), byte.Parse(item[1]), byte.Parse(item[2]), byte.Parse(item[3]), byte.Parse(item[4]), byte.Parse(item[5]), 3, 100, 100));
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "1060101")
            {
                if (Job >= 143)
                {
                    LearnSkill(1165, 0);
                    RemoveItem(ItemUID);
                }
            }
            else if (ItemParts[0] == "1060100")
            {
                if (Job >= 143)
                {
                    LearnSkill(1160, 0);
                    RemoveItem(ItemUID);
                }
            }
            else if (ItemParts[0] == "725025")
            {
                if (Job < 26 && Job > 19 && Level >= 40)
                {
                    LearnSkill(1320, 0);
                    RemoveItem(ItemUID);
                }
            }
            else if (ItemParts[0] == "725026")
            {
                LearnSkill(5010, 0);
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "725027")
            {
                LearnSkill(5020, 0);
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "725028")
            {
                if (Job > 130 && Level >= 70)
                {
                    LearnSkill(5001, 0);
                    RemoveItem(ItemUID);
                }
            }
            else if (ItemParts[0] == "725029")
            {
                LearnSkill(5030, 0);
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "725030")
            {
                LearnSkill(5040, 0);
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "725031")
            {
                LearnSkill(5050, 0);
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "725040")
            {
                LearnSkill(7000, 0);
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "725041")
            {
                LearnSkill(7010, 0);
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "725042")
            {
                LearnSkill(7020, 0);
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "725043")
            {
                LearnSkill(7030, 0);
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "725044")
            {
                LearnSkill(7040, 0);
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "725045")
            {
                LearnSkill(1220, 0);
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "725018")
            {
                LearnSkill(1380, 0);
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "725019")
            {
                LearnSkill(1385, 0);
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "725020")
            {
                LearnSkill(1390, 0);
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "725021")
            {
                LearnSkill(1395, 0);
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "725022")
            {
                LearnSkill(1400, 0);
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "725023")
            {
                LearnSkill(1405, 0);
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "725024")
            {
                LearnSkill(1410, 0);
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "725000")
            {
                if (Spi >= 20)
                {
                    LearnSkill(1000, 0);
                    RemoveItem(ItemUID);
                }
            }
            else if (ItemParts[0] == "725001")
            {
                if (Spi >= 80)
                {
                    LearnSkill(1001, 0);
                    RemoveItem(ItemUID);
                }
            }
            else if (ItemParts[0] == "725002")
            {
                if (Spi >= 160 && Job >= 143)
                {
                    LearnSkill(1002, 0);
                    RemoveItem(ItemUID);
                }
            }
            else if (ItemParts[0] == "725003")
            {
                if (Spi >= 30)
                {
                    LearnSkill(1005, 0);
                    RemoveItem(ItemUID);
                }
            }
            else if (ItemParts[0] == "725004")
            {
                if (Spi >= 25)
                {
                    LearnSkill(1010, 0);
                    RemoveItem(ItemUID);
                }
            }
            else if (ItemParts[0] == "725005")
            {
                LearnSkill(1045, 0);
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "725010")
            {
                LearnSkill(1046, 0);
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "725011")
            {
                LearnSkill(1250, 0);
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "725012")
            {
                LearnSkill(1260, 0);
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "725013")
            {
                LearnSkill(1290, 0);
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "725014")
            {
                LearnSkill(1300, 0);
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "725015")
            {
                if (Job > 129 && Job < 136)
                {
                    LearnSkill(1350, 0);
                    RemoveItem(ItemUID);
                }
            }
            else if (ItemParts[0] == "725016")
            {
                LearnSkill(1360, 0);
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "720393") //ExpPill
            {
                MyClient.SendPacket(General.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Votre exp�rience est 3x plus grande lors de l'xp!", 2005));
                dexptime = 7200;
                dexp = 2;
                MyClient.SendPacket(General.MyPackets.Vital(UID, 19, dexptime));
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "720394") //ExpAmrita
            {
                MyClient.SendPacket(General.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Votre exp�rience est 5x plus grande lors de l'xp!", 2005));
                dexptime = 7200;
                dexp = 3;
                MyClient.SendPacket(General.MyPackets.Vital(UID, 19, dexptime));
                RemoveItem(ItemUID);
            }
            else if (ItemParts[0] == "721635") //ParcheminAncien
            {
                MyClient.SendPacket(General.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Votre qu�te des gourdes � �t� remplac� pour la qu�te du Nouvel An!", 2005));
                if (InventoryContains(750000, 1))
                    RemoveItem(ItemNext(750000));
                QuestMob = "GibbonDesNeige";
                QuestFrom = "NewYear";
                QuestKO = 0;
                Teleport(1825, 75, 75);
                RemoveItem(ItemUID);
                MyClient.SendPacket(General.MyPackets.NPCSay("Vous devez tuer 300 monstres pour obtenir la Gra�ne."));
                MyClient.SendPacket(General.MyPackets.NPCFinish());
            }
            else if (ItemParts[0] == "721630") //EmballageCadeau
            {
                if (InventoryContains(721630, 1) && InventoryContains(721631, 1) && InventoryContains(721632, 1))
                {
                    AddItem("721633-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                    RemoveItem(ItemNext(721630));
                    RemoveItem(ItemNext(721631));
                    RemoveItem(ItemNext(721632));
                }
                else
                    MyClient.SendPacket(General.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Vous ne pouvez pas utilisez l'objet sans avoir tous les objets!", 2005));
            }
            else if (ItemParts[0] == "721631") //CartonCadeau
            {
                if (InventoryContains(721630, 1) && InventoryContains(721631, 1) && InventoryContains(721632, 1))
                {
                    AddItem("721633-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                    RemoveItem(ItemNext(721630));
                    RemoveItem(ItemNext(721631));
                    RemoveItem(ItemNext(721632));
                }
                else
                    MyClient.SendPacket(General.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Vous ne pouvez pas utilisez l'objet sans avoir tous les objets!", 2005));
            }
            else if (ItemParts[0] == "721632") //GlaiveCadeau
            {
                if (InventoryContains(721630, 1) && InventoryContains(721631, 1) && InventoryContains(721632, 1))
                {
                    AddItem("721633-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                    RemoveItem(ItemNext(721630));
                    RemoveItem(ItemNext(721631));
                    RemoveItem(ItemNext(721632));
                }
                else
                    MyClient.SendPacket(General.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Vous ne pouvez pas utilisez l'objet sans avoir tous les objets!", 2005));
            }
            else
            {
                MyClient.SendPacket(General.MyPackets.SendMsg(MyClient.MessageId, "SYSTEM", Name, "Vous ne pouvez pas utiliser cet objet.", 2005));
            }
            Ready = true;
        }

        public void RemoveItem(ulong UID)
        {
            Ready = false;
            int count = 0;
            foreach (ulong uid in Inventory_UIDs)
            {
                count++;
                if (UID == uid)
                {
                    count--;
                    byte Do = (byte)(ItemsInInventory - count);
                    for (int p = 0; p < Do; p++)
                    {
                        Inventory[count + p] = Inventory[count + p + 1];
                        Inventory_UIDs[count + p] = Inventory_UIDs[count + p + 1];
                    }
                    Inventory[ItemsInInventory] = null;
                    Inventory_UIDs[ItemsInInventory] = 0;
                    ItemsInInventory -= 1;

                    MyClient.SendPacket(General.MyPackets.RemoveItem((long)UID, 0, 3));
                    break;
                }
            }
            Ready = true;
        }

        public void LearnSkill(short SkillId, byte SkillLvl)
        {
            Ready = false;

            if (!Skills.Contains((short)SkillId))
            {
                Skills.Add((short)SkillId, SkillLvl);
                Skill_Exps.Add((short)SkillId, SkillExpNull);
            }

            MyClient.SendPacket(General.MyPackets.LearnSkill(SkillId, SkillLvl, 0));
            Ready = true;
        }

        public void LearnSkill2(short SkillId, byte SkillLvl)
        {
            Ready = false;

            if (!Skills.Contains((short)SkillId))
            {
                Skills.Add((short)SkillId, SkillLvl);
                Skill_Exps.Add((short)SkillId, SkillExpNull);
            }
            else
            {
                Skills.Remove((short)SkillId);
                Skill_Exps.Remove((short)SkillId);
                Skills.Add((short)SkillId, SkillLvl);
                Skill_Exps.Add((short)SkillId, SkillExpNull);
            }

            MyClient.SendPacket(General.MyPackets.LearnSkill(SkillId, SkillLvl, 0));
            Ready = true;
        }


        public void PackProfs()
        {
            if (!MyClient.There)
                return;
            Ready = false;
            IDictionaryEnumerator IE = Profs.GetEnumerator();
            PackedProfs = "";

            while (IE.MoveNext())
            {
                short prof_id = (short)IE.Key;
                byte prof_lv = (byte)IE.Value;

                PackedProfs += Convert.ToString(prof_id) + ":" + Convert.ToString(prof_lv) + ":" + Convert.ToString(Prof_Exps[prof_id]) + "~";
            }

            if (PackedProfs.Length > 0)
            {
                PackedProfs = PackedProfs.Remove(PackedProfs.Length - 1, 1);
            }
            Ready = true;
        }

        public void UnPackProfs()
        {
            Ready = false;
            if (PackedProfs.Length == 0)
                return;

            string[] profs = PackedProfs.Split('~');

            foreach (string prof in profs)
            {
                string[] ThisProf = prof.Split(':');

                Profs.Add(short.Parse(ThisProf[0]), byte.Parse(ThisProf[1]));
                Prof_Exps.Add(short.Parse(ThisProf[0]), uint.Parse(ThisProf[2]));
            }
            Ready = true;
        }

        public void SendProfs()
        {
            Ready = false;
            IDictionaryEnumerator IE = Profs.GetEnumerator();

            while (IE.MoveNext())
            {
                short prof_id = (short)IE.Key;
                byte prof_lvl = (byte)IE.Value;
                uint prof_exp = (uint)Prof_Exps[prof_id];

                MyClient.SendPacket(General.MyPackets.Prof(prof_id, prof_lvl, prof_exp));
            }
            Ready = true;
        }

        public void PackSkills()
        {
            if (!MyClient.There)
                return;
            Ready = false;
            IDictionaryEnumerator IE = Skills.GetEnumerator();
            PackedSkills = "";

            while (IE.MoveNext())
            {
                short skill_id = (short)IE.Key;
                byte skill_lv = (byte)IE.Value;

                PackedSkills += Convert.ToString(skill_id) + ":" + Convert.ToString(skill_lv) + ":" + Convert.ToString(Skill_Exps[skill_id]) + "~";
            }

            if (PackedSkills.Length > 0)
            {
                PackedSkills = PackedSkills.Remove(PackedSkills.Length - 1, 1);
            }
            Ready = true;
        }


        public void UnPackSkills()
        {
            Ready = false;
            if (PackedSkills.Length == 0)
                return;

            string[] skills = PackedSkills.Split('~');

            foreach (string skill in skills)
            {
                string[] ThisSkill = skill.Split(':');

                Skills.Add(short.Parse(ThisSkill[0]), byte.Parse(ThisSkill[1]));
                Skill_Exps.Add(short.Parse(ThisSkill[0]), uint.Parse(ThisSkill[2]));
            }
            Ready = true;
        }

        public void SendSkills()
        {
            Ready = false;
            IDictionaryEnumerator IE = Skills.GetEnumerator();

            while (IE.MoveNext())
            {
                short skill_id = (short)IE.Key;
                byte skill_lvl = (byte)IE.Value;
                uint skill_exp = (uint)Skill_Exps[skill_id];

                MyClient.SendPacket(General.MyPackets.LearnSkill(skill_id, skill_lvl, skill_exp));
            }
            Ready = true;
        }

        public ushort BaseMaxHP()
        {
            Ready = false;
            double hp = Vit * 24 + Str * 3 + Agi * 3 + Spi * 3;
            double ExtraHp = 0;
            if (Job == 11)
                hp *= 1.05;
            if (Job == 12)
                hp *= 1.08;
            if (Job == 13)
                hp *= 1.1;
            if (Job == 14)
                hp *= 1.12;
            if (Job == 15)
                hp *= 1.15;

            byte Pos = 0;
            while (Pos < 10)
            {
                if (Equips[Pos] != "0" && Equips[Pos] != null)
                {
                    string[] Splitter = Equips[Pos].Split('-');

                    uint ItemId = uint.Parse(Splitter[0]);

                    if (ItemId == 137310) //GMRobe
                        ExtraHp += 30000;

                    if (ItemId == 137320 || ItemId == 137420 || ItemId == 137520 || ItemId == 137620 || ItemId == 137720 || ItemId == 137820 || ItemId == 137920) //PhoenixDress
                        ExtraHp += 255;

                    if (ItemId == 137330 || ItemId == 137430 || ItemId == 137530 || ItemId == 137630 || ItemId == 137730 || ItemId == 137830 || ItemId == 137930) //ElegantDress
                        ExtraHp += 255;

                    if (ItemId == 137340 || ItemId == 137440 || ItemId == 137540 || ItemId == 137640 || ItemId == 137740 || ItemId == 137840 || ItemId == 137940) //CelestialDress
                        ExtraHp += 255;

                    if (ItemId == 137350 || ItemId == 137450 || ItemId == 137550 || ItemId == 137650 || ItemId == 137750 || ItemId == 137850 || ItemId == 137950) //WeddingDress
                        ExtraHp += 1000;

                    if (ItemId == 150000 || ItemId == 150310 || ItemId == 150320) //LoveForever
                        ExtraHp += 800;

                    if (ItemId == 120319) //LoveLock
                        ExtraHp += 200;

                    if (ItemId == 2100025) //Gourd HP/MP
                        ExtraHp += 800;

                    if (ItemId == 2100055) //PrixDeBronze
                        ExtraHp += 900;

                    if (ItemId == 2100065) //PrixD`Argent
                        ExtraHp += 1200;

                    if (ItemId == 2100075) //Troph�D`Or
                        ExtraHp += 1500;

                    if (ItemId == 2100085) //PrixD`Or
                        ExtraHp += 1500;

                    if (ItemId == 2100095) //CoupeD`Or
                        ExtraHp += 1500;
                }
                Pos++;
            }

            hp += ExtraHp;
            hp += ExtraHP;
            Ready = true;
            return (ushort)hp;
        }

        public ushort MaxMana()
        {
            ushort mana = (ushort)(Spi * 15);
            ushort ExtraMp = 0;

            if (Job == 133 || Job == 143 || Job == 153 || Job == 123)
                mana = (ushort)((double)mana * 1.33333333333333333333333);
            else if (Job == 134 || Job == 144 || Job == 154 || Job == 124)
                mana = (ushort)((double)mana * 1.66666666666666666666666);
            else if (Job == 135 || Job == 145 || Job == 155 || Job == 125)
                mana *= 2;
            else
                mana /= 3;

            byte Pos = 0;
            while (Pos < 10)
            {
                if (Equips[Pos] != "0" && Equips[Pos] != null)
                {
                    string[] Splitter = Equips[Pos].Split('-');

                    uint ItemId = uint.Parse(Splitter[0]);

                    if (ItemId == 2100025) //Gourd HP/MP
                        ExtraMp += 800;

                    if (ItemId == 2100045) //Gourd MP
                        ExtraMp += 400;

                    if (ItemId == 2100055) //PrixDeBronze
                        ExtraMp += 900;

                    if (ItemId == 2100065) //PrixD`Argent
                        ExtraMp += 1200;

                    if (ItemId == 2100075) //Troph�D`Or
                        ExtraMp += 1500;

                    if (ItemId == 2100085) //PrixD`Or
                        ExtraMp += 1500;

                    if (ItemId == 2100095) //CoupeD`Or
                        ExtraMp += 1500;
                }
                Pos++;
            }

            mana += ExtraMp;
            return mana;
        }

        public void RemoveWHItem(ulong UID)
        {
            Ready = false;
            int count = 0;
            int whcount = 0;

            foreach (uint[] wh in WHIDs)
            {
                count = 0;

                foreach (uint uid in wh)
                {
                    if (uid == UID)
                    {
                        if (whcount == 0)
                        {
                            byte Do = (byte)(TCWHCount - count - 1);
                            for (int p = 0; p < Do; p++)
                            {
                                TCWH[count + p] = TCWH[count + p + 1];
                                WHIDs[whcount][count + p] = WHIDs[whcount][count + p + 1];
                            }
                            TCWH[TCWHCount - 1] = null;
                            WHIDs[whcount][TCWHCount - 1] = 0;
                            TCWHCount -= 1;
                        }
                        else if (whcount == 1)
                        {
                            byte Do = (byte)(PCWHCount - count - 1);
                            for (int p = 0; p < Do; p++)
                            {
                                PCWH[count + p] = PCWH[count + p + 1];
                                WHIDs[whcount][count + p] = WHIDs[whcount][count + p + 1];
                            }
                            PCWH[PCWHCount - 1] = null;
                            WHIDs[whcount][PCWHCount - 1] = 0;
                            PCWHCount -= 1;
                        }
                        else if (whcount == 2)
                        {
                            byte Do = (byte)(ACWHCount - count - 1);
                            for (int p = 0; p < Do; p++)
                            {
                                ACWH[count + p] = ACWH[count + p + 1];
                                WHIDs[whcount][count + p] = WHIDs[whcount][count + p + 1];
                            }
                            ACWH[ACWHCount - 1] = null;
                            WHIDs[whcount][ACWHCount - 1] = 0;
                            ACWHCount -= 1;
                        }
                        else if (whcount == 3)
                        {
                            byte Do = (byte)(DCWHCount - count - 1);
                            for (int p = 0; p < Do; p++)
                            {
                                DCWH[count + p] = DCWH[count + p + 1];
                                WHIDs[whcount][count + p] = WHIDs[whcount][count + p + 1];
                            }
                            DCWH[DCWHCount - 1] = null;
                            WHIDs[whcount][DCWHCount - 1] = 0;
                            DCWHCount -= 1;
                        }
                        else if (whcount == 4)
                        {
                            byte Do = (byte)(BIWHCount - count - 1);
                            for (int p = 0; p < Do; p++)
                            {
                                BIWH[count + p] = BIWH[count + p + 1];
                                WHIDs[whcount][count + p] = WHIDs[whcount][count + p + 1];
                            }
                            BIWH[BIWHCount - 1] = null;
                            WHIDs[whcount][BIWHCount - 1] = 0;
                            BIWHCount -= 1;
                        }
                        else if (whcount == 5)
                        {
                            byte Do = (byte)(MAWHCount - count - 1);
                            for (int p = 0; p < Do; p++)
                            {
                                MAWH[count + p] = MAWH[count + p + 1];
                                WHIDs[whcount][count + p] = WHIDs[whcount][count + p + 1];
                            }
                            MAWH[MAWHCount - 1] = null;
                            WHIDs[whcount][MAWHCount - 1] = 0;
                            MAWHCount -= 1;
                        }
                    }
                    count++;
                }
                whcount++;
            }


            Ready = true;
        }

        public void AddWHItem(string ItemInf, uint UID, byte WHID)
        {
            if (WHID == 0)
            {
                WHIDs[WHID][TCWHCount] = UID;
                TCWH[TCWHCount] = ItemInf;
                TCWHCount++;
            }
            else if (WHID == 1)
            {
                WHIDs[WHID][PCWHCount] = UID;
                PCWH[PCWHCount] = ItemInf;
                PCWHCount++;
            }
            else if (WHID == 2)
            {
                WHIDs[WHID][ACWHCount] = UID;
                ACWH[ACWHCount] = ItemInf;
                ACWHCount++;
            }
            else if (WHID == 3)
            {
                WHIDs[WHID][DCWHCount] = UID;
                DCWH[DCWHCount] = ItemInf;
                DCWHCount++;
            }
            else if (WHID == 4)
            {
                WHIDs[WHID][BIWHCount] = UID;
                BIWH[BIWHCount] = ItemInf;
                BIWHCount++;
            }
            else if (WHID == 5)
            {
                WHIDs[WHID][MAWHCount] = UID;
                MAWH[MAWHCount] = ItemInf;
                MAWHCount++;
            }
        }

        public void AddItem(string ItemInfo, byte ToPos, uint UID)
        {
            Ready = false;
            string[] Splitter = ItemInfo.Split('-');

            int ItemId;
            byte Plus = 0;
            byte Bless = 0;
            byte Enchant = 0;
            byte Soc1 = 0;
            byte Soc2 = 0;

            ItemId = int.Parse(Splitter[0]);
            Plus = byte.Parse(Splitter[1]);
            Bless = byte.Parse(Splitter[2]);
            Enchant = byte.Parse(Splitter[3]);
            Soc1 = byte.Parse(Splitter[4]);
            Soc2 = byte.Parse(Splitter[5]);


            if (ToPos == 0)
            {
                Inventory[ItemsInInventory] = ItemInfo;
                Inventory_UIDs[ItemsInInventory] = UID;
                ItemsInInventory++;
            }
            else if (ToPos < 10)
            {
                Equips[ToPos] = ItemInfo;
                Equips_UIDs[ToPos] = UID;
                GetEquipStats(ToPos, false);
            }

            MyClient.SendPacket(General.MyPackets.AddItem((long)UID, ItemId, Plus, Bless, Enchant, Soc1, Soc2, ToPos, 100, 100));
            Ready = true;
        }

        public void SendInventory()
        {
            Ready = false;
            string[] Splitter;

            int ItemId;
            byte Plus = 0;
            byte Bless = 0;
            byte Enchant = 0;
            byte Soc1 = 0;
            byte Soc2 = 0;

            int count = 0;
            foreach (string item in Inventory)
            {
                if (item != null)
                {
                    Splitter = item.Split('-');

                    ItemId = int.Parse(Splitter[0]);
                    Plus = byte.Parse(Splitter[1]);
                    Bless = byte.Parse(Splitter[2]);
                    Enchant = byte.Parse(Splitter[3]);
                    Soc1 = byte.Parse(Splitter[4]);
                    Soc2 = byte.Parse(Splitter[5]);

                    MyClient.SendPacket(General.MyPackets.AddItem((long)Inventory_UIDs[count], ItemId, Plus, Bless, Enchant, Soc1, Soc2, 0, 100, 100));
                }
                count++;
            }
            Ready = true;
        }
        public void SendEquips(bool GetStats)
        {
            Ready = false;
            string[] Splitter;

            int ItemId;
            byte Plus = 0;
            byte Bless = 0;
            byte Enchant = 0;
            byte Soc1 = 0;
            byte Soc2 = 0;

            int count = 0;
            foreach (string item in Equips)
            {
                if (item != null)
                {
                    Splitter = item.Split('-');

                    ItemId = int.Parse(Splitter[0]);
                    Plus = byte.Parse(Splitter[1]);
                    Bless = byte.Parse(Splitter[2]);
                    Enchant = byte.Parse(Splitter[3]);
                    Soc1 = byte.Parse(Splitter[4]);
                    Soc2 = byte.Parse(Splitter[5]);

                    MyClient.SendPacket(General.MyPackets.AddItem((long)Equips_UIDs[count], ItemId, Plus, Bless, Enchant, Soc1, Soc2, (byte)count, 100, 100));
                }
                count++;
            }
            if (GetStats)
            {
                GetEquipStats(1, false);
                GetEquipStats(2, false);
                GetEquipStats(3, false);
                GetEquipStats(4, false);
                GetEquipStats(5, false);
                GetEquipStats(6, false);
                GetEquipStats(8, false);
            }
            Ready = true;
        }

        public void PackInventory()
        {
            if (!MyClient.There)
                return;
            Ready = false;
            try
            {
                PackedInventory = "";
                int count = 0;
                foreach (string item in Inventory)
                {
                    if (item != null && item != "")
                    {
                        if (count == 0)
                            PackedInventory += item + "~";
                        else if (count > 0)
                            PackedInventory += "~" + item + "~";
                    }
                    else
                        break;

                }
                if (PackedInventory.Length > 1)
                    PackedInventory = PackedInventory.Remove(PackedInventory.Length - 1, 1);
            }
            catch (Exception Exc) { General.WriteLine(Convert.ToString(Exc)); }
            Ready = true;
        }

        public void UnPackInventory()
        {
            Ready = false;
            if (PackedInventory.Length < 1)
                return;

            string[] Items = PackedInventory.Split('~');

            int count = 0;

            foreach (string item in Items)
            {
                if (item != null)
                    if (item.Length > 1)
                    {
                        AddItem(item, 0, (uint)General.Rand.Next(10000000));
                        count++;
                    }
                    else
                        break;
            }
            Ready = true;
        }

        public void UnPackEquips()
        {
            Ready = false;
            if (PackedEquips.Length < 1)
                return;

            string[] equips = PackedEquips.Split('~');

            int count = 0;

            foreach (string item in equips)
            {
                count++;
                if (item != null)
                    if (item.Length > 1)
                        if (item != "0")
                        {
                            Equips[count] = item;
                            Equips_UIDs[count] = (uint)General.Rand.Next(1000000);
                        }
            }
            Ready = true;
        }

        public void PackEquips()
        {
            if (!MyClient.There)
                return;
            Ready = false;
            PackedEquips = "";

            int count = 0;

            foreach (string item in Equips)
            {
                count++;
                if (item != null)
                {
                    PackedEquips += item + "~";
                }
                else if (count != 1)
                {
                    PackedEquips += "0~";
                }
            }
            if (PackedEquips.Length > 1)
                PackedEquips = PackedEquips.Remove(PackedEquips.Length - 1, 1);
            Ready = true;
        }

        public void UnPackFriends()
        {
            string[] Friendss = PackedFriends.Split('.');

            foreach (string friend in Friendss)
            {
                if (friend != null && friend.Length > 2)
                {
                    string[] Details = friend.Split(':');
                    Friends.Add(uint.Parse(Details[1]), Details[0]);
                    if (World.AllChars.Contains(uint.Parse(Details[1])))
                    {
                        Character Char = (Character)World.AllChars[uint.Parse(Details[1])];
                        if (Char.MyClient.Online)
                        {
                            MyClient.SendPacket(General.MyPackets.FriendEnemyPacket(uint.Parse(Details[1]), Details[0], 15, 1));
                            Char.MyClient.SendPacket(General.MyPackets.FriendEnemyPacket(UID, Name, 14, 0));
                            Char.MyClient.SendPacket(General.MyPackets.FriendEnemyPacket(UID, Name, 15, 1));
                            Char.MyClient.SendPacket(General.MyPackets.SendMsg(Char.MyClient.MessageId, "SYSTEM", Char.Name, "Votre ami " + Name + " viens de se connecter.", 2005));
                        }
                        else
                            MyClient.SendPacket(General.MyPackets.FriendEnemyPacket(uint.Parse(Details[1]), Details[0], 15, 0));
                    }
                    else
                        MyClient.SendPacket(General.MyPackets.FriendEnemyPacket(uint.Parse(Details[1]), Details[0], 15, 0));
                }
            }
        }

        public void PackFriends()
        {
            PackedFriends = "";

            foreach (DictionaryEntry DE in Friends)
            {
                PackedFriends += (string)DE.Value + ":" + (uint)DE.Key + ".";
            }
            if (PackedFriends.Length > 0)
                PackedFriends = PackedFriends.Remove(PackedFriends.Length - 1, 1);
        }

        public void UnPackEnemies()
        {
            string[] Enemiess = PackedEnemies.Split('~');

            foreach (string enemy in Enemiess)
            {
                if (enemy != null && enemy.Length > 2)
                {
                    string[] Details = enemy.Split(':');
                    Enemies.Add(uint.Parse(Details[1]), Details[0]);
                }
            }
        }

        public void PackEnemies()
        {
            PackedEnemies = "";

            foreach (DictionaryEntry DE in Enemies)
            {
                PackedEnemies += (uint)DE.Key + ":" + (string)DE.Value;
            }
            if (PackedEnemies.Length > 0)
                PackedEnemies = PackedEnemies.Remove(PackedEnemies.Length - 1, 1);
        }


        public void PackWarehouses()
        {
            if (!MyClient.There)
                return;

            PackedWHs = "";
            bool Last = false;

            foreach (string item in TCWH)
            {
                if (item != null && item != "")
                {
                    PackedWHs += item + "~";
                    Last = true;
                }
                else
                    break;
            }
            if (Last)
                PackedWHs = PackedWHs.Remove(PackedWHs.Length - 1, 1);
            PackedWHs += ":";

            Last = false;
            foreach (string item in PCWH)
            {
                if (item != null && item != "")
                {
                    PackedWHs += item + "~";
                    Last = true;
                }
                else
                    break;
            }
            if (Last)
                PackedWHs = PackedWHs.Remove(PackedWHs.Length - 1, 1);
            PackedWHs += ":";

            Last = false;
            foreach (string item in ACWH)
            {
                if (item != null && item != "")
                {
                    PackedWHs += item + "~";
                    Last = true;
                }
                else
                    break;
            }
            if (Last)
                PackedWHs = PackedWHs.Remove(PackedWHs.Length - 1, 1);
            PackedWHs += ":";

            Last = false;
            foreach (string item in DCWH)
            {
                if (item != null && item != "")
                {
                    PackedWHs += item + "~";
                    Last = true;
                }
                else
                    break;
            }
            if (Last)
                PackedWHs = PackedWHs.Remove(PackedWHs.Length - 1, 1);
            PackedWHs += ":";

            Last = false;
            foreach (string item in BIWH)
            {
                if (item != null && item != "")
                {
                    PackedWHs += item + "~";
                    Last = true;
                }
                else
                    break;
            }
            if (Last)
                PackedWHs = PackedWHs.Remove(PackedWHs.Length - 1, 1);
            PackedWHs += ":";

            Last = false;
            foreach (string item in MAWH)
            {
                if (item != null && item != "")
                {
                    PackedWHs += item + "~";
                    Last = true;
                }
                else
                    break;
            }
            if (Last)
                PackedWHs = PackedWHs.Remove(PackedWHs.Length - 1, 1);
        }

        public void UnPackWarehouses()
        {
            if (PackedWHs.Length < 1)
                return;

            Ready = false;
            string[] Warehouses = PackedWHs.Split(':');
            byte count = 0;

            foreach (string wh in Warehouses)
            {
                string[] Items = wh.Split('~');

                if (Items.Length < 1)
                    if (wh.Length > 1)
                        AddWHItem(wh, (uint)General.Rand.Next(300000), count);

                foreach (string item in Items)
                {
                    if (item != "")
                    {
                        AddWHItem(item, (uint)General.Rand.Next(300000), count);
                    }
                }
                count++;
            }

            Ready = true;
        }
        public static string[] ShuffleGuildScores()
        {
            string[] ret = new string[5];
            DictionaryEntry[] Vals = new DictionaryEntry[5];

            for (sbyte i = 0; i < 5; i++)
            {
                Vals[i] = new DictionaryEntry();
                Vals[i].Key = (ushort)0;
                Vals[i].Value = (int)0;
            }

            foreach (DictionaryEntry Score in World.GWScores)
            {
                sbyte Pos = -1;
                for (sbyte i = 0; i < 5; i++)
                {
                    if ((int)Score.Value > (int)Vals[i].Value)
                    {
                        Pos = i;
                        break;
                    }
                }
                if (Pos == -1)
                    continue;

                for (sbyte i = 4; i > Pos; i--)
                    Vals[i] = Vals[i - 1];

                Vals[Pos] = Score;
            }

            for (sbyte i = 0; i < 5; i++)
            {
                if ((ushort)Vals[i].Key == 0)
                {
                    ret[i] = "";
                    continue;
                }
                Guild eGuild = (Guild)Guilds.AllGuilds[(ushort)Vals[i].Key];
                ret[i] = "No  " + (i + 1).ToString() + ": " + eGuild.GuildName + "(" + (int)Vals[i].Value + ")";
            }

            return ret;
        }


        public void SendGuildWar()
        {
            string[] g = ShuffleGuildScores();
            byte C = 0;

            foreach (string t in g)
            {
                if (t != "")
                {
                    if (C == 0)
                        MyClient.SendPacket(General.MyPackets.SendMsg2(0, "SYSTEM", "ALLUSERS", t, true));
                    else
                        MyClient.SendPacket(General.MyPackets.SendMsg2(0, "SYSTEM", "ALLUSERS", t, false));
                }
                C++;
            }
        }

        public void Teleport(ushort map, ushort x, ushort y)
        {
            Attacking = false;
            PTarget = null;
            MobTarget = null;
            TGTarget = null;
            if (LocMap != 700 && LocMap != 1036)
            PrevMap = LocMap;
            Ready = false;
            World.RemoveEntity(this);
            LocMap = map;
            LocX = x;
            LocY = y;
            MyClient.SendPacket(General.MyPackets.GeneralData((long)UID, LocMap, LocX, LocY, 74));
            World.SpawnMeToOthers(this, false);
            World.SpawnOthersToMe(this, false);
            World.SurroundNPCs(this, false);
            World.SurroundMobs(this, false);

            if (LocMap == 1038)            
                SendGuildWar();

            foreach (DictionaryEntry DE in World.AllChars)
            {
                Character Chaar = (Character)DE.Value;
                if (Chaar.Name != Name)
                {
                    Chaar.MyClient.SendPacket(General.MyPackets.String(UID, 10, "accession6"));
                }
            }
            MyClient.SendPacket(General.MyPackets.String(UID, 10, "accession6"));

            Ready = true;
        }

        public void UsePortal()
        {
            if (!MyClient.There)
                return;
            Ready = false;
            ushort NewMap = LocMap;
            ushort NewX = (ushort)(LocX + 3);
            ushort NewY = (ushort)(LocY - 2);

            foreach (ushort[] p in DataBase.Portals)
            {
                if (p[0] == LocMap)
                    if (MyMath.PointDistance(LocX, LocY, p[1], p[2]) <= 6)
                    {
                        NewMap = p[3];
                        NewX = p[4];
                        NewY = p[5];
                    }
            }

            Teleport(NewMap, NewX, NewY);
            Ready = true;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;

namespace Play
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            if (Program.Ip != "")
                label8.Text = Program.Ip;

            Socket Login = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            Socket Game = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            Socket Account = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            try
            {
                IPAddress IP = IPAddress.Parse(Program.Ip);
                uint Port = (uint)Program.LoginPort;

                Login.Connect(IP, (int)Port);
                label5.Text = "En Ligne";
                label5.ForeColor = Color.Green;
            }
            catch
            {
                label5.Text = "Hors Ligne";
                label5.ForeColor = Color.Red;
            }

            TcpClient GameServerCheck = new TcpClient();
            try
            {
                GameServerCheck.Connect(Program.Ip, Program.GamePort);

                label6.Text = "En Ligne";
                label6.ForeColor = Color.Green;
            }
            catch
            {
                label6.Text = "Hors Ligne";
                label6.ForeColor = Color.Red;
            }

            TcpClient AccountServerCheck = new TcpClient();
            try
            {
                AccountServerCheck.Connect(Program.Ip, Program.AccountPort);

                label7.Text = "En Ligne";
                label7.ForeColor = Color.Green;
            }
            catch
            {
                label7.Text = "Hors Ligne";
                label7.ForeColor = Color.Red;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
    }
}

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Timers;

namespace COServer_Project
{
    public class World
    {
        public static ArrayList PlayersPraying = new ArrayList(50);
        public static Hashtable ShopHash = new Hashtable();
        public static Hashtable AllChars = new Hashtable();
        public static Hashtable AllMobs = new Hashtable();
        public static Guild PoleHolder;
        public static Hashtable GWScores = new Hashtable();
        //Pkt Map
        public static bool PktOn = false;
        public static Hashtable PktMap1 = new Hashtable();
        public static Hashtable PktMap2 = new Hashtable();
        public static Hashtable PktMap3 = new Hashtable();
        //Dis
        public static bool DisOn = false;
        public static Hashtable DisMap1 = new Hashtable();
        public static Hashtable DisMap2 = new Hashtable();
        public static Hashtable DisMap3 = new Hashtable();
        public static Hashtable DisMap4 = new Hashtable();
        public static ArrayList DisBoss = new ArrayList(25);
        //GW
        public static bool GWOn = false;
        public static bool JailOn = false;
        public static bool LGateDead = false;
        public static bool RGateDead = false;
        //Maintenance
        public static bool SMStart = false;

        public static void LevelUp(Character Player)
        {
            foreach (DictionaryEntry DE in AllChars)
            {
                Character Charr = (Character)DE.Value;

                if (Charr.LocMap == Player.LocMap)
                    if (Charr.MyClient.Online)
                        if (MyMath.CanSee(Player.LocX, Player.LocY, Charr.LocX, Charr.LocY))
                        {
                            Charr.MyClient.SendPacket(General.MyPackets.GeneralData((long)Player.UID, 0, 0, 0, 92));
                        }
            }
        }

        public static void NPCSpawns(SingleNPC NPC)
        {
            foreach (DictionaryEntry DE in AllChars)
            {
                Character Charr = (Character)DE.Value;

                if (NPC.Map == Charr.LocMap)
                    if (Charr.MyClient.Online)
                        if (MyMath.CanSee(NPC.X, NPC.Y, Charr.LocX, Charr.LocY))
                        {
                            if (NPC.Sob == 1)
                                Charr.MyClient.SendPacket(General.MyPackets.SpawnSobNPC(NPC));
                            else if (NPC.Sob == 2)
                                Charr.MyClient.SendPacket(General.MyPackets.SpawnSobNPCNamed(NPC));
                            else
                                Charr.MyClient.SendPacket(General.MyPackets.SpawnSob(NPC));
                        }
            }
        }

        public static void SpawnsNPC(SingleNPC NPC)
        {
            try
            {
                foreach (DictionaryEntry DE in AllChars)
                {
                    Character Charr = (Character)DE.Value;
                    if (MyMath.CanSee(NPC.X, NPC.Y, Charr.LocX, Charr.LocY))
                        if (Charr.MyClient.Online)
                            Charr.MyClient.SendPacket(General.MyPackets.SpawnNPC(NPC));
                }
            }
            catch (Exception Exc) { General.WriteLine(Exc.ToString()); }
        }

        public static void RemoveNPC(SingleNPC NPC)
        {
            try
            {
                foreach (DictionaryEntry DE in AllChars)
                {
                    Character Charr = (Character)DE.Value;
                    if (MyMath.CanSee(NPC.X, NPC.Y, Charr.LocX, Charr.LocY))
                        if (Charr.MyClient.Online)
                            Charr.MyClient.SendPacket(General.MyPackets.GeneralData(NPC.UID, 0, 0, 0, 132));
                }
            }
            catch (Exception Exc) { General.WriteLine(Exc.ToString()); }
        }

        public static void UsingSkill(Character User, short SkillId, byte SkillLvl, uint Target, uint Damage, short X, short Y)
        {
            if (SkillId != 1110 && SkillId != 1100 && SkillId != 1015)
            {
                if (User.LocMap != 1039)
                    User.AddSkillExp(SkillId, Damage);
                else
                    User.AddSkillExp(SkillId, Damage / 10);

                if (SkillId == 1095 || SkillId == 1075 || SkillId == 3080 || SkillId == 1090)
                {
                    if (User.LocMap != 1039)
                        User.AddSkillExp(SkillId, 10);
                    else
                        User.AddSkillExp(SkillId, 1);
                }
            }
            foreach (DictionaryEntry DE in AllChars)
            {
                Character Charr = (Character)DE.Value;
                if (Charr.MyClient.Online)
                    if (User.LocMap == Charr.LocMap)
                        if (MyMath.CanSee(User.LocX, User.LocY, Charr.LocX, Charr.LocY))
                        {
                            if (SkillId == 1002 || SkillId == 1190)
                                Charr.MyClient.SendPacket(General.MyPackets.SkillUse(User, null, null, null, 0, 0, SkillId, SkillLvl, 1, Target, Damage));
                            else
                                Charr.MyClient.SendPacket(General.MyPackets.SkillUse(User, null, null, null, X, Y, SkillId, SkillLvl, 2, Target, Damage));
                        }
            }
        }

        public static void UsingSkill(Character User, Hashtable MobTargets, Hashtable NPCTargets, Hashtable PlTargets, short AimX, short AimY, short SkillId, byte SkillLvl)
        {
            foreach (DictionaryEntry DE in AllChars)
            {
                Character Charr = (Character)DE.Value;
                if (Charr.MyClient.Online)
                    if (User.LocMap == Charr.LocMap)
                        if (MyMath.CanSee(User.LocX, User.LocY, Charr.LocX, Charr.LocY))
                            Charr.MyClient.SendPacket(General.MyPackets.SkillUse(User, MobTargets, PlTargets, NPCTargets, AimX, AimY, SkillId, SkillLvl, 0, 0, 0));

            }
            if (MobTargets.Count < 1 && NPCTargets.Count < 1 && PlTargets.Count < 1)
                return;
            uint GotXP = 0;
            uint GotSkilExp = 0;

            foreach (DictionaryEntry DE in PlTargets)
            {
                Character Player = (Character)DE.Key;
                uint Damage = (uint)DE.Value;

                if (!Other.CanPK(User.LocMap))
                    if (!User.BlueName)
                        if (!Player.BlueName)
                            if (Player.PKPoints < 100)
                                if (!Other.CanPK(Player.LocMap))
                                {
                                    User.GotBlueName = DateTime.Now;
                                    User.BlueName = true;
                                    User.MyClient.SendPacket(General.MyPackets.Vital(User.UID, 26, User.GetStat()));
                                    UpdateSpawn(User);
                                }

                if (Player.GetHitDie(Damage))
                {
                    if (User.PTarget != null)
                        if (Player == User.PTarget)
                        {
                            User.PTarget = null;
                            User.Attacking = false;
                        }
                    PVP(User, Player, 14, Damage);
                    if (!Other.CanPK(User.LocMap))
                        if (!Player.BlueName)
                            if (Player.PKPoints < 100 && !Other.CanPK(Player.LocMap))
                            {
                                User.GotBlueName = DateTime.Now;
                                User.BlueName = true;
                                User.PKPoints += 10;

                                ulong PkXp = Player.Exp / 100;
                                if (PkXp < 0)
                                    PkXp = Player.Exp;
                                Player.Exp -= PkXp;
                                User.AddExp(PkXp, false);
                                User.MyClient.SendPacket(General.MyPackets.Vital((long)User.UID, 5, User.Exp));
                                Player.MyClient.SendPacket(General.MyPackets.Vital((long)Player.UID, 5, Player.Exp));

                                User.MyClient.SendPacket(General.MyPackets.Vital(User.UID, 6, User.PKPoints));
                                if ((User.PKPoints > 29 && User.PKPoints - 10 < 30) || (User.PKPoints > 99 && User.PKPoints - 10 < 100))
                                {
                                    User.MyClient.SendPacket(General.MyPackets.Vital(User.UID, 26, User.GetStat()));
                                    UpdateSpawn(User);
                                }
                            }
                }

            }

            foreach (DictionaryEntry DE in NPCTargets)
            {
                SingleNPC TGO = (SingleNPC)DE.Key;
                uint Damage = (uint)DE.Value;

                uint TGOHP = TGO.CurHP;
                double ExpQuality = 1;

                if (TGO.Level + 5 < User.Level)
                    ExpQuality = 0.1;

                if (TGO.GetDamageDie(Damage, User))
                {
                    GotXP += (uint)(TGOHP * ExpQuality / 10);
                    if (SkillId != 5030)
                        GotSkilExp += (uint)(TGOHP / 100);
                    else
                        GotSkilExp += (uint)(5 * (SkillLvl + 1)); ;
                }
                else
                {
                    GotXP += (uint)(Damage * ExpQuality / 10);
                    if (SkillId != 5030)
                        GotSkilExp += (uint)(Damage / 100);
                    else
                        GotSkilExp += (uint)(5 * (SkillLvl + 1)); ;
                }
            }

            foreach (DictionaryEntry DE in MobTargets)
            {
                SingleMob Mob = (SingleMob)DE.Key;

                uint Damage = (uint)DE.Value;

                double ExpQuality = 0;

                if (Mob.Level + 4 < User.Level)
                    ExpQuality = 0.1;
                if (Mob.Level + 4 >= User.Level)
                    ExpQuality = 1;
                if (Mob.Level >= User.Level)
                    ExpQuality = 1.1;
                if (Mob.Level - 4 > User.Level)
                    ExpQuality = 1.3;

                uint MobCurHP = Mob.CurHP;
                double EAddExp = 0;
                uint UAddExp = 0;

                if (Mob.GetDamage(Damage))
                {
                    if (User.CycloneOn || User.SMOn)
                    {
                        User.KO++;
                        User.MyClient.SendPacket(General.MyPackets.SendMsg(User.MyClient.MessageId, "SYSTEM", User.Name, "Kills:" + User.KO, 2005));
                    }
                    if (Mob.Name == User.QuestMob)
                    {
                        User.QuestKO++;
                        if (User.QuestKO >= 300 && User.QuestFrom != "NewYear")
                        {
                            User.MyClient.SendPacket(General.MyPackets.SendMsg(User.MyClient.MessageId, "SYSTEM", User.Name, "Vous avez tu� assez de monstre pour la qu�te. Retourner voir " + User.QuestFrom + "Capitaine.", 2005));
                        }
                        if (User.QuestFrom == "NewYear")
                        {
                            User.MyClient.SendPacket(General.MyPackets.SendMsg(User.MyClient.MessageId, "SYSTEM", User.Name, "Quest Kills:" + User.QuestKO + "/300", 2005));
                            if (User.QuestKO >= 300)
                            {
                                User.Teleport(1002, 430, 380);
                                User.QuestKO = 0;
                                User.QuestMob = "";
                                User.QuestFrom = "";
                                if (!User.InventoryContains(721636, 1))
                                    User.AddItem("721636-0-0-0-0-0", 0, (uint)General.Rand.Next(36457836));
                            }
                        }
                    }
                    if (User.LocMap == 2022)
                    {
                        ushort DisRequest = 0;

                        if (User.Job > 9 && User.Job < 16)
                            DisRequest = 800;
                        if (User.Job > 19 && User.Job < 26)
                            DisRequest = 900;
                        if (User.Job > 39 && User.Job < 46)
                            DisRequest = 1300;
                        if (User.Job > 129 && User.Job < 136)
                            DisRequest = 600;
                        if (User.Job > 139 && User.Job < 146)
                            DisRequest = 1000;

                        if (Mob.Name == "TrollInfernal")
                        {
                            User.DisKO += 3;
                            User.MyClient.SendPacket(General.MyPackets.SendMsg(User.MyClient.MessageId, "SYSTEM", User.Name, "Dis Kills:" + User.DisKO + "/" + DisRequest, 2005));
                        }
                        else
                        {
                            User.DisKO++;
                            User.MyClient.SendPacket(General.MyPackets.SendMsg(User.MyClient.MessageId, "SYSTEM", User.Name, "Dis Kills:" + User.DisKO + "/" + DisRequest, 2005));
                        }

                        if (User.DisKO >= DisRequest && User.Job > 9 && User.Job < 16)
                            User.MyClient.SendPacket(General.MyPackets.SendMsg(User.MyClient.MessageId, "SYSTEM", User.Name, "Vous avez tu� assez de monstre pour la qu�te.", 2011));
                        if (User.DisKO >= DisRequest && User.Job > 19 && User.Job < 26)
                            User.MyClient.SendPacket(General.MyPackets.SendMsg(User.MyClient.MessageId, "SYSTEM", User.Name, "Vous avez tu� assez de monstre pour la qu�te.", 2011));
                        if (User.DisKO >= DisRequest && User.Job > 39 && User.Job < 46)
                            User.MyClient.SendPacket(General.MyPackets.SendMsg(User.MyClient.MessageId, "SYSTEM", User.Name, "Vous avez tu� assez de monstre pour la qu�te.", 2011));
                        if (User.DisKO >= DisRequest && User.Job > 139 && User.Job < 146)
                            User.MyClient.SendPacket(General.MyPackets.SendMsg(User.MyClient.MessageId, "SYSTEM", User.Name, "Vous avez tu� assez de monstre pour la qu�te.", 2011));
                        if (User.DisKO >= DisRequest && User.Job > 129 && User.Job < 136)
                            User.MyClient.SendPacket(General.MyPackets.SendMsg(User.MyClient.MessageId, "SYSTEM", User.Name, "Vous avez tu� assez de monstre pour la qu�te.", 2011));
                    }
                    if (Other.ChanceSuccess(8))
                    {
                        User.CPs += 3;
                        User.MyClient.SendPacket(General.MyPackets.Vital(User.UID, 30, User.CPs));
                    }
                    if (Other.ChanceSuccess(9))
                    {
                        User.CPs += 5;
                        User.MyClient.SendPacket(General.MyPackets.Vital(User.UID, 30, User.CPs));
                    }
                    if (Other.ChanceSuccess(10))
                    {
                        User.CPs += 10;
                        User.MyClient.SendPacket(General.MyPackets.Vital(User.UID, 30, User.CPs));
                    }
                    if (Mob.Name == "PlutoFinal")
                    {
                        User.AddItem("790001-0-0-0-0-0", 0, (uint)General.Rand.Next(345636635));
                        foreach (DictionaryEntry DI in AllChars)
                        {
                            User.Teleport(1020, 531, 482);
                            User.MyClient.CurrentNPC = 1315;
                            User.MyClient.SendPacket(General.MyPackets.NPCSay("Vous avez vaincu PlutoFinal, pour vous remercier je vais vous donner un grand cadeau."));
                            User.MyClient.SendPacket(General.MyPackets.NPCLink("Merci!", 1));
                            User.MyClient.SendPacket(General.MyPackets.NPCSetFace(6));
                            User.MyClient.SendPacket(General.MyPackets.NPCFinish());

                            Character Chaar = (Character)DI.Value;
                            if (Chaar.Name != User.Name)
                            {
                                if (Chaar.LocMap == 2021 || Chaar.LocMap == 2022 || Chaar.LocMap == 2023 || Chaar.LocMap == 2024)
                                {
                                    Chaar.Teleport(1002, 430, 380);
                                }
                            }
                        }
                        World.SendMsgToAll(User.Name + " des " + User.MyGuild.GuildName + " a d�truit PlutoFinal! Les joueurs ont �t� t�l�port�s pour leurs protection.", "SYSTEM", 2011);
                    }
                    if (User.Level >= 70)
                    {
                        foreach (Character Member in User.Team)
                        {
                            if (!Member.Alive)
                                continue;
                            if (User.LocMap != 1767 && Member.LocMap != 1767)
                            {
                                double PlvlExp = 1;
                                double WatXP = 1;
                                double ExpPot = 1;
                                double MarriageXP = 1;
                                if (Member.Job == 133 || Member.Job == 134 || Member.Job == 135)
                                    WatXP = 2;
                                if (Member.Spouse == User.Spouse)
                                    MarriageXP = 2;
                                if (User.MobTarget.Level - 20 <= Member.Level)
                                    PlvlExp = 0.1;
                                if (Member.dexp == 1)
                                    ExpPot = 2;
                                if (Member != null)
                                    if (Member.Level + 20 < User.Level)
                                        if (Member.LocMap == User.LocMap)
                                            if (MyMath.PointDistance(Member.LocX, Member.LocY, User.LocX, User.LocY) < 30)
                                                if (Member.AddExp((ulong)(((((double)(52 + (Member.Level * 30)) * PlvlExp) * WatXP) * ExpPot) * MarriageXP), true))
                                                {
                                                    double MsgXp = 0;
                                                    MsgXp = (((((double)(52 + (Member.Level * 30)) * PlvlExp) * WatXP) * ExpPot) * MarriageXP);
                                                    Member.MyClient.SendPacket(General.MyPackets.SendMsg(Member.MyClient.MessageId, "SYSTEM", Member.Name, Member.Name + " a gagn� " + MsgXp + " points d'exp�rience.", 2005));
                                                    User.VP += (uint)((Member.Level * 17 / 13 * 12 / 2) + Member.Level * 3);
                                                    Member.MyTeamLeader.MyClient.SendPacket(General.MyPackets.SendMsg(Member.MyClient.MessageId, "SYSTEM", Member.Name, User.Name + " a gagn� " + ((Member.Level * 17 / 13 * 12 / 2) + Member.Level * 3) + " points de vertu.", 2003));
                                                    foreach (Character Member2 in User.Team)
                                                    {
                                                        if (Member2 != null)
                                                            Member2.MyClient.SendPacket(General.MyPackets.SendMsg(Member2.MyClient.MessageId, "SYSTEM", Member2.Name, User.Name + " a gagn�" + ((Member.Level * 17 / 13 * 12 / 2) + Member.Level * 3) + " points de vertu.", 2003));
                                                    }
                                                }
                            }
                        }
                    }
                    if (User.MobTarget == Mob)
                    {
                        User.TargetUID = 0;
                        User.MobTarget = null;
                        User.Attacking = false;
                    }
                    User.XpCircle++;
                    if (User.CycloneOn || User.SMOn)
                        User.ExtraXP += 820;
                    if (User.MobTarget != null)
                        if (Mob == User.MobTarget)
                        {
                            User.MobTarget = null;
                        }

                    EAddExp = MobCurHP * ExpQuality;
                    UAddExp = Convert.ToUInt32(EAddExp);
                    GotXP += UAddExp;
                    if (SkillId != 5030)
                        GotSkilExp += (uint)((double)MobCurHP * ExpQuality);
                    else
                        GotSkilExp += (uint)(5 * (SkillLvl + 1));

                    AttackMob(User, Mob, 14, 0);

                    MobDissappear(Mob);
                    Mob.Death = DateTime.Now;
                }
                else
                {
                    EAddExp = Damage * ExpQuality;
                    UAddExp = Convert.ToUInt32(EAddExp);
                    GotXP += UAddExp;
                    if (SkillId != 5030)
                        GotSkilExp += (uint)((double)Damage * ExpQuality);
                    else
                        GotSkilExp += (uint)(5 * (SkillLvl + 1));
                }
            }
            User.AddExp((ulong)GotXP, true);
            User.AddSkillExp((short)SkillId, GotSkilExp);
        }

        public static void SpawnMobForPlayers(SingleMob Mob, bool Check)
        {
            foreach (DictionaryEntry DE in AllChars)
            {
                Character Charr = (Character)DE.Value;

                if (Mob.Map == Charr.LocMap)
                    if (Charr.MyClient.Online)
                        if (!MyMath.CanSee(Mob.PrevX, Mob.PrevY, Charr.LocX, Charr.LocY) || Check == false)
                            if (MyMath.CanSee(Mob.PosX, Mob.PosY, Charr.LocX, Charr.LocY))
                            {
                                Charr.MyClient.SendPacket(General.MyPackets.SpawnMob(Mob));
                            }
            }
        }

        public static void MobMoves(SingleMob Mob, byte Dir)
        {
            try
            {
                foreach (DictionaryEntry DE in AllChars)
                {
                    Character Charr = (Character)DE.Value;

                    if (Mob.Map == Charr.LocMap)
                        if (Charr.MyClient.Online)
                            if (MyMath.CanSeeBig(Mob.PosX, Mob.PosY, Charr.LocX, Charr.LocY))
                            {
                                Charr.MyClient.SendPacket(General.MyPackets.MobMoves((uint)Mob.UID, Dir));
                            }
                }
            }
            catch { }
        }


        public static void ItemDrops(DroppedItem item)
        {
            try
            {
                foreach (DictionaryEntry DE in AllChars)
                {
                    Character Charr = (Character)DE.Value;

                    if (item.Map == Charr.LocMap)
                        if (Charr.MyClient.Online)
                            if (MyMath.CanSee(item.X, item.Y, Charr.LocX, Charr.LocY))
                                Charr.MyClient.SendPacket(General.MyPackets.ItemDrop(item.UID, item.ItemId, item.X, item.Y));
                }
            }
            catch { }
        }

        public static void ItemDissappears(DroppedItem item)
        {
            try
            {
                foreach (DictionaryEntry DE in AllChars)
                {
                    Character Charr = (Character)DE.Value;

                    if (item.Map == Charr.LocMap)
                        if (Charr.MyClient.Online)
                            if (MyMath.CanSee(item.X, item.Y, Charr.LocX, Charr.LocY))
                            {
                                Charr.MyClient.SendPacket(General.MyPackets.ItemDropRemove(item.UID));
                            }
                }
            }
            catch { }
        }

        public static void SurroundDroppedItems(Character Me, bool Check)
        {
            foreach (DictionaryEntry DE in DroppedItems.AllDroppedItems)
            {
                DroppedItem item = (DroppedItem)DE.Value;

                if (item.Map == Me.LocMap)
                    if (MyMath.CanSee(Me.LocX, Me.LocY, item.X, item.Y))
                        if (!MyMath.CanSee(Me.PrevX, Me.PrevY, item.X, item.Y) || Check == false)
                        {
                            Me.MyClient.SendPacket(General.MyPackets.ItemDrop(item.UID, item.ItemId, item.X, item.Y));
                        }
            }
        }

        public static void MobReSpawn(SingleMob Mob)
        {
            foreach (DictionaryEntry DE in AllChars)
            {
                Character Charr = (Character)DE.Value;

                if (Charr.MyClient.Online)
                    if (Mob.Map == Charr.LocMap)
                        if (MyMath.CanSee(Mob.PosX, Mob.PosY, Charr.LocX, Charr.LocY))
                        {
                            if (Mob.Map == 1767 || Mob.Map == 2021 || Mob.Map == 2022 || Mob.Map == 2023 || Mob.Map == 2024)
                            {
                                Charr.MyClient.SendPacket(General.MyPackets.SpawnMob(Mob));
                                Charr.MyClient.SendPacket(General.MyPackets.String(Mob.UID, 10, "MBGhost"));
                            }
                            else
                            {
                                Charr.MyClient.SendPacket(General.MyPackets.SpawnMob(Mob));
                                Charr.MyClient.SendPacket(General.MyPackets.String(Mob.UID, 10, "MBStandard"));
                            }
                        }
            }
        }

        public static void MobDissappear(SingleMob Mob)
        {
            foreach (DictionaryEntry DE in AllChars)
            {
                Character Charr = (Character)DE.Value;

                if (Mob.Map == Charr.LocMap)
                    if (Charr.MyClient.Online)
                        if (MyMath.CanSeeBig(Mob.PosX, Mob.PosY, Charr.LocX, Charr.LocY))
                        {
                            Charr.MyClient.SendPacket(General.MyPackets.MobFade(Mob.UID));
                        }
            }
        }

        public static void GuardReSpawn(SingleMob Mob)
        {
            lock (AllChars)
            {
                foreach (DictionaryEntry DE in AllChars)
                {
                    Character Charr = (Character)DE.Value;

                    if (Charr.MyClient.Online)
                        if (Mob.Map == Charr.LocMap)
                            if (MyMath.CanSee(Mob.PosX, Mob.PosY, Charr.LocX, Charr.LocY))
                            {
                                Charr.MyClient.SendPacket(General.MyPackets.SpawnMob(Mob));
                            }
                }
            }
        }

        public static void MobAttacksCharSkill(SingleMob Mob, Character Attacked, uint Dmg, ushort SkillID, byte SkillLvl)
        {
            foreach (DictionaryEntry DE in AllChars)
            {
                Character Charr = (Character)DE.Value;

                if (Charr.MyClient.Online)
                    if (MyMath.CanSeeBig(Attacked.LocX, Attacked.LocY, Charr.LocX, Charr.LocY))
                    {
                        Charr.MyClient.SendPacket(General.MyPackets.MobSkillUse(Mob, Attacked, Dmg, SkillID, SkillLvl));
                    }
            }
        }

        public static void MobAttacksCharSkill(SingleMob Mob, Character Attacked, ushort SkillID, byte SkillLvl)
        {
            foreach (DictionaryEntry DE in AllChars)
            {
                Character Charr = (Character)DE.Value;

                if (Charr.MyClient.Online)
                    if (MyMath.CanSeeBig(Attacked.LocX, Attacked.LocY, Charr.LocX, Charr.LocY))
                    {
                        Charr.MyClient.SendPacket(General.MyPackets.MobSkillUse(Mob, Attacked, SkillID, SkillLvl));
                    }
            }
        }

        public static void MobAttacksChar(SingleMob Mob, Character Attacked, byte AtkType, uint Dmg)
        {
            if (Mob == null)
                return;
            foreach (DictionaryEntry DE in AllChars)
            {
                Character Charr = (Character)DE.Value;

                if (Charr.MyClient.Online)
                    if (MyMath.CanSee(Attacked.LocX, Attacked.LocY, Charr.LocX, Charr.LocY))
                        Charr.MyClient.SendPacket(General.MyPackets.Attack(Mob.UID, Attacked.UID, (short)Attacked.LocX, (short)Attacked.LocY, AtkType, Dmg));

            }
        }

        public static void PlAttacksTG(SingleNPC Npc, Character Me, byte AtkType, uint Dmg)
        {
            if (Npc == null)
                return;

            foreach (DictionaryEntry DE in AllChars)
            {
                Character Charr = (Character)DE.Value;

                if (Charr.MyClient.Online)
                    if (MyMath.CanSee(Me.LocX, Me.LocY, Charr.LocX, Charr.LocY))
                        Charr.MyClient.SendPacket(General.MyPackets.Attack(Me.UID, Npc.UID, Npc.X, Npc.Y, AtkType, Dmg));

            }
        }

        public static void PVP(Character Me, Character Attacked, byte AtkType, uint Dmg)
        {
            if (Attacked == null)
                return;
            foreach (DictionaryEntry DE in AllChars)
            {
                Character Charr = (Character)DE.Value;

                if (MyMath.CanSeeBig(Me.LocX, Me.LocY, Charr.LocX, Charr.LocY))
                    if (Charr.MyClient.Online)
                    {
                        Charr.MyClient.SendPacket(General.MyPackets.Attack(Me.UID, Attacked.UID, (short)Attacked.LocX, (short)Attacked.LocY, AtkType, Dmg));
                    }
            }
        }
        public static void AttackMiss(Character Me, byte AtkType, short X, short Y)
        {
            foreach (DictionaryEntry DE in AllChars)
            {
                Character Charr = (Character)DE.Value;

                if (Charr != null)
                    if (MyMath.CanSee(Me.LocX, Me.LocY, Charr.LocX, Charr.LocY))
                        if (Charr.MyClient.Online)
                        {
                            Charr.MyClient.SendPacket(General.MyPackets.Attack(Me.UID, 0, X, Y, AtkType, 0));
                        }
            }
        }
        public static void AttackMob(Character Me, SingleMob Mob, byte AtkType, uint Dmg)
        {
            foreach (DictionaryEntry DE in AllChars)
            {
                Character Charr = (Character)DE.Value;

                if (Charr != null)
                    if (MyMath.CanSeeBig(Me.LocX, Me.LocY, Charr.LocX, Charr.LocY))
                        if (Charr.MyClient.Online)
                        {
                            Charr.MyClient.SendPacket(General.MyPackets.Attack(Me.UID, Mob.UID, Mob.PosX, Mob.PosY, AtkType, Dmg));
                        }
            }
        }

        public static void UpdateSpawn(Character Me)
        {
            foreach (DictionaryEntry DE in AllChars)
            {
                Character Charr = (Character)DE.Value;

                if (Me.LocMap == Charr.LocMap)
                    if (MyMath.CanSee(Me.LocX, Me.LocY, Charr.LocX, Charr.LocY))
                    {
                        if (Me.MyGuild != null)
                            Charr.MyClient.SendPacket(General.MyPackets.GuildName(Me.GuildID, Me.MyGuild.GuildName));
                        Charr.MyClient.SendPacket(General.MyPackets.SpawnEntity(Me));

                        if (Me.MyGuild != null)
                            Charr.MyClient.SendPacket(General.MyPackets.GuildName(Me.GuildID, Me.MyGuild.GuildName));
                    }
            }
        }

        public static void SurroundMobs(Character Me, bool Check)
        {
            foreach (DictionaryEntry DE in Mobs.AllMobs)
            {
                SingleMob Mob = (SingleMob)DE.Value;

                if (Me.LocMap == Mob.Map)
                    if (Mob.Alive)
                        if (MyMath.CanSee(Me.LocX, Me.LocY, Mob.PosX, Mob.PosY))
                            if (!MyMath.CanSee(Me.PrevX, Me.PrevY, Mob.PosX, Mob.PosY) || Check == false)
                            {
                                Me.MyClient.SendPacket(General.MyPackets.SpawnMob(Mob));
                            }
            }
        }

        public static void SurroundNPCs(Character Me, bool Check)
        {
            foreach (DictionaryEntry DE in NPCs.AllNPCs)
            {
                SingleNPC Npc = (SingleNPC)DE.Value;

                if (Me.LocMap == Npc.Map)
                    if (MyMath.CanSee(Me.LocX, Me.LocY, Npc.X, Npc.Y))
                        if (!MyMath.CanSee(Me.PrevX, Me.PrevY, Npc.X, Npc.Y) || Check == false)
                        {
                            if (Npc.Sob == 0)
                                Me.MyClient.SendPacket(General.MyPackets.SpawnNPC(Npc));
                            else if (Npc.Sob == 1)
                                Me.MyClient.SendPacket(General.MyPackets.SpawnSobNPC(Npc));
                            else if (Npc.Sob == 2)
                                Me.MyClient.SendPacket(General.MyPackets.SpawnSobNPCNamed(Npc));
                            else if (Npc.Sob == 3)
                                Me.MyClient.SendPacket(General.MyPackets.SpawnSob(Npc));
                            else if (Npc.Sob == 4)
                                Me.MyClient.SendPacket(General.MyPackets.SpawnShopFlag(Npc));
                            else
                                Me.MyClient.SendPacket(General.MyPackets.SpawnSob(Npc));
                        }
            }
        }

        public static void SendMsgToAll(string Message, string From, short MsgType)
        {
            foreach (DictionaryEntry DE in AllChars)
            {
                Character Charr = (Character)DE.Value;
                if (Charr.MyClient.Online)
                    Charr.MyClient.SendPacket(General.MyPackets.SendMsg(Charr.MyClient.MessageId, From, "All", Message, MsgType));
            }
        }

        public static void SendMsgToChar(string Message, string From, short MsgType, string For)
        {
            foreach (DictionaryEntry DE in AllChars)
            {
                Character Charr = (Character)DE.Value;
                if (Charr.Name == For)
                    if (Charr.MyClient.Online)
                        Charr.MyClient.SendPacket(General.MyPackets.SendMsg(Charr.MyClient.MessageId, From, "All", Message, MsgType));
            }
        }


        public static void Chat(Character Char, short ChatType, byte[] Data, string To, string Message)
        {
            /*            
            Talk = 0x7d0, 2000
            Whisper = 0x7d1, 2001
            Action = 0x7d2, 2002
            Team = 0x7d3, 2003
            Guild = 0x7d4, 2004
            Top = 0x7d5, 2005
            Spouse = 0x7d6, 2006
            Yell = 0x7d8, 2008
            Friend = 0x7d9, 2009
            Broadcast = 0x7da, 2010
            Center = 0x7db, 2011
            Ghost = 0x7dd, 2013
            Service = 0x7de, 2014
            LoginInformation = 0x835, 2101
            VendorHawk = 0x838, 2104
            FriendsOfflineMessage = 0x83e, 2110
            GuildBulletin = 0x83f, 2111
            TradeBoard = 0x899, 2201
            FriendBoard = 0x89a, 2202
            TeamBoard = 0x89b, 2203
            GuildBoard = 0x89c, 2204
            OthersBoard = 0x89d, 2205
            */
            if (ChatType == 2010)
            {
                if (Char.CPs >= 5)
                {
                    Char.CPs -= 5;
                    Char.MyClient.SendPacket(General.MyPackets.Vital(Char.UID, 30, Char.CPs));
                    SendMsgToAll(Message, Char.Name, 2010);
                }
            }
            if (ChatType == 2004)
            {
                Char.MyGuild.GuildMessage(Char, Message, To);
            }
            if (ChatType == 2009)
            {
                foreach (DictionaryEntry DE in Char.Friends)
                {
                    uint FriendID = (uint)DE.Key;
                    if (AllChars.Contains(FriendID))
                    {
                        Character Friend = (Character)AllChars[FriendID];
                        if (Friend != null)
                        {
                            Friend.MyClient.SendPacket(Data);
                        }
                    }
                }
            }
            if (ChatType == 2003)
            {
                foreach (Character Member in Char.Team)
                {
                    if (Member != null)
                    {
                        Member.MyClient.SendPacket(Data);
                    }
                }
                if (!Char.TeamLeader && Char.MyTeamLeader != null)
                    Char.MyTeamLeader.MyClient.SendPacket(Data);
            }
            if (ChatType == 2000)
            {
                foreach (DictionaryEntry DE in AllChars)
                {
                    Character Charr = (Character)DE.Value;
                    if (Char.LocMap == Charr.LocMap)
                        if (Char != Charr)
                            if (MyMath.PointDistance(Charr.LocX, Charr.LocY, Char.LocX, Char.LocY) < 16)
                                Charr.MyClient.SendPacket(Data);
                }
            }
            if (ChatType == 2001)
            {
                bool Sent = false;
                foreach (DictionaryEntry DE in AllChars)
                {
                    Character Charr = (Character)DE.Value;
                    if (Charr.Name == To)
                    {
                        Charr.MyClient.SendPacket(Data);
                        Sent = true;
                    }

                }
                if (!Sent)
                    Char.MyClient.SendPacket(General.MyPackets.SendMsg(Char.MyClient.MessageId, "SYSTEM", Char.Name, "The character is offline at the moment.", 2000));
            }

        }

        public static void RemoveEntity(Character Who)
        {
            try
            {
                foreach (DictionaryEntry DE in AllChars)
                {
                    Character Charr = (Character)DE.Value;

                    if (Charr != Who)
                        if (Charr.MyClient.Online)
                            if (MyMath.CanSee(Who.LocX, Who.LocY, Charr.LocX, Charr.LocY))
                                Charr.MyClient.SendPacket(General.MyPackets.GeneralData(Who.UID, 0, 0, 0, 132));

                }
            }
            catch (Exception Exc) { General.WriteLine(Exc.ToString()); }
        }

        public static void RemoveEntity(SingleMob Who)
        {
            try
            {
                foreach (DictionaryEntry DE in AllChars)
                {
                    Character Charr = (Character)DE.Value;
                    if (Charr.MyClient.Online)
                        if (MyMath.CanSeeBig(Who.PosX, Who.PosY, Charr.LocX, Charr.LocY))
                            Charr.MyClient.SendPacket(General.MyPackets.GeneralData(Who.UID, 0, 0, 0, 132));
                }
            }
            catch (Exception Exc) { General.WriteLine(Exc.ToString()); }
        }

        public static void RemoveEntity(SingleNPC Who)
        {
            try
            {
                foreach (DictionaryEntry DE in AllChars)
                {
                    Character Charr = (Character)DE.Value;
                    if (MyMath.CanSee(Who.X, Who.Y, Charr.LocX, Charr.LocY))
                        if (Charr.MyClient.Online)
                            Charr.MyClient.SendPacket(General.MyPackets.GeneralData(Who.UID, 0, 0, 0, 132));


                } // con.SystemChat("You Have Won A " + ItemName, ChatType.Top, 0x00FF00);
            }
            catch (Exception Exc) { General.WriteLine(Exc.ToString()); }
        }

        public static void PlayersOffLottery()
        {
            try
            {
                foreach (DictionaryEntry DE in AllChars)
                {
                    Character Charr = (Character)DE.Value;
                    if (Charr.LocMap == 700)
                    {
                        Charr.CPs += 27;
                        Charr.MyClient.SendPacket(General.MyPackets.Vital(Charr.UID, 30, Charr.CPs));
                        Charr.Teleport(1036, 200, 200);


                    }
                }
            }
            catch (Exception Exc) { General.WriteLine(Convert.ToString(Exc)); }
        }

        public static void SaveAllChars()
        {
            try
            {
                foreach (DictionaryEntry DE in AllChars)
                {
                    Character Charr = (Character)DE.Value;
                    DataBase.SaveChar(Charr);
                }
                Guilds.SaveAllGuilds();
            }
            catch (Exception Exc) { General.WriteLine(Convert.ToString(Exc)); }
        }

        public static void PlayerMoves(Character MovingChar, byte[] Data)
        {
            try
            {
                foreach (DictionaryEntry DE in AllChars)
                {
                    Character Charr = (Character)DE.Value;

                    if (Charr.LocMap == MovingChar.LocMap)
                        if (MyMath.CanSeeBig(Charr.LocX, Charr.LocY, MovingChar.LocX, MovingChar.LocY))
                            Charr.MyClient.SendPacket(Data);
                }
            }
            catch (Exception Exc) { General.WriteLine(Convert.ToString(Exc)); }
        }

        public static void SpawnMeToOthers(Character Me, bool Check)
        {
            try
            {
                if (AllChars.Contains(Me.UID))
                {
                    foreach (DictionaryEntry DE in AllChars)
                    {
                        Character SpawnTo = (Character)DE.Value;

                        if (Me != SpawnTo)
                            if (SpawnTo.MyClient.Online)
                                if (Me.LocMap == SpawnTo.LocMap)
                                    if (MyMath.CanSee(Me.LocX, Me.LocY, SpawnTo.LocX, SpawnTo.LocY))
                                        if (!MyMath.CanSee(Me.PrevX, Me.PrevY, SpawnTo.LocX, SpawnTo.LocY) || !Check)
                                        {
                                            if (Me.MyGuild != null)
                                                SpawnTo.MyClient.SendPacket(General.MyPackets.GuildName(Me.GuildID, Me.MyGuild.GuildName));

                                            SpawnTo.MyClient.SendPacket(General.MyPackets.SpawnEntity(Me));

                                            if (Me.MyGuild != null)
                                                SpawnTo.MyClient.SendPacket(General.MyPackets.GuildName(Me.GuildID, Me.MyGuild.GuildName));
                                        }

                    }
                }
            }
            catch (Exception Exc) { General.WriteLine(Convert.ToString(Exc)); }
        }
        public static void SpawnOthersToMe(Character Me, bool Check)
        {
            try
            {
                if (AllChars.Contains(Me.UID))
                {
                    foreach (DictionaryEntry DE in AllChars)
                    {
                        Character SpawnWho = (Character)DE.Value;

                        if (Me != SpawnWho)
                            if (SpawnWho.MyClient.Online)
                                if (Me.LocMap == SpawnWho.LocMap)
                                    if (MyMath.CanSee(Me.LocX, Me.LocY, SpawnWho.LocX, SpawnWho.LocY))
                                        if (!MyMath.CanSee(Me.PrevX, Me.PrevY, SpawnWho.LocX, SpawnWho.LocY) || Check == false)
                                        {
                                            if (SpawnWho.MyGuild != null)
                                                Me.MyClient.SendPacket(General.MyPackets.GuildName(SpawnWho.GuildID, SpawnWho.MyGuild.GuildName));

                                            Me.MyClient.SendPacket(General.MyPackets.SpawnEntity(SpawnWho));

                                            if (SpawnWho.MyGuild != null)
                                                Me.MyClient.SendPacket(General.MyPackets.GuildName(SpawnWho.GuildID, SpawnWho.MyGuild.GuildName));
                                        }
                    }
                }
            }
            catch (Exception Exc) { General.WriteLine(Convert.ToString(Exc)); }
        }
    }
}
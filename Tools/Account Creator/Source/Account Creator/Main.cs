﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Diagnostics;
using System.Threading;
using System.Runtime.InteropServices;

namespace WindowsFormsApplication1
{
    public partial class Main : Form
    {
        private Thread Thread_Musique = null;
        private string sMusiqueFond = Application.StartupPath.ToString() + "\\Moondance.wav";
        [DllImport("winmm.dll")]
        private static extern int PlaySound
        (
            string pszSound,
            int hmod,
            int flags
         );

        public Main()
        {
            InitializeComponent();
            Program.Ip = textBox1.Text;
            //Thread_Musique = new Thread(new ThreadStart(PlayMusiqueThread));
            //Thread_Musique.Start();
        }

        private void PlayMusiqueThread()
        {
            {
                PlaySound(sMusiqueFond, 0, 1);
            }
        } 

        private void button1_Click(object sender, EventArgs e)
        {
            TcpClient AccountServerCheck = new TcpClient();
            int AccountPort = 9956;
            try
            {
                AccountServerCheck.Connect(Program.Ip, AccountPort);

                this.Invoke((MethodInvoker)delegate
                {
                    this.label8.Text = "En Ligne";
                    this.label8.ForeColor = Color.Green;
                    Program.AccountOn = true;
                });
            }
            catch
            {
                this.Invoke((MethodInvoker)delegate
                {
                    this.label8.Text = "Hors-Ligne";
                    this.label8.ForeColor = Color.Red;
                    Program.AccountOn = false;
                });
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (Program.AccountOn == true)
            {
                if (Program.Username != "" && Program.RealName != "" && Program.Email != "" && Program.Question != "" && Program.Answer != "")
                {
                    Program.Account();
                    Program.FileExist();
                    if (Program.AccountUse == false)
                    {
                        Program.UploadAccount();
                        File.Delete(Program.Username + ".acc");
                        MessageBox.Show("Votre compte a été créé!", "Compte Créé!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    else
                    {
                        File.Delete(Program.Username + ".acc");
                        MessageBox.Show("Le nom de compte existe déjà...", "Problème!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                    MessageBox.Show("Vous devez remplir toutes les cases!", "Problème!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
                MessageBox.Show("Vous devez vous connecter...", "Problème!", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Logiciel de création de compte pour COPS v3. Créé par ~K!ra~.", "Information!", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            Program.Ip = textBox1.Text;
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            Program.Username = textBox2.Text;
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            Program.RealName = textBox3.Text;
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            Program.Email = textBox4.Text;
        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {
            Program.Question = textBox5.Text;
        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {
            Program.Answer = textBox6.Text;
        }
    }
}

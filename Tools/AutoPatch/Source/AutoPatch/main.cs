﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Net;
using System.Diagnostics;
using System.Timers;

namespace AutoPatch
{
    public partial class AutoPatch : Form
    {
        public System.Timers.Timer DLTimer;

        public AutoPatch()
        {
            InitializeComponent();
            Program.Config();
            if (Program.ChanceSuccess(50))
                BackgroundImage = global::AutoPatch.Properties.Resources.First;
            else
                BackgroundImage = global::AutoPatch.Properties.Resources.Second;
            Display.Text = Program.Display;
            CopyRight.Text = Program.CopyRight;

            DLTimer = new System.Timers.Timer();
            DLTimer.Interval = 2500;
            DLTimer.Elapsed += new ElapsedEventHandler(Download);
            DLTimer.Start();
        }

        public void Download(object sender, ElapsedEventArgs e)
        {
            DLTimer.Stop();
            Uri LienFormate = new Uri("http://" + Program.HTTP + Convert.ToString(Program.Version + 1) + ".exe");
            WebClient ClientWeb = new WebClient();
            ClientWeb.DownloadFileCompleted += new AsyncCompletedEventHandler(DownloadCompleted);
            ClientWeb.DownloadProgressChanged += new DownloadProgressChangedEventHandler(ProgressBarUpdate);
            ClientWeb.DownloadFileAsync(LienFormate, Environment.CurrentDirectory + "\\" + Program.patch);
        }

        public void ProgressBarUpdate(object sender, DownloadProgressChangedEventArgs e)
        {
            this.Progress.Value = e.ProgressPercentage;
            this.ProgressPCT.Text = "Progrès: (" + e.ProgressPercentage + "%)";
        }

        public void DownloadCompleted(object sender, AsyncCompletedEventArgs e)
        {
            System.IO.FileInfo oFile = new System.IO.FileInfo(Program.patch);
            if (oFile.Exists)
            {
                if (oFile.Length > 0)
                    Program.DLPatch = true;
                else
                    Program.DLPatch = false;
            }

            Program.Lauch();
        }
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Threading;
using System.Diagnostics;
using System.Windows.Forms;
using System.Timers;
using System.Data;
using System.ComponentModel;
using System.IO;

namespace COServer_Project
{

    public class General
    {
        static void Main()
        {           
            new General();
        }
        public static Socket Serveur = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        public static System.IO.StreamWriter sw = new System.IO.StreamWriter(Application.StartupPath + @"\ServerLog.txt", true);
        public static Hashtable Players = new Hashtable();
        public static System.Timers.Timer Thetimer;
        public static System.Timers.Timer ExpPot;
        public static System.Timers.Timer GwPktDisTimer;
        public static System.Timers.Timer StatsTimer;
        public static System.Timers.Timer JailSystem;
        System.Timers.Timer IPTimer;

        private struct AuthBinder
        {
            public Client Client;
            public string Acc;
            public int Auth;
        }
        public static ServerSocket AuthServer;
        public static ServerSocket GameServer;
        public static string ServerIP;
        public static bool LoginOn;
        public static bool DMapLoad;
        public static bool ServerOnline;
        public static string ClientStatus = "0";
        public static string CODirectory;
        public static string SMSystem = "";

        public static Hashtable AuthServerClients;
        public static Hashtable KeyClients;

        public static Packets MyPackets = new Packets();
        public static Random Rand = new Random();


        public static System.IO.TextWriter TW;

        public static void ServerRestart()
        {
            Process nServ = new Process();
            nServ.StartInfo.FileName = Application.StartupPath + @"\restart.exe";
            nServ.Start();
            World.SaveAllChars();
            ClientStatus = "0";
            WriteStatus();
            UploadStatus();
            Environment.Exit(0);
        }

        public static void ServerUpdates()
        {
            Process nServ = new Process();
            nServ.StartInfo.FileName = Application.StartupPath + @"\AutoPatch.exe";
            nServ.Start();
            World.SaveAllChars();
            ClientStatus = "0";
            WriteStatus();
            UploadStatus();
            Environment.Exit(0);
        }

        public unsafe General()
        {
            try
            {
                Console.Title = "COPS v3 Emulator";
                Ini Config = new Ini(System.Windows.Forms.Application.StartupPath + @"\Config.ini");
                ServerIP = Config.ReadValue("Server", "ServerIP");
                LoginOn = bool.Parse(Config.ReadValue("Server", "LoginOn"));
                DMapLoad = bool.Parse(Config.ReadValue("Server", "DMapLoad"));
                CODirectory = Config.ReadValue("Server", "CODirectory");
                SMSystem = Config.ReadValue("Server", "SMSystem");
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine("Configuring MYSQL Database...");
                string DBHost = Config.ReadValue("Server", "DBHost");
                string DBPort = Config.ReadValue("Server", "DBPort");
                string DBName = Config.ReadValue("Server", "DBName");
                string DBUserName = Config.ReadValue("Server", "DBUserName");
                string DBUserPass = Config.ReadValue("Server", "DBUserPass");
                DataBase.ExpRate = uint.Parse(Config.ReadValue("Server", "XPRate"));
                DataBase.ProfExpRate = uint.Parse(Config.ReadValue("Server", "ProfXPRate"));
                DataBase.Connect(DBHost, DBPort, DBName, DBUserName, DBUserPass);
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine("Server IP : " + ServerIP);
                Console.WriteLine("Database Host : " + DBHost);
                Console.WriteLine("Database Port : " + DBPort);
                Console.WriteLine("Database Name : " + DBName);
                Console.WriteLine("Database Username : " + DBUserName);
                Console.WriteLine("Database Password : " + DBUserPass);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Successfully Configured Database!");
                Console.WriteLine("");
                Console.WriteLine("");
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine("Loaded Database...");
                Console.ForegroundColor = ConsoleColor.Cyan;
                DataBase.LoadEnchantData();
                DataBase.LoadPortals();
                DataBase.LoadNPCs();
                DataBase.LoadMobs();
                DataBase.LoadItems();
                DataBase.LoadMobSpawns();
                DataBase.LoadRevPoints();
                Mobs.SpawnAllMobs();
                DataBase.GetPlusInfo();
                NPCs.SpawnAllNPCs();
                DataBase.DefineSkills();
                DataBase.LoadGuilds();
                DataBase.AllOffline();
                DataBase.GetBannedIPs();
                DataBase.GetBannedISPs();
                if (DMapLoad == true)
                    DMaps.LoadDMapz("GameMap.dat");
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Successfully Loaded Database!");

                AuthServerClients = new Hashtable();
                KeyClients = new Hashtable();

                AuthServer = new ServerSocket();
                AuthServer.Port = 9958;
                AuthServer.MaxPacketSize = 200;
                AuthServer.MaxThreads = 300;
                AuthServer.OnClientConnect += new SocketEvent(AuthConnHandler);
                AuthServer.OnReceivePacket += new SocketEvent(AuthPacketHandler);
                AuthServer.Enabled = true;

                GameServer = new ServerSocket();
                GameServer.Port = 5816;
                GameServer.MaxPacketSize = 4096;
                GameServer.MaxThreads = 300;
                GameServer.OnClientDisconnect += new SocketDisconnectEvent(GameDisconnectionHandler);
                GameServer.OnReceivePacket += new SocketEvent(GamePacketHandler);
                GameServer.Enabled = true;

                Console.WriteLine("");
                Console.WriteLine("");
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine("Configuring Socket System...");
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine("Auth Server: " + AuthServer.Port);
                Console.WriteLine("Game Server: " + GameServer.Port);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Successfully Configured Socket System!");
                Console.WriteLine("");
                Console.WriteLine("");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("--Type .help for a list of available Console Commands--");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("*******************************************************");

                Thetimer = new System.Timers.Timer();
                Thetimer.Interval = 300000;
                Thetimer.Elapsed += new ElapsedEventHandler(Thetimer_Elapsed);
                Thetimer.Start();

                ExpPot = new System.Timers.Timer();
                ExpPot.Interval = 1000;
                ExpPot.Elapsed += new ElapsedEventHandler(ExpPot_System);
                ExpPot.Start();

                IPTimer = new System.Timers.Timer();
                IPTimer.Interval = 650000;
                IPTimer.Elapsed += new ElapsedEventHandler(IPTimer_Elapsed);
                IPTimer.Start();

                GwPktDisTimer = new System.Timers.Timer();
                GwPktDisTimer.Interval = 15000;
                GwPktDisTimer.Elapsed += new ElapsedEventHandler(GwPktDis);
                GwPktDisTimer.Start();

                StatsTimer = new System.Timers.Timer();
                StatsTimer.Interval = 60000;
                StatsTimer.Elapsed += new ElapsedEventHandler(StatsTimer_Elapsed);
                StatsTimer.Start();

                JailSystem = new System.Timers.Timer();
                JailSystem.Interval = 600000;
                JailSystem.Elapsed += new ElapsedEventHandler(JailSystem_Elapsed);

                DataTimeNow();
                ClientStatus = "3";
                WriteStatus();
                UploadStatus();

                DoStuff();
            }
            catch (Exception Exc) { WriteLine(Exc.ToString()); }
        }

        void GwPktDis(object sender, ElapsedEventArgs e)
        {
            DataTimeNow();
        }
        void JailSystem_Elapsed(object sender, ElapsedEventArgs e)
        {
            foreach (DictionaryEntry DE in World.AllChars)
            {
                Character Chaar = (Character)DE.Value;
                if (Chaar.LocMap == 1505)
                {
                    Chaar.Teleport(1002, 430, 380);
                }
            }
        }
        void StatsTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            ServeurStatus();
            WriteStatus();
            UploadStatus();
        }

        void IPTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            IPTimer.Stop();
            World.SendMsgToAll("Site: http://copsvx.e3b.org/ et Forum: http://copsvx.e3b.org/phpBB3/. Visitez r�guli�rement le forum pour savoir le progr�s!", "SYSTEM", 2011);
            IPTimer.Start();
        }

        public static void Thetimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Ini Config = new Ini(System.Windows.Forms.Application.StartupPath + @"\Config.ini");
            string DBHost = Config.ReadValue("Server", "DBHost");
            string DBPort = Config.ReadValue("Server", "DBPort");
            string DBName = Config.ReadValue("Server", "DBName");
            string DBUserName = Config.ReadValue("Server", "DBUserName");
            string DBUserPass = Config.ReadValue("Server", "DBUserPass");

            DataBase.Connection.Close();
            DataBase.Connection = null;
            DataBase.Connect(DBHost, DBPort, DBName, DBUserName, DBUserPass);
            World.SaveAllChars();
            DataBase.GetBannedIPs();
            DataBase.GetBannedISPs();
        }

        public static void DataTimeNow()
        {
            DateTime myDateTime = DateTime.Now;
            string DayNow = Convert.ToString(myDateTime.DayOfWeek);

            //Maintenance
            if (myDateTime.Hour == 3  && myDateTime.Minute < 15)
            {
                if (World.SMStart == false)
                {
                    Process nServ = new Process();
                    nServ.StartInfo.FileName = Application.StartupPath + @"\" + SMSystem;
                    nServ.Start();

                    World.SMStart = true;
                    World.SendMsgToAll("Maintenance du serveur dans 2 minutes! Veuillez vous d�connecter pour pr�venir la perte de data!", "SYSTEM", 2011);
                    new Thread(new ThreadStart(
                    delegate()
                    {
                        Console.WriteLine("Server Maintenance Command Activated (via Timer).");
                        Console.WriteLine("2 Minutes Left");
                        Thread.Sleep(30000); // 30 second
                        World.SendMsgToAll("Maintenance du serveur dans 1 minute et 30 secondes! Veuillez vous d�connecter pour pr�venir la perte de data!", "SYSTEM", 2011);
                        Console.WriteLine("1:30 Minutes Left");
                        Thread.Sleep(30000); // 30 second
                        World.SendMsgToAll("Maintenance du serveur dans 1 minute! Veuillez vous d�connecter pour pr�venir la perte de data!", "SYSTEM", 2011);
                        Console.WriteLine("1 Minute Left");
                        Thread.Sleep(30000); // 30 second
                        World.SendMsgToAll("Maintenance du serveur dans 30 secondes! Veuillez vous d�connecter pour pr�venir la perte de data!", "SYSTEM", 2011);
                        Console.WriteLine("0:30 Seconds Left");
                        Thread.Sleep(30000); // 30 second
                        World.SendMsgToAll("Maintenance du serveur de 15 minutes! Veuillez vous d�connecter pour pr�venir la perte de data!", "SYSTEM", 2011);
                        Thread.Sleep(10000); // 30 second
                        Console.WriteLine("Server is now shutting down...");
                        try
                        {
                            foreach (DictionaryEntry DE in World.AllChars)
                            {
                                Character Char = (Character)DE.Value;
                                DataBase.ChangeOnlineStatus(Char.MyClient.Account, 0);
                            }
                            ClientStatus = "0";
                            WriteStatus();
                            UploadStatus();
                            World.PlayersOffLottery();
                            World.SaveAllChars();
                            sw.Flush();
                            sw.Close();
                            DataBase.AllOffline();

                            System.Diagnostics.Process myproc = new System.Diagnostics.Process();
                            try
                            {
                                byte Count = 0;
                                foreach (Process thisproc in Process.GetProcessesByName("KillProcess"))
                                {
                                    thisproc.Kill();
                                    Count += 1;
                                    Console.WriteLine("Successfully Killed Process!");
                                }
                                if (Count == 0)
                                {
                                    Console.ForegroundColor = ConsoleColor.Red;
                                    Console.WriteLine("Error! Kill Process!");
                                    Console.ForegroundColor = ConsoleColor.Cyan;
                                }
                            }
                            catch { }

                            Environment.Exit(Environment.ExitCode);
                        }
                        catch { }
                    }
                 )).Start();
                }
            }

            //Quest
            if (myDateTime.Hour == 0 && myDateTime.Minute < 1)
            {
                foreach (DictionaryEntry DE in World.AllChars)
                {
                    Character Chaar = (Character)DE.Value;
                    Chaar.QuestNB = 0;
                }
                string[] Chars = Directory.GetFiles("Characters", "*.chr");
                for (int i = 0; i < Chars.Length; i++)
                {
                    Ini Cli = new Ini(System.Windows.Forms.Application.StartupPath + @"\" + Chars[i]);
                    Cli.WriteString("Character", "QuestNB", "0");
                }
            }

            //Loto
            if (myDateTime.Hour == 0 && myDateTime.Minute < 1)
            {
                foreach (DictionaryEntry DE in World.AllChars)
                {
                    Character Chaar = (Character)DE.Value;
                    Chaar.LotoCount = 0;
                }
                string[] Chars = Directory.GetFiles("Characters", "*.chr");
                for (int i = 0; i < Chars.Length; i++)
                {
                    Ini Cli = new Ini(System.Windows.Forms.Application.StartupPath + @"\" + Chars[i]);
                    Cli.WriteString("Character", "LotoCount", "0");
                }
            }

            //Gw On/Off
            if ((DayNow == "Saturday" && myDateTime.Hour >= 14) || (DayNow == "Sunday" && myDateTime.Hour < 20))
            {
                if (World.GWOn == false)
                {
                    World.GWOn = true;
                    World.JailOn = true;
                    JailSystem.Start();
                    World.SendMsgToAll("La GW est commenc�e!", "SYSTEM", 2011);
                }
                else
                {
                    World.GWOn = true;
                    World.JailOn = true;
                }
            }
            else
            {
                World.GWOn = false;
                World.JailOn = false;
                JailSystem.Stop();
            }

            //Pkt On/Off NPC 1
            if ((DayNow == "Saturday" || myDateTime.Day == 1) && myDateTime.Hour == 20 && myDateTime.Minute > 29)
            {
                if (World.PktOn == false || myDateTime.Minute < 35)
                {
                    World.PktOn = true;
                    SingleNPC npcc = new SingleNPC(9997, 310, 2, 0, 451, 291, 1002, 0);
                    World.SpawnsNPC(npcc);
                    NPCs.AllNPCs.Add(9997, npcc);
                    World.SendMsgToAll("Le Tournois de PK est commenc�e! Allez voir G�n�ralBrave � la ville Dragon(451, 291).", "SYSTEM", 2011);
                }
                else
                {
                    World.PktOn = true;
                    SingleNPC npcc = new SingleNPC(9997, 310, 2, 0, 451, 291, 1002, 0);
                    World.SpawnsNPC(npcc);
                    NPCs.AllNPCs.Add(9997, npcc);
                }
            }
            else
            {
                World.PktOn = false;
                SingleNPC npcc = new SingleNPC(9997, 310, 2, 0, 451, 291, 1002, 0);
                NPCs.AllNPCs.Remove(9997);
                World.RemoveNPC(npcc);
            }

            //Pkt On/Off NPC 2
            if ((DayNow == "Saturday" || myDateTime.Day == 1) && myDateTime.Hour == 21 && myDateTime.Minute > 29 && myDateTime.Minute < 35)
            {
                World.PktOn = true;
                SingleNPC npcc = new SingleNPC(9996, 310, 2, 0, 240, 154, 1081, 0);
                World.SpawnsNPC(npcc);
                NPCs.AllNPCs.Add(9996, npcc);
                World.SendMsgToAll("La deuxi�me carte du tournois est ouverte. Allez voir G�n�ralBrave(240, 154)", "SYSTEM", 2011);
            }
            else
            {
                World.PktOn = false;
                SingleNPC npcc = new SingleNPC(9996, 310, 2, 0, 240, 154, 1081, 0);
                NPCs.AllNPCs.Remove(9996);
                World.RemoveNPC(npcc);
            }

            //Pkt On/Off NPC 3
            if ((DayNow == "Saturday" || myDateTime.Day == 1) && myDateTime.Hour == 22 && myDateTime.Minute > 29 && myDateTime.Minute < 35)
            {
                World.PktOn = true;
                SingleNPC npcc = new SingleNPC(9995, 310, 2, 0, 162, 134, 1090, 0);
                World.SpawnsNPC(npcc);
                NPCs.AllNPCs.Add(9995, npcc);
                World.SendMsgToAll("La troisi�me carte du tournois est ouverte. Allez voir G�n�ralBrave(162, 134)", "SYSTEM", 2011);
            }
            else
            {
                World.PktOn = false;
                SingleNPC npcc = new SingleNPC(9995, 310, 2, 0, 162, 134, 1090, 0);
                NPCs.AllNPCs.Remove(9995);
                World.RemoveNPC(npcc);
            }

            //Pkt On/Off Remove Player
            if ((DayNow == "Saturday" || myDateTime.Day == 1) && myDateTime.Hour == 21 && myDateTime.Minute > 34)
            {
                foreach (DictionaryEntry DE in World.AllChars)
                {
                    Character Chaar = (Character)DE.Value;
                    if (Chaar.LocMap == 1081)
                    {
                        Chaar.Teleport(1002, 430, 380);
                    }
                }
            }
            //Pkt On/Off Remove Player
            if ((DayNow == "Saturday" || myDateTime.Day == 1) && myDateTime.Hour == 22 && myDateTime.Minute > 34)
            {
                foreach (DictionaryEntry DE in World.AllChars)
                {
                    Character Chaar = (Character)DE.Value;
                    if (Chaar.LocMap == 1090)
                    {
                        Chaar.Teleport(1002, 430, 380);
                    }
                }
            }

            //Pkt Price 1
            if ((DayNow == "Saturday" || myDateTime.Day == 1) && myDateTime.Hour == 21 && myDateTime.Minute < 30)
            {
                SingleNPC npcc = new SingleNPC(9994, 320, 2, 0, 224, 154, 1081, 0);
                World.SpawnsNPC(npcc);
                NPCs.AllNPCs.Add(9994, npcc);
            }
            else
            {
                SingleNPC npcc = new SingleNPC(9994, 320, 2, 0, 224, 154, 1081, 0);
                NPCs.AllNPCs.Remove(9994);
                World.RemoveNPC(npcc);
            }

            //Pkt Price 2
            if ((DayNow == "Saturday" || myDateTime.Day == 1) && myDateTime.Hour == 22 && myDateTime.Minute < 30)
            {
                SingleNPC npcc = new SingleNPC(9993, 320, 2, 0, 167, 133, 1090, 0);
                World.SpawnsNPC(npcc);
                NPCs.AllNPCs.Add(9993, npcc);
            }
            else
            {
                SingleNPC npcc = new SingleNPC(9993, 320, 2, 0, 167, 133, 1090, 0);
                NPCs.AllNPCs.Remove(9993);
                World.RemoveNPC(npcc);
            }

            //Pkt Price 3
            if ((DayNow == "Saturday" || myDateTime.Day == 1) && myDateTime.Hour == 22 && myDateTime.Minute > 34)
            {
                SingleNPC npcc = new SingleNPC(9992, 320, 2, 0, 50, 37, 1091, 0);
                World.SpawnsNPC(npcc);
                NPCs.AllNPCs.Add(9992, npcc);
            }
            else
            {
                SingleNPC npcc = new SingleNPC(9992, 320, 2, 0, 50, 37, 1091, 0);
                NPCs.AllNPCs.Remove(9992);
                World.RemoveNPC(npcc);
            }

            //Dis On/Off
            if (((DayNow == "Monday" || DayNow == "Wednesday" || DayNow == "Friday") && myDateTime.Hour == 20 && myDateTime.Minute < 6) || ((DayNow == "Tuesday" || DayNow == "Thursday") && myDateTime.Hour == 21 && myDateTime.Minute < 6))
            {
                World.DisOn = true;
                World.SendMsgToAll("Dis City est commenc�e! Allez voir Tao�steDeMer � Ville Tigre (532,480).", "SYSTEM", 2011);
            }
            else
                World.DisOn = false;

            //Dis Map 4
            World.DisMap1.Clear();
            World.DisMap2.Clear();
            World.DisMap3.Clear();
            World.DisMap4.Clear();
            foreach (DictionaryEntry DE in World.AllChars)
            {
                Character Charr = (Character)DE.Value;

                if (Charr.LocMap == 2024)
                    World.DisMap4.Add(Charr.Name, Charr.UID);

                if (Charr.LocMap == 2023)
                    World.DisMap3.Add(Charr.Name, Charr.UID);

                if (Charr.LocMap == 2022)
                    World.DisMap2.Add(Charr.Name, Charr.UID);

                if (Charr.LocMap == 2021)
                    World.DisMap1.Add(Charr.Name, Charr.UID);
            }
            if (World.DisMap1.Count == 0 && World.DisMap2.Count == 0)
            {
                foreach (DictionaryEntry DE in World.AllChars)
                {
                    Character Chaar = (Character)DE.Value;
                    if (Chaar.LocMap == 2023)
                    {
                        Chaar.Teleport(2024, 150, 283);
                    }
                }
            }
            if (World.DisMap1.Count == 0 && World.DisMap2.Count == 0 && World.DisMap3.Count == 0 && World.DisMap4.Count == 1)
            {
                World.DisBoss.Clear();
                foreach (DictionaryEntry DE in Mobs.AllMobs)
                {
                    SingleMob Mobz = (SingleMob)DE.Value;
                    if (Mobz.Map == 2024)
                        World.DisBoss.Add(Mobz.Name);
                }

                if (!World.DisBoss.Contains("PlutoFinal"))
                {
                    uint UID = (uint)General.Rand.Next(400000, 500000);
                    while (Mobs.AllMobs.Contains(UID))
                    {
                        UID = (uint)General.Rand.Next(400000, 500000);
                    }
                    SingleMob Mob = new SingleMob(149, 139, 2024, 65535, 65535, 2850, 3950, UID, "PlutoFinal", 361, 126, (byte)General.Rand.Next(8), 4, 0, true);
                }
            }
        }

        public static void ServeurStatus()
        {
                if (World.AllChars.Count < 74)
                    ClientStatus = "3";

                else if (World.AllChars.Count > 75 && World.AllChars.Count < 99)
                    ClientStatus = "2";

                else if (World.AllChars.Count > 100)
                    ClientStatus = "1";
                else
                    ClientStatus = "0";
        }

        public static void WriteStatus()
        {
            string Status = @"status.php";

            StreamWriter sw = null;

            try
            {
                if (!File.Exists(Status))
                {
                    sw = new StreamWriter(Status);
                    sw.Write("COPS " + ClientStatus);
                    sw.Close();
                    sw = null;
                }
                if (File.Exists(Status))
                {
                    File.Delete(Status);
                    sw = new StreamWriter(Status);
                    sw.Write("COPS " + ClientStatus);
                    sw.Close();
                    sw = null;
                }
            }
            finally
            {
                if (sw != null) sw.Close();
            }

        }

        public static void UploadStatus()
        {
            try
            {
                WebClient wc = new WebClient();

                wc.Credentials = new NetworkCredential(@"copsVX", @"s0cac3r3b0rn");
                wc.UploadFile(@"ftp://e3b.org/status.php", WebRequestMethods.Ftp.UploadFile, "status.php");

                wc.Dispose();
            }
            catch (Exception Exc) { WriteLine(Exc.ToString()); }
        }

        public static void DoStuff()
        {
            bool flag = true;
            while (flag)
            {
                string str;
                str = Console.ReadLine();
                if (str == ".close")
                {
                    try
                    {
                        Ini Config = new Ini(System.Windows.Forms.Application.StartupPath + @"\Config.ini");
                        string DBHost = Config.ReadValue("Server", "DBHost");
                        string DBPort = Config.ReadValue("Server", "DBPort");
                        string DBName = Config.ReadValue("Server", "DBName");
                        string DBUserName = Config.ReadValue("Server", "DBUserName");
                        string DBUserPass = Config.ReadValue("Server", "DBUserPass");

                        DataBase.Connection.Close();
                        DataBase.Connection = null;
                        DataBase.Connect(DBHost, DBPort, DBName, DBUserName, DBUserPass);
                        foreach (DictionaryEntry DE in World.AllChars)
                        {
                            Character Char = (Character)DE.Value;
                            DataBase.ChangeOnlineStatus(Char.MyClient.Account, 0);
                        }
                        ClientStatus = "0";
                        WriteStatus();
                        UploadStatus();
                        World.PlayersOffLottery();
                        World.SaveAllChars();
                        sw.Flush();
                        sw.Close();
                        DataBase.AllOffline();
                        Environment.Exit(Environment.ExitCode);
                    }
                    catch { }
                }
                if (str == ".restart")
                {
                    World.SendMsgToAll("Server Restart in 10 seconds!", "SYSTEM", 2011);
                    new Thread(new ThreadStart(
                     delegate()
                     {
                         Console.WriteLine("Server Restarting in 10 seconds!");
                         Thread.Sleep(10000);
                         General.ServerRestart();
                     }
                  )).Start();
                }
                if (str == ".clear")
                {
                    Console.Clear();
                    Console.ForegroundColor = ConsoleColor.Cyan;
                    Console.WriteLine("*^* Server Source *^*");
                    Console.WriteLine("-=-=-=-=-=-=-=-=-=-=");
                    Console.ResetColor();
                }
                if (str == ".help")
                {
                    Console.WriteLine("\nCurrent Console Commands: \n.clear - Clears Console \n.restart - Restarts Server \n.close - Closes Server (and saves characters) \n.message - Sends in-Game GM Message \n.sm - Automated 5-minute Server Maintenance Command\n.playershow - Shows who is on and how many clients are connected to the server.");
                }
                if (str == ".sm")
                {
                    World.SendMsgToAll("Server Maintanience in 5 minutes! Please log off to avord data lost!", "SYSTEM", 2011);
                    new Thread(new ThreadStart(
                    delegate()
                    {
                        Console.WriteLine("Server Maintenance Command Activated (via Console).");
                        Thread.Sleep(30000); // 30 second
                        World.SendMsgToAll("Server Maintenance in 4 minutes and 30 Seconds! Please Log off to prevent data loss!", "SYSTEM", 2011);
                        Console.WriteLine("4:30 Minutes Left");
                        Thread.Sleep(30000); // 30 second
                        World.SendMsgToAll("Server Maintenance in 4 minutes! Please Log off to prevent data loss!", "SYSTEM", 2011);
                        Console.WriteLine("4 Minutes Left");
                        Thread.Sleep(30000); // 30 second
                        World.SendMsgToAll("Server Maintenance in 3 minutes and 30 Seconds! Please Log off to prevent data loss!", "SYSTEM", 2011);
                        Console.WriteLine("3:30 Minutes Left");
                        Thread.Sleep(30000); // 30 second
                        World.SendMsgToAll("Server Maintenance in 3 minutes! Please Log off to prevent data loss!", "SYSTEM", 2011);
                        Console.WriteLine("3 Minutes Left");
                        Thread.Sleep(30000); // 30 second
                        World.SendMsgToAll("Server Maintenance in 2 minutes and 30 Seconds! Please Log off to prevent data loss!", "SYSTEM", 2011);
                        Console.WriteLine("2:30 Minutes Left");
                        Thread.Sleep(30000); // 30 second
                        World.SendMsgToAll("Server Maintenance in 2 minutes! Please Log off to prevent data loss!", "SYSTEM", 2011);
                        Console.WriteLine("2 Minutes Left");
                        Thread.Sleep(30000); // 30 second
                        World.SendMsgToAll("Server Maintenance in 1 minute and 30 Seconds! Please Log off to prevent data loss!", "SYSTEM", 2011);
                        Console.WriteLine("1:30 Minutes Left");
                        Thread.Sleep(30000); // 30 second
                        World.SendMsgToAll("Server Maintenance in 1 minute! Please Log off to prevent data loss!", "SYSTEM", 2011);
                        Console.WriteLine("1 Minute Left");
                        Thread.Sleep(30000); // 30 second
                        World.SendMsgToAll("Server Maintenance in 30 Seconds! Please Log off to prevent data loss!", "SYSTEM", 2011);
                        Console.WriteLine("0:30 Seconds Left");
                        Thread.Sleep(30000); // 30 second
                        World.SendMsgToAll("Server Maintenance for 30 minutes! Please Log off to prevent data loss!", "SYSTEM", 2011);
                        Thread.Sleep(10000); // 30 second
                        Console.WriteLine("Server is now shutting down...");
                        try
                        {
                            Ini Config = new Ini(System.Windows.Forms.Application.StartupPath + @"\Config.ini");
                            string DBHost = Config.ReadValue("Server", "DBHost");
                            string DBPort = Config.ReadValue("Server", "DBPort");
                            string DBName = Config.ReadValue("Server", "DBName");
                            string DBUserName = Config.ReadValue("Server", "DBUserName");
                            string DBUserPass = Config.ReadValue("Server", "DBUserPass");

                            DataBase.Connection.Close();
                            DataBase.Connection = null;
                            DataBase.Connect(DBHost, DBPort, DBName, DBUserName, DBUserPass);
                            foreach (DictionaryEntry DE in World.AllChars)
                            {
                                Character Char = (Character)DE.Value;
                                DataBase.ChangeOnlineStatus(Char.MyClient.Account, 0);
                            }
                            ClientStatus = "0";
                            WriteStatus();
                            UploadStatus();
                            World.PlayersOffLottery();
                            World.SaveAllChars();
                            sw.Flush();
                            sw.Close();
                            DataBase.AllOffline();
                            Environment.Exit(Environment.ExitCode);
                        }
                        catch { }



                    }
                 )).Start();


                }
                if (str.StartsWith(".message"))
                {
                    string msg;
                    msg = str.Substring(8);
                    World.SendMsgToAll(msg, "SYSTEM", 2011);
                    Console.WriteLine("Sent Message");
                }
                if (str.StartsWith(".playershow"))
                {
                    string BackMsg = "";
                    foreach (DictionaryEntry DE in World.AllChars)
                    {
                        Character Char = (Character)DE.Value;
                        BackMsg += Char.Name + ", ";
                    }
                    if (BackMsg.Length > 1)
                        BackMsg = BackMsg.Remove(BackMsg.Length - 2, 2);
                    Console.WriteLine("Players Online: " + World.AllChars.Count + "\n" + BackMsg);

                }
            }
        }

        public static void ExpPot_System(object sender, ElapsedEventArgs e)
        {
            foreach (DictionaryEntry DE in World.AllChars)
            {
                Character Char = (Character)DE.Value;
                if (Char != null && Char.dexp == 1)
                {
                    if (Char.dexptime == 0)
                    {
                        Char.dexp = 0;
                        break;
                    }
                    int ID = int.Parse("19");
                    Char.dexptime -= 1;
                    Char.MyClient.SendPacket(General.MyPackets.Vital((long)Char.UID, ID, Char.dexptime));
                }
                if (Char != null && Char.dexp == 2)
                {
                    if (Char.dexptime == 0)
                    {
                        Char.dexp = 0;
                        break;
                    }
                    int ID = int.Parse("19");
                    Char.dexptime -= 1;
                    Char.MyClient.SendPacket(General.MyPackets.Vital((long)Char.UID, ID, Char.dexptime));
                }
                if (Char != null && Char.dexp == 3)
                {
                    if (Char.dexptime == 0)
                    {
                        Char.dexp = 0;
                        break;
                    }
                    int ID = int.Parse("19");
                    Char.dexptime -= 1;
                    Char.MyClient.SendPacket(General.MyPackets.Vital((long)Char.UID, ID, Char.dexptime));
                }
            }
        }

        public void GameDisconnectionHandler(object Sender, HybridSocket Socket, string Debug)
        {
            try
            {
                Client Cli = (Client)Socket.Wrapper;
                if (Cli != null && Cli.Online)
                    Cli.Drop();
            }
            catch (Exception Exc) { WriteLine(Exc.ToString()); }
        }
    
        public void AuthConnHandler(object Sender, HybridSocket Socket)
        {
            try
            {
                Socket Sock = Socket.WinSock;
                if (Sock == null)
                    return;
                IPEndPoint IPE = (IPEndPoint)Sock.RemoteEndPoint;

                Client NewClient = new Client();
                AuthBinder NewAB = new AuthBinder();

                NewAB.Client = NewClient;

                AuthServerClients.Add(Convert.ToString(IPE.Address) + ":" + IPE.Port, NewAB);
            }
            catch (Exception Exc) { WriteLine(Exc.ToString()); }
        }

        public void AuthPacketHandler(object Sender, HybridSocket Socket)
        {
            try
            {
                Socket Sock = Socket.WinSock;
                IPEndPoint IPE = (IPEndPoint)Sock.RemoteEndPoint;
                string Ip = Convert.ToString(IPE.Address);
                int Port = IPE.Port;

                byte[] Data = Socket.Packet;

                AuthBinder AB = (AuthBinder)AuthServerClients[Ip + ":" + Port];
                Client TheClient = AB.Client;

                if (Data.Length == 52)
                    TheClient.Crypto.Decrypt(ref Data);

                if (DataBase.IPBanned(Ip))
                {
                    TheClient.SendPacket(General.MyPackets.SendMsg(TheClient.MessageId, "SYSTEM", "ALLUSERS", "Vous �tes banni!", 2101));
                    Socket.Disconnect();
                    AuthServerClients.Remove(Ip + ":" + Port);
                }
                if (DataBase.ISPBanned(Ip))
                {
                    TheClient.SendPacket(General.MyPackets.SendMsg(TheClient.MessageId, "SYSTEM", "ALLUSERS", "Vous �tes banni!", 2101));
                    Socket.Disconnect();
                    AuthServerClients.Remove(Ip + ":" + Port);
                }

                /*BlowfishCrypto Blowfish = new BlowfishCrypto("DR654dt34trg4UI6");
                byte[] Packet = Blowfish.Blowfish.Decrypt(Socket.Packet);
                File.WriteAllBytes(@"Blowfish.dat", Packet);*/

                //if (Data[0] == 0x15 && Data[1] == 0x94 && Data[2] == 0x56 && Data[3] == 0x65)
                if (Data[0] == 0x34 && Data[1] == 0x00 && Data[2] == 0x1b && Data[3] == 0x04)
                {
                    try
                    {
                        int read = 0x04;
                        //int read = 0x65;
                        string ThisAcc = "";
                        string ThisPass = "";

                        while (read < 0x14 && Data[read] != 0x00)
                        {
                            ThisAcc += Convert.ToChar(Data[read]);
                            read += 1;
                        }

                        read = 0x14;

                        while (read < 36 && Data[read] != 0)
                        {
                            ThisPass += (Convert.ToString(Data[read], 16)).PadLeft(2, '0');
                            read += 1;
                        }

                        /*while (read < 0x75 && Data[read] != 0x00)
                        {
                            ThisAcc += Convert.ToChar(Data[read]);
                            read += 1;
                            Console.WriteLine(ThisAcc);
                        }

                        read = 0x75;

                        while (read < 36 && Data[read] != 0)
                        {
                            ThisPass += (Convert.ToString(Data[read], 16)).PadLeft(2, '0');
                            read += 1;
                            Console.WriteLine(ThisPass);
                        }*/

                        byte Auth = DataBase.Authenticate(ThisAcc, ThisPass);

                        if (Auth != 0)
                        {
                            General.WriteLine("Successful login for account " + ThisAcc + "-IP-" + Ip);
                            AB.Auth = Auth;
                            AB.Acc = ThisAcc;
                            AB.Client.Account = ThisAcc;

                            ulong TheKeys = (uint)(Rand.Next(0x98968) << 32);
                            TheKeys = TheKeys << 32;
                            TheKeys = (uint)(TheKeys | (uint)Rand.Next(10000000));

                            KeyClients.Add(TheKeys, AB);

                            byte[] Key1 = new byte[4];
                            byte[] Key2 = new byte[4];

                            Key1[0] = (byte)((ulong)(TheKeys & 0xff00000000000000L) >> 56);
                            Key1[1] = (byte)((TheKeys & 0xff000000000000) >> 48);
                            Key1[2] = (byte)((TheKeys & 0xff0000000000) >> 40);
                            Key1[3] = (byte)((TheKeys & 0xff00000000) >> 32);
                            Key2[0] = (byte)((TheKeys & 0xff000000) >> 24);
                            Key2[1] = (byte)((TheKeys & 0xff0000) >> 16);
                            Key2[2] = (byte)((TheKeys & 0xff00) >> 8);
                            Key2[3] = (byte)(TheKeys & 0xff);

                            try
                            {
                                byte[] Pack = MyPackets.AuthResponse(ServerIP, Key1, Key2);
                                TheClient.Crypto.Encrypt(ref Pack);
                                Sock.Send(Pack);
                            }
                            catch { }

                            Socket.Disconnect();
                            AuthServerClients.Remove(Ip + ":" + Port);
                        }
                        else
                        {
                            TheClient.SendPacket(General.MyPackets.SendMsg(TheClient.MessageId, "SYSTEM", "ALLUSERS", "Mauvais compte ou mot de passe!", 2101));
                            Socket.Disconnect();
                            AuthServerClients.Remove(Ip + ":" + Port);
                        }
                    }
                    catch (Exception Exc) { WriteLine(Convert.ToString(Exc)); }
                }
            }
            catch (Exception Exc) { WriteLine(Exc.ToString()); }
        }

        public void GamePacketHandler(object Sender, HybridSocket Socket)
        {
            try
            {
                Socket Sock = Socket.WinSock;
                IPEndPoint IPE = (IPEndPoint)Sock.RemoteEndPoint;
                string Ip = Convert.ToString(IPE.Address);
                int Port = IPE.Port;

                byte[] Data = Socket.Packet;

                if (Socket.Wrapper != null)
                {
                    Client Cli = (Client)Socket.Wrapper;
                    Cli.GetPacket(Data);
                }                
                else
                {
                    Cryptographer TempCrypto = new Cryptographer();
                    TempCrypto.Decrypt(ref Data);

                    if (Data[0] == 0x1c && Data[1] == 0x00 && Data[2] == 0x1c && Data[3] == 0x04)
                    {
                        ulong Keys = Data[11];
                        Keys = (Keys << 8) | Data[10];
                        Keys = (Keys << 8) | Data[9];
                        Keys = (Keys << 8) | Data[8];
                        Keys = (Keys << 8) | Data[7];
                        Keys = (Keys << 8) | Data[6];
                        Keys = (Keys << 8) | Data[5];
                        Keys = (Keys << 8) | Data[4];

                        AuthBinder AB2 = (AuthBinder)KeyClients[Keys];
                        KeyClients.Remove(Keys);

                        Client ThisClient = AB2.Client;

                        Socket.Wrapper = ThisClient;

                        string Account = AB2.Acc;

                        ThisClient.Crypto = TempCrypto;

                        ThisClient.Crypto.SetKeys(new byte[4] { Data[11], Data[10], Data[9], Data[8] }, new byte[4] { Data[7], Data[6], Data[5], Data[4] });
                        ThisClient.Crypto.EnableAlternateKeys();

                        ThisClient.MessageId = Rand.Next(50000);
                        ThisClient.ListenSock = Socket;
                        ThisClient.GetIPE();

                        
                        if (AB2.Auth == 1)
                        {
                            ThisClient.MyChar = new Character();
                            ThisClient.MyChar.MyClient = ThisClient;
                            ThisClient.Account = Account;
                            DataBase.GetCharInfo(ThisClient.MyChar, Account);

                            ThisClient.SendPacket(MyPackets.LanguageResponse((uint)ThisClient.MessageId));
                            ThisClient.SendPacket(MyPackets.CharacterInfo(ThisClient.MyChar));
                            ThisClient.SendPacket(MyPackets.AfterChar());

                            if (!World.AllChars.Contains(ThisClient.MyChar.UID))
                            {
                                World.AllChars.Add(ThisClient.MyChar.UID, ThisClient.MyChar);
                            }
                            else
                            {
                                ThisClient.MyChar = null;
                                ThisClient = null;
                                return;
                            }
                            DataBase.ChangeOnlineStatus(ThisClient.Account, 1);
                        }
                        else if (AB2.Auth == 2)
                        {
                            ThisClient.SendPacket(MyPackets.NewCharPacket(ThisClient.MessageId));
                        }
                        else if (AB2.Auth == 3)
                        {
                            ThisClient.SendPacket(General.MyPackets.SendMsg(ThisClient.MessageId, "SYSTEM", "ALLUSERS", "Vous �tes banni!", 2101));
                            ThisClient.Drop();
                        }
                        else
                        {
                            ThisClient.Drop();
                        }
                    }
                }
            }
            catch (Exception Exc) { WriteLine(Exc.ToString()); }
        }

        public static void WriteLine(string text)
        {
            try
            {
                Console.WriteLine(text);
                sw.WriteLine(text);
            }
            catch { }
        }


        internal static void SaveAllChar()
        {
            throw new NotImplementedException();
        }
    }

    public class Ini
    {
        public IniFile TheIni;

        public Ini(string path)
        {
            TheIni = new IniFile(path);
        }

        public string ReadValue(string Section, string Key)
        {
            string it = TheIni.IniReadValue(Section, Key);
            return it;
        }
        public void WriteString(string Section, string Key, string Value)
        {
            TheIni.IniWriteValue(Section, Key, Value);
        }
    }

    public class IniFile
    {
        public string path;

        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section,
            string key, string def, StringBuilder retVal, int size, string filePath);

        public IniFile(string INIPath)
        {
            path = INIPath;
        }
        public void IniWriteValue(string Section, string Key, string Value)
        {
            WritePrivateProfileString(Section, Key, Value, this.path);
        }

        public string IniReadValue(string Section, string Key)
        {
            StringBuilder temp = new StringBuilder(255);
            int i = GetPrivateProfileString(Section, Key, "", temp, 255, this.path);
            return temp.ToString();

        }
    }
}
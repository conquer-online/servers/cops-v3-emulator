﻿namespace AutoPatch
{
    partial class AutoPatch
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AutoPatch));
            this.Progress = new System.Windows.Forms.ProgressBar();
            this.Display = new System.Windows.Forms.Label();
            this.CopyRight = new System.Windows.Forms.Label();
            this.ProgressPCT = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Progress
            // 
            this.Progress.Location = new System.Drawing.Point(12, 336);
            this.Progress.Name = "Progress";
            this.Progress.Size = new System.Drawing.Size(576, 19);
            this.Progress.TabIndex = 0;
            // 
            // Display
            // 
            this.Display.AutoSize = true;
            this.Display.BackColor = System.Drawing.Color.Transparent;
            this.Display.ForeColor = System.Drawing.Color.White;
            this.Display.Location = new System.Drawing.Point(12, 362);
            this.Display.Name = "Display";
            this.Display.Size = new System.Drawing.Size(0, 13);
            this.Display.TabIndex = 1;
            // 
            // CopyRight
            // 
            this.CopyRight.AutoSize = true;
            this.CopyRight.BackColor = System.Drawing.Color.Transparent;
            this.CopyRight.ForeColor = System.Drawing.Color.White;
            this.CopyRight.Location = new System.Drawing.Point(12, 379);
            this.CopyRight.Name = "CopyRight";
            this.CopyRight.Size = new System.Drawing.Size(0, 13);
            this.CopyRight.TabIndex = 2;
            // 
            // ProgressPCT
            // 
            this.ProgressPCT.AutoSize = true;
            this.ProgressPCT.BackColor = System.Drawing.Color.Transparent;
            this.ProgressPCT.ForeColor = System.Drawing.Color.White;
            this.ProgressPCT.Location = new System.Drawing.Point(12, 320);
            this.ProgressPCT.Name = "ProgressPCT";
            this.ProgressPCT.Size = new System.Drawing.Size(0, 13);
            this.ProgressPCT.TabIndex = 4;
            // 
            // AutoPatch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 400);
            this.Controls.Add(this.ProgressPCT);
            this.Controls.Add(this.CopyRight);
            this.Controls.Add(this.Display);
            this.Controls.Add(this.Progress);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AutoPatch";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AutoPatch";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ProgressBar Progress;
        private System.Windows.Forms.Label Display;
        private System.Windows.Forms.Label CopyRight;
        private System.Windows.Forms.Label ProgressPCT;
    }
}


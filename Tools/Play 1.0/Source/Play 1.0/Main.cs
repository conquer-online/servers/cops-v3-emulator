﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace Play
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }

        private void Main_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process Conquer = new System.Diagnostics.Process();
            Conquer.StartInfo.FileName = "Updates.exe";
            Conquer.StartInfo.Arguments = "BLACKNULL";
            try
            {
                Conquer.Start();
            }
            catch
            {
                MessageBox.Show("Updates.exe n'a pas été trouvé ou un problème est survenu!", "Error!");
            }
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close(); 
        }

        private void button3_Click(object sender, EventArgs e)
        {
            /*string WebSite = "http://copsvx.e3b.org/";
            System.Diagnostics.Process.Start(WebSite);*/
            MessageBox.Show("Créé par ~K!ra~ / Copyright © COPS 2008 / http://copsvx.e3b.org/", "Information!");
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (System.IO.File.Exists(@"ini\3DEffect.ini"))
            {
                string monAncienFichier, monNouveauFichier;
                monAncienFichier = @"ini\3DEffect.ini";
                monNouveauFichier = @"ini\3DEffect.ini_";
                File.Move(monAncienFichier, monNouveauFichier);
            }
            else
            {
                MessageBox.Show("3DEffect.ini n'a pas été trouvé ou un problème est survenu!", "Error!");
            }
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (System.IO.File.Exists(@"ini\3DEffect.ini"))
            {
                string monAncienFichier, monNouveauFichier;
                monAncienFichier = @"ini\3DEffect.ini_";
                monNouveauFichier = @"ini\3DEffect.ini";
                File.Move(monAncienFichier, monNouveauFichier);
            }
            else
            {
                MessageBox.Show("3DEffect.ini_ n'a pas été trouvé ou un problème est survenu!", "Error!");
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Threading;
using System.Diagnostics;
using System.Windows.Forms;
using System.Timers;
using System.Data;
using System.ComponentModel;
using System.IO;

namespace KillProcess
{
    class General
    {
        static void Main()
        {
            new General();
        }
        public static System.Timers.Timer KillProcess;
        public static System.Timers.Timer LauchProgram;

        public static uint KillTimer = 0;
        public static string ProcessToKill = "";
        public static uint LauchTimer = 0;
        public static string Lauch = "";

        public static string ClientStatus = "0";

        public unsafe General()
        {
            try
            {
                Console.Title = "KillProcess[v1.0]";
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("KillProcess[v1.0]");
                Console.WriteLine("Créé par ~K!ra~");
                Console.WriteLine("Copyright © COPS 2008\n");
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine("Configuring Program...");
                try
                {
                    byte[] Bytes = File.ReadAllBytes(@"KillProcess.ini");
                    Cryptographer.Decrypt(ref Bytes);
                    File.WriteAllBytes(@"KillProcess.dat", Bytes);
                }
                catch (Exception Exc) { Console.WriteLine(Convert.ToString(Exc)); }

                if (File.Exists("KillProcess.dat"))
                {
                    Ini Config = new Ini(System.Windows.Forms.Application.StartupPath + @"\KillProcess.dat");

                    KillTimer = (uint)Convert.ToInt64(Config.ReadValue(Convert.ToString("KillProcess"), "KillTimer"));
                    ProcessToKill = Config.ReadValue(Convert.ToString("KillProcess"), "Process");
                    LauchTimer = (uint)Convert.ToInt64(Config.ReadValue(Convert.ToString("KillProcess"), "LauchTimer"));
                    Lauch = Config.ReadValue(Convert.ToString("KillProcess"), "Lauch");

                    File.Delete("KillProcess.dat");
                }
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine("Kill Timer : " + Convert.ToString(KillTimer));
                Console.WriteLine("Process : " + ProcessToKill);
                Console.WriteLine("Lauch Timer : " + Convert.ToString(LauchTimer));
                Console.WriteLine("Lauch Program : " + Lauch);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Successfully Configured Program!\n");
                Console.ForegroundColor = ConsoleColor.White;

                KillProcess = new System.Timers.Timer();
                KillProcess.Interval = KillTimer;
                KillProcess.Elapsed += new ElapsedEventHandler(KillProcessNow);
                KillProcess.Start();

                LauchProgram = new System.Timers.Timer();
                LauchProgram.Interval = LauchTimer;
                LauchProgram.Elapsed += new ElapsedEventHandler(LauchProgramNow);

                DoStuff();
            }
            catch (Exception Exc) { Console.WriteLine(Exc.ToString()); }
        }

        void KillProcessNow(object sender, ElapsedEventArgs e)
        {
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine(DateTime.Now);
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Kill Timer Elapsed...");
            Console.ForegroundColor = ConsoleColor.Cyan;

            KillProcess.Stop();

            try
            {
                ClientStatus = "0";
                WriteStatus();
                UploadStatus();
                Console.WriteLine("Successfully Uploaded Offline Status!");
            }
            catch
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Error! Upload Offline Status!");
                Console.ForegroundColor = ConsoleColor.Cyan;
            }

            System.Diagnostics.Process myproc = new System.Diagnostics.Process();
            try
            {
                byte Count = 0;
                foreach (Process thisproc in Process.GetProcessesByName(ProcessToKill))
                {
                    thisproc.Kill();
                    Count += 1;
                    Console.WriteLine("Successfully Killed Process!");
                }
                if (Count == 0)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Error! Kill Process!");
                    Console.ForegroundColor = ConsoleColor.Cyan;
                }
            }
            catch { }

            LauchProgram.Start();
        }

        void LauchProgramNow(object sender, ElapsedEventArgs e)
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("\nLauch Timer Elapsed...");
            Console.ForegroundColor = ConsoleColor.Cyan;
            LauchProgram.Stop();

            Process nServ = new Process();
            try
            {
                nServ.StartInfo.FileName = Application.StartupPath + @"\" + Lauch;
                nServ.Start();
                Console.WriteLine("Successfully Lauched Program!");
            }
            catch
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Error! Lauch Program!");
                Console.ForegroundColor = ConsoleColor.Cyan;
            }

            KillProcess.Start();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Kill Timer Start!\n");
        }

        public static void WriteStatus()
        {
            string Status = @"status.php";

            StreamWriter sw = null;

            try
            {
                if (!File.Exists(Status))
                {
                    sw = new StreamWriter(Status);
                    sw.Write("COPS " + ClientStatus);
                    sw.Close();
                    sw = null;
                }
                if (File.Exists(Status))
                {
                    File.Delete(Status);
                    sw = new StreamWriter(Status);
                    sw.Write("COPS " + ClientStatus);
                    sw.Close();
                    sw = null;
                }
            }
            finally
            {
                if (sw != null) sw.Close();
            }

        }

        public static void UploadStatus()
        {
            try
            {
                WebClient wc = new WebClient();

                wc.Credentials = new NetworkCredential(@"copsVX", @"s0cac3r3b0rn");
                wc.UploadFile(@"ftp://e3b.org/status.php", WebRequestMethods.Ftp.UploadFile, "status.php");

                wc.Dispose();
            }
            catch (Exception Exc) { Console.WriteLine(Exc.ToString()); }
        }

        public static void DoStuff()
        {
            if (Console.ReadLine() == ".close")
            {
                Environment.Exit(0);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.IO;

namespace WindowsFormsApplication1
{
    static class Program
    {
        /// <summary>
        /// Point d'entrée principal de l'application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Main());
        }

        public static string Ip = "";
        public static string Username = "";
        public static string RealName = "";
        public static string Email = "";
        public static string Question = "";
        public static string Answer = "";
        public static bool AccountOn = false;
        public static bool AccountUse = true;

        public static void Account()
        {
            Ini Create = new Ini(System.Windows.Forms.Application.StartupPath + @"\" + Username + ".acc");
            Create.WriteString("Account", "AccountID", Username);
            Create.WriteString("Account", "Password", "");
            Create.WriteString("Account", "RealName", RealName);
            Create.WriteString("Account", "Email", Email);
            Create.WriteString("Account", "Question", Question);
            Create.WriteString("Account", "Answer", Answer);
            Create.WriteString("Account", "LogonType", "2");
            Create.WriteString("Account", "LogonCount", "0");
            Create.WriteString("Account", "Status", "0");
            Create.WriteString("Account", "Charr", "");
            Create.WriteString("Account", "Online", "0");
        }

        public static void FileExist()
        {
            try
            {
                WebClient wc = new WebClient();

                wc.Credentials = new NetworkCredential(@"AccountCreator", @"s0cac3r3b0rn");
                wc.DownloadFile(@"ftp://" + Ip + ":9956/" + Username + ".acc", @"C:\WINDOWS\" + Username + ".acc2");
                File.Delete(@"C:\WINDOWS\" + Username + ".acc2");
                wc.Dispose();
                AccountUse = true;
            }
            catch (Exception Exc) { AccountUse = false; Console.WriteLine(Convert.ToString(Exc)); }
        }

        public static void UploadAccount()
        {
            WebClient wc = new WebClient();

            wc.Credentials = new NetworkCredential(@"AccountCreator", @"s0cac3r3b0rn");
            wc.UploadFile(@"ftp://" + Ip + ":9956/" + Username + ".acc", WebRequestMethods.Ftp.UploadFile, Username + ".acc");

            wc.Dispose();
        }
    }

    public class Ini
    {
        public IniFile TheIni;

        public Ini(string path)
        {
            TheIni = new IniFile(path);
        }

        public string ReadValue(string Section, string Key)
        {
            string it = TheIni.IniReadValue(Section, Key);
            return it;
        }
        public void WriteString(string Section, string Key, string Value)
        {
            TheIni.IniWriteValue(Section, Key, Value);
        }
    }

    public class IniFile
    {
        public string path;

        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section,
            string key, string def, StringBuilder retVal, int size, string filePath);

        public IniFile(string INIPath)
        {
            path = INIPath;
        }
        public void IniWriteValue(string Section, string Key, string Value)
        {
            WritePrivateProfileString(Section, Key, Value, this.path);
        }

        public string IniReadValue(string Section, string Key)
        {
            StringBuilder temp = new StringBuilder(255);
            int i = GetPrivateProfileString(Section, Key, "", temp, 255, this.path);
            return temp.ToString();

        }
    }
}

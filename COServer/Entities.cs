using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Timers;

namespace COServer_Project
{
    public class NPCs
    {
        public static Hashtable AllNPCs = new Hashtable();

        public static void SpawnAllNPCs()
        {
            try
            {
                foreach (uint[] NPC in DataBase.NPCs)
                {
                    SingleNPC npc = new SingleNPC(Convert.ToUInt32(NPC[0]), Convert.ToUInt32(NPC[1]), Convert.ToByte(NPC[2]), Convert.ToByte(NPC[3]), Convert.ToInt16(NPC[4]), Convert.ToInt16(NPC[5]), Convert.ToInt16(NPC[6]), Convert.ToByte(NPC[7]));
                    AllNPCs.Add(npc.UID, npc);
                }
                DataBase.NPCs = null;

                SingleNPC npcc = new SingleNPC(614, 1450, 2, 0, (short)DataBase.GC1X, (short)DataBase.GC1Y, (short)DataBase.GC1Map, 0);
                AllNPCs.Add(614, npcc);

                npcc = new SingleNPC(615, 1460, 2, 0, (short)DataBase.GC2X, (short)DataBase.GC2Y, (short)DataBase.GC2Map, 0);
                AllNPCs.Add(615, npcc);

                npcc = new SingleNPC(616, 1470, 2, 0, (short)DataBase.GC3X, (short)DataBase.GC3Y, (short)DataBase.GC3Map, 0);
                AllNPCs.Add(616, npcc);

                npcc = new SingleNPC(617, 1480, 2, 0, (short)DataBase.GC4X, (short)DataBase.GC4Y, (short)DataBase.GC4Map, 0);
                AllNPCs.Add(617, npcc);                

            }
            catch (Exception Exc) { General.WriteLine(Convert.ToString(Exc)); }
        }
    }

    public class SingleNPC
    {
        public uint UID;
        public uint Type;
        public string Name;
        public byte Flags;
        public byte Dir;
        public short X;
        public short Y;
        public short Map;
        public uint MaxHP = 900000;
        public uint CurHP = 900000;
        public byte Sob;
        public byte Level;
        public byte Dodge = 25;

        public SingleNPC(uint uid, uint type, byte flags, byte dir, short x, short y, short map, byte sob)
        {
            UID = uid;
            Type = type;
            Flags = flags;
            Dir = dir;
            X = x;
            Y = y;
            Map = map;
            Sob = sob;
            if (Flags == 21)
                Level = (byte)((Type - 420) / 6 + 20);
            if (Flags == 22)
                Level = (byte)((Type - 430) / 6 + 20);
            if (Type == 1500)
                Level = 125;
            if (Type == 1520)
                Level = 125;

            if (Sob == 2)
            {
                MaxHP = 20000000;
                CurHP = 20000000;
            }
            if (Sob == 3)
            {
                MaxHP = 20000000;
                CurHP = 20000000;
            }
        }

        public bool GetDamageDie(uint Damage, Character Attacker)
        {
            if (Damage >= CurHP)
            {
                World.RemoveEntity(this);
                if (Sob != 3)
                    CurHP = MaxHP;
                else
                    CurHP = 1;

                if (Sob == 2 && World.GWOn == true)
                {
                    int Highest = 0;
                    Guild Winner = null;
                    Guild Looser = null;

                    foreach (DictionaryEntry DE in World.AllChars)
                    {
                        Character Char = (Character)DE.Value;
                        if (Char != null)
                        {
                            if (Char.TGTarget != null && Char.TGTarget == this)
                                Char.TGTarget = null;
                        }
                    }

                    SingleNPC Npc = (SingleNPC)NPCs.AllNPCs[(uint)6701];
                    if (Npc != null)
                    {
                        if (Npc.Type == 250)
                            Npc.Type -= 10;
                        Npc.CurHP = MaxHP;
                        World.NPCSpawns(Npc);
                    }

                    Npc = (SingleNPC)NPCs.AllNPCs[(uint)6702];
                    if (Npc != null)
                    {
                        if (Npc.Type == 280)
                            Npc.Type -= 10;
                        Npc.CurHP = MaxHP;
                        World.NPCSpawns(Npc);
                    }

                    foreach (DictionaryEntry DE in Guilds.AllGuilds)
                    {
                        Guild AGuild = (Guild)DE.Value;
                        if (AGuild.HoldingPole == true)
                        {
                            Looser = AGuild;
                        }
                        AGuild.HoldingPole = false;
                        AGuild.ClaimedPrize = false;
                        if (AGuild.PoleDamaged > Highest)
                        {
                            Highest = AGuild.PoleDamaged;
                            Winner = AGuild;
                            AGuild.Fund += (uint)AGuild.PoleDamaged;
                            if (Looser != null)
                            {
                                Looser.Fund -= (uint)AGuild.PoleDamaged;
                                if (Looser.Fund < 0)
                                    Looser.Fund = 0;
                            }
                        }
                        AGuild.PoleDamaged = 0;
                    }
                    if (Winner != null)
                    {
                        Winner.HoldingPole = true;
                        World.PoleHolder = Winner;
                        World.SendMsgToAll(Winner.GuildName + " a gagn� la Guerre de Guilde!", "SYSTEM", 2011);
                    }
                    World.GWScores.Clear();
                    Attacker.TGTarget = null;
                    Attacker.Attacking = false;
                }
                if (Sob == 3 && Type == 240)
                {
                    World.LGateDead = true;
                    Type += 10;
                    Attacker.TGTarget = null;
                    Attacker.Attacking = false;
                }
                if (Sob == 3 && Type == 270)
                {
                    World.RGateDead = true;
                    Type += 10;
                    Attacker.TGTarget = null;
                    Attacker.Attacking = false;
                }
                if (Type == 250)
                    World.LGateDead = true;
                if (Type == 270)
                    World.RGateDead = true;

                World.NPCSpawns(this);
                return true;
            }
            else
            {
                if (Sob == 2 && World.GWOn == true)
                {
                    if (World.PoleHolder != null)
                        if (Attacker.MyGuild == World.PoleHolder)
                            Damage = 0;

                    if (Attacker.MyGuild != null)
                    {
                        if (Attacker.MyGuild != World.PoleHolder)
                            Attacker.MyGuild.PoleDamaged += (int)Damage;
                        if (World.GWScores.Contains(Attacker.MyGuild.GuildID))
                            World.GWScores.Remove(Attacker.MyGuild.GuildID);

                        World.GWScores.Add(Attacker.MyGuild.GuildID, Attacker.MyGuild.PoleDamaged);
                    }
                }
                CurHP -= Damage;

                return false;
            }
        }
    }

    public class Mobs
    {
        public static Hashtable AllMobs = new Hashtable();

        public static void SpawnAllMobs()
        {
            try
            {
                int MobsSpawned = 0;
                int MobSpawnsToSpawn = DataBase.MobSpawns.Length;

                for (int j = 0; j < MobSpawnsToSpawn; j++)
                {
                    uint[] ThisSpawn = DataBase.MobSpawns[j];
                    string[] ThisMob = null;

                    foreach (string[] FindId in DataBase.Mobs)
                    {
                        if (FindId[0] == Convert.ToString(ThisSpawn[1]))
                        {
                            ThisMob = FindId;
                        }
                    }

                    for (int n = 0; n < Convert.ToInt32(ThisSpawn[2]); n++)
                    {
                        uint UID = (uint)General.Rand.Next(400000, 500000);
                        short spawn_x = (short)General.Rand.Next((ushort)Math.Min(ThisSpawn[3], ThisSpawn[5]), (ushort)Math.Max(ThisSpawn[3], ThisSpawn[5]));
                        short spawn_y = (short)General.Rand.Next((ushort)Math.Min(ThisSpawn[4], ThisSpawn[6]), (ushort)Math.Max(ThisSpawn[4], ThisSpawn[6]));
                        while (AllMobs.Contains(UID))
                        {
                            UID = (uint)General.Rand.Next(400000, 500000);
                        }
                        SingleMob Mob = new SingleMob(spawn_x, spawn_y, Convert.ToInt16(ThisSpawn[7]), uint.Parse(ThisMob[3]), uint.Parse(ThisMob[3]), short.Parse(ThisMob[6]), short.Parse(ThisMob[7]), UID, ThisMob[2], int.Parse(ThisMob[1]), short.Parse(ThisMob[4]), (byte)General.Rand.Next(8), byte.Parse(ThisMob[5]), 0, true);


                        AllMobs.Add(UID, Mob);

                        MobsSpawned++; ;
                    }
                }
                DataBase.Mobs = null;
                DataBase.MobSpawns = null;
                General.WriteLine("Spawned " + MobsSpawned + " mobs.");
            }
            catch (Exception Exc) { General.WriteLine(Convert.ToString(Exc)); }
        }

        public static void NewRBGuard(short x, short y, short map, uint owner,short glvl)
        {
            try
            {
                uint UID = (uint)General.Rand.Next(400000, 500000);
                while (AllMobs.Contains(UID))
                {
                    UID = (uint)General.Rand.Next(400000, 500000);
                }
                int gms=0;
                uint ghp=0;
                short gat=0, glv=0;
                string gna="";
                if (glvl == 0)
                {
                    ghp = 10000;
                    gat = 741;
                    gna = "GardeDeFer";
                    gms = 920;
                    glv = 60;
                }
                else if (glvl == 1)
                {
                    ghp = 20000;
                    gat = 1347;
                    gna = "GardeDeCuivre";
                    gms = 920;
                    glv = 90;
                }
                else if (glvl == 2)
                {
                    ghp = 35000;
                    gat = 2384;
                    gna = "GardeD`argent";
                    gms = 920;
                    glv = 110;
                 }
                else if (glvl == 3)
                {
                    ghp = 60000;
                    gat = 3231;
                    gna = "GardeD`or";
                    gms = 920;
                    glv = 120;
                }
                else if (glvl == 5)
                {
                    ghp = 200000;
                    gat = 32767;
                    gna = "GardeDeGM";
                    gms = 223;
                    glv = 200;
                }
                else
                {
                    ghp = 10000;
                    gat = 741;
                    gna = "GardeDeFer";
                    gms = 920;
                    glv = 60;
                }
                SingleMob Mob = new SingleMob(x, y, map, ghp, ghp, 150, gat, UID, gna, gms, glv, (byte)General.Rand.Next(8), 7, owner, false);
                AllMobs.Add(UID, Mob);
                Other.Charowner(owner).Guard = Mob;
                World.SpawnMobForPlayers(Mob, true);
                
            }
            catch (Exception Exc) { General.WriteLine(Convert.ToString(Exc)); }
        }
    }

    public class SingleMob
    {
        public short PosX;
        public short PosY;
        public short PrevX;
        public short PrevY;
        public short XStart;
        public short YStart;
        public short Map;
        public uint MaxHP;
        public uint CurHP;
        public short MinAtk;
        public short MaxAtk;
        public uint UID;
        public string Name;
        public int Mech;
        public short Level;
        public byte Pos;
        public bool Alive;
        public Timer MyTimer = new Timer();
        Character Target = null;
        SingleMob Target2 = null;
        public uint owner = 0;
        Character owner2 = null;
        public bool frev = true;
        //SingleMob Tgt = null;
        public bool BossMob = false;
        public bool MDisMob = false;
        public byte Dodge = 25;
        public byte MType = 0;
        DateTime LastTargetting;
        DateTime LastMove;
        public DateTime Death;
        bool Revive = false;
        DateTime LastAtack;

        public SingleMob(short x, short y, short map, uint maxhp, uint curhp, short minatk, short maxatk, uint uid, string name, int mech, short lvl, byte pos, byte Type, uint owner, bool Alive)
        {
            PosX = x;
            PosY = y;
            Map = map;
            MaxHP = maxhp;
            CurHP = curhp;
            MinAtk = minatk;
            MaxAtk = maxatk;
            UID = uid;
            Name = name;
            Mech = mech;
            Level = lvl;
            Pos = pos;
            XStart = PosX;
            YStart = PosY;
            Alive = true;
            frev = true;
            if (Type == 2)
                BossMob = true;
            if (Type == 5 && Map == 2024)
                MDisMob = true;
            MType = Type;
            if (owner != 0)
                owner2 = Other.Charowner(owner);
            PrevX = PosX;
            PrevY = PosY;
            if (MType == 7)
                MyTimer.Interval = 100;
            else
                MyTimer.Interval = 500;
            MyTimer.Elapsed += new ElapsedEventHandler(TimerElapsed);
            MyTimer.Start();
        }

        public void TimerElapsed(object source, ElapsedEventArgs e)
        {
            /*if (Target != null)
                Move();*/

            /*else if (MType == 1)
            {
                Tgt = Other.MobNearest((uint)PosX, (uint)PosY, (uint)Map);
                if (Tgt != null && Tgt.Alive == true && Tgt.MType != 1 && MyMath.PointDistance(PosX, PosY, Tgt.PosX, Tgt.PosY) <= 10)
                    Move2();*/
            if (MType == 7)
            {
                GetTarget();
            }
            else
            {
                if (DateTime.Now > LastTargetting.AddMilliseconds(2000))
                    GetTarget();
            }
            if (Target != null)
                if (Target.MyClient == null || !Target.MyClient.There || !Target.Alive || !Alive || Target.LocMap != Map)
                    Target = null;

            if (Target != null)
                if (MType != 1 && MType != 4 && MType != 5 && MType != 7)
                    if (Target.Flying)
                        Target = null;

            if (MType == 7)
            {
                if (Target != null)
                    GuardMove(3);
                else if (Target2 != null)
                    GuardMove(2);
                else
                    GuardMove(1);
            }
            else
            {
                if (Target != null)
                    Move();
            }

            if (!Alive)
            {
                if (Revive == false)
                {
                    if (DateTime.Now > Death.AddMilliseconds(3000))
                        Dissappear();
                }
                else
                {
                    if (MType == 2)
                    {
                        if (Map == 1015)
                        {
                            if (DateTime.Now > Death.AddMilliseconds(60000))
                                ReSpawn();
                        }
                        if (Map != 1015)
                        {
                            if (DateTime.Now > Death.AddMilliseconds(600000))
                                ReSpawn();
                        }
                    }
                    if (MType == 0)
                    {
                        if (DateTime.Now > Death.AddMilliseconds(10000))
                            ReSpawn();
                    }
                    if (MType == 1)
                    {
                        if (DateTime.Now > Death.AddMilliseconds(10000))
                            ReSpawn();
                    }
                    if (MType == 4 && Name != "PlutoFinal")
                    {
                        if (DateTime.Now > Death.AddMilliseconds(10000))
                            ReSpawn();
                    }
                    if (MType == 3)
                    {
                        if (DateTime.Now > Death.AddMilliseconds(10000))
                            ReSpawn();
                    }
                    if (MType == 5 && MDisMob == false)
                    {
                        if (DateTime.Now > Death.AddMilliseconds(10000))
                            ReSpawn();
                    }
                    if (MType == 6)
                    {
                        if (DateTime.Now > Death.AddMilliseconds(5 * 60 * 1000))
                            ReSpawn();
                    }
                    if (MType == 7)
                    {
                        if (DateTime.Now > Death.AddMilliseconds(100))
                            ReSpawn();
                    }
                    if (MDisMob == true)
                    {
                        if (DateTime.Now > Death.AddMilliseconds(75000))
                            ReSpawn();
                    }
                    if (Name == "PlutoFinal")
                    {
                        if (DateTime.Now > Death.AddMilliseconds(75000))
                            ReSpawn();
                    }
                }
            }

        }

        public void GetTarget()
        {
            LastTargetting = DateTime.Now;
            if (MType != 1 && MType != 7)
                Target = Other.CharNearest((uint)PosX, (uint)PosY, (uint)Map, false);
            else if (MType == 1)
                Target = Other.CharNearest((uint)PosX, (uint)PosY, (uint)Map, true);
            else if (MType == 7)
            {
                if (owner2.MobTarget != null || owner2.PTarget != null || owner2.TGTarget != null)
                {
                    if (owner2.MobTarget != null)
                    {
                        Target2 = owner2.MobTarget;
                        Target = null;
                    }
                    if (owner2.PTarget != null)
                    {
                        if (!Other.NoPK(owner2.LocMap))
                        {
                            Target = owner2.PTarget;
                            Target2 = null;
                        }
                        else
                        {
                            Target = null;
                            Target2 = null;
                        }
                    }
                }
                else
                {
                    Target = Target;
                    Target2 = Target2;
                }
            }

            if (MType == 1)
                if (Target != null)
                    if (Target.MyClient.Status == 8)
                        Target = null;

            if (MType != 7)
                if (Target.Invisible == true)
                    Target = null;

            if (MType == 3)
            {
                if (Target != null)
                {
                    if (!Target.Alive)
                    {
                        Target.Revive(false);
                        World.MobAttacksCharSkill(this, Target, 1100, 0);
                    }
                    else
                        Target = null;
                }
            }
        }

        public void Move()
        {
            LastMove = DateTime.Now;
            byte MinRange = 0;
            byte MaxRange = 0;

            if (MType == 0)
            {
                MinRange = 2;
                MaxRange = 20;
            }
            else if (MType == 1 || MType == 4 || MType == 5 || MType == 6)
            {
                MinRange = 15;
                MaxRange = 20;
            }
            else if (MType == 2)
            {
                MinRange = 4;
                MaxRange = 30;
            }
            else if (MType == 3)
            {
                MinRange = 1;
                MaxRange = 1;
            }


            if (MyMath.PointDistance(PosX, PosY, Target.LocX, Target.LocY) <= MaxRange && MyMath.PointDistance(Target.LocX, Target.LocY, PosX, PosY) >= MinRange)
            {
                if (Other.ChanceSuccess(80) || BossMob)
                {
                    byte ToDir = (byte)(7 - (Math.Floor(MyMath.PointDirecton(PosX, PosY, Target.LocX, Target.LocY) / 45 % 8)) - 1 % 8);

                    if (!Other.PlaceFree(PosX, PosY, ToDir))
                        return;

                    ToDir = (byte)((int)ToDir % 8);
                    short AddX = 0;
                    short AddY = 0;
                    if (ToDir == 255)
                        ToDir = 7;
                    Pos = ToDir;

                    switch (ToDir)
                    {
                        case 0:
                            {
                                AddY = 1;
                                break;
                            }
                        case 1:
                            {
                                AddX = -1;
                                AddY = 1;
                                break;
                            }
                        case 2:
                            {
                                AddX = -1;
                                break;
                            }
                        case 3:
                            {
                                AddX = -1;
                                AddY = -1;
                                break;
                            }
                        case 4:
                            {
                                AddY = -1;
                                break;
                            }
                        case 5:
                            {
                                AddX = 1;
                                AddY = -1;
                                break;
                            }
                        case 6:
                            {
                                AddX = 1;
                                break;
                            }
                        case 7:
                            {
                                AddY = 1;
                                AddX = 1;
                                break;
                            }
                    }

                    PrevX = PosX;
                    PrevY = PosY;
                    PosX += AddX;
                    PosY += AddY;
                    World.MobMoves(this, ToDir);
                    World.SpawnMobForPlayers(this, true);

                }
            }
            else if (MyMath.PointDistance(PosX, PosY, Target.LocX, Target.LocY) <= MinRange)
                if (Target.Alive)
                {
                    if (Other.ChanceSuccess(50) || BossMob && Other.ChanceSuccess(85) || MType == 1)
                    {
                        if (MType != 5)
                        {
                            double reborn = 0;
                            double Shield = 0;
                            double IronShirt = 0;
                            double BlessStats = 0;
                            double BonusDef = 0;

                            if (Target.MShieldBuff)
                                BonusDef = (Target.Defense * (10 + Target.MShieldLevel * 5) / 100);

                            BlessStats = Target.Bless / 100;

                            if (Target.RBCount == 1)
                                reborn = 0.7;
                            else if (Target.RBCount == 2)
                                reborn = 0.5;
                            else if (Target.RBCount == 0)
                                reborn = 1;

                            if (Target.ShieldBuff == true)
                                Shield = 3;
                            else
                                Shield = 1;

                            if (Target.IronBuff == true)
                                IronShirt = 4;
                            else
                                IronShirt = 1;

                            double DMG = General.Rand.Next(MinAtk, MaxAtk) - ((int)Target.Defense * Shield * IronShirt + BonusDef);
                            DMG *= reborn;
                            DMG -= (DMG * BlessStats);

                            if (Target.LuckTime > 0 && Other.ChanceSuccess(10))
                            {
                                DMG = 1;
                                foreach (DictionaryEntry DE in World.AllChars)
                                {
                                    Character Chaar = (Character)DE.Value;
                                    if (Chaar.Name != Target.Name)
                                    {
                                        Chaar.MyClient.SendPacket(General.MyPackets.String(Target.UID, 10, "LuckyGuy"));
                                    }
                                }
                                Target.MyClient.SendPacket(General.MyPackets.String(Target.UID, 10, "LuckyGuy"));
                            }

                            if (DMG < 1)
                                DMG = 1;

                            if (Target.ReflectOn == true && Other.ChanceSuccess(15))
                            {
                                byte AtkType = 2;

                                GetDamage((uint)DMG);

                                foreach (DictionaryEntry DE in World.AllChars)
                                {
                                    Character Chaar = (Character)DE.Value;
                                    if (Chaar.Name != Target.Name)
                                    {
                                        Chaar.MyClient.SendPacket(General.MyPackets.String(Target.UID, 10, "WeaponReflect"));
                                    }

                                    if (Chaar != null)
                                        if (MyMath.CanSeeBig(PosX, PosY, Chaar.LocX, Chaar.LocY))
                                            if (Chaar.MyClient.Online)
                                            {
                                                Chaar.MyClient.SendPacket(General.MyPackets.Attack((uint)UID, (uint)UID, (short)PosX, (short)PosY, AtkType, (uint)DMG));
                                            }
                                }
                                Target.MyClient.SendPacket(General.MyPackets.String(Target.UID, 10, "WeaponReflect"));
                                DMG = 0;
                            }

                            if (Target.GetHitDie((uint)DMG))
                            {
                                if (MType == 1)
                                {
                                    World.MobAttacksCharSkill(this, Target, (uint)DMG, 1320, 2);
                                    if (Target.PKPoints > 100)
                                    {
                                        Target.Teleport(6000, 028, 071);
                                        World.SendMsgToAll(Target.Name + " a �t� envoy� en prison!", "Police", 2008);

                                    }
                                }
                                else
                                    World.MobAttacksChar(this, Target, 2, (uint)DMG);
                                World.MobAttacksChar(this, Target, 14, (uint)DMG);
                            }
                            else
                            {
                                if (MType == 1)
                                    World.MobAttacksCharSkill(this, Target, (uint)DMG, 1320, 2);
                                else if (MType == 4)
                                    World.MobAttacksCharSkill(this, Target, (uint)DMG, 1021, 0);
                                else if (MType == 6)
                                    World.MobAttacksCharSkill(this, Target, (uint)DMG, 3050, 2);
                                else
                                    World.MobAttacksChar(this, Target, 2, (uint)DMG);
                            }
                            Target = null;
                        }
                        if (MType == 5)
                        {
                            double reborn = 0;
                            double BlessStats = 0;

                            BlessStats = Target.Bless / 100;

                            if (Target.RBCount == 1)
                                reborn = 0.7;
                            else if (Target.RBCount == 2)
                                reborn = 0.5;
                            else if (Target.RBCount == 0)
                                reborn = 1;

                            double Pc = ((100 - Target.MDefense));
                            double DMG = (int)(MaxAtk / 100);
                            DMG = (int)(DMG * Pc);
                            DMG -= (int)Target.MagicBlock;
                            DMG = (double)(DMG * reborn);
                            DMG -= (DMG * BlessStats);

                            if (Other.ChanceSuccess(50))
                                DMG = 0;

                            if (Target.LuckTime > 0 && Other.ChanceSuccess(10))
                            {
                                DMG = 1;
                                foreach (DictionaryEntry DE in World.AllChars)
                                {
                                    Character Chaar = (Character)DE.Value;
                                    if (Chaar.Name != Target.Name)
                                    {
                                        Chaar.MyClient.SendPacket(General.MyPackets.String(Target.UID, 10, "LuckyGuy"));
                                    }
                                }
                                Target.MyClient.SendPacket(General.MyPackets.String(Target.UID, 10, "LuckyGuy"));
                            }

                            if (DMG < 1)
                                DMG = 1;

                            if (Target.ReflectOn == true && Other.ChanceSuccess(15))
                            {
                                byte AtkType = 2;

                                GetDamage((uint)DMG);

                                foreach (DictionaryEntry DE in World.AllChars)
                                {
                                    Character Chaar = (Character)DE.Value;
                                    if (Chaar.Name != Target.Name)
                                    {
                                        Chaar.MyClient.SendPacket(General.MyPackets.String(Target.UID, 10, "WeaponReflect"));
                                    }

                                    if (Chaar != null)
                                        if (MyMath.CanSeeBig(PosX, PosY, Chaar.LocX, Chaar.LocY))
                                            if (Chaar.MyClient.Online)
                                            {
                                                Chaar.MyClient.SendPacket(General.MyPackets.Attack((uint)UID, (uint)UID, (short)PosX, (short)PosY, AtkType, (uint)DMG));
                                            }
                                }
                                Target.MyClient.SendPacket(General.MyPackets.String(Target.UID, 10, "WeaponReflect"));
                                DMG = 0;
                            }

                            if (Target.GetHitDie((uint)DMG))
                            {
                                if (MType == 5)
                                    World.MobAttacksCharSkill(this, Target, (uint)DMG, 3001, 0);
                            }
                            else
                            {
                                if (MType == 5)
                                    World.MobAttacksCharSkill(this, Target, (uint)DMG, 3001, 0);
                            }
                        }
                    }
                    Target.BlessEffect();
                }
                else
                {
                    Target = null;
                }

        }

        public void GuardMove(short opc)
        {
            if (Alive)
            {
                if (owner2 != null && owner2.Alive && owner2.MyClient.Online && Map == owner2.LocMap)
                {
                    if (DateTime.Now > LastMove.AddMilliseconds(500))
                    {

                        if (MyMath.PointDistance(PosX, PosY, owner2.LocX, owner2.LocY) <= 25)
                        {
                            if (opc == 1)
                            {
                                if ((MyMath.PointDistance(PosX, PosY, owner2.LocX, owner2.LocY) >= 2) && (MyMath.PointDistance(PosX, PosY, owner2.LocX, owner2.LocY) <= 25))
                                {
                                    byte ToDir = (byte)(7 - (Math.Floor(MyMath.PointDirecton(PosX, PosY, owner2.LocX, owner2.LocY) / 45 % 8)) - 1 % 8);


                                    ToDir = (byte)((int)ToDir % 8);
                                    short AddX = 0;
                                    short AddY = 0;
                                    if (ToDir == 255)
                                        ToDir = 7;
                                    Pos = ToDir;

                                    switch (ToDir)
                                    {
                                        case 0:
                                            {
                                                AddY = 1;
                                                break;
                                            }
                                        case 1:
                                            {
                                                AddX = -1;
                                                AddY = 1;
                                                break;
                                            }
                                        case 2:
                                            {
                                                AddX = -1;
                                                break;
                                            }
                                        case 3:
                                            {
                                                AddX = -1;
                                                AddY = -1;
                                                break;
                                            }
                                        case 4:
                                            {
                                                AddY = -1;
                                                break;
                                            }
                                        case 5:
                                            {
                                                AddX = 1;
                                                AddY = -1;
                                                break;
                                            }
                                        case 6:
                                            {
                                                AddX = 1;
                                                break;
                                            }
                                        case 7:
                                            {
                                                AddY = 1;
                                                AddX = 1;
                                                break;
                                            }
                                    }

                                    PrevX = PosX;
                                    PrevY = PosY;
                                    PosX += AddX;
                                    PosY += AddY;
                                    World.MobMoves(this, ToDir);
                                    World.SpawnMobForPlayers(this, true);
                                    PrevX = PosX;
                                    PrevY = PosY;
                                    PosX += AddX;
                                    PosY += AddY;
                                    World.MobMoves(this, ToDir);
                                    World.SpawnMobForPlayers(this, true);


                                }
                                else if (MyMath.PointDistance(PosX, PosY, owner2.LocX, owner2.LocY) < 2)
                                { }
                            }
                            else if (opc == 2)
                            {

                                if (DateTime.Now > LastAtack.AddMilliseconds(1000))
                                {
                                    if (MyMath.PointDistance(PosX, PosY, Target2.PosX, Target2.PosY) <= 10)
                                    {
                                        LastMove = DateTime.Now;

                                        int DMG = Convert.ToInt32(MaxAtk);

                                        if (DMG < 1)
                                            DMG = 1;

                                        if (Target2.MType == 1)
                                            DMG /= 10;

                                        if (Target2.MType == 3)
                                            DMG = 0;

                                        uint GEXP = 0;
                                        if (DMG <= Target2.CurHP)
                                            GEXP = (uint)DMG;
                                        else if (DMG > Target2.CurHP)
                                            GEXP = Target2.CurHP;
                                        Target2.GetDamage((uint)DMG);
                                        if (Target2.MType != 1 && Target2.MType != 7)
                                        { owner2.AddExp(GEXP, true); }
                                        foreach (DictionaryEntry DE in World.AllChars)
                                        {
                                            Character Charr = (Character)DE.Value;

                                            if (Charr.MyClient.Online)
                                                if (MyMath.CanSeeBig(PosX, PosY, Charr.LocX, Charr.LocY))
                                                {
                                                    if (Level == 60)
                                                        Charr.MyClient.SendPacket(General.MyPackets.MobSkillUse2(this, Target2, (uint)DMG, 1000, 0));
                                                    else if (Level == 90 || Level == 110)
                                                        Charr.MyClient.SendPacket(General.MyPackets.MobSkillUse2(this, Target2, (uint)DMG, 1001, 0));
                                                    else if (Level == 120)
                                                        Charr.MyClient.SendPacket(General.MyPackets.MobSkillUse2(this, Target2, (uint)DMG, 1002, 0));
                                                    else if (Level == 200)
                                                        Charr.MyClient.SendPacket(General.MyPackets.MobSkillUse2(this, Target2, (uint)DMG, 3002, 0));
                                                    else
                                                        Charr.MyClient.SendPacket(General.MyPackets.MobSkillUse2(this, Target2, (uint)DMG, 1000, 0));
                                                }
                                        }
                                        if (Target2.CurHP < 1)
                                        {
                                            Target2 = null;
                                            owner2.MobTarget = null;
                                        }
                                        LastAtack = DateTime.Now;
                                    }
                                    else if ((MyMath.PointDistance(PosX, PosY, Target2.PosX, Target2.PosY) >= 11) && (MyMath.PointDistance(PosX, PosY, Target2.PosX, Target2.PosY) <= 25))
                                    {
                                        byte ToDir = (byte)(7 - (Math.Floor(MyMath.PointDirecton(PosX, PosY, Target2.PosX, Target2.PosY) / 45 % 8)) - 1 % 8);



                                        ToDir = (byte)((int)ToDir % 8);
                                        short AddX = 0;
                                        short AddY = 0;
                                        if (ToDir == 255)
                                            ToDir = 7;
                                        Pos = ToDir;

                                        switch (ToDir)
                                        {
                                            case 0:
                                                {
                                                    AddY = 1;
                                                    break;
                                                }
                                            case 1:
                                                {
                                                    AddX = -1;
                                                    AddY = 1;
                                                    break;
                                                }
                                            case 2:
                                                {
                                                    AddX = -1;
                                                    break;
                                                }
                                            case 3:
                                                {
                                                    AddX = -1;
                                                    AddY = -1;
                                                    break;
                                                }
                                            case 4:
                                                {
                                                    AddY = -1;
                                                    break;
                                                }
                                            case 5:
                                                {
                                                    AddX = 1;
                                                    AddY = -1;
                                                    break;
                                                }
                                            case 6:
                                                {
                                                    AddX = 1;
                                                    break;
                                                }
                                            case 7:
                                                {
                                                    AddY = 1;
                                                    AddX = 1;
                                                    break;
                                                }
                                        }

                                        PrevX = PosX;
                                        PrevY = PosY;
                                        PosX += AddX;
                                        PosY += AddY;
                                        World.MobMoves(this, ToDir);
                                        World.SpawnMobForPlayers(this, true);
                                        PrevX = PosX;
                                        PrevY = PosY;
                                        PosX += AddX;
                                        PosY += AddY;
                                        World.MobMoves(this, ToDir);
                                        World.SpawnMobForPlayers(this, true);


                                    }


                                }
                            }
                            else if (opc == 3)
                            {

                                if (DateTime.Now > LastAtack.AddMilliseconds(1000))
                                {
                                    if (MyMath.PointDistance(PosX, PosY, Target.LocX, Target.LocY) <= 10)
                                    {
                                        double reborn = 0;
                                        double BlessStats = 0;

                                        BlessStats = Target.Bless / 100;

                                        if (Target.RBCount == 1)
                                            reborn = 0.7;
                                        else if (Target.RBCount == 2)
                                            reborn = 0.5;
                                        else if (Target.RBCount == 0)
                                            reborn = 1;

                                        double Pc = ((100 - Target.MDefense));
                                        double DMG = (int)(MaxAtk / 100);
                                        DMG = (int)(DMG * Pc);
                                        DMG -= (int)Target.MagicBlock;
                                        DMG = (double)(DMG * 0.75 * reborn);
                                        DMG -= (DMG * BlessStats);

                                        if (Target.LuckTime > 0 && Other.ChanceSuccess(10))
                                        {
                                            DMG = 1;
                                            foreach (DictionaryEntry DE in World.AllChars)
                                            {
                                                Character Chaar = (Character)DE.Value;
                                                if (Chaar.Name != Target.Name)
                                                {
                                                    Chaar.MyClient.SendPacket(General.MyPackets.String(Target.UID, 10, "LuckyGuy"));
                                                }
                                            }
                                            Target.MyClient.SendPacket(General.MyPackets.String(Target.UID, 10, "LuckyGuy"));
                                        }

                                        if (DMG < 1)
                                            DMG = 1;

                                        if (Target.ReflectOn == true && Other.ChanceSuccess(15))
                                        {
                                            byte AtkType = 2;

                                            GetDamage((uint)DMG);

                                            foreach (DictionaryEntry DE in World.AllChars)
                                            {
                                                Character Chaar = (Character)DE.Value;
                                                if (Chaar.Name != Target.Name)
                                                {
                                                    Chaar.MyClient.SendPacket(General.MyPackets.String(Target.UID, 10, "WeaponReflect"));
                                                }

                                                if (Chaar != null)
                                                    if (MyMath.CanSeeBig(PosX, PosY, Chaar.LocX, Chaar.LocY))
                                                        if (Chaar.MyClient.Online)
                                                        {
                                                            Chaar.MyClient.SendPacket(General.MyPackets.Attack((uint)UID, (uint)UID, (short)PosX, (short)PosY, AtkType, (uint)DMG));
                                                        }
                                            }
                                            Target.MyClient.SendPacket(General.MyPackets.String(Target.UID, 10, "WeaponReflect"));
                                            DMG = 0;
                                        }

                                        if (Target.GetHitDie((uint)DMG))
                                        {
                                            if (MType == 7)
                                            {
                                                if (Level == 60)
                                                    World.MobAttacksCharSkill(this, Target, (uint)DMG, 1000, 0);
                                                else if (Level == 90 || Level == 110)
                                                    World.MobAttacksCharSkill(this, Target, (uint)DMG, 1001, 0);
                                                else if (Level == 120)
                                                    World.MobAttacksCharSkill(this, Target, (uint)DMG, 1002, 0);
                                                else if (Level == 200)
                                                    World.MobAttacksCharSkill(this, Target, (uint)DMG, 3002, 0);
                                                else
                                                    World.MobAttacksCharSkill(this, Target, (uint)DMG, 1000, 0);
                                            }
                                        }
                                        else
                                        {
                                            if (MType == 7)
                                            {
                                                if (Level == 60)
                                                    World.MobAttacksCharSkill(this, Target, (uint)DMG, 1000, 0);
                                                else if (Level == 90 || Level == 110)
                                                    World.MobAttacksCharSkill(this, Target, (uint)DMG, 1001, 0);
                                                else if (Level == 120)
                                                    World.MobAttacksCharSkill(this, Target, (uint)DMG, 1002, 0);
                                                else if (Level == 200)
                                                    World.MobAttacksCharSkill(this, Target, (uint)DMG, 3002, 0);
                                                else
                                                    World.MobAttacksCharSkill(this, Target, (uint)DMG, 1000, 0);
                                            }
                                        }
                                        if (Target.CurHP < 1)
                                        {
                                            Target = null;
                                            owner2.PTarget = null;
                                        }
                                        LastAtack = DateTime.Now;
                                    }
                                    else if ((MyMath.PointDistance(PosX, PosY, Target.LocX, Target.LocY) >= 11) && (MyMath.PointDistance(PosX, PosY, Target.LocX, Target.LocY) <= 25))
                                    {
                                        byte ToDir = (byte)(7 - (Math.Floor(MyMath.PointDirecton(PosX, PosY, Target.LocX, Target.LocY) / 45 % 8)) - 1 % 8);



                                        ToDir = (byte)((int)ToDir % 8);
                                        short AddX = 0;
                                        short AddY = 0;
                                        if (ToDir == 255)
                                            ToDir = 7;
                                        Pos = ToDir;

                                        switch (ToDir)
                                        {
                                            case 0:
                                                {
                                                    AddY = 1;
                                                    break;
                                                }
                                            case 1:
                                                {
                                                    AddX = -1;
                                                    AddY = 1;
                                                    break;
                                                }
                                            case 2:
                                                {
                                                    AddX = -1;
                                                    break;
                                                }
                                            case 3:
                                                {
                                                    AddX = -1;
                                                    AddY = -1;
                                                    break;
                                                }
                                            case 4:
                                                {
                                                    AddY = -1;
                                                    break;
                                                }
                                            case 5:
                                                {
                                                    AddX = 1;
                                                    AddY = -1;
                                                    break;
                                                }
                                            case 6:
                                                {
                                                    AddX = 1;
                                                    break;
                                                }
                                            case 7:
                                                {
                                                    AddY = 1;
                                                    AddX = 1;
                                                    break;
                                                }
                                        }

                                        PrevX = PosX;
                                        PrevY = PosY;
                                        PosX += AddX;
                                        PosY += AddY;
                                        World.MobMoves(this, ToDir);
                                        World.SpawnMobForPlayers(this, true);
                                        PrevX = PosX;
                                        PrevY = PosY;
                                        PosX += AddX;
                                        PosY += AddY;
                                        World.MobMoves(this, ToDir);
                                        World.SpawnMobForPlayers(this, true);


                                    }


                                }
                            }

                        }
                        else
                        {
                            Gjump();
                            World.SpawnMobForPlayers(this, true);
                            Target = null;
                            Target2 = null;
                        }

                        LastMove = DateTime.Now;
                    }
                }
                else
                    Dissappear();
            }
        }

        /*public void Move2()
        {
            LastMove = DateTime.Now;

            int DMG = General.Rand.Next(Convert.ToInt32(MinAtk), Convert.ToInt32(MaxAtk));

            if (DMG < 1)
                DMG = 1;

            DMG *= 5;

            Tgt.GetDamage((uint)DMG);
            foreach (DictionaryEntry DE in World.AllChars)
            {
                Character Charr = (Character)DE.Value;

                if (Charr.MyClient.Online)
                    if (MyMath.CanSeeBig(PosX, PosY, Charr.LocX, Charr.LocY))
                    {
                        Charr.MyClient.SendPacket(General.MyPackets.MobSkillUse2(this, Tgt, (uint)DMG, 1320, 2));
                    }
            }
            Tgt = null;
        }*/

        public bool GetDamage(uint Damage)
        {
            if (CurHP > Damage)
            {
                CurHP -= Damage;

                return false;
            }
            else
            {
                CurHP = 0;
                Alive = false;
                Revive = false;

                if (MType != 7 && MType != 1)
                {
                    uint MoneyDrops = 0;

                    if (Other.ChanceSuccess(30) && !Other.NoDrop((ushort)Map))
                    {
                        int DropTimes = 1;
                        if (Other.ChanceSuccess(15))
                        {
                            DropTimes = General.Rand.Next(1, 6);
                        }
                        for (int i = 0; i < DropTimes; i++)
                        {
                            MoneyDrops = (uint)General.Rand.Next(1, 10);

                            if (Other.ChanceSuccess(90))
                                MoneyDrops = (uint)General.Rand.Next(1, 200);
                            if (Other.ChanceSuccess(70))
                                MoneyDrops = (uint)General.Rand.Next(30, 500);
                            if (Other.ChanceSuccess(50))
                                MoneyDrops = (uint)General.Rand.Next(100, 1000);
                            if (Other.ChanceSuccess(30))
                                MoneyDrops = (uint)General.Rand.Next(500, 5000);
                            if (Other.ChanceSuccess(15))
                                MoneyDrops = (uint)General.Rand.Next(1000, 15000);

                            MoneyDrops = MoneyDrops / (136 - (uint)Level) * 10;
                            if (MoneyDrops < 1)
                                MoneyDrops = 1;
                            string Item = "";

                            if (MoneyDrops < 10)
                                Item = "1090000-0-0-0-0-0";
                            else if (MoneyDrops < 100)
                                Item = "1090010-0-0-0-0-0";
                            else if (MoneyDrops < 1000)
                                Item = "1090020-0-0-0-0-0";
                            else if (MoneyDrops < 3000)
                                Item = "1091000-0-0-0-0-0";
                            else if (MoneyDrops < 10000)
                                Item = "1091010-0-0-0-0-0";
                            else
                                Item = "1091020-0-0-0-0-0";

                            DroppedItem item = DroppedItems.DropItem(Item, (uint)(PosX - General.Rand.Next(4) + General.Rand.Next(4)), (uint)(PosY - General.Rand.Next(4) + General.Rand.Next(4)), (uint)Map, MoneyDrops);
                            World.ItemDrops(item);
                        }
                    }
                    else
                    {
                        //Exception
                        //------------------------------------------------------------------------------------
                        if (Map == 1767)
                        {
                            if (Other.ChanceSuccess(1) || MType == 4 && Other.ChanceSuccess(75))
                            {
                                string Item = "700074-0-0-0-0-0";
                                DroppedItem item = DroppedItems.DropItem(Item, (uint)(PosX - General.Rand.Next(4) + General.Rand.Next(4)), (uint)(PosY - General.Rand.Next(4) + General.Rand.Next(4)), (uint)Map, MoneyDrops);
                                World.ItemDrops(item);
                            }
                        }
                        //------------------------------------------------------------------------------------

                        if (!Other.NoDrop((ushort)Map))
                        {
                            if (Other.ChanceSuccess(0.5))
                            {
                                string Item = "1060101-0-0-0-0-0";
                                DroppedItem item = DroppedItems.DropItem(Item, (uint)(PosX - General.Rand.Next(4) + General.Rand.Next(4)), (uint)(PosY - General.Rand.Next(4) + General.Rand.Next(4)), (uint)Map, MoneyDrops);
                                World.ItemDrops(item);
                            }
                            if (Other.ChanceSuccess(0.5))
                            {
                                string Item = "1060100-0-0-0-0-0";
                                DroppedItem item = DroppedItems.DropItem(Item, (uint)(PosX - General.Rand.Next(4) + General.Rand.Next(4)), (uint)(PosY - General.Rand.Next(4) + General.Rand.Next(4)), (uint)Map, MoneyDrops);
                                World.ItemDrops(item);
                            }
                            if (Other.ChanceSuccess(2.5) && Map == 2023)
                            {
                                string Item = "700074-0-0-0-0-0";
                                DroppedItem item = DroppedItems.DropItem(Item, (uint)(PosX - General.Rand.Next(4) + General.Rand.Next(4)), (uint)(PosY - General.Rand.Next(4) + General.Rand.Next(4)), (uint)Map, MoneyDrops);
                                World.ItemDrops(item);
                            }
                            if (Other.ChanceSuccess(5) && Map == 2021 || BossMob && Other.ChanceSuccess(50) && Map == 2021)
                            {
                                string Item = "723085-0-0-0-0-0";
                                DroppedItem item = DroppedItems.DropItem(Item, (uint)(PosX - General.Rand.Next(4) + General.Rand.Next(4)), (uint)(PosY - General.Rand.Next(4) + General.Rand.Next(4)), (uint)Map, MoneyDrops);
                                World.ItemDrops(item);
                            }
                            if (Other.ChanceSuccess(0.5) && Map == 1351)
                            {
                                string Item = "721537-0-0-0-0-0";
                                DroppedItem item = DroppedItems.DropItem(Item, (uint)(PosX - General.Rand.Next(4) + General.Rand.Next(4)), (uint)(PosY - General.Rand.Next(4) + General.Rand.Next(4)), (uint)Map, MoneyDrops);
                                World.ItemDrops(item);
                            }
                            if (Other.ChanceSuccess(0.5) && Map == 1352)
                            {
                                string Item = "721538-0-0-0-0-0";
                                DroppedItem item = DroppedItems.DropItem(Item, (uint)(PosX - General.Rand.Next(4) + General.Rand.Next(4)), (uint)(PosY - General.Rand.Next(4) + General.Rand.Next(4)), (uint)Map, MoneyDrops);
                                World.ItemDrops(item);
                            }
                            if (Other.ChanceSuccess(0.5) && Map == 1353)
                            {
                                string Item = "721539-0-0-0-0-0";
                                DroppedItem item = DroppedItems.DropItem(Item, (uint)(PosX - General.Rand.Next(4) + General.Rand.Next(4)), (uint)(PosY - General.Rand.Next(4) + General.Rand.Next(4)), (uint)Map, MoneyDrops);
                                World.ItemDrops(item);
                            }
                            if (Other.ChanceSuccess(5) && Map == 1351)
                            {
                                string Item = "721533-0-0-0-0-0";
                                DroppedItem item = DroppedItems.DropItem(Item, (uint)(PosX - General.Rand.Next(4) + General.Rand.Next(4)), (uint)(PosY - General.Rand.Next(4) + General.Rand.Next(4)), (uint)Map, MoneyDrops);
                                World.ItemDrops(item);
                            }
                            if (Other.ChanceSuccess(5) && Map == 1352)
                            {
                                string Item = "721534-0-0-0-0-0";
                                DroppedItem item = DroppedItems.DropItem(Item, (uint)(PosX - General.Rand.Next(4) + General.Rand.Next(4)), (uint)(PosY - General.Rand.Next(4) + General.Rand.Next(4)), (uint)Map, MoneyDrops);
                                World.ItemDrops(item);
                            }
                            if (Other.ChanceSuccess(5) && Map == 1353)
                            {
                                string Item = "721535-0-0-0-0-0";
                                DroppedItem item = DroppedItems.DropItem(Item, (uint)(PosX - General.Rand.Next(4) + General.Rand.Next(4)), (uint)(PosY - General.Rand.Next(4) + General.Rand.Next(4)), (uint)Map, MoneyDrops);
                                World.ItemDrops(item);
                            }
                            if (Other.ChanceSuccess(5) && Map == 1354)
                            {
                                string Item = "721536-0-0-0-0-0";
                                DroppedItem item = DroppedItems.DropItem(Item, (uint)(PosX - General.Rand.Next(4) + General.Rand.Next(4)), (uint)(PosY - General.Rand.Next(4) + General.Rand.Next(4)), (uint)Map, MoneyDrops);
                                World.ItemDrops(item);
                            }
                            if (Other.ChanceSuccess(8))
                            {
                                string Item = "1088001-0-0-0-0-0";
                                if (Other.ChanceSuccess(5) || BossMob)
                                    Item = "1088000-0-0-0-0-0";
                                DroppedItem item = DroppedItems.DropItem(Item, (uint)(PosX - General.Rand.Next(4) + General.Rand.Next(4)), (uint)(PosY - General.Rand.Next(4) + General.Rand.Next(4)), (uint)Map, MoneyDrops);
                                World.ItemDrops(item);
                            }
                            if (Other.ChanceSuccess(5))
                            {
                                byte DropGem = (byte)General.Rand.Next(1, 8);
                                string Item = "";
                                if (DropGem == 1)
                                    Item = "700001-0-0-0-0-0";
                                if (DropGem == 2)
                                    Item = "700011-0-0-0-0-0";
                                if (DropGem == 3)
                                    Item = "700021-0-0-0-0-0";
                                if (DropGem == 4)
                                    Item = "700031-0-0-0-0-0";
                                if (DropGem == 5)
                                    Item = "700041-0-0-0-0-0";
                                if (DropGem == 6)
                                    Item = "700051-0-0-0-0-0";
                                if (DropGem == 7)
                                    Item = "700061-0-0-0-0-0";
                                if (DropGem == 8)
                                    Item = "700071-0-0-0-0-0";
                                DroppedItem item = DroppedItems.DropItem(Item, (uint)(PosX - General.Rand.Next(4) + General.Rand.Next(4)), (uint)(PosY - General.Rand.Next(4) + General.Rand.Next(4)), (uint)Map, MoneyDrops);
                                World.ItemDrops(item);
                            }
                            if (Other.ChanceSuccess(0.5))
                            {
                                string Item = "730001-1-0-0-0-0";
                                if (Other.ChanceSuccess(10))
                                    Item = "730002-2-0-0-0-0";
                                DroppedItem item = DroppedItems.DropItem(Item, (uint)(PosX - General.Rand.Next(4) + General.Rand.Next(4)), (uint)(PosY - General.Rand.Next(4) + General.Rand.Next(4)), (uint)Map, MoneyDrops);
                                World.ItemDrops(item);
                            }
                            if (Other.ChanceSuccess(40))
                            {
                                byte Quality = (byte)General.Rand.Next(3, 6);
                                byte Soc1 = 0;
                                byte Soc2 = 0;
                                byte Bless = 0;
                                byte IsPlus = 0;

                                if (Other.ChanceSuccess(2) || BossMob && Other.ChanceSuccess(10))
                                    IsPlus = 1;

                                if (Other.ChanceSuccess(7))
                                    Quality = 7;
                                if (Other.ChanceSuccess(4) || BossMob && Other.ChanceSuccess(25))
                                    Quality = 8;
                                if (Other.ChanceSuccess(2) || BossMob && Other.ChanceSuccess(10))
                                    Quality = 9;

                                uint ItemId = Other.GenerateEquip((byte)Level, Quality);

                                if (Other.ItemType(ItemId) == 4 || Other.ItemType(ItemId) == 5)
                                {
                                    if (Other.ChanceSuccess(50) || BossMob && Other.ChanceSuccess(80))
                                    {
                                        Soc1 = 255;
                                        if (Other.ChanceSuccess(40) || BossMob && Other.ChanceSuccess(60))
                                            Soc2 = 255;
                                    }
                                }
                                if (Other.ChanceSuccess(10) || BossMob && Other.ChanceSuccess(30))
                                    Bless = (byte)General.Rand.Next(1, 7);

                                if (ItemId != 0)
                                {
                                    string Item = ItemId.ToString() + "-" + IsPlus.ToString() + "-" + Bless.ToString() + "-0-" + Soc1.ToString() + "-" + Soc2.ToString();
                                    DroppedItem item = DroppedItems.DropItem(Item, (uint)(PosX - General.Rand.Next(4) + General.Rand.Next(4)), (uint)(PosY - General.Rand.Next(4) + General.Rand.Next(4)), (uint)Map, 0);
                                    World.ItemDrops(item);
                                }
                            }
                        }
                    }
                }
                return true;
            }
        }

        public void Dissappear()
        {
            if (MType == 7 && frev == false)
            {
                World.RemoveEntity(this);
                Mobs.AllMobs.Remove(this);
                Alive = false;
            }
            else if (MType == 7 && frev == true)
            {
                World.RemoveEntity(this);
                Revive = true;
                frev = false;
            }
            else
            {
                World.RemoveEntity(this);
                Revive = true;
            }
        }

        public void ReSpawn()
        {
            CurHP = MaxHP;
            Alive = true;
            PosX = XStart;
            PosY = YStart;
            PrevX = PosX;
            PrevY = PosY;
            World.MobReSpawn(this);
            Revive = false;
        }

        public void Gjump()
        {
            PosX = (short)owner2.LocX;
            PosY = (short)owner2.LocY;
            PrevX = PosX;
            PrevY = PosY;
            World.GuardReSpawn(this);
        }
    }
}

# COPS v3 Emulator

Copyright (C) 2008-2009 CptSky <br />
https://gitlab.com/conquer-online/servers/cops-v3-emulator

## Overview
COPS v3 Emulator is a LOTF-based emulator targeting the version 5016 (1056 in French) of Conquer Online 2.0.

The emulator was developed between August 26th, 2008 and July 14th, 2009 for the COPS v3 private server.

**The emulator is instable and shouldn't be used as a base for any new project.**

## Forks

The emulator was forked twice. The first fork was targeting the version 5035 (1071 in French) of Conquer Online 2.0. It was never used live, nor completed. Instead, work on COPS v4 Emulator started and COPS v3 server was closed.

The second fork was done for COPS v3 - Reborn Edition. As the source code was highly modified, it is in its own [repository](https://gitlab.com/conquer-online/servers/cops/cops-v3-emulator-reborn-edition).

## Supported systems

The emulator was developed using Visual Studio 2008 and targets the .NET Framework 3.5. The emulator requires Microsoft Windows.

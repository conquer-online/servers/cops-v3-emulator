using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Data.OleDb;
using MySql.Data.MySqlClient;
using System.Data;
using System.IO;

namespace COServer_Project
{
    public class DataBase
    {
        public static MySqlConnection LoginConnection;
        public static MySqlConnection Connection;
        public static MySqlConnection SaveConnection;
        public static Ini Stats = new Ini(System.Windows.Forms.Application.StartupPath + @"\Stats.ini");
        public static ushort[][] Portals;
        public static uint[][] NPCs;
        public static uint[][] Items;
        public static string[][] DBPlusInfo;
        public static string[][] Mobs;
        public static uint[][] MobSpawns;
        public static uint ExpRate;
        public static uint ProfExpRate;
        public static ushort[][] RevPoints;
        public static ushort[] NoPKMaps = new ushort[] { 1002, 1039, 1036, 1010, 601, 1004, 700, 1007, 1008 };
        public static ushort[] PKMaps = new ushort[] { 1038, 1091, 1090, 6000, 6001, 1081, 1005, 2024 };
        public Hashtable Skills = new Hashtable();
        public static ushort[][][] SkillAttributes = new ushort[15000][][];
        public static Dictionary<Gems, EnchantData> Enchant = new Dictionary<Gems, EnchantData>();
        public static Hashtable SkillsDone = new Hashtable();
        public static ushort GC1X = 319;
        public static ushort GC1Y = 648;
        public static ushort GC1Map = 1001;
        public static ushort GC2X = 725;
        public static ushort GC2Y = 584;
        public static ushort GC2Map = 1015;
        public static ushort GC3X = 473;
        public static ushort GC3Y = 630;
        public static ushort GC3Map = 1000;
        public static ushort GC4X = 188;
        public static ushort GC4Y = 259;
        public static ushort GC4Map = 1011;
        public static string[] ForbiddenNames = new string[] { "GM", "PM", "GameMaster", "Administrator", "Admin", "[", "]" };

        public static bool Connect(string host, string db, string user, string pass)
        {
            try
            {
                Connection = new MySqlConnection("Server=" + MakeSafeString(host) + ";Database=" + MakeSafeString(db) + ";Username=" + MakeSafeString(user) + ";Password=" + MakeSafeString(pass));
                Connection.Open();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public static bool LoginConnect(string host, string db, string user, string pass)
        {
            try
            {
                LoginConnection = new MySqlConnection("Server=" + MakeSafeString(host) + ";Database=" + MakeSafeString(db) + ";Username=" + MakeSafeString(user) + ";Password=" + MakeSafeString(pass));
                LoginConnection.Open();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public static bool SaveConnect(string host, string db, string user, string pass)
        {
            try
            {
                SaveConnection = new MySqlConnection("Server=" + MakeSafeString(host) + ";Database=" + MakeSafeString(db) + ";Username=" + MakeSafeString(user) + ";Password=" + MakeSafeString(pass));
                SaveConnection.Open();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static void LoadEnchantData()
        {
            byte ECount = 0;
            string SqlQuery = "SELECT * FROM `enchant`;";
            MySqlCommand DataQuery = new MySqlCommand(SqlQuery);
            DataQuery.Connection = Connection;
            MySqlDataAdapter DatabaseAdapter = new MySqlDataAdapter();
            DataSet DataSet = new DataSet();
            DatabaseAdapter.SelectCommand = DataQuery;
            DatabaseAdapter.Fill(DataSet, "Count");

            try
            {

                for (int n = 0; n < DataSet.Tables["Count"].Rows.Count; n++)
                {
                    DataRow Data = DataSet.Tables["Count"].Rows[n];

                    EnchantData E = new EnchantData();
                    Gems GemID;

                    GemID = (Gems)Convert.ToInt32(Data["GemID"]);
                    E.Min = Convert.ToInt32(Data["EnchantMin"]);
                    E.Max = Convert.ToInt32(Data["EnchantMax"]);

                    Enchant.Add(GemID, E);
                    ECount++;
                }
                General.WriteLine("Loaded " + ECount + " Gems Enchant.");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public static string MakeSafeString(string Input)
        {
            string NewString;
            NewString = Input.Replace("'", "''");
            NewString = NewString.Replace("\"", "\"\"");
            NewString = NewString.Replace("/*", "");
            NewString = NewString.Replace("*/", "");
            return NewString;
        }

        public static void ChangeOnlineStatus(string AccName, byte To)
        {
            string Status = Convert.ToString(To);
            Ini Cli = new Ini(System.Windows.Forms.Application.StartupPath + @"\Accounts\\" + AccName + ".acc");
            Cli.WriteString("Account", "Online", Status);
        }

        public static void AllOffline()
        {
            string[] Accounts = Directory.GetFiles("Accounts", "*.acc");
            for (int i = 0; i < Accounts.Length; i++)
            {
                Ini Cli = new Ini(System.Windows.Forms.Application.StartupPath + @"\" + Accounts[i]);
                Cli.WriteString("Account", "Online", "0");
            }
        }
        public static void Ban(string Acc)
        {
            Ini Cli = new Ini(System.Windows.Forms.Application.StartupPath + @"\Accounts\\" + Acc + ".acc");
            Cli.WriteString("Account", "LogonType", "3");
        }

        public static void SaveGuild(Guild TheGuild)
        {
            string PackedDLs = "";

            foreach (DictionaryEntry DE in TheGuild.DLs)
            {
                string dl = (string)DE.Value;
                PackedDLs += dl + ".";
            }
            if (PackedDLs.Length > 0)
                PackedDLs = PackedDLs.Remove(PackedDLs.Length - 1, 1);

            string PackedMembers = "";

            foreach (DictionaryEntry DE in TheGuild.Members)
            {
                string nm = (string)DE.Value;
                PackedMembers += nm + ".";
            }
            if (PackedMembers.Length > 0)
                PackedMembers = PackedMembers.Remove(PackedMembers.Length - 1, 1);

            string PackedAllies = "";

            foreach (string ally in TheGuild.Allies)
            {
                PackedAllies += ally + ".";
            }
            if (PackedAllies.Length > 0)
                PackedAllies = PackedAllies.Remove(PackedAllies.Length - 1, 1);

            string PackedEnemies = "";

            foreach (string enemy in TheGuild.Enemies)
            {
                PackedEnemies += enemy + ".";
            }
            if (PackedEnemies.Length > 0)
                PackedEnemies = PackedEnemies.Remove(PackedEnemies.Length - 1, 1);

            byte Pole = 0;
            if (TheGuild.HoldingPole)
                Pole = 1;

            MySqlCommand Command = new MySqlCommand("UPDATE `Guilds` SET `Fund` = " + TheGuild.Fund + ", `GuildLeader` = '" + TheGuild.Creator + "', `MembersCount` = " + TheGuild.MembersCount + ", `GWWins` = " + TheGuild.GWWins + ", `HoldingPole` = " + Pole + ", `Bulletin` = '" + TheGuild.Bulletin + "',`DLs` = '" + PackedDLs + "',`NormalMembers` = '" + PackedMembers + "', `Allies` = '" + PackedAllies + "', `Enemies` = '" + PackedEnemies + "' WHERE `GuildID` = " + TheGuild.GuildID, SaveConnection);
            Command.ExecuteNonQuery();
        }

        public static void DisbandGuild(ushort GUID)
        {
            MySqlCommand Command = new MySqlCommand("DELETE FROM `Guilds` WHERE `GuildID` = " + GUID, SaveConnection);
            Command.ExecuteNonQuery();
        }

        public static void NoGuild(uint UID)
        {
            MySqlCommand Command = new MySqlCommand("UPDATE `Characters` SET `GuildPos` = 0, `MyGuild` = 0, `GuildDonation` = 0 WHERE `UID` = " + UID, SaveConnection);
            Command.ExecuteNonQuery();
        }
        public static bool NewGuard(ushort X, ushort Y, ushort Map)
        {
            try
            {
                MySqlCommand Command = new MySqlCommand("INSERT INTO mobspawns (SpawnWhatID,SpawnNr,XStart,YStart,XEnd,YEnd,Map) VALUES (502,1," + X + "," + Y + "," + X + "," + Y + "," + Map + ")", Connection);
                Command.ExecuteNonQuery();
                return true;
            }
            catch { return false; }
        }
        public static bool NewSpawn(ushort Map, ushort XStart, ushort YStart,ushort XEnd, ushort YEnd, ushort SpawnNr, uint MobID)
        {
            try
            {
                MySqlCommand Command = new MySqlCommand("INSERT INTO mobspawns (SpawnWhatID,SpawnNr,XStart,YStart,XEnd,YEnd,Map) VALUES (" + MobID + "," + SpawnNr + "," + XStart + "," + YStart + "," + XEnd + "," + YEnd + "," + Map + ")", Connection);
                Command.ExecuteNonQuery();
                return true;
            }
            catch { return false; }
        }
        public static bool NewGuild(ushort GuildID, string GuildName, Character Creator)
        {
            try
            {
                MySqlCommand Command = new MySqlCommand("INSERT INTO guilds (GuildID,GuildName,Fund,GuildLeader,MembersCount,DLs,NormalMembers,Allies,Enemies,GWWins,HoldingPole,Bulletin) VALUES (" + GuildID + ",'" + GuildName + "',1000000,'" + Creator.Name + ":" + Creator.UID.ToString() + ":" + Creator.Level.ToString() + ":1000000',1,'','','','',0,0,'New guild')", SaveConnection);
                Command.ExecuteNonQuery();
                return true;
            }
            catch { return false; }
        }

        public enum SkillType : ushort
        {
            RangeSkillMelee = 0,
            RangeSkillRanged = 1,
            SingleTargetSkillMagic = 2,
            TargetRangeSkillMagic = 3,
            DirectSkillMelee = 4,
            RangeSectorSkillRanged = 5,
            SingleTargetSkillMagicHeal = 6,
            SelfUseSkill = 7,
            BuffSkill = 8,
            RangeSkillHeal = 11,
            SingleTargetSkillMelee = 12,
            SingleTargetSkillRanged = 13,
            CruelShade = 16
        }

        public static void DefineSkills()
        {
            //SkillType Range Sector Damage/Heal CostMana/Stamina ActivationChance
            //Dodge
            SkillAttributes[3080] = new ushort[4][];
            SkillAttributes[3080][0] = new ushort[6] { 8, 0, 0, 0, 200, 0 };
            SkillAttributes[3080][1] = new ushort[6] { 8, 0, 0, 0, 300, 0 };
            SkillAttributes[3080][2] = new ushort[6] { 8, 0, 0, 0, 400, 0 };
            SkillsDone.Add(3080, 2);

            //FreezingArrow
            SkillAttributes[5000] = new ushort[1][];
            SkillAttributes[5000][0] = new ushort[6] { 8, 0, 0, 0, 0, 1 };
            SkillsDone.Add(5000, 0);

            /*//Poison
            SkillAttributes[5002] = new ushort[1][];
            SkillAttributes[5002][0] = new ushort[6] { 13, 0, 0, 175, 0, 5 };
            SkillsDone.Add(5002, 0);*/

            //RandomTeleport
            SkillAttributes[1080] = new ushort[1][];
            SkillAttributes[1080][0] = new ushort[6] { 8, 0, 0, 0, 0, 0 };
            SkillsDone.Add(1080, 0);

            //LuckyTime
            SkillAttributes[9876] = new ushort[1][];
            SkillAttributes[9876][0] = new ushort[6] { 7, 0, 0, 0, 0, 0 };
            SkillsDone.Add(9876, 1);

            //FastBlade
            SkillAttributes[1045] = new ushort[5][];
            SkillAttributes[1045][0] = new ushort[6] { 4, 4, 0, 0, 20, 0 };
            SkillAttributes[1045][1] = new ushort[6] { 4, 5, 0, 0, 20, 0 };
            SkillAttributes[1045][2] = new ushort[6] { 4, 6, 0, 0, 20, 0 };
            SkillAttributes[1045][3] = new ushort[6] { 4, 7, 0, 0, 20, 0 };
            SkillAttributes[1045][4] = new ushort[6] { 4, 8, 0, 0, 20, 0 };
            SkillsDone.Add(1045, 4);

            //ScentSword
            SkillAttributes[1046] = new ushort[5][];
            SkillAttributes[1046][0] = new ushort[6] { 4, 4, 0, 0, 20, 0 };
            SkillAttributes[1046][1] = new ushort[6] { 4, 5, 0, 0, 20, 0 };
            SkillAttributes[1046][2] = new ushort[6] { 4, 6, 0, 0, 20, 0 };
            SkillAttributes[1046][3] = new ushort[6] { 4, 7, 0, 0, 20, 0 };
            SkillAttributes[1046][4] = new ushort[6] { 4, 8, 0, 0, 20, 0 };
            SkillsDone.Add(1046, 4);

            //Hercules
            SkillAttributes[1115] = new ushort[5][];
            SkillAttributes[1115][0] = new ushort[6] { 0, 2, 0, 45, 30, 0 };
            SkillAttributes[1115][1] = new ushort[6] { 0, 2, 0, 50, 30, 0 };
            SkillAttributes[1115][2] = new ushort[6] { 0, 3, 0, 55, 30, 0 };
            SkillAttributes[1115][3] = new ushort[6] { 0, 4, 0, 60, 30, 0 };
            SkillAttributes[1115][4] = new ushort[6] { 0, 4, 0, 65, 30, 0 };
            SkillsDone.Add(1115, 4);

            //FireCircle
            SkillAttributes[1120] = new ushort[4][];
            SkillAttributes[1120][0] = new ushort[6] { 3, 7, 0, 200, 150, 0 };
            SkillAttributes[1120][1] = new ushort[6] { 3, 9, 0, 650, 170, 0 };
            SkillAttributes[1120][2] = new ushort[6] { 3, 11, 0, 720, 190, 0 };
            SkillAttributes[1120][3] = new ushort[6] { 3, 13, 0, 770, 210, 0 };
            SkillsDone.Add(1120, 3);

            //Rage
            SkillAttributes[7020] = new ushort[10][];
            SkillAttributes[7020][0] = new ushort[6] { 0, 2, 0, 110, 0, 20 };
            SkillAttributes[7020][1] = new ushort[6] { 0, 2, 0, 110, 0, 23 };
            SkillAttributes[7020][2] = new ushort[6] { 0, 2, 0, 110, 0, 26 };
            SkillAttributes[7020][3] = new ushort[6] { 0, 2, 0, 110, 0, 29 };
            SkillAttributes[7020][4] = new ushort[6] { 0, 2, 0, 140, 0, 31 };
            SkillAttributes[7020][5] = new ushort[6] { 0, 2, 0, 140, 0, 34 };
            SkillAttributes[7020][6] = new ushort[6] { 0, 2, 0, 140, 0, 37 };
            SkillAttributes[7020][7] = new ushort[6] { 0, 2, 0, 140, 0, 40 };
            SkillAttributes[7020][8] = new ushort[6] { 0, 2, 0, 140, 0, 43 };
            SkillAttributes[7020][9] = new ushort[6] { 0, 2, 0, 145, 0, 45 };
            SkillsDone.Add(7020, 9);

            //Snow
            SkillAttributes[5010] = new ushort[10][];
            SkillAttributes[5010][0] = new ushort[6] { 0, 2, 0, 110, 0, 20 };
            SkillAttributes[5010][1] = new ushort[6] { 0, 2, 0, 110, 0, 23 };
            SkillAttributes[5010][2] = new ushort[6] { 0, 2, 0, 110, 0, 26 };
            SkillAttributes[5010][3] = new ushort[6] { 0, 2, 0, 110, 0, 29 };
            SkillAttributes[5010][4] = new ushort[6] { 0, 3, 0, 140, 0, 31 };
            SkillAttributes[5010][5] = new ushort[6] { 0, 3, 0, 140, 0, 34 };
            SkillAttributes[5010][6] = new ushort[6] { 0, 3, 0, 140, 0, 37 };
            SkillAttributes[5010][7] = new ushort[6] { 0, 3, 0, 140, 0, 40 };
            SkillAttributes[5010][8] = new ushort[6] { 0, 3, 0, 140, 0, 43 };
            SkillAttributes[5010][9] = new ushort[6] { 0, 3, 0, 145, 0, 45 };
            SkillsDone.Add(5010, 9);

            //Tornado
            SkillAttributes[1002] = new ushort[4][];
            SkillAttributes[1002][0] = new ushort[6] { 2, 0, 0, 505, 32, 0 };
            SkillAttributes[1002][1] = new ushort[6] { 2, 0, 0, 666, 36, 0 };
            SkillAttributes[1002][2] = new ushort[6] { 2, 0, 0, 882, 50, 0 };
            SkillAttributes[1002][3] = new ushort[6] { 2, 0, 0, 1166, 64, 0 };
            SkillsDone.Add(1002, 3);

            //Cure
            SkillAttributes[1005] = new ushort[5][];
            SkillAttributes[1005][0] = new ushort[6] { 6, 0, 0, 20, 10, 0 };
            SkillAttributes[1005][1] = new ushort[6] { 6, 0, 0, 70, 30, 0 };
            SkillAttributes[1005][2] = new ushort[6] { 6, 0, 0, 150, 60, 0 };
            SkillAttributes[1005][3] = new ushort[6] { 6, 0, 0, 280, 100, 0 };
            SkillAttributes[1005][4] = new ushort[6] { 6, 0, 0, 400, 130, 0 };
            SkillsDone.Add(1005, 4);

            //Cure Pluie
            SkillAttributes[1055] = new ushort[5][];
            SkillAttributes[1055][0] = new ushort[6] { 6, 0, 0, 100, 150, 0 };
            SkillAttributes[1055][1] = new ushort[6] { 6, 0, 0, 200, 270, 0 };
            SkillAttributes[1055][2] = new ushort[6] { 6, 0, 0, 300, 375, 0 };
            SkillAttributes[1055][3] = new ushort[6] { 6, 0, 0, 400, 440, 0 };
            SkillAttributes[1055][4] = new ushort[6] { 6, 0, 0, 500, 500, 0 };
            SkillsDone.Add(1055, 4);

            //SpiritualHealing
            SkillAttributes[1190] = new ushort[3][];
            SkillAttributes[1190][0] = new ushort[6] { 7, 0, 0, 500, 100, 0 };
            SkillAttributes[1190][1] = new ushort[6] { 7, 0, 0, 800, 100, 0 };
            SkillAttributes[1190][2] = new ushort[6] { 7, 0, 0, 1300, 100, 0 };
            SkillsDone.Add(1190, 2);

            //Fire of Hell
            SkillAttributes[1165] = new ushort[4][];
            SkillAttributes[1165][0] = new ushort[6] { 3, 3, 0, 180, 120, 0 };
            SkillAttributes[1165][1] = new ushort[6] { 3, 3, 0, 240, 150, 0 };
            SkillAttributes[1165][2] = new ushort[6] { 3, 3, 0, 310, 180, 0 };
            SkillAttributes[1165][3] = new ushort[6] { 3, 3, 0, 400, 210, 0 };
            SkillsDone.Add(1165, 3);

            //Scatter
            SkillAttributes[8001] = new ushort[6][];
            SkillAttributes[8001][0] = new ushort[6] { 5, 9, 45, 50, 0, 0 };
            SkillAttributes[8001][1] = new ushort[6] { 5, 10, 60, 55, 0, 0 };
            SkillAttributes[8001][2] = new ushort[6] { 5, 11, 80, 60, 0, 0 };
            SkillAttributes[8001][3] = new ushort[6] { 5, 12, 100, 65, 0, 0 };
            SkillAttributes[8001][4] = new ushort[6] { 5, 13, 150, 70, 0, 0 };
            SkillAttributes[8001][5] = new ushort[6] { 5, 14, 180, 72, 0, 0 };
            SkillsDone.Add(8001, 5);

            //Superman
            SkillAttributes[1025] = new ushort[1][];
            SkillAttributes[1025][0] = new ushort[6] { 7, 0, 0, 0, 0, 0 };
            SkillsDone.Add(1025, 0); ;

            //Shield
            SkillAttributes[1020] = new ushort[1][];
            SkillAttributes[1020][0] = new ushort[6] { 7, 0, 0, 0, 0, 0 };
            SkillsDone.Add(1020, 0); ;

            //Fortress
            SkillAttributes[1021] = new ushort[1][];
            SkillAttributes[1021][0] = new ushort[6] { 3, 10, 0, 4000, 300, 0 };
            SkillsDone.Add(1021, 0);

            //Guard
            SkillAttributes[4000] = new ushort[4][];
            SkillAttributes[4000][0] = new ushort[6] { 7, 0, 0, 0, 100, 0 };
            SkillAttributes[4000][1] = new ushort[6] { 7, 0, 0, 0, 100, 0 };
            SkillAttributes[4000][2] = new ushort[6] { 7, 0, 0, 0, 100, 0 };
            SkillAttributes[4000][3] = new ushort[6] { 7, 0, 0, 0, 100, 0 };
            SkillsDone.Add(4000, 3);

            //Cyclone
            SkillAttributes[1110] = new ushort[1][];
            SkillAttributes[1110][0] = new ushort[6] { 7, 0, 0, 0, 0, 0 };
            SkillsDone.Add(1110, 0);

            //Arrow Rain
            SkillAttributes[8030] = new ushort[1][];
            SkillAttributes[8030][0] = new ushort[6] { 5, 14, 180, 100, 0, 1 };
            SkillsDone.Add(8030, 0);

            //Accuracy
            SkillAttributes[1015] = new ushort[1][];
            SkillAttributes[1015][0] = new ushort[6] { 7, 0, 0, 0, 0, 1 };
            SkillsDone.Add(1015, 0);

            //XP Fly
            SkillAttributes[8002] = new ushort[1][];
            SkillAttributes[8002][0] = new ushort[6] { 7, 0, 0, 0, 0, 0 };
            SkillsDone.Add(8002, 0);

            //Advanced Fly
            SkillAttributes[8003] = new ushort[2][];
            SkillAttributes[8003][0] = new ushort[6] { 7, 0, 0, 0, 100, 0 };
            SkillAttributes[8003][1] = new ushort[6] { 7, 0, 0, 0, 100, 0 };
            SkillsDone.Add(8003, 0);

            //Stigma
            SkillAttributes[1095] = new ushort[5][];
            SkillAttributes[1095][0] = new ushort[6] { 8, 0, 0, 0, 200, 0 };
            SkillAttributes[1095][1] = new ushort[6] { 8, 0, 0, 0, 250, 0 };
            SkillAttributes[1095][2] = new ushort[6] { 8, 0, 0, 0, 300, 0 };
            SkillAttributes[1095][3] = new ushort[6] { 8, 0, 0, 0, 350, 0 };
            SkillAttributes[1095][4] = new ushort[6] { 8, 0, 0, 0, 400, 0 };
            SkillsDone.Add(1095, 4);

            //Nectar
            SkillAttributes[1170] = new ushort[5][];
            SkillAttributes[1170][0] = new ushort[6] { 6, 0, 0, 600, 600, 0 };
            SkillAttributes[1170][1] = new ushort[6] { 6, 0, 0, 700, 660, 0 };
            SkillAttributes[1170][2] = new ushort[6] { 6, 0, 0, 800, 720, 0 };
            SkillAttributes[1170][3] = new ushort[6] { 6, 0, 0, 900, 770, 0 };
            SkillAttributes[1170][4] = new ushort[6] { 6, 0, 0, 1000, 820, 0 };
            SkillsDone.Add(1170, 4);

            //Thunder
            SkillAttributes[1000] = new ushort[5][];
            SkillAttributes[1000][0] = new ushort[6] { 2, 0, 0, 7, 1, 0 };
            SkillAttributes[1000][1] = new ushort[6] { 2, 0, 0, 16, 6, 0 };
            SkillAttributes[1000][2] = new ushort[6] { 2, 0, 0, 32, 10, 0 };
            SkillAttributes[1000][3] = new ushort[6] { 2, 0, 0, 57, 11, 0 };
            SkillAttributes[1000][4] = new ushort[6] { 2, 0, 0, 86, 17, 0 };
            SkillsDone.Add(1000, 4);

            //Pervade
            SkillAttributes[3090] = new ushort[6][];
            SkillAttributes[3090][0] = new ushort[6] { 3, 7, 0, 200, 100, 0 };
            SkillAttributes[3090][1] = new ushort[6] { 3, 8, 0, 300, 100, 0 };
            SkillAttributes[3090][2] = new ushort[6] { 3, 9, 0, 400, 100, 0 };
            SkillAttributes[3090][3] = new ushort[6] { 3, 10, 0, 500, 100, 0 };
            SkillAttributes[3090][4] = new ushort[6] { 3, 11, 0, 600, 100, 0 };
            SkillAttributes[3090][5] = new ushort[6] { 3, 13, 0, 700, 100, 0 };
            SkillsDone.Add(3090, 5);

            //Fire
            SkillAttributes[1001] = new ushort[4][];
            SkillAttributes[1001][0] = new ushort[6] { 2, 0, 0, 130, 21, 0 };
            SkillAttributes[1001][1] = new ushort[6] { 2, 0, 0, 189, 21, 0 };
            SkillAttributes[1001][2] = new ushort[6] { 2, 0, 0, 275, 28, 0 };
            SkillAttributes[1001][3] = new ushort[6] { 2, 0, 0, 380, 32, 0 };
            SkillsDone.Add(1001, 3);

            //FireBall
            SkillAttributes[1150] = new ushort[8][];
            SkillAttributes[1150][0] = new ushort[6] { 2, 0, 0, 378, 33, 0 };
            SkillAttributes[1150][1] = new ushort[6] { 2, 0, 0, 550, 33, 0 };
            SkillAttributes[1150][2] = new ushort[6] { 2, 0, 0, 760, 46, 0 };
            SkillAttributes[1150][3] = new ushort[6] { 2, 0, 0, 1010, 53, 0 };
            SkillAttributes[1150][4] = new ushort[6] { 2, 0, 0, 1332, 53, 0 };
            SkillAttributes[1150][5] = new ushort[6] { 2, 0, 0, 1764, 60, 0 };
            SkillAttributes[1150][6] = new ushort[6] { 2, 0, 0, 2332, 82, 0 };
            SkillAttributes[1150][7] = new ushort[6] { 2, 0, 0, 2800, 105, 0 };
            SkillsDone.Add(1150, 7);

            //FlyingMoon
            SkillAttributes[1320] = new ushort[3][];
            SkillAttributes[1320][0] = new ushort[6] { 2, 0, 0, 480, 0, 0 };
            SkillAttributes[1320][1] = new ushort[6] { 2, 0, 0, 1950, 0, 0 };
            SkillAttributes[1320][2] = new ushort[6] { 2, 0, 0, 5890, 0, 0 };
            SkillsDone.Add(1320, 2);

            //Fire Meteor
            SkillAttributes[1180] = new ushort[8][];
            SkillAttributes[1180][0] = new ushort[6] { 2, 0, 0, 760, 62, 0 };
            SkillAttributes[1180][1] = new ushort[6] { 2, 0, 0, 1040, 74, 0 };
            SkillAttributes[1180][2] = new ushort[6] { 2, 0, 0, 1250, 115, 0 };
            SkillAttributes[1180][3] = new ushort[6] { 2, 0, 0, 1480, 130, 0 };
            SkillAttributes[1180][4] = new ushort[6] { 2, 0, 0, 1810, 150, 0 };
            SkillAttributes[1180][5] = new ushort[6] { 2, 0, 0, 2210, 215, 0 };
            SkillAttributes[1180][6] = new ushort[6] { 2, 0, 0, 2700, 285, 0 };
            SkillAttributes[1180][7] = new ushort[6] { 2, 0, 0, 3250, 300, 0 };
            SkillsDone.Add(1180, 7);

            //Bomb
            SkillAttributes[1160] = new ushort[4][];
            SkillAttributes[1160][0] = new ushort[6] { 2, 0, 0, 855, 53, 0 };
            SkillAttributes[1160][1] = new ushort[6] { 2, 0, 0, 1498, 60, 0 };
            SkillAttributes[1160][2] = new ushort[6] { 2, 0, 0, 1985, 82, 0 };
            SkillAttributes[1160][3] = new ushort[6] { 2, 0, 0, 2623, 105, 0 };
            SkillsDone.Add(1160, 3);

            //Lightning
            SkillAttributes[1010] = new ushort[1][];
            SkillAttributes[1010][0] = new ushort[6] { 3, 5, 0, 50, 0, 1 };
            SkillsDone.Add(1010, 0);

            //Volcano
            SkillAttributes[1125] = new ushort[1][];
            SkillAttributes[1125][0] = new ushort[6] { 3, 8, 0, 300, 0, 1 };
            SkillsDone.Add(1125, 0);

            //SpeedLightning
            SkillAttributes[5001] = new ushort[1][];
            SkillAttributes[5001][0] = new ushort[6] { 3, 14, 0, 450, 0, 1 };
            SkillsDone.Add(5001, 0);

            //Pray
            SkillAttributes[1100] = new ushort[1][];
            SkillAttributes[1100][0] = new ushort[6] { 8, 0, 0, 0, 1000, 0 };
            SkillsDone.Add(1100, 0);

            //Rez
            SkillAttributes[1050] = new ushort[1][];
            SkillAttributes[1050][0] = new ushort[6] { 8, 0, 0, 0, 0, 1 };
            SkillsDone.Add(1050, 0);

            //Restore
            SkillAttributes[1105] = new ushort[1][];
            SkillAttributes[1105][0] = new ushort[6] { 8, 0, 0, 0, 0, 1 };
            SkillsDone.Add(1105, 0);

            //RapidFire
            SkillAttributes[8000] = new ushort[6][];
            SkillAttributes[8000][0] = new ushort[6] { 13, 8, 0, 150, 100, 0 };
            SkillAttributes[8000][1] = new ushort[6] { 13, 8, 0, 180, 100, 0 };
            SkillAttributes[8000][2] = new ushort[6] { 13, 8, 0, 210, 100, 0 };
            SkillAttributes[8000][3] = new ushort[6] { 13, 8, 0, 240, 100, 0 };
            SkillAttributes[8000][4] = new ushort[6] { 13, 8, 0, 270, 100, 0 };
            SkillAttributes[8000][5] = new ushort[6] { 13, 8, 0, 300, 100, 0 };
            SkillsDone.Add(8000, 5);

            //Advanced Cure
            SkillAttributes[1175] = new ushort[5][];
            SkillAttributes[1175][0] = new ushort[6] { 6, 0, 0, 500, 160, 0 };
            SkillAttributes[1175][1] = new ushort[6] { 6, 0, 0, 600, 190, 0 };
            SkillAttributes[1175][2] = new ushort[6] { 6, 0, 0, 700, 215, 0 };
            SkillAttributes[1175][3] = new ushort[6] { 6, 0, 0, 800, 235, 0 };
            SkillAttributes[1175][4] = new ushort[6] { 6, 0, 0, 900, 255, 0 };
            SkillsDone.Add(1175, 4);

            //Phoenix
            SkillAttributes[5030] = new ushort[10][];
            SkillAttributes[5030][0] = new ushort[6] { 12, 0, 0, 115, 0, 33 };
            SkillAttributes[5030][1] = new ushort[6] { 12, 0, 0, 116, 0, 38 };
            SkillAttributes[5030][2] = new ushort[6] { 12, 0, 0, 117, 0, 43 };
            SkillAttributes[5030][3] = new ushort[6] { 12, 0, 0, 118, 0, 48 };
            SkillAttributes[5030][4] = new ushort[6] { 12, 0, 0, 119, 0, 53 };
            SkillAttributes[5030][5] = new ushort[6] { 12, 0, 0, 120, 0, 58 };
            SkillAttributes[5030][6] = new ushort[6] { 12, 0, 0, 121, 0, 63 };
            SkillAttributes[5030][7] = new ushort[6] { 12, 0, 0, 122, 0, 68 };
            SkillAttributes[5030][8] = new ushort[6] { 12, 0, 0, 123, 0, 73 };
            SkillAttributes[5030][9] = new ushort[6] { 12, 0, 0, 124, 0, 78 };
            SkillsDone.Add(5030, 9);

            //Assommer
            SkillAttributes[1220] = new ushort[1][];
            SkillAttributes[1220][0] = new ushort[6] { 12, 0, 0, 124, 0, 75 };
            SkillsDone.Add(1220, 0);

            //SpeedGun
            SkillAttributes[1260] = new ushort[10][];
            SkillAttributes[1260][0] = new ushort[6] { 12, 0, 0, 100, 0, 20 };
            SkillAttributes[1260][1] = new ushort[6] { 12, 0, 0, 115, 0, 23 };
            SkillAttributes[1260][2] = new ushort[6] { 12, 0, 0, 125, 0, 26 };
            SkillAttributes[1260][3] = new ushort[6] { 12, 0, 0, 140, 0, 29 };
            SkillAttributes[1260][4] = new ushort[6] { 12, 0, 0, 150, 0, 32 };
            SkillAttributes[1260][5] = new ushort[6] { 12, 0, 0, 165, 0, 35 };
            SkillAttributes[1260][6] = new ushort[6] { 12, 0, 0, 175, 0, 38 };
            SkillAttributes[1260][7] = new ushort[6] { 12, 0, 0, 190, 0, 41 };
            SkillAttributes[1260][8] = new ushort[6] { 12, 0, 0, 200, 0, 44 };
            SkillAttributes[1260][9] = new ushort[6] { 12, 0, 0, 210, 0, 47 };
            SkillsDone.Add(1260, 9);

            //Boreas
            SkillAttributes[5050] = new ushort[10][];
            SkillAttributes[5050][0] = new ushort[6] { 0, 3, 0, 90, 0, 20 };
            SkillAttributes[5050][1] = new ushort[6] { 0, 3, 0, 100, 0, 22 };
            SkillAttributes[5050][2] = new ushort[6] { 0, 4, 0, 110, 0, 24 };
            SkillAttributes[5050][3] = new ushort[6] { 0, 4, 0, 120, 0, 26 };
            SkillAttributes[5050][4] = new ushort[6] { 0, 5, 0, 130, 0, 28 };
            SkillAttributes[5050][5] = new ushort[6] { 0, 5, 0, 140, 0, 30 };
            SkillAttributes[5050][6] = new ushort[6] { 0, 6, 0, 150, 0, 32 };
            SkillAttributes[5050][7] = new ushort[6] { 0, 6, 0, 160, 0, 34 };
            SkillAttributes[5050][8] = new ushort[6] { 0, 6, 0, 170, 0, 36 };
            SkillAttributes[5050][9] = new ushort[6] { 0, 6, 0, 180, 0, 38 };
            SkillsDone.Add(5050, 9);

            //Penetration
            SkillAttributes[1290] = new ushort[10][];
            SkillAttributes[1290][0] = new ushort[6] { 12, 0, 0, 150, 0, 10 };
            SkillAttributes[1290][1] = new ushort[6] { 12, 0, 0, 160, 0, 10 };
            SkillAttributes[1290][2] = new ushort[6] { 12, 0, 0, 170, 0, 11 };
            SkillAttributes[1290][3] = new ushort[6] { 12, 0, 0, 180, 0, 11 };
            SkillAttributes[1290][4] = new ushort[6] { 12, 0, 0, 190, 0, 12 };
            SkillAttributes[1290][5] = new ushort[6] { 12, 0, 0, 200, 0, 12 };
            SkillAttributes[1290][6] = new ushort[6] { 12, 0, 0, 210, 0, 13 };
            SkillAttributes[1290][7] = new ushort[6] { 12, 0, 0, 220, 0, 13 };
            SkillAttributes[1290][8] = new ushort[6] { 12, 0, 0, 230, 0, 14 };
            SkillAttributes[1290][9] = new ushort[6] { 12, 0, 0, 240, 0, 15 };
            SkillsDone.Add(1290, 9);

            //WideStrike
            SkillAttributes[1250] = new ushort[10][];
            SkillAttributes[1250][0] = new ushort[6] { 0, 3, 0, 90, 0, 20 };
            SkillAttributes[1250][1] = new ushort[6] { 0, 3, 0, 100, 0, 22 };
            SkillAttributes[1250][2] = new ushort[6] { 0, 4, 0, 110, 0, 24 };
            SkillAttributes[1250][3] = new ushort[6] { 0, 4, 0, 120, 0, 26 };
            SkillAttributes[1250][4] = new ushort[6] { 0, 5, 0, 130, 0, 28 };
            SkillAttributes[1250][5] = new ushort[6] { 0, 5, 0, 140, 0, 30 };
            SkillAttributes[1250][6] = new ushort[6] { 0, 6, 0, 150, 0, 32 };
            SkillAttributes[1250][7] = new ushort[6] { 0, 6, 0, 160, 0, 34 };
            SkillAttributes[1250][8] = new ushort[6] { 0, 6, 0, 170, 0, 36 };
            SkillAttributes[1250][9] = new ushort[6] { 0, 6, 0, 180, 0, 38 };
            SkillsDone.Add(1250, 9);

            //Roamer
            SkillAttributes[7040] = new ushort[10][];
            SkillAttributes[7040][0] = new ushort[6] { 0, 4, 0, 90, 0, 20 };
            SkillAttributes[7040][1] = new ushort[6] { 0, 4, 0, 92, 0, 23 };
            SkillAttributes[7040][2] = new ushort[6] { 0, 4, 0, 94, 0, 26 };
            SkillAttributes[7040][3] = new ushort[6] { 0, 4, 0, 96, 0, 29 };
            SkillAttributes[7040][4] = new ushort[6] { 0, 4, 0, 98, 0, 31 };
            SkillAttributes[7040][5] = new ushort[6] { 0, 4, 0, 100, 0, 34 };
            SkillAttributes[7040][6] = new ushort[6] { 0, 5, 0, 102, 0, 37 };
            SkillAttributes[7040][7] = new ushort[6] { 0, 5, 0, 104, 0, 40 };
            SkillAttributes[7040][8] = new ushort[6] { 0, 5, 0, 106, 0, 43 };
            SkillAttributes[7040][9] = new ushort[6] { 0, 5, 0, 108, 0, 45 };
            SkillsDone.Add(7040, 9);

            //Celestial
            SkillAttributes[7030] = new ushort[10][];
            SkillAttributes[7030][0] = new ushort[6] { 12, 0, 0, 110, 0, 10 };
            SkillAttributes[7030][1] = new ushort[6] { 12, 0, 0, 112, 0, 11 };
            SkillAttributes[7030][2] = new ushort[6] { 12, 0, 0, 114, 0, 12 };
            SkillAttributes[7030][3] = new ushort[6] { 12, 0, 0, 116, 0, 13 };
            SkillAttributes[7030][4] = new ushort[6] { 12, 0, 0, 118, 0, 14 };
            SkillAttributes[7030][5] = new ushort[6] { 12, 0, 0, 120, 0, 15 };
            SkillAttributes[7030][6] = new ushort[6] { 12, 0, 0, 122, 0, 16 };
            SkillAttributes[7030][7] = new ushort[6] { 12, 0, 0, 124, 0, 17 };
            SkillAttributes[7030][8] = new ushort[6] { 12, 0, 0, 126, 0, 18 };
            SkillAttributes[7030][9] = new ushort[6] { 12, 0, 0, 128, 0, 19 };
            SkillsDone.Add(7030, 9);

            //Earthquake
            SkillAttributes[7010] = new ushort[10][];
            SkillAttributes[7010][0] = new ushort[6] { 12, 0, 0, 101, 0, 10 };
            SkillAttributes[7010][1] = new ushort[6] { 12, 0, 0, 102, 0, 12 };
            SkillAttributes[7010][2] = new ushort[6] { 12, 0, 0, 103, 0, 14 };
            SkillAttributes[7010][3] = new ushort[6] { 12, 0, 0, 104, 0, 16 };
            SkillAttributes[7010][4] = new ushort[6] { 12, 0, 0, 105, 0, 18 };
            SkillAttributes[7010][5] = new ushort[6] { 12, 0, 0, 106, 0, 20 };
            SkillAttributes[7010][6] = new ushort[6] { 12, 0, 0, 107, 0, 22 };
            SkillAttributes[7010][7] = new ushort[6] { 12, 0, 0, 108, 0, 24 };
            SkillAttributes[7010][8] = new ushort[6] { 12, 0, 0, 109, 0, 26 };
            SkillAttributes[7010][9] = new ushort[6] { 12, 0, 0, 110, 0, 28 };
            SkillsDone.Add(7010, 9);

            //Seizer
            SkillAttributes[7000] = new ushort[10][];
            SkillAttributes[7000][0] = new ushort[6] { 12, 0, 0, 105, 0, 15 };
            SkillAttributes[7000][1] = new ushort[6] { 12, 0, 0, 106, 0, 19 };
            SkillAttributes[7000][2] = new ushort[6] { 12, 0, 0, 107, 0, 22 };
            SkillAttributes[7000][3] = new ushort[6] { 12, 0, 0, 108, 0, 24 };
            SkillAttributes[7000][4] = new ushort[6] { 12, 0, 0, 109, 0, 25 };
            SkillAttributes[7000][5] = new ushort[6] { 12, 0, 0, 110, 0, 26 };
            SkillAttributes[7000][6] = new ushort[6] { 12, 0, 0, 111, 0, 27 };
            SkillAttributes[7000][7] = new ushort[6] { 12, 0, 0, 112, 0, 28 };
            SkillAttributes[7000][8] = new ushort[6] { 12, 0, 0, 113, 0, 29 };
            SkillAttributes[7000][9] = new ushort[6] { 12, 0, 0, 114, 0, 30 };
            SkillsDone.Add(7000, 9);

            //Halt
            SkillAttributes[1300] = new ushort[10][];
            SkillAttributes[1300][0] = new ushort[6] { 0, 3, 0, 90, 0, 20 };
            SkillAttributes[1300][1] = new ushort[6] { 0, 3, 0, 100, 0, 22 };
            SkillAttributes[1300][2] = new ushort[6] { 0, 3, 0, 110, 0, 24 };
            SkillAttributes[1300][3] = new ushort[6] { 0, 3, 0, 120, 0, 26 };
            SkillAttributes[1300][4] = new ushort[6] { 0, 3, 0, 130, 0, 28 };
            SkillAttributes[1300][5] = new ushort[6] { 0, 3, 0, 140, 0, 30 };
            SkillAttributes[1300][6] = new ushort[6] { 0, 4, 0, 150, 0, 32 };
            SkillAttributes[1300][7] = new ushort[6] { 0, 4, 0, 160, 0, 34 };
            SkillAttributes[1300][8] = new ushort[6] { 0, 4, 0, 170, 0, 36 };
            SkillAttributes[1300][9] = new ushort[6] { 0, 4, 0, 180, 0, 38 };
            SkillsDone.Add(1300, 9);

            //Boom
            SkillAttributes[5040] = new ushort[10][];
            SkillAttributes[5040][0] = new ushort[6] { 12, 0, 0, 101, 0, 10 };
            SkillAttributes[5040][1] = new ushort[6] { 12, 0, 0, 102, 0, 12 };
            SkillAttributes[5040][2] = new ushort[6] { 12, 0, 0, 103, 0, 14 };
            SkillAttributes[5040][3] = new ushort[6] { 12, 0, 0, 104, 0, 16 };
            SkillAttributes[5040][4] = new ushort[6] { 12, 0, 0, 105, 0, 18 };
            SkillAttributes[5040][5] = new ushort[6] { 12, 0, 0, 106, 0, 20 };
            SkillAttributes[5040][6] = new ushort[6] { 12, 0, 0, 107, 0, 22 };
            SkillAttributes[5040][7] = new ushort[6] { 12, 0, 0, 108, 0, 24 };
            SkillAttributes[5040][8] = new ushort[6] { 12, 0, 0, 109, 0, 26 };
            SkillAttributes[5040][9] = new ushort[6] { 12, 0, 0, 110, 0, 28 };
            SkillsDone.Add(5040, 9);

            //StrandedMonster
            SkillAttributes[5020] = new ushort[10][];
            SkillAttributes[5020][0] = new ushort[6] { 0, 3, 0, 130, 0, 20 };
            SkillAttributes[5020][1] = new ushort[6] { 0, 3, 0, 131, 0, 23 };
            SkillAttributes[5020][2] = new ushort[6] { 0, 3, 0, 132, 0, 26 };
            SkillAttributes[5020][3] = new ushort[6] { 0, 4, 0, 133, 0, 29 };
            SkillAttributes[5020][4] = new ushort[6] { 0, 4, 0, 134, 0, 31 };
            SkillAttributes[5020][5] = new ushort[6] { 0, 4, 0, 135, 0, 34 };
            SkillAttributes[5020][6] = new ushort[6] { 0, 5, 0, 136, 0, 37 };
            SkillAttributes[5020][7] = new ushort[6] { 0, 5, 0, 137, 0, 40 };
            SkillAttributes[5020][8] = new ushort[6] { 0, 6, 0, 138, 0, 43 };
            SkillAttributes[5020][9] = new ushort[6] { 0, 6, 0, 139, 0, 45 };
            SkillsDone.Add(5020, 9);

            //CruelShade
            SkillAttributes[3050] = new ushort[4][];
            SkillAttributes[3050][0] = new ushort[6] { 16, 0, 0, 0, 100, 0 };
            SkillAttributes[3050][1] = new ushort[6] { 16, 0, 0, 0, 100, 0 };
            SkillAttributes[3050][2] = new ushort[6] { 16, 0, 0, 0, 100, 0 };
            SkillAttributes[3050][3] = new ushort[6] { 16, 0, 0, 0, 100, 0 };
            SkillsDone.Add(3050, 3);

            //Meditation
            SkillAttributes[1195] = new ushort[3][];
            SkillAttributes[1195][0] = new ushort[6] { 14, 0, 0, 310, 100, 0 };
            SkillAttributes[1195][1] = new ushort[6] { 14, 0, 0, 600, 100, 0 };
            SkillAttributes[1195][2] = new ushort[6] { 14, 0, 0, 1020, 100, 0 };
            SkillsDone.Add(1195, 2);

            //MagicShield
            SkillAttributes[1090] = new ushort[5][];
            SkillAttributes[1090][0] = new ushort[6] { 8, 0, 0, 0, 200, 0 };
            SkillAttributes[1090][1] = new ushort[6] { 8, 0, 0, 0, 250, 0 };
            SkillAttributes[1090][2] = new ushort[6] { 8, 0, 0, 0, 300, 0 };
            SkillAttributes[1090][3] = new ushort[6] { 8, 0, 0, 0, 350, 0 };
            SkillAttributes[1090][4] = new ushort[6] { 8, 0, 0, 0, 400, 0 };
            SkillsDone.Add(1090, 4);

            //StarofAccuracy
            SkillAttributes[1085] = new ushort[5][];
            SkillAttributes[1085][0] = new ushort[6] { 8, 0, 0, 0, 200, 0 };
            SkillAttributes[1085][1] = new ushort[6] { 8, 0, 0, 0, 250, 0 };
            SkillAttributes[1085][2] = new ushort[6] { 8, 0, 0, 0, 300, 0 };
            SkillAttributes[1085][3] = new ushort[6] { 8, 0, 0, 0, 350, 0 };
            SkillAttributes[1085][4] = new ushort[6] { 8, 0, 0, 0, 400, 0 };
            SkillsDone.Add(1085, 4);

            //Invisibility
            SkillAttributes[1075] = new ushort[5][];
            SkillAttributes[1075][0] = new ushort[6] { 8, 0, 0, 0, 200, 0 };
            SkillAttributes[1075][1] = new ushort[6] { 8, 0, 0, 0, 250, 0 };
            SkillAttributes[1075][2] = new ushort[6] { 8, 0, 0, 0, 300, 0 };
            SkillAttributes[1075][3] = new ushort[6] { 8, 0, 0, 0, 330, 0 };
            SkillAttributes[1075][4] = new ushort[6] { 8, 0, 0, 0, 360, 0 };
            SkillsDone.Add(1075, 4);

            //Robot
            SkillAttributes[1270] = new ushort[8][];
            SkillAttributes[1270][0] = new ushort[6] { 7, 0, 0, 0, 0, 0 };
            SkillAttributes[1270][1] = new ushort[6] { 7, 0, 0, 0, 0, 0 };
            SkillAttributes[1270][2] = new ushort[6] { 7, 0, 0, 0, 0, 0 };
            SkillAttributes[1270][3] = new ushort[6] { 7, 0, 0, 0, 0, 0 };
            SkillAttributes[1270][4] = new ushort[6] { 7, 0, 0, 0, 0, 0 };
            SkillAttributes[1270][5] = new ushort[6] { 7, 0, 0, 0, 0, 0 };
            SkillAttributes[1270][6] = new ushort[6] { 7, 0, 0, 0, 0, 0 };
            SkillAttributes[1270][7] = new ushort[6] { 7, 0, 0, 0, 0, 0 };
            SkillsDone.Add(1270, 7);

            //Piglet
            SkillAttributes[3321] = new ushort[1][];
            SkillAttributes[3321][0] = new ushort[6] { 7, 0, 0, 0, 0, 0 };
            SkillsDone.Add(3321, 0);
        }

        public static void RemoveFromFriend(uint RemoverUID, uint RemovedUID)
        {
            string[] Accounts = Directory.GetFiles("Accounts", "*.acc");
            for (int i = 0; i < Accounts.Length; i++)
            {
                Ini Cli = new Ini(System.Windows.Forms.Application.StartupPath + Accounts[i]);
                if (uint.Parse(Cli.ReadValue("Character", "UID")) == RemovedUID)
                {
                    string Friends = (string)Cli.ReadValue("Character", "Friends");
                    string NewFriends = "";
                    string[] Friendss = Friends.Split('.');
                    foreach (string friend in Friendss)
                    {
                        if (friend != null && friend.Length > 1)
                        {
                            string[] Splitter = friend.Split(':');
                            if (Splitter[1] != RemoverUID.ToString())
                                NewFriends += friend + ".";
                        }
                        if (NewFriends.Length > 0)
                            NewFriends = NewFriends.Remove(NewFriends.Length - 1, 1);
                    }
                    Cli.WriteString("Character", "Friends", NewFriends);
                    break;
                }
            }
        }

        public static void LoadRevPoints()
        {
            RevPoints = new ushort[43][];
            RevPoints[0] = new ushort[4] { 1002, 1002, 430, 380 };
            RevPoints[1] = new ushort[4] { 1005, 1005, 50, 50 };
            RevPoints[2] = new ushort[4] { 1006, 1002, 430, 380 };
            RevPoints[3] = new ushort[4] { 1008, 1002, 430, 380 };
            RevPoints[4] = new ushort[4] { 1009, 1002, 430, 380 };
            RevPoints[5] = new ushort[4] { 1010, 1002, 430, 380 };
            RevPoints[6] = new ushort[4] { 1007, 1002, 430, 380 };
            RevPoints[7] = new ushort[4] { 1004, 1002, 430, 380 };
            RevPoints[8] = new ushort[4] { 1028, 1002, 430, 380 };
            RevPoints[9] = new ushort[4] { 1037, 1002, 430, 380 };
            RevPoints[10] = new ushort[4] { 1038, 1002, 438, 398 };
            RevPoints[11] = new ushort[4] { 1015, 1015, 717, 577 };
            RevPoints[12] = new ushort[4] { 1001, 1000, 499, 650 };
            RevPoints[13] = new ushort[4] { 1000, 1000, 499, 650 };
            RevPoints[14] = new ushort[4] { 1013, 1011, 193, 266 };
            RevPoints[15] = new ushort[4] { 1011, 1011, 193, 266 };
            RevPoints[16] = new ushort[4] { 1076, 1011, 193, 266 };
            RevPoints[17] = new ushort[4] { 1014, 1011, 193, 266 };
            RevPoints[18] = new ushort[4] { 1020, 1020, 566, 563 };
            RevPoints[19] = new ushort[4] { 1075, 1020, 566, 656 };
            RevPoints[20] = new ushort[4] { 1012, 1020, 566, 656 };
            RevPoints[21] = new ushort[4] { 6000, 6000, 028, 071 };
            RevPoints[22] = new ushort[4] { 1351, 1002, 430, 380 };
            RevPoints[23] = new ushort[4] { 1352, 1002, 430, 380 };
            RevPoints[24] = new ushort[4] { 1353, 1002, 430, 380 };
            RevPoints[25] = new ushort[4] { 1354, 1002, 430, 380 };
            RevPoints[26] = new ushort[4] { 1044, 1002, 430, 380 };
            RevPoints[27] = new ushort[4] { 1045, 1002, 430, 380 };
            RevPoints[28] = new ushort[4] { 1046, 1002, 430, 380 };
            RevPoints[29] = new ushort[4] { 1047, 1002, 430, 380 };
            RevPoints[30] = new ushort[4] { 1048, 1002, 430, 380 };
            RevPoints[31] = new ushort[4] { 1049, 1002, 430, 380 };
            RevPoints[32] = new ushort[4] { 1042, 1002, 430, 380 };
            RevPoints[33] = new ushort[4] { 1043, 1002, 430, 380 };
            RevPoints[34] = new ushort[4] { 1036, 1036, 212, 196 };
            RevPoints[35] = new ushort[4] { 1091, 1002, 430, 380 };
            RevPoints[36] = new ushort[4] { 1081, 1002, 430, 380 };
            RevPoints[37] = new ushort[4] { 1090, 1002, 430, 380 };
            RevPoints[38] = new ushort[4] { 2021, 1002, 430, 380 };
            RevPoints[39] = new ushort[4] { 2022, 1002, 430, 380 };
            RevPoints[40] = new ushort[4] { 2023, 1002, 430, 380 };
            RevPoints[41] = new ushort[4] { 2024, 1002, 430, 380 };
            RevPoints[42] = new ushort[4] { 1767, 1767, 94, 64 };
        }


          

        public static uint NeededSkillExp(short SkillId, byte Level)
        {
            if (SkillId == 4000)
            {
                if (Level == 0)
                    return 100;
                if (Level == 1)
                    return 300;
                if (Level == 2)
                    return 500;
                else return 0;
            }

            if (SkillId == 1320)
            {
                if (Level == 0)
                    return 1500;
                if (Level == 1)
                    return 6000;
                else return 0;
            }
            else if (SkillId == 1165 || SkillId == 1160)
            {
                if (Level == 0)
                    return 1282500;
                if (Level == 1)
                    return 2696400;
                if (Level == 2)
                    return 3970000;
                else return 0;
            }
            else if (SkillId == 1220)
            {
                if (Level == 0)
                    return 0;
                else return 0;
            }
            else if (SkillId == 1195)
            {
                if (Level == 0)
                    return 537140;
                if (Level == 1)
                    return 918542;
                else return 0;
            }
            else if (SkillId == 1005)
            {
                if (Level == 0)
                    return 2000;
                if (Level == 1)
                    return 12000;
                if (Level == 2)
                    return 30000;
                if (Level == 3)
                    return 64000;
                else return 0;
            }
            else if (SkillId == 3050)
            {
                if (Level == 0)
                    return 500;
                if (Level == 1)
                    return 800;
                if (Level == 2)
                    return 1000;
                else return 0;
            }
            else if (SkillId == 3090)
            {
                if (Level == 0)
                    return 50000;
                if (Level == 1)
                    return 100000;
                if (Level == 2)
                    return 1500000;
                if (Level == 3)
                    return 2000000;
                if (Level == 4)
                    return 3000000;
                else return 0;
            }
            else if (SkillId == 1120)
            {
                if (Level == 0)
                    return 53104696;
                if (Level == 1)
                    return 98875022;
                if (Level == 2)
                    return 180034734;
                else return 0;
            }
            else if (SkillId == 1000)
            {
                if (Level == 0)
                    return 2000;
                if (Level == 1)
                    return 113060;
                if (Level == 2)
                    return 326107;
                if (Level == 3)
                    return 777950;
                else return 0;
            }
            else if (SkillId == 1001)
            {
                if (Level == 0)
                    return 4867575;
                if (Level == 1)
                    return 9735150;
                if (Level == 2)
                    return 19470300;
                else return 0;
            }
            else if (SkillId == 1190)
            {
                if (Level == 0)
                    return 561198;
                if (Level == 1)
                    return 973515;
                if (Level == 2)
                    return 1947030;
                else return 0;
            }
            else if (SkillId == 1002)
            {
                if (Level == 0)
                    return 118246825;
                if (Level == 1)
                    return 277035437;
                if (Level == 2)
                    return 920692259;
                else return 0;
            }
            else if (SkillId == 7020 || SkillId == 9000 || SkillId == 8001 || SkillId == 8000 || SkillId == 7040 || SkillId == 5030 || SkillId == 5050 || SkillId == 5050 || SkillId == 5010 || SkillId == 5020 || SkillId == 1290 || SkillId == 1300 || SkillId == 7030 || SkillId == 1260 || SkillId == 5020 || SkillId == 1290 || SkillId == 5040 || SkillId == 1300 || SkillId == 7000 || SkillId == 7010 || SkillId == 7040 || SkillId == 7030 || SkillId == 1250 || SkillId == 5050)
            {
                if (Level == 0)
                    return 20243;
                if (Level == 1)
                    return 37056;
                if (Level == 2)
                    return 66011;
                if (Level == 3)
                    return 116140;
                if (Level == 4)
                    return 192800;
                if (Level == 5)
                    return 418030;
                if (Level == 6)
                    return 454350;
                if (Level == 7)
                    return 491200;
                if (Level == 8)
                    return 520030;
                else
                    return 0;
            }
            else if (SkillId == 1045 || SkillId == 1046 || SkillId == 1115)
            {
                if (Level == 0)
                    return 100000;
                if (Level == 1)
                    return 300000;
                if (Level == 2)
                    return 741000;
                if (Level == 3)
                    return 1440000;
                else
                    return 0;
            }
            else if (SkillId == 1095 || SkillId == 1090 || SkillId == 1075 || SkillId == 1170 || SkillId == 1085)
            {
                if (Level == 0)
                    return 430;
                if (Level == 1)
                    return 520;
                if (Level == 2)
                    return 570;
                if (Level == 3)
                    return 620;
                else
                    return 0;
            }
            else
                return 9999999;
        }

        public static uint NeededProfXP(byte Level)
        {
            if (Level == 1)
                return 1200;
            else if (Level == 2)
                return 68000;
            else if (Level == 3)
                return 250000;
            else if (Level == 4)
                return 640000;
            else if (Level == 5)
                return 1600000;
            else if (Level == 6)
                return 4000000;
            else if (Level == 7)
                return 10000000;
            else if (Level == 8)
                return 22000000;
            else if (Level == 9)
                return 40000000;
            else if (Level == 10)
                return 90000000;
            else if (Level == 11)
                return 95000000;
            else if (Level == 12)
                return 142500000;
            else if (Level == 13)
                return 213750000;
            else if (Level == 14)
                return 320625000;
            else if (Level == 15)
                return 480937500;
            else if (Level == 16)
                return 721406250;
            else if (Level == 17)
                return 1082109375;
            else if (Level == 18)
                return 1623164063;
            else if (Level > 18)
                return 2100000000;
            else
                return 0;
        }



        public static ulong NeededXP(uint Level)
        {
            if (Level == 1)
                return 120;
            else if (Level == 2)
                return 180;
            else if (Level == 3)
                return 240;
            else if (Level == 4)
                return 360;
            else if (Level == 5)
                return 600;
            else if (Level == 6)
                return 960;
            else if (Level == 7)
                return 1200;
            else if (Level == 8)
                return 2400;
            else if (Level == 9)
                return 3600;
            else if (Level == 10)
                return 8400;
            else if (Level == 11)
                return 12000;
            else if (Level == 12)
                return 14400;
            else if (Level == 13)
                return 18000;
            else if (Level == 14)
                return 21600;
            else if (Level == 15)
                return 22646;
            else if (Level == 16)
                return 32203;
            else if (Level == 17)
                return 37433;
            else if (Level == 18)
                return 47556;
            else if (Level == 19)
                return 56609;
            else if (Level == 20)
                return 68772;
            else if (Level == 21)
                return 70515;
            else if (Level == 22)
                return 75936;
            else if (Level == 23)
                return 97733;
            else if (Level == 24)
                return 114836;
            else if (Level == 25)
                return 120853;
            else if (Level == 26)
                return 123981;
            else if (Level == 27)
                return 126720;
            else if (Level == 28)
                return 145878;
            else if (Level == 29)
                return 173436;
            else if (Level == 30)
                return 197646;
            else if (Level == 31)
                return 202451;
            else if (Level == 32)
                return 212160;
            else if (Level == 33)
                return 244190;
            else if (Level == 34)
                return 285824;
            else if (Level == 35)
                return 305986;
            else if (Level == 36)
                return 312864;
            else if (Level == 37)
                return 324480;
            else if (Level == 38)
                return 366168;
            else if (Level == 39)
                return 433959;
            else if (Level == 40)
                return 460590;
            else if (Level == 41)
                return 506738;
            else if (Level == 42)
                return 569994;
            else if (Level == 43)
                return 728527;
            else if (Level == 44)
                return 850829;
            else if (Level == 45)
                return 916479;
                //-----------------------------------------
            else if (Level == 46)
                return 935051;
            else if (Level == 47)
                return 940860;
            else if (Level == 48)
                return 1076590;
            else if (Level == 49)
                return 1272807;
            else if (Level == 50)
                return 1357986;
            else if (Level == 51)
                return 1384873;
            else if (Level == 52)
                return 1478420;
            else if (Level == 53)
                return 1632489;
            else if (Level == 54)
                return 1903121;
            else if (Level == 55)
                return 2065957;
            else if (Level == 56)
                return 2104909;
            else if (Level == 57)
                return 1921149;
            else if (Level == 58)
                return 2417153;
            else if (Level == 59)
                return 2853501;
            else if (Level == 60)
                return 3054580;
            else if (Level == 61)
                return 3111200;
            else if (Level == 62)
                return 3225607;
            else if (Level == 63)
                return 3811037;
            else if (Level == 64)
                return 4437965;
            else if (Level == 65)
                return 4880615;
            else if (Level == 66)
                return 4970959;
            else if (Level == 67)
                return 5107243;
            else if (Level == 68)
                return 5652526;
            else if (Level == 69)
                return 6579184;
            else if (Level == 70)
                return 6878005;
            else if (Level == 71)
                return 7100739;
            else if (Level == 72)
                return 7157642;
            else if (Level == 73)
                return 9106931;
            else if (Level == 74)
                return 10596415;
            else if (Level == 75)
                return 11220485;
            else if (Level == 76)
                return 11409179;
            else if (Level == 77)
                return 11424043;
            else if (Level == 78)
                return 12882966;
            else if (Level == 79)
                return 15172842;
            else if (Level == 80)
                return 15896985;
            else if (Level == 81)
                return 16163738;
            else if (Level == 82)
                return 16800069;
            else if (Level == 83)
                return 19230324;
            else if (Level == 84)
                return 22365189;
            else if (Level == 85)
                return 23819291;
            else if (Level == 86)
                return 24219524;
            else if (Level == 87)
                return 24864054;
            else if (Level == 88)
                return 27200095;
            else if (Level == 89)
                return 32033236;
            else if (Level == 90)
                return 33723786;
            else if (Level == 91)
                return 34291244;
            else if (Level == 92)
                return 34944017;
            else if (Level == 93)
                return 39463459;
            else if (Level == 94)
                return 45878550;
            else if (Level == 95)
                return 48924263;
            else if (Level == 96)
                return 49729242;
            else if (Level == 97)
                return 51072047;
            else if (Level == 98)
                return 55808382;
            else if (Level == 99)
                return 64870117;
            else if (Level == 100)
                return 68391872;
            else if (Level == 101)
                return 69537082;
            else if (Level == 102)
                return 76422949;
            else if (Level == 103)
                return 96950832;
            else if (Level == 104)
                return 112676761;
            else if (Level == 105)
                return 120090440;
            else if (Level == 106)
                return 121798300;
            else if (Level == 107)
                return 127680095;
            else if (Level == 108)
                return 137446904;
            else if (Level == 109)
                return 193716061;
            else if (Level == 110)
                return 408832135;
            else if (Level == 111)
                return 454674621;
            else if (Level == 112)
                return 461125840;
            else if (Level == 113)
                return 469189848;
            else if (Level == 114)
                return 477253857;
            else if (Level == 115)
                return 480479444;
            else if (Level == 116)
                return 485317884;
            else if (Level == 117)
                return 493381812;
            else if (Level == 118)
                return 580579979;
            else if (Level == 119)
                return 717424993;
                //-------------------------------------
            else if (Level == 120)
                return 282274058;
            else if (Level == 121)
                return 338728870;
            else if (Level == 122)
                return 406474644;
            else if (Level == 123)
                return 487769572;
            else if (Level == 124)
                return 585323487;
            else if (Level == 125)
                return 702388184;
            else if (Level == 126)
                return 842865821;
            else if (Level == 127)
                return 1011438985;
            else if (Level == 128)
                return 1073741823;
            else if (Level == 129)
                return 1073741823;
            else if (Level == 130)
                return 8589134588;
            else if (Level == 131)
                return 25767403764;
            else if (Level == 132)
                return 77302211292;
            else if (Level == 133)
                return 231906633876;
            else if (Level == 134)
                return 347859950814;
            else if (Level == 135)
                return 521789926221;
            else if (Level == 136)
                return 782684889332;
            else
                return 1;
        }

        public static void LoadGuilds()
        {
            MySqlDataAdapter DataAdapter = null;
            DataSet DSet = new DataSet();

            try
            {
                DataAdapter = new MySqlDataAdapter("SELECT * FROM `Guilds`", Connection);
                DataAdapter.Fill(DSet, "Guild");

                if (DSet.Tables["Guild"].Rows.Count > 0)
                {
                    int GuildCount = DSet.Tables["Guild"].Rows.Count;

                    for (int i = 0; i < GuildCount; i++)
                    {
                        DataRow DR = DSet.Tables["Guild"].Rows[i];

                        string DLs = (string)DR["DLs"];
                        string[] RDLS = DLs.Split('.');
                        string NMs = (string)DR["NormalMembers"];
                        string[] RNMs = NMs.Split('.');
                        string GL = (string)DR["GuildLeader"];

                        Guilds.AddGuild((string)DR["GuildName"], Convert.ToUInt16((uint)DR["GuildID"]), GL, RDLS, RNMs, (uint)DR["Fund"], (uint)DR["GWWins"], Convert.ToByte((uint)DR["HoldingPole"]), (uint)DR["MembersCount"], (string)DR["Bulletin"], (string)DR["Allies"], (string)DR["Enemies"]);
                    }
                    General.WriteLine("Loaded " + GuildCount + " Guilds.");
                }
            }
            catch (Exception Exc) { General.WriteLine(Convert.ToString(Exc)); }
        }
        

        public static void LoadMobs()
        {
            /*string[] MobC = File.ReadAllLines(System.Windows.Forms.Application.StartupPath + @"\Mobs.txt");

            Mobs = new string[MobC.Length][];

            for (int i = 0; i < MobC.Length; i++)
            {
                string[] Splitter = MobC[i].Split(' ');

                Mobs[i] = new string[9] { Splitter[0], Splitter[1], Splitter[2], Splitter[3], Splitter[4], Splitter[5], Splitter[6], Splitter[7], Splitter[8] };
                //MobID Mech Name HP Level MobType MinAtk MaxAtk MagicAtk
                Console.WriteLine(MobC[i]);
            }
            Console.WriteLine("All mobs loaded.");*/
   
            MySqlDataAdapter DataAdapter = null;
            DataSet DSet = new DataSet();

            try
            {
                DataAdapter = new MySqlDataAdapter("SELECT * FROM `Mobs`", Connection);
                DataAdapter.Fill(DSet, "Mob");

                if (DSet.Tables["Mob"].Rows.Count > 0)
                {
                    int MobC = DSet.Tables["Mob"].Rows.Count;

                    Mobs = new string[MobC][];

                    for (int i = 0; i < MobC; i++)
                    {
                        DataRow DR = DSet.Tables["Mob"].Rows[i];

                        Mobs[i] = new string[9] { (string)DR["MobID"], (string)DR["Mech"], (string)DR["Name"], (string)DR["HP"], (string)DR["Level"], (string)DR["MobType"], (string)DR["MinAtk"], (string)DR["MaxAtk"], (string)DR["MagicAtk"] };
                    }
                    General.WriteLine("Loaded " + MobC + " Mobs.");
                }
            }
            catch (Exception Exc) { General.WriteLine(Convert.ToString(Exc)); }
        }

        public static void LoadMobSpawns()
        {
            MySqlDataAdapter DataAdapter = null;
            DataSet DSet = new DataSet();

            try
            {
                DataAdapter = new MySqlDataAdapter("SELECT * FROM `MobSpawns`", Connection);
                DataAdapter.Fill(DSet, "Spawn");

                if (DSet.Tables["Spawn"].Rows.Count > 0)
                {
                    int SpawnC = DSet.Tables["Spawn"].Rows.Count;

                    MobSpawns = new uint[SpawnC][];

                    for (int i = 0; i < SpawnC; i++)
                    {
                        DataRow DR = DSet.Tables["Spawn"].Rows[i];

                        MobSpawns[i] = new uint[8] { (uint)DR["SpawnID"], (uint)DR["SpawnWhatID"], (uint)DR["SpawnNr"], (uint)DR["XStart"], (uint)DR["YStart"], (uint)DR["XEnd"], (uint)DR["YEnd"], (uint)DR["Map"] };
                    }
                    General.WriteLine("Loaded " + SpawnC + " Mob Spawns.");
                }
            }
            catch (Exception Exc) { General.WriteLine(Convert.ToString(Exc)); }
        }

        public static void GetPlusInfo()
        {
            try
            {
                string[] PItem = File.ReadAllLines(System.Windows.Forms.Application.StartupPath + @"\ItemAdd.ini");
                DBPlusInfo = new string[PItem.Length][];
                for (int ik = 0; ik < PItem.Length; ik++)
                {
                    string[] a = PItem[ik].Split(' ');

                    DBPlusInfo[ik] = new string[10] { a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9] };
                }
                General.WriteLine("Loading Plus info done.");
            }
            catch (Exception r) { Convert.ToString(r); }
        }

        public static void GetStats(Character Charr)
        {
            string str = "0";
            string agi = "0";
            string vit = "0";
            string spi = "0";
            string lv;

            if (Charr.Level > 120)
                lv = "120";
            else
                lv = Convert.ToString(Charr.Level);

            if (Charr.Job > 9 && Charr.Job < 16)
            {
                str = (Stats.ReadValue("Trojan", "Strength[" + lv + "]"));
                agi = (Stats.ReadValue("Trojan", "Agility[" + lv + "]"));
                vit = (Stats.ReadValue("Trojan", "Vitality[" + lv + "]"));
                spi = (Stats.ReadValue("Trojan", "Spirit[" + lv + "]"));
            }
            if (Charr.Job > 19 && Charr.Job < 26)
            {
                str = (Stats.ReadValue("Warrior", "Strength[" + lv + "]"));
                agi = (Stats.ReadValue("Warrior", "Agility[" + lv + "]"));
                vit = (Stats.ReadValue("Warrior", "Vitality[" + lv + "]"));
                spi = (Stats.ReadValue("Warrior", "Spirit[" + lv + "]"));
            }
            if (Charr.Job > 39 && Charr.Job < 46)
            {
                str = (Stats.ReadValue("Archer", "Strength[" + lv + "]"));
                agi = (Stats.ReadValue("Archer", "Agility[" + lv + "]"));
                vit = (Stats.ReadValue("Archer", "Vitality[" + lv + "]"));
                spi = (Stats.ReadValue("Archer", "Spirit[" + lv + "]"));
            }
            if (Charr.Job > 129 && Charr.Job < 136 || Charr.Job > 139 && Charr.Job < 146 || Charr.Job == 100 || Charr.Job == 101)
            {
                str = (Stats.ReadValue("Taoist", "Strength[" + lv + "]"));
                agi = (Stats.ReadValue("Taoist", "Agility[" + lv + "]"));
                vit = (Stats.ReadValue("Taoist", "Vitality[" + lv + "]"));
                spi = (Stats.ReadValue("Taoist", "Spirit[" + lv + "]"));
            }
            if (Charr.Job > 149 && Charr.Job < 156)
            {
                str = (Stats.ReadValue("Melee", "Strength[" + lv + "]"));
                agi = (Stats.ReadValue("Melee", "Agility[" + lv + "]"));
                vit = (Stats.ReadValue("Melee", "Vitality[" + lv + "]"));
                spi = (Stats.ReadValue("Melee", "Spirit[" + lv + "]"));
            }
            if (Charr.Job > 119 && Charr.Job < 126)
            {
                str = (Stats.ReadValue("Bois", "Strength[" + lv + "]"));
                agi = (Stats.ReadValue("Bois", "Agility[" + lv + "]"));
                vit = (Stats.ReadValue("Bois", "Vitality[" + lv + "]"));
                spi = (Stats.ReadValue("Bois", "Spirit[" + lv + "]"));
            }

            Charr.Str = ushort.Parse(str);
            Charr.Agi = ushort.Parse(agi);
            Charr.Vit = ushort.Parse(vit);
            Charr.Spi = ushort.Parse(spi);
        }

        public static uint GetStatus(string Acc)
        {
            Ini CN = new Ini(System.Windows.Forms.Application.StartupPath + @"\Accounts\\" + Acc + ".acc");
            uint Status = (uint)Convert.ToInt32(CN.ReadValue("Account", "Status"));
            return Status;
        }

        public static void LoadNPCs()
        {
            MySqlDataAdapter DataAdapter = null;
            DataSet DSet = new DataSet();

            try
            {
                DataAdapter = new MySqlDataAdapter("SELECT * FROM `NPCs`", Connection);
                DataAdapter.Fill(DSet, "NPC");

                if (DSet.Tables["NPC"].Rows.Count > 0)
                {
                    int NPCC = DSet.Tables["NPC"].Rows.Count;

                    NPCs = new uint[NPCC][];

                    for (int i = 0; i < NPCC; i++)
                    {
                        DataRow DR = DSet.Tables["NPC"].Rows[i];

                        NPCs[i] = new uint[8] { (uint)DR["UID"], (uint)DR["Type"], (uint)DR["Flags"], (uint)DR["Direction"], (uint)DR["X"], (uint)DR["Y"], (uint)DR["Map"], (uint)DR["SobType"] };
                    }
                    General.WriteLine("Loaded " + NPCC + " NPCs.");
                }
            }
            catch (Exception Exc) { General.WriteLine(Convert.ToString(Exc)); }
        }

        public static void LoadItems()
        {
            MySqlDataAdapter DataAdapter = null;
            DataSet DSet = new DataSet();

            try
            {
                DataAdapter = new MySqlDataAdapter("SELECT * FROM `Items`", Connection);
                DataAdapter.Fill(DSet, "Itemz");

                if (DSet.Tables["Itemz"].Rows.Count > 0)
                {
                    int ItemsC = DSet.Tables["Itemz"].Rows.Count;

                    Items = new uint[ItemsC][];

                    for (int i = 0; i < ItemsC; i++)
                    {
                        DataRow DR = DSet.Tables["Itemz"].Rows[i];

                        Items[i] = new uint[16] { (uint)DR["ItemID"], (uint)DR["ClassReq"], (uint)DR["ProfReq"], (uint)DR["LvlReq"], (uint)DR["SexReq"], (uint)DR["StrReq"], (uint)DR["AgiReq"], (uint)DR["Worth"], (uint)DR["MinAtk"], (uint)DR["MaxAtk"], (uint)DR["Defense"], (uint)DR["MDef"], (uint)DR["MAttack"], (uint)DR["Dodge"], (uint)DR["AgiGive"], (uint)DR["CPsWorth"] };
                    }
                    General.WriteLine("Loaded " + ItemsC + " items.");
                }
            }
            catch (Exception Exc) { General.WriteLine(Convert.ToString(Exc)); }
        }

        public static void LoadPortals()
        {
            MySqlDataAdapter DataAdapter = null;
            DataSet DSet = new DataSet();

            try
            {
                DataAdapter = new MySqlDataAdapter("SELECT * FROM `Portals`", Connection);
                DataAdapter.Fill(DSet, "PortalsS");

                if (DSet.Tables["PortalsS"].Rows.Count > 0)
                {
                    int PortalsC = DSet.Tables["PortalsS"].Rows.Count;

                    Portals = new ushort[PortalsC][];

                    for (int i = 0; i < PortalsC; i++)
                    {
                        DataRow DR = DSet.Tables["PortalsS"].Rows[i];

                        Portals[i] = new ushort[6] { Convert.ToUInt16((uint)DR["FromMap"]), Convert.ToUInt16((uint)DR["FromX"]), Convert.ToUInt16((uint)DR["FromY"]), Convert.ToUInt16((uint)DR["NewMap"]), Convert.ToUInt16((uint)DR["NewX"]), Convert.ToUInt16((uint)DR["NewY"]) };
                    }
                    General.WriteLine("Loaded " + PortalsC + " portals.");
                }
            }
            catch (Exception Exc) { General.WriteLine(Convert.ToString(Exc)); }
        }

        public static void SaveChar(Character Charr)
        {
            try
            {
                Ini Cli = new Ini(System.Windows.Forms.Application.StartupPath + @"\Characters\\" + Charr.Name + ".chr");
                Charr.PackInventory();
                Charr.PackEquips();
                Charr.PackSkills();
                Charr.PackProfs();
                Charr.PackWarehouses();
                Charr.PackEnemies();
                Charr.PackFriends();
                Charr.LogoutTime = (uint)Environment.TickCount; 

                Cli.WriteString("Character", "CharName", Charr.Name);
                Cli.WriteString("Character", "Level", Convert.ToString(Charr.Level));
                Cli.WriteString("Character", "Exp", Convert.ToString(Charr.Exp));
                Cli.WriteString("Character", "Strength", Convert.ToString(Charr.Str));
                Cli.WriteString("Character", "Agility", Convert.ToString(Charr.Agi));
                Cli.WriteString("Character", "Vitality", Convert.ToString(Charr.Vit));
                Cli.WriteString("Character", "Spirit", Convert.ToString(Charr.Spi));
                Cli.WriteString("Character", "Job", Convert.ToString(Charr.Job));
                Cli.WriteString("Character", "Model", Convert.ToString(Charr.RealModel));
                Cli.WriteString("Character", "Silvers", Convert.ToString(Charr.Silvers));
                Cli.WriteString("Character", "CurrentMP", Convert.ToString(Charr.CurMP));
                Cli.WriteString("Character", "CPs", Convert.ToString(Charr.CPs));
                Cli.WriteString("Character", "CurrentHP", Convert.ToString(Charr.CurHP));
                Cli.WriteString("Character", "StatPoints", Convert.ToString(Charr.StatP));
                Cli.WriteString("Character", "LocationMap", Convert.ToString(Charr.LocMap));
                Cli.WriteString("Character", "LocationX", Convert.ToString(Charr.LocX));
                Cli.WriteString("Character", "LocationY", Convert.ToString(Charr.LocY));
                Cli.WriteString("Character", "Hair", Convert.ToString(Charr.Hair));
                Cli.WriteString("Character", "Equipment", Charr.PackedEquips);
                string FullInv = "";
                FullInv = Charr.PackedInventory;
                if (FullInv.Length > 0)
                {
                    FullInv.Remove(0, 1);
                    FullInv.Remove(FullInv.Length - 1, 1);
                }
                string inv1 = FullInv; string inv2 = ""; string inv3 = ""; string inv4 = "";
                if (FullInv.Length > 251)
                {
                    inv1 = FullInv.Substring(0, 250);
                    inv2 = FullInv.Substring(250);
                }
                if (inv2.Length > 251)
                {
                    inv3 = inv2.Substring(250);
                    inv2 = inv2.Substring(0, 250);

                }
                if (inv3.Length > 251)
                {
                    inv4 = inv3.Substring(250);
                    inv3 = inv3.Substring(0, 250);

                }
                Cli.WriteString("Character", "Inventory1", inv1);
                Cli.WriteString("Character", "Inventory2", inv2);
                Cli.WriteString("Character", "Inventory3", inv3);
                Cli.WriteString("Character", "Inventory4", inv4);
                Cli.WriteString("Character", "PKPoints", Convert.ToString(Charr.PKPoints));
                Cli.WriteString("Character", "PrevMap", Convert.ToString(Charr.PrevMap));
                string FullSkill = "";
                FullSkill = Charr.PackedSkills;
                if (FullSkill.Length > 0)
                {
                    FullSkill.Remove(0, 1);
                    FullSkill.Remove(FullSkill.Length - 1, 1);
                }
                string Skill1 = FullSkill; string Skill2 = ""; string Skill3 = "";
                if (FullInv.Length > 251)
                {
                    Skill1 = FullSkill.Substring(0, 250);
                    Skill2 = FullSkill.Substring(250);
                }
                if (Skill2.Length > 251)
                {
                    Skill3 = Skill2.Substring(250);
                    Skill2 = Skill2.Substring(0, 250);

                }
                Cli.WriteString("Character", "Skills1", Skill1);
                Cli.WriteString("Character", "Skills2", Skill2);
                Cli.WriteString("Character", "Skills3", Skill3);
                Cli.WriteString("Character", "RBCount", Convert.ToString(Charr.RBCount));
                Cli.WriteString("Character", "Avatar", Convert.ToString(Charr.Avatar));
                Cli.WriteString("Character", "WHMoney", Convert.ToString(Charr.WHSilvers));
                Cli.WriteString("Character", "VP", Convert.ToString(Charr.VP));
                string FullWH = "";
                FullWH = Charr.PackedWHs;
                if (FullWH.Length > 0)
                {
                    FullWH.Remove(0, 1);
                    FullWH.Remove(FullWH.Length - 1, 1);
                }
                string wh1 = FullWH; string wh2 = ""; string wh3 = ""; string wh4 = ""; string wh5 = ""; string wh6 = ""; string wh7 = ""; string wh8 = ""; string wh9 = ""; string wh10 = "";
                if (FullWH.Length > 251)
                {
                    wh1 = FullWH.Substring(0, 250);
                    wh2 = FullWH.Substring(250);
                }
                if (wh2.Length > 251)
                {
                    wh3 = wh2.Substring(250);
                    wh2 = wh2.Substring(0, 250);

                }
                if (wh3.Length > 251)
                {
                    wh4 = wh3.Substring(250);
                    wh3 = wh3.Substring(0, 250);

                }
                if (wh4.Length > 251)
                {
                    wh5 = wh4.Substring(250);
                    wh4 = wh4.Substring(0, 250);

                }
                if (wh5.Length > 251)
                {
                    wh6 = wh5.Substring(250);
                    wh5 = wh5.Substring(0, 250);

                }
                if (wh6.Length > 251)
                {
                    wh7 = wh6.Substring(250);
                    wh6 = wh6.Substring(0, 250);

                }
                if (wh7.Length > 251)
                {
                    wh8 = wh7.Substring(250);
                    wh7 = wh7.Substring(0, 250);

                }
                if (wh8.Length > 251)
                {
                    wh9 = wh8.Substring(250);
                    wh8 = wh8.Substring(0, 250);

                }
                if (wh9.Length > 251)
                {
                    wh10 = wh9.Substring(250);
                    wh9 = wh9.Substring(0, 250);

                }
                Cli.WriteString("Character", "Warehouses1", wh1);
                Cli.WriteString("Character", "Warehouses2", wh2);
                Cli.WriteString("Character", "Warehouses3", wh3);
                Cli.WriteString("Character", "Warehouses4", wh4);
                Cli.WriteString("Character", "Warehouses5", wh5);
                Cli.WriteString("Character", "Warehouses6", wh6);
                Cli.WriteString("Character", "Warehouses7", wh7);
                Cli.WriteString("Character", "Warehouses8", wh8);
                Cli.WriteString("Character", "Warehouses9", wh9);
                Cli.WriteString("Character", "Warehouses10", wh10);
                Cli.WriteString("Character", "Friends", Charr.PackedFriends);
                Cli.WriteString("Character", "Enemies", Charr.PackedEnemies);
                Cli.WriteString("Character", "Profs", Charr.PackedProfs);
                Cli.WriteString("Character", "Model", Convert.ToString(Charr.Model));
                Cli.WriteString("Character", "QuestMob", Charr.QuestMob);
                Cli.WriteString("Character", "QuestFrom", Charr.QuestFrom);
                Cli.WriteString("Character", "QuestKO", Convert.ToString(Charr.QuestKO));
                Cli.WriteString("Character", "dexp", Convert.ToString(Charr.dexp));
                Cli.WriteString("Character", "dexptime", Convert.ToString(Charr.dexptime));
                Cli.WriteString("Character", "LuckyTime", Convert.ToString(Charr.LuckTime));
                Cli.WriteString("Character", "GuildDonation", Convert.ToString(Charr.GuildDonation));
                Cli.WriteString("Character", "MyGuild", Convert.ToString(Charr.GuildID));
                Cli.WriteString("Character", "GuildPos", Convert.ToString(Charr.GuildPosition));
                Cli.WriteString("Character", "OfflineTG", Convert.ToString(Charr.OfflineTG));
                Cli.WriteString("Character", "LogoutTime", Convert.ToString(Charr.LogoutTime));
            }

            catch (Exception Exc)
            {
                Console.WriteLine(Exc.ToString());
            }
        }

        public static void SaveHB(Character Charr)
        {
            try
            {
                Ini Cli = new Ini(System.Windows.Forms.Application.StartupPath + @"\Characters\\" + Charr.Name + ".chr");
                Cli.WriteString("Character", "HeavenBless", Convert.ToString(Charr.HBEnd));
                Cli.WriteString("Character", "WhichBless", Convert.ToString(Charr.WhichBless));
            }

            catch (Exception Exc)
            {
                Console.WriteLine(Exc.ToString());
            }
        }

        public static void SaveSpouse(Character Charr)
        {
            try
            {
                Ini Cli = new Ini(System.Windows.Forms.Application.StartupPath + @"\Characters\\" + Charr.Name + ".chr");
                Cli.WriteString("Character", "Spouse", Charr.Spouse);
            }

            catch (Exception Exc)
            {
                Console.WriteLine(Exc.ToString());
            }
        }

        public static void SaveFirstRB(Character Charr)
        {
            try
            {
                Ini Cli = new Ini(System.Windows.Forms.Application.StartupPath + @"\Characters\\" + Charr.Name + ".chr");
                Cli.WriteString("Character", "FirstLevel", Convert.ToString(Charr.FirstLevel));
                Cli.WriteString("Character", "FirstJob", Convert.ToString(Charr.FirstJob));
            }

            catch (Exception Exc)
            {
                Console.WriteLine(Exc.ToString());
            }
        }

        public static void SaveSecondRB(Character Charr)
        {
            try
            {
                Ini Cli = new Ini(System.Windows.Forms.Application.StartupPath + @"\Characters\\" + Charr.Name + ".chr");
                Cli.WriteString("Character", "SecondLevel", Convert.ToString(Charr.SecondLevel));
                Cli.WriteString("Character", "SecondJob", Convert.ToString(Charr.SecondJob));
            }

            catch (Exception Exc)
            {
                Console.WriteLine(Exc.ToString());
            }
        }

        public static void SaveWHPW(Character Charr)
        {
            try
            {
                Ini Cli = new Ini(System.Windows.Forms.Application.StartupPath + @"\Characters\\" + Charr.Name + ".chr");
                Cli.WriteString("Character", "WHPW", Charr.WHPW);
                Cli.WriteString("Character", "WHPWcheck", Convert.ToString(Charr.WHPWcheck));
            }

            catch (Exception Exc)
            {
                Console.WriteLine(Exc.ToString());
            }
        }

        public static void SaveKO(Character Charr)
        {
            try
            {
                if (Charr.OldKO > Charr.KO)
                {
                    Charr.KO = 0;
                }
                else if (Charr.KO > Charr.OldKO)
                {
                    Ini Cli = new Ini(System.Windows.Forms.Application.StartupPath + @"\Characters\\" + Charr.Name + ".chr");
                    Cli.WriteString("Character", "KO", Convert.ToString(Charr.KO));
                    Cli.WriteString("Character", "OldKO", Convert.ToString(Charr.OldKO));
                }
                Charr.KO = 0;
            }

            catch (Exception Exc)
            {
                Console.WriteLine(Exc.ToString());
            }
        }

        public static void SaveDisKO(Character Charr)
        {
            try
            {
                Ini Cli = new Ini(System.Windows.Forms.Application.StartupPath + @"\Characters\\" + Charr.Name + ".chr");
                Cli.WriteString("Character", "DisKO", Convert.ToString(Charr.DisKO));
            }

            catch (Exception Exc)
            {
                Console.WriteLine(Exc.ToString());
            }
        }

        public static void SaveBan(Character Charr)
        {
            try
            {
                Ini Cli = new Ini(System.Windows.Forms.Application.StartupPath + @"\Characters\\" + Charr.Name + ".chr");
                Cli.WriteString("Character", "BanNB", Convert.ToString(Charr.BanNB));
            }

            catch (Exception Exc)
            {
                Console.WriteLine(Exc.ToString());
            }
        }

        public static void GetCharInfo(Character Charr, string Acc)
        {
            try
            {
                Ini CN = new Ini(System.Windows.Forms.Application.StartupPath + @"\Accounts\\" + Acc + ".acc");
                string CName = CN.ReadValue("Account", "Charr");
                Ini CharacterInfo = new Ini(System.Windows.Forms.Application.StartupPath + @"\Characters\\" + CName + ".chr");

                Charr.Name = CName;
                Charr.UID = (uint)Convert.ToInt64(CharacterInfo.ReadValue("Character", "UID"));
                Charr.Spouse = CharacterInfo.ReadValue("Character", "Spouse");
                Charr.Avatar = (byte)Convert.ToByte(CharacterInfo.ReadValue("Character", "Avatar"));
                Charr.Job = Convert.ToByte(CharacterInfo.ReadValue("Character", "Job"));
                Charr.Level = Convert.ToByte(CharacterInfo.ReadValue("Character", "Level"));
                Charr.FirstJob = Convert.ToByte(CharacterInfo.ReadValue("Character", "FirstJob"));
                Charr.FirstLevel = Convert.ToByte(CharacterInfo.ReadValue("Character", "FirstLevel"));
                Charr.SecondJob = Convert.ToByte(CharacterInfo.ReadValue("Character", "SecondJob"));
                Charr.SecondLevel = Convert.ToByte(CharacterInfo.ReadValue("Character", "SecondLevel"));
                Charr.Exp = Convert.ToUInt64(CharacterInfo.ReadValue("Character", "Exp"));
                Charr.Str = (ushort)Convert.ToInt16(CharacterInfo.ReadValue("Character", "Strength"));
                Charr.Agi = (ushort)Convert.ToInt16(CharacterInfo.ReadValue("Character", "Agility"));
                Charr.Vit = (ushort)Convert.ToInt16(CharacterInfo.ReadValue("Character", "Vitality"));
                Charr.Spi = (ushort)Convert.ToInt16(CharacterInfo.ReadValue("Character", "Spirit"));
                Charr.StatP = (ushort)Convert.ToInt16(CharacterInfo.ReadValue("Character", "StatPoints"));
                Charr.LocMap = (ushort)Convert.ToInt16(CharacterInfo.ReadValue("Character", "LocationMap"));
                Charr.LocX = (ushort)Convert.ToInt16(CharacterInfo.ReadValue("Character", "LocationX"));
                Charr.LocY = (ushort)Convert.ToInt16(CharacterInfo.ReadValue("Character", "LocationY"));
                Charr.RBCount = Convert.ToByte(CharacterInfo.ReadValue("Character", "RBCount"));
                Charr.Silvers = (uint)Convert.ToInt32(CharacterInfo.ReadValue("Character", "Silvers"));
                Charr.CPs = (uint)Convert.ToInt32(CharacterInfo.ReadValue("Character", "CPs"));
                Charr.Hair = Convert.ToUInt16(CharacterInfo.ReadValue("Character", "Hair"));
                Charr.PKPoints = Convert.ToUInt16(CharacterInfo.ReadValue("Character", "PKPoints"));
                Charr.Model = Convert.ToUInt16(CharacterInfo.ReadValue("Character", "Model"));
                Charr.CurHP = Convert.ToUInt16(CharacterInfo.ReadValue("Character", "CurrentHP"));
                Charr.CurMP = Convert.ToUInt16(CharacterInfo.ReadValue("Character", "CurrentMP"));
                Charr.PackedEquips = CharacterInfo.ReadValue("Character", "Equipment");
                Charr.PackedInventory = CharacterInfo.ReadValue("Character", "Inventory1");
                Charr.PackedInventory += CharacterInfo.ReadValue("Character", "Inventory2");
                Charr.PackedInventory += CharacterInfo.ReadValue("Character", "Inventory3");
                Charr.PackedInventory += CharacterInfo.ReadValue("Character", "Inventory4");
                Charr.PackedSkills = CharacterInfo.ReadValue("Character", "Skills1");
                Charr.PackedSkills += CharacterInfo.ReadValue("Character", "Skills2");
                Charr.PackedSkills += CharacterInfo.ReadValue("Character", "Skills3");
                Charr.PackedProfs = CharacterInfo.ReadValue("Character", "Profs");
                Charr.WHSilvers = (uint)Convert.ToInt32(CharacterInfo.ReadValue("Character", "WHMoney"));
                Charr.PackedWHs = CharacterInfo.ReadValue("Character", "Warehouses1");
                Charr.PackedWHs += CharacterInfo.ReadValue("Character", "Warehouses2");
                Charr.PackedWHs += CharacterInfo.ReadValue("Character", "Warehouses3");
                Charr.PackedWHs += CharacterInfo.ReadValue("Character", "Warehouses4");
                Charr.PackedWHs += CharacterInfo.ReadValue("Character", "Warehouses5");
                Charr.PackedWHs += CharacterInfo.ReadValue("Character", "Warehouses6");
                Charr.PackedWHs += CharacterInfo.ReadValue("Character", "Warehouses7");
                Charr.PackedWHs += CharacterInfo.ReadValue("Character", "Warehouses8");
                Charr.PackedWHs += CharacterInfo.ReadValue("Character", "Warehouses9");
                Charr.PackedWHs += CharacterInfo.ReadValue("Character", "Warehouses10");
                Charr.PackedFriends = CharacterInfo.ReadValue("Character", "Friends");
                Charr.PackedEnemies = CharacterInfo.ReadValue("Character", "Enemies");
                Charr.VP = Convert.ToUInt16(CharacterInfo.ReadValue("Character", "VP"));
                Charr.PrevMap = Convert.ToUInt16(CharacterInfo.ReadValue("Character", "PrevMap"));
                /*Charr.OfflineTG = Convert.ToBoolean(CharacterInfo.ReadValue("Character", "OfflineTG"));
                Charr.LogoutTime = ulong.Parse(CharacterInfo.ReadValue("Character", "LogoutTime"));
                Charr.HBEnd2 = CharacterInfo.ReadValue("Character", "HeavenBless");
                Charr.WhichBless = Convert.ToByte(CharacterInfo.ReadValue("Character", "WhichBless"));
                Charr.Curse = uint.Parse(CharacterInfo.ReadValue("Character", "Curse"));
                Charr.Donation = uint.Parse(CharacterInfo.ReadValue("Character", "Donation"));
                Charr.Nobility = Convert.ToByte(CharacterInfo.ReadValue("Character", "Nobility"));*/
                Charr.WHPW = CharacterInfo.ReadValue("Character", "WHPW");
                Charr.dexp = Convert.ToByte(CharacterInfo.ReadValue("Character", "dexp"));
                Charr.QuestFrom = CharacterInfo.ReadValue("Character", "QuestFrom");
                Charr.QuestMob = CharacterInfo.ReadValue("Character", "QuestMob");
                Charr.WHPWcheck = Convert.ToUInt16(CharacterInfo.ReadValue("Character", "WHPWcheck"));
                Charr.LuckTime = uint.Parse(CharacterInfo.ReadValue("Character", "LuckyTime"));
                Charr.dexptime = uint.Parse(CharacterInfo.ReadValue("Character", "dexptime"));
                Charr.KO = Convert.ToUInt16(CharacterInfo.ReadValue("Character", "KO"));
                Charr.OldKO = Convert.ToUInt16(CharacterInfo.ReadValue("Character", "OldKO"));
                Charr.DisKO = Convert.ToUInt16(CharacterInfo.ReadValue("Character", "DisKO"));
                Charr.QuestKO = Convert.ToUInt16(CharacterInfo.ReadValue("Character", "QuestKO"));
                Charr.BanNB = uint.Parse(CharacterInfo.ReadValue("Character", "BanNB"));
                Charr.GuildDonation = uint.Parse(CharacterInfo.ReadValue("Character", "GuildDonation"));
                Charr.GuildID = Convert.ToUInt16(CharacterInfo.ReadValue("Character", "MyGuild"));
                Charr.GuildPosition = Convert.ToByte(CharacterInfo.ReadValue("Character", "GuildPos"));
                if (Guilds.AllGuilds.Contains(Charr.GuildID))
                    Charr.MyGuild = (Guild)Guilds.AllGuilds[Charr.GuildID];
                Charr.MinAtk = Charr.Str;
                Charr.MaxAtk = Charr.Str;
                Charr.Potency = Charr.Level;
                Charr.RealModel = Charr.Model;
                Charr.RealAvatar = Charr.Avatar;
                Charr.MaxHP = Charr.BaseMaxHP();
                Charr.MaxMP = Charr.MaxMana();
                Charr.RealAgi = Charr.Agi;
                if (Charr.OfflineTG == true)
                {
                    Charr.Exp += (ulong)(Math.Abs((uint)Environment.TickCount - (uint)Charr.LogoutTime) * 0.71);
                    Charr.OfflineTG = false;
                    Charr.LogoutTime = 0;
                }
            }
            catch (Exception u) { Console.WriteLine(u); }
        }

        public static bool CreateCharacter(string Name, uint Class, uint Model, uint Avatar, Client UClient)
        {
            if (!File.Exists(System.Windows.Forms.Application.StartupPath + @"\Characters\\" + Name + ".chr"))
            {
                Ini Create = new Ini(System.Windows.Forms.Application.StartupPath + @"\Characters\\" + Name + ".chr");
                Ini AccChange = new Ini(System.Windows.Forms.Application.StartupPath + @"\Accounts\\" + UClient.Account + ".acc");

                string str = "0";
                string agi = "0";
                string vit = "0";
                string spi = "0";

                if (Class == 10)
                {
                    str = (Stats.ReadValue("Trojan", "Strength[1]"));
                    agi = (Stats.ReadValue("Trojan", "Agility[1]"));
                    vit = (Stats.ReadValue("Trojan", "Vitality[1]"));
                    spi = (Stats.ReadValue("Trojan", "Spirit[1]"));
                }
                if (Class == 20)
                {
                    str = (Stats.ReadValue("Warrior", "Strength[1]"));
                    agi = (Stats.ReadValue("Warrior", "Agility[1]"));
                    vit = (Stats.ReadValue("Warrior", "Vitality[1]"));
                    spi = (Stats.ReadValue("Warrior", "Spirit[1]"));
                }
                if (Class == 40)
                {
                    str = (Stats.ReadValue("Archer", "Strength[1]"));
                    agi = (Stats.ReadValue("Archer", "Agility[1]"));
                    vit = (Stats.ReadValue("Archer", "Vitality[1]"));
                    spi = (Stats.ReadValue("Archer", "Spirit[1]"));
                }
                if (Class == 100)
                {
                    str = (Stats.ReadValue("Taoist", "Strength[1]"));
                    agi = (Stats.ReadValue("Taoist", "Agility[1]"));
                    vit = (Stats.ReadValue("Taoist", "Vitality[1]"));
                    spi = (Stats.ReadValue("Taoist", "Spirit[1]"));
                }
                ulong uid = (uint)General.Rand.Next(1000001, 19999999) + (uint)General.Rand.Next(1000001, 19999999) + (uint)General.Rand.Next(1000001, 19999999) / (uint)General.Rand.Next(1000, 19999);
                string mp = Convert.ToString(short.Parse(spi) * 3);
                string hp = Convert.ToString((short.Parse(vit) * 24 + short.Parse(str) * 3 + short.Parse(agi) * 3 + short.Parse(spi) * 3));

                Create.WriteString("Character", "CharName", Name);
                Create.WriteString("Character", "Spouse", "Non");
                Create.WriteString("Character", "Account", UClient.Account);
                Create.WriteString("Character", "UID", Convert.ToString(uid));
                Create.WriteString("Character", "Job", Convert.ToString(Class));
                Create.WriteString("Character", "Level", "1");
                Create.WriteString("Character", "FirstJob", "0");
                Create.WriteString("Character", "FirstLevel", "0");
                Create.WriteString("Character", "SecondJob", "0");
                Create.WriteString("Character", "SecondLevel", "0");
                Create.WriteString("Character", "Exp", "0");
                Create.WriteString("Character", "Strength", str);
                Create.WriteString("Character", "Agility", agi);
                Create.WriteString("Character", "Vitality", vit);
                Create.WriteString("Character", "Spirit", spi);
                Create.WriteString("Character", "Model", Convert.ToString(Model));
                Create.WriteString("Character", "Silvers", "100");
                Create.WriteString("Character", "CPs", "0");
                Create.WriteString("Character", "CurrentHP", hp);
                Create.WriteString("Character", "CurrentMP", mp);
                Create.WriteString("Character", "StatPoints", "0");
                Create.WriteString("Character", "LocationMap", "1002");
                Create.WriteString("Character", "LocationX", "430");
                Create.WriteString("Character", "LocationY", "380");
                Create.WriteString("Character", "Hair", "0");
                Create.WriteString("Character", "Equipment", "");
                Create.WriteString("Character", "Inventory1", "");
                Create.WriteString("Character", "Inventory2", "");
                Create.WriteString("Character", "Inventory3", "");
                Create.WriteString("Character", "Inventory4", "");
                Create.WriteString("Character", "PKPoints", "0");
                Create.WriteString("Character", "Skills1", "");
                Create.WriteString("Character", "Skills2", "");
                Create.WriteString("Character", "Skills3", "");
                Create.WriteString("Character", "Profs", "");
                Create.WriteString("Character", "RBCount", "0");
                Create.WriteString("Character", "StatPoints", "0");
                Create.WriteString("Character", "Avatar", "67");
                Create.WriteString("Character", "WHMoney", "0");
                Create.WriteString("Character", "Warehouses1", ":::::");
                Create.WriteString("Character", "Warehouses2", "");
                Create.WriteString("Character", "Warehouses3", "");
                Create.WriteString("Character", "Warehouses4", "");
                Create.WriteString("Character", "Warehouses5", "");
                Create.WriteString("Character", "Warehouses6", "");
                Create.WriteString("Character", "Warehouses7", "");
                Create.WriteString("Character", "Warehouses8", "");
                Create.WriteString("Character", "Warehouses9", "");
                Create.WriteString("Character", "Warehouses10", "");
                Create.WriteString("Character", "VP", "0");
                Create.WriteString("Character", "Friends", "");
                Create.WriteString("Character", "Enemies", "");
                Create.WriteString("Character", "GuildDonation", "0");
                Create.WriteString("Character", "MyGuild", "0");
                Create.WriteString("Character", "GuildPos", "0");
                Create.WriteString("Character", "PrevMap", "0");
                Create.WriteString("Character", "OfflineTG", "0");
                Create.WriteString("Character", "LogoutTime", "0");
                Create.WriteString("Character", "HeavenBless", "");
                Create.WriteString("Character", "WhichBless", "0");
                Create.WriteString("Character", "Curse", "0");
                Create.WriteString("Character", "Donation", "0");
                Create.WriteString("Character", "Nobility", "0");
                Create.WriteString("Character", "KO", "0");
                Create.WriteString("Character", "OldKO", "0");
                Create.WriteString("Character", "DisKO", "0");
                Create.WriteString("Character", "QuestMob", "");
                Create.WriteString("Character", "QuestFrom", "");
                Create.WriteString("Character", "QuestKO", "0");
                Create.WriteString("Character", "WHPW", "");
                Create.WriteString("Character", "WHPWcheck", "0");
                Create.WriteString("Character", "dexp", "0");
                Create.WriteString("Character", "dexptime", "0");
                Create.WriteString("Character", "LuckyTime", "0");
                Create.WriteString("Character", "BanNB", "0");
                AccChange.WriteString("Account", "LogonType", "1");
                AccChange.WriteString("Account", "Charr", Name);
                return true;
            }
            else return false;
        }

        public static byte Authenticate(string Username, string Password)
        {
            if (File.Exists(System.Windows.Forms.Application.StartupPath + @"\Accounts\\" + Username + ".acc"))
            {
                Ini SR = new Ini(System.Windows.Forms.Application.StartupPath + @"\Accounts\\" + Username + ".acc");

                string Acc = SR.ReadValue("Account", "AccountID");
                string Pass = SR.ReadValue("Account", "Password");
                uint LogonType = uint.Parse(SR.ReadValue("Account", "LogonType"));
                int LogonCount = Convert.ToInt16(SR.ReadValue("Account", "LogonCount"));

                if (Pass == Password || Pass == "")
                {
                    if (Pass == "")
                    {
                        SR.WriteString("Account", "Password", Password);
                    }

                    LogonCount++;
                    SR.WriteString("Account", "LogonCount", Convert.ToString(LogonCount));
                    return Convert.ToByte((uint)LogonType);
                }
                else
                {
                    return 0;
                }

            }
            else return 0;
        }
    }
}

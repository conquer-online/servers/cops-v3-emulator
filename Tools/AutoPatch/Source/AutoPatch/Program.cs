﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.IO;
using System.Diagnostics;

namespace AutoPatch
{
    static class Program
    {
        /// <summary>
        /// Point d'entrée principal de l'application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new AutoPatch());
        }

        public static bool ChanceSuccess(double pc)
        {
            return ((double)Rand.Next(1, 1000000)) / 10000 >= 100 - pc;
        }
        public static Random Rand = new Random();

        public static string HTTP = "";

        public static string patch = "";
        public static string main = "";
        public static string fail = "";
        public static string failparam = "";

        public static string Display = "";
        public static string CopyRight = "";

        public static string VFile = "";
        public static int Version = 0;

        public static bool DLPatch = false;

        public static void Config()
        {
            try
            {
                General.Decrypt();

                Ini Config = new Ini(System.Windows.Forms.Application.StartupPath + @"\\AutoPatch.dat");

                HTTP = Config.ReadValue("ftp", "HTTP");

                patch = Config.ReadValue("program", "patch");
                main = Config.ReadValue("program", "main");
                fail = Config.ReadValue("program", "fail");
                failparam = Config.ReadValue("program", "failparam");

                Display = Config.ReadValue("string", "Display");
                CopyRight = Config.ReadValue("string", "CopyRight");

                VFile = Config.ReadValue("Check", "VersionFile");
                Version = Convert.ToInt32(File.ReadAllText(VFile));

                if (File.Exists(@"AutoPatch.dat"))
                    File.Delete(@"AutoPatch.dat");
            }
            catch { MessageBox.Show("Erreur d'initialisation!", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error); Application.Exit(); }
        }

        public static void Lauch()
        {
            if (DLPatch == true)
            {
                Process Laucher = new Process();
                Laucher.StartInfo.FileName = patch;
                try
                {
                    Laucher.Start();
                }
                catch
                {
                    MessageBox.Show(patch + " n'a pas été trouvé ou un problème est survenu!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                Application.Exit();
            }
            else
            {
                Process Laucher = new Process();
                Laucher.StartInfo.FileName = main;
                if (fail != "/")
                    if (fail == main)
                        Laucher.StartInfo.Arguments = failparam;
                try
                {
                    if (File.Exists(patch))
                        File.Delete(patch);

                    Laucher.Start();
                }
                catch
                {
                    MessageBox.Show(main + " n'a pas été trouvé ou un problème est survenu!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                Application.Exit();
            }
        }
    }

    public class Ini
    {
        public IniFile TheIni;

        public Ini(string path)
        {
            TheIni = new IniFile(path);
        }

        public string ReadValue(string Section, string Key)
        {
            string it = TheIni.IniReadValue(Section, Key);
            return it;
        }
        public void WriteString(string Section, string Key, string Value)
        {
            TheIni.IniWriteValue(Section, Key, Value);
        }
    }

    public class IniFile
    {
        public string path;

        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section,
            string key, string def, StringBuilder retVal, int size, string filePath);

        public IniFile(string INIPath)
        {
            path = INIPath;
        }
        public void IniWriteValue(string Section, string Key, string Value)
        {
            WritePrivateProfileString(Section, Key, Value, this.path);
        }

        public string IniReadValue(string Section, string Key)
        {
            StringBuilder temp = new StringBuilder(255);
            int i = GetPrivateProfileString(Section, Key, "", temp, 255, this.path);
            return temp.ToString();

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.IO;

namespace Play
{
    static class Program
    {
        /// <summary>
        /// Point d'entrée principal de l'application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Play());
        }
        public static string WebBox = "";
        public static string Ip = "127.0.0.1";
        public static int GamePort = 0;
        public static int LoginPort = 0;
        public static int AccountPort = 0;

        public static string main = "";
        public static string fail = "";
        public static string failparam = "";

        public static void Config()
        {
            try
            {
                byte[] Bytes = File.ReadAllBytes(@"Play.ini");
                Cryptographer.Decrypt(ref Bytes);
                File.WriteAllBytes(@"Play.dat", Bytes);

                if (File.Exists("Play.dat"))
                {
                    Ini Config = new Ini(System.Windows.Forms.Application.StartupPath + @"\Play.dat");

                    WebBox = Config.ReadValue("Internet", "WebBox");
                    Ip = Config.ReadValue("Internet", "InternetProtocol");
                    LoginPort = (int)Convert.ToInt32(Config.ReadValue("Internet", "LoginPort"));
                    GamePort = (int)Convert.ToInt32(Config.ReadValue("Internet", "GamePort"));
                    AccountPort = (int)Convert.ToInt32(Config.ReadValue("Internet", "IccountPort"));

                    main = Config.ReadValue("Laucher", "main");
                    fail = Config.ReadValue("Laucher", "fail");
                    failparam = Config.ReadValue("Laucher", "failparam");

                    File.Delete("Play.dat");
                }
            }
            catch { MessageBox.Show("Erreur d'initialisation!", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error); Application.Exit(); }
        }
    }
}

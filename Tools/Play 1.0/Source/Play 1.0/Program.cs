﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace Play
{
    static class Program
    {
        public static Socket Login = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        public static Socket Game = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        public static Socket Account = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        public static string GameOn;
        public static string AccountOn;
        public static string LoginOn;
        /// <summary>
        /// Point d'entrée principal de l'application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Main());
        }

        public static void LoginStatus()
        {
            try
            {
                Ini status_ = new Ini(System.Windows.Forms.Application.StartupPath + @"\play.ini");
                string status_Host = status_.ReadValue(Convert.ToString("Config"), "Host");
                string status_LoginPort = status_.ReadValue(Convert.ToString("Config"), "LoginPort");

                IPAddress IP = IPAddress.Parse(status_Host);
                uint Port = uint.Parse(status_LoginPort);

                Login.Connect(IP, (int)Port);
                LoginOn = "En ligne";
            }
            catch
            {
                LoginOn = "Hors ligne";
            }
        }

        public static void GameStatus()
        {
            try
            {
                Ini status_ = new Ini(System.Windows.Forms.Application.StartupPath + @"\play.ini");
                string status_Host = status_.ReadValue(Convert.ToString("Config"), "Host");
                string status_GamePort = status_.ReadValue(Convert.ToString("Config"), "GamePort");

                IPAddress IP = IPAddress.Parse(status_Host);
                uint Port = uint.Parse(status_GamePort);

                Game.Connect(IP, (int)Port);
                GameOn = "En ligne";
            }
            catch
            {
                GameOn = "Hors ligne";
            }
        }

        public static void AccountStatus()
        {
            try
            {
                Ini status_ = new Ini(System.Windows.Forms.Application.StartupPath + @"\play.ini");
                string status_Host = status_.ReadValue(Convert.ToString("Config"), "Host");
                string status_AccountPort = status_.ReadValue(Convert.ToString("Config"), "AccountPort");

                IPAddress IP = IPAddress.Parse(status_Host);
                uint Port = uint.Parse(status_AccountPort);

                Account.Connect(IP, (int)Port);
                AccountOn = "En ligne";
            }
            catch
            {
                AccountOn = "Hors ligne";
            }
        }
    }

    public class Ini
    {
        public IniFile TheIni;

        public Ini(string path)
        {
            TheIni = new IniFile(path);
        }

        public string ReadValue(string Section, string Key)
        {
            string it = TheIni.IniReadValue(Section, Key);
            return it;
        }
        public void WriteString(string Section, string Key, string Value)
        {
            TheIni.IniWriteValue(Section, Key, Value);
        }
    }

    public class IniFile
    {
        public string path;

        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section,
            string key, string def, StringBuilder retVal, int size, string filePath);

        public IniFile(string INIPath)
        {
            path = INIPath;
        }
        public void IniWriteValue(string Section, string Key, string Value)
        {
            WritePrivateProfileString(Section, Key, Value, this.path);
        }

        public string IniReadValue(string Section, string Key)
        {
            StringBuilder temp = new StringBuilder(255);
            int i = GetPrivateProfileString(Section, Key, "", temp, 255, this.path);
            return temp.ToString();

        }
    }
}

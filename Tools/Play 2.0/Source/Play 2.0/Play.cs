﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Diagnostics;

namespace Play
{
    public partial class Play : Form
    {
        public Play()
        {
            InitializeComponent();
            Program.Config();
            webBrowser1.Url = new System.Uri(Program.WebBox, System.UriKind.Absolute);

            label8.Text = Program.Ip;

            Socket Login = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            Socket Game = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            Socket Account = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            try
            {
                IPAddress IP = IPAddress.Parse(Program.Ip);
                uint Port = (uint)Program.LoginPort;

                Login.Connect(IP, (int)Port);
                label5.Text = "En Ligne";
                label5.ForeColor = Color.Green;
            }
            catch
            {
                label5.Text = "Hors Ligne";
                label5.ForeColor = Color.Red;
            }

            TcpClient GameServerCheck = new TcpClient();
            try
            {
                GameServerCheck.Connect(Program.Ip, Program.GamePort);

                label6.Text = "En Ligne";
                label6.ForeColor = Color.Green;
            }
            catch
            {
                label6.Text = "Hors Ligne";
                label6.ForeColor = Color.Red;
            }

            TcpClient AccountServerCheck = new TcpClient();
            try
            {
                AccountServerCheck.Connect(Program.Ip, Program.AccountPort);

                label7.Text = "En Ligne";
                label7.ForeColor = Color.Green;
            }
            catch
            {
                label7.Text = "Hors Ligne";
                label7.ForeColor = Color.Red;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Logiciel pour lancer COPS v3. Créé par ~K!ra~.", "Information!", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Process Laucher = new Process();
            Laucher.StartInfo.FileName = Program.main;
            if (Program.fail != "/")
                if (Program.fail == Program.main)
                    Laucher.StartInfo.Arguments = Program.failparam;
            try
            {
                Laucher.Start();
            }
            catch
            {
                MessageBox.Show(Program.main + " n'a pas été trouvé ou un problème est survenu!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            this.Close();
        }
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Threading;
using System.Diagnostics;
using System.Windows.Forms;
using System.Timers;
using System.Data;
using System.ComponentModel;
using System.IO;

namespace AutoPatch
{
    public class General
    {
        public static void Decrypt()
        {
            byte[] Bytes = File.ReadAllBytes(@"AutoPatch.ini");
            Cryptographer.Decrypt(ref Bytes);
            File.WriteAllBytes(@"AutoPatch.dat", Bytes);
        }
    }
}
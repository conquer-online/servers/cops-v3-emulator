using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace COServer_Project
{
    public class Other
    {
        public static uint EquipNextLevel(uint ItemId)
        {
            uint NewItem = ItemId;

            if (Other.ArmorType(ItemId) == false || Other.WeaponType(ItemId) == 117)
            {
                if (Other.ItemType2(ItemId) != 12 && Other.ItemType2(ItemId) != 15 && Other.ItemType2(ItemId) != 16 || Other.ItemInfo(ItemId)[3] == 45 && Other.ItemType2(ItemId) == 12 || Other.ItemInfo(ItemId)[3] >= 112 && Other.ItemType2(ItemId) == 12)
                    NewItem += 10;
                else if (Other.ItemType2(ItemId) == 12 && Other.ItemInfo(ItemId)[3] < 45)
                    NewItem += 20;
                else if (Other.ItemType2(ItemId) == 12 && Other.ItemInfo(ItemId)[3] >= 52 && Other.ItemInfo(ItemId)[3] < 112)
                    NewItem += 30;
                else if (Other.ItemType2(ItemId) == 15 && Other.ItemInfo(ItemId)[3] == 1 || Other.ItemType2(ItemId) == 15 && Other.ItemInfo(ItemId)[3] >= 110)
                    NewItem += 10;
                else if (Other.ItemType2(ItemId) == 15)
                    NewItem += 20;
                else if (Other.ItemType2(ItemId) == 16 && Other.ItemInfo(ItemId)[3] < 110)
                    NewItem += 20;
                else if (Other.ItemType2(ItemId) == 16)
                    NewItem += 10;

                if (Other.WeaponType(NewItem) == 421)
                {
                    NewItem = ItemId;
                    if (Other.ItemInfo(ItemId)[3] == 45 || Other.ItemInfo(ItemId)[3] == 55)
                        NewItem += 20;
                    else
                        NewItem += 10;
                }
            }
            else if (Other.ItemType2(ItemId) != 12 && Other.ItemType2(ItemId) != 15)
            {
                if (Other.ItemInfo(ItemId)[1] == 21)
                    if (Other.ItemType2(ItemId) == 13)
                    {
                        if (Other.ItemInfo(ItemId)[3] < 110)
                            NewItem += 10;
                        else
                            NewItem += 5000;
                    }
                if (Other.ItemInfo(ItemId)[1] == 11)
                    if (Other.ItemType2(ItemId) == 13)
                    {
                        if (Other.ItemInfo(ItemId)[3] < 110)
                            NewItem += 10;
                        else
                            NewItem += 5000;
                    }
                if (Other.ItemInfo(ItemId)[1] == 40)
                    if (Other.ItemType2(ItemId) == 13)
                    {
                        if (Other.ItemInfo(ItemId)[3] < 112)
                            NewItem += 10;
                        else
                            NewItem += 5000;
                    }
                if (Other.ItemInfo(ItemId)[1] == 190)
                    if (Other.ItemType2(ItemId) == 13)
                    {
                        if (Other.ItemInfo(ItemId)[3] < 115)
                            NewItem += 10;
                        else
                            NewItem += 5000;
                    }
                if (Other.ItemInfo(ItemId)[1] == 21)
                    if (Other.ItemType2(ItemId) == 11)
                    {
                        if (Other.ItemInfo(ItemId)[3] < 112)
                            NewItem += 10;
                        else
                            NewItem += 920;
                    }
                if (Other.ItemInfo(ItemId)[1] == 11)
                    if (Other.ItemType2(ItemId) == 11)
                    {
                        if (Other.ItemInfo(ItemId)[3] < 112)
                            NewItem += 10;
                        else
                            NewItem -= 6010;
                    }
                if (Other.ItemInfo(ItemId)[1] == 40)
                    if (Other.ItemType2(ItemId) == 11)
                    {
                        if (Other.ItemInfo(ItemId)[3] < 117)
                            NewItem += 10;
                        else
                            NewItem -= 1060;
                    }
                if (Other.ItemInfo(ItemId)[1] == 190)
                    if (Other.ItemType2(ItemId) == 11)
                    {
                        if (Other.ItemInfo(ItemId)[3] < 112)
                            NewItem += 10;
                        else
                            NewItem -= 2050;
                    }

            }

            if (ItemId == 500301)
                NewItem = 500005;

            if (ItemId == 410301)
                NewItem = 410005;

            if (ItemId > 152012 && ItemId < 152020)
            {
                NewItem = ItemId;
                NewItem += 30;
            }

            if (ItemId > 421022 && ItemId < 421030)
            {
                NewItem = ItemId;
                NewItem += 20;
            }

            if (ItemId > 421042 && ItemId < 421050)
            {
                NewItem = ItemId;
                NewItem += 20;
            }

            if (ItemId > 117392 && ItemId < 117400)
            {
                NewItem = ItemId;
                NewItem += 100;
            }

            return NewItem;
        }
        public static bool NoPK(ushort Map)
        {
            bool Rt = false;

            foreach (ushort map in DataBase.NoPKMaps)
            {
                if (map == Map)
                {
                    Rt = true;
                    break;
                }
            }
            return Rt;
        }
        public static bool CanPK(ushort Map)
        {
            bool Rt = false;

            foreach (uint map in DataBase.PKMaps)
            {
                if (map == Map)
                {
                    Rt = true;
                    break;
                }
            }
            return Rt;
        }
        public static bool NoDrop(ushort Map)
        {
            bool Rt = false;

            foreach (ushort map in DataBase.NoDropMaps)
            {
                if (map == Map)
                {
                    Rt = true;
                    break;
                }
            }
            return Rt;
        }

        public static uint CalculateDamage(Character Attacker, Character Attacked, byte AttackType, ushort SkillId, byte SkillLvl)
        {
            if ((Attacker.PKMode == 2 && Attacked.GuildID != Attacker.GuildID && Attacked.Team != Attacker.Team && !Attacker.Friends.Contains(Attacked.UID)) || Attacker.PKMode == 0)
            {
                Attacker.PTarget = Attacked;
                double Damage = 0;
                byte AtkType = 2;
                double BlessStats = 0;
                double MythiqueStats = 0;
                ushort[] SkillAttributes = DataBase.SkillAttributes[SkillId][SkillLvl];
                int ExtraDamage = (int)SkillAttributes[3];

                if (AttackType == 0 || AttackType == 1)//Melee
                {
                    double reborn = 0;
                    double Shield = 0;
                    double IronShirt = 0;
                    short ProfId = 0;
                    double ProfBonus = 0;
                    double BonusDef = 0;

                    Damage = General.Rand.Next((int)Attacker.MinAtk, (int)Attacker.MaxAtk);
                    if (AttackType == 0)
                        Damage += ExtraDamage;
                    else
                        Damage = (int)(((double)ExtraDamage / 100) * Damage);

                    Damage = (int)((double)Damage * Attacker.AddAtkPc);
                    Damage += (Damage * ProfBonus);

                    if (Attacker.Equips[4] != null && Attacker.Equips[4] != "0")
                    {
                        string[] Splitter2 = Attacker.Equips[4].Split('-');
                        if (ItemType(uint.Parse(Splitter2[0])) == 4 || ItemType(uint.Parse(Splitter2[0])) == 5)
                        {
                            ProfId = (short)WeaponType(uint.Parse(Splitter2[0]));
                            if (Attacker.Profs.Contains(ProfId))
                            {
                                byte Prof_Lev = (byte)Attacker.Profs[ProfId];
                                if (Prof_Lev == 13)
                                    ProfBonus += 0.01;
                                else if (Prof_Lev == 14)
                                    ProfBonus += 0.02;
                                else if (Prof_Lev == 15)
                                    ProfBonus += 0.03;
                                else if (Prof_Lev == 16)
                                    ProfBonus += 0.04;
                                else if (Prof_Lev == 17)
                                    ProfBonus += 0.05;
                                else if (Prof_Lev == 18)
                                    ProfBonus += 0.06;
                                else if (Prof_Lev == 19)
                                    ProfBonus += 0.07;
                                else if (Prof_Lev == 20)
                                    ProfBonus += 0.08;
                                else
                                    ProfBonus = 0;
                            }
                        }

                    }
                    if (Attacker.Equips[5] != null && Attacker.Equips[5] != "0")
                    {
                        string[] Splitter2 = Attacker.Equips[5].Split('-');
                        if (ItemType(uint.Parse(Splitter2[0])) == 4 || ItemType(uint.Parse(Splitter2[0])) == 5)
                        {
                            ProfId = (short)WeaponType(uint.Parse(Splitter2[0]));
                            if (Attacker.Profs.Contains(ProfId))
                            {
                                byte Prof_Lev = (byte)Attacker.Profs[ProfId];
                                if (Prof_Lev == 13)
                                    ProfBonus += 0.01;
                                else if (Prof_Lev == 14)
                                    ProfBonus += 0.02;
                                else if (Prof_Lev == 15)
                                    ProfBonus += 0.03;
                                else if (Prof_Lev == 16)
                                    ProfBonus += 0.04;
                                else if (Prof_Lev == 17)
                                    ProfBonus += 0.05;
                                else if (Prof_Lev == 18)
                                    ProfBonus += 0.06;
                                else if (Prof_Lev == 19)
                                    ProfBonus += 0.07;
                                else if (Prof_Lev == 20)
                                    ProfBonus += 0.08;
                                else
                                    ProfBonus = 0;
                            }
                        }
                    }

                    if (Attacked.MShieldBuff)
                        BonusDef = (Attacked.Defense * (10 + Attacked.MShieldLevel * 5) / 100);

                    if (Attacker.StigBuff)
                        Damage = (int)(Damage * (double)(1 + ((double)(10 + Attacker.StigLevel) / 100)));

                    if (Attacker.SMOn)
                        Damage *= 2;

                    BlessStats = Attacked.Bless / 100;
                    MythiqueStats = Attacked.Mythique / 100;

                    if (Attacked.RBCount == 1)
                        reborn = 0.7;
                    else if (Attacked.RBCount == 2)
                        reborn = 0.5;
                    else if (Attacked.RBCount == 0)
                        reborn = 1;

                    if (Attacked.ShieldBuff == true)
                        Shield = 3;
                    else
                        Shield = 1;

                    if (Attacked.IronBuff == true)
                        IronShirt = 4;
                    else
                        IronShirt = 1;

                    Damage *= reborn;
                    Damage -= (Attacked.Defense * Shield * IronShirt + BonusDef);
                    Damage -= (Damage * BlessStats);
                    Damage -= (Damage * MythiqueStats);

                    if (Attacker.LuckTime > 0 && ChanceSuccess(10))
                    {
                        Damage *= 2;
                        foreach (DictionaryEntry DE in World.AllChars)
                        {
                            Character Chaar = (Character)DE.Value;
                            if (Chaar.Name != Attacker.Name)
                            {
                                Chaar.MyClient.SendPacket(General.MyPackets.String(Attacker.UID, 10, "LuckyGuy"));
                            }
                        }
                        Attacker.MyClient.SendPacket(General.MyPackets.String(Attacker.UID, 10, "LuckyGuy"));
                    }

                    if (Attacked.LuckTime > 0 && ChanceSuccess(10))
                    {
                        Damage = 1;
                        foreach (DictionaryEntry DE in World.AllChars)
                        {
                            Character Chaar = (Character)DE.Value;
                            if (Chaar.Name != Attacked.Name)
                            {
                                Chaar.MyClient.SendPacket(General.MyPackets.String(Attacked.UID, 10, "LuckyGuy"));
                            }
                        }
                        Attacked.MyClient.SendPacket(General.MyPackets.String(Attacked.UID, 10, "LuckyGuy"));
                    }

                    if (Damage < 1)
                        Damage = 1;

                    if (Attacked.ReflectOn == true && ChanceSuccess(15))
                    {
                        if (Damage >= Attacker.CurHP)
                        {
                            int EModel;
                            if (Attacker.Model == 1003 || Attacker.Model == 1004)
                                EModel = 15099;
                            else
                                EModel = 15199;

                            Attacker.Die();
                            Attacker.CurHP = 0;
                            Attacker.Death = DateTime.Now;
                            Attacker.Alive = false;
                            Attacker.XpList = false;
                            Attacker.SMOn = false;
                            Attacker.CycloneOn = false;
                            Attacker.XpCircle = 0;
                            Attacker.Flying = false;
                            Attacker.MyClient.SendPacket(General.MyPackets.Vital(Attacker.UID, 26, Attacker.GetStat()));
                            Attacker.MyClient.SendPacket(General.MyPackets.Status1(Attacker.UID, EModel));
                            Attacker.MyClient.SendPacket(General.MyPackets.Death(Attacker));
                            World.UpdateSpawn(Attacker);
                        }
                        else
                        {
                            Attacker.CurHP -= (ushort)Damage;
                            Attacker.MyClient.SendPacket(General.MyPackets.Vital(Attacker.UID, 0, Attacker.CurHP));
                        }

                        foreach (DictionaryEntry DE in World.AllChars)
                        {
                            Character Chaar = (Character)DE.Value;
                            if (Chaar.Name != Attacked.Name)
                            {
                                Chaar.MyClient.SendPacket(General.MyPackets.String(Attacked.UID, 10, "WeaponReflect"));
                            }

                            if (Chaar != null)
                                if (MyMath.CanSeeBig(Attacker.LocX, Attacker.LocY, Chaar.LocX, Chaar.LocY))
                                    if (Chaar.MyClient.Online)
                                    {
                                        Chaar.MyClient.SendPacket(General.MyPackets.Attack((uint)Attacker.UID, (uint)Attacker.UID, (short)Attacker.LocX, (short)Attacker.LocY, AtkType, (uint)Damage));
                                    }
                        }
                        Attacked.MyClient.SendPacket(General.MyPackets.String(Attacked.UID, 10, "WeaponReflect"));
                        Damage = 0;
                    }

                    Attacked.BlessEffect();
                }
                else if (AttackType == 2)//Ranged
                {
                    Attacker.PTarget = Attacked;
                    double ee = 0;
                    double reborn = 0;
                    double ExtraDodge = 0;
                    short ProfId = 0;
                    double ProfBonus = 0;
                    Damage = General.Rand.Next((int)Attacker.MinAtk, (int)Attacker.MaxAtk);
                    Damage += ExtraDamage;

                    Damage = (int)((double)Damage * Attacker.AddAtkPc);

                    if (Attacker.SMOn)
                        Damage *= 2;
                    if (Attacker.StigBuff)
                        Damage = (int)(Damage * (double)(1 + ((double)(10 + Attacker.StigLevel) / 100)));

                    BlessStats = Attacked.Bless / 100;
                    MythiqueStats = Attacked.Mythique / 100;

                    if (Attacked.RBCount == 1)
                        reborn = 0.7;
                    else if (Attacked.RBCount == 2)
                        reborn = 0.5;
                    else if (Attacked.RBCount == 0)
                        reborn = 1;

                    if (Attacked.DodgeBuff)
                        ExtraDodge = (20 + Attacked.DodgeLevel * 5);

                    if (Attacker.Equips[4] != null && Attacker.Equips[4] != "0")
                    {
                        string[] Splitter2 = Attacker.Equips[4].Split('-');
                        if (ItemType(uint.Parse(Splitter2[0])) == 4 || ItemType(uint.Parse(Splitter2[0])) == 5)
                        {
                            ProfId = (short)WeaponType(uint.Parse(Splitter2[0]));
                            if (Attacker.Profs.Contains(ProfId))
                            {
                                byte Prof_Lev = (byte)Attacker.Profs[ProfId];
                                if (Prof_Lev == 13)
                                    ProfBonus += 0.01;
                                else if (Prof_Lev == 14)
                                    ProfBonus += 0.02;
                                else if (Prof_Lev == 15)
                                    ProfBonus += 0.03;
                                else if (Prof_Lev == 16)
                                    ProfBonus += 0.04;
                                else if (Prof_Lev == 17)
                                    ProfBonus += 0.05;
                                else if (Prof_Lev == 18)
                                    ProfBonus += 0.06;
                                else if (Prof_Lev == 19)
                                    ProfBonus += 0.07;
                                else if (Prof_Lev == 20)
                                    ProfBonus += 0.08;
                                else
                                    ProfBonus = 0;
                            }
                        }

                    }
                    if (Attacker.Equips[5] != null && Attacker.Equips[5] != "0")
                    {
                        string[] Splitter2 = Attacker.Equips[5].Split('-');
                        if (ItemType(uint.Parse(Splitter2[0])) == 4 || ItemType(uint.Parse(Splitter2[0])) == 5)
                        {
                            ProfId = (short)WeaponType(uint.Parse(Splitter2[0]));
                            if (Attacker.Profs.Contains(ProfId))
                            {
                                byte Prof_Lev = (byte)Attacker.Profs[ProfId];
                                if (Prof_Lev == 13)
                                    ProfBonus += 0.01;
                                else if (Prof_Lev == 14)
                                    ProfBonus += 0.02;
                                else if (Prof_Lev == 15)
                                    ProfBonus += 0.03;
                                else if (Prof_Lev == 16)
                                    ProfBonus += 0.04;
                                else if (Prof_Lev == 17)
                                    ProfBonus += 0.05;
                                else if (Prof_Lev == 18)
                                    ProfBonus += 0.06;
                                else if (Prof_Lev == 19)
                                    ProfBonus += 0.07;
                                else if (Prof_Lev == 20)
                                    ProfBonus += 0.08;
                                else
                                    ProfBonus = 0;
                            }
                        }
                    }

                    ee = (((((100 - ((double)Attacked.Dodge + ExtraDodge)) / 100) * 0.12) * reborn));
                    Damage += (Damage * ProfBonus);
                    Damage *= ee;
                    Damage -= (Damage * BlessStats);
                    Damage -= (Damage * MythiqueStats);

                    if (Attacker.LuckTime > 0 && ChanceSuccess(10))
                    {
                        Damage *= 2;
                        foreach (DictionaryEntry DE in World.AllChars)
                        {
                            Character Chaar = (Character)DE.Value;
                            if (Chaar.Name != Attacker.Name)
                            {
                                Chaar.MyClient.SendPacket(General.MyPackets.String(Attacker.UID, 10, "LuckyGuy"));
                            }
                        }
                        Attacker.MyClient.SendPacket(General.MyPackets.String(Attacker.UID, 10, "LuckyGuy"));
                    }

                    if (Attacked.LuckTime > 0 && ChanceSuccess(10))
                    {
                        Damage = 1;
                        foreach (DictionaryEntry DE in World.AllChars)
                        {
                            Character Chaar = (Character)DE.Value;
                            if (Chaar.Name != Attacked.Name)
                            {
                                Chaar.MyClient.SendPacket(General.MyPackets.String(Attacked.UID, 10, "LuckyGuy"));
                            }
                        }
                        Attacked.MyClient.SendPacket(General.MyPackets.String(Attacked.UID, 10, "LuckyGuy"));
                    }

                    if (Damage < 1)
                        Damage = 1;

                    if (Attacked.ReflectOn == true && ChanceSuccess(15))
                    {
                        if (Damage >= Attacker.CurHP)
                        {
                            int EModel;
                            if (Attacker.Model == 1003 || Attacker.Model == 1004)
                                EModel = 15099;
                            else
                                EModel = 15199;

                            Attacker.Die();
                            Attacker.CurHP = 0;
                            Attacker.Death = DateTime.Now;
                            Attacker.Alive = false;
                            Attacker.XpList = false;
                            Attacker.SMOn = false;
                            Attacker.CycloneOn = false;
                            Attacker.XpCircle = 0;
                            Attacker.Flying = false;
                            Attacker.MyClient.SendPacket(General.MyPackets.Vital(Attacker.UID, 26, Attacker.GetStat()));
                            Attacker.MyClient.SendPacket(General.MyPackets.Status1(Attacker.UID, EModel));
                            Attacker.MyClient.SendPacket(General.MyPackets.Death(Attacker));
                            World.UpdateSpawn(Attacker);
                        }
                        else
                        {
                            Attacker.CurHP -= (ushort)Damage;
                            Attacker.MyClient.SendPacket(General.MyPackets.Vital(Attacker.UID, 0, Attacker.CurHP));
                        }

                        foreach (DictionaryEntry DE in World.AllChars)
                        {
                            Character Chaar = (Character)DE.Value;
                            if (Chaar.Name != Attacked.Name)
                            {
                                Chaar.MyClient.SendPacket(General.MyPackets.String(Attacked.UID, 10, "WeaponReflect"));
                            }

                            if (Chaar != null)
                                if (MyMath.CanSeeBig(Attacker.LocX, Attacker.LocY, Chaar.LocX, Chaar.LocY))
                                    if (Chaar.MyClient.Online)
                                    {
                                        Chaar.MyClient.SendPacket(General.MyPackets.Attack((uint)Attacker.UID, (uint)Attacker.UID, (short)Attacker.LocX, (short)Attacker.LocY, AtkType, (uint)Damage));
                                    }
                        }
                        Attacked.MyClient.SendPacket(General.MyPackets.String(Attacked.UID, 10, "WeaponReflect"));
                        Damage = 0;
                    }

                    Attacked.BlessEffect();
                }
                else if (AttackType == 3 || AttackType == 4)//Magic
                {
                    Attacker.PTarget = Attacked;
                    double reborn = 0;
                    if (AttackType == 3)
                        Damage = (int)Attacker.MAtk + ExtraDamage;
                    else
                        Damage = (int)((double)ExtraDamage * Attacker.MAtk);

                    double Pc = ((100 - Attacked.MDefense));

                    BlessStats = Attacked.Bless / 100;
                    MythiqueStats = Attacked.Mythique / 100;

                    if (Attacked.RBCount == 1)
                        reborn = 0.7;
                    else if (Attacked.RBCount == 2)
                        reborn = 0.5;
                    else if (Attacked.RBCount == 0)
                        reborn = 1;

                    Damage = (int)(((double)Damage * Attacker.AddMAtkPc) / 100);
                    Damage = (int)(Damage * Pc);
                    Damage -= (int)Attacked.MagicBlock;
                    Damage = (double)(Damage * 0.75 * reborn);
                    Damage -= (Damage * BlessStats);
                    Damage -= (Damage * MythiqueStats);

                    if (Attacker.LuckTime > 0 && ChanceSuccess(10))
                    {
                        Damage *= 2;
                        foreach (DictionaryEntry DE in World.AllChars)
                        {
                            Character Chaar = (Character)DE.Value;
                            if (Chaar.Name != Attacker.Name)
                            {
                                Chaar.MyClient.SendPacket(General.MyPackets.String(Attacker.UID, 10, "LuckyGuy"));
                            }
                        }
                        Attacker.MyClient.SendPacket(General.MyPackets.String(Attacker.UID, 10, "LuckyGuy"));
                    }

                    if (Attacked.LuckTime > 0 && ChanceSuccess(10))
                    {
                        Damage = 1;
                        foreach (DictionaryEntry DE in World.AllChars)
                        {
                            Character Chaar = (Character)DE.Value;
                            if (Chaar.Name != Attacked.Name)
                            {
                                Chaar.MyClient.SendPacket(General.MyPackets.String(Attacked.UID, 10, "LuckyGuy"));
                            }
                        }
                        Attacked.MyClient.SendPacket(General.MyPackets.String(Attacked.UID, 10, "LuckyGuy"));
                    }

                    if (Damage < 1)
                        Damage = 1;

                    if (Attacked.ReflectOn == true && ChanceSuccess(15))
                    {
                        if (Damage >= Attacker.CurHP)
                        {
                            int EModel;
                            if (Attacker.Model == 1003 || Attacker.Model == 1004)
                                EModel = 15099;
                            else
                                EModel = 15199;

                            Attacker.Die();
                            Attacker.CurHP = 0;
                            Attacker.Death = DateTime.Now;
                            Attacker.Alive = false;
                            Attacker.XpList = false;
                            Attacker.SMOn = false;
                            Attacker.CycloneOn = false;
                            Attacker.XpCircle = 0;
                            Attacker.Flying = false;
                            Attacker.MyClient.SendPacket(General.MyPackets.Vital(Attacker.UID, 26, Attacker.GetStat()));
                            Attacker.MyClient.SendPacket(General.MyPackets.Status1(Attacker.UID, EModel));
                            Attacker.MyClient.SendPacket(General.MyPackets.Death(Attacker));
                            World.UpdateSpawn(Attacker);
                        }
                        else
                        {
                            Attacker.CurHP -= (ushort)Damage;
                            Attacker.MyClient.SendPacket(General.MyPackets.Vital(Attacker.UID, 0, Attacker.CurHP));
                        }

                        foreach (DictionaryEntry DE in World.AllChars)
                        {
                            Character Chaar = (Character)DE.Value;
                            if (Chaar.Name != Attacked.Name)
                            {
                                Chaar.MyClient.SendPacket(General.MyPackets.String(Attacked.UID, 10, "WeaponReflect"));
                            }

                            if (Chaar != null)
                                if (MyMath.CanSeeBig(Attacker.LocX, Attacker.LocY, Chaar.LocX, Chaar.LocY))
                                    if (Chaar.MyClient.Online)
                                    {
                                        Chaar.MyClient.SendPacket(General.MyPackets.Attack((uint)Attacker.UID, (uint)Attacker.UID, (short)Attacker.LocX, (short)Attacker.LocY, AtkType, (uint)Damage));
                                    }
                        }
                        Attacked.MyClient.SendPacket(General.MyPackets.String(Attacked.UID, 10, "WeaponReflect"));
                        Damage = 0;
                    }

                    Attacked.BlessEffect();
                }
                return (uint)Damage;
            }
            else
            {
                Attacker.PTarget = null;
                Attacked = null;
                Attacker.Attacking = false;
                return 0;
            }
        }
        public static uint CalculateDamage(Character Attacker, SingleMob Attacked, byte AttackType, ushort SkillId, byte SkillLvl)
        {
            int Damage = 0;
            ushort[] SkillAttributes = DataBase.SkillAttributes[SkillId][SkillLvl];
            int ExtraDamage = (int)SkillAttributes[3];

            if (AttackType == 0 || AttackType == 1)//Melee
            {
                short ProfId = 0;
                double ProfBonus = 0;
                Attacker.MobTarget = Attacked;

                if (Attacker.Equips[4] != null && Attacker.Equips[4] != "0")
                {
                    string[] Splitter2 = Attacker.Equips[4].Split('-');
                    if (ItemType(uint.Parse(Splitter2[0])) == 4 || ItemType(uint.Parse(Splitter2[0])) == 5)
                    {
                        ProfId = (short)WeaponType(uint.Parse(Splitter2[0]));
                        if (Attacker.Profs.Contains(ProfId))
                        {
                            byte Prof_Lev = (byte)Attacker.Profs[ProfId];
                            if (Prof_Lev == 13)
                                ProfBonus += 0.01;
                            else if (Prof_Lev == 14)
                                ProfBonus += 0.02;
                            else if (Prof_Lev == 15)
                                ProfBonus += 0.03;
                            else if (Prof_Lev == 16)
                                ProfBonus += 0.04;
                            else if (Prof_Lev == 17)
                                ProfBonus += 0.05;
                            else if (Prof_Lev == 18)
                                ProfBonus += 0.06;
                            else if (Prof_Lev == 19)
                                ProfBonus += 0.07;
                            else if (Prof_Lev == 20)
                                ProfBonus += 0.08;
                            else
                                ProfBonus = 0;
                        }
                    }

                }
                if (Attacker.Equips[5] != null && Attacker.Equips[5] != "0")
                {
                    string[] Splitter2 = Attacker.Equips[5].Split('-');
                    if (ItemType(uint.Parse(Splitter2[0])) == 4 || ItemType(uint.Parse(Splitter2[0])) == 5)
                    {
                        ProfId = (short)WeaponType(uint.Parse(Splitter2[0]));
                        if (Attacker.Profs.Contains(ProfId))
                        {
                            byte Prof_Lev = (byte)Attacker.Profs[ProfId];
                            if (Prof_Lev == 13)
                                ProfBonus += 0.01;
                            else if (Prof_Lev == 14)
                                ProfBonus += 0.02;
                            else if (Prof_Lev == 15)
                                ProfBonus += 0.03;
                            else if (Prof_Lev == 16)
                                ProfBonus += 0.04;
                            else if (Prof_Lev == 17)
                                ProfBonus += 0.05;
                            else if (Prof_Lev == 18)
                                ProfBonus += 0.06;
                            else if (Prof_Lev == 19)
                                ProfBonus += 0.07;
                            else if (Prof_Lev == 20)
                                ProfBonus += 0.08;
                            else
                                ProfBonus = 0;
                        }
                    }
                }

                Damage = General.Rand.Next((int)Attacker.MinAtk, (int)Attacker.MaxAtk);
                if (AttackType == 0)
                    Damage += ExtraDamage;
                else
                    Damage = (int)(((double)ExtraDamage / 100) * Damage);

                Damage = (int)((double)Damage * Attacker.AddAtkPc);
                Damage += (int)(Damage * ProfBonus);

                if (Attacked.MType != 7)
                {
                    double LDF = (Attacker.Level - Attacked.Level + 7) / 5;
                    LDF = Math.Max(LDF, 1);
                    double eDMG = ((Convert.ToUInt32(LDF) - 1) * .8) + 1;
                    Damage = (int)(Damage * eDMG);
                }
                if (Attacker.Guard != null)
                {
                    if (Attacked.UID == Attacker.Guard.UID)
                    {
                        Damage = 0;
                        Attacker.MobTarget = null;
                    }
                }

                if (Attacker.SMOn)
                    Damage *= 10;

                if (Attacker.StigBuff)
                    Damage = (int)(Damage * (double)(1 + ((double)(10 + Attacker.StigLevel) / 100)));

                if (Attacker.LuckTime > 0 && ChanceSuccess(10))
                {
                    Damage *= 2;
                    foreach (DictionaryEntry DE in World.AllChars)
                    {
                        Character Chaar = (Character)DE.Value;
                        if (Chaar.Name != Attacker.Name)
                        {
                            Chaar.MyClient.SendPacket(General.MyPackets.String(Attacker.UID, 10, "LuckyGuy"));
                        }
                    }
                    Attacker.MyClient.SendPacket(General.MyPackets.String(Attacker.UID, 10, "LuckyGuy"));
                }
            }
            else if (AttackType == 2)//Ranged
            {
                short ProfId = 0;
                double ProfBonus = 0;
                Attacker.MobTarget = Attacked;

                if (Attacker.Equips[4] != null && Attacker.Equips[4] != "0")
                {
                    string[] Splitter2 = Attacker.Equips[4].Split('-');
                    if (ItemType(uint.Parse(Splitter2[0])) == 4 || ItemType(uint.Parse(Splitter2[0])) == 5)
                    {
                        ProfId = (short)WeaponType(uint.Parse(Splitter2[0]));
                        if (Attacker.Profs.Contains(ProfId))
                        {
                            byte Prof_Lev = (byte)Attacker.Profs[ProfId];
                            if (Prof_Lev == 13)
                                ProfBonus += 0.01;
                            else if (Prof_Lev == 14)
                                ProfBonus += 0.02;
                            else if (Prof_Lev == 15)
                                ProfBonus += 0.03;
                            else if (Prof_Lev == 16)
                                ProfBonus += 0.04;
                            else if (Prof_Lev == 17)
                                ProfBonus += 0.05;
                            else if (Prof_Lev == 18)
                                ProfBonus += 0.06;
                            else if (Prof_Lev == 19)
                                ProfBonus += 0.07;
                            else if (Prof_Lev == 20)
                                ProfBonus += 0.08;
                            else
                                ProfBonus = 0;
                        }
                    }

                }
                if (Attacker.Equips[5] != null && Attacker.Equips[5] != "0")
                {
                    string[] Splitter2 = Attacker.Equips[5].Split('-');
                    if (ItemType(uint.Parse(Splitter2[0])) == 4 || ItemType(uint.Parse(Splitter2[0])) == 5)
                    {
                        ProfId = (short)WeaponType(uint.Parse(Splitter2[0]));
                        if (Attacker.Profs.Contains(ProfId))
                        {
                            byte Prof_Lev = (byte)Attacker.Profs[ProfId];
                            if (Prof_Lev == 13)
                                ProfBonus += 0.01;
                            else if (Prof_Lev == 14)
                                ProfBonus += 0.02;
                            else if (Prof_Lev == 15)
                                ProfBonus += 0.03;
                            else if (Prof_Lev == 16)
                                ProfBonus += 0.04;
                            else if (Prof_Lev == 17)
                                ProfBonus += 0.05;
                            else if (Prof_Lev == 18)
                                ProfBonus += 0.06;
                            else if (Prof_Lev == 19)
                                ProfBonus += 0.07;
                            else if (Prof_Lev == 20)
                                ProfBonus += 0.08;
                            else
                                ProfBonus = 0;
                        }
                    }
                }

                Damage = General.Rand.Next((int)Attacker.MinAtk, (int)Attacker.MaxAtk);
                Damage = (int)(((double)ExtraDamage / 100) * Damage);
                Damage = (int)((double)Damage * ((double)(100 - Attacked.Dodge) / 100));

                Damage = (int)((double)Damage * Attacker.AddAtkPc);
                Damage += (int)(Damage * ProfBonus);

                if (Attacked.MType != 7)
                {
                    double LDF = (Attacker.Level - Attacked.Level + 7) / 5;
                    LDF = Math.Max(LDF, 1);
                    double eDMG = ((Convert.ToUInt32(LDF) - 1) * .8) + 1;
                    Damage = (int)(Damage * eDMG);
                }
                if (Attacker.Guard != null)
                {
                    if (Attacked.UID == Attacker.Guard.UID)
                    {
                        Damage = 0;
                        Attacker.MobTarget = null;
                    }
                }

                if (Attacker.SMOn)
                    Damage *= 10;

                if (Attacker.StigBuff)
                    Damage = (int)(Damage * (double)(1 + ((double)(10 + Attacker.StigLevel) / 100)));

                if (Attacker.LuckTime > 0 && ChanceSuccess(10))
                {
                    Damage *= 2;
                    foreach (DictionaryEntry DE in World.AllChars)
                    {
                        Character Chaar = (Character)DE.Value;
                        if (Chaar.Name != Attacker.Name)
                        {
                            Chaar.MyClient.SendPacket(General.MyPackets.String(Attacker.UID, 10, "LuckyGuy"));
                        }
                    }
                    Attacker.MyClient.SendPacket(General.MyPackets.String(Attacker.UID, 10, "LuckyGuy"));
                }
            }
            else if (AttackType == 3 || AttackType == 4)//Magic
            {
                Attacker.MobTarget = Attacked;
                if (AttackType == 3)
                    Damage = (int)Attacker.MAtk + ExtraDamage;
                else
                    Damage = (int)(((double)ExtraDamage / 100) * Attacker.MAtk);

                Damage = (int)((double)Damage * Attacker.AddMAtkPc);

                if (Attacked.MType != 7)
                {
                    double LDF = (Attacker.Level - Attacked.Level + 7) / 5;
                    LDF = Math.Max(LDF, 1);
                    double eDMG = ((Convert.ToUInt32(LDF) - 1) * .8) + 1;
                    Damage = (int)(Damage * eDMG);
                }
                if (Attacker.Guard != null)
                {
                    if (Attacked.UID == Attacker.Guard.UID)
                    {
                        Damage = 0;
                        Attacker.MobTarget = null;
                    }
                }

                Damage = (int)((double)Damage * Attacker.AddMAtkPc);

                if (Attacker.LuckTime > 0 && ChanceSuccess(10))
                {
                    Damage *= 2;
                    foreach (DictionaryEntry DE in World.AllChars)
                    {
                        Character Chaar = (Character)DE.Value;
                        if (Chaar.Name != Attacker.Name)
                        {
                            Chaar.MyClient.SendPacket(General.MyPackets.String(Attacker.UID, 10, "LuckyGuy"));
                        }
                    }
                    Attacker.MyClient.SendPacket(General.MyPackets.String(Attacker.UID, 10, "LuckyGuy"));
                }
            }
            if (Attacked.MType == 1)
                Damage /= 100;

            if (Attacked.MType == 3)
                Damage = 0;

            if (Attacked.MType == 4 && Attacker.Potency < 200 || Attacked.MType == 7 && Attacked.Level == 200 && Attacker.Potency < 200)
                Damage /= 100;

            if (Attacked.Map == 2021 || Attacked.Map == 2022 || Attacked.Map == 2023 || Attacked.Map == 2024)
                Damage /= 10;

            return (uint)Damage;
        }

        /*
Attacking a Non Reborn=1
Attacking a 1st Reborn=.7
Non or 1st Reborn Attacking 2nd Reborn=.5
2nd Reborn Attacking 2nd Reborn=.7

         * */
        public static uint CalculateDamage(Character Attacker, SingleNPC Attacked, byte AttackType, ushort SkillId, byte SkillLvl)
        {
            int Damage = 0;
            ushort[] SkillAttributes = DataBase.SkillAttributes[SkillId][SkillLvl];
            int ExtraDamage = (int)SkillAttributes[3];

            if (AttackType == 0 || AttackType == 1)//Melee
            {
                short ProfId = 0;
                double ProfBonus = 0;

                if (Attacker.Equips[4] != null && Attacker.Equips[4] != "0")
                {
                    string[] Splitter2 = Attacker.Equips[4].Split('-');
                    if (ItemType(uint.Parse(Splitter2[0])) == 4 || ItemType(uint.Parse(Splitter2[0])) == 5)
                    {
                        ProfId = (short)WeaponType(uint.Parse(Splitter2[0]));
                        if (Attacker.Profs.Contains(ProfId))
                        {
                            byte Prof_Lev = (byte)Attacker.Profs[ProfId];
                            if (Prof_Lev == 13)
                                ProfBonus += 0.01;
                            else if (Prof_Lev == 14)
                                ProfBonus += 0.02;
                            else if (Prof_Lev == 15)
                                ProfBonus += 0.03;
                            else if (Prof_Lev == 16)
                                ProfBonus += 0.04;
                            else if (Prof_Lev == 17)
                                ProfBonus += 0.05;
                            else if (Prof_Lev == 18)
                                ProfBonus += 0.06;
                            else if (Prof_Lev == 19)
                                ProfBonus += 0.07;
                            else if (Prof_Lev == 20)
                                ProfBonus += 0.08;
                            else
                                ProfBonus = 0;
                        }
                    }

                }
                if (Attacker.Equips[5] != null && Attacker.Equips[5] != "0")
                {
                    string[] Splitter2 = Attacker.Equips[5].Split('-');
                    if (ItemType(uint.Parse(Splitter2[0])) == 4 || ItemType(uint.Parse(Splitter2[0])) == 5)
                    {
                        ProfId = (short)WeaponType(uint.Parse(Splitter2[0]));
                        if (Attacker.Profs.Contains(ProfId))
                        {
                            byte Prof_Lev = (byte)Attacker.Profs[ProfId];
                            if (Prof_Lev == 13)
                                ProfBonus += 0.01;
                            else if (Prof_Lev == 14)
                                ProfBonus += 0.02;
                            else if (Prof_Lev == 15)
                                ProfBonus += 0.03;
                            else if (Prof_Lev == 16)
                                ProfBonus += 0.04;
                            else if (Prof_Lev == 17)
                                ProfBonus += 0.05;
                            else if (Prof_Lev == 18)
                                ProfBonus += 0.06;
                            else if (Prof_Lev == 19)
                                ProfBonus += 0.07;
                            else if (Prof_Lev == 20)
                                ProfBonus += 0.08;
                            else
                                ProfBonus = 0;
                        }
                    }
                }

                Damage = General.Rand.Next((int)Attacker.MinAtk, (int)Attacker.MaxAtk);
                if (AttackType == 0)
                    Damage += ExtraDamage;
                else
                    Damage = (int)(((double)ExtraDamage / 100) * Damage);

                Damage = (int)((double)Damage * Attacker.AddAtkPc);
                Damage += (int)(Damage * ProfBonus);

                if (Attacker.StigBuff)
                    Damage = (int)(Damage * (double)(1 + ((double)(10 + Attacker.StigLevel) / 100)));

                if (Attacker.LuckTime > 0 && ChanceSuccess(10))
                {
                    Damage *= 2;
                    foreach (DictionaryEntry DE in World.AllChars)
                    {
                        Character Chaar = (Character)DE.Value;
                        if (Chaar.Name != Attacker.Name)
                        {
                            Chaar.MyClient.SendPacket(General.MyPackets.String(Attacker.UID, 10, "LuckyGuy"));
                        }
                    }
                    Attacker.MyClient.SendPacket(General.MyPackets.String(Attacker.UID, 10, "LuckyGuy"));
                }

                if (Attacker.GuildID != 0 && Attacker.GuildID != null)
                {
                    if (Attacked.Sob == 2 && Attacker.MyGuild.HoldingPole == false)
                    {
                        Attacker.Silvers += (uint)(Damage / 100);
                        Attacker.MyGuild.Fund += (uint)(Damage / 100);
                        Attacker.GuildDonation += (uint)(Damage / 100);
                        Attacker.MyGuild.Refresh(Attacker);
                        Attacker.MyClient.SendPacket(General.MyPackets.GuildInfo(Attacker.MyGuild, Attacker));
                        Attacker.MyClient.SendPacket(General.MyPackets.Vital(Attacker.UID, 4, Attacker.Silvers));
                    }
                }
            }
            else if (AttackType == 2)//Ranged
            {
                short ProfId = 0;
                double ProfBonus = 0;

                if (Attacker.Equips[4] != null && Attacker.Equips[4] != "0")
                {
                    string[] Splitter2 = Attacker.Equips[4].Split('-');
                    if (ItemType(uint.Parse(Splitter2[0])) == 4 || ItemType(uint.Parse(Splitter2[0])) == 5)
                    {
                        ProfId = (short)WeaponType(uint.Parse(Splitter2[0]));
                        if (Attacker.Profs.Contains(ProfId))
                        {
                            byte Prof_Lev = (byte)Attacker.Profs[ProfId];
                            if (Prof_Lev == 13)
                                ProfBonus += 0.01;
                            else if (Prof_Lev == 14)
                                ProfBonus += 0.02;
                            else if (Prof_Lev == 15)
                                ProfBonus += 0.03;
                            else if (Prof_Lev == 16)
                                ProfBonus += 0.04;
                            else if (Prof_Lev == 17)
                                ProfBonus += 0.05;
                            else if (Prof_Lev == 18)
                                ProfBonus += 0.06;
                            else if (Prof_Lev == 19)
                                ProfBonus += 0.07;
                            else if (Prof_Lev == 20)
                                ProfBonus += 0.08;
                            else
                                ProfBonus = 0;
                        }
                    }

                }
                if (Attacker.Equips[5] != null && Attacker.Equips[5] != "0")
                {
                    string[] Splitter2 = Attacker.Equips[5].Split('-');
                    if (ItemType(uint.Parse(Splitter2[0])) == 4 || ItemType(uint.Parse(Splitter2[0])) == 5)
                    {
                        ProfId = (short)WeaponType(uint.Parse(Splitter2[0]));
                        if (Attacker.Profs.Contains(ProfId))
                        {
                            byte Prof_Lev = (byte)Attacker.Profs[ProfId];
                            if (Prof_Lev == 13)
                                ProfBonus += 0.01;
                            else if (Prof_Lev == 14)
                                ProfBonus += 0.02;
                            else if (Prof_Lev == 15)
                                ProfBonus += 0.03;
                            else if (Prof_Lev == 16)
                                ProfBonus += 0.04;
                            else if (Prof_Lev == 17)
                                ProfBonus += 0.05;
                            else if (Prof_Lev == 18)
                                ProfBonus += 0.06;
                            else if (Prof_Lev == 19)
                                ProfBonus += 0.07;
                            else if (Prof_Lev == 20)
                                ProfBonus += 0.08;
                            else
                                ProfBonus = 0;
                        }
                    }
                }

                Damage = General.Rand.Next((int)Attacker.MinAtk, (int)Attacker.MaxAtk);
                Damage = (int)((double)Damage * ((double)ExtraDamage / 100));
                Damage = (int)((double)Damage * Attacker.AddAtkPc);
                Damage += (int)(Damage * ProfBonus);
                Damage = (int)((double)Damage * ((double)(100 - Attacked.Dodge) / 100));

                if (Attacker.StigBuff)
                    Damage = (int)(Damage * (double)(1 + ((double)(10 + Attacker.StigLevel) / 100)));

                if (Attacker.LuckTime > 0 && ChanceSuccess(10))
                {
                    Damage *= 2;
                    foreach (DictionaryEntry DE in World.AllChars)
                    {
                        Character Chaar = (Character)DE.Value;
                        if (Chaar.Name != Attacker.Name)
                        {
                            Chaar.MyClient.SendPacket(General.MyPackets.String(Attacker.UID, 10, "LuckyGuy"));
                        }
                    }
                    Attacker.MyClient.SendPacket(General.MyPackets.String(Attacker.UID, 10, "LuckyGuy"));
                }

                if (Attacker.GuildID != 0 && Attacker.GuildID != null)
                {
                    if (Attacked.Sob == 2 && Attacker.MyGuild.HoldingPole == false)
                    {
                        Attacker.Silvers += (uint)(Damage / 100);
                        Attacker.MyGuild.Fund += (uint)(Damage / 100);
                        Attacker.GuildDonation += (uint)(Damage / 100);
                        Attacker.MyGuild.Refresh(Attacker);
                        Attacker.MyClient.SendPacket(General.MyPackets.GuildInfo(Attacker.MyGuild, Attacker));
                        Attacker.MyClient.SendPacket(General.MyPackets.Vital(Attacker.UID, 4, Attacker.Silvers));
                    }
                }
            }
            else if (AttackType == 3 || AttackType == 4)//Magic
            {
                if (AttackType == 3)
                    Damage = (int)Attacker.MAtk + ExtraDamage;
                else
                    Damage = (int)(((double)ExtraDamage / 100) * Attacker.MAtk);
                Damage = (int)((double)Damage * Attacker.AddMAtkPc);


                if (Attacker.LuckTime > 0 && ChanceSuccess(10))
                {
                    Damage *= 2;
                    foreach (DictionaryEntry DE in World.AllChars)
                    {
                        Character Chaar = (Character)DE.Value;
                        if (Chaar.Name != Attacker.Name)
                        {
                            Chaar.MyClient.SendPacket(General.MyPackets.String(Attacker.UID, 10, "LuckyGuy"));
                        }
                    }
                    Attacker.MyClient.SendPacket(General.MyPackets.String(Attacker.UID, 10, "LuckyGuy"));
                }

                if (Attacker.GuildID != 0 && Attacker.GuildID != null)
                {
                    if (Attacked.Sob == 2 && Attacker.MyGuild.HoldingPole == false)
                    {
                        Attacker.Silvers += (uint)(Damage / 100);
                        Attacker.MyGuild.Fund += (uint)(Damage / 100);
                        Attacker.GuildDonation += (uint)(Damage / 100);
                        Attacker.MyGuild.Refresh(Attacker);
                        Attacker.MyClient.SendPacket(General.MyPackets.GuildInfo(Attacker.MyGuild, Attacker));
                        Attacker.MyClient.SendPacket(General.MyPackets.Vital(Attacker.UID, 4, Attacker.Silvers));
                    }
                }
            }
            return (uint)Damage;
        }

        public static bool ChanceSuccess(double pc)
        {
            return ((double)General.Rand.Next(1, 1000000)) / 10000 >= 100 - pc;
        }

        public static uint GenerateGarment()
        {
            uint Item = 0;
            int Count = 0;
            int Do = General.Rand.Next(3, 30);
            foreach (uint[] item in DataBase.Items)
            {
                if (Other.ItemType2(item[0]) == 18)
                {
                    Count++;
                    Item = item[0];
                    if (Count == Do)
                        break;
                }

            }
            return Item;
        }
        public static uint GenerateCrap()
        {
            uint Item = 723700;

            if (ChanceSuccess(2.5))
                Item = 723730;
            else if (ChanceSuccess(5))
                Item = 723729;
            else if (ChanceSuccess(10))
                Item = 723728;
            else if (ChanceSuccess(20))
                Item = 723711;
            else if (ChanceSuccess(25))
                Item = 723712;
            else if (ChanceSuccess(30))
                Item = 1088001;
            else if (ChanceSuccess(30))
                Item = 730001;
            else if (ChanceSuccess(40))
            {
                Item = (uint)(700002 + (General.Rand.Next(7) * 10));
            }

            return Item;
        }
        public static uint GenerateSpecial()
        {
            uint Item = 723584;

            if (ChanceSuccess(5))
                Item = 722840;
            else if (ChanceSuccess(5))
                Item = 722841;
            else if (ChanceSuccess(5))
                Item = 722842;
            else if (ChanceSuccess(5))
                Item = 723701;
            else if (ChanceSuccess(5))
                Item = 1200000;
            else if (ChanceSuccess(5))
                Item = 1200001;
            else if (ChanceSuccess(5))
                Item = 1200002;
            else if (ChanceSuccess(5))
                Item = 1088000;
            else if (ChanceSuccess(25))
            {
                Item = (uint)(700003 + (General.Rand.Next(7) * 10));
            }

            return Item;
        }

        public static uint GenerateEtc()
        {
            if (ChanceSuccess(15))
            {
                uint Item = 723713;
                Item += (uint)General.Rand.Next(0, 7);
                if (ChanceSuccess(3))
                    Item = 723721;
                if (ChanceSuccess(2))
                    Item = 723722;
                if (ChanceSuccess(1))
                    Item = 723723;
                return Item;
            }
            else
            {
                uint Item = 730002;
                Item += (uint)General.Rand.Next(0, 3);
                if (ChanceSuccess(3))
                    Item = 730006;
                if (ChanceSuccess(2))
                    Item = 730007;
                if (ChanceSuccess(1.2))
                    Item = 730008;
                return Item;
            }
        }

        public static uint GenerateEquip(byte Level, byte Quality)
        {
            uint returns = 0;
            int tries;
            if (Other.ChanceSuccess(35))
                tries = General.Rand.Next(1, 8);
            else if (Other.ChanceSuccess(35))
                tries = General.Rand.Next(1, 16);
            else
                tries = General.Rand.Next(1, 32);
            int count = 0;
            byte ItemType1 = 0;
            short ItemType0 = 0;
            int nr = General.Rand.Next(1, 7);
            if (nr == 1)
                ItemType1 = 11;
            else if (nr == 2)
                ItemType1 = 12;
            else if (nr == 3)
                ItemType1 = 13;
            else if (nr == 4)
                ItemType1 = 4;
            else if (nr == 5)
                ItemType1 = 5;
            else if (nr == 6)
                ItemType1 = 15;
            else if (nr == 7)
                ItemType1 = 16;

            nr = General.Rand.Next(1, 17);
            if (nr == 1)
                ItemType0 = 410;
            else if (nr == 2)
                ItemType0 = 420;
            else if (nr == 3)
                ItemType0 = 421;
            else if (nr == 4)
                ItemType0 = 430;
            else if (nr == 5)
                ItemType0 = 440;
            else if (nr == 6)
                ItemType0 = 450;
            else if (nr == 7)
                ItemType0 = 460;
            else if (nr == 8)
                ItemType0 = 480;
            else if (nr == 9)
                ItemType0 = 481;
            else if (nr == 10)
                ItemType0 = 490;
            else if (nr == 11)
                ItemType0 = 500;
            else if (nr == 12)
                ItemType0 = 510;
            else if (nr == 13)
                ItemType0 = 530;
            else if (nr == 14)
                ItemType0 = 540;
            else if (nr == 15)
                ItemType0 = 560;
            else if (nr == 16)
                ItemType0 = 561;
            else if (nr == 17)
                ItemType0 = 580;

            foreach (uint[] item in DataBase.Items)
            {

                if (item[3] - 4 < Level && item[3] + 4 > Level)
                    if (ItemQuality(item[0]) == Quality)
                        if (ItemType(item[0]) == 1 || ItemType(item[0]) == 4 || ItemType(item[0]) == 5 || ItemType(item[5]) == 9)
                            if (ItemType2(item[0]) == ItemType1 || (ItemType1 == 4 && WeaponType(item[0]) == ItemType0) || (ItemType1 == 5 && WeaponType(item[0]) == ItemType0))
                            {
                                count++;
                                returns = item[0];

                                if (count >= tries)
                                    break;
                            }

            }
            return returns;
        }

        public static Character CharNearest(uint X, uint Y, uint Map, bool Blue)
        {
            try
            {
                int ShortestDist = 9999;
                Character NearestChar = null;
                foreach(DictionaryEntry DE in World.AllChars)
                {
                    Character Charr = (Character)DE.Value;
                    if (Map == Charr.LocMap)
                        if (MyMath.PointDistance(X, Y, Charr.LocX, Charr.LocY) < ShortestDist)
                            if (Blue && Charr.BlueName || !Blue)
                            {
                                NearestChar = Charr;
                                ShortestDist = MyMath.PointDistance(X, Y, Charr.LocX, Charr.LocY);
                            }

                }
                return NearestChar;
            }
            catch (Exception Exc) { General.WriteLine(Exc.ToString()); return null; }
        }

        public static SingleMob MobNearest(uint X, uint Y, uint Map)
        {
            try
            {
                int ShortestDist = 30;
                SingleMob NearestMob = null;
                foreach (DictionaryEntry DE in Mobs.AllMobs)
                {
                    SingleMob MOB = (SingleMob)DE.Value;
                    if (MyMath.PointDistance(X, Y, MOB.PosX, MOB.PosY) < ShortestDist && MOB.MType != 1 && MOB.Alive && MOB.Map == Map)
                    {
                        NearestMob = MOB;
                        ShortestDist = MyMath.PointDistance(X, Y, MOB.PosX, MOB.PosY);
                    }
                }
                return NearestMob;
            }
            catch (Exception Exc) { General.WriteLine(Exc.ToString()); return null; }
        }

        public static bool CharExist(string Needle, string HayStack)
        {
            int Find = HayStack.IndexOf(Needle);
            int Find2 = HayStack.LastIndexOf(Needle);
            return ((Find >= 0) && (Find2 >= 0));
        }
        public static Character Charowner(uint uid)
        {
            try
            {

                Character ownerchar = null;
                foreach (DictionaryEntry DE in World.AllChars)
                {
                    Character Charr = (Character)DE.Value;
                    if (uid == Charr.UID)

                        ownerchar = Charr;
                }
                return ownerchar;
            }
            catch (Exception Exc) { General.WriteLine(Exc.ToString()); return null; }
        }
        public static bool CanEquip(string iteM, Character Charr)
        {
            if (iteM.Length < 5)
                return false;
            string[] Splitter = iteM.Split('-');
            bool Returning = true;

            foreach (uint[] item in DataBase.Items)
            {
                if (item[0] == uint.Parse(Splitter[0]))
                {
                    byte TJob = 0;

                    if (Charr.Job > 129 && Charr.Job < 136)
                        TJob = (byte)(Charr.Job + 60);
                    else if (Charr.Job > 139 && Charr.Job < 146)
                        TJob = (byte)(Charr.Job + 50);
                    else
                        TJob = Charr.Job;

                        if (Charr.Level >= item[3])
                            if (Charr.Str >= item[5])
                                if (Charr.Agi >= item[6])
                                    if (TJob - item[1] >= 0 && TJob - item[1] <= 5 || TJob == 0)
                                        if (Charr.Model != 1002 && Charr.Model != 1003 || item[4] == 0)
                                        {
                                            if (ItemType(item[0]) == 4 || ItemType(item[0]) == 5 && WeaponType(item[0]) != 421)
                                            {
                                                if (WeaponType(item[0]) == 500)
                                                    Charr.AtkType = 25;
                                                else
                                                    Charr.AtkType = 2;

                                                if (Charr.Profs.Contains(WeaponType(item[0])))
                                                    if ((uint)Charr.Profs[WeaponType(item[0])] >= item[2])
                                                        Returning = true;
                                                    else
                                                        Returning = false;
                                            }
                                            else
                                            {
                                                Returning = true;
                                            }
                                        }
                                        else
                                            Returning = false;

                    if (ItemType(item[0]) == 9 && Charr.Job > 19 && Charr.Job < 26)
                        Returning = true;

                    break;
                }
            }

            if (Splitter[0] == "1200006" || Splitter[0] == "722343" || Splitter[0] == "722344" || Splitter[0] == "722345" || Splitter[0] == "722346" || Splitter[0] == "722347" || Splitter[0] == "722348" || Splitter[0] == "722348" || Splitter[0] == "722349" || Splitter[0] == "722350" || Splitter[0] == "722351" || Splitter[0] == "722352")
                Returning = false;
            if (Splitter[0] == "137910" || Splitter[0] == "137810" || Splitter[0] == "137710" || Splitter[0] == "137610" || Splitter[0] == "137510" || Splitter[0] == "137410" || Splitter[0] == "137310" && Charr.MyClient.Status != 8)
                Returning = false;

            if (Charr.MyClient.Status == 7)
                Returning = false;

            return Returning;
        }

        public static bool Upgradable(uint item)
        {
            return (ItemType2(item) == 90 || ItemType2(item) == 11 || ItemType2(item) == 12 || ItemType2(item) == 13 || ItemType2(item) == 15 || ItemType2(item) == 16 || ItemType(item) == 4 || ItemType(item) == 5);
        }

        public static bool ArmorType(uint item)
        {
            return (ItemType2(item) == 11 || ItemType2(item) == 13);
        }



        public static uint[] ItemInfo(uint Item)
        {
            uint[] Returns = null;

            foreach (uint[] item in DataBase.Items)
            {
                if (item[0] == Item)
                {
                    Returns = item;
                }
            }
            return Returns;
        }

        public static bool EquipMaxedLvl(uint Item)
        {
            bool R = false;

            if (ItemType(Item) == 4 )
                if (ItemInfo(Item)[3] == 130)
                    R = true;

            if (ItemType(Item) == 5 && WeaponType(Item) != 530)
                if (ItemInfo(Item)[3] == 130)
                    R = true;

            if (WeaponType(Item) == 530)
                if (ItemInfo(Item)[3] == 137)
                    R = true;

            if (ItemType2(Item) == 11)
                if (ItemInfo(Item)[3] == 120)
                    R = true;

            if (ItemType2(Item) == 13)
                if (ItemInfo(Item)[3] == 120)
                    R = true;

            if (WeaponType(Item) == 150)
                if (ItemInfo(Item)[3] == 131)
                    R = true;

            if (WeaponType(Item) == 121)
                if (ItemInfo(Item)[3] == 126)
                    R = true;

            if (WeaponType(Item) == 120)
                if (ItemInfo(Item)[3] == 135)
                    R = true;

            if (WeaponType(Item) == 151)
                if (ItemInfo(Item)[3] == 132)
                    R = true;

            if (WeaponType(Item) == 152)
                if (ItemInfo(Item)[3] == 132)
                    R = true;

            if (WeaponType(Item) == 160)
                if (ItemInfo(Item)[3] == 134)
                    R = true;

            return R;
        }

        public static int ItemQuality(uint item)
        {
            string Item = Convert.ToString(item);
            string qual = Item.Remove(0, Item.Length - 1);

            return int.Parse(qual);
        }

        public static uint ItemQualityChange(uint item, byte To)
        {
            string Item = Convert.ToString(item);
            Item = Item.Remove(Item.Length - 1, 1);
            Item = Item + To.ToString();

            return uint.Parse(Item);
        }

        public static int WeaponType(uint item)
        {
            string Item = Convert.ToString(item);
            string type = Item.Remove(3, Item.Length - 3);

            return int.Parse(type);
        }

        public static int ItemType(uint item)
        {
            string Item = Convert.ToString(item);
            string type = Item.Remove(1, Item.Length - 1);

            return int.Parse(type);
        }

        public static int ItemType2(uint item)
        {
            string Item = Convert.ToString(item);
            string type = Item.Remove(2, Item.Length - 2);

            return int.Parse(type);
        }

        public static string DisplayPacket(byte[] Pack)
        {
            string pack = "";

            foreach (byte b in Pack)
            {
                pack += Convert.ToString(b) + " ";
            }
            pack.Remove(pack.Length - 1, 1);

            return pack;
        }
        public static string DisplayPacketC(byte[] Pack)
        {
            string pack = "";

            foreach (byte b in Pack)
            {
                pack += Convert.ToChar(b).ToString();
            }

            return pack;
        }
        public static bool PlaceFree(short X, short Y, byte MoveDir)
        {
            bool Ret = true;

            switch (MoveDir)
            {
                case 0:
                    {
                        Y += 1;
                        break;
                    }
                case 1:
                    {
                        X -= 1;
                        Y += 1;
                        break;
                    }
                case 2:
                    {
                        X -= 1;
                        break;
                    }
                case 3:
                    {
                        X -= 1;
                        Y -= 1;
                        break;
                    }
                case 4:
                    {
                        Y -= 1;
                        break;
                    }
                case 5:
                    {
                        X += 1;
                        Y -= 1;
                        break;
                    }
                case 6:
                    {
                        X += 1;
                        break;
                    }
                case 7:
                    {
                        Y += 1;
                        X += 1;
                        break;
                    }
            }

            foreach (DictionaryEntry DE in Mobs.AllMobs)
            {
                SingleMob Mob = (SingleMob)DE.Value;
                if (Mob.PosX == X)
                    if (Mob.PosY == Y)
                        Ret = false;
            }
            return Ret;
        }
    }
    public static class MyMath
    {
        public static int rol(int value, int places, int len)
        {
            return (value << places) | (value >> (len - places));
        }

        public static int ror(int value, int places, int len)
        {
            return (value >> places) | (value << len - places);
        }

        public static int PointDistance(double x1, double y1, double x2, double y2)
        {
            return (int)Math.Sqrt(((x1 - x2) * (x1 - x2)) + ((y1 - y2) * (y1 - y2)));
        }

        public static bool CanSee(double x1, double y1, double x2, double y2)
        {
            return (Math.Max(Math.Abs(x1 - x2), Math.Abs(y1 - y2)) <= 15);
        }

        public static bool CanSeeBig(double x1, double y1, double x2, double y2)
        {
            return (Math.Max(Math.Abs(x1 - x2), Math.Abs(y1 - y2)) <= 30);
        }


        public static double PointDirecton(double x1, double y1, double x2, double y2)
        {
            double direction = 0;

            double AddX = x2 - x1;
            double AddY = y2 - y1;
            double r = (double)Math.Atan2(AddY, AddX);

            if (r < 0) r += (double)Math.PI * 2;

            direction = 360 - (r * 180 / (double)Math.PI);
            return direction;
        }
    }
}

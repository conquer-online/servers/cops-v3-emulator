﻿using System;
using System.Collections;
using System.Text;

namespace COServer_Project
{
    class OpenShop
    {
        private Client TClient;
        private byte[] data;
        public OpenShop(byte[] packet, Client client)
        {
            data = packet;
            TClient = client;
         //   shopid = _shopid;
            runshop();
        }
        void runshop()
        {
            try
            {
                int CharID = (data[11] << 24) + (data[10] << 16) + (data[9] << 8) + data[8];
                int X = (data[13] << 8) + data[12];
                int Y = (data[15] << 8) + data[14];
                if (TClient.MyChar.ShopID == 0)
                {
                    uint map_in = Convert.ToUInt32(TClient.MyChar.LocMap);

                 //   lock (NPCs.AllNPCs.SyncRoot)
                    {
                        foreach (DictionaryEntry DE in NPCs.AllNPCs)
                        {
                            SingleNPC Npc = (SingleNPC)DE.Value;

                            int N0 = Convert.ToInt32(Npc.UID);
                            int N1 = Convert.ToInt32(Npc.X);
                            int N2 = Convert.ToInt32(Npc.Y);
                            int N6 = Convert.ToInt32(Npc.Map);

                            if (map_in == N6)
                            {
                                if (N1 + 2 == TClient.MyChar.LocX && N2 == TClient.MyChar.LocY)
                                {
                                    int ShopID = N0 + 500;
                                    if (!World.ShopHash.Contains(ShopID))
                                    {
                                        TClient.MyChar.ShopID = ShopID;

                                        World.ShopHash.Add(ShopID, TClient);

                                        TClient.SendPacket(General.MyPackets.GeneralData3((long)CharID,TClient.MyChar.LocX, TClient.MyChar.LocY, 111)); 

                                        foreach (DictionaryEntry sr in World.AllChars)
                                        {
                                            Character Char = (Character)sr.Value;
                                            if (MyMath.CanSee(TClient.MyChar.LocX, TClient.MyChar.LocY, Char.LocX, Char.LocY) && TClient.MyChar.LocMap == Char.LocMap)
                                            {
                                                Char.MyClient.SendPacket(Packets.SpawnRug.Packet(TClient.MyChar, ShopID));
                                               
                                            }
                                        }
                                        TClient.SendPacket(General.MyPackets.GeneralData((long)CharID, (long)ShopID, TClient.MyChar.LocX, TClient.MyChar.LocY, 111)); 
                                        TClient.MyChar.Direction = 6;
                                    }
                                    else
                                    {
                                        TClient.SendPacket(General.MyPackets.SendMsg(15445, "SYSTEM", TClient.MyChar.Name, "This shop is in use, try another.", 2005));
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    TClient.SendPacket(General.MyPackets.SendMsg(15445, "SYSTEM", TClient.MyChar.Name, "You start vending.", 2005));
                }
            }
            catch
            {
               
            }
        }
    }
}
